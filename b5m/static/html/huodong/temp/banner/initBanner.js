var americaFed = window.americaFed || {};

americaFed.indexFun = function() {
  this.indexInit();
  // window.Cookies.set('hddatacount',1);
};

americaFed.indexInit = function() {
	this.b5mBanner({
		b5m:'http://www.b5m.com', //配置B5M帐号登录；
		sina:'http://www.sina.com', //配置微博登录；
		qq:'http://www.qq.com', //配置QQ登录；
		taobao:'http://www.taobao.com', //配置淘宝登录；
		reg:'http://www.b5m.com' //配置注册；
	});
};

americaFed.b5mBanner = function(options){
	// 检测IE6支持
    if(typeof document.body.style.maxHeight === 'undefined') {
        return false;
    }
	options=$.extend({b5m:null,sina:null,qq:0,taobao:'cur',reg:'reg'},options||{});
	var bannerHtml = "<div class='b5m-banner'><div class='banner-wapper cf'><a href='javascript:void(0)' class='btn-banner-colse'>关闭</a><div class='banner-right cf'><b>还没有帐号？</b><a href='"+options.reg+"'>轻松注册</a></div><div class='banner-left'><h4>登录后可参与iphone5s免费抽奖~~每月还送ipad mini，快快行动吧！</h4><a href='"+options.b5m+"'><i class='icon-banner-b5m'>B5M帐号登录</i>B5M帐号登录</a><a href='"+options.sina+"'><i class='icon-banner-sina'>微博登录</i>微博登录</a><a href='"+options.qq+"'><i class='icon-banner-qq'>QQ登录</i>QQ登录</a><a href='"+options.taobao+"'><i class='icon-banner-taobao'>淘宝登录</i>淘宝登录</a></div></div></div>";
	$(bannerHtml).appendTo($('body'));
	$('.btn-banner-colse').click(function(){
		$('.b5m-banner').remove();
	})
}