define(function(require,exports,module) {

   var $ = require('$');
    require('ui/jscrollpane/2.0.19/jquery.jscrollpane.css');
    require('ui/jscrollpane/2.0.19/jquery.jscrollpane.min');
    require('ui/jscrollpane/2.0.19/jquery.mousewheel');

    function jScrollPane(options) {

        var settings = $.extend({
          container:null
        },options||{});

        //这里重新定义$()是因为这里指向的是seajs的jquery.js，$.fn.jScrollPane指向seajs的jquery
        var container = $(settings.container).jScrollPane();
        var api = container.data('jsp');

        //dom update
        this.update = function() {
            setTimeout(function() {
                api.reinitialise();
            },0);
        };

    }
    module.exports = jScrollPane;

});