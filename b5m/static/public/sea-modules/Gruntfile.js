module.exports = function(grunt){
	var transport = require('grunt-cmd-transport');
	var style = transport.style.init(grunt);
	var text = transport.text.init(grunt);
	var script = transport.script.init(grunt);
     grunt.initConfig({
				pkg : grunt.file.readJSON('package.json'),
				transport : {
					core : {
						options : {
							parsers : {
								'.html' : [ text.html2jsParser ],
								'.css' : [ style.css2jsParser ],
								'.js' : [ script.jsParser ]
							},
							idleading : 'modules/'
						},
						files : [ {
							expand : true,
							cwd : 'modules/',
							src : '**/*.{js,css,html}',
							dest : 'dist/modules/'
						} ]
					}
				},
				uglify : {
					options : {
						banner : '<%= pkg.b5mBannerStart %><%=pkg.description%>\n开发人员：<%=pkg.author%>\n<%= grunt.template.today("yyyy-mm-dd") %>\n<%=pkg.b5mBannerEnd%>\n'
					},
					modules : {
						files : [ {
							expand : true,
							cwd : 'dist/',
							src : [ 'modules/**/*.js',
									'!modules/**/*-debug.js',
									'!modules/**/*.html.js',
									'!modules/**/*-debug.html.js' ],
							dest : 'dist/',
							ext : '.js'

						} ]
					},
					libs : {
						files : {
							'dist/libs.js' : [ 'seajs/seajs/2.1.0/sea.js',
							                   'seajs/seajs-style/1.0.0/seajs-style.js',
							                   'config-debug.js' ]
						}
					},
					other : {
						files : {
						/*
						 * 'jquery/jquery/1.6.4/jquery.js' :
						 * ['jquery/jquery/1.6.4/jquery-debug.js'],
						 * 'jquery/jquery/1.7.2/jquery.js' :
						 * ['jquery/jquery/1.7.2/jquery-debug.js'],
						 * 'jquery/boxy/0.1.4/boxy.js':['jquery/boxy/0.1.4/boxy-debug.js'],
						 * 'jquery/ui/1.10.3/js/jqueryui.js':['jquery/ui/1.10.3/js/jqueryui-debug.js'],
						 * 'jquery/validate/1.11.1/validate.js':['jquery/validate/1.11.1/validate-debug.js'],
						 * 'modules/default/common/avatar/swf/swfobject.js':['modules/default/common/avatar/swf/swfobject-debug.js'],
						 * 'jquery/ZeroClipboard/1.2.3/ZeroClipboard.js' :
						 * ['jquery/ZeroClipboard/1.2.3/ZeroClipboard-debug.js']
						 */
						}
					}
				},
				cssmin : {
					options : {
						banner : '<%= pkg.b5mBannerStart %><%=pkg.description%>\n开发人员：<%=pkg.author%>\n<%= grunt.template.today("yyyy-mm-dd") %>\n<%=pkg.b5mBannerEnd%>\n'
					},
					modules : {
						files : [ {
							expand : true,
							cwd : 'dist/',
							src : [ 'modules/**/*.css',
									'!modules/**/*-debug.css' ],
							dest : 'dist/',
							ext : '.css'
						} ]
					}
				},
				clean : {
					build : [ 'dist/modules', 'dist/libs.js' ]
				},
				copy : {
					nojsCopy : {
						expand : true,
						cwd : 'modules/',
						src : [ '**', '!**/*.js' ],
						dest : 'dist/modules/'
					}
				}
     });
    
     grunt.loadNpmTasks('grunt-cmd-transport');
     grunt.loadNpmTasks('grunt-cmd-concat');
     grunt.loadNpmTasks('grunt-contrib-uglify');
     grunt.loadNpmTasks('grunt-contrib-clean');
     grunt.loadNpmTasks('grunt-contrib-cssmin');
     grunt.loadNpmTasks('grunt-contrib-copy');
    
     grunt.registerTask('build',['clean','transport','uglify','cssmin','copy']);
};
