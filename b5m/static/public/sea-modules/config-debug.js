(function(){
	var libsDom = document.getElementById("uc-libs"), rootPath = 'http://staticcdn.b5m.com/static/public/sea-modules', ucVersion = libsDom.attributes["ucVersion"].value;
	window.B5M_JS = {
			debug:false,
			rootPath : rootPath,
			jsPath : rootPath+'/dist',
			b5mJsPath: 'http://staticcdn.b5m.com/static/public/sea-modules/m/',
			version : ucVersion
	};
	B5M_JS.seajsMap = [
	                   [ /^(.*\.(?:css|js))(.*)$/i, '$1?'+B5M_JS.version ],
	                   [ /^(.*\.(?:html))(.*)$/i, '$1.js?'+B5M_JS.version ]
	                   ];
	if(B5M_JS.debug){
		B5M_JS.jsPath = B5M_JS.rootPath+'/uc-js';
		B5M_JS.seajsMap = B5M_JS.seajsMap[0];
	}
	seajs.config({
		base:B5M_JS.jsPath,
		debug:B5M_JS.debug,
		alias: {
			"$": "jquery/jquery/1.9.1/jquery",
			"common": "modules/uc/default/common/common",
			"underscore": "gallery/underscore/1.4.4/underscore",
			"backbone": "gallery/backbone/1.0.0/backbone",
			
			"m/ui/tabswitch/1.0.0/tabswitch":B5M_JS.b5mJsPath+"ui/tabswitch/1.0.0/tabswitch.js",
			"share/1.0.0/share":B5M_JS.b5mJsPath+"share/1.0.0/share.js",
			"m/share/1.0.0/img/share.css":B5M_JS.b5mJsPath+"share/1.0.0/img/share.css"
		},
		map: B5M_JS.seajsMap,
		preload: ["$"]
	});
	if(B5M_JS.debug)
		seajs.use('seajs/seajs-text/1.0.1/seajs-text');
	seajs.use(['$'],function($){
		window.jQuery = window.$ = $;
	});
})(window);