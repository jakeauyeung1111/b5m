define("modules/2.0.0/user/trade/common/models/OrderCodeCollection-debug", [ "$-debug", "underscore-debug", "backbone-debug", "common-debug" ], function(require, exports, module) {
    this._ = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    this.COMMON = require("common-debug");
    var OrderCodes = [ {
        code: 11,
        name: "支付充值"
    }, {
        code: 12,
        name: "活动奖励"
    }, {
        code: 13,
        name: "任务奖励"
    }, {
        code: 14,
        name: "支付+帮豆充值"
    }, {
        code: 15,
        name: "退款"
    }, {
        code: 21,
        name: "消费"
    }, {
        code: 31,
        name: "兑换"
    }, {
        code: 41,
        name: "转账"
    } ];
    var OrderCodeCollection = {
        Models: {},
        Collections: {},
        Urls: {}
    };
    OrderCodeCollection.Models.OrderCode = Backbone.Model.extend({});
    OrderCodeCollection.Collections.OrderCodeCollection = Backbone.Collection.extend({
        model: OrderCodeCollection.Models.OrderCode,
        initialize: function() {
            this.add(OrderCodes);
        }
    });
    module.exports = OrderCodeCollection.Collections.OrderCodeCollection;
});