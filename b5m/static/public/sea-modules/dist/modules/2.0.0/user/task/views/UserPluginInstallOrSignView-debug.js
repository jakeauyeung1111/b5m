define("modules/2.0.0/user/task/views/UserPluginInstallOrSignView-debug", [ "$-debug", "underscore-debug", "modules/default/common/ModalDialogView-debug", "common-debug", "modules/2.0.0/user/task/tpl/user-plugin-install-or-sign-debug.html" ], function(require, exports, module) {
    this._ = require("underscore-debug");
    this.COMMON = require("common-debug");
    var ModalDialogView = require("modules/default/common/ModalDialogView-debug");
    var userPluginInstallOrSignHtml = require("modules/2.0.0/user/task/tpl/user-plugin-install-or-sign-debug.html");
    var UserPluginInstallOrSignView = ModalDialogView.extend({
        initialize: function(opt) {
            this.data = {
                flag: opt.hasInstallPlugin
            };
        },
        events: {
            "click #pluginSign": "pluginSign",
            "click .close": "close"
        },
        render: function() {
            this.template = _.template(userPluginInstallOrSignHtml);
            this.$el.html(this.template(this.data));
            this.showModal();
        },
        pluginInstall: function(e) {
            e.preventDefault();
        },
        pluginSign: function(e) {
            e.preventDefault();
            this.data.isInstall = this.data.flag ? "1" : "0";
            var self_this = this;
            $.post(B5M_UC.rootPath + "/user/task/data/plug.htm", this.data, function(response) {
                var res = eval(response);
                if (res.ok) {
                    window.location.reload();
                } else {
                    self_this.showTip(res.data);
                }
            });
        },
        close: function() {
            this.hideModal();
        }
    });
    module.exports = UserPluginInstallOrSignView;
});