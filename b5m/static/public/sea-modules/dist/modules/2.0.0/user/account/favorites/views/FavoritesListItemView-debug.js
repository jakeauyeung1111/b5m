define("modules/2.0.0/user/account/favorites/views/FavoritesListItemView-debug", [ "$-debug", "underscore-debug", "backbone-debug", "common-debug", "modules/2.0.0/user/account/favorites/tpl/favorites-list-item-debug.html", "arale/cookie/1.0.2/cookie-debug" ], function(require, exports, module) {
    this._ = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    this.COMMON = require("common-debug");
    this.tpl = require("modules/2.0.0/user/account/favorites/tpl/favorites-list-item-debug.html");
    this.Cookie = require("arale/cookie/1.0.2/cookie-debug");
    this.delUrl = B5M_UC.rootPath + "/gc/user/favorites/data/delete.htm?jsonpCallback=?";
    //	this.delUrl = 'http://ucenter.stage.bang5mai.com/gc/user/favorites/data/delete.htm?jsonpCallback=?';
    var FavoritesListItemView = {
        Views: {}
    };
    FavoritesListItemView.Views.FavoritesListItemView = Backbone.View.extend({
        template: _.template(tpl),
        initialize: function(opt) {
            this.FavoritesListView = opt.FavoritesListView;
            this.$el.html(this.template(this.model.toJSON()));
            this.render();
        },
        events: {
            "click .del-favorites-but": "delFavoritesById"
        },
        delFavoritesById: function(e) {
            var $currentDom = $(e.currentTarget);
            var self = this, $msg = $('<div class="dialog__confirm"><p class="dialog_fav"><strong>请确认是否删除收藏的宝贝</strong></p><div class="dialog__confirm__btn"><a href="javascript:void(0);" class="btn btn-3">确认</a> <a href="javascript:void(0);" class="btn btn-4">取消</a></div></div>');
            $msg.find(".btn-3").click(function() {
                $.getJSON(delUrl, {
                    userId: Cookie.get("token"),
                    ugcId: $currentDom.attr("attr-id")
                }, function(result) {
                    COMMON.checkJSonData(result);
                    if (!result.ok) {
                        COMMON.showTip(result.message);
                        return;
                    }
                    var _pageNum = self.FavoritesListView.page.settings.pageNum;
                    if (self.FavoritesListView.favoritesCollection.length == 1 && _pageNum > 1) _pageNum = _pageNum - 1;
                    self.FavoritesListView.pageHandler({
                        pageNum: _pageNum
                    });
                });
                COMMON.hideDialog();
            });
            $msg.find(".btn-4").click(function() {
                COMMON.hideDialog();
            });
            COMMON.showSimpleDialog($msg, "我的收藏");
        }
    });
    module.exports = FavoritesListItemView.Views.FavoritesListItemView;
});