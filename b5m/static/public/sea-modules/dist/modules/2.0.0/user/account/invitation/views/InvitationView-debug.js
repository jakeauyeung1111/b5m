define("modules/2.0.0/user/account/invitation/views/InvitationView-debug", [ "$-debug", "common-debug", "underscore-debug", "backbone-debug", "modules/2.0.0/user/account/invitation/models/InvitationDetailModel-debug", "modules/2.0.0/user/account/invitation/tpl/invitation-debug.html", "jquery/ZeroClipboard/1.2.3/ZeroClipboard-debug", "modules/default/common/PopwindowShareView-debug" ], function(require, exports, module) {
    this.COMMON = require("common-debug");
    this._ = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    this.InvitationDetailModel = require("modules/2.0.0/user/account/invitation/models/InvitationDetailModel-debug");
    this.ZeroClipboard = require("jquery/ZeroClipboard/1.2.3/ZeroClipboard-debug");
    this.tpl_invitation = require("modules/2.0.0/user/account/invitation/tpl/invitation-debug.html");
    var popWindowShareFed = require("modules/default/common/PopwindowShareView-debug");
    var InvitationView = Backbone.View.extend({
        template: _.template(tpl_invitation),
        tagName: "div",
        initialize: function(opt) {
            //初始化参数
            this.parameterData = this.initParameter(opt);
            this.invitationDetailModel = new InvitationDetailModel();
            var self = this;
            this.invitationDetailModel.fetch({
                success: function() {
                    self.parameterData = _.extend(self.parameterData, self.invitationDetailModel.toJSON());
                    self.render();
                }
            });
        },
        initParameter: function(para) {
            return _.extend({
                title: "邀请好友来帮5买",
                summary: "有个网购省钱利器我必须告诉你，搜一下商品就能省很多Money！还有更多优惠方式，还不立刻行动？"
            }, para);
        },
        render: function() {
            this.$el.find(".ui-loading").hide();
            try {
                this.$el.append(this.template(this.parameterData));
            } catch (e) {
                COMMON.showSimpleTip("出错啦！！！赶紧去买六合彩吧！！！");
            }
            var clip = new ZeroClipboard(this.$el.find("#copy-but")[0], {
                moviePath: B5M_UC.jsPath + "/jquery/ZeroClipboard/1.2.3/ZeroClipboard.swf"
            });
            // 复制内容到剪贴板成功后的操作
            clip.on("complete", function(client, args) {
                COMMON.showSimpleTip("复制成功!!!");
            });
        },
        events: {
            "click #qqzone": "init",
            "click #sinamb": "init",
            "click #qqmb": "init"
        },
        init: function(state) {
            var type = $(state.currentTarget).attr("attr-type");
            var sync = $(state.currentTarget).attr("attr-sync");
            $.ajax({
                url: B5M_UC.rootPath + "/user/third/getToken.htm",
                type: "POST",
                async: false,
                data: {
                    state: type
                },
                success: function(result) {
                    if (result && result.ok == true) {
                        popWindowShareFed.popwindow({
                            title: "帮5买",
                            content: $("textarea").val(),
                            href: $("textarea").val().substr($("textarea").val().indexOf("http")),
                            type: type
                        }, function(res) {
                            var url = B5M_UC.rootPath + "/user/third/share.htm";
                            var data = {
                                state: type,
                                summary: res["content"],
                                title: res["title"]
                            };
                            if (type == 1) {
                                data.url = res["href"];
                                if (sync) {
                                    data.nswb = sync;
                                }
                            } else {
                                data.summary = data.summary + "  " + res["href"];
                            }
                            if (res["pic"]) {
                                data.images = res["pic"];
                            }
                            $.ajax({
                                type: "POST",
                                url: url,
                                data: data,
                                success: function(httpObj) {
                                    if (httpObj.ok) {
                                        popWindowShareFed.popwindowClose();
                                        COMMON.showSimpleTip("分享成功");
                                    } else {
                                        COMMON.showSimpleTip("分享失败，请稍后分享");
                                    }
                                }
                            });
                        });
                    } else if (result.ok == false) {
                        var newWindow = window.open();
                        setTimeout(function() {
                            newWindow.location = B5M_UC.rootPath + "/user/third/login/auth.htm?type=" + type + "&refererUrl=&api=/setToken";
                        }, 10);
                    }
                }
            });
        }
    });
    module.exports = InvitationView;
});