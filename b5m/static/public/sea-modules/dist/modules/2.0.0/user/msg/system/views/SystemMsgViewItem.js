/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/2.0.0/user/msg/system/views/SystemMsgViewItem",["$","underscore","backbone","common","modules/2.0.0/user/msg/system/tpl/system-msg-item.html"],function(e,t,i){this._=e("underscore"),this.Backbone=e("backbone"),this.COMMON=e("common"),this.tpl_system_msg_item=e("modules/2.0.0/user/msg/system/tpl/system-msg-item.html");var a={Views:{}};a.Views.SystemMsgViewItem=Backbone.View.extend({tagName:"li",template:_.template(tpl_system_msg_item),initialize:function(e){this.SystemMsgView=e.SystemMsgView,this.$el.html(this.template(this.model.toJSON())),1==this.model.get("status")&&this.$el.find(".messageBody").attr("class","messageBody message-read")},events:{"click .del":"delHandler","click .msgDetailBut":"msgDetailHandler"},delHandler:function(){var e=$('<div class="dialog__confirm"><p><strong>请确认是否删除该消息？</strong></p><div class="dialog__confirm__btn"><a id="delBut" href="javascript:void(0);" class="btn btn-3">确认</a> <a id="cancelDelBut" href="javascript:void(0);" class="btn btn-4">取消</a> </div> </div>');e.find("#cancelDelBut").click(function(){COMMON.hideDialog()});var t=this;e.find("#delBut").click(function(){t.updateReadStateOrDelMsg(2,function(e){if(!e.ok)return COMMON.showTip(e.data),void 0;var i=t.SystemMsgView.page.settings.pageNum;1==t.SystemMsgView.systemMsgCollection.length&&i>1&&(i-=1),t.SystemMsgView.pageHandler({pageNum:i})}),COMMON.hideDialog()}),COMMON.showSimpleDialog(e,"删除消息")},msgDetailHandler:function(e){var t=$(e.currentTarget).closest("dl").find(".message-detail");"true"==t.attr("isShow")?t.attr("isShow",!1).hide():t.attr("isShow",!0).show();var i=this;0==this.model.get("status")&&this.updateReadStateOrDelMsg(1,function(e){if(e.ok){i.model.set("status",1),i.$el.find(".messageBody").attr("class","messageBody message-read");var t=$(".new-msg-count").find("em"),a=parseInt(t.html())-1;0>=a?$(".new-msg-count").empty():t.html(a)}})},updateReadStateOrDelMsg:function(e,t){$.getJSON(B5M_UC.rootPath+"/user/message/data/update.htm?jsonpCallback=?",{messageId:this.model.get("id"),status:e},function(e){COMMON.checkJSonData(e),t&&"function"==(typeof t).toLowerCase()&&t(e)})}}),i.exports=a.Views.SystemMsgViewItem});