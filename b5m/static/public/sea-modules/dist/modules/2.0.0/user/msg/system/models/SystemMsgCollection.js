/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/2.0.0/user/msg/system/models/SystemMsgCollection",["$","underscore","backbone","common"],function(e,t,i){this._=e("underscore"),this.Backbone=e("backbone"),this.COMMON=e("common");var a={Models:{},Collections:{},Urls:{}};a.Urls.systemMsgList=B5M_UC.rootPath+"/user/message/data/list.htm",a.Models.Msg=Backbone.Model.extend({}),a.Collections.SystemMsgCollection=Backbone.Collection.extend({url:a.Urls.systemMsgList,model:a.Models.Msg,sync:function(e,t,i){var a=_.extend({async:!1,cache:!1,dataType:"jsonp",jsonp:"jsonpCallback",type:"POST",url:t.url,processData:!0},i);return $.ajax(a)},parse:function(e){return COMMON.checkJSonData(e),e.ok?this.initModelData(e.data):(COMMON.showTip(e.data),null)},initModelData:function(e){return this.dataTotal=e.totalRecord,e.records}}),i.exports=a.Collections.SystemMsgCollection});