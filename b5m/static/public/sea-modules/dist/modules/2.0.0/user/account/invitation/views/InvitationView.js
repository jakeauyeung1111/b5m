/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/2.0.0/user/account/invitation/views/InvitationView",["$","common","underscore","backbone","modules/2.0.0/user/account/invitation/models/InvitationDetailModel","modules/2.0.0/user/account/invitation/tpl/invitation.html","jquery/ZeroClipboard/1.2.3/ZeroClipboard","modules/default/common/PopwindowShareView"],function(e,t,i){this.COMMON=e("common"),this._=e("underscore"),this.Backbone=e("backbone"),this.InvitationDetailModel=e("modules/2.0.0/user/account/invitation/models/InvitationDetailModel"),this.ZeroClipboard=e("jquery/ZeroClipboard/1.2.3/ZeroClipboard"),this.tpl_invitation=e("modules/2.0.0/user/account/invitation/tpl/invitation.html");var o=e("modules/default/common/PopwindowShareView"),a=Backbone.View.extend({template:_.template(tpl_invitation),tagName:"div",initialize:function(e){this.parameterData=this.initParameter(e),this.invitationDetailModel=new InvitationDetailModel;var t=this;this.invitationDetailModel.fetch({success:function(){t.parameterData=_.extend(t.parameterData,t.invitationDetailModel.toJSON()),t.render()}})},initParameter:function(e){return _.extend({title:"邀请好友来帮5买",summary:"有个网购省钱利器我必须告诉你，搜一下商品就能省很多Money！还有更多优惠方式，还不立刻行动？"},e)},render:function(){this.$el.find(".ui-loading").hide();try{this.$el.append(this.template(this.parameterData))}catch(e){COMMON.showSimpleTip("出错啦！！！赶紧去买六合彩吧！！！")}var t=new ZeroClipboard(this.$el.find("#copy-but")[0],{moviePath:B5M_UC.jsPath+"/jquery/ZeroClipboard/1.2.3/ZeroClipboard.swf"});t.on("complete",function(){COMMON.showSimpleTip("复制成功!!!")})},events:{"click #qqzone":"init","click #sinamb":"init","click #qqmb":"init"},init:function(e){var t=$(e.currentTarget).attr("attr-type"),i=$(e.currentTarget).attr("attr-sync");$.ajax({url:B5M_UC.rootPath+"/user/third/getToken.htm",type:"POST",async:!1,data:{state:t},success:function(e){if(e&&1==e.ok)o.popwindow({title:"帮5买",content:$("textarea").val(),href:$("textarea").val().substr($("textarea").val().indexOf("http")),type:t},function(e){var a=B5M_UC.rootPath+"/user/third/share.htm",n={state:t,summary:e.content,title:e.title};1==t?(n.url=e.href,i&&(n.nswb=i)):n.summary=n.summary+"  "+e.href,e.pic&&(n.images=e.pic),$.ajax({type:"POST",url:a,data:n,success:function(e){e.ok?(o.popwindowClose(),COMMON.showSimpleTip("分享成功")):COMMON.showSimpleTip("分享失败，请稍后分享")}})});else if(0==e.ok){var a=window.open();setTimeout(function(){a.location=B5M_UC.rootPath+"/user/third/login/auth.htm?type="+t+"&refererUrl=&api=/setToken"},10)}}})}});i.exports=a});