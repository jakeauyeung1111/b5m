/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/2.0.0/user/task/views/UserTaskView",["$","underscore","backbone","common","modules/2.0.0/user/task/models/UserTaskCollection","modules/2.0.0/user/task/views/UserTaskItemView"],function(e,t,i){this._=e("underscore"),this.Backbone=e("backbone"),this.COMMON=e("common"),this.UserTaskCollection=e("modules/2.0.0/user/task/models/UserTaskCollection"),this.UserTaskItemView=e("modules/2.0.0/user/task/views/UserTaskItemView");var s="modules/2.0.0/user/task/tpl/task-jqqd.html",o={Views:{},Urls:{}};o.Views.UserTaskView=Backbone.View.extend({initialize:function(e){this.el=e.el,this.taskItemTpl=e.taskItemTpl,this.showSize=e.showSize,this.dataNotIn=e.dataNotIn,this.userTaskCollection=new UserTaskCollection.UserTaskCollection,this.render()},render:function(){this.$el.find(".ui-loading").hide();var t=0;if(this.userTaskCollection.each(function(e){if(!(void 0!=this.showSize&&t>=this.showSize)){if(void 0!=this.dataNotIn)for(var i in this.dataNotIn)if(this.dataNotIn[i]==e.get(i))return;this.renderTask(e,t),t++}},this),!this.dataNotIn){var i=this;e.async(s,function(e){i.$el.find("tbody").append(e)})}0==this.userTaskCollection.length&&this.$el.find(".ui-empty-data").show()},renderTask:function(e,t){var i="even";0!=t%2&&(i="odd");var s=new UserTaskItemView.UserTaskItemView({model:e,taskItemTpl:this.taskItemTpl,attributes:{"class":i}});this.$el.find("tbody").append(s.el)}}),i.exports.UserTaskView=o.Views.UserTaskView});