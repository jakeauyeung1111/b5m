/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/2.0.0/user/account/security/index/views/UserSimpleInfoView",["$","common","underscore","backbone","modules/2.0.0/user/account/security/index/tpl/user-info-simple.html","modules/2.0.0/user/account/security/index/tpl/security-item.html","modules/2.0.0/user/user/models/UserModel"],function(e,t,i){this.COMMON=e("common"),this.underscore=e("underscore"),this.Backbone=e("backbone"),this.UserCenterCollection=e("modules/2.0.0/user/user/models/UserModel");var a=Backbone.View.extend({initialize:function(e){this.el=e.el,this.type=e.type,this.data=new UserCenterCollection.UserModel({type:1}).toJSON(),this.buildUserSecurity(this.data),this.render()},render:function(){var t="modules/2.0.0/user/account/security/index/tpl/user-info-simple.html";"items"==this.type&&(t="modules/2.0.0/user/account/security/index/tpl/security-item.html"),this.template=underscore.template(e(t)),this.$el.html(this.template(this.data))},buildUserSecurity:function(e){var t=30,i="差";COMMON.isEmptyValue(e.avatar)&&(e.avatar="http://staticcdn.b5m.com/static/images/center/default-avatar-medium.png"),"REG"==e.activation&&(t+=35),1==e.isMobileBind&&(t+=35),t>35&&65>=t&&(i="中"),t>65&&(i="健康"),e.lastLoginTime=COMMON.isEmptyValue(e.lastLoginTime)?COMMON.formatTime(new Date,"yyyy-MM-dd"):COMMON.formatTime(e.lastLoginTime,"yyyy-MM-dd"),e.secureScore=t,e.secureLabel=i}});i.exports=a});