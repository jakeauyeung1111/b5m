/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/2.0.0/user/task/views/UserPluginInstallOrSignView",["$","underscore","modules/default/common/ModalDialogView","common","modules/2.0.0/user/task/tpl/user-plugin-install-or-sign.html"],function(require,exports,module){this._=require("underscore"),this.COMMON=require("common");var ModalDialogView=require("modules/default/common/ModalDialogView"),userPluginInstallOrSignHtml=require("modules/2.0.0/user/task/tpl/user-plugin-install-or-sign.html"),UserPluginInstallOrSignView=ModalDialogView.extend({initialize:function(e){this.data={flag:e.hasInstallPlugin}},events:{"click #pluginSign":"pluginSign","click .close":"close"},render:function(){this.template=_.template(userPluginInstallOrSignHtml),this.$el.html(this.template(this.data)),this.showModal()},pluginInstall:function(e){e.preventDefault()},pluginSign:function(e){e.preventDefault(),this.data.isInstall=this.data.flag?"1":"0";var self_this=this;$.post(B5M_UC.rootPath+"/user/task/data/plug.htm",this.data,function(response){var res=eval(response);res.ok?window.location.reload():self_this.showTip(res.data)})},close:function(){this.hideModal()}});module.exports=UserPluginInstallOrSignView});