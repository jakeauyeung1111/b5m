/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/2.0.0/user/trade/common/models/ExchangeDetailModel",["$","underscore","backbone","common"],function(e,t,i){this._=e("underscore"),this.Backbone=e("backbone"),this.COMMON=e("common");var o={Models:{},Collections:{},Urls:{}},s=window.location.href.indexOf("ucenter.b5m.com"),a=window.location.href.indexOf("ucenter.prod.bang5mai.com");o.Urls.exchange_detail=s>0&&10>s||a>0&&10>a?"http://sa1.b5m.com:58080/B5MCMS/exchangeApiController/couponInfo.do":"http://cms.stage.bang5mai.com/B5MCMS/exchangeApiController/couponInfo.do",o.Models.Trade=Backbone.Model.extend({}),o.Collections.ExchangeDetailModel=Backbone.Collection.extend({url:o.Urls.exchange_detail,sync:function(e,t,i){var o=_.extend({async:!1,cache:!1,dataType:"jsonp",jsonp:"jsonpCallback",type:"POST",url:t.url,processData:!0},i);return $.ajax(o)},parse:function(e){return COMMON.checkJSonData(e),e.ok?this.initModelData(e.data):(COMMON.showTip(void 0==e.data||null==e.data||"null"==e.data?e.msg:e.data),null)},initModelData:function(e){return e.couponPnInfo&&(e.couponPnInfo=$.parseJSON(e.couponPnInfo)),e}}),i.exports=o.Collections.ExchangeDetailModel});