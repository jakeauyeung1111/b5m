/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/2.0.0/user/trade/common/views/AccountHistoryItemView",["$","underscore","backbone","common"],function(e,t,i){this._=e("underscore"),this.Backbone=e("backbone"),this.COMMON=e("common");var o="modules/2.0.0/user/trade/common/tpl/account-history-item.html",s="modules/2.0.0/user/trade/common/tpl/account-history-exchange-item.html",a={Views:{}};a.Views.AccountHistoryItemView=Backbone.View.extend({tagName:"tr",initialize:function(t){var i=o,a=this;"default"==t.tplName&&(i=o),"exchange"==t.tplName&&(i=s),e.async(i,function(e){a.template=_.template(e),a.$el.html(a.template(a.model.toJSON())),a.render()})}}),i.exports=a.Views.AccountHistoryItemView});