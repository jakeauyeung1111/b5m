define("modules/2.0.0/user/props/views/NotUsedPropView-debug", [ "$-debug", "underscore-debug", "backbone-debug", "modules/2.0.0/user/props/tpl/prop-item-debug.html", "modules/2.0.0/user/props/views/UserPropsUseDialogView-debug" ], function(require, exports, module) {
    this.underscore = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    this.UserPropsUseDialogView = require("modules/2.0.0/user/props/views/UserPropsUseDialogView-debug");
    var notUsedPropView = Backbone.View.extend({
        tagName: "li",
        initialize: function() {
            this.render();
        },
        render: function() {
            //加载模版
            if (this.template === undefined) this.template = underscore.template(require("modules/2.0.0/user/props/tpl/prop-item-debug.html"));
            this.$el.html(this.template(this.model));
            return this;
        },
        events: {
            "click .useProp": "useProp"
        },
        useProp: function() {
            new UserPropsUseDialogView({
                beanCount: this.model.beanCount
            }).render();
        }
    });
    module.exports = notUsedPropView;
});