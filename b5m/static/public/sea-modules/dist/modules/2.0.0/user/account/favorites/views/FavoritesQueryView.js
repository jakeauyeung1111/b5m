/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/2.0.0/user/account/favorites/views/FavoritesQueryView",["$","underscore","backbone","common","modules/2.0.0/user/account/favorites/views/WebsiteView","modules/2.0.0/user/account/favorites/tpl/favorites-query.html","modules/2.0.0/user/account/favorites/views/FavoritesListView"],function(e,t,i){this._=e("underscore"),this.Backbone=e("backbone"),this.COMMON=e("common"),this.tpl_favorites_query=e("modules/2.0.0/user/account/favorites/tpl/favorites-query.html"),this.WebsiteView=e("modules/2.0.0/user/account/favorites/views/WebsiteView"),this.FavoritesListView=e("modules/2.0.0/user/account/favorites/views/FavoritesListView");var o={Views:{},Urls:{}};o.Views.FavoritesQueryView=Backbone.View.extend({template:_.template(tpl_favorites_query),initialize:function(e){this.websiteView=new WebsiteView,this.el=e.el,this.render()},render:function(){return this.$el.html(this.template()),this.$el.find("#website-list").append(this.websiteView.$el.html()),this.renderFavoritesListView({type:0,priceType:0}),this},events:{"mouseenter .layout-tab-tit > span":"tabFocusin","click .webSite-But":"webSiteClick"},tabFocusin:function(e){this.$el.find(".webSite-But").each(function(e){0==e?$(this).addClass("current"):$(this).removeClass("current")});var t=$(e.currentTarget),i=t.attr("attr-type");0==i?this.renderFavoritesListView({type:i,priceType:0}):1==i&&this.renderFavoritesListView({type:i,priceType:1})},webSiteClick:function(e){var t=$(e.currentTarget),i=t.attr("attr-code"),o=this.$el.find(".tabCur").attr("attr-type"),a=void 0;1==o&&(a=1),this.renderFavoritesListView({type:o,priceType:a,webSite:i})},renderFavoritesListView:function(e){new FavoritesListView({$el:this.$el.find(".layout-tab-content:eq("+e.type+")").empty(),priceType:e.priceType,webSite:e.webSite})}}),i.exports=o.Views.FavoritesQueryView});