/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/2.0.0/user/props/views/UserPropsUseDialogView",["$","underscore","modules/default/common/ModalDialogView","common","modules/2.0.0/user/props/tpl/user-prop-dialog.html"],function(require,exports,module){this._=require("underscore"),this.COMMON=require("common");var ModalDialogView=require("modules/default/common/ModalDialogView"),UserPropsUseDialogView=ModalDialogView.extend({initialize:function(e){this.beanCount=e.beanCount},events:{"click #useMyProp":"useMyProp","click .close":"close"},render:function(){var e=this;require.async(["modules/2.0.0/user/props/tpl/user-prop-dialog.html"],function(t){e.showMyPropsDialog(t)})},showMyPropsDialog:function(e){this.template=_.template(e),this.$el.html(this.template({beanCount:this.beanCount})),this.showModal()},useMyProp:function(e){e.preventDefault();var self_this=this;$.post(B5M_UC.rootPath+"/props/info/data/useMyProp.htm",this.data,function(response){var res=eval(response);res.ok?window.location.reload():self_this.showTip(res.data)})},close:function(){this.hideModal()}});module.exports=UserPropsUseDialogView});