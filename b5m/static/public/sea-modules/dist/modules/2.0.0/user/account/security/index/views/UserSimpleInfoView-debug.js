define("modules/2.0.0/user/account/security/index/views/UserSimpleInfoView-debug", [ "$-debug", "common-debug", "underscore-debug", "backbone-debug", "modules/2.0.0/user/account/security/index/tpl/user-info-simple-debug.html", "modules/2.0.0/user/account/security/index/tpl/security-item-debug.html", "modules/2.0.0/user/user/models/UserModel-debug" ], function(require, exports, module) {
    this.COMMON = require("common-debug");
    this.underscore = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    this.UserCenterCollection = require("modules/2.0.0/user/user/models/UserModel-debug");
    var UserSimpleInfoView = Backbone.View.extend({
        initialize: function(opt) {
            this.el = opt.el;
            this.type = opt.type;
            this.data = new UserCenterCollection.UserModel({
                type: 1
            }).toJSON();
            this.buildUserSecurity(this.data);
            this.render();
        },
        render: function() {
            var htmlUrl = "modules/2.0.0/user/account/security/index/tpl/user-info-simple.html";
            if (this.type == "items") htmlUrl = "modules/2.0.0/user/account/security/index/tpl/security-item.html";
            this.template = underscore.template(require(htmlUrl));
            this.$el.html(this.template(this.data));
        },
        buildUserSecurity: function(data) {
            var secureScore = 30;
            var secureLabel = "差";
            if (COMMON.isEmptyValue(data.avatar)) data.avatar = "http://staticcdn.b5m.com/static/images/center/default-avatar-medium.png";
            if (data.activation == "REG") secureScore = secureScore + 35;
            if (data.isMobileBind == 1) {
                secureScore = secureScore + 35;
            }
            if (secureScore > 35 && secureScore <= 65) secureLabel = "中";
            if (secureScore > 65) secureLabel = "健康";
            //格式化最后登录时间
            if (COMMON.isEmptyValue(data.lastLoginTime)) {
                data.lastLoginTime = COMMON.formatTime(new Date(), "yyyy-MM-dd");
            } else {
                data.lastLoginTime = COMMON.formatTime(data.lastLoginTime, "yyyy-MM-dd");
            }
            data.secureScore = secureScore;
            data.secureLabel = secureLabel;
        }
    });
    module.exports = UserSimpleInfoView;
});