/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/2.0.0/user/account/security/password/views/EditPasswordView",["$","common","underscore","backbone","modules/2.0.0/user/account/security/password/tpl/edit-password.html","jquery/validate/1.11.1/validate","modules/default/common/TestPassword"],function(e,t,i){this.COMMON=e("common"),this.underscore=e("underscore"),this.Backbone=e("backbone"),this.TestPassword=e("modules/default/common/TestPassword");var a=Backbone.View.extend({tagName:"form",events:{"click #editPwdSubmit":"editPwdSub"},initialize:function(e){this.el=e.el,this.render(),this.testPassword=new TestPassword,this.initValidateForm()},render:function(){void 0===this.template&&(this.template=underscore.template(e("modules/2.0.0/user/account/security/password/tpl/edit-password.html"))),this.$el.html(this.template)},initValidateForm:function(){var e=$("#editPwdForm"),t=this;jQuery.validator.addMethod("isOldPwd",function(e){return e!=$("#password").val()},"新密码与原密码一致，请重新输入"),jQuery.validator.addMethod("isPasswordOK",function(e){return/^[a-zA-Z_0-9]{6,15}$/.test(e)},"请输入正确格式的字母，数字,下划线"),e.validate({rules:{password:{required:!0,rangelength:[6,15],isPasswordOK:!0},newPassword:{required:!0,rangelength:[6,15],isPasswordOK:!0,isOldPwd:!0},confirmPassword:{required:!0,rangelength:[6,15],isPasswordOK:!0,equalTo:"#newPassword"}},messages:{password:{required:"请输入原密码",rangelength:"密码长度必须介于 {0} 和 {1} 之间"},newPassword:{required:"请输入新密码",rangelength:"密码长度必须介于 {0} 和 {1} 之间"},confirmPassword:{required:"请输入确认密码",rangelength:"密码长度必须介于 {0} 和 {1} 之间",equalTo:"两次输入的密码不一致"}},errorElement:"span",errorClass:"text_error",errorPlacement:function(e,t){var i=t.parent().parent().find(".cl-999").empty();i.append(e.html())},success:function(e,t){$(t).parent().parent().find(".cl-999").empty()},onclick:!1}),this.$editPwdFormG=e,$("#password").bind("blur",function(){var e=$.trim($("#newPassword").val());COMMON.isEmptyValue(e)&&($("#newPassword").addClass("text_error"),$("#newPasswordMsg").html("原密码与新密码一致，请重新输入"))}),$("#newPassword").keyup(function(){$("#test-pwd").empty().append(t.testPassword.getDefaultHtml($(this).val()))})},editPwdSub:function(e){this.$editPwdFormG.valid()&&$.post(B5M_UC.rootPath+"/user/info/data/editPwd.htm",$(e.currentTarget).parents("form:first").serialize(),function(e){function t(){window.location.href=B5M_UC.rootPath+"/forward.htm?method=/user/user/index"}if(COMMON.checkJSonData(e),1==e.ok){COMMON.showSimpleTip("修改密码成功","修改密码");var i=2,a=setInterval(function(){i-=1,0>=i&&(i=2,t(),clearInterval(a))},1e3)}else COMMON.showSimpleTip(e.data,"修改密码"),$("#password").val(""),$("#newPassword").val(""),$("#confirmPassword").val(""),$(".cl-999").empty()})}});i.exports=a});