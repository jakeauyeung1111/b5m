/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/2.0.0/user/trade/bangbi/exchange/views/ExchangeConfirmViews",["$","underscore","backbone","common","modules/2.0.0/user/trade/bangbi/exchange/tpl/exchangeConfirm.html"],function(e,t,i){this.COMMON=e("common"),this.underscore=e("underscore"),this.Backbone=e("backbone");var s=Backbone.View.extend({tagName:"form",events:{"click #exchangeBangbi2bean":"exchangeBangbi2bean","click #btnClose":"btnClose"},initialize:function(){this.render()},render:function(){return this.template=underscore.template(e("modules/2.0.0/user/trade/bangbi/exchange/tpl/exchangeConfirm.html")),this.$el.html(this.template(this.model)),this},exchangeBangbi2bean:function(e){var t=this;$.post(B5M_UC.rootPath+"/exchange/exchange.htm",$(e.currentTarget).parents("form:first").serialize(),function(e){if(COMMON.checkJSonData(e),COMMON.hideDialog(),e.ok){COMMON.showSimpleTip("兑换成功","帮币兑换帮豆");var i=parseFloat($("#restBangbi").text()-t.model.exchangeBangbi);return $("#restBangbi").text(i),null}COMMON.showSimpleTip("兑换失败","帮币兑换帮豆")})},btnClose:function(){COMMON.hideDialog()}});i.exports=s});