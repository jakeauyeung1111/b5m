/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/2.0.0/user/account/security/email/views/ValidateEmailView",["$","common","underscore","backbone","jquery/validate/1.11.1/validate","modules/2.0.0/user/account/security/email/tpl/validate-email.html","modules/2.0.0/user/account/security/email/tpl/validate-email-2.html","modules/2.0.0/user/user/models/UserModel"],function(require,exports,module){this.COMMON=require("common"),this.underscore=require("underscore"),this.Backbone=require("backbone"),this.UserCenterCollection=require("modules/2.0.0/user/user/models/UserModel");var ValidateEmailView=Backbone.View.extend({tagName:"form",events:{"click #sendValidateEmail":"sendValidateEmail","click #reSendEmail":"sendEmail"},initialize:function(e){this.el=e.el,this.data=new UserCenterCollection.UserModel({type:1}).toJSON(),this.render(),this.initValidate(),this.keyPressEvent()},render:function(e){var t=require("modules/2.0.0/user/account/security/email/tpl/validate-email.html");COMMON.isEmptyValue(e)||"checkMailHome"!=e||(t=require("modules/2.0.0/user/account/security/email/tpl/validate-email-2.html"),COMMON.isEmptyValue(this.data.emailDomain)&&this.buildEmailDomain(this.data)),this.template=underscore.template(t),this.$el.html(this.template(this.data))},buildEmailDomain:function(e){var t=e.email;COMMON.isEmptyValue(t)&&(e.emailDomain="");var i=t.indexOf("@"),a=t.substring(i+1);0==a.indexOf("gmail")&&(a="google.com"),e.emailDomain="http://mail."+a},sendValidateEmail:function(e){this.validateForm.valid()&&this.sendEmail(e)},sendEmail:function(e){var t=this;$(e.currentTarget).wait("btn btn-1 waiting","btn btn-1",5e3);var i=$("#email").val();$.post(B5M_UC.rootPath+"/user/info/data/verifyEmail.htm","email="+i,function(e){1==e.code?(t.data.emailDomain=e.data,t.data.email=i,t.render("checkMailHome")):COMMON.showSimpleTip("操作失败")})},initValidate:function(){var $validateEmailForm=$("#validateEmailForm");jQuery.validator.addMethod("isEmail",function(value,element){var code=0;return $.ajax({type:"post",data:"email="+value,async:!1,url:B5M_UC.rootPath+"/user/info/data/isNameOrEmailUse.do",success:function(r){code=eval(r).code}}),0==code?!0:!1},"邮箱已经被占用，请更换邮箱"),$validateEmailForm.validate({rules:{email:{required:!0,email:!0,isEmail:!0}},messages:{email:{required:"Email不能为空",email:"邮箱地址格式不正确"}},errorElement:"span",errorClass:"text_error",errorPlacement:function(e,t){$("#serror").empty().hide(),t.attr("class","text text_error");var i=t.parent().parent().find(".errorBox").empty();i.append('<img class="ui-icon ui-icon-error" src="'+B5M_UC.rootPath+'/images/ucenter/blank.png" />&nbsp;'+e.html())},success:function(e,t){var i=$(t).parent().parent().find(".errorBox").empty();"code"!=$(t).attr("name")&&i.append('<img class="ui-icon ui-icon-right" src="'+B5M_UC.rootPath+'/images/ucenter/blank.png">')},onclick:!1}),this.validateForm=$validateEmailForm},keyPressEvent:function(){$("#email").keydown(function(e){return 13==e.keyCode||13==e.which?!1:!0})}});module.exports=ValidateEmailView});