/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/2.0.0/user/user/models/UserModel",["$","underscore","backbone","common"],function(e,t,i){this._=e("underscore"),this.Backbone=e("backbone"),this.COMMON=e("common");var a={Models:{},Urls:{}};a.Urls.UserInfo=B5M_UC.rootPath+"/user/user/data/info.htm",a.Models.UserModel=Backbone.Model.extend({url:a.Urls.UserInfo,parse:function(e){return COMMON.checkJSonData(e),e.ok?this.initModelData(e.data):(COMMON.showTip(e.data),null)},initialize:function(e){if(!COMMON.isEmptyValue(e)){var t=e.type;COMMON.isEmptyValue(t)||(this.url=a.Urls.UserInfo+"?isSimple="+t)}this.fetch({async:!1,cache:!1})},initModelData:function(e){if(COMMON.isEmptyValue(e.currentResidence))e.cProvince=31,e.cCity=1;else{var t=e.currentResidence.split("-");e.cProvince=t[0],e.cCity=t[1]}if(COMMON.isEmptyValue(e.hometown))e.hProvince=31,e.hCity=1;else{var i=e.hometown.split("-");e.hProvince=i[0],e.hCity=i[1]}if(COMMON.isEmptyValue(e.birthdayStr))e.year=2013,e.month=10,e.day=1,e.birthdayStr="2013-10-1";else{var a=e.birthdayStr,o=a.split("-");e.year=o[0],e.month=Math.abs(o[1]),e.day=Math.abs(o[2]),e.birthdayStr=a}return e}}),i.exports.UserModel=a.Models.UserModel});