/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/2.0.0/user/account/favorites/views/FavoritesListItemView",["$","underscore","backbone","common","modules/2.0.0/user/account/favorites/tpl/favorites-list-item.html","arale/cookie/1.0.2/cookie"],function(e,t,o){this._=e("underscore"),this.Backbone=e("backbone"),this.COMMON=e("common"),this.tpl=e("modules/2.0.0/user/account/favorites/tpl/favorites-list-item.html"),this.Cookie=e("arale/cookie/1.0.2/cookie"),this.delUrl=B5M_UC.rootPath+"/gc/user/favorites/data/delete.htm?jsonpCallback=?";var i={Views:{}};i.Views.FavoritesListItemView=Backbone.View.extend({template:_.template(tpl),initialize:function(e){this.FavoritesListView=e.FavoritesListView,this.$el.html(this.template(this.model.toJSON())),this.render()},events:{"click .del-favorites-but":"delFavoritesById"},delFavoritesById:function(e){var t=$(e.currentTarget),o=this,i=$('<div class="dialog__confirm"><p class="dialog_fav"><strong>请确认是否删除收藏的宝贝</strong></p><div class="dialog__confirm__btn"><a href="javascript:void(0);" class="btn btn-3">确认</a> <a href="javascript:void(0);" class="btn btn-4">取消</a></div></div>');i.find(".btn-3").click(function(){$.getJSON(delUrl,{userId:Cookie.get("token"),ugcId:t.attr("attr-id")},function(e){if(COMMON.checkJSonData(e),!e.ok)return COMMON.showTip(e.message),void 0;var t=o.FavoritesListView.page.settings.pageNum;1==o.FavoritesListView.favoritesCollection.length&&t>1&&(t-=1),o.FavoritesListView.pageHandler({pageNum:t})}),COMMON.hideDialog()}),i.find(".btn-4").click(function(){COMMON.hideDialog()}),COMMON.showSimpleDialog(i,"我的收藏")}}),o.exports=i.Views.FavoritesListItemView});