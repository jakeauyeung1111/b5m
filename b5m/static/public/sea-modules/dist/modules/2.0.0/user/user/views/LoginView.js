/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/2.0.0/user/user/views/LoginView",["$","underscore","backbone","modules/2.0.0/user/user/tpl/login.html","jquery/validate/1.11.1/validate"],function(e,t,i){this._=e("underscore"),this.Backbone=e("backbone"),this.tpl_user_login=e("modules/2.0.0/user/user/tpl/login.html");var o={Views:{},Urls:{LoginNum:B5M_UC.rootPath+"/user/user/data/loginNum.htm?jsonpCallback=?",Login:B5M_UC.rootPath+"/user/user/data/login.htm?jsonpCallback=?"}};this.root=void 0,o.Views.LoginView=Backbone.View.extend({template:_.template(tpl_user_login),initialize:function(e){root=this,this.opt=e,this.render(),this.initValidate(),this.loginNum(this.$el.find('input[name="email"]').val())},render:function(){this.$el.find(".ui-loading").hide(),this.$el.html(this.template(this.opt))},events:{"click #submit_btn":"submitHandler","click #refreshCode":"refreshCodeHandler",'keydown input[name="password"],input[name="code"]':"submitHandler"},submitHandler:function(e){("keydown"!=e.type||13==e.keyCode)&&this.$loginForm.valid()&&$.getJSON(o.Urls.Login,this.$loginForm.serializeArray(),function(e){if(e.ok){if(root.opt&&root.opt.loginSuccessHandler)return root.opt.loginSuccessHandler(e.data),void 0;window.location.href=e.data.refererUrl}else root.$el.find(".login-chk").before('<div id="serror" class="login-field login-field-spe cfx"><div class="login-error"><i></i>'+e.data+"</div></div>"),root.refreshCodeHandler(),root.loginNum(root.$el.find('input[name="email"]').val())})},loginNum:function(e){!e&&0>=e.length||$.getJSON(o.Urls.LoginNum,"email="+e,function(e){e.ok?root.$el.find(".rows-verify").css("display","none"):root.$el.find(".rows-verify").css("display","block")})},refreshCodeHandler:function(){$("#code").attr("src",B5M_UC.rootPath+"/validateCode.do?f="+Math.random())},initValidate:function(){this.$loginForm=this.$el.find("#loginForm"),jQuery.validator.addMethod("codeLength",function(e){return 4==e.length?!0:!1},"验证码为4位"),jQuery.validator.addMethod("loginNum",function(e){return root.loginNum(e),!0},""),this.$loginForm.validate({rules:{email:{required:!0,email:!0,loginNum:!0},password:{required:!0,rangelength:[6,15]},code:{required:!0,codeLength:!0}},messages:{email:{required:"Email不能为空",email:"邮箱地址格式不正确"},password:{required:"密码不能为空",rangelength:"密码长度必须介于 {0} 和 {1} 之间"},code:{required:"验证码不能为空"}},errorElement:"div",errorClass:"text-error",errorPlacement:function(e,t){root.$el.find("#serror").remove();var i=t.parent();i.find('div[class="login-error"]').remove(),i.append('<div class="login-error"><i></i>'+e.html()+"</div>")},success:function(e,t){$(t).parent().find('div[class="login-error"]').remove()},onclick:!1})}}),i.exports=o.Views.LoginView});