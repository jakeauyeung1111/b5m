/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/2.0.0/user/trade/common/models/OrderCodeCollection",["$","underscore","backbone","common"],function(e,t,i){this._=e("underscore"),this.Backbone=e("backbone"),this.COMMON=e("common");var o=[{code:11,name:"支付充值"},{code:12,name:"活动奖励"},{code:13,name:"任务奖励"},{code:14,name:"支付+帮豆充值"},{code:15,name:"退款"},{code:21,name:"消费"},{code:31,name:"兑换"},{code:41,name:"转账"}],s={Models:{},Collections:{},Urls:{}};s.Models.OrderCode=Backbone.Model.extend({}),s.Collections.OrderCodeCollection=Backbone.Collection.extend({model:s.Models.OrderCode,initialize:function(){this.add(o)}}),i.exports=s.Collections.OrderCodeCollection});