define("modules/2.1.0/goods/models/TejiaCollection-debug", [ "$-debug", "backbone-debug", "common-debug" ], function(require, exports, module) {
    this._ = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    this.COMMON = require("common-debug");
    var TejiaCollection = {
        Models: {},
        Collections: {},
        Urls: {}
    };
    TejiaCollection.Urls.TejiaUrl = B5M_UC.rootPath + "/other/goods/data/tejia.htm";
    TejiaCollection.Models.Tejia = Backbone.Model.extend({});
    TejiaCollection.Collections.Tejias = Backbone.Collection.extend({
        model: TejiaCollection.Models.Tejia,
        url: TejiaCollection.Urls.TejiaUrl,
        parse: function(response) {
            COMMON.checkJSonData(response);
            var data = response;
            for (var i = 0; i < data.length; i++) {
                data[i].autoId = data[i].id;
                delete data[i].id;
            }
            return data;
        },
        sync: function(method, model, options) {
            var params = _.extend({
                //				async:false,
                cache: false,
                dataType: "jsonp",
                jsonp: "jsonpCallback",
                // the api requires the jsonp callback name to be this exact name
                type: "POST",
                url: model.url,
                processData: true
            }, options);
            return $.ajax(params);
        }
    });
    module.exports = TejiaCollection.Collections.Tejias;
});