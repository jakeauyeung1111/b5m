define("modules/2.1.0/goods/views/TejiaViews-debug", [ "$-debug", "underscore-debug", "backbone-debug", "common-debug", "modules/2.1.0/goods/views/TejiaViewItem-debug", "modules/2.1.0/goods/models/TejiaCollection-debug" ], function(require, exports, module) {
    this.COMMON = require("common-debug");
    this.underscore = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    this.TejiaView = require("modules/2.1.0/goods/views/TejiaViewItem-debug");
    this.TejiaCollection = require("modules/2.1.0/goods/models/TejiaCollection-debug");
    var root = undefined;
    this.TejiaViews = Backbone.View.extend({
        template: _.template('<ul class="cfx"></ul>'),
        initialize: function(opt) {
            root = this;
            this.$el.append(this.template(opt));
            this.render();
        },
        render: function() {
            this.$el.find(".ui-loading").hide();
            this.tejiaCollection = new TejiaCollection();
            this.tejiaCollection.fetch({
                success: function() {
                    root.showTejiaList();
                    if (root.tejiaCollection.length == 0) root.$el.append('<div class="ui-empty-data">我们正在为您准备推荐商品...</div>').show();
                }
            });
        },
        showTejiaList: function() {
            this.tejiaCollection.each(function(tejia, index) {
                var itemClass = "";
                if ((index + 1) % 4 == 0) itemClass = "last";
                var htmlElement = new TejiaView({
                    model: tejia,
                    attributes: {
                        "class": itemClass
                    }
                });
                this.$el.find("ul").append(htmlElement.el);
            }, this);
        }
    });
    module.exports = TejiaViews;
});