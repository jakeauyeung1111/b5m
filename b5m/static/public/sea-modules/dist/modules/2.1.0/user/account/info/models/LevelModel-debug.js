define("modules/2.1.0/user/account/info/models/LevelModel-debug", [ "$-debug", "underscore-debug", "backbone-debug", "common-debug" ], function(require, exports, module) {
    this._ = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    this.COMMON = require("common-debug");
    var LevelModel = {
        Models: {},
        Urls: {}
    };
    LevelModel.Urls.LevelModel = B5M_UC.rootPath + "/user/info/data/levelRule.htm";
    LevelModel.Models.LevelModel = Backbone.Model.extend({
        url: LevelModel.Urls.LevelModel,
        sync: function(method, model, options) {
            var params = _.extend({
                async: false,
                cache: false,
                dataType: "jsonp",
                jsonp: "jsonpCallback",
                // the api requires the jsonp callback name to be this exact name
                type: "POST",
                url: model.url,
                processData: true
            }, options);
            return $.ajax(params);
        },
        parse: function(response) {
            COMMON.checkJSonData(response);
            if (response.ok) {
                var data = response.data;
                for (var i = 0; i < data.length; i++) {
                    data[i].autoId = data[i].id;
                    delete data[i].id;
                }
                return data;
            }
            COMMON.showTip(response.data);
            return null;
        },
        initialize: function() {
            this.fetch();
        }
    });
    module.exports.LevelModel = LevelModel.Models.LevelModel;
});