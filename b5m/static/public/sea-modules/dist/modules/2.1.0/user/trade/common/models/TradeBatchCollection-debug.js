define("modules/2.1.0/user/trade/common/models/TradeBatchCollection-debug", [ "$-debug", "underscore-debug", "backbone-debug", "common-debug", "modules/2.1.0/user/trade/common/models/OrderCodeCollection-debug" ], function(require, exports, module) {
    this._ = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    this.COMMON = require("common-debug");
    this.OrderCodeCollection = require("modules/2.1.0/user/trade/common/models/OrderCodeCollection-debug");
    var TradeBatchCollection = {
        Models: {},
        Collections: {},
        Urls: {}
    };
    TradeBatchCollection.Urls.tradeBatchList = B5M_UC.rootPath + "/user/trade/common/data/tradeBatchQuery.htm";
    TradeBatchCollection.Models.Trade = Backbone.Model.extend({});
    TradeBatchCollection.Collections.TradeBatchCollection = Backbone.Collection.extend({
        url: TradeBatchCollection.Urls.tradeBatchList,
        model: TradeBatchCollection.Models.Trade,
        sync: function(method, model, options) {
            var params = _.extend({
                async: false,
                cache: false,
                dataType: "jsonp",
                jsonp: "jsonpCallback",
                // the api requires the jsonp callback name to be this exact name
                type: "POST",
                url: model.url,
                processData: true
            }, options);
            return $.ajax(params);
        },
        parse: function(response) {
            COMMON.checkJSonData(response);
            if (response.ok) return this.initModelData(response.data);
            COMMON.showTip(response.data);
            return null;
        },
        initModelData: function(data) {
            var orderCodeCollection = new OrderCodeCollection();
            _.each(data.list, function(obj) {
                obj.autoId = obj.id;
                delete obj.id;
                _.each(orderCodeCollection.models, function(model) {
                    if (model.get("code") == obj.orderCode) {
                        obj._orderName = model.get("name");
                        return;
                    }
                });
                if (obj._orderName == undefined) obj._orderName = "";
            });
            this.dataTotal = data.total;
            return data.list;
        }
    });
    module.exports = TradeBatchCollection.Collections.TradeBatchCollection;
});