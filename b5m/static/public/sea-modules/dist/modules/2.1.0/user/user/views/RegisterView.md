# 用户注册框

- pubdate: 2012-12-12 12:00
- author:zhangshun

------

````html
<div class="reg-box" id="register-view">
    <div class="ui-loading">正在加载...</div>
</div>
````
````javascript
seajs.use('modules/2.1.0/user/user/views/RegisterView',function(RegisterView){
	new RegisterView({
            el:'#register-view',
            userType:'',                                //用户类型
            userName:'',                                //用户名
            email:'',                                   //初始化帐户名(会在帐户名文本框中显示)
            url:'http%3A%2F%2Fwww.b5m.com',		//回跳地址
            userMps:'organic',				//来源渠道
            state:''
    });
});
````
