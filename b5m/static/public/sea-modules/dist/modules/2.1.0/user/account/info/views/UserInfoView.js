/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/2.1.0/user/account/info/views/UserInfoView",["$","underscore","backbone","common","modules/2.1.0/user/account/info/models/UserModel","modules/2.1.0/user/account/info/models/LevelModel"],function(e,t,i){this._=e("underscore"),this.Backbone=e("backbone"),this.COMMON=e("common"),this.UserModel=e("modules/2.1.0/user/account/info/models/UserModel"),this.LevelModel=e("modules/2.1.0/user/account/info/models/LevelModel");var o={Views:{},Urls:{}};this.tpl_user_info_id_1="modules/2.1.0/user/account/info/tpl/info-1.html",this.tpl_user_info_id_2="modules/2.1.0/user/account/info/info-2.html",this.tpl_user_info_id_3="modules/2.1.0/user/account/info/info-3.html",this.tpl_user_info_id_4="modules/2.1.0/user/account/info/info-b5m.html";var a=void 0;o.Views.UserInfoView=Backbone.View.extend({initialize:function(t){a=this,this.userModel=new UserModel.UserModel({type:2}),this.levelModel=new LevelModel.LevelModel;var i=tpl_user_info_id_1,o=this;switch(t.tplId){case 1:i=tpl_user_info_id_1;break;case 2:i=tpl_user_info_id_2;break;case 3:i=tpl_user_info_id_3;break;case 4:i=tpl_user_info_id_4}e.async(i,function(e){o.template=_.template(e),o.render()})},render:function(){this.$el.find(".ui-loading").hide();var e=_.extend(this.userModel.toJSON(),this.levelModel.toJSON());e.lastLoginTime=null==e.lastLoginTime||void 0==e.lastLoginTime?COMMON.formatTime(new Date,"yyyy-MM-dd"):COMMON.formatTime(e.lastLoginTime,"yyyy-MM-dd"),this.$el.html(this.template(e)),0==this.userModel.length&&this.$el.find(".ui-empty-data").show()},events:{"click #everySign":"everySign","click #hasInstallPlugin":"hasInstallPlugin","click #editAvatarBut":"editAvatar","mouseover .user-pic":"mouseover","mouseleave .user-pic":"mouseleave","hover #bangbichargebeanhandle":"bangbichargebeanhandle"},editAvatar:function(){e.async(["arale/dialog/1.2.6/dialog","modules/default/common/avatar/AvatarView"],function(e,t){new e({content:(new t).el,width:"740px",height:"581px"}).after("hide",function(){var e=a.$el.find("#avatarImg");e.attr("src",e.attr("src").split("?")[0]+"?_="+Math.random())}).show()})},everySign:function(e){var t=$(e.currentTarget);"true"!=t.attr("isSign")&&$.getJSON(B5M_UC.rootPath+"/user/task/data/sign.htm?jsonpCallback=?",function(e){e.ok?window.location.href=window.location.href:COMMON.showTip(e.data)})},hasInstallPlugin:function(){e.async(["modules/2.1.0/user/task/tpl/user-plugin-install-or-sign.html","arale/dialog/1.2.6/dialog"],function(e,t){var i=new t({content:_.template(e,{flag:a.isInstallPlugin()})}).show();$(i.contentElement).find("#pluginSign").click(function(e){a.pluginSign(e,i)}),$(i.contentElement).find("#pluginInstall").click(function(){i.destroy()})})},isInstallPlugin:function(){var e=!!document.getElementById("b5mmain");if(e)return e;var t=$('<span><object type="application/x-bang5taoplugin" width="0" height="0" style="display:none"></object></span>');return $("body").append(t),!!t.find("object")[0].getCacheValue},pluginSign:function(e,t){e.preventDefault(),$.getJSON(B5M_UC.rootPath+"/user/task/data/plug.htm?jsonpCallback=?",{isInstall:"1"},function(e){e.ok?window.location.reload():(t.destroy(),COMMON.showTip(e.data))})},mouseover:function(e){$(e.currentTarget).find("a").animate({bottom:21},200)},mouseleave:function(e){$(e.currentTarget).find("a").animate({bottom:-1},200)},bangbichargebeanhandle:function(e){$(e.currentTarget).css("text-decoration","none")}}),i.exports=o.Views.UserInfoView});