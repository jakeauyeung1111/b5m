define("modules/2.0.0/user/trade/bangbi/exchange/views/ExchangeConfirmViews-debug", [ "$-debug", "underscore-debug", "backbone-debug", "common-debug", "modules/2.0.0/user/trade/bangbi/exchange/tpl/exchangeConfirm-debug.html" ], function(require, exports, module) {
    this.COMMON = require("common-debug");
    this.underscore = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    var ExchangeConfirmViews = Backbone.View.extend({
        tagName: "form",
        events: {
            "click #exchangeBangbi2bean": "exchangeBangbi2bean",
            "click #btnClose": "btnClose"
        },
        initialize: function() {
            this.render();
        },
        render: function() {
            //加载模版
            this.template = underscore.template(require("modules/2.0.0/user/trade/bangbi/exchange/tpl/exchangeConfirm-debug.html"));
            this.$el.html(this.template(this.model));
            return this;
        },
        exchangeBangbi2bean: function(e) {
            var self_this = this;
            $.post(B5M_UC.rootPath + "/exchange/exchange.htm", $(e.currentTarget).parents("form:first").serialize(), function(response) {
                COMMON.checkJSonData(response);
                COMMON.hideDialog();
                if (response.ok) {
                    COMMON.showTip("兑换成功", "帮币兑换帮豆");
                    var lastBangbi = parseFloat($("#restBangbi").text() - self_this.model.exchangeBangbi);
                    $("#restBangbi").text(lastBangbi);
                    return null;
                }
                COMMON.showTip("兑换失败", "帮币兑换帮豆");
            });
        },
        btnClose: function() {
            COMMON.hideDialog();
        }
    });
    module.exports = ExchangeConfirmViews;
});