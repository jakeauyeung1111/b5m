define("modules/2.0.0/user/trade/bangbi/exchange/views/ExchangeViews-debug", [ "$-debug", "underscore-debug", "backbone-debug", "common-debug", "modules/2.0.0/user/trade/bangbi/exchange/tpl/exchange-debug.html", "modules/2.0.0/user/trade/bangbi/exchange/views/ExchangeConfirmViews-debug", "modules/2.0.0/user/user/models/UserModel-debug" ], function(require, exports, module) {
    this.COMMON = require("common-debug");
    this.underscore = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    this.UserCenterCollection = require("modules/2.0.0/user/user/models/UserModel-debug");
    this.ExchangeConfirmViews = require("modules/2.0.0/user/trade/bangbi/exchange/views/ExchangeConfirmViews-debug");
    var ExchangeViews = Backbone.View.extend({
        tagName: "form",
        events: {
            "change #selectBangbi": "selectBangbi",
            "click #showConfirmDialog": "showConfirmDialog"
        },
        initialize: function(opt) {
            this.el = opt.el;
            this.render();
        },
        render: function() {
            //加载模版
            this.template = underscore.template(require("modules/2.0.0/user/trade/bangbi/exchange/tpl/exchange-debug.html"));
            var data = new UserCenterCollection.UserModel().toJSON();
            this.$el.html(this.template(data));
        },
        selectBangbi: function() {
            var selectBangbi = $("#selectBangbi").val();
            var restBangbi = $("#restBangbi").text();
            if (parseInt(restBangbi) < parseInt(selectBangbi)) {
                COMMON.showTip("您的剩余帮币数量不够！", "帮豆兑换帮币");
                $("#exchargeBeanAmout").val("");
                return false;
            }
            $("#exchargeBeanAmout").val(selectBangbi * 100);
        },
        showConfirmDialog: function(e) {
            //判断是否符合兑换条件
            this.selectBangbi();
            if (COMMON.isEmptyValue($("#exchargeBeanAmout").val())) {
                return;
            }
            var bangbi2beanModel = {
                exchangeBangbi: $("#selectBangbi").val(),
                exchangeBean: $("#exchargeBeanAmout").val()
            };
            var exchangeConfirmViews = new ExchangeConfirmViews({
                model: bangbi2beanModel
            });
            COMMON.showSimpleDialog(exchangeConfirmViews.el, "帮币兑换帮豆确认");
        }
    });
    module.exports = ExchangeViews;
});