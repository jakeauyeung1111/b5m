/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/2.1.0/user/msg/system/views/SystemMsgView",["$","underscore","backbone","common","modules/default/common/Page","modules/2.1.0/user/msg/system/models/SystemMsgCollection","modules/2.1.0/user/msg/system/views/SystemMsgViewItem"],function(e,t,i){this._=e("underscore"),this.Backbone=e("backbone"),this.COMMON=e("common"),this.Page=e("modules/default/common/Page"),this.tpl_account_history="modules/2.1.0/user/msg/system/tpl/system-msg.html",this.SystemMsgCollection=e("modules/2.1.0/user/msg/system/models/SystemMsgCollection"),this.SystemMsgViewItem=e("modules/2.1.0/user/msg/system/views/SystemMsgViewItem");var o={Views:{},Urls:{}};o.Views.SystemMsgView=Backbone.View.extend({initialize:function(t){this.parameterData=this.initParameter(t),this.el=t.el;var i=tpl_account_history,o=this;"default"==this.parameterData.tplName&&(i=tpl_account_history),e.async(i,function(e){o.$el.append(e),o.systemMsgCollection=new SystemMsgCollection,o.pageHandler({pageNum:1})})},initParameter:function(e){return _.extend({pageNum:1,pageSize:5,showPage:!1,tplName:"default"},e)},render:function(){this.systemMsgCollection.each(function(e,t){this.renderData(e,t)},this)},renderData:function(e,t){var i="";0!=t%2&&(i="trEvenBg");var o=new SystemMsgViewItem({model:e,SystemMsgView:this,attributes:{"class":i}});this.$el.find("#user-message-list").append(o.el)},pageHandler:function(e){this.refreshMsgCount();var t=this;this.systemMsgCollection.fetch({data:{currentPage:e.pageNum,pageSize:t.parameterData.pageSize},success:function(){return t.hideLodingData(),t.$el.find("#message-count").empty().html(t.systemMsgCollection.dataTotal),0==t.systemMsgCollection.length?(t.$el.find(".center-msg-none").show(),void 0):(t.render(),t.systemMsgCollection.length>0&&t.refreshPage(e),void 0)}})},refreshMsgCount:function(){$.getJSON(B5M_UC.rootPath+"/user/message/data/count.htm?jsonpCallback=?",function(e){if(COMMON.checkJSonData(e),e.ok){var t=$(".new-msg-count").empty();0!=e.data&&t.html("(<em>"+e.data+"</em>)")}else COMMON.showTip(e.data)})},refreshPage:function(e){if(!this.page){var t=this;this.page=new Page({onClick:function(e){t.pageHandler({pageNum:e})}})}this.$el.find(".page-view").empty().append(this.page.getPageDom({pageNum:e.pageNum,total:this.systemMsgCollection.dataTotal,pageSize:this.parameterData.pageSize}))},showLoadingData:function(){this.$el.find(".ui-loading").show(),this.$el.find(".page-view").empty(),this.$el.find("#user-message-list").empty(),this.$el.find(".center-msg-none").hide()},hideLodingData:function(){this.$el.find(".ui-loading").hide(),this.$el.find(".page-view").empty(),this.$el.find("#user-message-list").empty(),this.$el.find(".center-msg-none").hide()}}),i.exports=o.Views.SystemMsgView});