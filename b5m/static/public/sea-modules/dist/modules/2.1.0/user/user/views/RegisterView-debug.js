define("modules/2.1.0/user/user/views/RegisterView-debug", [ "$-debug", "underscore-debug", "backbone-debug", "common-debug", "jquery/validate/1.11.1/validate-debug", "modules/default/common/TestPassword-debug" ], function(require, exports, module) {
    this._ = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    this.COMMON = require("common-debug");
    this.TestPassword = require("modules/default/common/TestPassword-debug");
    var RegisterView = {
        Views: {},
        Urls: {}
    };
    var root = undefined;
    RegisterView.Views.RegisterView = Backbone.View.extend({
        initialize: function(opt) {
            root = this;
            root.parameter = opt;
            root.parameter.isSubmitBtnDisable = true;
            this.render();
        },
        render: function() {
            this.$el.find(".ui-loading").hide();
            require.async("modules/2.1.0/user/user/tpl/register.html", function(html) {
                root.$el.html(_.template(html, root.parameter));
                root.testPwd = new TestPassword();
                root.$el.find("#testPwd").empty().append(root.testPwd.getDefaultHtml(""));
                root.initValidate();
                root.$el.find("#protocol-but").click(function(e) {
                    root.showProtocolDialog(e);
                });
            });
        },
        events: {
            "click #refreshCode": "refreshCode",
            "click #submit_btn": "submit_btn",
            "keydown #valiCode": "valiCodeKeyDown",
            "keyup #password": "testPassword"
        },
        showProtocolDialog: function(e) {
            require.async([ "arale/dialog/1.2.6/dialog", "modules/2.1.0/user/user/tpl/protocol.html" ], function(Dialog, html) {
                new Dialog({
                    content: html,
                    width: "695px",
                    height: "390px"
                }).after("show", function() {
                    var dialog = this;
                    $(this.contentElement[0]).find(".dialog-protocol-btn").click(function() {
                        root.$el.find("#J_Access").attr("checked", "checked");
                        dialog.destroy();
                    });
                }).show();
            });
        },
        testPassword: function(e) {
            e.preventDefault();
            $(e.currentTarget).parent().parent().find("#testPwd").empty().append(this.testPwd.getDefaultHtml($(e.currentTarget).val()));
        },
        refreshCode: function(e) {
            $("#code").attr("src", B5M_UC.rootPath + "/validateCode.do?f=" + Math.random());
        },
        valiCodeKeyDown: function(e) {
            if (e.keyCode == 13) this.submitHandler();
        },
        submit_btn: function(e) {
            e.preventDefault();
            this.submitHandler();
        },
        submitHandler: function(e) {
            //提交按钮不可点击
            if (root.parameter.isSubmitBtnDisable === false) return;
            if (this.validateForm.valid()) {
                //是否同意协议
                if (!root.$el.find("#J_Access").attr("checked")) {
                    COMMON.showTip("请接受服务条款");
                    return;
                }
                //手机 用户名 email处理
                var valArray = this.validateForm.serializeArray();
                for (var i = 0; i < valArray.length; i++) {
                    if (valArray[i].name === "email") {
                        if (root.isMobile(valArray[i].value)) {
                            valArray[i].name = "mobile";
                        } else if (root.isName(valArray[i].value)) {
                            valArray[i].name = "userName";
                        } else if (root.isEmail(valArray[i].value)) {
                            break;
                        } else {
                            COMMON.showTip("字段类型找不到");
                        }
                        break;
                    }
                }
                root.submitBtnDisable();
                $.getJSON(B5M_UC.rootPath + "/user/info/data/register.htm", valArray, function(data) {
                    root.submitBtnAble();
                    if (data.ok) {
                        if (root.parameter && root.parameter.registerSuccessHandler) {
                            root.parameter.registerSuccessHandler(data.data);
                            return;
                        } else if (COMMON.isEmptyValue(root.parameter.url)) {
                            window.location.href = B5M_UC.rootPath + "/forward.htm?method=/user/user/index";
                        } else {
                            window.location.href = root.parameter.url;
                        }
                    } else {
                        root.$el.find("#serror").empty().append('<div class="msg-error"><i></i>' + data.data + "</div>");
                        root.refreshCode();
                    }
                });
            }
        },
        isNameOrEmailUse: function(value) {
            var code = 0;
            $.ajax({
                type: "post",
                data: "email=" + value,
                dataType: "jsonp",
                jsonp: "jsonpCallback",
                async: false,
                url: B5M_UC.rootPath + "/user/info/data/isNameOrEmailUse.do",
                success: function(r) {
                    code = eval(r).code;
                }
            });
            return code == 0 ? true : false;
        },
        isMobile: function(value) {
            return /^(13[0-9]|15[012356789]|18[0-9]|14[57])[0-9]{8}$/.test(value);
        },
        isEmail: function(value) {
            return /^[\w-]+(\.[\w-]+)*@[\w-]+(\.[\w-]+)+$/.test(value);
        },
        isName: function(value) {
            return /^[a-zA-Z_0-9\u4e00-\u9fa5]{4,15}$/.test(value);
        },
        isMobileUse: function(value) {
            var code = 0;
            $.ajax({
                type: "post",
                data: "mobile=" + value,
                dataType: "jsonp",
                jsonp: "jsonpCallback",
                async: false,
                url: B5M_UC.rootPath + "/user/info/data/isMobileUse.htm",
                success: function(r) {
                    code = eval(r).code;
                }
            });
            return code === 1 ? true : false;
        },
        initValidate: function() {
            var $registerForm = $("#registerForm");
            //自定义异步验证验证
            jQuery.validator.addMethod("isAccount", function(value, element, params) {
                if (root.isMobile(value)) {
                    if (root.isMobileUse(value)) {
                        return true;
                    } else {
                        this.settings.messages.email.isAccount = "手机号已经被占用，请更换手机号";
                        return false;
                    }
                }
                if (root.isEmail(value)) {
                    if (root.isNameOrEmailUse(value)) {
                        return true;
                    } else {
                        this.settings.messages.email.isAccount = "邮箱已经被占用，请更换邮箱";
                        return false;
                    }
                }
                //临时代码-隐藏用户名 start
                this.settings.messages.email.isAccount = "请输入邮箱/手机";
                return false;
                //end
                if (!/^[a-zA-Z_0-9\u4e00-\u9fa5]+$/.test(value)) {
                    this.settings.messages.email.isAccount = '用户名由中文、英文、数字及"_"组成';
                    return false;
                }
                if (root.isName(value)) {
                    if (root.isNameOrEmailUse(value)) {
                        return true;
                    } else {
                        this.settings.messages.email.isAccount = "用户名已经被占用，请更换用户名";
                        return false;
                    }
                } else {
                    this.settings.messages.email.isAccount = "用户名长度需4至15位";
                    return false;
                }
            }, jQuery.format("{0}"));
            jQuery.validator.addMethod("codeLength", function(value, element) {
                return value.length == 4 ? true : false;
            }, "验证码为4位");
            jQuery.validator.addMethod("isPasswordOK", function(value, element) {
                return /^[a-zA-Z_0-9]{6,15}$/.test(value);
            }, "请输入正确格式的字母，数字");
            $registerForm.validate({
                rules: {
                    email: {
                        required: true,
                        isAccount: true
                    },
                    password: {
                        required: true,
                        rangelength: [ 6, 15 ],
                        isPasswordOK: true
                    },
                    confirmPassword: {
                        required: true,
                        rangelength: [ 6, 15 ],
                        isPasswordOK: true,
                        equalTo: "#password"
                    },
                    code: {
                        required: true,
                        codeLength: true
                    }
                },
                messages: {
                    email: {
                        //						required: "请输入邮箱/用户名/手机",
                        required: "请输入邮箱/手机",
                        isAccount: ""
                    },
                    password: {
                        required: "密码不能为空",
                        rangelength: "密码长度需{0}至{1}位"
                    },
                    confirmPassword: {
                        required: "确认密码不能为空",
                        rangelength: "密码长度需{0}至{1}位",
                        equalTo: "两次输入的密码不一致"
                    },
                    code: {
                        required: "验证码不能为空"
                    }
                },
                errorElement: "span",
                errorClass: "kv-text-error",
                errorPlacement: function(error, element) {
                    $("#serror").empty();
                    element.attr("class", "kv-text kv-text-error");
                    var errorBox = element.parent().parent().find(".msg-box").empty();
                    errorBox.append('<div class="msg-error show"><i></i>' + error.html() + "</div>");
                },
                success: function(label, d) {
                    var parent = $(d).parent().parent().find(".msg-box").empty();
                    if ($(d).attr("name") != "code") parent.append('<div class="msg-success show"><i></i></div>');
                },
                onclick: false
            });
            this.validateForm = $registerForm;
        },
        submitBtnAble: function() {
            root.parameter.isSubmitBtnDisable = true;
            root.$el.find("#submit_btn").removeClass("kv-submit-disable").html("用户注册");
        },
        submitBtnDisable: function() {
            root.parameter.isSubmitBtnDisable = false;
            root.$el.find("#submit_btn").addClass("kv-submit-disable").html("正在注册...");
        }
    });
    module.exports = RegisterView.Views.RegisterView;
});