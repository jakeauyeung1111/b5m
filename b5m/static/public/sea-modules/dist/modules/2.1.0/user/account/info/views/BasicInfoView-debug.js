define("modules/2.1.0/user/account/info/views/BasicInfoView-debug", [ "$-debug", "common-debug", "underscore-debug", "backbone-debug", "modules/default/common/DateCascade-debug", "modules/default/common/CityCascade-debug", "modules/2.1.0/user/account/info/models/UserModel-debug", "jquery/validate/1.11.1/validate-debug" ], function(require, exports, module) {
    this.COMMON = require("common-debug");
    this._ = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    this.UserCenterCollection = require("modules/2.1.0/user/account/info/models/UserModel-debug");
    this.CityCascade = require("modules/default/common/CityCascade-debug");
    this.DateCascade = require("modules/default/common/DateCascade-debug");
    var BasicInfoView = Backbone.View.extend({
        tagName: "form",
        events: {
            "click #editBasicInfoSub": "editBasicInfoSub"
        },
        initialize: function(opt) {
            this.el = opt.el;
            var self = this;
            require.async("modules/2.1.0/user/account/info/tpl/basic-info.html", function(html) {
                self.template = _.template(html);
                self.render();
                self.initValidateForm();
            });
        },
        render: function() {
            this.data = new UserCenterCollection.UserModel({
                type: 1
            }).toJSON();
            var infoArray = [ this.data.email, this.data.nickName, this.data.realName, this.data.gender, this.data.birthday, this.data.currentResidence, this.data.hometown, this.data.mobile, this.data.avatar ];
            //用户完成资料程度
            this.userInfoRate(this.data, infoArray);
            this.$el.html(this.template(this.data));
            //生日
            this.initBirthDay(this.data.birthday);
            //性别
            this.initGender(this.data.gender);
            //邮箱
            this.initEmailStatus(this.data.activation);
            //居住地址
            this.initCurrentResidence(this.data.currentResidence);
            //家乡地址
            this.initHomeTown(this.data.hometown);
        },
        initGender: function(gender) {
            /*初始化性别*/
            if (gender == "male") {
                $("input[name=gender][value='male']").attr("checked", true);
            } else if (gender == "female") {
                $("input[name=gender][value='female']").attr("checked", true);
            } else {
                $("input[name=gender][value='male']").attr("checked", true);
            }
        },
        initEmailStatus: function(activation) {
            /*判断邮箱是否验证*/
            if (activation == "REG") {
                $("#validateEmail").hide();
            } else {
                $("#validateEmail").show();
            }
        },
        initBirthDay: function(birthDay) {
            new DateCascade({
                yearDom: $("#year")[0],
                monthDom: $("#month")[0],
                dayDom: $("#day")[0],
                constellationId: "constellationshow",
                constellationHiddenInputDomId: "constellation",
                birthDayHiddenInputDomId: "brithdayStr"
            });
        },
        initCurrentResidence: function(currentResidence) {
            //居住地址
            new CityCascade({
                provDom: $("#cProvince")[0],
                cityDom: $("#cCity")[0],
                noLimit: {
                    city: !0
                },
                badDesign: !0,
                hiddenInputDomId: "currentResidence"
            });
        },
        initHomeTown: function(hometown) {
            //家乡地址
            new CityCascade({
                provDom: $("#hProvince")[0],
                cityDom: $("#hCity")[0],
                noLimit: {
                    city: !0
                },
                badDesign: !0,
                hiddenInputDomId: "hometown"
            });
        },
        initValidateForm: function() {
            //验证
            var $userBasicInfoForm = $("#userBasicInfoForm");
            jQuery.validator.addMethod("isNickNameOK", function(value, element) {
                if ($.trim(value) == "") {
                    $(element).parent().find(".errorBox").empty();
                    return true;
                }
                var flag = $.trim(value).length < 15;
                return flag;
            }, "昵称长度不能超过15位");
            jQuery.validator.addMethod("isMobileOK", function(value, element) {
                if ($.trim(value) == "") {
                    $(element).parent().find(".errorBox").empty();
                    return true;
                }
                return /^((\(\d{3}\))|(\d{3}\-))?1\d{10}$/.test(value);
            }, "请输入合法的手机号码");
            jQuery.validator.addMethod("isMobileUse", function(value, element) {
                if ($.trim(value) == "") {
                    $(element).parent().find(".errorBox").empty();
                    return true;
                }
                var isOk = false;
                $.ajax({
                    type: "post",
                    data: "mobile=" + value,
                    async: false,
                    url: B5M_UC.rootPath + "/user/info/data/isMobileUse.htm",
                    success: function(r) {
                        isOk = eval(r).ok;
                    }
                });
                return isOk;
            }, "手机号码已经被占用，请输入新的号码");
            //自定义异步验证验证
            jQuery.validator.addMethod("isEmail", function(value, element) {
                var code = 0;
                $.ajax({
                    type: "post",
                    data: "email=" + value,
                    async: false,
                    url: B5M_UC.rootPath + "/user/info/data/isNameOrEmailUse.htm",
                    success: function(r) {
                        code = eval(r).code;
                    }
                });
                return code == 0 ? true : false;
            }, "邮箱已经被占用，请更换邮箱");
            if (!COMMON.isEmptyValue(this.data.email)) {
                $userBasicInfoForm.validate({
                    rules: {
                        nickName: {
                            isNickNameOK: true
                        },
                        mobile: {
                            isMobileOK: true,
                            isMobileUse: true,
                            rangelength: [ 11, 12 ]
                        }
                    },
                    messages: {
                        mobile: {
                            rangelength: "手机长度必11位"
                        }
                    },
                    errorElement: "span",
                    errorClass: "text_error",
                    errorPlacement: function(error, element) {
                        var errorBox = element.parent().parent().find(".errorBox").empty();
                        errorBox.append(error.html());
                    },
                    success: function(label, d) {
                        var parent = $(d).parent().parent().find(".errorBox").empty();
                    },
                    onclick: false
                });
            } else {
                $userBasicInfoForm.validate({
                    rules: {
                        email: {
                            required: true,
                            email: true,
                            isEmail: true
                        },
                        nickName: {
                            isNickNameOK: true
                        },
                        mobile: {
                            isMobileOK: true,
                            isMobileUse: true,
                            rangelength: [ 11, 12 ]
                        }
                    },
                    messages: {
                        email: {
                            required: "当前邮箱不能为空",
                            email: "邮箱地址格式不正确"
                        },
                        mobile: {
                            rangelength: "手机长度必11位"
                        }
                    },
                    errorElement: "span",
                    errorClass: "text_error",
                    errorPlacement: function(error, element) {
                        var errorBox = element.parent().parent().find(".errorBox").empty();
                        errorBox.append(error.html());
                    },
                    success: function(label, d) {
                        var parent = $(d).parent().parent().find(".errorBox").empty();
                    },
                    onclick: false
                });
            }
            this.validateForm = $userBasicInfoForm;
        },
        //提交编辑用户信息
        editBasicInfoSub: function(e) {
            var self_this = this;
            if (self_this.validateForm.valid()) {
                $.post(B5M_UC.rootPath + "/user/info/data/updateUserInfo.htm", $(e.currentTarget).parents("form:first").serialize(), function(response) {
                    COMMON.checkJSonData(response);
                    if (response.ok) {
                        COMMON.showTip("修改成功", "编辑用户信息");
                    } else {
                        COMMON.showTip(response.data, "编辑用户信息");
                    }
                });
            }
        },
        userInfoRate: function(data, array) {
            var userInfoRate = 10;
            var j = 0;
            for (var i = 0; i < array.length; i++) {
                if (!COMMON.isEmptyValue(array[i])) {
                    userInfoRate = userInfoRate + 10;
                } else {
                    j++;
                    if (j == 1) {
                        var unCompleteRule = "";
                        switch (i) {
                          case 0:
                            unCompleteRule = "完成邮箱";
                            break;

                          case 1:
                            unCompleteRule = "完成昵称";
                            break;

                          case 2:
                            unCompleteRule = "完成真实姓名";
                            break;

                          case 3:
                            unCompleteRule = "完成性别";
                            break;

                          case 4:
                            unCompleteRule = "完成生日";
                            break;

                          case 5:
                            unCompleteRule = "完成居住地";
                            break;

                          case 6:
                            unCompleteRule = "完成家乡";
                            break;

                          case 7:
                            unCompleteRule = "完成手机";
                            break;

                          case 8:
                            unCompleteRule = "完成头像上传";
                            break;
                        }
                        data.unCompleteRule = unCompleteRule;
                        data.unCompleteRuleRate = "+10%";
                    }
                }
            }
            if (userInfoRate == 100) {
                data.unCompleteRule = "";
                data.unCompleteRuleRate = "";
            }
            data.userInfoRate = userInfoRate;
        }
    });
    module.exports = BasicInfoView;
});