define("modules/2.1.0/user/account/info/views/SecuritySettingView-debug", [ "$-debug", "underscore-debug", "backbone-debug", "modules/2.1.0/user/account/info/models/UserModel-debug" ], function(require, exports, module) {
    this._ = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    this.UserCenterCollection = require("modules/2.1.0/user/account/info/models/UserModel-debug");
    var SecuritySettingView = Backbone.View.extend({
        initialize: function(opt) {
            this.el = opt.el;
            this.data = new UserCenterCollection.UserModel({
                type: 1
            }).toJSON();
            this.render();
        },
        render: function() {
            var self = this;
            require.async("modules/2.1.0/user/account/info/tpl/security-item.html", function(html) {
                self.template = _.template(html);
                self.$el.html(self.template(self.data));
            });
        }
    });
    module.exports = SecuritySettingView;
});