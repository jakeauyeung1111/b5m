define("modules/2.0.0/user/trade/bangbi/pay/views/AlipayView-debug", [ "$-debug", "underscore-debug", "backbone-debug", "modules/2.0.0/user/trade/bangbi/pay/tpl/alipay-form-debug.html", "modules/2.0.0/user/trade/bangbi/pay/tpl/pay-dialog-debug.html", "modules/2.0.0/user/user/models/UserModel-debug" ], function(require, exports, module) {
    this.COMMON = require("common-debug");
    this.underscore = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    this.UserCenterCollection = require("modules/2.0.0/user/user/models/UserModel-debug");
    var AlipayView = Backbone.View.extend({
        tagName: "form",
        events: {
            "click #alipaySubmit": "userPay",
            "change #amount": "selectBb",
            "change #dikou": "dikou"
        },
        initialize: function(opt) {
            this.el = opt.el;
            this.render();
        },
        render: function() {
            //加载模版
            this.template = underscore.template(require("modules/2.0.0/user/trade/bangbi/pay/tpl/alipay-form-debug.html"));
            var data = new UserCenterCollection.UserModel().toJSON();
            this.$el.html(this.template(data));
        },
        userPay: function(e) {
            //提示框
            var diaLogHtml = underscore.template(require("modules/2.0.0/user/trade/bangbi/pay/tpl/pay-dialog-debug.html"))(this.model);
            COMMON.showSimpleDialog(diaLogHtml, "充值状态确认");
            //form 表单提交
            $(e.currentTarget).parents("form:first").submit();
        },
        selectBb: function(e) {
            var chargeMoney = $(e.currentTarget).val();
            var dikouBangbi = chargeMoney * .2;
            var dikouBean = dikouBangbi * 105;
            $("#totalMoney").text(chargeMoney);
            $("#dikou").attr("checked", false);
            $("#canDikouBean").text(dikouBean);
            $("#lastChargeMoney").text(chargeMoney);
        },
        dikou: function(e) {
            var chargeMoney = $("#amount").val();
            //直接充值
            if ($(e.currentTarget).attr("checked") == undefined) {
                $("#lastChargeMoney").text(chargeMoney);
                $("#payType").val(1);
                return;
            }
            //充值加抵扣
            var restBean = $("#restBean").val();
            var dikouBangbi = chargeMoney * .2;
            var dikouBean = dikouBangbi * 105;
            if (parseInt(restBean) < parseInt(dikouBean)) {
                COMMON.showTip("您的剩余帮豆不够,不能参加抵扣！");
                $(e.currentTarget).attr("checked", false);
                return;
            }
            var lastChargeMoney = parseInt(chargeMoney) - parseInt(dikouBangbi);
            $("#lastChargeMoney").text(lastChargeMoney);
            $("#payType").val(2);
        }
    });
    module.exports = AlipayView;
});