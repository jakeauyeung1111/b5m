/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/2.1.0/user/trade/korea/views/OrderView",["$","underscore","backbone","common","modules/2.1.0/user/trade/korea/models/OrderCollection","modules/2.1.0/user/trade/korea/views/OrderItemView","modules/default/common/Page"],function(e,t,i){this._=e("underscore"),this.Backbone=e("backbone"),this.COMMON=e("common"),this.OrderCollection=e("modules/2.1.0/user/trade/korea/models/OrderCollection"),this.OrderItemView=e("modules/2.1.0/user/trade/korea/views/OrderItemView"),this.Page=e("modules/default/common/Page");var o={Views:{},Urls:{}};o.Views.OrderView=Backbone.View.extend({initialize:function(t){this.parameterData=this.initParameter(t),this.el=t.el;var i=this;e.async("modules/2.1.0/user/trade/korea/tpl/order.html",function(e){i.template=_.template(e),i.$el.append(i.template()),i.orderCollection=new OrderCollection,i.pageHandler({pageNum:1})})},initParameter:function(e){return _.extend({pageNum:1,pageSize:10,showPage:!1},e)},render:function(){this.orderCollection.each(function(e,t){this.renderData(e,t)},this)},renderData:function(e,t){var i="";0!=t%2&&(i="trEvenBg");var o=new OrderItemView({model:e,attributes:{"class":i},tplName:this.parameterData.tplName});this.$el.find("tbody").append(o.el)},pageHandler:function(e){var t=this;this.orderCollection.fetch({data:{pageNum:e.pageNum,pageSize:t.parameterData.pageSize},success:function(){return t.hideLodingData(),0==t.orderCollection.length?(t.$el.find(".ui-empty-data").show(),void 0):(t.render(),t.orderCollection.length>0&&t.refreshPage(e),void 0)}})},refreshPage:function(e){if(!this.page){var t=this;this.page=new Page({onClick:function(e){t.pageHandler({pageNum:e})}})}this.$el.find(".page-view").empty().append(this.page.getPageDom({pageNum:e.pageNum,total:this.orderCollection.dataTotal,pageSize:this.parameterData.pageSize}))},showLoadingData:function(){this.$el.find(".ui-loading").show(),this.$el.find(".page-view").empty(),this.$el.find("tbody").empty(),this.$el.find(".ui-empty-data").hide()},hideLodingData:function(){this.$el.find(".ui-loading").hide(),this.$el.find(".page-view").empty(),this.$el.find("tbody").empty(),this.$el.find(".ui-empty-data").hide()}}),i.exports=o.Views.OrderView});