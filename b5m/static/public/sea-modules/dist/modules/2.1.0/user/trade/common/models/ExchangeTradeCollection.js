/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/2.1.0/user/trade/common/models/ExchangeTradeCollection",["$","underscore","backbone","common"],function(e,t,i){this._=e("underscore"),this.Backbone=e("backbone"),this.COMMON=e("common");var o={Models:{},Collections:{},Urls:{}},a=B5M_UC.rootPath;o.Urls.categorysUrl=a+"/dh/category.do",o.Urls.categoryDetailsUrl=a+"/dh/order.do",o.Collections.categorys=Backbone.Collection.extend({url:o.Urls.categorysUrl,sync:function(e,t,i){var o=_.extend({async:!1,cache:!1,dataType:"jsonp",jsonp:"jsonpCallback",type:"POST",url:t.url,processData:!0},i);return $.ajax(o)},parse:function(e){return e.ok?e.data:e},initialize:function(){}}),o.Collections.categoryDetails=Backbone.Collection.extend({url:o.Urls.categoryDetailsUrl,sync:function(e,t,i){var o=_.extend({async:!1,cache:!1,dataType:"jsonp",jsonp:"jsonpCallback",type:"POST",url:t.url,processData:!0},i);return $.ajax(o)},parse:function(e){return e.ok?this.initData(e.data):e},initData:function(e){return _.each(e.datas,function(e){if(COMMON.isEmptyValue(e.edate)||(e.edate=COMMON.formatTime(e.edate)),!COMMON.isEmptyValue(e.userAddress))try{var t=JSON.parse(e.userAddress);e.couponNo=t.couponNo,e.couponPwd=t.couponPwd}catch(i){}if(!COMMON.isEmptyValue(e.couponPrice))if(e.couponType=1,$.contains(e.couponPrice,"-")){e.couponType=2;var o=e.couponPrice.split("-");e.fullPrice=o[0],e.cutPrice=o[1]}else e.cutPrice=e.couponPrice}),e},initialize:function(){}}),i.exports.categorys=o.Collections.categorys,i.exports.categoryDetails=o.Collections.categoryDetails});