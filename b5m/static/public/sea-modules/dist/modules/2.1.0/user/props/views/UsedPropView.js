/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/2.0.0/user/props/views/UsedPropView",["$","underscore","backbone","common","modules/2.0.0/user/props/tpl/used-prop-list.html","modules/2.0.0/user/props/models/UserPropModel"],function(e,t,i){this._=e("underscore"),this.Backbone=e("backbone"),this.COMMON=e("common");var o=e("modules/2.0.0/user/props/models/UserPropModel"),a=e("modules/2.0.0/user/props/tpl/used-prop-list.html"),s='<div class="favorites-none" style="display: block;"><span>暂无使用道具记录</span></div>',n=Backbone.View.extend({initialize:function(e){this.el=e.el,this.render()},render:function(){void 0===this.template&&(this.template=_.template(a)),this.userPropModel=new o({type:"used"});try{var e=this.userPropModel.get("used").list;if(COMMON.isEmptyValue(e))return this.$el.html(s),void 0;var t=this.template({propList:e});this.$el.html(t)}catch(i){return this.$el.html(s),void 0}this.resetTotalCount()},resetTotalCount:function(){var e=this.userPropModel.get("used").totalCount;COMMON.isEmptyValue(e)||$("#used-props").html("("+e+")")}});i.exports=n});