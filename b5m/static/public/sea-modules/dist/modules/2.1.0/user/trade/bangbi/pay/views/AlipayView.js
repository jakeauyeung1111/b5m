/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/2.0.0/user/trade/bangbi/pay/views/AlipayView",["$","underscore","backbone","modules/2.0.0/user/trade/bangbi/pay/tpl/alipay-form.html","modules/2.0.0/user/trade/bangbi/pay/tpl/pay-dialog.html","modules/2.0.0/user/user/models/UserModel"],function(e,t,i){this.COMMON=e("common"),this.underscore=e("underscore"),this.Backbone=e("backbone"),this.UserCenterCollection=e("modules/2.0.0/user/user/models/UserModel");var o=Backbone.View.extend({tagName:"form",events:{"click #alipaySubmit":"userPay","change #amount":"selectBb","change #dikou":"dikou"},initialize:function(e){this.el=e.el,this.render()},render:function(){this.template=underscore.template(e("modules/2.0.0/user/trade/bangbi/pay/tpl/alipay-form.html"));var t=(new UserCenterCollection.UserModel).toJSON();this.$el.html(this.template(t))},userPay:function(t){var i=underscore.template(e("modules/2.0.0/user/trade/bangbi/pay/tpl/pay-dialog.html"))(this.model);COMMON.showSimpleDialog(i,"充值状态确认"),$(t.currentTarget).parents("form:first").submit()},selectBb:function(e){var t=$(e.currentTarget).val(),i=.2*t,o=105*i;$("#totalMoney").text(t),$("#dikou").attr("checked",!1),$("#canDikouBean").text(o),$("#lastChargeMoney").text(t)},dikou:function(e){var t=$("#amount").val();if(void 0==$(e.currentTarget).attr("checked"))return $("#lastChargeMoney").text(t),$("#payType").val(1),void 0;var i=$("#restBean").val(),o=.2*t,a=105*o;if(parseInt(i)<parseInt(a))return COMMON.showTip("您的剩余帮豆不够,不能参加抵扣！"),$(e.currentTarget).attr("checked",!1),void 0;var s=parseInt(t)-parseInt(o);$("#lastChargeMoney").text(s),$("#payType").val(2)}});i.exports=o});