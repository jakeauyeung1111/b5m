define("modules/2.0.0/user/user/views/info/BindUserNameView-debug", [ "$-debug", "underscore-debug", "backbone-debug", "common-debug", "modules/2.0.0/user/user/tpl/info/bind-user-name-debug.html", "jquery/validate/1.11.1/validate-debug" ], function(require, exports, module) {
    this.underscore = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    this.COMMON = require("common-debug");
    var BindUserNameView = Backbone.View.extend({
        tagName: "form",
        initialize: function(opt) {
            this.el = opt.el;
            this.render();
            //初始化验证
            this.initValidateForm();
        },
        render: function() {
            //加载模版
            this.template = underscore.template(require("modules/2.0.0/user/user/tpl/info/bind-user-name-debug.html"));
            this.$el.html(this.template);
        },
        initValidateForm: function() {
            var $bindNameForm = $("#bindNameForm");
            jQuery.validator.addMethod("isUserNameUse", function(value, element) {
                var code = 0;
                $.ajax({
                    type: "post",
                    data: "userName=" + value,
                    async: false,
                    url: B5M_UC.rootPath + "/user/info/data/isNameOrEmailUse.do",
                    success: function(r) {
                        code = eval(r).code;
                    }
                });
                return code == 0 ? true : false;
            }, "用户名已使用，请更换用户名");
            jQuery.validator.addMethod("isUserNameOK", function(value, element) {
                return /^[a-zA-Z_0-9\u4e00-\u9fa5]+/.test(value);
            }, "用户名格式不正确,含有非法字符");
            jQuery.validator.addMethod("isUserNameLengthOK", function(value, element) {
                var unlen = value.replace(/[^\x00-\xff]/g, "**").length;
                if (unlen < 4 || unlen > 15) {
                    return false;
                }
                return true;
            }, "4到15个字符，一个汉字为两个字符");
            $bindNameForm.validate({
                rules: {
                    userName: {
                        required: true,
                        isUserNameOK: true,
                        isUserNameLengthOK: true,
                        isUserNameUse: true
                    }
                },
                messages: {
                    userName: {
                        required: "用户名不能为空"
                    }
                },
                errorElement: "span",
                errorClass: "text_error",
                errorPlacement: function(error, element) {
                    //$('#serror').empty();
                    //element.attr('class','text text_error');
                    var errorBox = element.parent().parent().find(".cl-999").empty();
                    errorBox.append(error.html());
                },
                success: function(label, d) {
                    var parent = $(d).parent().parent().find(".cl-999").empty();
                },
                onclick: false
            });
            $('input[name="code"]').keydown(function(event) {
                if (event.keyCode == 13) submitHandler();
            });
            $("#bind_name_submit").click(function() {
                if ($bindNameForm.valid()) {
                    $bindNameForm.submit();
                }
            });
        }
    });
    module.exports = BindUserNameView;
});