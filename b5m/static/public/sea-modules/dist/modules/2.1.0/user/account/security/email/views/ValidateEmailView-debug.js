define("modules/2.1.0/user/account/security/email/views/ValidateEmailView-debug", [ "$-debug", "common-debug", "underscore-debug", "backbone-debug", "jquery/validate/1.11.1/validate-debug", "modules/2.1.0/user/account/info/models/UserModel-debug" ], function(require, exports, module) {
    this.COMMON = require("common-debug");
    this._ = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    this.UserCenterCollection = require("modules/2.1.0/user/account/info/models/UserModel-debug");
    var ValidateEmailView = Backbone.View.extend({
        tagName: "form",
        events: {
            "click #sendValidateEmail": "sendValidateEmail",
            "click #reSendEmail": "sendEmail"
        },
        initialize: function(opt) {
            this.el = opt.el;
            this.data = new UserCenterCollection.UserModel({
                type: 1
            }).toJSON();
            this.render();
        },
        render: function(type) {
            var self = this;
            var myTye = type;
            require.async([ "modules/2.1.0/user/account/security/email/tpl/validate-email.html", "modules/2.1.0/user/account/security/email/tpl/validate-email-2.html" ], function(html, html2) {
                var renderHtml = html;
                if (!COMMON.isEmptyValue(myTye) && myTye == "checkMailHome") {
                    renderHtml = html2;
                    if (COMMON.isEmptyValue(self.data.emailDomain)) self.buildEmailDomain(self.data);
                }
                self.template = _.template(renderHtml);
                self.$el.html(self.template(self.data));
                self.initValidateForm();
                self.keyPressEvent();
            });
        },
        buildEmailDomain: function(data) {
            var email = data.email;
            if (COMMON.isEmptyValue(email)) data.emailDomain = "";
            var indexOf = email.indexOf("@");
            var endStr = email.substring(indexOf + 1);
            if (endStr.indexOf("gmail") == 0) endStr = "google.com";
            data.emailDomain = "http://mail." + endStr;
        },
        sendValidateEmail: function(e) {
            if (this.validateForm.valid()) {
                this.sendEmail(e);
            }
        },
        sendEmail: function(e) {
            var self_this = this;
            $(e.currentTarget).wait("btn btn-1 waiting", "btn btn-1", 5e3);
            var emailValue = $("#email").val();
            $.post(B5M_UC.rootPath + "/user/info/data/verifyEmail.htm", "email=" + emailValue, function(response) {
                if (response.code == 1) {
                    self_this.data.emailDomain = response.data;
                    self_this.data.email = emailValue;
                    self_this.render("checkMailHome");
                } else {
                    COMMON.showTip("操作失败");
                }
            });
        },
        initValidateForm: function() {
            //初始化验证
            var $validateEmailForm = $("#validateEmailForm");
            //自定义异步验证验证
            jQuery.validator.addMethod("isEmail", function(value, element) {
                var code = 0;
                $.ajax({
                    type: "post",
                    data: "email=" + value,
                    async: false,
                    url: B5M_UC.rootPath + "/user/info/data/isNameOrEmailUse.do",
                    success: function(r) {
                        code = eval(r).code;
                    }
                });
                return code == 0 ? true : false;
            }, "邮箱已经被占用，请更换邮箱");
            $validateEmailForm.validate({
                rules: {
                    email: {
                        required: true,
                        email: true,
                        isEmail: true
                    }
                },
                messages: {
                    email: {
                        required: "Email不能为空",
                        email: "邮箱地址格式不正确"
                    }
                },
                errorElement: "span",
                errorClass: "text_error",
                errorPlacement: function(error, element) {
                    $("#serror").empty().hide();
                    element.attr("class", "text text_error");
                    var errorBox = element.parent().parent().find(".errorBox").empty();
                    errorBox.append('<img class="ui-icon ui-icon-error" src="' + B5M_UC.rootPath + '/images/ucenter/blank.png" />&nbsp;' + error.html());
                },
                success: function(label, d) {
                    var parent = $(d).parent().parent().find(".errorBox").empty();
                    if ($(d).attr("name") != "code") parent.append('<img class="ui-icon ui-icon-right" src="' + B5M_UC.rootPath + '/images/ucenter/blank.png">');
                },
                onclick: false
            });
            this.validateForm = $validateEmailForm;
        },
        keyPressEvent: function() {
            $("#email").keydown(function(event) {
                if (event.keyCode == 13 || event.which == 13) {
                    return false;
                }
                return true;
            });
        }
    });
    module.exports = ValidateEmailView;
});