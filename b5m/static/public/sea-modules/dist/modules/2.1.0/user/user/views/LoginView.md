# 用户登录框

- pubdate: 2012-12-12 12:00
- author:zhangshun

------

##默认用法
````html
<div id="LoginView" style="width: 275px;">
	<div class="ui-loading">正在加载...</div>
</div>
````
````javascript
seajs.use(['modules/2.0.0/user/user/views/LoginView'],function(LoginView){		
        new LoginView({
                el:'#LoginView',
                referer:'http%3A%2F%2Fwww.b5m.com',		//回跳地址
                email:'',					//初始化帐户名(会在帐户名文本框中显示)
                msg:'',						//错误提示信息（默认不填）
                userMps:'organic'				//来源渠道
        });
});
````

##登录成功回调处理用法
````html
<div id="LoginView-2" style="width: 275px;">
	<div class="ui-loading">正在加载...</div>
</div>
````
````javascript
seajs.use(['modules/2.0.0/user/user/views/LoginView'],function(LoginView){		
        new LoginView({
                el:'#LoginView-2',
                referer:'http%3A%2F%2Fwww.b5m.com',		//回跳地址
                email:'',					//初始化帐户名(会在帐户名文本框中显示)
                msg:'',						//错误提示信息（默认不填）
                userMps:'organic',				//来源渠道
                loginSuccessHandler:function(data){	        //登录成功回调处理
                        alert('Baby你登录成功了：'+data);
                }
        });
});
````
