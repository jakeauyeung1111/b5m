define("modules/2.0.0/user/props/views/UserPropsUseDialogView-debug", [ "$-debug", "underscore-debug", "modules/default/common/ModalDialogView-debug", "common-debug", "modules/2.0.0/user/props/tpl/user-prop-dialog-debug.html" ], function(require, exports, module) {
    this._ = require("underscore-debug");
    this.COMMON = require("common-debug");
    var ModalDialogView = require("modules/default/common/ModalDialogView-debug");
    var UserPropsUseDialogView = ModalDialogView.extend({
        initialize: function(opt) {
            this.beanCount = opt.beanCount;
        },
        events: {
            "click #useMyProp": "useMyProp",
            "click .close": "close"
        },
        render: function() {
            var self_this = this;
            require.async([ "modules/2.0.0/user/props/tpl/user-prop-dialog.html" ], function(userPropDialogHtml) {
                self_this.showMyPropsDialog(userPropDialogHtml);
            });
        },
        showMyPropsDialog: function(userPropDialogHtml) {
            this.template = _.template(userPropDialogHtml);
            this.$el.html(this.template({
                beanCount: this.beanCount
            }));
            this.showModal();
        },
        useMyProp: function(e) {
            e.preventDefault();
            var self_this = this;
            $.post(B5M_UC.rootPath + "/props/info/data/useMyProp.htm", this.data, function(response) {
                var res = eval(response);
                if (res.ok) {
                    window.location.reload();
                } else {
                    self_this.showTip(res.data);
                }
            });
        },
        close: function() {
            this.hideModal();
        }
    });
    module.exports = UserPropsUseDialogView;
});