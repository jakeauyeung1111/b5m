/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/2.1.0/user/trade/common/views/TradeBatchItemView",["$","underscore","backbone","common"],function(e,t,i){this._=e("underscore"),this.Backbone=e("backbone"),this.COMMON=e("common"),this.tpl_trade_batch_item="modules/2.1.0/user/trade/common/tpl/trade-batch-item.html";var o={Views:{}};o.Views.TradeBatchItemView=Backbone.View.extend({tagName:"tr",initialize:function(){var t=this;e.async(tpl_trade_batch_item,function(i){t.template=_.template(i),t.$el.html(t.template(t.model.toJSON())),t.render(),t.model.get("groupName")&&"ec"==t.model.get("groupName").toLocaleLowerCase()&&"21"==t.model.get("orderCode")&&e.async("modules/2.1.0/user/trade/common/views/TradeBatchDetailItemView",function(e){var i=new e({model:t.model}),o=t.$el.find("td:last").html();t.$el.find("td:last").empty().append('<a class="view-success singleIco" href="javascript:void(0);">'+o+'<u class="singleIco-u"></u></a>'),t.$el.append(i.$el)})})},events:{"click .view-success":function(e){var t=$(e.currentTarget),i=t.closest("tr");(!i.next().attr("class")||0>i.next().attr("class").indexOf("view-success-panel"))&&i.after(i.find(".view-success-panel"));var o=i.next();o.attr("class").indexOf("show")>=0?(t.attr("class","view-success singleIco"),o.attr("class","view-success-panel")):(t.attr("class","view-success singleIco on"),o.attr("class","view-success-panel show"))}}}),i.exports=o.Views.TradeBatchItemView});