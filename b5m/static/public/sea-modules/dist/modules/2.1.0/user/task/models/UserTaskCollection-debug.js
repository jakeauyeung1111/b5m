define("modules/2.1.0/user/task/models/UserTaskCollection-debug", [ "$-debug", "underscore-debug", "backbone-debug", "common-debug" ], function(require, exports, module) {
    this._ = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    this.COMMON = require("common-debug");
    var UserTaskCollection = {
        Models: {},
        Collections: {},
        Urls: {}
    };
    UserTaskCollection.Urls.TaskList = B5M_UC.rootPath + "/user/task/data/list.htm";
    UserTaskCollection.Models.UserTask = Backbone.Model.extend({});
    UserTaskCollection.Collections.UserTaskCollection = Backbone.Collection.extend({
        url: UserTaskCollection.Urls.TaskList,
        model: UserTaskCollection.Models.UserTask,
        sync: function(method, model, options) {
            var params = _.extend({
                async: false,
                cache: false,
                dataType: "jsonp",
                jsonp: "jsonpCallback",
                // the api requires the jsonp callback name to be this exact name
                type: "POST",
                url: model.url,
                processData: true
            }, options);
            return $.ajax(params);
        },
        parse: function(response) {
            COMMON.checkJSonData(response);
            if (response.ok) return this.initModelData(response.data);
            COMMON.showTip(response.data);
            return null;
        },
        initModelData: function(data) {
            for (var i = 0; i < data.length; i++) {
                if (!this.stringStartWithString("http:", data[i].rulesImg)) data[i].rulesImg = B5M_UC.rootPath + data[i].rulesImg;
                if (data[i].useIs) {
                    data[i].actionName = "已" + data[i].actionName;
                    data[i].actionUrl = "javascript:void(0);";
                } else {
                    data[i].actionName = "去" + data[i].actionName;
                    if (!this.stringStartWithString("http:", data[i].actionUrl)) data[i].actionUrl = B5M_UC.rootPath + data[i].actionUrl;
                }
            }
            return data;
        },
        stringStartWithString: function(startWithStr, str) {
            return $.trim(str).indexOf(startWithStr) == 0 ? true : false;
        },
        comparator: function(o1, o2) {
            return o1.get("rulesSort") > o2.get("rulesSort") ? true : false;
        }
    });
    module.exports.UserTaskCollection = UserTaskCollection.Collections.UserTaskCollection;
});