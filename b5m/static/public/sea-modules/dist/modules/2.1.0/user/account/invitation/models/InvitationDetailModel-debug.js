define("modules/2.1.0/user/account/invitation/models/InvitationDetailModel-debug", [ "$-debug", "underscore-debug", "backbone-debug", "common-debug" ], function(require, exports, module) {
    this._ = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    this.COMMON = require("common-debug");
    var InvitationDetailModel = {
        Models: {},
        Collections: {},
        Urls: {}
    };
    InvitationDetailModel.Urls.detail = B5M_UC.rootPath + "/user/invitation/data/detail.htm";
    InvitationDetailModel.Models.Trade = Backbone.Model.extend({});
    InvitationDetailModel.Collections.InvitationDetailModel = Backbone.Model.extend({
        url: InvitationDetailModel.Urls.detail,
        sync: function(method, model, options) {
            var params = _.extend({
                //				async:false,
                cache: false,
                dataType: "jsonp",
                jsonp: "jsonpCallback",
                // the api requires the jsonp callback name to be this exact name
                type: "POST",
                url: model.url,
                processData: true
            }, options);
            return $.ajax(params);
        },
        parse: function(response) {
            COMMON.checkJSonData(response);
            if (response.ok) return this.initModelData(response.data);
            COMMON.showTip(response.data);
            return null;
        },
        initModelData: function(data) {
            return data;
        }
    });
    module.exports = InvitationDetailModel.Collections.InvitationDetailModel;
});