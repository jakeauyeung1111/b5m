/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/2.0.0/user/trade/bangbi/exchange/views/ExchangeViews",["$","underscore","backbone","common","modules/2.0.0/user/trade/bangbi/exchange/tpl/exchange.html","modules/2.0.0/user/trade/bangbi/exchange/views/ExchangeConfirmViews","modules/2.0.0/user/user/models/UserModel"],function(e,t,i){this.COMMON=e("common"),this.underscore=e("underscore"),this.Backbone=e("backbone"),this.UserCenterCollection=e("modules/2.0.0/user/user/models/UserModel"),this.ExchangeConfirmViews=e("modules/2.0.0/user/trade/bangbi/exchange/views/ExchangeConfirmViews");var o=Backbone.View.extend({tagName:"form",events:{"change #selectBangbi":"selectBangbi","click #showConfirmDialog":"showConfirmDialog"},initialize:function(e){this.el=e.el,this.render()},render:function(){this.template=underscore.template(e("modules/2.0.0/user/trade/bangbi/exchange/tpl/exchange.html"));var t=(new UserCenterCollection.UserModel).toJSON();this.$el.html(this.template(t))},selectBangbi:function(){var e=$("#selectBangbi").val(),t=$("#restBangbi").text();return parseInt(t)<parseInt(e)?(COMMON.showTip("您的剩余帮币数量不够！","帮豆兑换帮币"),$("#exchargeBeanAmout").val(""),!1):($("#exchargeBeanAmout").val(100*e),void 0)},showConfirmDialog:function(){if(this.selectBangbi(),!COMMON.isEmptyValue($("#exchargeBeanAmout").val())){var e={exchangeBangbi:$("#selectBangbi").val(),exchangeBean:$("#exchargeBeanAmout").val()},t=new ExchangeConfirmViews({model:e});COMMON.showSimpleDialog(t.el,"帮币兑换帮豆确认")}}});i.exports=o});