define("modules/2.1.0/user/trade/korea/views/OrderItemView-debug", [ "$-debug", "underscore-debug", "backbone-debug", "common-debug", "modules/2.1.0/user/trade/korea/tpl/order-item-debug.html" ], function(require, exports, module) {
    this._ = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    this.COMMON = require("common-debug");
    this.tpl = require("modules/2.1.0/user/trade/korea/tpl/order-item-debug.html");
    var OrderItemView = {
        Views: {}
    };
    OrderItemView.Views.OrderItemView = Backbone.View.extend({
        template: _.template(tpl),
        tagName: "tr",
        initialize: function(opt) {
            this.$el.html(this.template(this.model.toJSON()));
            this.render();
        }
    });
    module.exports = OrderItemView.Views.OrderItemView;
});