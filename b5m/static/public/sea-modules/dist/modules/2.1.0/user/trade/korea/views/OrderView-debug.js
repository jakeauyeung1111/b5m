define("modules/2.1.0/user/trade/korea/views/OrderView-debug", [ "$-debug", "underscore-debug", "backbone-debug", "common-debug", "modules/2.1.0/user/trade/korea/models/OrderCollection-debug", "modules/2.1.0/user/trade/korea/views/OrderItemView-debug", "modules/default/common/Page-debug" ], function(require, exports, module) {
    this._ = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    this.COMMON = require("common-debug");
    this.OrderCollection = require("modules/2.1.0/user/trade/korea/models/OrderCollection-debug");
    this.OrderItemView = require("modules/2.1.0/user/trade/korea/views/OrderItemView-debug");
    this.Page = require("modules/default/common/Page-debug");
    var OrderView = {
        Views: {},
        Urls: {}
    };
    OrderView.Views.OrderView = Backbone.View.extend({
        initialize: function(opt) {
            //初始化参数
            this.parameterData = this.initParameter(opt);
            this.el = opt.el;
            var self = this;
            require.async("modules/2.1.0/user/trade/korea/tpl/order.html", function(tpl) {
                self.template = _.template(tpl);
                self.$el.append(self.template());
                //初始化交易记录
                self.orderCollection = new OrderCollection();
                self.pageHandler({
                    pageNum: 1
                });
            });
        },
        initParameter: function(para) {
            return _.extend({
                pageNum: 1,
                pageSize: 10,
                showPage: false
            }, para);
        },
        render: function() {
            this.orderCollection.each(function(item, index) {
                this.renderData(item, index);
            }, this);
        },
        renderData: function(item, index) {
            var itemClass = "";
            if (index % 2 != 0) itemClass = "trEvenBg";
            var orderItemView = new OrderItemView({
                model: item,
                attributes: {
                    "class": itemClass
                },
                tplName: this.parameterData.tplName
            });
            this.$el.find("tbody").append(orderItemView.el);
        },
        pageHandler: function(opt) {
            //加载交易记录
            var self = this;
            this.orderCollection.fetch({
                data: {
                    pageNum: opt.pageNum,
                    pageSize: self.parameterData.pageSize
                },
                success: function() {
                    self.hideLodingData();
                    //数据为空时处理
                    if (self.orderCollection.length == 0) {
                        self.$el.find(".ui-empty-data").show();
                        return;
                    }
                    //渲染交易记录数据
                    self.render();
                    //刷新分页控件
                    if (self.orderCollection.length > 0) self.refreshPage(opt);
                }
            });
        },
        refreshPage: function(opt) {
            //初始化分页控件
            if (!this.page) {
                var self = this;
                this.page = new Page({
                    onClick: function(pageNum, dom) {
                        self.pageHandler({
                            pageNum: pageNum
                        });
                    }
                });
            }
            this.$el.find(".page-view").empty().append(this.page.getPageDom({
                pageNum: opt.pageNum,
                total: this.orderCollection.dataTotal,
                pageSize: this.parameterData.pageSize
            }));
        },
        showLoadingData: function() {
            this.$el.find(".ui-loading").show();
            this.$el.find(".page-view").empty();
            this.$el.find("tbody").empty();
            this.$el.find(".ui-empty-data").hide();
        },
        hideLodingData: function() {
            this.$el.find(".ui-loading").hide();
            this.$el.find(".page-view").empty();
            this.$el.find("tbody").empty();
            this.$el.find(".ui-empty-data").hide();
        }
    });
    module.exports = OrderView.Views.OrderView;
});