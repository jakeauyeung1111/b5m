define("modules/2.1.0/ads/views/AdsView-debug", [ "$-debug", "common-debug", "underscore-debug", "backbone-debug", "modules/2.1.0/ads/models/AdsCollection-debug" ], function(require, exports, module) {
    this._ = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    this.COMMON = require("common-debug");
    this.AdsCollection = require("modules/2.1.0/ads/models/AdsCollection-debug");
    var AdsView = Backbone.View.extend({
        initialize: function() {
            this.adsCollection = new AdsCollection({
                position: this.options.position
            });
            var self_this = this;
            this.adsCollection.fetch({
                success: function() {
                    self_this.render();
                },
                error: function(e) {
                    console.log(e);
                }
            });
        },
        render: function() {
            var adsCollection = this.adsCollection.toJSON();
            var defaultHtml = "modules/2.1.0/ads/tpl/ad-postion-1.html";
            if (!COMMON.isEmptyValue(this.options.type)) {
                if (this.options.type == 2) defaultHtml = "modules/2.1.0/ads/tpl/ad-postion-2-3.html";
            }
            var self_this = this;
            require.async(defaultHtml, function(html) {
                self_this.template = _.template(html);
                if (COMMON.isEmptyValue(adsCollection)) {
                    return;
                }
                $(".ui-loading").hide();
                var htmlDom = self_this.template({
                    adsCollection: adsCollection
                });
                self_this.$el.html(htmlDom);
            });
        }
    });
    module.exports = AdsView;
});