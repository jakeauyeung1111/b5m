/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/2.1.0/ads/models/AdsCollection",["$","backbone","underscore","common"],function(e,t,i){this._=e("underscore"),this.Backbone=e("backbone"),this.COMMON=e("common");var o={Models:{},Collections:{},Urls:{}};o.Urls.AdsUrl="http://sa1.b5m.com:58080/B5MCMS/ucenter/getImgs.do",o.Models.Ad=Backbone.Model.extend({url:o.Urls.AdsUrl,sync:function(e,t,i){var o=_.extend({async:!1,cache:!1,dataType:"jsonp",jsonp:"jsonpCallback",type:"POST",url:t.url,processData:!0},i);return $.ajax(o)},parse:function(e){return e.ok?e.data:e},initialize:function(e){var t=COMMON.isEmptyValue(e)?1:e.position;this.url=o.Urls.AdsUrl+"?position="+t}}),i.exports=o.Models.Ad});