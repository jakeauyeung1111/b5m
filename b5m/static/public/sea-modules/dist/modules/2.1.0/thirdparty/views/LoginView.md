# 第三方用户登录框

- pubdate: 2012-12-12 12:00
- author:zhangshun

------

````html
<div id="ThirdPartyLoginView-1">
	<div class="ui-loading">正在加载...</div>
</div>
````
````javascript
seajs.use('modules/2.1.0/goods/views/TejiaViews',function(TejiaViews){
	seajs.use(['modules/2.0.0/thirdparty/views/LoginView'],function(LoginView){		
		new LoginView({
			el:'#ThirdPartyLoginView-1',
			referer:'http%3A%2F%2Fwww.b5m.com',		//回跳地址
			userMps:'organic'						//来源渠道
		});
	});
});
````