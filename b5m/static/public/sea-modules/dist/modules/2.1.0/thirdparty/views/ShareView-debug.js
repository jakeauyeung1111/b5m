define("modules/2.0.0/thirdparty/views/ShareView-debug", [ "$-debug", "underscore-debug", "backbone-debug", "common-debug", "modules/default/common/PopwindowShareView-debug", "http://staticcdn.b5m.com/css/common/popwindow-share-debug.css" ], function(require, exports, module) {
    this._ = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    this.COMMON = require("common-debug");
    this.popWindowShareFed = require("modules/default/common/PopwindowShareView-debug");
    var ShareView = {
        Views: {},
        Urls: {}
    };
    this.root = undefined;
    ShareView.Views.ShareView = Backbone.View.extend({
        initialize: function(opt) {
            //初始化参数
            this.parameterData = this.initParameter(opt);
            root = this;
            require.async("modules/2.0.0/thirdparty/tpl/share-" + this.parameterData.tpl + ".html", function(tpl) {
                root.template = _.template(tpl);
                root.render();
            });
        },
        initParameter: function(para) {
            return _.extend({
                title: "帮5买",
                content: "帮5买",
                tpl: 24
            }, para);
        },
        render: function() {
            this.$el.find(".ui-loading").hide();
            this.$el.html(this.template(this.parameterData));
            try {} catch (e) {
                COMMON.showTip("出错啦！！！赶紧去买六合彩吧！！！");
            }
        },
        events: {
            "click #qqzone,#sinamb,#qqmb,#qqwx": "shareItemHandler"
        },
        shareItemHandler: function(state) {
            var type = Number($(state.currentTarget).attr("attr-type"));
            switch (type) {
              case 1:
                this.sharePublic(state, type);
                break;

              case 2:
                this.sharePublic(state, type);
                break;

              case 3:
                this.shareWeiXin(state, type);
                break;
            }
        },
        shareWeiXin: function(state, type) {
            window.open(B5M_UC.rootPath + "/user/third/share/weixin.htm?url=" + (this.parameterData.url ? encodeURIComponent(this.parameterData.url) : "") + "&content=" + (this.parameterData.content ? encodeURIComponent(this.parameterData.content) : ""), "_blank");
        },
        sharePublic: function(state, type) {
            var sync = $(state.currentTarget).attr("attr-sync");
            $.getJSON(B5M_UC.rootPath + "/user/third/getToken.htm?jsonpCallback=?", {
                state: type
            }, function(result) {
                if (!result.ok) {
                    var newWindow = window.open();
                    setTimeout(function() {
                        newWindow.location = B5M_UC.rootPath + "/user/third/login/auth.htm?type=" + type + "&refererUrl=&api=/setToken";
                    }, 10);
                } else {
                    popWindowShareFed.popwindow({
                        title: root.parameterData.title,
                        content: root.parameterData.content,
                        href: root.parameterData.url,
                        type: type
                    }, function(res) {
                        var data = {
                            state: type,
                            summary: res["content"],
                            title: res["title"]
                        };
                        if (type == 1) {
                            data.url = res["href"];
                            if (sync) {
                                data.nswb = sync;
                            }
                        } else {
                            data.summary = data.summary + "  " + res["href"];
                        }
                        if (res["pic"]) {
                            data.images = res["pic"];
                        }
                        $.getJSON(B5M_UC.rootPath + "/user/third/share.htm?jsonpCallback=?", data, function(httpObj) {
                            if (httpObj.ok) {
                                popWindowShareFed.popwindowClose();
                                COMMON.showTip("分享成功");
                            } else {
                                COMMON.showTip("分享失败，请稍后分享");
                            }
                        });
                    });
                }
            });
        }
    });
    module.exports = ShareView.Views.ShareView;
});