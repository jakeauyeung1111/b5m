define("modules/2.0.0/user/account/favorites/views/FavoritesQueryView-debug", [ "$-debug", "underscore-debug", "backbone-debug", "common-debug", "modules/2.0.0/user/account/favorites/views/WebsiteView-debug", "modules/2.0.0/user/account/favorites/tpl/favorites-query-debug.html", "modules/2.0.0/user/account/favorites/views/FavoritesListView-debug" ], function(require, exports, module) {
    this._ = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    this.COMMON = require("common-debug");
    this.tpl_favorites_query = require("modules/2.0.0/user/account/favorites/tpl/favorites-query-debug.html");
    this.WebsiteView = require("modules/2.0.0/user/account/favorites/views/WebsiteView-debug");
    this.FavoritesListView = require("modules/2.0.0/user/account/favorites/views/FavoritesListView-debug");
    var FavoritesQueryView = {
        Views: {},
        Urls: {}
    };
    FavoritesQueryView.Views.FavoritesQueryView = Backbone.View.extend({
        template: _.template(tpl_favorites_query),
        initialize: function(opt) {
            this.websiteView = new WebsiteView();
            this.el = opt.el;
            this.render();
        },
        render: function() {
            //渲染模版
            this.$el.html(this.template());
            //渲染商家
            this.$el.find("#website-list").append(this.websiteView.$el.html());
            //渲染收藏数据
            this.renderFavoritesListView({
                type: 0,
                priceType: 0
            });
            return this;
        },
        events: {
            "mouseenter .layout-tab-tit > span": "tabFocusin",
            "click .webSite-But": "webSiteClick"
        },
        tabFocusin: function(e) {
            this.$el.find(".webSite-But").each(function(index) {
                if (index == 0) {
                    $(this).addClass("current");
                } else {
                    $(this).removeClass("current");
                }
            });
            var $targetDom = $(e.currentTarget), type = $targetDom.attr("attr-type");
            if (type == 0) {
                this.renderFavoritesListView({
                    type: type,
                    priceType: 0
                });
            } else if (type == 1) {
                this.renderFavoritesListView({
                    type: type,
                    priceType: 1
                });
            }
        },
        webSiteClick: function(e) {
            var $targetDom = $(e.currentTarget), code = $targetDom.attr("attr-code");
            //检测当前选中tab
            var _type = this.$el.find(".tabCur").attr("attr-type"), _priceType = undefined;
            if (_type == 1) _priceType = 1;
            this.renderFavoritesListView({
                type: _type,
                priceType: _priceType,
                webSite: code
            });
        },
        renderFavoritesListView: function(opt) {
            new FavoritesListView({
                $el: this.$el.find(".layout-tab-content:eq(" + opt.type + ")").empty(),
                priceType: opt.priceType,
                webSite: opt.webSite
            });
        }
    });
    module.exports = FavoritesQueryView.Views.FavoritesQueryView;
});