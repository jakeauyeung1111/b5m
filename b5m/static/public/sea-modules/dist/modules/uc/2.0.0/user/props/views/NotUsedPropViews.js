/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/2.0.0/user/props/views/NotUsedPropViews",["$","backbone","common","modules/2.0.0/user/props/views/NotUsedPropView","modules/2.0.0/user/props/models/UserPropModel"],function(e,t,i){this.Backbone=e("backbone"),this.COMMON=e("common");var o=e("modules/2.0.0/user/props/views/NotUsedPropView"),a=e("modules/2.0.0/user/props/models/UserPropModel"),n='<div class="favorites-none" style="display: block;"><span>未使用道具为空</span></div>',r=Backbone.View.extend({initialize:function(e){this.el=e.el,this.render()},render:function(){return this.$el.find(".ui-loading").hide(),this.userPropModel=new a({type:"unused"}),COMMON.isEmptyValue(this.userPropModel)?(this.$el.find("ul").append(n),void 0):(this.showNotUsedPropList(),this.resetTotalCount(),void 0)},showNotUsedPropList:function(){try{var e=this.userPropModel.get("unused").list;if(COMMON.isEmptyValue(e))return this.$el.find("ul").append(n),void 0;for(var t=0;e.length>t;t++){var i=new o({model:e[t]});this.$el.find("ul").append(i.el)}}catch(a){this.$el.find("ul").append(n)}},resetTotalCount:function(){var e=this.userPropModel.get("unused").totalCount;COMMON.isEmptyValue(e)||$("#not_use-props").html("("+e+")")}});i.exports=r});