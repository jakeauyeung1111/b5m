/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/2.0.0/user/account/favorites/models/FavoritesCollection",["$","underscore","backbone","common","modules/2.0.0/user/account/favorites/models/WebsiteCollection"],function(e,t,o){this._=e("underscore"),this.Backbone=e("backbone"),this.COMMON=e("common"),this.WebsiteCollection=e("modules/2.0.0/user/account/favorites/models/WebsiteCollection");var i={Models:{},Collections:{},Urls:{}};i.Urls.query=B5M_UC.rootPath+"/gc/user/favorites/data/query.htm",i.Models.Favorites=Backbone.Model.extend({}),i.Collections.FavoritesCollection=Backbone.Collection.extend({url:i.Urls.query,model:i.Models.Favorites,sync:function(e,t,o){var i=_.extend({async:!1,cache:!1,dataType:"jsonp",jsonp:"jsonpCallback",type:"POST",url:t.url,processData:!0},o);return $.ajax(i)},parse:function(e){return COMMON.checkJSonData(e),e.ok?this.initModelData(e.data):(COMMON.showTip(e.data),null)},initModelData:function(e){this.dataTotal=e.page.total,this.websiteCollection=new WebsiteCollection;for(var t=e.list,o=0;t.length>o;o++)t[o].webSiteName=this.websiteCollection.getNameByCode(t[o].webSite),t[o].favoritesTime=new Date(Date.parse(t[o].favoritesTime)).Format("yyyy-MM-dd");return t}}),o.exports=i.Collections.FavoritesCollection});