define("modules/2.0.0/user/user/models/LevelModel-debug", [ "$-debug", "underscore-debug", "backbone-debug", "common-debug" ], function(require, exports, module) {
    this._ = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    this.COMMON = require("common-debug");
    var LevelModel = {
        Models: {},
        Urls: {}
    };
    LevelModel.Urls.LevelModel = B5M_UC.rootPath + "/user/info/data/levelRule.htm";
    LevelModel.Models.LevelModel = Backbone.Model.extend({
        url: LevelModel.Urls.LevelModel,
        parse: function(response) {
            COMMON.checkJSonData(response);
            if (response.ok) {
                var data = response.data;
                for (var i = 0; i < data.length; i++) {
                    data[i].autoId = data[i].id;
                    delete data[i].id;
                }
                return data;
            }
            COMMON.showTip(response.data);
            return null;
        },
        initialize: function() {
            this.fetch({
                async: false,
                cache: false
            });
        }
    });
    module.exports.LevelModel = LevelModel.Models.LevelModel;
});