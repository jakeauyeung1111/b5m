define("modules/2.0.0/user/task/views/UserTaskItemView-debug", [ "$-debug", "underscore-debug", "backbone-debug", "common-debug" ], function(require, exports, module) {
    this._ = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    this.COMMON = require("common-debug");
    var tpl_task_item_1 = "modules/2.0.0/user/task/tpl/task-item-1.html";
    var tpl_task_item_2 = "modules/2.0.0/user/task/tpl/task-item-2.html";
    var UserTaskItemView = {
        View: {}
    };
    UserTaskItemView.View.UserTaskItemView = Backbone.View.extend({
        tagName: "tr",
        initialize: function(opt) {
            var self = this;
            var tpl_task_item = tpl_task_item_1;
            switch (opt.taskItemTpl) {
              case 1:
                tpl_task_item = tpl_task_item_1;
                break;

              case 2:
                tpl_task_item = tpl_task_item_2;
                break;
            }
            require.async(tpl_task_item, function(tpl) {
                self.template = _.template(tpl), self.$el.html(self.template(self.model.toJSON()));
                self.render();
            });
        }
    });
    module.exports.UserTaskItemView = UserTaskItemView.View.UserTaskItemView;
});