/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/2.0.0/user/user/views/info/RegisterView",["$","underscore","backbone","common","jquery/validate/1.11.1/validate","modules/2.0.0/user/user/tpl/info/register.html","modules/2.0.0/thirdparty/views/LoginView"],function(require,exports,module){this._=require("underscore"),this.Backbone=require("backbone"),this.COMMON=require("common"),this.ThirdPartyLoginView=require("modules/2.0.0/thirdparty/views/LoginView"),this.tpl_user_register=require("modules/2.0.0/user/user/tpl/info/register.html");var RegisterView={Views:{},Urls:{}};RegisterView.Views.RegisterView=Backbone.View.extend({template:_.template(tpl_user_register),initialize:function(e){this.opt=e,this.el=e.el,this.render(),this.initValidate()},render:function(){this.$el.find(".ui-loading").hide(),this.$el.html(this.template(this.opt)),this.thirdPartyLoginView=new ThirdPartyLoginView({redirectUrl:this.opt.url,userMps:this.opt.userMps,tpl:2}),this.$el.find(".platform").append(this.thirdPartyLoginView.$el)},events:{"click #refreshCode":"refreshCode","click #submit_btn":"submit_btn","keydown #valiCode":"valiCodeKeyDown"},refreshCode:function(e){e.preventDefault(),$("#code").attr("src",B5M_UC.rootPath+"/validateCode.do?f="+Math.random())},valiCodeKeyDown:function(e){13==e.keyCode&&this.submitHandler()},submit_btn:function(e){e.preventDefault(),this.submitHandler()},submitHandler:function(){this.validateForm.valid()&&this.validateForm.submit()},initValidate:function(){var $registerForm=$("#registerForm");jQuery.validator.addMethod("isEmail",function(value,element){var code=0;return $.ajax({type:"post",data:"email="+value,async:!1,url:B5M_UC.rootPath+"/user/info/data/isNameOrEmailUse.do",success:function(r){code=eval(r).code}}),0==code?!0:!1},"邮箱已经被占用，请更换邮箱"),jQuery.validator.addMethod("codeLength",function(e){return 4==e.length?!0:!1},"验证码为4位"),jQuery.validator.addMethod("isPasswordOK",function(e){return/^[a-zA-Z_0-9]{6,15}$/.test(e)},"请输入正确格式的字母，数字"),$registerForm.validate({rules:{email:{required:!0,email:!0,isEmail:!0},password:{required:!0,rangelength:[6,15],isPasswordOK:!0},confirmPassword:{required:!0,rangelength:[6,15],isPasswordOK:!0,equalTo:"#password"},code:{required:!0,codeLength:!0}},messages:{email:{required:"Email不能为空",email:"邮箱地址格式不正确"},password:{required:"密码不能为空",rangelength:"密码长度必须介于 {0} 和 {1} 之间"},confirmPassword:{required:"确认密码不能为空",rangelength:"密码长度必须介于 {0} 和 {1} 之间",equalTo:"两次输入的密码不一致"},code:{required:"验证码不能为空"}},errorElement:"span",errorClass:"text_error",errorPlacement:function(e,t){$("#serror").empty().hide(),t.attr("class","text text_error");var i=t.parent().parent().find(".errorBox").empty();i.append('<img class="ui-icon ui-icon-error" src="'+B5M_UC.rootPath+'/images/ucenter/blank.png" />&nbsp;'+e.html())},success:function(e,t){var i=$(t).parent().parent().find(".errorBox").empty();"code"!=$(t).attr("name")&&i.append('<img class="ui-icon ui-icon-right" src="'+B5M_UC.rootPath+'/images/ucenter/blank.png">')},onclick:!1}),this.validateForm=$registerForm;var $msgError=$("#serror");""!=$.trim($msgError.find("span").html())&&$msgError.show()}}),module.exports.RegisterView=RegisterView.Views.RegisterView});