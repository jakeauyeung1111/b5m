define("modules/2.0.0/user/user/views/LoginView-debug", [ "$-debug", "underscore-debug", "backbone-debug", "modules/2.0.0/user/user/tpl/login-debug.html", "jquery/validate/1.11.1/validate-debug" ], function(require, exports, module) {
    this._ = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    this.tpl_user_login = require("modules/2.0.0/user/user/tpl/login-debug.html");
    var LoginView = {
        Views: {},
        Urls: {
            LoginNum: B5M_UC.rootPath + "/user/user/data/loginNum.htm?jsonpCallback=?",
            Login: B5M_UC.rootPath + "/user/user/data/login.htm?jsonpCallback=?"
        }
    };
    this.root = undefined;
    LoginView.Views.LoginView = Backbone.View.extend({
        template: _.template(tpl_user_login),
        initialize: function(opt) {
            root = this;
            this.opt = opt;
            //渲染Dom
            this.render();
            //初始化验证Dom
            this.initValidate();
            //检测meail登录错误次数
            this.loginNum(this.$el.find('input[name="email"]').val());
        },
        render: function() {
            this.$el.find(".ui-loading").hide();
            this.$el.html(this.template(this.opt));
        },
        events: {
            "click #submit_btn": "submitHandler",
            "click #refreshCode": "refreshCodeHandler",
            'keydown input[name="password"],input[name="code"]': "submitHandler"
        },
        submitHandler: function(event) {
            if (event.type == "keydown" && event.keyCode != 13) return;
            if (this.$loginForm.valid()) {
                $.getJSON(LoginView.Urls.Login, this.$loginForm.serializeArray(), function(data) {
                    if (data.ok) {
                        if (root.opt && root.opt.loginSuccessHandler) {
                            root.opt.loginSuccessHandler(data.data);
                            return;
                        }
                        window.location.href = data.data.refererUrl;
                    } else {
                        root.$el.find(".login-chk").before('<div id="serror" class="login-field login-field-spe cfx"><div class="login-error"><i></i>' + data.data + "</div></div>");
                        root.refreshCodeHandler();
                        root.loginNum(root.$el.find('input[name="email"]').val());
                    }
                });
            }
        },
        loginNum: function(value) {
            if (!value && value.length <= 0) return;
            $.getJSON(LoginView.Urls.LoginNum, "email=" + value, function(data) {
                if (data.ok) {
                    root.$el.find(".rows-verify").css("display", "none");
                } else {
                    root.$el.find(".rows-verify").css("display", "block");
                }
            });
        },
        refreshCodeHandler: function() {
            $("#code").attr("src", B5M_UC.rootPath + "/validateCode.do?f=" + Math.random());
        },
        initValidate: function() {
            //初始化表单Dom
            this.$loginForm = this.$el.find("#loginForm");
            //定义验证规则
            jQuery.validator.addMethod("codeLength", function(value, element) {
                return value.length == 4 ? true : false;
            }, "验证码为4位");
            jQuery.validator.addMethod("loginNum", function(value, element) {
                root.loginNum(value);
                return true;
            }, "");
            //实例化验证对象
            this.$loginForm.validate({
                rules: {
                    email: {
                        required: true,
                        email: true,
                        loginNum: true
                    },
                    password: {
                        required: true,
                        rangelength: [ 6, 15 ]
                    },
                    code: {
                        required: true,
                        codeLength: true
                    }
                },
                messages: {
                    email: {
                        required: "Email不能为空",
                        email: "邮箱地址格式不正确"
                    },
                    password: {
                        required: "密码不能为空",
                        rangelength: "密码长度必须介于 {0} 和 {1} 之间"
                    },
                    code: {
                        required: "验证码不能为空"
                    }
                },
                errorElement: "div",
                errorClass: "text-error",
                errorPlacement: function(error, element) {
                    root.$el.find("#serror").remove();
                    var parent = element.parent();
                    parent.find('div[class="login-error"]').remove();
                    parent.append('<div class="login-error"><i></i>' + error.html() + "</div>");
                },
                success: function(label, d) {
                    $(d).parent().find('div[class="login-error"]').remove();
                },
                onclick: false
            });
        }
    });
    module.exports = LoginView.Views.LoginView;
});