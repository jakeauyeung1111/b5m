/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/2.0.0/user/props/views/NotUsedPropView",["$","underscore","backbone","modules/2.0.0/user/props/tpl/prop-item.html","modules/2.0.0/user/props/views/UserPropsUseDialogView"],function(e,t,i){this.underscore=e("underscore"),this.Backbone=e("backbone"),this.UserPropsUseDialogView=e("modules/2.0.0/user/props/views/UserPropsUseDialogView");var o=Backbone.View.extend({tagName:"li",initialize:function(){this.render()},render:function(){return void 0===this.template&&(this.template=underscore.template(e("modules/2.0.0/user/props/tpl/prop-item.html"))),this.$el.html(this.template(this.model)),this},events:{"click .useProp":"useProp"},useProp:function(){new UserPropsUseDialogView({beanCount:this.model.beanCount}).render()}});i.exports=o});