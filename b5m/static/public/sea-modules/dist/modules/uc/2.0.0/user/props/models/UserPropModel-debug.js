define("modules/2.0.0/user/props/models/UserPropModel-debug", [ "backbone-debug", "common-debug" ], function(require, exports, module) {
    this.Backbone = require("backbone-debug");
    this.COMMON = require("common-debug");
    var UserPropModels = {
        Models: {},
        Urls: {}
    };
    UserPropModels.Urls.UserPropUrl = B5M_UC.rootPath + "/props/info/data/getProps.htm";
    UserPropModels.Models.UserProps = Backbone.Model.extend({
        url: UserPropModels.Urls.UserPropUrl,
        parse: function(response) {
            COMMON.checkJSonData(response);
            if (response.ok) return response.data;
            return null;
        },
        initialize: function(options) {
            if (!COMMON.isEmptyValue(options)) {
                var type = options.type;
                if (!COMMON.isEmptyValue(type)) this.url = UserPropModels.Urls.UserPropUrl + "?type=" + type;
            }
            this.fetch({
                async: false,
                cache: false
            });
        }
    });
    module.exports = UserPropModels.Models.UserProps;
});