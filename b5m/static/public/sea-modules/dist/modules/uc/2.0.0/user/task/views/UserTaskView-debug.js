define("modules/2.0.0/user/task/views/UserTaskView-debug", [ "$-debug", "underscore-debug", "backbone-debug", "common-debug", "modules/2.0.0/user/task/models/UserTaskCollection-debug", "modules/2.0.0/user/task/views/UserTaskItemView-debug" ], function(require, exports, module) {
    this._ = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    this.COMMON = require("common-debug");
    this.UserTaskCollection = require("modules/2.0.0/user/task/models/UserTaskCollection-debug");
    this.UserTaskItemView = require("modules/2.0.0/user/task/views/UserTaskItemView-debug");
    var tpl_task_jqqd = "modules/2.0.0/user/task/tpl/task-jqqd.html";
    var UserTaskView = {
        Views: {},
        Urls: {}
    };
    UserTaskView.Views.UserTaskView = Backbone.View.extend({
        initialize: function(opt) {
            this.el = opt.el;
            this.taskItemTpl = opt.taskItemTpl;
            this.showSize = opt.showSize;
            this.dataNotIn = opt.dataNotIn;
            this.userTaskCollection = new UserTaskCollection.UserTaskCollection();
            this.render();
        },
        render: function() {
            this.$el.find(".ui-loading").hide();
            var tempNum = 0;
            this.userTaskCollection.each(function(item, index) {
                if (this.showSize != undefined && tempNum >= this.showSize) return;
                if (this.dataNotIn != undefined) {
                    for (var key in this.dataNotIn) {
                        if (this.dataNotIn[key] == item.get(key)) return;
                    }
                }
                this.renderTask(item, tempNum);
                tempNum++;
            }, this);
            if (!this.dataNotIn) {
                var self = this;
                require.async(tpl_task_jqqd, function(tpl) {
                    self.$el.find("tbody").append(tpl);
                });
            }
            if (this.userTaskCollection.length == 0) this.$el.find(".ui-empty-data").show();
        },
        renderTask: function(item, index) {
            var itemClass = "even";
            if (index % 2 != 0) itemClass = "odd";
            var userTaskItemView = new UserTaskItemView.UserTaskItemView({
                model: item,
                taskItemTpl: this.taskItemTpl,
                attributes: {
                    "class": itemClass
                }
            });
            this.$el.find("tbody").append(userTaskItemView.el);
        }
    });
    module.exports.UserTaskView = UserTaskView.Views.UserTaskView;
});