define("modules/2.0.0/user/trade/common/views/TradeBatchDetailItemView-debug", [ "$-debug", "underscore-debug", "backbone-debug", "common-debug", "modules/2.0.0/user/trade/common/models/ExchangeDetailModel-debug" ], function(require, exports, module) {
    this._ = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    this.COMMON = require("common-debug");
    this.ExchangeDetailModel = require("modules/2.0.0/user/trade/common/models/ExchangeDetailModel-debug");
    this.tpl_trade_batch_tetail_item_1 = "modules/2.0.0/user/trade/common/tpl/trade-batch-detail-item-1.html";
    this.tpl_trade_batch_tetail_item_2 = "modules/2.0.0/user/trade/common/tpl/trade-batch-detail-item-2.html";
    var TradeBatchDetailItemView = {
        Views: {},
        Urls: {}
    };
    TradeBatchDetailItemView.Views.TradeBatchDetailItemView = Backbone.View.extend({
        tagName: "tr",
        attributes: {
            "class": "view-success-panel"
        },
        initialize: function(opt) {
            var self = this;
            this.exchangeDetailModel = new ExchangeDetailModel();
            this.exchangeDetailModel.fetch({
                data: {
                    orderId: self.model.get("orderId")
                },
                success: function() {
                    if (self.exchangeDetailModel.length <= 0) return;
                    var tpl = null, type = self.exchangeDetailModel.at(0).get("type");
                    if (type == null || type == undefined) {
                        COMMON.showTip("详情类型为空");
                        return;
                    }
                    if (type.code == "EC_3") {
                        tpl = tpl_trade_batch_tetail_item_2;
                    } else {
                        tpl = tpl_trade_batch_tetail_item_1;
                    }
                    require.async(tpl, function(result_tpl) {
                        self.template = _.template(result_tpl);
                        self.$el.html(self.template(self.exchangeDetailModel.toJSON()[0]));
                        self.render();
                    });
                }
            });
        }
    });
    module.exports = TradeBatchDetailItemView.Views.TradeBatchDetailItemView;
});