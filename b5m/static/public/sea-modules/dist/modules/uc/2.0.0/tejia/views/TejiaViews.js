/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/2.0.0/tejia/views/TejiaViews",["$","underscore","backbone","common","modules/2.0.0/tejia/views/TejiaView","modules/2.0.0/tejia/models/TejiaCollection"],function(e,t,o){this.COMMON=e("common"),this.underscore=e("underscore"),this.Backbone=e("backbone");var i=e("modules/2.0.0/tejia/views/TejiaView"),a=e("modules/2.0.0/tejia/models/TejiaCollection"),n=Backbone.View.extend({initialize:function(e){this.el=e.el,this.render()},render:function(){this.$el.find(".ui-loading").hide(),this.tejiaCollection=new a.TejiaCollection,this.showTejiaList(),0==this.tejiaCollection.length&&this.$el.append('<div class="ui-empty-data"><span class="tejia-no-data"> 我们正在为您准备推荐商品...</span></div>').show()},showTejiaList:function(){this.tejiaCollection.each(function(e,t){var o="";0==(t+1)%4&&(o="last");var a=new i({model:e,attributes:{"class":o}});this.$el.find("ul").append(a.el)},this)}});o.exports=n});