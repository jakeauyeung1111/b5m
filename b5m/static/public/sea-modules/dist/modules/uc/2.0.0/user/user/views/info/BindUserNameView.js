/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/2.0.0/user/user/views/info/BindUserNameView",["$","underscore","backbone","common","modules/2.0.0/user/user/tpl/info/bind-user-name.html","jquery/validate/1.11.1/validate"],function(require,exports,module){this.underscore=require("underscore"),this.Backbone=require("backbone"),this.COMMON=require("common");var BindUserNameView=Backbone.View.extend({tagName:"form",initialize:function(e){this.el=e.el,this.render(),this.initValidateForm()},render:function(){this.template=underscore.template(require("modules/2.0.0/user/user/tpl/info/bind-user-name.html")),this.$el.html(this.template)},initValidateForm:function(){var $bindNameForm=$("#bindNameForm");jQuery.validator.addMethod("isUserNameUse",function(value,element){var code=0;return $.ajax({type:"post",data:"userName="+value,async:!1,url:B5M_UC.rootPath+"/user/info/data/isNameOrEmailUse.do",success:function(r){code=eval(r).code}}),0==code?!0:!1},"用户名已使用，请更换用户名"),jQuery.validator.addMethod("isUserNameOK",function(e){return/^[a-zA-Z_0-9\u4e00-\u9fa5]+/.test(e)},"用户名格式不正确,含有非法字符"),jQuery.validator.addMethod("isUserNameLengthOK",function(e){var t=e.replace(/[^\x00-\xff]/g,"**").length;return 4>t||t>15?!1:!0},"4到15个字符，一个汉字为两个字符"),$bindNameForm.validate({rules:{userName:{required:!0,isUserNameOK:!0,isUserNameLengthOK:!0,isUserNameUse:!0}},messages:{userName:{required:"用户名不能为空"}},errorElement:"span",errorClass:"text_error",errorPlacement:function(e,t){var i=t.parent().parent().find(".cl-999").empty();i.append(e.html())},success:function(e,t){$(t).parent().parent().find(".cl-999").empty()},onclick:!1}),$('input[name="code"]').keydown(function(e){13==e.keyCode&&submitHandler()}),$("#bind_name_submit").click(function(){$bindNameForm.valid()&&$bindNameForm.submit()})}});module.exports=BindUserNameView});