/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/2.0.0/user/trade/common/views/TradeBatchDetailItemView",["$","underscore","backbone","common","modules/2.0.0/user/trade/common/models/ExchangeDetailModel"],function(e,t,i){this._=e("underscore"),this.Backbone=e("backbone"),this.COMMON=e("common"),this.ExchangeDetailModel=e("modules/2.0.0/user/trade/common/models/ExchangeDetailModel"),this.tpl_trade_batch_tetail_item_1="modules/2.0.0/user/trade/common/tpl/trade-batch-detail-item-1.html",this.tpl_trade_batch_tetail_item_2="modules/2.0.0/user/trade/common/tpl/trade-batch-detail-item-2.html";var o={Views:{},Urls:{}};o.Views.TradeBatchDetailItemView=Backbone.View.extend({tagName:"tr",attributes:{"class":"view-success-panel"},initialize:function(){var t=this;this.exchangeDetailModel=new ExchangeDetailModel,this.exchangeDetailModel.fetch({data:{orderId:t.model.get("orderId")},success:function(){if(!(0>=t.exchangeDetailModel.length)){var i=null,o=t.exchangeDetailModel.at(0).get("type");if(null==o||void 0==o)return COMMON.showTip("详情类型为空"),void 0;i="EC_3"==o.code?tpl_trade_batch_tetail_item_2:tpl_trade_batch_tetail_item_1,e.async(i,function(e){t.template=_.template(e),t.$el.html(t.template(t.exchangeDetailModel.toJSON()[0])),t.render()})}}})}}),i.exports=o.Views.TradeBatchDetailItemView});