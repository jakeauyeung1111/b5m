define("modules/2.0.0/user/trade/common/models/AccountHistoryCollection-debug", [ "$-debug", "underscore-debug", "backbone-debug", "common-debug", "modules/2.0.0/user/trade/common/models/OrderCodeCollection-debug" ], function(require, exports, module) {
    this._ = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    this.COMMON = require("common-debug");
    this.OrderCodeCollection = require("modules/2.0.0/user/trade/common/models/OrderCodeCollection-debug");
    var AccountHistoryCollection = {
        Models: {},
        Collections: {},
        Urls: {}
    };
    AccountHistoryCollection.Urls.accountHistoryList = B5M_UC.rootPath + "/user/trade/common/data/accountHistoryQuery.htm";
    AccountHistoryCollection.Models.Trade = Backbone.Model.extend({});
    AccountHistoryCollection.Collections.AccountHistoryCollection = Backbone.Collection.extend({
        url: AccountHistoryCollection.Urls.accountHistoryList,
        model: AccountHistoryCollection.Models.Trade,
        parse: function(response) {
            COMMON.checkJSonData(response);
            if (response.ok) return this.initModelData(response.data);
            COMMON.showTip(response.data);
            return null;
        },
        initModelData: function(data) {
            var orderCodeCollection = new OrderCodeCollection();
            _.each(data.list, function(obj) {
                obj.autoId = obj.id;
                delete obj.id;
                _.each(orderCodeCollection.models, function(model) {
                    if (model.get("code") == obj.orderCode) {
                        obj._orderName = model.get("name");
                        return;
                    }
                });
                if (obj._orderName == undefined) obj._orderName = "";
            });
            this.dataTotal = data.total;
            return data.list;
        }
    });
    module.exports = AccountHistoryCollection.Collections.AccountHistoryCollection;
});