/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/2.0.0/thirdparty/views/ShareView",["$","underscore","backbone","common","modules/uc/default/common/PopwindowShareView","http://staticcdn.b5m.com/css/common/popwindow-share.css"],function(e,t,o){this._=e("underscore"),this.Backbone=e("backbone"),this.COMMON=e("common"),this.popWindowShareFed=e("modules/uc/default/common/PopwindowShareView");var i={Views:{},Urls:{}};this.root=void 0,i.Views.ShareView=Backbone.View.extend({initialize:function(t){this.parameterData=this.initParameter(t),root=this,e.async("modules/2.0.0/thirdparty/tpl/share-"+this.parameterData.tpl+".html",function(e){root.template=_.template(e),root.render()})},initParameter:function(e){return _.extend({title:"帮5买",content:"帮5买",tpl:24},e)},render:function(){this.$el.find(".ui-loading").hide(),this.$el.html(this.template(this.parameterData));try{}catch(e){COMMON.showTip("出错啦！！！赶紧去买六合彩吧！！！")}},events:{"click #qqzone,#sinamb,#qqmb,#qqwx":"shareItemHandler"},shareItemHandler:function(e){var t=Number($(e.currentTarget).attr("attr-type"));switch(t){case 1:this.sharePublic(e,t);break;case 2:this.sharePublic(e,t);break;case 3:this.shareWeiXin(e,t)}},shareWeiXin:function(){window.open(B5M_UC.rootPath+"/user/third/share/weixin.htm?url="+(this.parameterData.url?encodeURIComponent(this.parameterData.url):"")+"&content="+(this.parameterData.content?encodeURIComponent(this.parameterData.content):""),"_blank")},sharePublic:function(e,t){var o=$(e.currentTarget).attr("attr-sync");$.getJSON(B5M_UC.rootPath+"/user/third/getToken.htm?jsonpCallback=?",{state:t},function(e){if(e.ok)popWindowShareFed.popwindow({title:root.parameterData.title,content:root.parameterData.content,href:root.parameterData.url,type:t},function(e){var i={state:t,summary:e.content,title:e.title};1==t?(i.url=e.href,o&&(i.nswb=o)):i.summary=i.summary+"  "+e.href,e.pic&&(i.images=e.pic),$.getJSON(B5M_UC.rootPath+"/user/third/share.htm?jsonpCallback=?",i,function(e){e.ok?(popWindowShareFed.popwindowClose(),COMMON.showSimpleTip("分享成功")):COMMON.showSimpleTip("分享失败，请稍后分享")})});else{var i=window.open();setTimeout(function(){i.location=B5M_UC.rootPath+"/user/third/login/auth.htm?type="+t+"&refererUrl=&api=/setToken"},10)}})}}),o.exports=i.Views.ShareView});