/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/2.0.0/user/user/views/info/UserInfoView",["$","underscore","backbone","common","modules/2.0.0/user/user/models/UserModel","modules/2.0.0/user/user/models/LevelModel","modules/2.0.0/user/task/views/UserPluginInstallOrSignView"],function(e,t,i){this._=e("underscore"),this.Backbone=e("backbone"),this.COMMON=e("common"),this.UserModel=e("modules/2.0.0/user/user/models/UserModel"),this.LevelModel=e("modules/2.0.0/user/user/models/LevelModel"),this.UserPluginInstallOrSignView=e("modules/2.0.0/user/task/views/UserPluginInstallOrSignView");var o={Views:{},Urls:{}},a="modules/2.0.0/user/user/tpl/info/info-1.html",n="modules/2.0.0/user/user/tpl/info/info-2.html",r="modules/2.0.0/user/user/tpl/info/info-3.html",s="modules/2.0.0/user/user/tpl/info/info-b5m.html";o.Views.UserInfoView=Backbone.View.extend({initialize:function(t){this.el=t.el,this.userModel=new UserModel.UserModel({type:2}),this.levelModel=new LevelModel.LevelModel;var i=a,o=this;switch(t.tplId){case 1:i=a;break;case 2:i=n;break;case 3:i=r;break;case 4:i=s}e.async(i,function(e){o.template=_.template(e),o.render()})},render:function(){this.$el.find(".ui-loading").hide();var e=_.extend(this.userModel.toJSON(),this.levelModel.toJSON());e.lastLoginTime=null==e.lastLoginTime||void 0==e.lastLoginTime?COMMON.formatTime(new Date,"yyyy-MM-dd"):COMMON.formatTime(e.lastLoginTime,"yyyy-MM-dd"),this.$el.html(this.template(e)),0==this.userModel.length&&this.$el.find(".ui-empty-data").show()},events:{"click #everySign":"everySign","click #hasInstallPlugin":"hasInstallPlugin","mouseover .user-pic":"mouseover","mouseleave .user-pic":"mouseleave","hover #bangbichargebeanhandle":"bangbichargebeanhandle"},everySign:function(e){var t=$(e.currentTarget);"true"!=t.attr("isSign")&&$.ajax({type:"POST",url:B5M_UC.rootPath+"/user/task/data/sign.htm",dataType:"json",success:function(e){e.ok?window.location.href=B5M_UC.rootPath:COMMON.showSimpleTip(e.data)}})},hasInstallPlugin:function(){var e=this.isInstallPlugin();new UserPluginInstallOrSignView({hasInstallPlugin:e}).render()},isInstallPlugin:function(){var e=!!document.getElementById("b5mmain");if(e)return e;var t=$('<span><object type="application/x-bang5taoplugin" width="0" height="0" style="display:none"></object></span>');return $("body").append(t),!!t.find("object")[0].getCacheValue},mouseover:function(e){$(e.currentTarget).find("a").animate({bottom:21},200)},mouseleave:function(e){$(e.currentTarget).find("a").animate({bottom:-1},200)},bangbichargebeanhandle:function(e){$(e.currentTarget).css("text-decoration","none")}}),i.exports.UserInfoView=o.Views.UserInfoView});