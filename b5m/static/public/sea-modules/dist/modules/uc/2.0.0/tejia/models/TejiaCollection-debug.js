define("modules/2.0.0/tejia/models/TejiaCollection-debug", [ "$-debug", "backbone-debug", "common-debug" ], function(require, exports, module) {
    this._ = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    this.COMMON = require("common-debug");
    var TejiaCollection = {
        Models: {},
        Collections: {},
        Urls: {}
    };
    TejiaCollection.Urls.TejiaUrl = B5M_UC.rootPath + "/user/info/goodList.htm";
    TejiaCollection.Models.Tejia = Backbone.Model.extend({});
    TejiaCollection.Collections.Tejias = Backbone.Collection.extend({
        model: TejiaCollection.Models.Tejia,
        url: TejiaCollection.Urls.TejiaUrl,
        parse: function(response) {
            COMMON.checkJSonData(response);
            var data = response;
            for (var i = 0; i < data.length; i++) {
                data[i].autoId = data[i].id;
                delete data[i].id;
            }
            return data;
        },
        initialize: function() {
            this.fetch({
                async: false,
                cache: false
            });
        }
    });
    module.exports.TejiaCollection = TejiaCollection.Collections.Tejias;
});