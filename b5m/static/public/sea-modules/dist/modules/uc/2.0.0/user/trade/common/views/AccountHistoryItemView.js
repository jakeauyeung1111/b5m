/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/2.0.0/user/trade/common/views/AccountHistoryItemView",["$","underscore","backbone","common"],function(e,t,o){this._=e("underscore"),this.Backbone=e("backbone"),this.COMMON=e("common");var i="modules/2.0.0/user/trade/common/tpl/account-history-item.html",a="modules/2.0.0/user/trade/common/tpl/account-history-exchange-item.html",n={Views:{}};n.Views.AccountHistoryItemView=Backbone.View.extend({tagName:"tr",initialize:function(t){var o=i,n=this;"default"==t.tplName&&(o=i),"exchange"==t.tplName&&(o=a),e.async(o,function(e){n.template=_.template(e),n.$el.html(n.template(n.model.toJSON())),n.render()})}}),o.exports=n.Views.AccountHistoryItemView});