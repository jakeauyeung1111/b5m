define("modules/2.0.0/user/user/views/info/RegisterView-debug", [ "$-debug", "underscore-debug", "backbone-debug", "common-debug", "jquery/validate/1.11.1/validate-debug", "modules/2.0.0/user/user/tpl/info/register-debug.html", "modules/2.0.0/thirdparty/views/LoginView-debug" ], function(require, exports, module) {
    this._ = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    this.COMMON = require("common-debug");
    this.ThirdPartyLoginView = require("modules/2.0.0/thirdparty/views/LoginView-debug");
    this.tpl_user_register = require("modules/2.0.0/user/user/tpl/info/register-debug.html");
    var RegisterView = {
        Views: {},
        Urls: {}
    };
    RegisterView.Views.RegisterView = Backbone.View.extend({
        template: _.template(tpl_user_register),
        initialize: function(opt) {
            this.opt = opt;
            this.el = opt.el;
            this.render();
            this.initValidate();
        },
        render: function() {
            this.$el.find(".ui-loading").hide();
            this.$el.html(this.template(this.opt));
            this.thirdPartyLoginView = new ThirdPartyLoginView({
                redirectUrl: this.opt.url,
                userMps: this.opt.userMps,
                tpl: 2
            });
            this.$el.find(".platform").append(this.thirdPartyLoginView.$el);
        },
        events: {
            "click #refreshCode": "refreshCode",
            "click #submit_btn": "submit_btn",
            "keydown #valiCode": "valiCodeKeyDown"
        },
        refreshCode: function(e) {
            e.preventDefault();
            $("#code").attr("src", B5M_UC.rootPath + "/validateCode.do?f=" + Math.random());
        },
        valiCodeKeyDown: function(e) {
            if (e.keyCode == 13) this.submitHandler();
        },
        submit_btn: function(e) {
            e.preventDefault();
            this.submitHandler();
        },
        submitHandler: function(e) {
            if (this.validateForm.valid()) {
                this.validateForm.submit();
            }
        },
        initValidate: function() {
            var $registerForm = $("#registerForm");
            //自定义异步验证验证
            jQuery.validator.addMethod("isEmail", function(value, element) {
                var code = 0;
                $.ajax({
                    type: "post",
                    data: "email=" + value,
                    async: false,
                    url: B5M_UC.rootPath + "/user/info/data/isNameOrEmailUse.do",
                    success: function(r) {
                        code = eval(r).code;
                    }
                });
                return code == 0 ? true : false;
            }, "邮箱已经被占用，请更换邮箱");
            jQuery.validator.addMethod("codeLength", function(value, element) {
                return value.length == 4 ? true : false;
            }, "验证码为4位");
            jQuery.validator.addMethod("isPasswordOK", function(value, element) {
                return /^[a-zA-Z_0-9]{6,15}$/.test(value);
            }, "请输入正确格式的字母，数字");
            $registerForm.validate({
                rules: {
                    email: {
                        required: true,
                        email: true,
                        isEmail: true
                    },
                    password: {
                        required: true,
                        rangelength: [ 6, 15 ],
                        isPasswordOK: true
                    },
                    confirmPassword: {
                        required: true,
                        rangelength: [ 6, 15 ],
                        isPasswordOK: true,
                        equalTo: "#password"
                    },
                    code: {
                        required: true,
                        codeLength: true
                    }
                },
                messages: {
                    email: {
                        required: "Email不能为空",
                        email: "邮箱地址格式不正确"
                    },
                    password: {
                        required: "密码不能为空",
                        rangelength: "密码长度必须介于 {0} 和 {1} 之间"
                    },
                    confirmPassword: {
                        required: "确认密码不能为空",
                        rangelength: "密码长度必须介于 {0} 和 {1} 之间",
                        equalTo: "两次输入的密码不一致"
                    },
                    code: {
                        required: "验证码不能为空"
                    }
                },
                errorElement: "span",
                errorClass: "text_error",
                errorPlacement: function(error, element) {
                    $("#serror").empty().hide();
                    element.attr("class", "text text_error");
                    var errorBox = element.parent().parent().find(".errorBox").empty();
                    errorBox.append('<img class="ui-icon ui-icon-error" src="' + B5M_UC.rootPath + '/images/ucenter/blank.png" />&nbsp;' + error.html());
                },
                success: function(label, d) {
                    var parent = $(d).parent().parent().find(".errorBox").empty();
                    if ($(d).attr("name") != "code") parent.append('<img class="ui-icon ui-icon-right" src="' + B5M_UC.rootPath + '/images/ucenter/blank.png">');
                },
                //				onfocusout: function(element) { $(element).valid(); }
                onclick: false
            });
            this.validateForm = $registerForm;
            var $msgError = $("#serror");
            if ($.trim($msgError.find("span").html()) != "") $msgError.show();
        }
    });
    module.exports.RegisterView = RegisterView.Views.RegisterView;
});