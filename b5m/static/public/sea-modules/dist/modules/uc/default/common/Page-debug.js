define("modules/uc/default/common/Page-debug", [ "underscore-debug" ], function(require, exports, module) {
    this._ = require("underscore-debug");
    var Util = {
        E: function(id) {
            return typeof id == "string" ? document.getElementById(id) : id;
        },
        addEvent: function(id, events, fn) {
            var dom = Util.E(id);
            if (dom == null) return 1;
            events = events || "click";
            if ((typeof fn).toLowerCase() == "function") {
                dom.attachEvent ? dom.attachEvent("on" + events, fn) : dom.addEventListener ? dom.addEventListener(events, fn, !1) : dom["on" + events] = fn;
                return 0;
            }
        }
    };
    var self;
    function Page(options) {
        var PAGE_COUNT = 0, self = this;
        this.defaults = {
            pageNum: 0,
            total: 0,
            pageSize: 10,
            onClick: function() {}
        };
        this.init = function(options) {
            self.settings = _.extend(self.defaults, options);
        };
        this.onClickPageLink = function(tagA, pageDom) {
            return function(event) {
                if (tagA.id == "goA") {
                    var value = pageDom.getElementsByTagName("input")[0].value, intValue = parseInt(value);
                    if (intValue && PAGE_COUNT >= intValue) tagA.setAttribute("pageNum", value);
                }
                var pageNum = tagA.getAttribute("pageNum");
                if (pageNum != "lock" && pageNum != null) self.settings.onClick(pageNum, tagA);
            };
        };
        this.pageLinkBindEvent = function() {
            var html = this.getPageHtml(), dom = document.createElement("div"), tagAs = dom.getElementsByTagName("a");
            dom.innerHTML = html;
            for (var i = 0; i < tagAs.length; i++) {
                Util.addEvent(tagAs[i], "click", self.onClickPageLink(tagAs[i], dom));
                if (tagAs[i].id == "goA") {
                    var input = dom.getElementsByTagName("input")[0];
                    Util.addEvent(input, "keyup", function(event) {
                        //响应鼠标事件，允许左右方向键移动 
                        event = window.event || event;
                        if (event.keyCode == 37 | event.keyCode == 39) {
                            return;
                        }
                        //先把非数字的都替换掉，除了数字和. 
                        input.value = input.value.replace(/[^\d]/g, "");
                        //必须保证第一个为数字而不是0 
                        input.value = input.value.replace(/^0/g, "");
                        //不能超过最大页码
                        var total = parseInt(self.settings.total), pageSize = parseInt(self.settings.pageSize), pageCount = Math.floor((total + pageSize - 1) / pageSize);
                        if (parseInt(input.value) > pageCount) {
                            input.value = pageCount;
                        }
                    });
                }
            }
            return dom;
        };
        this.getPageDom = function(options) {
            self.init(options);
            return this.pageLinkBindEvent();
        };
        this.getPageHtml = function(options) {
            self.init(options);
            var pageNum = parseInt(self.settings.pageNum);
            var total = parseInt(self.settings.total);
            var pageSize = parseInt(self.settings.pageSize);
            var pageCount = PAGE_COUNT = Math.floor((total + pageSize - 1) / pageSize);
            var start = pageNum > 2 ? pageNum - 2 : 1;
            var end = Math.min(start + 4, pageCount);
            var next = Math.min(pageNum + 1, pageCount);
            if (pageCount - start < 4) start = pageCount - 4 > 0 ? pageCount - 4 : 1;
            var html = "";
            if (pageNum <= 1) {
                html += '<a class="first dis" href="javascript:void(0);" pageNum="lock">首页</a>';
                html += '<a class="prev dis" href="javascript:void(0);" pageNum="lock">&lt;</a>';
            } else {
                html += '<a class="first" href="javascript:void(0);" pageNum="1">首页</a>';
                html += '<a class="prev" href="javascript:void(0);" pageNum="' + (pageNum - 1) + '">&lt;</a>';
            }
            if (start > 1) {
                html += "<span>...</span>";
            }
            for (var i = start; i <= end; i++) {
                html += '<a class="' + (i == pageNum ? "cur" : "") + '" href="javascript:void(0);" pageNum="' + i + '">' + i + "</a>";
            }
            if (pageCount - pageNum > 2 && pageCount > 5) {
                html += "<span>...</span>";
            }
            if (pageNum < pageCount) {
                html += '<a class="next" href="javascript:void(0);" pageNum="' + next + '">&gt;</a>';
                html += '<a class="last" href="javascript:void(0);" pageNum="' + pageCount + '">尾页</a>';
            } else {
                html += '<a class="next dis" href="javascript:void(0);" pageNum="lock">&gt;</a>';
                html += '<a class="last dis" href="javascript:void(0);" pageNum="lock">尾页</a>';
            }
            html += '<span class="all">共' + pageCount + "页&nbsp;&nbsp;去第</span>";
            html += '<span class="page-input"><input type="text"><a href="javascript:void(0)" id="goA">GO</a></span><span class="go">页</span>';
            return html;
        };
        self.init(options);
    }
    module.exports = Page;
});