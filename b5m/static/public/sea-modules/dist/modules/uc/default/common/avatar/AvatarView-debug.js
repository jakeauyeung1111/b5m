define("modules/uc/default/common/avatar/AvatarView-debug", [ "underscore-debug", "backbone-debug", "modules/uc/default/common/avatar/tpl/avatar-debug.html" ], function(require, exports, module) {
    this._ = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    this.tpl_avatar = require("modules/uc/default/common/avatar/tpl/avatar-debug.html");
    var AvatarView = Backbone.View.extend({
        template: _.template(tpl_avatar),
        initialize: function(options) {
            this.initParameter(options);
            this.render();
        },
        initParameter: function(options) {
            this.defaults = {
                editUrl: B5M_UC.rootPath + "/user/info/data/editAvatar.htm"
            };
            this.settings = _.extend(this.defaults, options);
        },
        render: function() {
            this.$el.append(this.template({
                editUrl: this.settings.editUrl
            }));
        }
    });
    module.exports = AvatarView;
});