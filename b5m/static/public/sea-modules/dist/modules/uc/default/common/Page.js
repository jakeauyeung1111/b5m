/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/uc/default/common/Page",["underscore"],function(e,t,i){function o(e){var t=0,i=this;this.defaults={pageNum:0,total:0,pageSize:10,onClick:function(){}},this.init=function(e){i.settings=_.extend(i.defaults,e)},this.onClickPageLink=function(e,o){return function(){if("goA"==e.id){var a=o.getElementsByTagName("input")[0].value,n=parseInt(a);n&&t>=n&&e.setAttribute("pageNum",a)}var s=e.getAttribute("pageNum");"lock"!=s&&null!=s&&i.settings.onClick(s,e)}},this.pageLinkBindEvent=function(){var e=this.getPageHtml(),t=document.createElement("div"),o=t.getElementsByTagName("a");t.innerHTML=e;for(var n=0;o.length>n;n++)if(a.addEvent(o[n],"click",i.onClickPageLink(o[n],t)),"goA"==o[n].id){var s=t.getElementsByTagName("input")[0];a.addEvent(s,"keyup",function(e){if(e=window.event||e,!(37==e.keyCode|39==e.keyCode)){s.value=s.value.replace(/[^\d]/g,""),s.value=s.value.replace(/^0/g,"");var t=parseInt(i.settings.total),o=parseInt(i.settings.pageSize),a=Math.floor((t+o-1)/o);parseInt(s.value)>a&&(s.value=a)}})}return t},this.getPageDom=function(e){return i.init(e),this.pageLinkBindEvent()},this.getPageHtml=function(e){i.init(e);var o=parseInt(i.settings.pageNum),a=parseInt(i.settings.total),n=parseInt(i.settings.pageSize),s=t=Math.floor((a+n-1)/n),r=o>2?o-2:1,l=Math.min(r+4,s),d=Math.min(o+1,s);4>s-r&&(r=s-4>0?s-4:1);var c="";1>=o?(c+='<a class="first dis" href="javascript:void(0);" pageNum="lock">首页</a>',c+='<a class="prev dis" href="javascript:void(0);" pageNum="lock">&lt;</a>'):(c+='<a class="first" href="javascript:void(0);" pageNum="1">首页</a>',c+='<a class="prev" href="javascript:void(0);" pageNum="'+(o-1)+'">&lt;</a>'),r>1&&(c+="<span>...</span>");for(var u=r;l>=u;u++)c+='<a class="'+(u==o?"cur":"")+'" href="javascript:void(0);" pageNum="'+u+'">'+u+"</a>";return s-o>2&&s>5&&(c+="<span>...</span>"),s>o?(c+='<a class="next" href="javascript:void(0);" pageNum="'+d+'">&gt;</a>',c+='<a class="last" href="javascript:void(0);" pageNum="'+s+'">尾页</a>'):(c+='<a class="next dis" href="javascript:void(0);" pageNum="lock">&gt;</a>',c+='<a class="last dis" href="javascript:void(0);" pageNum="lock">尾页</a>'),c+='<span class="all">共'+s+"页&nbsp;&nbsp;去第</span>",c+='<span class="page-input"><input type="text"><a href="javascript:void(0)" id="goA">GO</a></span><span class="go">页</span>'},i.init(e)}this._=e("underscore");var a={E:function(e){return"string"==typeof e?document.getElementById(e):e},addEvent:function(e,t,i){var o=a.E(e);return null==o?1:(t=t||"click","function"==(typeof i).toLowerCase()?(o.attachEvent?o.attachEvent("on"+t,i):o.addEventListener?o.addEventListener(t,i,!1):o["on"+t]=i,0):void 0)}};i.exports=o});