define("modules/uc/default/common/TestPassword-debug", [ "http://staticcdn.b5m.com/static/css/center/setting-debug.css" ], function(require, exports, module) {
    function TestPassword(opt) {
        this.init(opt);
    }
    (function(TestPassword) {
        TestPassword.init = function(opt) {};
        /**
		 * 测试密码强度
		 * @param password 密码
		 * @param username 用户名
		 * @return
		 * 				0:密码太短
		 * 				1:较弱
		 * 				2:弱
		 * 				3:一般
		 * 				4:强
		 */
        TestPassword.testPwd = function(password) {
            return checkStrong(password);
        };
        TestPassword.getDefaultHtml = function(password) {
            var code = this.testPwd(password);
            if (code <= 1) return '<p class="pwd-power pwd-weak"></p>';
            if (code == 2 || code == 3) return '<p class="pwd-power pwd-middle"></p>';
            if (code == 4) return '<p class="pwd-power pwd-strong"></p>';
        };
        //测试某个字符是属于哪一类.  
        function CharMode(iN) {
            if (iN >= 48 && iN <= 57) //数字  
            return 1;
            if (iN >= 65 && iN <= 90) //大写字母  
            return 2;
            if (iN >= 97 && iN <= 122) //小写  
            return 4; else return 8;
        }
        //计算出当前密码当中一共有多少种模式  
        function bitTotal(num) {
            var modes = 0;
            for (var i = 0; i < 4; i++) {
                if (num & 1) modes++;
                num >>>= 1;
            }
            return modes;
        }
        //返回密码的强度级别  
        function checkStrong(sPW) {
            if (sPW.length <= 4) return 0;
            //密码太短  
            Modes = 0;
            for (var i = 0; i < sPW.length; i++) {
                //测试每一个字符的类别并统计一共有多少种模式.  
                Modes |= CharMode(sPW.charCodeAt(i));
            }
            return bitTotal(Modes);
        }
    })(TestPassword.prototype);
    module.exports = TestPassword;
});