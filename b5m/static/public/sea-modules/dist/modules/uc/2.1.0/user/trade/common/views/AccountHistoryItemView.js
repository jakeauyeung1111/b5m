/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/uc/2.1.0/user/trade/common/views/AccountHistoryItemView",["$","underscore","backbone","common"],function(e,t,i){this._=e("underscore"),this.Backbone=e("backbone"),this.COMMON=e("common"),this.tpl_account_history_item="modules/uc/2.1.0/user/trade/common/tpl/account-history-item.html",this.tpl_account_history_exchange_item="modules/uc/2.1.0/user/trade/common/tpl/account-history-exchange-item.html";var o={Views:{}};o.Views.AccountHistoryItemView=Backbone.View.extend({tagName:"tr",initialize:function(t){var i=tpl_account_history_item,o=this;"default"==t.tplName&&(i=tpl_account_history_item),"exchange"==t.tplName&&(i=tpl_account_history_exchange_item),e.async(i,function(e){o.template=_.template(e),o.$el.html(o.template(o.model.toJSON())),o.render()})}}),i.exports=o.Views.AccountHistoryItemView});