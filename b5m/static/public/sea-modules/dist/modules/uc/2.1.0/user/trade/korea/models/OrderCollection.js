/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/uc/2.1.0/user/trade/korea/models/OrderCollection",["$","underscore","backbone","common"],function(e,t,i){this._=e("underscore"),this.Backbone=e("backbone"),this.COMMON=e("common");var o={Models:{},Collections:{},Urls:{}};o.Urls.orderList="http://korea.b5m.com/shop/orderApi.php",o.Models.Order=Backbone.Model.extend({}),o.Collections.OrderCollection=Backbone.Collection.extend({url:o.Urls.orderList,model:o.Models.Order,sync:function(e,t,i){var o=_.extend({async:!1,cache:!1,dataType:"jsonp",jsonp:"jsonpCallback",type:"POST",url:t.url,processData:!0},i);return $.ajax(o)},parse:function(e){return COMMON.checkJSonData(e),e.ok?this.initModelData(e.data):(COMMON.showTip(e.data),null)},initModelData:function(e){return _.each(e.record,function(e){e.autoId=e.id,delete e.id}),this.dataTotal=e.page.total,e.record}}),i.exports=o.Collections.OrderCollection});