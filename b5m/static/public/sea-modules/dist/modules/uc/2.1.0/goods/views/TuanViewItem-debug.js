define("modules/uc/2.1.0/goods/views/TuanViewItem-debug", [ "$-debug", "underscore-debug", "backbone-debug", "common-debug", "modules/uc/2.1.0/goods/tpl/tuan-item-debug.html" ], function(require, exports, module) {
    this.COMMON = require("common-debug");
    this._ = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    this.TuanViewItem = Backbone.View.extend({
        tagName: "li",
        initialize: function() {
            this.render();
        },
        render: function() {
            //加载模版
            if (this.template === undefined) this.template = _.template(require("modules/uc/2.1.0/goods/tpl/tuan-item-debug.html"));
            this.$el.html(this.template(this.model.toJSON()));
            return this;
        }
    });
    module.exports = TuanViewItem;
});