/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/uc/2.1.0/user/account/favorites/models/WebsiteCollection",["$","underscore","backbone","common"],function(e,t,i){this._=e("underscore"),this.Backbone=e("backbone"),this.COMMON=e("common"),this.Websites=[{code:void 0,name:"全部"},{code:"jd.com",name:"京东"},{code:"taobao.com",name:"淘宝"},{code:"tmall.com",name:"天猫"},{code:"yhd.com",name:"一号店"},{code:"yixun.com",name:"易讯"},{code:"amazon.cn",name:"亚马逊"},{code:"dangdang.com",name:"当当网"},{code:"suning.com",name:"苏宁"},{code:"gome.com.cn",name:"国美"},{code:"vip.com",name:"唯品会"}];var o={Models:{},Collections:{},Urls:{}};o.Models.Website=Backbone.Model.extend({}),o.Collections.WebsiteCollection=Backbone.Collection.extend({model:o.Models.Website,initialize:function(){this.add(Websites)},getNameByCode:function(e){var t;return this.each(function(i){return i.get("code")==e?(t=i,!1):void 0}),t?t.get("name"):"暂无"}}),i.exports=o.Collections.WebsiteCollection});