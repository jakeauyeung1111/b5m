/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/uc/2.1.0/user/task/views/UserTaskView",["$","underscore","backbone","common","modules/uc/2.1.0/user/task/models/UserTaskCollection","modules/uc/2.1.0/user/task/views/UserTaskItemView","modules/uc/2.1.0/user/task/tpl/task.html"],function(e,t,i){this._=e("underscore"),this.Backbone=e("backbone"),this.COMMON=e("common"),this.UserTaskCollection=e("modules/uc/2.1.0/user/task/models/UserTaskCollection"),this.UserTaskItemView=e("modules/uc/2.1.0/user/task/views/UserTaskItemView"),this.tpl_task=e("modules/uc/2.1.0/user/task/tpl/task.html"),this.tpl_task_jqqd="modules/uc/2.1.0/user/task/tpl/task-jqqd.html";var o={Views:{},Urls:{}};this.root=void 0,o.Views.UserTaskView=Backbone.View.extend({template:_.template(tpl_task),initialize:function(e){root=this,this.parameterData=this.initParameter(e),root.$el.html(root.template(this.parameterData)),this.userTaskCollection=new UserTaskCollection.UserTaskCollection,this.userTaskCollection.fetch({cache:!1,success:function(){root.render()}})},initParameter:function(e){return _.extend({showSize:void 0,taskItemTpl:1,dataNotIn:void 0,showOtherTask:!0,showHead:!0},e)},render:function(){this.$el.find(".ui-loading").hide();var t=0;if(this.userTaskCollection.each(function(e){if(!(void 0!=this.parameterData.showSize&&t>=this.parameterData.showSize)){if(void 0!=this.parameterData.dataNotIn)for(var i in this.parameterData.dataNotIn)if(this.parameterData.dataNotIn[i]==e.get(i))return;this.renderTask(e,t),t++}},this),this.parameterData.showOtherTask){var i=this;e.async(tpl_task_jqqd,function(e){i.$el.find("tbody").append(e)})}0==this.userTaskCollection.length&&this.$el.find(".ui-empty-data").show()},renderTask:function(e,t){var i="even";0!=t%2&&(i="odd");var o=new UserTaskItemView.UserTaskItemView({model:e,taskItemTpl:this.parameterData.taskItemTpl,attributes:{"class":i}});this.$el.find("tbody").append(o.el)}}),i.exports=o.Views.UserTaskView});