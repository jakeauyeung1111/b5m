define("modules/uc/2.1.0/user/trade/common/models/ExchangeDetailModel-debug", [ "$-debug", "underscore-debug", "backbone-debug", "common-debug" ], function(require, exports, module) {
    this._ = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    this.COMMON = require("common-debug");
    var ExchangeDetailModel = {
        Models: {},
        Collections: {},
        Urls: {}
    };
    var domin_url = window.location.href.indexOf("ucenter.b5m.com"), domin_url2 = window.location.href.indexOf("ucenter.prod.bang5mai.com");
    if (0 < domin_url && domin_url < 10 || 0 < domin_url2 && domin_url2 < 10) {
        ExchangeDetailModel.Urls.exchange_detail = "http://sa1.b5m.com:58080/B5MCMS/exchangeApiController/couponInfo.do";
    } else {
        ExchangeDetailModel.Urls.exchange_detail = "http://cms.stage.bang5mai.com/B5MCMS/exchangeApiController/couponInfo.do";
    }
    ExchangeDetailModel.Models.Trade = Backbone.Model.extend({});
    ExchangeDetailModel.Collections.ExchangeDetailModel = Backbone.Collection.extend({
        url: ExchangeDetailModel.Urls.exchange_detail,
        sync: function(method, model, options) {
            var params = _.extend({
                async: false,
                cache: false,
                dataType: "jsonp",
                jsonp: "jsonpCallback",
                // the api requires the jsonp callback name to be this exact name
                type: "POST",
                url: model.url,
                processData: true
            }, options);
            return $.ajax(params);
        },
        parse: function(response) {
            COMMON.checkJSonData(response);
            if (response.ok) return this.initModelData(response.data);
            COMMON.showTip(response.data == undefined || response.data == null || response.data == "null" ? response.msg : response.data);
            return null;
        },
        initModelData: function(data) {
            if (data.couponPnInfo) data.couponPnInfo = $.parseJSON(data.couponPnInfo);
            return data;
        }
    });
    module.exports = ExchangeDetailModel.Collections.ExchangeDetailModel;
});