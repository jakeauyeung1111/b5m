define("modules/uc/2.1.0/user/trade/common/views/TradeBatchItemView-debug", [ "$-debug", "underscore-debug", "backbone-debug", "common-debug" ], function(require, exports, module) {
    this._ = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    this.COMMON = require("common-debug");
    this.tpl_trade_batch_item = "modules/uc/2.1.0/user/trade/common/tpl/trade-batch-item.html";
    var TradeBatchItemView = {
        Views: {}
    };
    TradeBatchItemView.Views.TradeBatchItemView = Backbone.View.extend({
        tagName: "tr",
        initialize: function(opt) {
            var self = this;
            require.async(tpl_trade_batch_item, function(tpl) {
                self.template = _.template(tpl), self.$el.html(self.template(self.model.toJSON()));
                self.render();
                //兑换详情页
                if (self.model.get("groupName") && self.model.get("groupName").toLocaleLowerCase() == "ec" && self.model.get("orderCode") == "21") {
                    require.async("modules/uc/2.1.0/user/trade/common/views/TradeBatchDetailItemView", function(TradeBatchDetailItemView) {
                        var tradeBatchDetailItemView = new TradeBatchDetailItemView({
                            model: self.model
                        });
                        //添加向下箭头
                        var html = self.$el.find("td:last").html();
                        self.$el.find("td:last").empty().append('<a class="view-success singleIco" href="javascript:void(0);">' + html + '<u class="singleIco-u"></u></a>');
                        //追加到当前table行内
                        self.$el.append(tradeBatchDetailItemView.$el);
                    });
                }
            });
        },
        events: {
            "click .view-success": function(e) {
                var $span = $(e.currentTarget), $parentTR = $span.closest("tr");
                //检测详情是否存在
                if (!$parentTR.next().attr("class") || $parentTR.next().attr("class").indexOf("view-success-panel") < 0) $parentTR.after($parentTR.find(".view-success-panel"));
                var $parentTRNext = $parentTR.next();
                //关闭或打开
                if ($parentTRNext.attr("class").indexOf("show") >= 0) {
                    $span.attr("class", "view-success singleIco");
                    $parentTRNext.attr("class", "view-success-panel");
                } else {
                    $span.attr("class", "view-success singleIco on");
                    $parentTRNext.attr("class", "view-success-panel show");
                }
            }
        }
    });
    module.exports = TradeBatchItemView.Views.TradeBatchItemView;
});