/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/uc/2.1.0/user/account/favorites/views/WebsiteView",["$","underscore","backbone","common","modules/uc/2.1.0/user/account/favorites/models/WebsiteCollection","modules/uc/2.1.0/user/account/favorites/views/WebsiteItemView"],function(e,t,i){this._=e("underscore"),this.Backbone=e("backbone"),this.COMMON=e("common"),this.WebsiteCollection=e("modules/uc/2.1.0/user/account/favorites/models/WebsiteCollection"),this.WebsiteItemView=e("modules/uc/2.1.0/user/account/favorites/views/WebsiteItemView");var o={Views:{},Urls:{}};o.Views.WebsiteView=Backbone.View.extend({initialize:function(){this.websiteCollection=new WebsiteCollection,this.render()},render:function(){this.websiteCollection.each(function(e,t){this.renderData(e,t)},this)},renderData:function(e,t){var i=new WebsiteItemView({model:e});0==t&&i.$el.find("a").addClass("current"),this.$el.append(i.el)}}),i.exports=o.Views.WebsiteView});