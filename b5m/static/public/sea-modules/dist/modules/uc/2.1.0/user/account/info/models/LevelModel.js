/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/uc/2.1.0/user/account/info/models/LevelModel",["$","underscore","backbone","common"],function(e,t,i){this._=e("underscore"),this.Backbone=e("backbone"),this.COMMON=e("common");var o={Models:{},Urls:{}};o.Urls.LevelModel=B5M_UC.rootPath+"/user/info/data/levelRule.htm",o.Models.LevelModel=Backbone.Model.extend({url:o.Urls.LevelModel,sync:function(e,t,i){var o=_.extend({async:!1,cache:!1,dataType:"jsonp",jsonp:"jsonpCallback",type:"POST",url:t.url,processData:!0},i);return $.ajax(o)},parse:function(e){if(COMMON.checkJSonData(e),e.ok){for(var t=e.data,i=0;t.length>i;i++)t[i].autoId=t[i].id,delete t[i].id;return t}return COMMON.showTip(e.data),null},initialize:function(){this.fetch()}}),i.exports.LevelModel=o.Models.LevelModel});