define("modules/uc/2.1.0/user/account/info/views/UserInfoView-debug", [ "$-debug", "underscore-debug", "backbone-debug", "common-debug", "modules/uc/2.1.0/user/account/info/models/UserModel-debug", "modules/uc/2.1.0/user/account/info/models/LevelModel-debug" ], function(require, exports, module) {
    this._ = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    this.COMMON = require("common-debug");
    this.UserModel = require("modules/uc/2.1.0/user/account/info/models/UserModel-debug");
    this.LevelModel = require("modules/uc/2.1.0/user/account/info/models/LevelModel-debug");
    var UserInfoView = {
        Views: {},
        Urls: {}
    };
    this.tpl_user_info_id_1 = "modules/uc/2.1.0/user/account/info/tpl/info-1.html";
    this.tpl_user_info_id_2 = "modulesuc//2.1.0/user/account/info/info-2.html";
    this.tpl_user_info_id_3 = "modules/uc/2.1.0/user/account/info/info-3.html";
    this.tpl_user_info_id_4 = "modules/uc/2.1.0/user/account/info/info-b5m.html";
    var root = undefined;
    UserInfoView.Views.UserInfoView = Backbone.View.extend({
        initialize: function(opt) {
            root = this;
            this.userModel = new UserModel.UserModel({
                type: 2
            });
            this.levelModel = new LevelModel.LevelModel();
            //加载模版
            var tpl_user_info = tpl_user_info_id_1, self = this;
            switch (opt.tplId) {
              case 1:
                tpl_user_info = tpl_user_info_id_1;
                break;

              case 2:
                tpl_user_info = tpl_user_info_id_2;
                break;

              case 3:
                tpl_user_info = tpl_user_info_id_3;
                break;

              case 4:
                tpl_user_info = tpl_user_info_id_4;
                break;
            }
            require.async(tpl_user_info, function(html) {
                self.template = _.template(html), self.render();
            });
        },
        render: function() {
            this.$el.find(".ui-loading").hide();
            var data = _.extend(this.userModel.toJSON(), this.levelModel.toJSON());
            //格式化最后登录时间
            if (data.lastLoginTime == null || data.lastLoginTime == undefined) {
                data.lastLoginTime = COMMON.formatTime(new Date(), "yyyy-MM-dd");
            } else {
                data.lastLoginTime = COMMON.formatTime(data.lastLoginTime, "yyyy-MM-dd");
            }
            this.$el.html(this.template(data));
            if (this.userModel.length == 0) this.$el.find(".ui-empty-data").show();
        },
        events: {
            "click #everySign": "everySign",
            "click #hasInstallPlugin": "hasInstallPlugin",
            "click #editAvatarBut": "editAvatar",
            "mouseover .user-pic": "mouseover",
            "mouseleave .user-pic": "mouseleave",
            "hover #bangbichargebeanhandle": "bangbichargebeanhandle"
        },
        editAvatar: function() {
            require.async([ "arale/dialog/1.2.6/dialog", "modules/uc/default/common/avatar/AvatarView" ], function(Dialog, AvatarView) {
                new Dialog({
                    content: new AvatarView().el,
                    width: "740px",
                    height: "581px"
                }).after("hide", function() {
                    var $avatarImg = root.$el.find("#avatarImg");
                    $avatarImg.attr("src", $avatarImg.attr("src").split("?")[0] + "?_=" + Math.random());
                }).show();
            });
        },
        everySign: function(e) {
            var $evarySign = $(e.currentTarget);
            if ($evarySign.attr("isSign") == "true") return;
            $.getJSON(B5M_UC.rootPath + "/user/task/data/sign.htm?jsonpCallback=?", function(httpObj) {
                if (httpObj.ok) {
                    window.location.href = window.location.href;
                } else {
                    COMMON.showTip(httpObj.data);
                }
            });
        },
        hasInstallPlugin: function(e) {
            require.async([ "modules/uc/2.1.0/user/task/tpl/user-plugin-install-or-sign.html", "arale/dialog/1.2.6/dialog" ], function(html, Dialog) {
                var dialog = new Dialog({
                    content: _.template(html, {
                        flag: root.isInstallPlugin()
                    })
                }).show();
                $(dialog.contentElement).find("#pluginSign").click(function(e) {
                    root.pluginSign(e, dialog);
                });
                $(dialog.contentElement).find("#pluginInstall").click(function(e) {
                    dialog.destroy();
                });
            });
        },
        isInstallPlugin: function() {
            var b1 = !!document.getElementById("b5mmain");
            if (b1) return b1;
            var $obj = $('<span><object type="application/x-bang5taoplugin" width="0" height="0" style="display:none"></object></span>');
            $("body").append($obj);
            return !!$obj.find("object")[0].getCacheValue;
        },
        pluginSign: function(e, dialog) {
            e.preventDefault();
            $.getJSON(B5M_UC.rootPath + "/user/task/data/plug.htm?jsonpCallback=?", {
                isInstall: "1"
            }, function(response) {
                if (response.ok) {
                    window.location.reload();
                } else {
                    dialog.destroy();
                    COMMON.showTip(response.data);
                }
            });
        },
        mouseover: function(e) {
            $(e.currentTarget).find("a").animate({
                bottom: 21
            }, 200);
        },
        mouseleave: function(e) {
            $(e.currentTarget).find("a").animate({
                bottom: -1
            }, 200);
        },
        bangbichargebeanhandle: function(e) {
            $(e.currentTarget).css("text-decoration", "none");
        }
    });
    module.exports = UserInfoView.Views.UserInfoView;
});