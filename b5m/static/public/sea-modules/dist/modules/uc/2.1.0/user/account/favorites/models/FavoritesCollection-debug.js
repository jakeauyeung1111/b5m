define("modules/uc/2.1.0/user/account/favorites/models/FavoritesCollection-debug", [ "$-debug", "underscore-debug", "backbone-debug", "common-debug", "modules/uc/2.1.0/user/account/favorites/models/WebsiteCollection-debug" ], function(require, exports, module) {
    this._ = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    this.COMMON = require("common-debug");
    this.WebsiteCollection = require("modules/uc/2.1.0/user/account/favorites/models/WebsiteCollection-debug");
    var FavoritesCollection = {
        Models: {},
        Collections: {},
        Urls: {}
    };
    FavoritesCollection.Urls.query = B5M_UC.rootPath + "/gc/user/favorites/data/query.htm";
    //	FavoritesCollection.Urls.query = 'http://ucenter.stage.bang5mai.com/gc/user/favorites/data/query.htm';
    FavoritesCollection.Models.Favorites = Backbone.Model.extend({});
    FavoritesCollection.Collections.FavoritesCollection = Backbone.Collection.extend({
        url: FavoritesCollection.Urls.query,
        model: FavoritesCollection.Models.Favorites,
        sync: function(method, model, options) {
            var params = _.extend({
                //				async:false,
                cache: false,
                dataType: "jsonp",
                jsonp: "jsonpCallback",
                // the api requires the jsonp callback name to be this exact name
                type: "POST",
                url: model.url,
                processData: true
            }, options);
            return $.ajax(params);
        },
        parse: function(response) {
            COMMON.checkJSonData(response);
            if (response.ok) return this.initModelData(response.data);
            COMMON.showTip(response.data);
            return null;
        },
        initModelData: function(data) {
            this.dataTotal = data.page.total;
            this.websiteCollection = new WebsiteCollection();
            var list = data.list;
            for (var i = 0; i < list.length; i++) {
                list[i].goodsUrl = "http://www.b5m.com/go.php?sid=ucenter&dest=" + encodeURIComponent(list[i].goodsUrl);
                list[i].webSiteName = this.websiteCollection.getNameByCode(list[i].webSite);
                list[i].favoritesTime = new Date(Date.parse(list[i].favoritesTime)).Format("yyyy-MM-dd");
            }
            return list;
        }
    });
    module.exports = FavoritesCollection.Collections.FavoritesCollection;
});