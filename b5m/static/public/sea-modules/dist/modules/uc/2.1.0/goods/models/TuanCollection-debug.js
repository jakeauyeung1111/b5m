define("modules/uc/2.1.0/goods/models/TuanCollection-debug", [ "$-debug", "backbone-debug", "common-debug" ], function(require, exports, module) {
    this._ = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    this.COMMON = require("common-debug");
    var TuanCollection = {
        Models: {},
        Collections: {},
        Urls: {}
    };
    TuanCollection.Urls.TuanUrl = B5M_UC.rootPath + "/other/goods/data/tuan.htm";
    TuanCollection.Models.Tuan = Backbone.Model.extend({});
    TuanCollection.Collections.Tuan = Backbone.Collection.extend({
        model: TuanCollection.Models.Tuan,
        url: TuanCollection.Urls.TuanUrl,
        parse: function(response) {
            COMMON.checkJSonData(response);
            var data = response;
            for (var i = 0; i < data.length; i++) {
                data[i].autoId = data[i].id;
                delete data[i].id;
            }
            return data;
        },
        sync: function(method, model, options) {
            var params = _.extend({
                //				async:false,
                cache: false,
                dataType: "jsonp",
                jsonp: "jsonpCallback",
                // the api requires the jsonp callback name to be this exact name
                type: "POST",
                url: model.url,
                processData: true
            }, options);
            return $.ajax(params);
        }
    });
    module.exports = TuanCollection.Collections.Tuan;
});