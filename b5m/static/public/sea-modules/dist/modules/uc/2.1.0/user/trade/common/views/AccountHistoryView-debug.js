define("modules/uc/2.1.0/user/trade/common/views/AccountHistoryView-debug", [ "$-debug", "underscore-debug", "backbone-debug", "common-debug", "modules/uc/2.1.0/user/trade/common/models/AccountHistoryCollection-debug", "modules/uc/2.1.0/user/trade/common/views/AccountHistoryItemView-debug", "modules/uc/default/common/Page-debug", "http://www.my97.net/dp/My97DatePicker/WdatePicker-debug.js", "modules/uc/2.1.0/user/trade/common/views/TradeNoResultView-debug" ], function(require, exports, module) {
    this._ = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    this.COMMON = require("common-debug");
    this.AccountHistoryCollection = require("modules/uc/2.1.0/user/trade/common/models/AccountHistoryCollection-debug");
    this.AccountHistoryItemView = require("modules/uc/2.1.0/user/trade/common/views/AccountHistoryItemView-debug");
    this.TradeNoResultView = require("modules/uc/2.1.0/user/trade/common/views/TradeNoResultView-debug");
    this.Page = require("modules/uc/default/common/Page-debug");
    this.tpl_account_history = "modules/uc/2.1.0/user/trade/common/tpl/account-history.html";
    this.tpl_account_history_exchange = "modules/uc/2.1.0/user/trade/common/tpl/account-history-exchange.html";
    var AccountHistoryView = {
        Views: {},
        Urls: {}
    };
    AccountHistoryView.Views.AccountHistoryView = Backbone.View.extend({
        initialize: function(opt) {
            //初始化参数
            this.parameterData = this.initParameter(opt);
            this.el = opt.el;
            var tpl_addr = tpl_account_history, self = this;
            if (this.parameterData.tplName == "default") tpl_addr = tpl_account_history;
            if (this.parameterData.tplName == "exchange") tpl_addr = tpl_account_history_exchange;
            require.async(tpl_addr, function(tpl) {
                self.template = _.template(tpl);
                self.$el.html(self.template({
                    startTimeId: self.parameterData.startTimeId,
                    endTimeId: self.parameterData.endTimeId
                }));
                //初始化交易记录
                self.accountHistoryCollection = new AccountHistoryCollection();
                self.pageHandler({
                    pageNum: 1
                });
                //初始化日期控件
                self.initDatepicker();
            });
        },
        initDatepicker: function() {
            var $startTime = this.$el.find("#" + this.parameterData.startTimeId), $endTime = this.$el.find("#" + this.parameterData.endTimeId), self = this;
            var currentDate = COMMON.formatDate(new Date(), "yyyy-MM-dd");
            $startTime.focus(function() {
                WdatePicker({
                    maxDate: "#F{$dp.$D('" + self.parameterData.endTimeId + "')||'" + currentDate + "'}"
                });
            });
            $endTime.focus(function() {
                WdatePicker({
                    minDate: "#F{$dp.$D('" + self.parameterData.startTimeId + "')}",
                    maxDate: currentDate
                });
            });
        },
        initParameter: function(para) {
            return _.extend({
                pageNum: 1,
                pageSize: 10,
                currencyType: 0,
                startTime: "",
                endTime: "",
                showPage: false,
                direction: 0,
                orderCode: "",
                tplName: "default",
                startTimeId: "startTimeId",
                endTimeId: "endTimeId"
            }, para);
        },
        render: function() {
            this.accountHistoryCollection.each(function(item, index) {
                this.renderData(item, index);
            }, this);
        },
        renderData: function(item, index) {
            var itemClass = "";
            if (index % 2 != 0) itemClass = "trEvenBg";
            var accountHistoryItemView = new AccountHistoryItemView({
                model: item,
                attributes: {
                    "class": itemClass
                },
                tplName: this.parameterData.tplName
            });
            this.$el.find("tbody").append(accountHistoryItemView.el);
        },
        pageHandler: function(opt) {
            //加载交易记录
            var self = this;
            this.accountHistoryCollection.fetch({
                data: {
                    userId: "123456",
                    pageNum: opt.pageNum,
                    pageSize: self.parameterData.pageSize,
                    currencyType: self.parameterData.currencyType,
                    startTime: self.parameterData.startTime,
                    endTime: self.parameterData.endTime,
                    direction: self.parameterData.direction,
                    orderCode: self.parameterData.orderCode
                },
                async: false,
                cache: false
            });
            this.hideLodingData();
            //数据为空时处理
            if (this.accountHistoryCollection.length == 0) {
                //require.async(tpl_index_no_trade, function(tpl) {
                self.$el.find(".ui-empty-data").empty().append(new TradeNoResultView().el).show();
                //self.accordionFun(opt.direction);
                //});
                return;
            }
            //渲染交易记录数据
            this.render();
            //刷新分页控件
            if (this.accountHistoryCollection.length > 0) this.refreshPage(opt);
        },
        refreshPage: function(opt) {
            //初始化分页控件
            if (!this.page) {
                var self = this;
                this.page = new Page({
                    onClick: function(pageNum, dom) {
                        self.pageHandler({
                            pageNum: pageNum
                        });
                    }
                });
            }
            this.$el.find(".page-view").empty().append(this.page.getPageDom({
                pageNum: opt.pageNum,
                total: this.accountHistoryCollection.dataTotal,
                pageSize: this.parameterData.pageSize
            }));
        },
        events: {
            "click .select-trade-but": "conditionsQuery"
        },
        conditionsQuery: function(e) {
            $(e.currentTarget).wait("btn btn-5 l waiting", "btn btn-5 l select-trade-but", 500);
            var startTime = this.string2Date(this.$el.find("#" + this.parameterData.startTimeId).val()).getTime();
            var endTime = this.string2Date(this.$el.find("#" + this.parameterData.endTimeId).val()).getTime();
            this.parameterData.startTime = startTime ? COMMON.formatTime(startTime, "yyyy-MM-dd hh:mm:ss") : "";
            this.parameterData.endTime = endTime ? COMMON.formatTime(endTime, "yyyy-MM-dd hh:mm:ss") : "";
            this.showLoadingData();
            this.pageHandler({
                pageNum: 1
            });
        },
        string2Date: function(date) {
            return new Date(Date.parse(date.replace(/-/g, "/")));
        },
        showLoadingData: function() {
            this.$el.find(".ui-loading").show();
            this.$el.find(".page-view").empty();
            this.$el.find("tbody").empty();
            this.$el.find(".ui-empty-data").hide();
        },
        hideLodingData: function() {
            this.$el.find(".ui-loading").hide();
            this.$el.find(".page-view").empty();
            this.$el.find("tbody").empty();
            this.$el.find(".ui-empty-data").hide();
        }
    });
    module.exports = AccountHistoryView.Views.AccountHistoryView;
});