/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/uc/2.1.0/user/account/invitation/views/InvitationView",["$","common","underscore","backbone","modules/uc/2.1.0/user/account/invitation/models/InvitationDetailModel","modules/uc/2.1.0/user/account/invitation/tpl/invitation.html","jquery/ZeroClipboard/1.2.3/ZeroClipboard","share/1.0.0/share"],function(e,t,i){this.COMMON=e("common"),this._=e("underscore"),this.Backbone=e("backbone"),this.InvitationDetailModel=e("modules/uc/2.1.0/user/account/invitation/models/InvitationDetailModel"),this.ZeroClipboard=e("jquery/ZeroClipboard/1.2.3/ZeroClipboard"),this.Share=e("share/1.0.0/share"),this.tpl_invitation=e("modules/uc/2.1.0/user/account/invitation/tpl/invitation.html");var o=Backbone.View.extend({template:_.template(tpl_invitation),tagName:"div",initialize:function(e){this.parameterData=this.initParameter(e),this.invitationDetailModel=new InvitationDetailModel;var t=this;this.invitationDetailModel.fetch({success:function(){t.parameterData=_.extend(t.parameterData,t.invitationDetailModel.toJSON()),t.render()}})},initParameter:function(e){return _.extend({title:"邀请好友来帮5买",summary:"有个网购省钱利器我必须告诉你，搜一下商品就能省很多Money！还有更多优惠方式，还不立刻行动？"},e)},render:function(){this.$el.find(".ui-loading").hide();try{this.$el.append(this.template(this.parameterData))}catch(e){COMMON.showTip("出错啦！！！赶紧去买六合彩吧！！！")}var t=new ZeroClipboard(this.$el.find("#copy-but")[0],{moviePath:B5M_JS.jsPath+"/jquery/ZeroClipboard/1.2.3/ZeroClipboard.swf"});t.on("complete",function(){COMMON.showTip("复制成功!!!")}),new Share({id:this.$el.find(".deal-share")[0],href:this.invitationDetailModel.get("invitationUrl"),content:this.parameterData.summary})}});i.exports=o});