define("modules/uc/2.1.0/user/trade/common/views/TradeBatchView-debug", [ "$-debug", "underscore-debug", "backbone-debug", "common-debug", "modules/uc/2.1.0/user/trade/common/models/TradeBatchCollection-debug", "modules/uc/2.1.0/user/trade/common/views/TradeBatchItemView-debug", "modules/uc/default/common/Page-debug", "http://www.my97.net/dp/My97DatePicker/WdatePicker-debug.js", "modules/uc/2.1.0/user/trade/common/views/TradeNoResultView-debug", "modules/uc/2.1.0/user/trade/common/tpl/trade-batch-debug.html" ], function(require, exports, module) {
    this._ = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    this.COMMON = require("common-debug");
    this.TradeBatchCollection = require("modules/uc/2.1.0/user/trade/common/models/TradeBatchCollection-debug");
    this.TradeBatchItemView = require("modules/uc/2.1.0/user/trade/common/views/TradeBatchItemView-debug");
    this.TradeNoResultView = require("modules/uc/2.1.0/user/trade/common/views/TradeNoResultView-debug");
    this.Page = require("modules/uc/default/common/Page-debug");
    this.tpl_trade_batch = require("modules/uc/2.1.0/user/trade/common/tpl/trade-batch-debug.html");
    var TradeBatchView = {
        Views: {},
        Urls: {}
    };
    this.root = undefined;
    TradeBatchView.Views.TradeBatchView = Backbone.View.extend({
        template: _.template(tpl_trade_batch),
        initialize: function(opt) {
            //初始化参数
            root = this;
            this.parameterData = this.initParameter(opt);
            this.$el.append(this.template(this.parameterData));
            //初始化交易记录
            this.tradeBatchCollection = new TradeBatchCollection();
            this.pageHandler({
                pageNum: 1
            });
            //初始化日期控件
            this.initDatepicker();
        },
        initParameter: function(para) {
            return _.extend({
                pageNum: 1,
                pageSize: 10,
                currencyType: 0,
                startTime: "",
                endTime: "",
                showPage: false,
                direction: 0,
                startTimeId: "startTimeId",
                endTimeId: "endTimeId"
            }, para);
        },
        initDatepicker: function() {
            var $startTime = this.$el.find("#" + this.parameterData.startTimeId), $endTime = this.$el.find("#" + this.parameterData.endTimeId), self = this;
            var currentDate = COMMON.formatDate(new Date(), "yyyy-MM-dd");
            $startTime.focus(function() {
                WdatePicker({
                    maxDate: "#F{$dp.$D('" + self.parameterData.endTimeId + "')||'" + currentDate + "'}"
                });
            });
            $endTime.focus(function() {
                WdatePicker({
                    minDate: "#F{$dp.$D('" + self.parameterData.startTimeId + "')}",
                    maxDate: currentDate
                });
            });
        },
        render: function() {
            //进度条隐藏
            this.$el.find(".ui-loading").hide();
            this.tradeBatchCollection.each(function(item, index) {
                this.renderData(item, index);
            }, this);
            //数据为空时处理
            if (this.tradeBatchCollection.length == 0) {
                //var self = this;
                //require.async(tpl_index_no_trade, function(tpl) {
                //	self.$el.find('.ui-empty-data').empty().append(tpl).show();
                //});
                this.$el.find(".ui-empty-data").empty().append(new TradeNoResultView().el).show();
                this.$el.find(".page-view").empty();
            } else {
                this.$el.find(".ui-empty-data").hide();
            }
        },
        renderData: function(item, index) {
            var itemClass = "";
            if (index % 2 != 0) itemClass = "even";
            var tradeBatchItemView = new TradeBatchItemView({
                model: item,
                attributes: {
                    "class": itemClass,
                    title: "交易单号:" + item.get("orderId")
                }
            });
            this.$el.find("tbody").append(tradeBatchItemView.el);
        },
        pageHandler: function(opt) {
            //加载交易记录
            var self = this;
            this.tradeBatchCollection.fetch({
                data: {
                    userId: "123456",
                    pageNum: opt.pageNum,
                    pageSize: self.parameterData.pageSize,
                    currencyType: self.parameterData.currencyType,
                    startTime: self.parameterData.startTime,
                    endTime: self.parameterData.endTime,
                    direction: self.parameterData.direction
                },
                async: false,
                cache: false
            });
            //渲染交易记录数据
            this.$el.find("tbody").empty();
            this.render();
            //刷新分页控件
            if (this.tradeBatchCollection.length > 0) this.refreshPage(opt);
        },
        refreshPage: function(opt) {
            //初始化分页控件
            if (!this.page) {
                var self = this;
                this.page = new Page({
                    onClick: function(pageNum, dom) {
                        self.pageHandler({
                            pageNum: pageNum
                        });
                    }
                });
            }
            this.$el.find(".page-view").empty().append(this.page.getPageDom({
                pageNum: opt.pageNum,
                total: this.tradeBatchCollection.dataTotal,
                pageSize: this.parameterData.pageSize
            }));
        },
        events: {
            "click #select-trade-but": "conditionsQuery"
        },
        conditionsQuery: function(e) {
            $(e.currentTarget).wait("center-record-search fl waiting", "center-record-search fl select-trade-but", 500);
            var startTime = this.string2Date(this.$el.find("#" + this.parameterData.startTimeId).val()).getTime();
            var endTime = this.string2Date(this.$el.find("#" + this.parameterData.endTimeId).val()).getTime();
            this.parameterData.startTime = startTime ? COMMON.formatTime(startTime, "yyyy-MM-dd hh:mm:ss") : "";
            this.parameterData.endTime = endTime ? COMMON.formatTime(endTime, "yyyy-MM-dd hh:mm:ss") : "";
            this.parameterData.direction = this.$el.find("#select_val").val();
            this.pageHandler({
                pageNum: 1
            });
        },
        string2Date: function(date) {
            return new Date(Date.parse(date.replace(/-/g, "/")));
        }
    });
    module.exports = TradeBatchView.Views.TradeBatchView;
});