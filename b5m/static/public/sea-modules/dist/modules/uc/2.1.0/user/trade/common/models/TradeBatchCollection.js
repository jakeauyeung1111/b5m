/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/uc/2.1.0/user/trade/common/models/TradeBatchCollection",["$","underscore","backbone","common","modules/uc/2.1.0/user/trade/common/models/OrderCodeCollection"],function(e,t,i){this._=e("underscore"),this.Backbone=e("backbone"),this.COMMON=e("common"),this.OrderCodeCollection=e("modules/uc/2.1.0/user/trade/common/models/OrderCodeCollection");var o={Models:{},Collections:{},Urls:{}};o.Urls.tradeBatchList=B5M_UC.rootPath+"/user/trade/common/data/tradeBatchQuery.htm",o.Models.Trade=Backbone.Model.extend({}),o.Collections.TradeBatchCollection=Backbone.Collection.extend({url:o.Urls.tradeBatchList,model:o.Models.Trade,sync:function(e,t,i){var o=_.extend({async:!1,cache:!1,dataType:"jsonp",jsonp:"jsonpCallback",type:"POST",url:t.url,processData:!0},i);return $.ajax(o)},parse:function(e){return COMMON.checkJSonData(e),e.ok?this.initModelData(e.data):(COMMON.showTip(e.data),null)},initModelData:function(e){var t=new OrderCodeCollection;return _.each(e.list,function(e){e.autoId=e.id,delete e.id,_.each(t.models,function(t){return t.get("code")==e.orderCode?(e._orderName=t.get("name"),void 0):void 0}),void 0==e._orderName&&(e._orderName="")}),this.dataTotal=e.total,e.list}}),i.exports=o.Collections.TradeBatchCollection});