define("modules/uc/2.1.0/user/trade/common/views/ExchangeTradeItemView-debug", [ "$-debug", "common-debug", "underscore-debug", "backbone-debug", "modules/uc/2.1.0/user/trade/common/tpl/exchange-category-item-debug.html", "modules/uc/2.1.0/user/trade/common/views/ExchangeTradeDetailListView-debug" ], function(require, exports, module) {
    this._ = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    this.COMMON = require("common-debug");
    var tpl = require("modules/uc/2.1.0/user/trade/common/tpl/exchange-category-item-debug.html");
    this.ExchangeTradeDetailListView = require("modules/uc/2.1.0/user/trade/common/views/ExchangeTradeDetailListView-debug");
    var ExchangeTradeItemView = Backbone.View.extend({
        template: _.template(tpl),
        events: {
            "click .exchange_category": "query"
        },
        initialize: function(opt) {
            this.render();
        },
        render: function() {
            var html = this.template(this.model.toJSON());
            this.$el.html(html);
            return this;
        },
        query: function() {
            var self_this = this;
            this.$el.parent().parent().find("a").removeClass("current");
            this.$el.find("a").addClass("current");
            new ExchangeTradeDetailListView({
                el: "#exchange-details",
                cateId: self_this.model.id
            });
        }
    });
    module.exports = ExchangeTradeItemView;
});