/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/uc/2.1.0/user/trade/common/views/ExchangeTradeItemView",["$","common","underscore","backbone","modules/uc/2.1.0/user/trade/common/tpl/exchange-category-item.html","modules/uc/2.1.0/user/trade/common/views/ExchangeTradeDetailListView"],function(e,t,i){this._=e("underscore"),this.Backbone=e("backbone"),this.COMMON=e("common");var o=e("modules/uc/2.1.0/user/trade/common/tpl/exchange-category-item.html");this.ExchangeTradeDetailListView=e("modules/uc/2.1.0/user/trade/common/views/ExchangeTradeDetailListView");var a=Backbone.View.extend({template:_.template(o),events:{"click .exchange_category":"query"},initialize:function(){this.render()},render:function(){var e=this.template(this.model.toJSON());return this.$el.html(e),this},query:function(){var e=this;this.$el.parent().parent().find("a").removeClass("current"),this.$el.find("a").addClass("current"),new ExchangeTradeDetailListView({el:"#exchange-details",cateId:e.model.id})}});i.exports=a});