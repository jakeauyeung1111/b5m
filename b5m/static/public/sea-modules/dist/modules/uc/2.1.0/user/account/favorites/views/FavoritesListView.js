/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/uc/2.1.0/user/account/favorites/views/FavoritesListView",["$","underscore","backbone","common","modules/uc/default/common/Page","modules/uc/2.1.0/user/account/favorites/models/FavoritesCollection","modules/uc/2.1.0/user/account/favorites/views/FavoritesListItemView","arale/cookie/1.0.2/cookie"],function(e,t,i){this._=e("underscore"),this.Backbone=e("backbone"),this.COMMON=e("common"),this.Page=e("modules/uc/default/common/Page"),this.tpl_favorites="modules/uc/2.1.0/user/account/favorites/tpl/favorites-list.html",this.FavoritesCollection=e("modules/uc/2.1.0/user/account/favorites/models/FavoritesCollection"),this.FavoritesListItemView=e("modules/uc/2.1.0/user/account/favorites/views/FavoritesListItemView"),this.Cookie=e("arale/cookie/1.0.2/cookie");var o={Views:{},Urls:{}};o.Views.FavoritesListView=Backbone.View.extend({initialize:function(t){this.parameterData=this.initParameter(t);var i=tpl_favorites,o=this;"default"==this.parameterData.tplName&&(i=tpl_favorites),e.async(i,function(e){o.$el.append(e),o.favoritesCollection=new FavoritesCollection,o.pageHandler({pageNum:1})})},initParameter:function(e){return _.extend({pageNum:1,pageSize:10,showPage:!0,tplName:"default"},e)},render:function(){this.favoritesCollection.each(function(e,t){this.renderData(e,t)},this)},renderData:function(e){var t=new FavoritesListItemView({model:e,FavoritesListView:this});this.$el.find("#favorites-result").append(t.el)},pageHandler:function(e){var t=this;this.favoritesCollection.fetch({data:{userId:Cookie.get("token"),pageNum:e.pageNum,pageSize:t.parameterData.pageSize,priceType:this.parameterData.priceType,webSite:this.parameterData.webSite},success:function(){return t.hideLodingData(),0==t.favoritesCollection.length?(t.$el.find("#favorites-none").show(),t.appendShowView(),void 0):(t.render(),t.favoritesCollection.length>0&&t.refreshPage(e),t.appendShowView(),void 0)}})},appendShowView:function(){this.parameterData.$el&&this.parameterData.$el.append(this.el)},refreshPage:function(e){if(!this.page){var t=this;this.page=new Page({onClick:function(e){t.pageHandler({pageNum:e})}})}var i=this.page.getPageDom({pageNum:e.pageNum,total:this.favoritesCollection.dataTotal,pageSize:this.parameterData.pageSize});this.parameterData.showPage&&this.$el.find(".page-view").show().empty().append(i)},showLoadingData:function(){this.$el.find(".ui-loading").show(),this.$el.find(".page-view").empty(),this.$el.find("#favorites-result").empty(),this.$el.find("#favorites-none").hide()},hideLodingData:function(){this.$el.find(".ui-loading").hide(),this.$el.find(".page-view").empty(),this.$el.find("#favorites-result").empty(),this.$el.find("#favorites-none").hide()}}),i.exports=o.Views.FavoritesListView});