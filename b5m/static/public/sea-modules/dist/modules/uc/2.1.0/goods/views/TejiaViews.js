/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/uc/2.1.0/goods/views/TejiaViews",["$","underscore","backbone","common","modules/uc/2.1.0/goods/views/TejiaViewItem","modules/uc/2.1.0/goods/models/TejiaCollection"],function(e,t,i){this.COMMON=e("common"),this.underscore=e("underscore"),this.Backbone=e("backbone"),this.TejiaView=e("modules/uc/2.1.0/goods/views/TejiaViewItem"),this.TejiaCollection=e("modules/uc/2.1.0/goods/models/TejiaCollection");var o=void 0;this.TejiaViews=Backbone.View.extend({template:_.template('<ul class="cfx"></ul>'),initialize:function(e){o=this,this.$el.append(this.template(e)),this.render()},render:function(){this.$el.find(".ui-loading").hide(),this.tejiaCollection=new TejiaCollection,this.tejiaCollection.fetch({success:function(){o.showTejiaList(),0==o.tejiaCollection.length&&o.$el.append('<div class="ui-empty-data">我们正在为您准备推荐商品...</div>').show()}})},showTejiaList:function(){this.tejiaCollection.each(function(e,t){var i="";0==(t+1)%4&&(i="last");var o=new TejiaView({model:e,attributes:{"class":i}});this.$el.find("ul").append(o.el)},this)}}),i.exports=TejiaViews});