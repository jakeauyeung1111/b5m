/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/uc/2.1.0/user/task/models/UserTaskCollection",["$","underscore","backbone","common"],function(e,t,i){this._=e("underscore"),this.Backbone=e("backbone"),this.COMMON=e("common");var o={Models:{},Collections:{},Urls:{}};o.Urls.TaskList=B5M_UC.rootPath+"/user/task/data/list.htm",o.Models.UserTask=Backbone.Model.extend({}),o.Collections.UserTaskCollection=Backbone.Collection.extend({url:o.Urls.TaskList,model:o.Models.UserTask,sync:function(e,t,i){var o=_.extend({async:!1,cache:!1,dataType:"jsonp",jsonp:"jsonpCallback",type:"POST",url:t.url,processData:!0},i);return $.ajax(o)},parse:function(e){return COMMON.checkJSonData(e),e.ok?this.initModelData(e.data):(COMMON.showTip(e.data),null)},initModelData:function(e){for(var t=0;e.length>t;t++)this.stringStartWithString("http:",e[t].rulesImg)||(e[t].rulesImg=B5M_UC.rootPath+e[t].rulesImg),e[t].useIs?(e[t].actionName="已"+e[t].actionName,e[t].actionUrl="javascript:void(0);"):(e[t].actionName="去"+e[t].actionName,this.stringStartWithString("http:",e[t].actionUrl)||(e[t].actionUrl=B5M_UC.rootPath+e[t].actionUrl));return e},stringStartWithString:function(e,t){return 0==$.trim(t).indexOf(e)?!0:!1},comparator:function(e,t){return e.get("rulesSort")>t.get("rulesSort")?!0:!1}}),i.exports.UserTaskCollection=o.Collections.UserTaskCollection});