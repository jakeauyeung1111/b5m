define("modules/uc/2.1.0/user/trade/common/views/ExchangeTradeView-debug", [ "$-debug", "common-debug", "underscore-debug", "backbone-debug", "modules/uc/2.1.0/user/trade/common/models/ExchangeTradeCollection-debug", "modules/uc/2.1.0/user/trade/common/views/ExchangeTradeItemView-debug" ], function(require, exports, module) {
    this._ = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    this.COMMON = require("common-debug");
    this.ExchangeTradeCollection = require("modules/uc/2.1.0/user/trade/common/models/ExchangeTradeCollection-debug");
    var ExchangeTradeItemView = require("modules/uc/2.1.0/user/trade/common/views/ExchangeTradeItemView-debug");
    var ExchangeTradeView = Backbone.View.extend({
        initialize: function() {
            this.categorysCollection = new ExchangeTradeCollection.categorys();
            var self_this = this;
            this.categorysCollection.fetch({
                success: function() {
                    self_this.render();
                },
                error: function(e) {
                    console.log(e);
                }
            });
        },
        render: function() {
            this.categorysCollection.each(function(item, index) {
                var myExchangeTradeItemView = new ExchangeTradeItemView({
                    model: item
                });
                this.$el.append(myExchangeTradeItemView.el);
            }, this);
        }
    });
    module.exports = ExchangeTradeView;
});