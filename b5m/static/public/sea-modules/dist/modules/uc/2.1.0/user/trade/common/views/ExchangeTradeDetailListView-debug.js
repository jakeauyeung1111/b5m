define("modules/uc/2.1.0/user/trade/common/views/ExchangeTradeDetailListView-debug", [ "$-debug", "common-debug", "underscore-debug", "backbone-debug", "modules/uc/default/common/Page-debug", "arale/cookie/1.0.2/cookie-debug", "modules/uc/2.1.0/user/trade/common/models/ExchangeTradeCollection-debug" ], function(require, exports, module) {
    this._ = require("underscore-debug");
    this.Jq$ = require("$-debug");
    this.Backbone = require("backbone-debug");
    this.COMMON = require("common-debug");
    this.Page = require("modules/uc/default/common/Page-debug");
    this.Cookie = require("arale/cookie/1.0.2/cookie-debug");
    this.ExchangeTradeCollection = require("modules/uc/2.1.0/user/trade/common/models/ExchangeTradeCollection-debug");
    var ExchangeTradeDetailListView = Backbone.View.extend({
        initialize: function(opt) {
            //初始化参数
            this.parameterData = this.initParameter(opt);
            this.categoryDetails = new ExchangeTradeCollection.categoryDetails();
            //分页处理
            this.pageHandler({
                pageNum: 1,
                cateId: opt.cateId
            });
        },
        initParameter: function(para) {
            return _.extend({
                pageNum: 1,
                pageSize: 12,
                showPage: true
            }, para);
        },
        render: function() {
            var categoryDetailsCollection = this.categoryDetails.toJSON();
            var defaultHtml = "modules/uc/2.1.0/user/trade/common/tpl/exchange-category-details.html";
            var self_this = this;
            require.async(defaultHtml, function(html) {
                self_this.template = _.template(html);
                if (categoryDetailsCollection[0].totalPages == 0) {
                    self_this.$el.html("");
                    $("#exchange-none").show();
                    return;
                }
                $("#exchange-none").hide();
                var htmlDom = self_this.template({
                    detailList: categoryDetailsCollection[0].datas
                });
                self_this.$el.html(htmlDom);
                $(function() {
                    //兑换中心优惠券
                    var couponList = $("#J_coupon").find("li");
                    couponList.has(".exchange-coupons").mouseenter(function() {
                        $(this).find(".exchange-coupons").stop().show().animate({
                            top: 0
                        }, 500);
                    }).mouseleave(function() {
                        $(this).find(".exchange-coupons").stop().animate({
                            top: "138px"
                        }, 500, function() {
                            $(this).hide();
                        });
                    });
                });
            });
        },
        pageHandler: function(opt) {
            var self = this;
            this.categoryDetails.fetch({
                data: {
                    userId: Cookie.get("token"),
                    pageNum: opt.pageNum,
                    pageSize: self.parameterData.pageSize,
                    cateId: opt.cateId
                },
                success: function(response) {
                    self.hideLodingData();
                    var categoryDetailsCollection = response.toJSON();
                    //如果无数处理,隐藏分页
                    if (categoryDetailsCollection[0].totalPages > 0) {
                        self.$el.parent().find(".page-view").show();
                    }
                    //渲染列表数据
                    self.render();
                    //刷新列表数据的分页控件
                    self.refreshPage(opt);
                    //显示展示页
                    self.appendShowView();
                },
                error: function(response) {
                    console.log(response);
                }
            });
        },
        appendShowView: function() {
            this.parameterData.$el && this.parameterData.$el.append(this.el);
        },
        hideLodingData: function() {
            this.$el.find(".ui-loading").hide();
            this.$el.parent().find(".page-view").hide();
        },
        refreshPage: function(opt) {
            //初始化分页控件
            if (!this.page) {
                var self = this;
                this.page = new Page({
                    onClick: function(pageNum, dom) {
                        self.pageHandler({
                            pageNum: pageNum,
                            cateId: opt.cateId
                        });
                    }
                });
            }
            var totalCount = this.categoryDetails.toJSON()[0].totalCount;
            var pageDom = this.page.getPageDom({
                pageNum: opt.pageNum,
                total: totalCount,
                pageSize: this.parameterData.pageSize
            });
            if (this.parameterData.showPage) {
                this.$el.parent().find(".page-view").html(pageDom);
            }
        }
    });
    module.exports = ExchangeTradeDetailListView;
});