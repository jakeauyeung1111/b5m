define("modules/uc/2.1.0/ads/models/AdsCollection-debug", [ "$-debug", "backbone-debug", "underscore-debug", "common-debug" ], function(require, exports, module) {
    this._ = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    this.COMMON = require("common-debug");
    var AdsCollection = {
        Models: {},
        Collections: {},
        Urls: {}
    };
    AdsCollection.Urls.AdsUrl = "http://sa1.b5m.com:58080/B5MCMS/ucenter/getImgs.do";
    //AdsCollection.Urls.AdsUrl = 'http://ucenter.stage.bang5mai.com/user/user/data/info.htm?isSimple=1';
    AdsCollection.Models.Ad = Backbone.Model.extend({
        url: AdsCollection.Urls.AdsUrl,
        sync: function(method, model, options) {
            var params = _.extend({
                async: false,
                cache: false,
                dataType: "jsonp",
                jsonp: "jsonpCallback",
                // the api requires the jsonp callback name to be this exact name
                type: "POST",
                url: model.url,
                processData: true
            }, options);
            return $.ajax(params);
        },
        parse: function(response) {
            if (response.ok) return response.data;
            return response;
        },
        initialize: function(options) {
            var adPosition = COMMON.isEmptyValue(options) ? 1 : options.position;
            this.url = AdsCollection.Urls.AdsUrl + "?position=" + adPosition;
        }
    });
    module.exports = AdsCollection.Models.Ad;
});