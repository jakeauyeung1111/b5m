define("modules/uc/2.1.0/user/task/views/UserTaskView-debug", [ "$-debug", "underscore-debug", "backbone-debug", "common-debug", "modules/uc/2.1.0/user/task/models/UserTaskCollection-debug", "modules/uc/2.1.0/user/task/views/UserTaskItemView-debug", "modules/uc/2.1.0/user/task/tpl/task-debug.html" ], function(require, exports, module) {
    this._ = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    this.COMMON = require("common-debug");
    this.UserTaskCollection = require("modules/uc/2.1.0/user/task/models/UserTaskCollection-debug");
    this.UserTaskItemView = require("modules/uc/2.1.0/user/task/views/UserTaskItemView-debug");
    this.tpl_task = require("modules/uc/2.1.0/user/task/tpl/task-debug.html");
    this.tpl_task_jqqd = "modules/uc/2.1.0/user/task/tpl/task-jqqd.html";
    var UserTaskView = {
        Views: {},
        Urls: {}
    };
    this.root = undefined;
    UserTaskView.Views.UserTaskView = Backbone.View.extend({
        template: _.template(tpl_task),
        initialize: function(opt) {
            root = this;
            //初始化参数
            this.parameterData = this.initParameter(opt);
            root.$el.html(root.template(this.parameterData));
            this.userTaskCollection = new UserTaskCollection.UserTaskCollection();
            this.userTaskCollection.fetch({
                cache: false,
                success: function() {
                    root.render();
                }
            });
        },
        initParameter: function(para) {
            return _.extend({
                showSize: undefined,
                taskItemTpl: 1,
                dataNotIn: undefined,
                showOtherTask: true,
                showHead: true
            }, para);
        },
        render: function() {
            this.$el.find(".ui-loading").hide();
            var tempNum = 0;
            this.userTaskCollection.each(function(item, index) {
                if (this.parameterData.showSize != undefined && tempNum >= this.parameterData.showSize) return;
                if (this.parameterData.dataNotIn != undefined) {
                    for (var key in this.parameterData.dataNotIn) {
                        if (this.parameterData.dataNotIn[key] == item.get(key)) return;
                    }
                }
                this.renderTask(item, tempNum);
                tempNum++;
            }, this);
            if (this.parameterData.showOtherTask) {
                var self = this;
                require.async(tpl_task_jqqd, function(tpl) {
                    self.$el.find("tbody").append(tpl);
                });
            }
            if (this.userTaskCollection.length == 0) this.$el.find(".ui-empty-data").show();
        },
        renderTask: function(item, index) {
            var itemClass = "even";
            if (index % 2 != 0) itemClass = "odd";
            var userTaskItemView = new UserTaskItemView.UserTaskItemView({
                model: item,
                taskItemTpl: this.parameterData.taskItemTpl,
                attributes: {
                    "class": itemClass
                }
            });
            this.$el.find("tbody").append(userTaskItemView.el);
        }
    });
    module.exports = UserTaskView.Views.UserTaskView;
});