define("modules/uc/2.1.0/user/account/security/mobile/views/BindMobileView-debug", [ "$-debug", "common-debug", "underscore-debug", "backbone-debug", "jquery/validate/1.11.1/validate-debug", "modules/uc/2.1.0/user/account/info/models/UserModel-debug" ], function(require, exports, module) {
    this.COMMON = require("common-debug");
    this.underscore = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    this.UserCenterCollection = require("modules/uc/2.1.0/user/account/info/models/UserModel-debug");
    var BindMobileView = Backbone.View.extend({
        initialize: function(opt) {
            this.el = opt.el;
            var self = this;
            require.async("modules/uc/2.1.0/user/account/security/mobile/tpl/bind-mobile.html", function(html) {
                self.template = _.template(html);
                self.data = new UserCenterCollection.UserModel({
                    type: 1
                }).toJSON();
                self.$el.html(self.template(self.data));
                self.initValidateForm();
                self.keyPressEvent();
            });
        },
        initValidateForm: function() {
            //验证
            var $sendMsgToMobileForm = $("#sendMsgToMobileForm");
            var $validateCodeForm = $("#validateCodeForm");
            var self_this = this;
            jQuery.validator.addMethod("isMobileOK", function(value, element) {
                return /^((\(\d{3}\))|(\d{3}\-))?1\d{10}$/.test(value);
            }, "请输入合法的手机号码");
            jQuery.validator.addMethod("isMobileUse", function(value, element) {
                var isOk = false;
                $.ajax({
                    type: "post",
                    data: "mobile=" + value,
                    async: false,
                    url: B5M_UC.rootPath + "/user/info/data/isMobileUse.htm",
                    success: function(r) {
                        isOk = eval(r).ok;
                    }
                });
                return isOk;
            }, "手机号码已经被占用，请输入新的号码");
            $sendMsgToMobileForm.validate({
                rules: {
                    mobile: {
                        required: true,
                        isMobileOK: true,
                        isMobileUse: true,
                        rangelength: [ 11, 12 ]
                    }
                },
                messages: {
                    mobile: {
                        required: "手机不能为空",
                        rangelength: "手机长度必11位"
                    }
                },
                errorElement: "span",
                errorClass: "text_error",
                errorPlacement: function(error, element) {
                    var errorBox = element.parent().parent().find(".cl-999").empty();
                    errorBox.append(error.html());
                },
                success: function(label, d) {
                    var parent = $(d).parent().parent().find(".cl-999").empty();
                },
                onclick: false
            });
            $validateCodeForm.validate({
                rules: {
                    code: {
                        required: true,
                        rangelength: [ 6, 7 ]
                    }
                },
                messages: {
                    code: {
                        required: "验证码不能为空",
                        rangelength: "验证码长度6位"
                    }
                },
                errorElement: "span",
                errorClass: "text_error",
                errorPlacement: function(error, element) {
                    var errorBox = element.parent().parent().find(".cl-999").empty();
                    errorBox.append(error.html());
                },
                success: function(label, d) {
                    var parent = $(d).parent().parent().find(".cl-999").empty();
                },
                onclick: false
            });
            $("#getValidatCode").click(function(e) {
                e.preventDefault();
                if (!COMMON.isEmptyValue(self_this.data.mobile)) {
                    self_this.getValidatCode(e);
                } else {
                    if ($sendMsgToMobileForm.valid()) {
                        self_this.getValidatCode(e);
                    }
                }
            });
            $("#validateMobileCode").click(function(e) {
                e.preventDefault();
                if ($validateCodeForm.valid()) {
                    self_this.validateMobileCode(e);
                }
            });
        },
        getValidatCode: function(e) {
            var time = 59;
            $(e.currentTarget).addClass("waiting").val("重新发送验证码（" + time + "）");
            //当前不可操作
            $(e.currentTarget).attr({
                disabled: true
            });
            //设置可操作样式
            $("#code").attr({
                disabled: false
            });
            $("#validateMobileCode").removeClass("btn btn-grey btn--getcode get_code").addClass("btn btn-yellow btn--getcode get_code");
            $("#validateMobileCode").attr({
                disabled: false
            });
            var timeTask = setInterval(function() {
                if (time <= 0) {
                    $(e.currentTarget).removeClass("waiting").val("重新获取验证码");
                    $(e.currentTarget).attr({
                        disabled: false
                    });
                    clearInterval(timeTask);
                    return;
                }
                $(e.currentTarget).val("重新发送验证码（" + --time + "）");
            }, 1e3);
            $.ajax({
                type: "post",
                data: "mobile=" + $("#mobile").val(),
                async: false,
                url: B5M_UC.rootPath + "/user/info/mobile/getCode.htm",
                success: function(response) {
                    COMMON.checkJSonData(response);
                    if (response.ok == false) {
                        COMMON.showTip(response.data);
                    }
                }
            });
        },
        validateMobileCode: function(e) {
            $.post(B5M_UC.rootPath + "/user/info/mobile/validateCode.htm", $(e.currentTarget).parents("form:first").serialize(), function(response) {
                COMMON.checkJSonData(response);
                if (response.ok == false) {
                    COMMON.showTip(response.data);
                    return;
                }
                window.location.href = B5M_UC.rootPath + "/forward.htm?method=/user/account/security/mobile/bindMobileOk";
            });
        },
        keyPressEvent: function() {
            $("#mobile").keydown(function(event) {
                if (event.keyCode == 13 || event.which == 13) {
                    return false;
                }
                return true;
            });
        }
    });
    module.exports = BindMobileView;
});