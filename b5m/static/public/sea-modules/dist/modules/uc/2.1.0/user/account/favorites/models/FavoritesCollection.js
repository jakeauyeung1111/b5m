/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/uc/2.1.0/user/account/favorites/models/FavoritesCollection",["$","underscore","backbone","common","modules/uc/2.1.0/user/account/favorites/models/WebsiteCollection"],function(e,t,i){this._=e("underscore"),this.Backbone=e("backbone"),this.COMMON=e("common"),this.WebsiteCollection=e("modules/uc/2.1.0/user/account/favorites/models/WebsiteCollection");var o={Models:{},Collections:{},Urls:{}};o.Urls.query=B5M_UC.rootPath+"/gc/user/favorites/data/query.htm",o.Models.Favorites=Backbone.Model.extend({}),o.Collections.FavoritesCollection=Backbone.Collection.extend({url:o.Urls.query,model:o.Models.Favorites,sync:function(e,t,i){var o=_.extend({cache:!1,dataType:"jsonp",jsonp:"jsonpCallback",type:"POST",url:t.url,processData:!0},i);return $.ajax(o)},parse:function(e){return COMMON.checkJSonData(e),e.ok?this.initModelData(e.data):(COMMON.showTip(e.data),null)},initModelData:function(e){this.dataTotal=e.page.total,this.websiteCollection=new WebsiteCollection;for(var t=e.list,i=0;t.length>i;i++)t[i].goodsUrl="http://www.b5m.com/go.php?sid=ucenter&dest="+encodeURIComponent(t[i].goodsUrl),t[i].webSiteName=this.websiteCollection.getNameByCode(t[i].webSite),t[i].favoritesTime=new Date(Date.parse(t[i].favoritesTime)).Format("yyyy-MM-dd");return t}}),i.exports=o.Collections.FavoritesCollection});