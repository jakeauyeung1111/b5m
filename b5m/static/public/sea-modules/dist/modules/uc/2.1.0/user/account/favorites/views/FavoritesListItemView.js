/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/uc/2.1.0/user/account/favorites/views/FavoritesListItemView",["$","underscore","backbone","common","modules/uc/2.1.0/user/account/favorites/tpl/favorites-list-item.html","arale/cookie/1.0.2/cookie","arale/dialog/1.2.6/confirmbox"],function(e,t,i){this._=e("underscore"),this.Backbone=e("backbone"),this.COMMON=e("common"),this.ConfirmBox=e("arale/dialog/1.2.6/confirmbox"),this.tpl=e("modules/uc/2.1.0/user/account/favorites/tpl/favorites-list-item.html"),this.Cookie=e("arale/cookie/1.0.2/cookie"),this.delUrl=B5M_UC.rootPath+"/gc/user/favorites/data/delete.htm?jsonpCallback=?";var o={Views:{}};o.Views.FavoritesListItemView=Backbone.View.extend({template:_.template(tpl),tagName:"li",initialize:function(e){this.FavoritesListView=e.FavoritesListView,this.$el.html(this.template(this.model.toJSON())),this.render()},events:{"click .del-favorites-but":"delFavoritesById"},delFavoritesById:function(e){var t=this,i=$(e.currentTarget);new ConfirmBox({trigger:e.currentTarget,title:"我的收藏",message:"请确认是否删除收藏的宝贝？",onConfirm:function(){$.getJSON(delUrl,{userId:Cookie.get("token"),ugcId:i.attr("attr-id")},function(e){if(COMMON.checkJSonData(e),!e.ok)return COMMON.showTip(e.message),void 0;var i=t.FavoritesListView.page.settings.pageNum;1==t.FavoritesListView.favoritesCollection.length&&i>1&&(i-=1),t.FavoritesListView.pageHandler({pageNum:i})}),this.destroy()},onCancel:function(){this.destroy()},beforeHide:function(){this.destroy()}}).show()}}),i.exports=o.Views.FavoritesListItemView});