define("modules/uc/2.1.0/user/trade/korea/models/OrderCollection-debug", [ "$-debug", "underscore-debug", "backbone-debug", "common-debug" ], function(require, exports, module) {
    this._ = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    this.COMMON = require("common-debug");
    var OrderCollection = {
        Models: {},
        Collections: {},
        Urls: {}
    };
    OrderCollection.Urls.orderList = "http://korea.b5m.com/shop/orderApi.php";
    OrderCollection.Models.Order = Backbone.Model.extend({});
    OrderCollection.Collections.OrderCollection = Backbone.Collection.extend({
        url: OrderCollection.Urls.orderList,
        model: OrderCollection.Models.Order,
        sync: function(method, model, options) {
            var params = _.extend({
                async: false,
                cache: false,
                dataType: "jsonp",
                jsonp: "jsonpCallback",
                // the api requires the jsonp callback name to be this exact name
                type: "POST",
                url: model.url,
                processData: true
            }, options);
            return $.ajax(params);
        },
        parse: function(response) {
            COMMON.checkJSonData(response);
            if (response.ok) return this.initModelData(response.data);
            COMMON.showTip(response.data);
            return null;
        },
        initModelData: function(data) {
            _.each(data.record, function(obj) {
                obj.autoId = obj.id;
                delete obj.id;
            });
            this.dataTotal = data.page.total;
            return data.record;
        }
    });
    module.exports = OrderCollection.Collections.OrderCollection;
});