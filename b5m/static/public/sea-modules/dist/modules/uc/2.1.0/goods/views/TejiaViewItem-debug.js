define("modules/uc/2.1.0/goods/views/TejiaViewItem-debug", [ "$-debug", "underscore-debug", "backbone-debug", "common-debug", "modules/uc/2.1.0/goods/tpl/tejia-item-debug.html" ], function(require, exports, module) {
    this.COMMON = require("common-debug");
    this.underscore = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    var TejiaView = Backbone.View.extend({
        tagName: "li",
        initialize: function() {
            this.render();
        },
        render: function() {
            //加载模版
            if (this.template === undefined) this.template = underscore.template(require("modules/uc/2.1.0/goods/tpl/tejia-item-debug.html"));
            var newDomObjHtml = this.template(this.model.toJSON());
            this.$el.html(newDomObjHtml);
            return this;
        }
    });
    module.exports = TejiaView;
});