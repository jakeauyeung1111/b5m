/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/uc/2.1.0/user/account/info/models/UserModel",["$","underscore","backbone","common"],function(e,t,i){this._=e("underscore"),this.Backbone=e("backbone"),this.COMMON=e("common");var o={Models:{},Urls:{}};o.Urls.UserInfo=B5M_UC.rootPath+"/user/user/data/info.htm",o.Models.UserModel=Backbone.Model.extend({url:o.Urls.UserInfo,sync:function(e,t,i){var o=_.extend({async:!1,cache:!1,dataType:"jsonp",jsonp:"jsonpCallback",type:"POST",url:t.url,processData:!0},i);return $.ajax(o)},parse:function(e){return COMMON.checkJSonData(e),e.ok?this.initModelData(e.data):(COMMON.showTip(e.data),null)},initialize:function(e){if(!COMMON.isEmptyValue(e)){var t=e.type;COMMON.isEmptyValue(t)||(this.url=o.Urls.UserInfo+"?isSimple="+t)}this.fetch()},initModelData:function(e){if(COMMON.isEmptyValue(e.currentResidence))e.cProvince=31,e.cCity=1;else{var t=e.currentResidence.split("-");e.cProvince=t[0],e.cCity=t[1]}if(COMMON.isEmptyValue(e.hometown))e.hProvince=31,e.hCity=1;else{var i=e.hometown.split("-");e.hProvince=i[0],e.hCity=i[1]}if(COMMON.isEmptyValue(e.birthdayStr))e.year=2013,e.month=10,e.day=1,e.birthdayStr="2013-10-1";else{var o=e.birthdayStr,a=o.split("-");e.year=a[0],e.month=Math.abs(a[1]),e.day=Math.abs(a[2]),e.birthdayStr=o}return e}}),i.exports.UserModel=o.Models.UserModel});