/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/uc/2.1.0/user/trade/common/views/ExchangeTradeDetailListView",["$","common","underscore","backbone","modules/uc/default/common/Page","arale/cookie/1.0.2/cookie","modules/uc/2.1.0/user/trade/common/models/ExchangeTradeCollection"],function(e,t,i){this._=e("underscore"),this.Jq$=e("$"),this.Backbone=e("backbone"),this.COMMON=e("common"),this.Page=e("modules/uc/default/common/Page"),this.Cookie=e("arale/cookie/1.0.2/cookie"),this.ExchangeTradeCollection=e("modules/uc/2.1.0/user/trade/common/models/ExchangeTradeCollection");var o=Backbone.View.extend({initialize:function(e){this.parameterData=this.initParameter(e),this.categoryDetails=new ExchangeTradeCollection.categoryDetails,this.pageHandler({pageNum:1,cateId:e.cateId})},initParameter:function(e){return _.extend({pageNum:1,pageSize:12,showPage:!0},e)},render:function(){var t=this.categoryDetails.toJSON(),i="modules/uc/2.1.0/user/trade/common/tpl/exchange-category-details.html",o=this;e.async(i,function(e){if(o.template=_.template(e),0==t[0].totalPages)return o.$el.html(""),$("#exchange-none").show(),void 0;$("#exchange-none").hide();var i=o.template({detailList:t[0].datas});o.$el.html(i),$(function(){var e=$("#J_coupon").find("li");e.has(".exchange-coupons").mouseenter(function(){$(this).find(".exchange-coupons").stop().show().animate({top:0},500)}).mouseleave(function(){$(this).find(".exchange-coupons").stop().animate({top:"138px"},500,function(){$(this).hide()})})})})},pageHandler:function(e){var t=this;this.categoryDetails.fetch({data:{userId:Cookie.get("token"),pageNum:e.pageNum,pageSize:t.parameterData.pageSize,cateId:e.cateId},success:function(i){t.hideLodingData();var o=i.toJSON();o[0].totalPages>0&&t.$el.parent().find(".page-view").show(),t.render(),t.refreshPage(e),t.appendShowView()},error:function(e){console.log(e)}})},appendShowView:function(){this.parameterData.$el&&this.parameterData.$el.append(this.el)},hideLodingData:function(){this.$el.find(".ui-loading").hide(),this.$el.parent().find(".page-view").hide()},refreshPage:function(e){if(!this.page){var t=this;this.page=new Page({onClick:function(i){t.pageHandler({pageNum:i,cateId:e.cateId})}})}var i=this.categoryDetails.toJSON()[0].totalCount,o=this.page.getPageDom({pageNum:e.pageNum,total:i,pageSize:this.parameterData.pageSize});this.parameterData.showPage&&this.$el.parent().find(".page-view").html(o)}});i.exports=o});