define("modules/uc/2.1.0/user/account/favorites/views/WebsiteView-debug", [ "$-debug", "underscore-debug", "backbone-debug", "common-debug", "modules/uc/2.1.0/user/account/favorites/models/WebsiteCollection-debug", "modules/uc/2.1.0/user/account/favorites/views/WebsiteItemView-debug" ], function(require, exports, module) {
    this._ = require("underscore-debug");
    this.Backbone = require("backbone-debug");
    this.COMMON = require("common-debug");
    this.WebsiteCollection = require("modules/uc/2.1.0/user/account/favorites/models/WebsiteCollection-debug");
    this.WebsiteItemView = require("modules/uc/2.1.0/user/account/favorites/views/WebsiteItemView-debug");
    var WebsiteView = {
        Views: {},
        Urls: {}
    };
    WebsiteView.Views.WebsiteView = Backbone.View.extend({
        initialize: function() {
            this.websiteCollection = new WebsiteCollection();
            this.render();
        },
        render: function() {
            this.websiteCollection.each(function(item, index) {
                this.renderData(item, index);
            }, this);
        },
        renderData: function(item, index) {
            var websiteItemView = new WebsiteItemView({
                model: item
            });
            if (index == 0) websiteItemView.$el.find("a").addClass("current");
            this.$el.append(websiteItemView.el);
        }
    });
    module.exports = WebsiteView.Views.WebsiteView;
});