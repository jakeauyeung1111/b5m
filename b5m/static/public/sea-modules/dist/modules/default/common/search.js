/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/default/common/search",["$","jquery/placeholder/jquery.placeholder"],function(e){this.$=e("$"),e("modules/default/common/css/search.css"),$.placeholder=e("jquery/placeholder/jquery.placeholder");var t=e("modules/default/common/tpl/search.html");$("body").children(":first").append(t);var o=$("#J_search"),i=o.find(">input"),a=o.find(">a"),n=$('<div id="downList" class="searchlist"><div class="downList"><ul id="list"></ul></div></div>').appendTo(o.parent()),r=n.find("ul");$(function(){i.placeholder({color:"#ccc",useBrowserPlaceholder:!0,trim:!0,dataName:"original-font-color",className:"placeholder",placeholder:"快速搜索 全网比价"});var e=-1,t=0,o=function(e){if(e==i.data("placeholder"))return i.val("").focus(),!1;if(e=$.trim(e).replace(/_||/g,"").replace(/</gi,"&lt;").replace(/>/gi,"&gt;").replace(/"/g,"&quot;"),e.length>100)return alert("关键词不能超过100个字符，请重新输入"),!1;e=encodeURIComponent(e),e=e.replace(/%2F/g,"/");var t="http://www1.b5m.com/search/s/___image________________%23keyword%23.html";return t.replace("%23keyword%23",e)};i.unbind("input propertychange").bind("input propertychange",function(o){if(!o.originalEvent||!o.originalEvent.propertyName||"value"==o.originalEvent.propertyName){var i=$.trim($(this).val());if(""==i)return n.hide(),!1;var a="http://www1.b5m.com/autofill.htm?collectionName=b5mp&keyWord="+i;$.ajax({type:"GET",url:a,dataType:"jsonp",jsonp:"jsoncallback",timeout:5e3,data:"",success:function(o){if(""!=o){var i="";$.each(o,function(e,t){i+="<li k='"+e+"' keyword=\""+t.value+"\"><a href='javascript:doSubmit("+'"'+t.value+'"'+");' target='_self'><span>"+t.hl_value+"</span></a></li>"}),n.show(),r.empty().html(i).find("li").hover(function(){r.find("a.hover").removeClass(),$(this).find("a").addClass("hover"),e=parseInt($(this).attr("k")||0)},function(){e=-1,$(this).find("a").removeClass("hover")}),t=r.find("li").length}}})}}).bind("keydown",function(a){if("block"==n.css("display")&&38==a.keyCode||40==a.keyCode||13==a.keyCode){if(13==a.keyCode){var s=i.val();-1!=e&&(s=r.find("li").eq(e).attr("keyword"));var l=o(s),d=document.createElement("a");if(document.body.appendChild(d),d.href=l,d.target="_blank",d.click)d.click();else try{var c=document.createEvent("Event");c.initEvent("click",!0,!0),d.dispatchEvent(c)}catch(a){}$(d).remove()}else e=38==a.keyCode?-1>=e-1?e=t-1:e-1:e+1>=t?e=0:e+1,r.find("a.hover").removeClass("hover").end().find("li").eq(e).find("a").addClass("hover");return!1}}).bind("focusin",function(){$(this).addClass("focusin")}).bind("focusout",function(){$(this).removeClass("focusin")}),$(document).bind("mousedown",function(e){!$(e.target).parents("#downList").length&&!$(e.target).parent().is("#downList")&&n.hide()}),window.doSubmit=function(e){var t=e||i.val(),a=o(t);a&&window.open(a,"_blank"),n.hide()},a.attr("hidefocus","true").attr("href","javascript:doSubmit();").attr("target","_self")})});