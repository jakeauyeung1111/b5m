define("modules/default/common/Jobs-debug", [], function(require, exports, module) {
    (function(textArray) {
        var styleRe = /\{([^}]+)\}/g;
        try {
            if (window.console && window.console.log) {
                var i, len, text, args;
                for (i = 0, len = textArray.length; i < len; i++) {
                    args = [ "" ];
                    text = textArray[i];
                    if (styleRe.test(text)) {
                        text = text.replace(styleRe, function($0, $1) {
                            args.push($1);
                            return "%c";
                        });
                    }
                    eval('console.log("' + text + " " + args.join('","') + '")');
                }
            }
        } catch (e) {}
    })([ "帮5买是个好网站呐，各路群雄聚集于此！！！", "我只能以林志玲的口气说：很棒很棒很棒哦～", "加入我们，让购物动次动次动次动次起来~", "请将简历发送至 {color:red}jobs@b5m.cn（邮件标题请以“姓名-应聘XX职位-来自console”命名）", "http://www.b5m.com/about/job", "{font-size:162px;font-family:courier new;width:162px;background:url(http://staticcdn.b5m.com/static/images/common/weixin_b5m.png) no-repeat;} " ]);
});