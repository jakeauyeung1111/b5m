define("modules/default/common/common-debug", [ "$-debug", "modules/default/common/Jobs-debug" ], function(require, exports, module) {
    require("modules/default/common/Jobs-debug");
    // 对Date的扩展，将 Date 转化为指定格式的String
    // 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符， 
    // 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字) 
    // 例子： 
    // (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423 
    // (new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18 
    Date.prototype.Format = function(fmt) {
        var o = {
            "M+": this.getMonth() + 1,
            //月份 
            "d+": this.getDate(),
            //日 
            "h+": this.getHours(),
            //小时 
            "m+": this.getMinutes(),
            //分 
            "s+": this.getSeconds(),
            //秒 
            "q+": Math.floor((this.getMonth() + 3) / 3),
            //季度 
            S: this.getMilliseconds()
        };
        if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (var k in o) if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length));
        return fmt;
    };
    var URLS = {
        LoginPage: B5M_UC.rootPath + "/forward.htm?method=/user/user/login"
    };
    var FUNCTIONS = {
        showTip: function(msg) {
            require.async("modules/default/common/ModalDialogView", function(Dialog) {
                var dialog = new Dialog();
                dialog.showTip(msg);
                setTimeout(function() {
                    dialog.hideModal();
                }, 1e3);
            });
        },
        formatDate: function(date, pattern) {
            if (FUNCTIONS.isEmptyValue(date)) date = new Date();
            if (FUNCTIONS.isEmptyValue(pattern)) pattern = "yyyy-MM-dd hh:mm";
            return date.Format(pattern);
        },
        formatTime: function(time, pattern) {
            if (FUNCTIONS.isEmptyValue(time)) {
                alert("毫米数为空");
                return;
            }
            var date = new Date();
            date.setTime(time);
            return FUNCTIONS.formatDate(date, pattern);
        },
        isEmptyValue: function(value) {
            var type;
            if (value == null) {
                // 等同于 value === undefined || value === null
                return true;
            }
            type = Object.prototype.toString.call(value).slice(8, -1);
            switch (type) {
              case "String":
                return !$.trim(value);

              case "Array":
                return !value.length;

              case "Object":
                return $.isEmptyObject(value);

              // 普通对象使用 for...in 判断，有 key 即为 false
                default:
                return false;
            }
        },
        checkJSonData: function(data) {
            if (data == null || data == undefined) {} else if (data.code == 10008) {
                window.location.href = URLS.LoginPage + "&loginReferer=" + encodeURIComponent(window.location.href);
            }
        }
    };
    module.exports = {
        showTip: FUNCTIONS.showTip,
        formatDate: FUNCTIONS.formatDate,
        formatTime: FUNCTIONS.formatTime,
        isEmptyValue: FUNCTIONS.isEmptyValue,
        checkJSonData: FUNCTIONS.checkJSonData
    };
});