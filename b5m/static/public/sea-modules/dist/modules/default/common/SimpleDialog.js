/*!

JYJCnCJCnttjv      iICttntntnCzU   YvivivYY     viivvvYY
ztFUIbEQEEApUbo    tdUzpZbbEbEEB   viiiiiSbi   iiijCjXCz
nYJCziiiiiSnJJPU   nFCoXvvvvccCS   viiiiiitC   i vYiviYC
CYCJF      ICJFO   UStoi           pXciiiiiI   ivcivvjtI
tvCCziiiiiCJnSdi   IoCoiiYnCji     tjYjjYiiii iivvjjjYCn 
CYXtCZbOEEoJtv     EIFSPEQdpIPZo   SvvciiJviiiivvJivjjCo
tYCCSCCttCAtnjv   ionJAjiiinoCSAU  BbPQc QddBBQUzz YAnpd
CYCnI      FCSzOi           zJCzb  BpAbJ BBBMMMBMp SSCSd
JcCCA      SJCSAj njCtj    iICtob  QpZMO iMMMBBMMi BBIoF
CYCnoiiiiiCFCJoEi CBZAAPCjtAnoFbi  MBBMW  MMMMBMM  MMBMM
ACUFAbQEQbdUPPbi   iFZbPbbbAPPzi   MMMMB  QMMMMMj  MMMMM
jcjXjCjCXtYYii        iYjXjYi      MQQBb   MMBBM   BBBBM
 
--------------------------------------------------------
帮5买（www.b5m.com）
开发人员：尹正飞(qq:657812595) 杨建远(qq:690492623)
2014-04-17
--------------------------------------------------------

*/
define("modules/default/common/SimpleDialog",["$","modules/default/common/common","http://staticcdn.b5m.com/static/css/center/center-reset.css"],function(e,t,i){var o=e("modules/default/common/common"),a=function(e){this.init(e)};(function(e){e.init=function(e){this.content=e.content,this.title=e.title},e.showSimpleTip=function(){var t='<div class="dialog__confirm"><span>';t+=this.content,t+="</span></div>";var i=o.isEmptyValue(this.title)?"提示框":this.title;e.showDialog(t,i)},e.showDialog=function(t){var i=i||"提示",o=$(t),a="",s=$(document).width(),n=$(document).height(),r=$(window).height(),l=$(window).scrollTop(),d=$(".dialog-mask");if(!o)return!1;a='<div class="dialog" id="simpleDialog" ><div class="dialog__head"><h3 class="title">',a+=i,a+='</h3><a href="javascript:void(0);" class="close">x</a></div><div class="dialog__body">',a+="</div></div>",d.length?(d.show(),$(".dialog").show()):(d=$('<div class="dialog-mask"></div>').css("height",n).appendTo($("body")),$(a).appendTo($("body")),$(".dialog__body").append(o));var c=$("#simpleDialog");$close=c.find(".close"),dialog_w=c.outerWidth(),dialog_h=c.outerHeight(),pos_x=parseInt((s-dialog_w)/2),pos_y=parseInt((r+l-dialog_h)/2),d.css({height:n}),c.css({left:pos_x,top:pos_y}).show(),$close.on("click",function(t){t.preventDefault(),e.hideDialog()})},e.hideDialog=function(){$(".dialog").remove(),$(".dialog-mask").remove()}})(a.prototype),i.exports=a});