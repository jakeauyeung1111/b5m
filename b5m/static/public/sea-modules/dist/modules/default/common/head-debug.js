define("modules/default/common/head-debug", [ "$-debug", "cookie-debug", "gallery/underscore/1.4.4/underscore-debug", "gallery/backbone/1.0.0/backbone-debug" ], function(require, exports, module) {
    require("modules/default/common/css/header-debug.css");
    var tpl_head = require("modules/default/common/tpl/head-debug.html");
    var underscore = require("gallery/underscore/1.4.4/underscore-debug");
    this.Backbone = require("gallery/backbone/1.0.0/backbone-debug");
    this.$ = require("$-debug");
    var uc = null;
    module.exports = {
        isLogin: function() {
            return this.getUCenter().get("isLogin");
        },
        readData: function() {
            var UCenter = Backbone.Model.extend({});
            uc = new UCenter();
            var islogin = (islogin = $.cookie("login")) == undefined ? "false" : islogin;
            if (islogin == "false") {
                islogin = false;
            } else {
                islogin = true;
            }
            uc.set("isLogin", islogin);
            uc.set("showName", $.cookie("showname"));
        },
        getUCenter: function() {
            return uc;
        },
        loadUserStatusTpl: function() {
            seajs.use("modules/default/common/tpl/header-userstatus.html", function(u_s_tpl) {
                module.exports.dataMergeUserStatusTpl(u_s_tpl);
            });
        },
        dataMergeUserStatusTpl: function(u_s_tpl) {
            var html = $('<span id="b5m_yzf">' + tpl_head + "</span>").find('div[class="bar-login"]').empty().append(underscore.template(u_s_tpl, uc.toJSON())).closest("#b5m_yzf").html();
            module.exports.tplToHtml(html);
        },
        tplToHtml: function(tpl) {
            $("body").children(":first").before("<span>" + tpl + "</span>");
        }
    };
    //初始化用户信息
    module.exports.readData();
    if (!uc.get("isLogin")) {
        module.exports.tplToHtml(tpl_head);
    } else {
        module.exports.loadUserStatusTpl();
    }
});