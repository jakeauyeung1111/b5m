define("modules/default/common/SimpleDialog-debug", [ "$-debug", "modules/default/common/common-debug", "http://staticcdn.b5m.com/static/css/center/center-reset-debug.css" ], function(require, exports, module) {
    var COMMON = require("modules/default/common/common-debug");
    var SimpleDialog = function(opt) {
        this.init(opt);
    };
    (function(SimpleDialog) {
        SimpleDialog.init = function(opt) {
            this.content = opt.content;
            this.title = opt.title;
        };
        SimpleDialog.showSimpleTip = function() {
            var _html = '<div class="dialog__confirm"><span>';
            _html += this.content;
            _html += "</span></div>";
            var _title = COMMON.isEmptyValue(this.title) ? "提示框" : this.title;
            SimpleDialog.showDialog(_html, _title);
        };
        SimpleDialog.showDialog = function(html, title) {
            var _title = _title || "提示", _content = $(html), _html = "", doc_w = $(document).width(), doc_h = $(document).height(), win_h = $(window).height(), scroll_top = $(window).scrollTop(), $dialog_mask = $(".dialog-mask");
            if (!_content) return false;
            _html = '<div class="dialog" id="simpleDialog" ><div class="dialog__head"><h3 class="title">';
            _html += _title;
            _html += '</h3><a href="javascript:void(0);" class="close">x</a></div><div class="dialog__body">';
            _html += "</div></div>";
            // open the dialog
            if (!$dialog_mask.length) {
                $dialog_mask = $('<div class="dialog-mask"></div>').css("height", doc_h).appendTo($("body"));
                $(_html).appendTo($("body"));
                $(".dialog__body").append(_content);
            } else {
                $dialog_mask.show();
                $(".dialog").show();
            }
            var $dialog = $("#simpleDialog");
            $close = $dialog.find(".close"), dialog_w = $dialog.outerWidth(), dialog_h = $dialog.outerHeight(), 
            // position
            pos_x = parseInt((doc_w - dialog_w) / 2), pos_y = parseInt((win_h + scroll_top - dialog_h) / 2);
            $dialog_mask.css({
                height: doc_h
            });
            $dialog.css({
                left: pos_x,
                top: pos_y
            }).show();
            // close the  dialog
            $close.on("click", function(e) {
                e.preventDefault();
                SimpleDialog.hideDialog();
            });
        };
        SimpleDialog.hideDialog = function() {
            $(".dialog").remove();
            $(".dialog-mask").remove();
        };
    })(SimpleDialog.prototype);
    module.exports = SimpleDialog;
});