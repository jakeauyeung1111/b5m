/*
 * 重构搜索功能
 * henry.li@b5m.com
 */
define("modules/default/common/search-debug", [ "$-debug", "jquery/placeholder/jquery.placeholder-debug" ], function(require, exports, module) {
    this.$ = require("$-debug");
    require("modules/default/common/css/search-debug.css");
    $.placeholder = require("jquery/placeholder/jquery.placeholder-debug");
    var tpl_search = require("modules/default/common/tpl/search-debug.html");
    $("body").children(":first").append(tpl_search);
    var J_search = $("#J_search");
    var input = J_search.find(">input");
    var submit = J_search.find(">a");
    var J_searchList = $('<div id="downList" class="searchlist"><div class="downList"><ul id="list"></ul></div></div>').appendTo(J_search.parent());
    var J_searchUl = J_searchList.find("ul");
    $(function() {
        input.placeholder({
            color: "#ccc",
            useBrowserPlaceholder: true,
            trim: true,
            dataName: "original-font-color",
            className: "placeholder",
            placeholder: "快速搜索 全网比价"
        });
        var k = -1, len = 0;
        var getUrlByKeyWords = function(val) {
            if (val == input.data("placeholder")) {
                input.val("").focus();
                return false;
            }
            val = $.trim(val).replace(/_||/g, "").replace(/</gi, "&lt;").replace(/>/gi, "&gt;").replace(/"/g, "&quot;");
            if (val.length > 100) {
                alert("关键词不能超过100个字符，请重新输入");
                return false;
            }
            val = encodeURIComponent(val);
            val = val.replace(/%2F/g, "/");
            //var target = "http://www1.b5m.com/search/s/________________%23keyword%23.html";
            var target = "http://www1.b5m.com/search/s/___image________________%23keyword%23.html";
            return target.replace("%23keyword%23", val);
        };
        input.unbind("input propertychange").bind("input propertychange", function(e) {
            if (e.originalEvent && e.originalEvent.propertyName && e.originalEvent.propertyName != "value") {
                //取到原始的event，判断是否修改了值
                return;
            }
            var val = $.trim($(this).val());
            if (val == "") {
                J_searchList.hide();
                return false;
            }
            //    var url = "http://www1.b5m.com/autofill.htm?collectionName=b5mp&keyWord=" + val;
            var url = "http://www1.b5m.com/autofill.htm?collectionName=b5mp&keyWord=" + val;
            $.ajax({
                type: "GET",
                url: url,
                dataType: "jsonp",
                jsonp: "jsoncallback",
                timeout: 5e3,
                data: "",
                success: function(httpObj) {
                    if (httpObj != "") {
                        var html = "";
                        $.each(httpObj, function(i, item) {
                            html += "<li k='" + i + "' keyword=\"" + item.value + "\"><a href='javascript:doSubmit(" + '"' + item.value + '"' + ");' target='_self'><span>" + item.hl_value + "</span></a></li>";
                        });
                        J_searchList.show();
                        J_searchUl.empty().html(html).find("li").hover(function() {
                            J_searchUl.find("a.hover").removeClass();
                            $(this).find("a").addClass("hover");
                            k = parseInt($(this).attr("k") || 0);
                        }, function() {
                            k = -1;
                            $(this).find("a").removeClass("hover");
                        });
                        len = J_searchUl.find("li").length;
                    }
                }
            });
        }).bind("keydown", function(e) {
            if (J_searchList.css("display") == "block" && e.keyCode == 38 || e.keyCode == 40 || e.keyCode == 13) {
                if (e.keyCode == 13) {
                    var keyWords = input.val();
                    if (k != -1) {
                        keyWords = J_searchUl.find("li").eq(k).attr("keyword");
                    }
                    var url = getUrlByKeyWords(keyWords);
                    var el = document.createElement("a");
                    document.body.appendChild(el);
                    el.href = url;
                    el.target = "_blank";
                    if (el.click) {
                        el.click();
                    } else {
                        //safari浏览器click事件处理
                        try {
                            var evt = document.createEvent("Event");
                            evt.initEvent("click", true, true);
                            el.dispatchEvent(evt);
                        } catch (e) {}
                    }
                    $(el).remove();
                } else {
                    k = e.keyCode == 38 ? k - 1 <= -1 ? k = len - 1 : k - 1 : k + 1 >= len ? k = 0 : k + 1;
                    J_searchUl.find("a.hover").removeClass("hover").end().find("li").eq(k).find("a").addClass("hover");
                }
                return false;
            }
        }).bind("focusin", function() {
            $(this).addClass("focusin");
        }).bind("focusout", function() {
            $(this).removeClass("focusin");
        });
        $(document).bind("mousedown", function(e) {
            !$(e.target).parents("#downList").length && !$(e.target).parent().is("#downList") && J_searchList.hide();
        });
        window.doSubmit = function(_val) {
            var originalVal = _val || input.val();
            var url = getUrlByKeyWords(originalVal);
            url && window.open(url, "_blank");
            J_searchList.hide();
            return;
        };
        submit.attr("hidefocus", "true").attr("href", "javascript:doSubmit();").attr("target", "_self");
    });
});