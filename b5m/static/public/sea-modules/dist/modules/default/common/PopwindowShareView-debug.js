define("modules/default/common/PopwindowShareView-debug", [ "$-debug", "common-debug" ], function(require, exports, module) {
    var popWindowShareFed = {
        /*检测IE6*/
        ie6: function() {
            return typeof document.getElementsByTagName("body")[0].style.maxHeight == "undefined";
        }(),
        /*最大分享内容字节数*/
        insertNum: 120,
        /*分享内容输入*/
        shareTxt: function() {
            var _this = this, $txtArea = $(".popwindow-share-txt", ".popwindow-share"), msg = $txtArea.attr("msg");
            /*输入框默认值检测*/
            $txtArea.focus(function() {
                var $this = $(this), thisVal = $this.val().replace(/\s+/gi, "");
                if (msg.replace(/\s+/gi, "") == thisVal) {
                    $this.val("").css("color", "#333");
                }
            }).blur(function() {
                var $this = $(this), thisVal = $this.val().replace(/\s+/gi, "");
                if (msg.replace(/\s+/gi, "") == thisVal || thisVal == "") {
                    $this.val(msg).css("color", "#999");
                } else {
                    $this.css("color", "#333");
                }
            }).trigger("blur");
            /*输入框输入文字*/
            var insertNumDom = $(".popwindow-share-txtCanInsertNum");
            $txtArea.on({
                keyup: function() {
                    var $this = $(this), thisVal = $this.val(), thisValLen = thisVal.length, canInsertNum = _this.insertNum - thisValLen;
                    if (thisVal == msg) {
                        insertNumDom.text(_this.insertNum);
                        return;
                    }
                    if (canInsertNum < 20) {
                        insertNumDom.css("color", "#f00");
                    } else {
                        insertNumDom.removeAttr("style");
                    }
                    if (thisValLen >= _this.insertNum) {
                        $this.val(thisVal.substr(0, _this.insertNum));
                        insertNumDom.text(0);
                    } else {
                        insertNumDom.text(canInsertNum);
                    }
                }
            });
            $(".popwindow-share-close", ".popwindow-share").click(function() {
                $(".popwindow-share").remove();
            });
        },
        /*分享弹窗关闭*/
        popwindowClose: function() {
            $(".popwindow-share").remove();
        },
        /**
     * 弹窗 - 弹出
     * @param args{
     *  title:(string)分享标题
     *  href:(string)分享链接
     *  content:(string)分享内容
     *  fun:(function) 分享回调
     * }
     * @param callBack{function}分享回调
     * @returns {object} 返回分享对象
     */
        popwindow: function(args, callBack) {
            var _Args = $.extend({}, {
                title: "帮5买值得买频道",
                href: "http://www.b5m.com",
                content: "",
                type: "1",
                pic: [],
                fun: false
            }, args || {}), popWindowStr = '<div class="popwindow-share"><div class="popwindow-share-insert"><h3>分享<span>还能输入<b class="popwindow-share-txtCanInsertNum">120</b>个字符</span></h3><textarea class="popwindow-share-txt" msg="请输入分享内容...">' + _Args["content"] + "</textarea><p><b>分享来自“" + _Args["title"] + '”</b><a href="' + _Args["href"] + '" target="_blank">' + _Args["href"] + "</a></p></div>", $popBox, popBoxHeight = 0;
            /*生成分享图片*/
            if (_Args.pic.length > 0) {
                var imgScrollHtml, imgHtml = "";
                for (var i = 0; i < _Args.pic.length; i++) {
                    imgHtml += '<span><img src="' + _Args.pic[i] + '"/><s></s></span>';
                }
                imgScrollHtml = '<div class="popwindow-share-pic"><p><input type="checkbox" id="popwindow-share-piccheckbox" value="1" class="popwindow-share-piccheckbox" checked/><label for="popwindow-share-piccheckbox">同时分享选中图片</label></p><div class="popwindow-share-picbox"><div class="ov"><div class="popwindow-share-cont ov">' + imgHtml + '</div></div><s class="popwindow-share-pic-next"><u></u></s><s class="popwindow-share-pic-prev"><u></u></s></div></div>';
                popWindowStr += imgScrollHtml;
            }
            popWindowStr += '<div class="popwindow-share-btbox"><span class="popwindow-share-subBt">分享</span><a href="http://ucenter.b5m.com/user/third/login/auth.htm?type=' + _Args.type + '&refererUrl=&api=/setToken" target="_blank" class="popwindow-share-transit" target="_blank">换个账号分享</a></div><s class="popwindow-share-close"></s></div>';
            $(popWindowStr).appendTo($("body"));
            $popBox = $(".popwindow-share");
            popBoxHeight = $popBox.outerHeight(true);
            if (typeof document.body.style.maxWidth == "undefined") {
                var winHeight = $(window).height();
                $(window).resize(function() {
                    winHeight = $(window).height();
                });
                $(window).scroll(function() {
                    $popBox.css("top", (winHeight - popBoxHeight) / 2 + $(this).scrollTop() + "px");
                }).trigger("scroll");
            } else {
                $popBox.css("marginTop", "-" + popBoxHeight / 2 + "px");
            }
            popWindowShareFed.shareTxt();
            if (_Args.pic.length > 0) {
                $("span:first", ".popwindow-share-cont").addClass("cur");
                popWindowShareFed.imgScroll();
                popWindowShareFed.sltShareImg();
            }
            /*点击分享按钮*/
            $(".popwindow-share-subBt", ".popwindow-share").click(function() {
                var shareVals = {}, shareTxtBox = $(".popwindow-share-txt");
                if (shareTxtBox.attr("msg") == shareTxtBox.val()) {}
                shareVals.title = _Args["title"];
                shareVals.href = _Args["href"];
                shareVals.content = shareTxtBox.val();
                if (_Args.pic.length > 0) {
                    if ($("#popwindow-share-piccheckbox").is(":checked")) shareVals.pic = $(".cur>img", ".popwindow-share-cont").attr("src");
                }
                /*分享回调*/
                if (callBack) {
                    callBack(shareVals);
                }
                if (_Args["fun"]) {
                    _Args["fun"](shareVals);
                }
            });
            return this;
        },
        /*分享图片滚动*/
        imgScroll: function(args) {
            var _Args = $.extend({}, {
                auto: false
            }, args || {}), scrollBoxId = ".popwindow-share-picbox", scrollContId = ".popwindow-share-cont", autoScroll, scrollTim = 4e3;
            if ($("span", scrollContId).length < 5) {
                $(".popwindow-share-pic-next,.popwindow-share-pic-prev", scrollBoxId).css("opacity", "0.3");
                return this;
            }
            /*向前滚动*/
            var scrollNext = function() {
                var firstDom = $("span:first", scrollContId), $contBox = $(scrollContId);
                $contBox.animate({
                    marginLeft: "-=" + firstDom.outerWidth(true)
                }, function() {
                    $contBox.append(firstDom).removeAttr("style");
                });
            };
            /*向后滚动*/
            var scrollPrev = function() {
                var lastDom = $("span:last", scrollContId), $contBox = $(scrollContId), domWidth = lastDom.outerWidth(true);
                $contBox.prepend(lastDom).css("marginLeft", "-" + domWidth + "px").animate({
                    marginLeft: "+=" + domWidth
                });
            };
            if (_Args["auto"]) {
                $(scrollBoxId).hover(function() {
                    clearInterval(autoScroll);
                }, function() {
                    autoScroll = setInterval(function() {
                        scrollNext();
                    }, scrollTim);
                }).trigger("mouseout");
            }
            $(".popwindow-share-pic-next,.popwindow-share-pic-prev", scrollBoxId).on({
                click: function() {
                    var $this = $(this);
                    if ($this.hasClass("popwindow-share-pic-next")) {
                        scrollPrev();
                    } else {
                        scrollNext();
                    }
                }
            });
            return this;
        },
        /*分享图片选择*/
        sltShareImg: function() {
            $("span", ".popwindow-share-cont").click(function() {
                if (!$("#popwindow-share-piccheckbox").is(":checked")) {
                    return false;
                }
                var $this = $(this);
                if ($this.find("s").length == 0) {
                    $this.append("<s></s>");
                }
                $this.addClass("cur").siblings().removeClass("cur");
                return this;
            });
        }
    };
    module.exports = popWindowShareFed;
});