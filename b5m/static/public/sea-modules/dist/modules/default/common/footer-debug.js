define("modules/default/common/footer-debug", [ "$-debug" ], function(require, exports, module) {
    var tpl_footer = require("modules/default/common/tpl/footer-debug.html");
    module.exports = {
        tplToHtml: function(tpl) {
            $("body").children(":last").after(tpl);
        }
    };
    module.exports.tplToHtml(tpl_footer);
});