define("m/ui/tabswitch/1.0.0/tabswitch", [ "$" ], function(require, exports, module) {
    var $ = require('$'),root = undefined, timer;
    var tt;
    var Tabswitch = function(options){
        root = this;
        this.init($.extend({}, Tabswitch.defauts, options));
    };

    Tabswitch.prototype.init = function(opts){
        var $el = $(opts.el ? opts.el : window),
            $tab = $el.find(opts.triggers),
            $tabContent = $el.find(opts.panels);
        if ($el !== window) {
            $el.addClass(opts.classPrefix);
        }

        $tab.removeClass(opts.activeTriggerClass).eq(opts.activeIndex).addClass(opts.activeTriggerClass);
        $tabContent.hide().eq(opts.activeIndex).show();
        var tabHandle = function(elem){
                elem.siblings().removeClass(opts.activeTriggerClass).end().addClass(opts.activeTriggerClass);
                $tabContent.hide().eq(elem.index()).show();
            },
            delay = function(elem, delay){
                clearTimeout(tt);
                !delay ? tabHandle(elem) : tt = setTimeout(function(){tabHandle(elem);}, delay);
            },
            start = function(){
                if (!opts.autoPlay || !opts.interval) return;
                timer = setInterval(autoRun, opts.interval);
            },
            autoRun = function(isPrev){
                var $cur = $tab.filter(opts.activeTriggerClass),
                    $firstNav = $tab.first(),
                    $lastNav = $tab.last(),
                    len = $tab.length,
                    index = $cur.index(),
                    item, i;

                if (isPrev) {
                    index -= 1;
                    item = index === -1 ? $lastNav : $cur.prev();
                } else {
                    index += 1;
                    item = index === len ? $firstNav : $cur.next();
                }

                i = index === len ? 0 : index;

                $cur.removeClass(opts.activeTriggerClass);
                item.addClass(opts.activeTriggerClass);

                $tabContent.hide().eq(i).show();
            };

        $tab.each(function(){
            $(this).on(opts.triggerType, function(){
                delay($(this), opts.delay);
            });
        });

        if (opts.autoPlay) {
            start();
            if ($('.ui-tab').length) {
                $('.ui-tab').hover(function(){
                    clearInterval(timer);
                    timer = undefined;
                }, function(){
                    start();
                });
            }
        }
    };

    Tabswitch.defauts = {
        el: undefined,       //el: '#id'
        classPrefix: 'ui-tab',
        triggers: [],
        panels: [],
        triggerType: 'mouseenter',
        delay: 200,
        activeIndex: 0,
        autoPlay: false,
        interval: 5e3
    };

    module.exports = Tabswitch;
});