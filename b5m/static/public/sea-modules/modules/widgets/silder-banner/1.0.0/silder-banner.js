/*
 * silder
 */
define('m/ui/silder-banner/1.0.0/silder-banner', ['$'], function(require, exports, module) {
    var $ = require('$');
    jQuery.easing['jswing'] = jQuery.easing['swing'];

    jQuery.extend(jQuery.easing,
            {
                def: 'easeOutQuad',
                swing: function(x, t, b, c, d) {
                    //alert(jQuery.easing.default);
                    return jQuery.easing[jQuery.easing.def](x, t, b, c, d);
                },
                easeInQuad: function(x, t, b, c, d) {
                    return c * (t /= d) * t + b;
                },
                easeOutQuad: function(x, t, b, c, d) {
                    return -c * (t /= d) * (t - 2) + b;
                },
                easeInOutQuad: function(x, t, b, c, d) {
                    if ((t /= d / 2) < 1)
                        return c / 2 * t * t + b;
                    return -c / 2 * ((--t) * (t - 2) - 1) + b;
                },
                easeInCubic: function(x, t, b, c, d) {
                    return c * (t /= d) * t * t + b;
                },
                easeOutCubic: function(x, t, b, c, d) {
                    return c * ((t = t / d - 1) * t * t + 1) + b;
                },
                easeInOutCubic: function(x, t, b, c, d) {
                    if ((t /= d / 2) < 1)
                        return c / 2 * t * t * t + b;
                    return c / 2 * ((t -= 2) * t * t + 2) + b;
                },
                easeInQuart: function(x, t, b, c, d) {
                    return c * (t /= d) * t * t * t + b;
                },
                easeOutQuart: function(x, t, b, c, d) {
                    return -c * ((t = t / d - 1) * t * t * t - 1) + b;
                },
                easeInOutQuart: function(x, t, b, c, d) {
                    if ((t /= d / 2) < 1)
                        return c / 2 * t * t * t * t + b;
                    return -c / 2 * ((t -= 2) * t * t * t - 2) + b;
                },
                easeInQuint: function(x, t, b, c, d) {
                    return c * (t /= d) * t * t * t * t + b;
                },
                easeOutQuint: function(x, t, b, c, d) {
                    return c * ((t = t / d - 1) * t * t * t * t + 1) + b;
                },
                easeInOutQuint: function(x, t, b, c, d) {
                    if ((t /= d / 2) < 1)
                        return c / 2 * t * t * t * t * t + b;
                    return c / 2 * ((t -= 2) * t * t * t * t + 2) + b;
                },
                easeInSine: function(x, t, b, c, d) {
                    return -c * Math.cos(t / d * (Math.PI / 2)) + c + b;
                },
                easeOutSine: function(x, t, b, c, d) {
                    return c * Math.sin(t / d * (Math.PI / 2)) + b;
                },
                easeInOutSine: function(x, t, b, c, d) {
                    return -c / 2 * (Math.cos(Math.PI * t / d) - 1) + b;
                },
                easeInExpo: function(x, t, b, c, d) {
                    return (t == 0) ? b : c * Math.pow(2, 10 * (t / d - 1)) + b;
                },
                easeOutExpo: function(x, t, b, c, d) {
                    return (t == d) ? b + c : c * (-Math.pow(2, -10 * t / d) + 1) + b;
                },
                easeInOutExpo: function(x, t, b, c, d) {
                    if (t == 0)
                        return b;
                    if (t == d)
                        return b + c;
                    if ((t /= d / 2) < 1)
                        return c / 2 * Math.pow(2, 10 * (t - 1)) + b;
                    return c / 2 * (-Math.pow(2, -10 * --t) + 2) + b;
                },
                easeInCirc: function(x, t, b, c, d) {
                    return -c * (Math.sqrt(1 - (t /= d) * t) - 1) + b;
                },
                easeOutCirc: function(x, t, b, c, d) {
                    return c * Math.sqrt(1 - (t = t / d - 1) * t) + b;
                },
                easeInOutCirc: function(x, t, b, c, d) {
                    if ((t /= d / 2) < 1)
                        return -c / 2 * (Math.sqrt(1 - t * t) - 1) + b;
                    return c / 2 * (Math.sqrt(1 - (t -= 2) * t) + 1) + b;
                },
                easeInElastic: function(x, t, b, c, d) {
                    var s = 1.70158;
                    var p = 0;
                    var a = c;
                    if (t == 0)
                        return b;
                    if ((t /= d) == 1)
                        return b + c;
                    if (!p)
                        p = d * .3;
                    if (a < Math.abs(c)) {
                        a = c;
                        var s = p / 4;
                    }
                    else
                        var s = p / (2 * Math.PI) * Math.asin(c / a);
                    return -(a * Math.pow(2, 10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p)) + b;
                },
                easeOutElastic: function(x, t, b, c, d) {
                    var s = 1.70158;
                    var p = 0;
                    var a = c;
                    if (t == 0)
                        return b;
                    if ((t /= d) == 1)
                        return b + c;
                    if (!p)
                        p = d * .3;
                    if (a < Math.abs(c)) {
                        a = c;
                        var s = p / 4;
                    }
                    else
                        var s = p / (2 * Math.PI) * Math.asin(c / a);
                    return a * Math.pow(2, -10 * t) * Math.sin((t * d - s) * (2 * Math.PI) / p) + c + b;
                },
                easeInOutElastic: function(x, t, b, c, d) {
                    var s = 1.70158;
                    var p = 0;
                    var a = c;
                    if (t == 0)
                        return b;
                    if ((t /= d / 2) == 2)
                        return b + c;
                    if (!p)
                        p = d * (.3 * 1.5);
                    if (a < Math.abs(c)) {
                        a = c;
                        var s = p / 4;
                    }
                    else
                        var s = p / (2 * Math.PI) * Math.asin(c / a);
                    if (t < 1)
                        return -.5 * (a * Math.pow(2, 10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p)) + b;
                    return a * Math.pow(2, -10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p) * .5 + c + b;
                },
                easeInBack: function(x, t, b, c, d, s) {
                    if (s == undefined)
                        s = 1.70158;
                    return c * (t /= d) * t * ((s + 1) * t - s) + b;
                },
                easeOutBack: function(x, t, b, c, d, s) {
                    if (s == undefined)
                        s = 1.70158;
                    return c * ((t = t / d - 1) * t * ((s + 1) * t + s) + 1) + b;
                },
                easeInOutBack: function(x, t, b, c, d, s) {
                    if (s == undefined)
                        s = 1.70158;
                    if ((t /= d / 2) < 1)
                        return c / 2 * (t * t * (((s *= (1.525)) + 1) * t - s)) + b;
                    return c / 2 * ((t -= 2) * t * (((s *= (1.525)) + 1) * t + s) + 2) + b;
                },
                easeInBounce: function(x, t, b, c, d) {
                    return c - jQuery.easing.easeOutBounce(x, d - t, 0, c, d) + b;
                },
                easeOutBounce: function(x, t, b, c, d) {
                    if ((t /= d) < (1 / 2.75)) {
                        return c * (7.5625 * t * t) + b;
                    } else if (t < (2 / 2.75)) {
                        return c * (7.5625 * (t -= (1.5 / 2.75)) * t + .75) + b;
                    } else if (t < (2.5 / 2.75)) {
                        return c * (7.5625 * (t -= (2.25 / 2.75)) * t + .9375) + b;
                    } else {
                        return c * (7.5625 * (t -= (2.625 / 2.75)) * t + .984375) + b;
                    }
                },
                easeInOutBounce: function(x, t, b, c, d) {
                    if (t < d / 2)
                        return jQuery.easing.easeInBounce(x, t * 2, 0, c, d) * .5 + b;
                    return jQuery.easing.easeOutBounce(x, t * 2 - d, 0, c, d) * .5 + c * .5 + b;
                }
            });

    //判断浏览器是否支持某css3样式
    var supports = (function() {
        var div = document.createElement('div'),
                vendors = 'Khtml O Moz Webkit'.split(' '),
                len = vendors.length;
        return function(prop) {
            if (window.ActiveXObject && parseInt(navigator.userAgent.toLowerCase().match(/msie ([\d.]+)/)[1], 10) <= 8) {
                return false;
            }
            if (prop in div.style)
                return true;
            if ('-ms-' + prop in div.style)
                return true;
            prop = prop.replace(/^[a-z]/, function(val) {
                return val.toUpperCase();
            });
            while (len--) {
                if (vendors[len] + prop in div.style) {
                    return true;
                }
            }
            return false;
        };
    })();
    var supportsTransition = supports('transition');



    var navPciSilder = function() {
        var silder = $("#nav-silder");
        var sileder_item = silder.find('.silder-item li');
        var silder_pointer = silder.find(".silder-pointer li");
        var win_width = silder.width();
        var moveLeft = (win_width - 980) / 2 + 460;
        sileder_item.eq(0).show().find('dl').fadeIn();
        silder.find('.silder-item img[data-src]').each(function() {
            $(this).css('background', 'url(' + $(this).attr('data-src') + ') no-repeat top center').removeAttr('data-src');
        });
        silder.find('.silder-pointer img[data-src]').each(function() {
            $(this).attr('src', $(this).attr('data-src')).removeAttr('data-src');
        });
        sileder_item.each(function(){
            var bg = $(this).attr('data-bg');
            if(bg){
                bg = bg.indexOf('#') == 0 ? bg : 'url('+ bg +') repeat-x top center';
                $(this).css('background',bg);
            }
        });

        function moveTo(oldIndex, newIndex) {
            sileder_item.eq(oldIndex).find('dl').animate({
                left: -moveLeft
            }, 700, 'linear');
            sileder_item.eq(newIndex).find('dl').css({'left': 0, 'display': 'block'});
            silder.find('.silder-item').stop().animate({
                left: -newIndex * 100 + '%'
            }, 1300, 'easeInOutQuint',function(){
                sileder_item.eq(newIndex).find('dl').css({'left': 0, 'display': 'block'});
            });
        }

        var timer = false;
        silder_pointer.mouseenter(function() {
            timer && clearTimeout(timer);
            if (supportsTransition) {
                if (!$(this).hasClass('cur')) {
                    var oldIndex, newIndex = silder_pointer.index($(this));
                    var $cur = silder_pointer.filter('.cur');
                    oldIndex = silder_pointer.index($cur);
                    $cur.removeClass('cur');
                    $(this).addClass('cur');
                    timer = setTimeout(function() {
                        moveTo(oldIndex, newIndex);
                    }, 300);
                }
            } else {
                if (!$(this).hasClass('cur')) {
                    var oldIndex, newIndex = silder_pointer.index($(this));
                    var $cur = silder_pointer.filter('.cur');
                    oldIndex = silder_pointer.index($cur);
                    $cur.animate({
                        opaicty: 0.4,
                        marginTop: 9
                    }, 200, 'easeInQuart', function() {
                        $cur.removeClass('cur');
                    });
                    $(this).animate({
                        opaicty: 1,
                        marginTop: -10
                    }, 300, 'easeInQuart', function() {
                        $(this).addClass('cur');
                        moveTo(oldIndex, newIndex);
                    });
                }
            }


        });

        var autoTimer = false;
        function autoMove() {
            autoTimer = setTimeout(function() {
                var oldIndex = silder_pointer.index(silder_pointer.filter('.cur'));
                var newIndex = oldIndex + 1 >= 6 ? 0 : oldIndex + 1;
                silder_pointer.eq(newIndex).trigger('mouseenter');
                autoTimer = setTimeout(function() {
                    autoMove();
                }, 3000);
            }, 3000);
        }
        autoMove();

        silder.hover(function() {
            clearTimeout(autoTimer);
        }, function() {
            autoMove();
        });
    }
    ;

    var secondPciSilder = function() {
        var silder = $("#second-silder");
        var sileder_item = silder.find('.silder-item');
        var silder_pointer = silder.find(".silder-pointer li:not(.prev,.next)");
        var pText = silder.find(".silder-pointer p");
        var pointer = silder.find('a.cur');
        var baseLeft = 218;
        var next = silder.find('.next a');
        var prev = silder.find('.prev a');
        var speed = 500;
        pointer.fadeIn();
        silder_pointer.mouseenter(function() {
            var newIndex = silder_pointer.index(this);
            pointer.attr('href', $(this).find('a').attr("href")).stop().animate({
                right: baseLeft - newIndex * 60 + 'px'
            }, speed);
            sileder_item.stop().animate({
                left: -609 * newIndex
            }, speed);


            silder_pointer.removeClass('cur');
            $(this).addClass('cur');

            var data = sileder_item.find('a').eq(newIndex);
            pText.stop().animate({opacity: 0.2}, speed / 2, function() {
                $(this).find('a').html(data.attr('title')).attr('href', data.attr('href'));
                $(this).find('span').html(data.attr('data-info'));
                pText.animate({opacity: 1}, speed / 2);
            });
        });
        next.add(prev).bind('click', function(e) {
            e.preventDefault();
            var oldIndex = silder_pointer.index(silder_pointer.filter('.cur'));
            var newIndex;
            if ($(this).parent().hasClass('prev')) {
                newIndex = oldIndex - 1 < 0 ? silder_pointer.length - 1 : oldIndex - 1;
            } else {
                newIndex = oldIndex + 1 > silder_pointer.length - 1 ? 0 : oldIndex + 1;
            }
            silder_pointer.eq(newIndex).trigger('mouseenter');
        });
        var autoTimer = false;
        function autoMove() {
            autoTimer = setTimeout(function() {
                next.trigger('click');
                autoTimer = setTimeout(function() {
                    autoMove();
                }, 3000);
            }, 3000);
        }
        autoMove();
        silder.hover(function() {
            clearTimeout(autoTimer);
        }, function() {
            autoMove();
        });


        var $second_silder2 = $("#second-silder2");
        var animate = $second_silder2.find('ul');
        var _prev = $second_silder2.find('a.prev');
        var _next = $second_silder2.find('a.next');
        var w = $second_silder2.width() + 21;
        var len = animate.find('li').length;
        animate.width(w / 2 * len);
        $second_silder2.hover(function() {
            _prev.css('opacity', 0).stop().animate({
                left: 0,
                opacity: 1
            });
            _next.css('opacity', 0).stop().animate({
                right: 0,
                opacity: 1
            });
        }, function() {
            _prev.stop().animate({
                left: -25,
                opacity: 0
            });
            _next.stop().animate({
                right: -25,
                opacity: 0
            });
        });
        var page = 0;
        len /= 2;
        _prev.click(function(e) {
            e.preventDefault();
            page = page - 1 < 0 ? len - 1 : page - 1;
            move();
        });
        _next.click(function(e) {
            e.preventDefault();
            page = page + 1 >= len ? 0 : page + 1;
            move();
        });
        _prev.add(_next).mouseover(function() {
            $(this).css('opacity', 0.6);
        }).mousedown(function() {
            $(this).css('margin', '1px 0 0')
        }).mouseup(function() {
            $(this).css('margin', '0')
        }).mouseout(function() {
            $(this).css('opacity', 1)
        })
        function move() {
            animate.stop().animate({
                left: -w * page
            }, 500);
        }
        ;
    }
    exports.run = function(type) {
        switch (type) {
            case 'index' :
                navPciSilder();
                break;
            case 'second' :
                secondPciSilder()
                break;
            default :
                break;
        }
    };
});
