define('modules/2.0.0/user/user/models/LevelModel',['$','underscore','backbone','common'],function(require, exports, module){
	
	this._ = require('underscore');
	this.Backbone = require('backbone');
	this.COMMON = require('common');
	
	var LevelModel = {
			Models:{},
			Urls:{}
	};
	
	LevelModel.Urls.LevelModel = B5M_UC.rootPath+'/user/info/data/levelRule.htm';
	
	LevelModel.Models.LevelModel = Backbone.Model.extend({
		url:LevelModel.Urls.LevelModel,
		parse: function(response) {
			COMMON.checkJSonData(response);
			if(response.ok){
				var data = response.data;
				for ( var i = 0; i < data.length; i++) {
					data[i].autoId = data[i].id;
					delete data[i].id;
				}
				return data;
			}
			COMMON.showTip(response.data);
			return null;
		},
		initialize:function(){
			this.fetch({
				async:false,
				cache:false
			});
		}
	});
	
	module.exports.LevelModel = LevelModel.Models.LevelModel;

});