define('modules/2.0.0/user/props/views/NotUsedPropView',['$','underscore','backbone','modules/2.0.0/user/props/tpl/prop-item.html','modules/2.0.0/user/props/views/UserPropsUseDialogView'],function(require, exports, module){
	this.underscore = require('underscore');
	this.Backbone = require('backbone');
	this.UserPropsUseDialogView = require('modules/2.0.0/user/props/views/UserPropsUseDialogView');
	
	var notUsedPropView = Backbone.View.extend({
		tagName: 'li',
		initialize: function () {
	    	this.render();
	    },	    
	    render: function () {
	    	//加载模版
			if(this.template === undefined)
				this.template = underscore.template(require('modules/2.0.0/user/props/tpl/prop-item.html'));
			this.$el.html(this.template(this.model));
			return this;
	    },
	    events:{
			'click .useProp':'useProp'
		},
	    useProp:function(){
	    	new UserPropsUseDialogView({beanCount:this.model.beanCount}).render();
	    }
	});
	
	module.exports = notUsedPropView;
});