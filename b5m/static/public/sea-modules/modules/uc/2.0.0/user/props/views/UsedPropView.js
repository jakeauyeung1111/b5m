define('modules/2.0.0/user/props/views/UsedPropView',['$','underscore','backbone','common','modules/2.0.0/user/props/tpl/used-prop-list.html','modules/2.0.0/user/props/models/UserPropModel'],function(require, exports, module){
	
	this._ = require('underscore');
	this.Backbone = require('backbone');
	this.COMMON = require('common');
	var UserPropModel = require('modules/2.0.0/user/props/models/UserPropModel');
	var usedPropListHtml = require('modules/2.0.0/user/props/tpl/used-prop-list.html');
	var emptyHtml = '<div class="favorites-none" style="display: block;"><span>暂无使用道具记录</span></div>';
	
	var usedPropView = Backbone.View.extend({
		initialize: function (opt) {
			this.el = opt.el;
	    	this.render();
	    },	    
	    render: function () {
	    	//加载模版
			if(this.template === undefined)
				this.template = _.template(usedPropListHtml);
	    	
			this.userPropModel = new UserPropModel({type:'used'});
			try{
				var array = this.userPropModel.get('used').list;
				if(COMMON.isEmptyValue(array)){
					this.$el.html(emptyHtml);
					return;
				}
				
		    	var newDomObjHtml = this.template({propList:array});
				this.$el.html(newDomObjHtml);
			}catch(e){
				this.$el.html(emptyHtml);
				return;
			}
			
			this.resetTotalCount();
	    },
	    //重置总数数据
	    resetTotalCount:function(){
	    	var totalCount = this.userPropModel.get('used').totalCount;
	    	if(!COMMON.isEmptyValue(totalCount))
	    		$("#used-props").html('('+totalCount+')');
	    }
	});
	
	module.exports = usedPropView;
});