define('modules/2.0.0/user/task/views/UserPluginInstallOrSignView',['$','underscore','modules/default/common/ModalDialogView','common','modules/2.0.0/user/task/tpl/user-plugin-install-or-sign.html'],function(require, exports, module){
	this._ = require('underscore');
	this.COMMON = require('common');
	var ModalDialogView = require('modules/default/common/ModalDialogView');
	var userPluginInstallOrSignHtml = require('modules/2.0.0/user/task/tpl/user-plugin-install-or-sign.html');
	
	
	var  UserPluginInstallOrSignView = ModalDialogView.extend({
		initialize:function(opt){
			this.data = {flag:opt.hasInstallPlugin};
		},
		events:{
		     "click #pluginSign":"pluginSign",
		     "click .close":'close'
		},
		render:function(){
			this.template = _.template(userPluginInstallOrSignHtml);
			this.$el.html(this.template(this.data));
			this.showModal();
		},
		pluginInstall:function(e){
			e.preventDefault();
		},
		pluginSign:function(e){
			e.preventDefault();
			this.data.isInstall = this.data.flag?"1":"0";
			var self_this = this ;
			$.post(B5M_UC.rootPath+'/user/task/data/plug.htm',this.data,function(response){
				var res = eval(response);
				if(res.ok){
					window.location.reload();
				}else{
					self_this.showTip(res.data);
				}
			});
		},
		close:function(){
			this.hideModal();
		}
	});
	
	module.exports = UserPluginInstallOrSignView;
	
});