define('modules/2.0.0/user/trade/common/views/TradeNoResultView',['$','underscore','backbone','modules/2.0.0/user/trade/common/tpl/index-no-trade.html'],function(require, exports, module){
	
	this.underscore = require('underscore');
	this.Backbone = require('backbone');
	var tpl_index_no_trade = 'modules/2.0.0/user/trade/common/tpl/index-no-trade.html';
	
	var TradeNoResultView = Backbone.View.extend({
		
		events:{
			"click h4":"accordionFun"
		},
		
		initialize:function(opt){
			this.render();
		},
		render:function(){
			//加载模版
			this.template = underscore.template(require(tpl_index_no_trade));
			this.$el.html(this.template());
			return this;
		},
		accordionFun:function(e){
            var _cur = 'open';
            var _this = $(e.currentTarget);
            if(_cur === _this.attr('class')){
            	_this.removeClass(_cur);
            	_this.next('div').hide();
            }else{
            	_this.attr('class',_cur);
            	_this.next('div').show();
            }
	    }
	});
	
	module.exports = TradeNoResultView;
	
});
	