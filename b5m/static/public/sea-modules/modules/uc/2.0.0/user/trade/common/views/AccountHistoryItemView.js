define('modules/2.0.0/user/trade/common/views/AccountHistoryItemView',['$','underscore','backbone','common'],function(require, exports, module){
	
	this._ = require('underscore');
	this.Backbone = require('backbone');
	this.COMMON = require('common');
	
	var tpl_account_history_item = 'modules/2.0.0/user/trade/common/tpl/account-history-item.html';
	var tpl_account_history_exchange_item = 'modules/2.0.0/user/trade/common/tpl/account-history-exchange-item.html';
	
	var AccountHistoryItemView = {
			Views:{}
	};
	
	AccountHistoryItemView.Views.AccountHistoryItemView = Backbone.View.extend({
		tagName: 'tr',
		initialize:function(opt){
			
			var tpl_addr = tpl_account_history_item, self = this;
			if(opt.tplName == 'default')
				tpl_addr = tpl_account_history_item;
			if(opt.tplName == 'exchange')
				tpl_addr = tpl_account_history_exchange_item;
			require.async(tpl_addr, function(tpl) {
				self.template = _.template(tpl),
				self.$el.html( self.template( self.model.toJSON() ) );
				self.render();
			});
		}
	});
	
	module.exports = AccountHistoryItemView.Views.AccountHistoryItemView;

});