define('modules/2.0.0/user/props/views/UserPropsUseDialogView',['$','underscore','modules/default/common/ModalDialogView','common','modules/2.0.0/user/props/tpl/user-prop-dialog.html'],function(require, exports, module){
	this._ = require('underscore');
	this.COMMON = require('common');
	var ModalDialogView = require('modules/default/common/ModalDialogView');
	
	var UserPropsUseDialogView = ModalDialogView.extend({
		initialize:function(opt){
			this.beanCount = opt.beanCount;
		},
		events:{
		     "click #useMyProp":"useMyProp",
		     "click .close":'close'
		},
		render:function(){
			var self_this = this ;
			require.async(['modules/2.0.0/user/props/tpl/user-prop-dialog.html'],function(userPropDialogHtml){
				self_this.showMyPropsDialog(userPropDialogHtml);
			});
		},
		showMyPropsDialog:function(userPropDialogHtml){
			this.template= _.template(userPropDialogHtml);
			this.$el.html(this.template({beanCount:this.beanCount}));
			this.showModal();
		},
		useMyProp:function(e){
			e.preventDefault();
			var self_this = this ;
			$.post(B5M_UC.rootPath+'/props/info/data/useMyProp.htm',this.data,function(response){
				var res = eval(response);
				if(res.ok){
					window.location.reload();
				}else{
					self_this.showTip(res.data);
				}
			});
		},
		close:function(){
			this.hideModal();
		}
	});
	
	module.exports = UserPropsUseDialogView;
});