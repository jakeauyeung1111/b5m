define('modules/2.0.0/user/props/views/NotUsedPropViews',['$','backbone','common','modules/2.0.0/user/props/views/NotUsedPropView','modules/2.0.0/user/props/models/UserPropModel'],function(require, exports, module){
	
	this.Backbone = require('backbone');
	this.COMMON = require('common');
	var NotUsedPropView = require('modules/2.0.0/user/props/views/NotUsedPropView');
	var UserPropModel = require('modules/2.0.0/user/props/models/UserPropModel');
	var emptyHtml = '<div class="favorites-none" style="display: block;"><span>未使用道具为空</span></div>';
	
	var NotUsedPropViews = Backbone.View.extend({
		initialize: function (opt) {
			this.el = opt.el;
			this.render();
	    },
	    render: function () {
	    	this.$el.find('.ui-loading').hide();
	    	this.userPropModel = new UserPropModel({type:'unused'});
	    	if(COMMON.isEmptyValue(this.userPropModel)){
	    		this.$el.find('ul').append(emptyHtml);
	    		return;
	    	}
	    	this.showNotUsedPropList();
	    	this.resetTotalCount();
	    },
	   //集合拼装
	    showNotUsedPropList:function(){
	    	try{
	    		var array = this.userPropModel.get('unused').list;
	    		if(COMMON.isEmptyValue(array)){
					//无数据处理
		    		this.$el.find('ul').append(emptyHtml);
		    		return;
				}
		    	for(var i = 0 ;i<array.length;i++){
		    		var notUsedPropView = new NotUsedPropView({
						model:array[i]
					});
					this.$el.find('ul').append(notUsedPropView.el);
		    	}
	    	}catch(e){
	    		this.$el.find('ul').append(emptyHtml);
	    	}
	    	
	    },
	    //重置总数数据
	    resetTotalCount:function(){
	    	var totalCount = this.userPropModel.get('unused').totalCount;
	    	if(!COMMON.isEmptyValue(totalCount))
	    		$("#not_use-props").html('('+totalCount+')');
	    }
	   
	});
	
	module.exports = NotUsedPropViews;
	
});