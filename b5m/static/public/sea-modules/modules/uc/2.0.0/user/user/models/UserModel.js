define('modules/2.0.0/user/user/models/UserModel',['$','underscore','backbone','common'],function(require, exports, module){
	
	this._ = require('underscore');
	this.Backbone = require('backbone');
	this.COMMON = require('common');
	
	var UserModel = {
			Models:{},
			Urls:{}
	};
	
	UserModel.Urls.UserInfo = B5M_UC.rootPath+'/user/user/data/info.htm';
	
	UserModel.Models.UserModel = Backbone.Model.extend({
		url:UserModel.Urls.UserInfo,
		parse: function(response) {
			COMMON.checkJSonData(response);
			if(response.ok)
				return this.initModelData(response.data);
			COMMON.showTip(response.data);
			return null;
		},
		initialize:function(options){
			if(!COMMON.isEmptyValue(options)){
				var type = options.type;
				if(!COMMON.isEmptyValue(type))
					this.url = UserModel.Urls.UserInfo+'?isSimple='+type;
			}
			this.fetch({
				async:false,
				cache:false
			});
		},
		initModelData:function(data){
			if(COMMON.isEmptyValue(data.currentResidence)){
				data.cProvince=31;
				data.cCity=1;
			}else{
				 var currentResidenceArray = data.currentResidence.split("-");
				 data.cProvince=currentResidenceArray[0];
				 data.cCity=currentResidenceArray[1];
			}
			
			if(COMMON.isEmptyValue(data.hometown)){
				data.hProvince=31;
				data.hCity=1;
			}else{
				 var hometownArray = data.hometown.split("-");
				 data.hProvince=hometownArray[0];
				 data.hCity=hometownArray[1];
			}
			
			if(COMMON.isEmptyValue(data.birthdayStr)){
				data.year=2013;
				data.month=10;
				data.day=1;
				data.birthdayStr="2013-10-1";
			}else{
				var birthdayStr = data.birthdayStr;
				var birthdayArray = birthdayStr.split("-");
				data.year=birthdayArray[0];
				data.month=Math.abs(birthdayArray[1]);
				data.day=Math.abs(birthdayArray[2]);
				data.birthdayStr=birthdayStr;
			}
			return data;
		}
	});
	
	module.exports.UserModel = UserModel.Models.UserModel;

});