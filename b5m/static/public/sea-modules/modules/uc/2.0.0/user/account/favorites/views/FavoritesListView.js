define('modules/2.0.0/user/account/favorites/views/FavoritesListView',['$','underscore','backbone','common','modules/uc/default/common/Page','modules/uc/2.0.0/user/account/favorites/models/FavoritesCollection','modules/uc/2.0.0/user/account/favorites/views/FavoritesListItemView','arale/cookie/1.0.2/cookie'],function(require, exports, module){
	
	this._ = require('underscore');
	this.Backbone = require('backbone');
	this.COMMON = require('common');
	
	this.Page = require('modules/uc/default/common/Page');
	
	this.tpl_favorites = 'modules/uc/2.0.0/user/account/favorites/tpl/favorites-list.html';
	this.FavoritesCollection = require('modules/uc/2.0.0/user/account/favorites/models/FavoritesCollection');
	this.FavoritesListItemView = require('modules/uc/2.0.0/user/account/favorites/views/FavoritesListItemView');
	this.Cookie = require('arale/cookie/1.0.2/cookie');
	
	var FavoritesListView = {
			Views:{},
			Urls:{}
	};
	
	FavoritesListView.Views.FavoritesListView = Backbone.View.extend({
		initialize:function(opt){
			
			//初始化参数
			this.parameterData = this.initParameter(opt);
			
			var tpl_addr = tpl_favorites, self = this;
			if(this.parameterData.tplName == 'default')
				tpl_addr = tpl_favorites;
			require.async(tpl_addr, function(tpl) {
				
				self.$el.html(tpl);
				
				//初始化收藏记录记录
				self.favoritesCollection = new FavoritesCollection();
				self.pageHandler({
					pageNum:1
				});
				
			});
			
		},
		initParameter:function(para){
			return _.extend({
				pageNum:1,
				pageSize:6,
				tplName:'default'
			},para);
		},
		render:function(){
			this.favoritesCollection.each(function( item, index) {
	            this.renderData( item, index );
	        }, this );
		},
		renderData:function(item, index){
			var favoritesListItemView = new FavoritesListItemView({
	            model: item,
	            FavoritesListView:this
	        });
			
	        this.$el.find('#favorites-result').append( favoritesListItemView.el );
		},
		pageHandler:function(opt){
			
			//加载系统消息
			var self = this;
			this.favoritesCollection.fetch({
				data:{
					userId:Cookie.get('token'),
					pageNum:opt.pageNum,
					pageSize:self.parameterData.pageSize,
					priceType:this.parameterData.priceType,
					webSite:this.parameterData.webSite
				},
                success: function(){
                	
                	self.hideLodingData();
        			//数据为空时处理
        			if(self.favoritesCollection.length == 0){
    					self.$el.find('.favorites-none').show();
    					//添加到展示页
    					self.appendShowView();
        				return;
        			}
        			
        			//渲染数据
        			self.render();
        			
        			//刷新分页控件
        			if(self.favoritesCollection.length > 0)
        				self.refreshPage(opt);
        			
        			//添加到展示页
        			self.appendShowView();
                }
            });
		},
		appendShowView:function(){
			this.parameterData.$el.append(this.el);
		},
		refreshPage:function(opt){
			
			//初始化分页控件
			if(!this.page){
				var self = this;
				this.page = new Page({
					onClick:function(pageNum, dom){
						self.pageHandler({
							pageNum:pageNum
						});
					}
				});
			}
			
			this.$el.find('.page-view').empty().append(this.page.getPageDom({
				pageNum : opt.pageNum,
				total : this.favoritesCollection.dataTotal,
				pageSize : this.parameterData.pageSize
			}));
		},
		showLoadingData:function(){
			this.$el.find('.ui-loading').show();
			this.$el.find('.page-view').empty();
			this.$el.find('#favorites-result').empty();
			this.$el.find('.favorites-none').hide();
		},
		hideLodingData:function(){
			this.$el.find('.ui-loading').hide();
			this.$el.find('.page-view').empty();
			this.$el.find('#favorites-result').empty();
			this.$el.find('.favorites-none').hide();
		}
	});
	
	module.exports = FavoritesListView.Views.FavoritesListView;

});