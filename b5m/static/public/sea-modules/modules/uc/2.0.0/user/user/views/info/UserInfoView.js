define('modules/2.0.0/user/user/views/info/UserInfoView',['$','underscore','backbone','common','modules/2.0.0/user/user/models/UserModel','modules/2.0.0/user/user/models/LevelModel','modules/2.0.0/user/task/views/UserPluginInstallOrSignView'],function(require, exports, module){
	
	this._ = require('underscore');
	this.Backbone = require('backbone');
	this.COMMON = require('common');
	this.UserModel = require('modules/2.0.0/user/user/models/UserModel');
	this.LevelModel = require('modules/2.0.0/user/user/models/LevelModel');
	this.UserPluginInstallOrSignView = require('modules/2.0.0/user/task/views/UserPluginInstallOrSignView');
	
	var UserInfoView = {
			Views:{},
			Urls:{}
	};
	var tpl_user_info_id_1 = 'modules/2.0.0/user/user/tpl/info/info-1.html';
	var tpl_user_info_id_2 = 'modules/2.0.0/user/user/tpl/info/info-2.html';
	var tpl_user_info_id_3 = 'modules/2.0.0/user/user/tpl/info/info-3.html';
	var tpl_user_info_id_4 = 'modules/2.0.0/user/user/tpl/info/info-b5m.html';
	
	UserInfoView.Views.UserInfoView = Backbone.View.extend({
		initialize:function(opt){
			this.el = opt.el;
			this.userModel = new UserModel.UserModel({type:2});
			this.levelModel = new LevelModel.LevelModel();
			
			//加载模版
			var tpl_user_info = tpl_user_info_id_1, self = this;
			switch(opt.tplId){
			case 1:tpl_user_info=tpl_user_info_id_1;break;
			case 2:tpl_user_info=tpl_user_info_id_2;break;
			case 3:tpl_user_info=tpl_user_info_id_3;break;
			case 4:tpl_user_info=tpl_user_info_id_4;break;
			}
			require.async(tpl_user_info,function(html){
				self.template=_.template(html),
				self.render();
			});
			
		},
		render:function(){
			
			this.$el.find('.ui-loading').hide();
			var data =  _.extend(this.userModel.toJSON(), this.levelModel.toJSON());
			//格式化最后登录时间
			if(data.lastLoginTime == null || data.lastLoginTime == undefined){
				data.lastLoginTime = COMMON.formatTime(new Date(),'yyyy-MM-dd');;
			}else{
				data.lastLoginTime = COMMON.formatTime(data.lastLoginTime,'yyyy-MM-dd');
			}
			this.$el.html( this.template(data) );
			
			if(this.userModel.length == 0)
				this.$el.find('.ui-empty-data').show();
		},
		events:{
			'click #everySign':'everySign',
			'click #hasInstallPlugin':'hasInstallPlugin',
			'mouseover .user-pic':'mouseover',
			'mouseleave .user-pic':'mouseleave',
			'hover #bangbichargebeanhandle':'bangbichargebeanhandle'
		},
		everySign:function(e){
			var $evarySign = $(e.currentTarget);
			if($evarySign.attr('isSign') == 'true')
				return;
			$.ajax({
				type: "POST",
				url: B5M_UC.rootPath+"/user/task/data/sign.htm",
				dataType:"json",
				success: function(httpObj){
					if(httpObj.ok){
						window.location.href=B5M_UC.rootPath;
					}else{
						COMMON.showSimpleTip(httpObj.data);
					}
				}
			}); 
		},
		hasInstallPlugin:function(e){
			var hasInstallPlugin =  this.isInstallPlugin();
			new UserPluginInstallOrSignView({
				hasInstallPlugin:hasInstallPlugin
			}).render();
		},
		isInstallPlugin:function(){
			var  b1 = !!document.getElementById('b5mmain');
			if(b1) return b1;
			
			var $obj = $('<span><object type="application/x-bang5taoplugin" width="0" height="0" style="display:none"></object></span>');
			$('body').append($obj);
			return !!$obj.find('object')[0].getCacheValue;
		},
		mouseover: function(e){
			$(e.currentTarget).find('a').animate({
                bottom: 21
            }, 200);
	    },
	    mouseleave:function(e){
	    	$(e.currentTarget).find('a').animate({
                bottom: -1
            }, 200);
        },
        bangbichargebeanhandle:function(e){
        	$(e.currentTarget).css('text-decoration','none');
        }

	});
	
	module.exports.UserInfoView = UserInfoView.Views.UserInfoView;

});