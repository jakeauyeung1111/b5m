define('modules/2.0.0/user/account/favorites/models/WebsiteCollection',['$','underscore','backbone','common'],function(require, exports, module){
	
	this._ = require('underscore');
	this.Backbone = require('backbone');
	this.COMMON = require('common');
	
	this.Websites = [
     		{'code':undefined,'name':'全部'},
     		{'code':'jd.com','name':'京东'},
			{'code':'taobao.com','name':'淘宝'},
			{'code':'tmall.com','name':'天猫'},
			{'code':'yhd.com','name':'一号店'},
			{'code':'yixun.com','name':'易讯'},
			{'code':'amazon.cn','name':'亚马逊'},
			{'code':'dangdang.com','name':'当当网'},
			{'code':'suning.com','name':'苏宁'},
			{'code':'gome.com.cn','name':'国美'},
			{'code':'vip.com','name':'唯品会'} 
	];
	
	var WebsiteCollection = {
			Models:{},
			Collections:{},
			Urls:{}
	};
	
	WebsiteCollection.Models.Website = Backbone.Model.extend({});
	
	WebsiteCollection.Collections.WebsiteCollection = Backbone.Collection.extend({
		model:WebsiteCollection.Models.Website,
		initialize:function(){
			this.add(Websites);
		},
		getNameByCode:function(code){
			var item;
			this.each(function( _item, index) {
				if(_item.get('code') == code){
					item = _item;
					return false;
				}
	        });
			return item?item.get('name'):'暂无';
		}
	});
	
	module.exports = WebsiteCollection.Collections.WebsiteCollection;

});