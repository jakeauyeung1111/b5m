define('modules/2.0.0/user/account/security/password/views/EditPasswordView',['$','common','underscore','backbone','modules/uc/2.0.0/user/account/security/password/tpl/edit-password.html','jquery/validate/1.11.1/validate','modules/uc/default/common/TestPassword'],function(require, exports, module){
	this.COMMON = require('common');
	this.underscore = require('underscore');
	this.Backbone = require('backbone');
	this.TestPassword =require('modules/uc/default/common/TestPassword');
	
	var EditPasswordView = Backbone.View.extend({
		tagName:"form",
		
		events:{
			"click #editPwdSubmit":"editPwdSub"
		},
		
		initialize: function (opt) {
	    	this.el = opt.el;
	    	this.render();
	    	this.testPassword = new TestPassword();
	    	//初始化验证
	    	this.initValidateForm();
	    },	    
	    render: function () {
	    	//加载模版
			if(this.template === undefined)
				this.template = underscore.template(require('modules/uc/2.0.0/user/account/security/password/tpl/edit-password.html'));
			this.$el.html(this.template);
		},
		initValidateForm:function(){
			
			var $editPwdForm = $('#editPwdForm');
			var self_this = this ;
			//自定义验证
			jQuery.validator.addMethod("isOldPwd", function(value, element) {
				return value != $("#password").val();       
			}, "新密码与原密码一致，请重新输入"); 
			
			jQuery.validator.addMethod("isPasswordOK", function(value, element) {
				return /^[a-zA-Z_0-9]{6,15}$/.test(value);
			}, "请输入正确格式的字母，数字,下划线");
			
			$editPwdForm.validate({
				rules: {
					password: {
						required: true,
						rangelength:[6,15],
						isPasswordOK: true
					},
					newPassword: {
						required: true,
						rangelength:[6,15],
						isPasswordOK: true,
						isOldPwd:true
					},
					confirmPassword: {
						required: true,
						rangelength:[6,15],
						isPasswordOK: true,
						equalTo:'#newPassword'
					}
				},
				messages: {
					password: {
						required: "请输入原密码",
						rangelength: "密码长度必须介于 {0} 和 {1} 之间"
					},
					newPassword: {
						required: "请输入新密码",
						rangelength: "密码长度必须介于 {0} 和 {1} 之间"
						
					},
					confirmPassword: {
						required: "请输入确认密码",
						rangelength: "密码长度必须介于 {0} 和 {1} 之间",
						equalTo:"两次输入的密码不一致"
					}
				},
				 errorElement: "span",
				 errorClass:'text_error',
				 errorPlacement: function(error, element) {
					 var errorBox = element.parent().parent().find('.cl-999').empty();
					 errorBox.append(error.html());
				 },
				 success: function(label,d) {
						$(d).parent().parent().find('.cl-999').empty();
				 },
				 onclick: false
			});
			
			this.$editPwdFormG = $editPwdForm;
			
			$('#password').bind('blur',function(){
				var newPassword = $.trim($('#newPassword').val());
				if(COMMON.isEmptyValue(newPassword)){
					$('#newPassword').addClass('text_error');
					$("#newPasswordMsg").html('原密码与新密码一致，请重新输入');
				}
			});
			$('#newPassword').keyup(function(){
				$('#test-pwd').empty().append(self_this.testPassword.getDefaultHtml($(this).val()));
			});
		},
		editPwdSub:function(e){
			if(!this.$editPwdFormG.valid()){
				return ;
			}
			
			$.post(B5M_UC.rootPath+'/user/info/data/editPwd.htm',$(e.currentTarget).parents("form:first").serialize(),function(response){
				COMMON.checkJSonData(response);
				 if(response.ok==true){
	      			COMMON.showSimpleTip('修改密码成功','修改密码');
	      			var vc = 2;
	    			var sh = setInterval(function(){
	    				vc = vc - 1;
	    				if(vc <= 0){
	    					vc = 2;
	    					pageJump();
	    					clearInterval(sh);
	    				}
	    			},1000);
	      			
	    			function pageJump(){
	    				window.location.href = B5M_UC.rootPath+"/forward.htm?method=/user/user/index";;
	    			}
	    			
	        	}else{
	        		COMMON.showSimpleTip(response.data,'修改密码');
	        		$('#password').val('');
		        	$('#newPassword').val('');
		        	$('#confirmPassword').val(''); 
		        	$('.cl-999').empty();
	        	}
			});
		}
	});
		
	module.exports=EditPasswordView;
	
});