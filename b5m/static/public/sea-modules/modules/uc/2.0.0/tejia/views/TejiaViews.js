define('modules/2.0.0/tejia/views/TejiaViews',['$','underscore','backbone','common','modules/2.0.0/tejia/views/TejiaView','modules/2.0.0/tejia/models/TejiaCollection'],function(require, exports, module){
	
	this.COMMON = require('common');
	this.underscore = require('underscore');
	this.Backbone = require('backbone');
	var TejiaView = require('modules/2.0.0/tejia/views/TejiaView');
	var TejiaCollection= require('modules/2.0.0/tejia/models/TejiaCollection');
	var TejiaViews = Backbone.View.extend({
		initialize: function (opt) {
			this.el = opt.el;
			this.render();
	    },
	    render: function () {
	    	
	    	this.$el.find('.ui-loading').hide();
	    	
	    	this.tejiaCollection = new TejiaCollection.TejiaCollection();
	    	this.showTejiaList();
	    	
	    	if(this.tejiaCollection.length == 0)
				this.$el.append('<div class="ui-empty-data"><span class="tejia-no-data"> 我们正在为您准备推荐商品...</span></div>').show();
	    },
	    
	  //tejia 集合拼装
	    showTejiaList:function(){
	    	 this.tejiaCollection.each(function(tejia, index){
	    		 var itemClass = '';
	 			if((index+1)%4 == 0)
	 				itemClass = 'last';
	    		 var htmlElement = new TejiaView({
	    			 model:tejia,
	    			 attributes: {
	 	    			'class':itemClass
	 	    		}
	    		 });
	    		 this.$el.find('ul').append(htmlElement.el);
	    	 },this);
	    }
    });
	
	module.exports=TejiaViews;
	
});