define('modules/2.0.0/tejia/views/TejiaView',['$','underscore','backbone','common','modules/2.0.0/tejia/tpl/tejia-item.html'],function(require, exports, module){
	
	this.COMMON = require('common');
	this.underscore = require('underscore');
	this.Backbone = require('backbone');

	var TejiaView = Backbone.View.extend({
		tagName: 'li',		
	    initialize: function () {
	    	this.render();
	    },	    
	    render: function () {
	    	//加载模版
			if(this.template === undefined)
				this.template = underscore.template(require('modules/2.0.0/tejia/tpl/tejia-item.html'));
	    	
	    	var newDomObjHtml = this.template(this.model.toJSON());
			this.$el.html(newDomObjHtml);
			return this;
	    }
	});	
	
	module.exports=TejiaView;
});