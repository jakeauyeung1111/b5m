define(['$','underscore','backbone','common','modules/uc/2.1.0/goods/views/TuanViewItem','modules/uc/2.1.0/goods/models/TuanCollection'],function(require, exports, module){
	
	this.COMMON = require('common');
	this._ = require('underscore');
	this.Backbone = require('backbone');
	this.TuanView = require('modules/uc/2.1.0/goods/views/TuanViewItem');
	this.TuanCollection= require('modules/uc/2.1.0/goods/models/TuanCollection');
	
	var root = undefined;
	this.TuanViews = Backbone.View.extend({
		template: _.template('<ul class="cfx"></ul>'),
		initialize: function (opt) {
			root = this;
			this.$el.append(this.template(opt));
			this.render();
	    },
	    render: function () {
	    	
	    	this.$el.find('.ui-loading').hide();
	    	
	    	this.tuanCollection = new TuanCollection();
	    	this.tuanCollection.fetch({
				success:function(){
					root.showTuanList();
					if(root.tuanCollection.length == 0)
						root.$el.append('<div class="ui-empty-data">我们正在为您准备推荐商品...</div>').show();
				}
			});
	    },
	    showTuanList:function(){
	    	 this.tuanCollection.each(function(tuan, index){
	    		 var itemClass = '';
	 			if((index+1)%4 == 0)
	 				itemClass = 'last';
	    		 var htmlElement = new TuanView({
	    			 model:tuan,
	    			 attributes: {
	 	    			'class':itemClass
	 	    		}
	    		 });
	    		 this.$el.find('ul').append(htmlElement.el);
	    	 },this);
	    }
    });
	
	module.exports=TuanViews;
	
});