define(['$','underscore','backbone','common','modules/uc/2.1.0/goods/views/TejiaViewItem','modules/uc/2.1.0/goods/models/TejiaCollection'],function(require, exports, module){
	
	this.COMMON = require('common');
	this.underscore = require('underscore');
	this.Backbone = require('backbone');
	this.TejiaView = require('modules/uc/2.1.0/goods/views/TejiaViewItem');
	this.TejiaCollection= require('modules/uc/2.1.0/goods/models/TejiaCollection');
	
	var root = undefined;
	this.TejiaViews = Backbone.View.extend({
		template: _.template('<ul class="cfx"></ul>'),
		initialize: function (opt) {
			root = this;
			this.$el.append(this.template(opt));
			this.render();
	    },
	    render: function () {
	    	
	    	this.$el.find('.ui-loading').hide();
	    	
	    	this.tejiaCollection = new TejiaCollection();
	    	this.tejiaCollection.fetch({
				success:function(){
					root.showTejiaList();
					if(root.tejiaCollection.length == 0)
						root.$el.append('<div class="ui-empty-data">我们正在为您准备推荐商品...</div>').show();
				}
			});
	    },
	    showTejiaList:function(){
	    	 this.tejiaCollection.each(function(tejia, index){
	    		 var itemClass = '';
	 			if((index+1)%4 == 0)
	 				itemClass = 'last';
	    		 var htmlElement = new TejiaView({
	    			 model:tejia,
	    			 attributes: {
	 	    			'class':itemClass
	 	    		}
	    		 });
	    		 this.$el.find('ul').append(htmlElement.el);
	    	 },this);
	    }
    });
	
	module.exports=TejiaViews;
	
});