define(['$','underscore','backbone','common','modules/uc/2.1.0/goods/tpl/tuan-item.html'],function(require, exports, module){
	
	this.COMMON = require('common');
	this._ = require('underscore');
	this.Backbone = require('backbone');

	this.TuanViewItem = Backbone.View.extend({
		tagName: 'li',		
	    initialize: function () {
	    	this.render();
	    },	    
	    render: function () {
	    	//加载模版
			if(this.template === undefined)
				this.template = _.template(require('modules/uc/2.1.0/goods/tpl/tuan-item.html'));
			this.$el.html(this.template(this.model.toJSON()));
			return this;
	    }
	});	
	
	module.exports=TuanViewItem;
});