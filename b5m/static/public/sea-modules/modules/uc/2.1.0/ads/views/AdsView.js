define(['$','common','underscore','backbone','modules/uc/2.1.0/ads/models/AdsCollection'],function(require, exports, module){
	this._ = require('underscore');
	this.Backbone = require('backbone');
	this.COMMON = require('common');
	this.AdsCollection = require('modules/uc/2.1.0/ads/models/AdsCollection');
	
	
	var AdsView = Backbone.View.extend({
		
		initialize: function () {
			this.adsCollection =  new AdsCollection({position:this.options.position});
			var self_this = this;
			this.adsCollection.fetch({
	    		success:function(){
	    			self_this.render();
	    		},
	    		error:function(e){
	    			console.log(e);
	    		}
	    	});
	    },	    
	    render: function () {
	    	var adsCollection= this.adsCollection.toJSON();
	    	var defaultHtml = 'modules/uc/2.1.0/ads/tpl/ad-postion-1.html';
	    	
	    	if(!COMMON.isEmptyValue(this.options.type)){
	    		if(this.options.type ==2)
	    			defaultHtml = 'modules/uc/2.1.0/ads/tpl/ad-postion-2-3.html';
	    	}
	    	var self_this = this;
	    	require.async(defaultHtml,function(html){
	    		self_this.template= _.template(html);
				if(COMMON.isEmptyValue(adsCollection)){
					return;
				}
				$(".ui-loading").hide();
				var htmlDom = self_this.template({adsCollection:adsCollection});
				self_this.$el.html(htmlDom);
			});
		}
	});
	module.exports = AdsView;
});