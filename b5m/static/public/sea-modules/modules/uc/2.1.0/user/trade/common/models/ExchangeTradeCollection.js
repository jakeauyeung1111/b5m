define(['$','underscore','backbone','common'],function(require, exports, module){
	this._ = require('underscore');
	this.Backbone = require('backbone');
	this.COMMON = require('common');
	
	var ExchangeTradeCollection = {
			Models:{},
			Collections:{},
			Urls:{}
	};
	
	var domain= B5M_UC.rootPath;
	//var domain= "http://ucenter.stage.bang5mai.com";
	
	ExchangeTradeCollection.Urls.categorysUrl = domain+"/dh/category.do";
	ExchangeTradeCollection.Urls.categoryDetailsUrl =domain+"/dh/order.do";
	
	ExchangeTradeCollection.Collections.categorys = Backbone.Collection.extend({
		url: ExchangeTradeCollection.Urls.categorysUrl,
		sync:function(method, model, options){
			var params = _.extend({
				async:false,
				cache:false,
				dataType: 'jsonp',
				jsonp:"jsonpCallback",   // the api requires the jsonp callback name to be this exact name
	            type: 'POST',
	            url: model.url,
	            processData: true
	        }, options);
	        return $.ajax(params);
		},
		parse:function(response) {
			if(response.ok)
				return response.data;
			return response;
		},
		initialize:function(){
		}
	});
	
	
	ExchangeTradeCollection.Collections.categoryDetails = Backbone.Collection.extend({
		url: ExchangeTradeCollection.Urls.categoryDetailsUrl,
		sync:function(method, model, options){
			var params = _.extend({
				async:false,
				cache:false,
				dataType: 'jsonp',
				jsonp:"jsonpCallback",   // the api requires the jsonp callback name to be this exact name
	            type: 'POST',
	            url: model.url,
	            processData: true
	        }, options);
	        return $.ajax(params);
		},
		parse:function(response) {
			if(response.ok)
				return this.initData(response.data);
			
			return response;
		},
		initData:function(data){
			_.each(data.datas,function(obj){
				if(!COMMON.isEmptyValue(obj.edate))
					obj.edate=COMMON.formatTime(obj.edate);
				if(!COMMON.isEmptyValue(obj.userAddress)){
					try{
						var addressObject = JSON.parse(obj.userAddress);
						obj.couponNo = addressObject.couponNo;
						obj.couponPwd = addressObject.couponPwd;
					}catch(e){}
				}
				if(!COMMON.isEmptyValue(obj.couponPrice)){
					obj.couponType=1;
					if($.contains(obj.couponPrice,"-")){
						obj.couponType=2;
						var splitArray = obj.couponPrice.split("-");
						obj.fullPrice = splitArray[0];
						obj.cutPrice =splitArray[1];
					}else{
						obj.cutPrice = obj.couponPrice;
					}
				}
			});
			return data;
		},
		initialize:function(){
		}
	});
	
	module.exports.categorys = ExchangeTradeCollection.Collections.categorys;
	module.exports.categoryDetails = ExchangeTradeCollection.Collections.categoryDetails;
});