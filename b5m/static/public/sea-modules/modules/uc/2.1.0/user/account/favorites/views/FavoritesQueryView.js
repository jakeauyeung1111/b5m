define(['$','underscore','backbone','common','modules/uc/2.1.0/user/account/favorites/views/WebsiteView','modules/uc/2.1.0/user/account/favorites/tpl/favorites-query.html','modules/uc/2.1.0/user/account/favorites/views/FavoritesListView','m/ui/tabswitch/1.0.0/tabswitch'],function(require, exports, module){
	
	this._ = require('underscore');
	this.Backbone = require('backbone');
	this.COMMON = require('common');
	
	this.tpl_favorites_query = require('modules/uc/2.1.0/user/account/favorites/tpl/favorites-query.html');
	this.WebsiteView = require('modules/uc/2.1.0/user/account/favorites/views/WebsiteView');
	this.FavoritesListView = require('modules/uc/2.1.0/user/account/favorites/views/FavoritesListView');
	this.Tabswitch = require('m/ui/tabswitch/1.0.0/tabswitch');
	
	var FavoritesQueryView = {
			Views:{},
			Urls:{}
	};
	
	FavoritesQueryView.Views.FavoritesQueryView = Backbone.View.extend({
		template: _.template(tpl_favorites_query),
		initialize:function(opt){
			
			//初始化参数
			this.parameterData = this.initParameter(opt);
			
			this.websiteView = new WebsiteView();
			
			this.el = opt.el;
			this.render();
			
			this.initNoticeState();
			
			new Tabswitch({
	            el: '#ui-tab-1',
	            triggers: '.ui-tab-nav li',
	            panels: '.ui-tab-content .ui-tab-item',
	            activeTriggerClass: 'ui-tab-active'
	        });
		},
		initParameter:function(para){
			return _.extend({
				pageNum:1,
				pageSize:10,
				showPage:true,
				tplName:'default'
			},para);
		},
		initNoticeState:function(){
			$.getJSON(B5M_UC.rootPath+'/gc/msg/notice/setting/data/read.htm',{
				userId:Cookie.get('token'),
				_:Math.random()
			},function(data){
				if(!data.ok){
					COMMON.showTip(data.message);
				}else{
					$('#J_notice > li').each(function(){
						_.each(data.data,function(obj){
							if(obj.bizType == 1 && obj.messageType == this.attr('attr-type')){
								if(obj.status == 0){
									this.removeClass('current');
								}else if(obj.status == 1){
									this.addClass('current');
								}
							}
						},$(this));
					});
				}
			});
		},
		render:function(){
			
			//渲染模版
			this.$el.html(this.template());
			//渲染商家
			this.$el.find('#website-list').append(this.websiteView.$el.html());
			//渲染收藏数据
			this.renderFavoritesListView({
				type:0,
				priceType:0
			});
			
			return this;
		},
		events:{
			'mouseenter .ui-tab-nav > li':'tabFocusin',
			'click .webSite-But':'webSiteClick',
			'click #J_notice > li':'noticeHandler'
		},
		noticeHandler:function(e){
			e.preventDefault();
			var $this = $(e.currentTarget),type = $this.attr('attr-type');
			if (!$this.hasClass('current')) {
				this.editNoticeState(type,1,function(){
					$this.addClass('current');
				});
			} else {
				$this.removeClass('current');
				this.editNoticeState(type,0);
			}
		},
		editNoticeState:function(messageType,status,successHandler){
			$.getJSON(B5M_UC.rootPath+'/gc/msg/notice/setting/data/edit.htm',{
				userId:Cookie.get('token'),
				bizType:1,
				messageType:messageType,
				status:status,
				_:Math.random()
			},function(data){
				if(!data.ok && data.code == 40001){
					COMMON.showTip(data.message);
					setTimeout(function(){
						window.location = B5M_UC.rootPath + '/forward.htm?method=/user/account/security/mobile/bindMobile';
					},1000);
				}else if(!data.ok && data.code == 40002){
					COMMON.showTip(data.message);
					setTimeout(function(){
						window.location = B5M_UC.rootPath + '/forward.htm?method=/user/account/security/email/validateEmail';
					},1000);
				}else if(!data.ok){
					COMMON.showTip(data.message);
				}else if(data.ok){
					successHandler && typeof successHandler == 'function' && successHandler(data);
				}
			});
		},
		tabFocusin:function(e){
			
			this.$el.find('.webSite-But').each(function(index){
				if(index == 0){
					$(this).addClass('current');
				}else{
					$(this).removeClass('current');
				}
			});
			
			var $targetDom = $(e.currentTarget), type = $targetDom.attr('attr-type');
			if(type == 0){
				this.renderFavoritesListView({
					type:type,
					priceType:0
				});
			}else if(type == 1){
				this.renderFavoritesListView({
					type:type,
					priceType:1
				});
			}
		},
		webSiteClick:function(e){
			var $targetDom = $(e.currentTarget), code = $targetDom.attr('attr-code');
			
			this.$el.find('#website-list .current').removeClass('current');
			$targetDom.addClass('current');
			
			//检测当前选中tab
			var _type = this.$el.find('.ui-tab-active').attr('attr-type'),_priceType=undefined;
			if(_type == 1)
				_priceType = 1;
			
			this.renderFavoritesListView({
				type:_type,
				priceType:_priceType,
				webSite:code
			});
		},
		renderFavoritesListView:function(opt){
			new FavoritesListView({
				$el:this.$el.find('.ui-tab-item:eq('+opt.type+')').empty(),
				priceType:opt.priceType,
				webSite:opt.webSite,
				pageSize:this.parameterData.pageSize
			});
		}
	});
	
	module.exports = FavoritesQueryView.Views.FavoritesQueryView;

});