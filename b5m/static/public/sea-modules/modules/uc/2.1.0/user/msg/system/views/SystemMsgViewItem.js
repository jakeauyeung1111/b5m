define(['$','underscore','backbone','common','modules/uc/2.1.0/user/msg/system/tpl/system-msg-item.html','arale/dialog/1.2.6/confirmbox'],function(require, exports, module){
	
	this._ = require('underscore');
	this.Backbone = require('backbone');
	this.COMMON = require('common');
	this.ConfirmBox = require('arale/dialog/1.2.6/confirmbox');
	
	this.tpl_system_msg_item = require('modules/uc/2.1.0/user/msg/system/tpl/system-msg-item.html');
	
	var SystemMsgViewItem = {
			Views:{}
	};
	
	SystemMsgViewItem.Views.SystemMsgViewItem = Backbone.View.extend({
		tagName: 'li',
		template:_.template(tpl_system_msg_item),
		initialize:function(opt){
			this.SystemMsgView = opt.SystemMsgView;
			this.$el.html( this.template( this.model.toJSON()));
			
			//已读设置为细体
			if(this.model.get('status') == 1)
				this.$el.find('.messageBody').attr('class','messageBody message-read');
		},
		events:{
			'click .del':'delHandler',
			'click .msgDetailBut':'msgDetailHandler'
		},
		delHandler:function(e){
			var self = this;
			new ConfirmBox({
		        trigger: e.currentTarget,
		        title: '删除消息',
		        message: '请确认是否删除该消息？',
		        onConfirm: function() {
		        	self.updateReadStateOrDelMsg(2,function(result){
						if(!result.ok){
							COMMON.showTip(result.data);
							return;
						}
						
						var _pageNum = self.SystemMsgView.page.settings.pageNum;
						if(self.SystemMsgView.systemMsgCollection.length == 1 && _pageNum>1)
							_pageNum = _pageNum -1;
						
						self.SystemMsgView.pageHandler({
							pageNum:_pageNum
						});
					});
		        	
		        	this.destroy();
		        },
		        onCancel:function(){
		        	this.destroy();
		        },
		        beforeHide:function(){
		        	this.destroy();
		        }
		    }).show();
		},
		msgDetailHandler:function(e){
			var $detail = $(e.currentTarget).closest('dl').find('.center-msg-detail');
			if($detail.attr('isShow') == 'true'){
				$detail.attr('isShow',false).hide();
			}else{
				$detail.attr('isShow',true).show();
			}
			
			//修改为已读
			var self = this;
			if(this.model.get('status') == 0)
				this.updateReadStateOrDelMsg(1,function(response){
					if(response.ok){
						self.model.set('status',1);
						self.$el.find('.messageBody').attr('class','messageBody message-read');
						var $newMsgCount = $('.new-msg-count').find('em'),num = (parseInt($newMsgCount.html())-1); 
						num <= 0?$('.new-msg-count').empty():$newMsgCount.html(num);
					}
				});
		},
		updateReadStateOrDelMsg:function(_status,callbackFn){
			$.getJSON(B5M_UC.rootPath+'/user/message/data/update.htm?jsonpCallback=?',{
				messageId:this.model.get('id'),
				status:_status
			},function(response){
				COMMON.checkJSonData(response);
				if(callbackFn && (typeof callbackFn).toLowerCase() == "function")
					callbackFn(response);
			});
		}
	});
	
	module.exports = SystemMsgViewItem.Views.SystemMsgViewItem;

});