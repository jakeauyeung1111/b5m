define(['$','underscore','backbone','common','modules/uc/2.1.0/user/account/favorites/models/WebsiteCollection','modules/uc/2.1.0/user/account/favorites/views/WebsiteItemView'],function(require, exports, module){
	
	this._ = require('underscore');
	this.Backbone = require('backbone');
	this.COMMON = require('common');
	
	this.WebsiteCollection = require('modules/uc/2.1.0/user/account/favorites/models/WebsiteCollection');
	this.WebsiteItemView = require('modules/uc/2.1.0/user/account/favorites/views/WebsiteItemView');
	
	var WebsiteView = {
			Views:{},
			Urls:{}
	};
	
	WebsiteView.Views.WebsiteView = Backbone.View.extend({
		initialize:function(){
			
			this.websiteCollection = new WebsiteCollection();
			
			this.render();
		},
		render:function(){
			this.websiteCollection.each(function( item, index) {
	            this.renderData( item, index );
	        }, this );
		},
		renderData:function(item, index){
			var websiteItemView = new WebsiteItemView({
	            model: item
	        });
			if(index == 0)
				websiteItemView.$el.find('a').addClass('current');
	        this.$el.append( websiteItemView.el );
		}
	});
	
	module.exports = WebsiteView.Views.WebsiteView;

});