define(['$','underscore','backbone','common','modules/uc/2.1.0/user/trade/korea/tpl/order-item.html'],function(require, exports, module){
	
	this._ = require('underscore');
	this.Backbone = require('backbone');
	this.COMMON = require('common');
	
	this.tpl = require('modules/uc/2.1.0/user/trade/korea/tpl/order-item.html');
	
	var OrderItemView = {
			Views:{}
	};
	
	OrderItemView.Views.OrderItemView = Backbone.View.extend({
		template:_.template(tpl),
		tagName: 'tr',
		initialize:function(opt){
			this.$el.html( this.template( this.model.toJSON() ) );
			this.render();
		}
	});
	
	module.exports = OrderItemView.Views.OrderItemView;

});