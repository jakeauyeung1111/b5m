define(['$','underscore','backbone','common','modules/uc/2.1.0/user/account/favorites/tpl/favorites-list-item.html','arale/cookie/1.0.2/cookie','arale/dialog/1.2.6/confirmbox'],function(require, exports, module){
	
	this._ = require('underscore');
	this.Backbone = require('backbone');
	this.COMMON = require('common');
	this.ConfirmBox = require('arale/dialog/1.2.6/confirmbox');
	
	this.tpl = require('modules/uc/2.1.0/user/account/favorites/tpl/favorites-list-item.html');
	this.Cookie = require('arale/cookie/1.0.2/cookie');
	this.delUrl = B5M_UC.rootPath+'/gc/user/favorites/data/delete.htm?jsonpCallback=?';
//	this.delUrl = 'http://ucenter.stage.bang5mai.com/gc/user/favorites/data/delete.htm?jsonpCallback=?';
	
	var FavoritesListItemView = {
			Views:{}
	};
	
	FavoritesListItemView.Views.FavoritesListItemView = Backbone.View.extend({
		template:_.template(tpl),
		tagName: "li",
		initialize:function(opt){
			this.FavoritesListView = opt.FavoritesListView;
			this.$el.html( this.template( this.model.toJSON() ) );
			this.render();
		},
		events:{
			'click .del-favorites-but':'delFavoritesById'
		},
		delFavoritesById:function(e){
			var self=this,$currentDom = $(e.currentTarget);
			new ConfirmBox({
		        trigger: e.currentTarget,
		        title: '我的收藏',
		        message: '请确认是否删除收藏的宝贝？',
		        onConfirm: function() {
		        	$.getJSON(delUrl,{
						userId:Cookie.get('token'),
						ugcId:$currentDom.attr('attr-id')
					},function(result){
						
						COMMON.checkJSonData(result);
						if(!result.ok){
							COMMON.showTip(result.message);
							return;
						}
						
						var _pageNum = self.FavoritesListView.page.settings.pageNum;
						if(self.FavoritesListView.favoritesCollection.length == 1 && _pageNum>1)
							_pageNum = _pageNum -1;
						
						self.FavoritesListView.pageHandler({
							pageNum:_pageNum
						});
						
					});
		        	
		        	this.destroy();
		        },
		        onCancel:function(){
		        	this.destroy();
		        },
		        beforeHide:function(){
		        	this.destroy();
		        }
		    }).show();
		}
	});
	
	module.exports = FavoritesListItemView.Views.FavoritesListItemView;

});