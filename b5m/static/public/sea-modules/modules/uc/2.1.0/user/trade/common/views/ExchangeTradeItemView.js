define(['$','common','underscore','backbone','modules/uc/2.1.0/user/trade/common/tpl/exchange-category-item.html','modules/uc/2.1.0/user/trade/common/views/ExchangeTradeDetailListView'],function(require, exports, module){
	
	this._ = require('underscore');
	this.Backbone = require('backbone');
	this.COMMON = require('common');
	var tpl = require('modules/uc/2.1.0/user/trade/common/tpl/exchange-category-item.html');
	this.ExchangeTradeDetailListView = require('modules/uc/2.1.0/user/trade/common/views/ExchangeTradeDetailListView');

	var ExchangeTradeItemView = Backbone.View.extend({
		template: _.template(tpl),
		events:{
			"click .exchange_category":"query"
		},
		initialize : function(opt) {
			this.render();
		},
		render : function() {
			var html = this.template(this.model.toJSON());
			this.$el.html(html);
			return this;
		},
		query:function(){
			var self_this = this ;
			this.$el.parent().parent().find('a').removeClass('current');
			this.$el.find('a').addClass('current');
			new ExchangeTradeDetailListView({
				el:"#exchange-details",
				cateId:self_this.model.id
			});
		}
	});
	
	module.exports = ExchangeTradeItemView;
});