define(['$','underscore','backbone','modules/uc/2.1.0/user/account/info/models/UserModel'],function(require, exports, module){
	this._ = require('underscore');
	this.Backbone = require('backbone');
	this.UserCenterCollection = require('modules/uc/2.1.0/user/account/info/models/UserModel');
	
	var SecuritySettingView = Backbone.View.extend({
		initialize: function (opt) {
	    	this.el = opt.el;
	    	this.data =  new UserCenterCollection.UserModel({type:1}).toJSON();
	    	this.render();
	    },	    
	    render: function () {
	    	var self = this;
	    	require.async('modules/uc/2.1.0/user/account/info/tpl/security-item.html',function(html){
				self.template= _.template(html);
				self.$el.html(self.template(self.data));
			});
		}
	});
	
	module.exports = SecuritySettingView;
});