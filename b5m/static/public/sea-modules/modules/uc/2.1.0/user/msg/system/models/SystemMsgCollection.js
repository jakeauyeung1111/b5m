define(['$','underscore','backbone','common'],function(require, exports, module){
	
	this._ = require('underscore');
	this.Backbone = require('backbone');
	this.COMMON = require('common');
	
	var SystemMsgCollection = {
			Models:{},
			Collections:{},
			Urls:{}
	};
	SystemMsgCollection.Urls.systemMsgList = B5M_UC.rootPath+'/user/message/data/list.htm';
	
	SystemMsgCollection.Models.Msg = Backbone.Model.extend({});
	
	SystemMsgCollection.Collections.SystemMsgCollection = Backbone.Collection.extend({
		url: SystemMsgCollection.Urls.systemMsgList,
		model:SystemMsgCollection.Models.Msg,
		sync:function(method, model, options){
			var params = _.extend({
				async:false,
				cache:false,
				dataType: 'jsonp',
				jsonp: 		"jsonpCallback",   // the api requires the jsonp callback name to be this exact name
	            type: 'POST',
	            url: model.url,
	            processData: true
	        }, options);
	        return $.ajax(params);
		},
		parse: function(response) {
			COMMON.checkJSonData(response);
			if(response.ok)
				return this.initModelData(response.data);
			COMMON.showTip(response.data);
			return null;
		},
		initModelData:function(data){
			this.dataTotal = data.totalRecord;
			return data.records;
		}
	});
	
	module.exports = SystemMsgCollection.Collections.SystemMsgCollection;

});