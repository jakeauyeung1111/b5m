define(['$','underscore','backbone','common'],function(require, exports, module){
	
	this._ = require('underscore');
	this.Backbone = require('backbone');
	this.COMMON = require('common');
	
	this.tpl_task_item_1 = 'modules/uc/2.1.0/user/task/tpl/task-item-1.html';
	this.tpl_task_item_2 = 'modules/uc/2.1.0/user/task/tpl/task-item-2.html';
	
	var UserTaskItemView = {
			View:{}
	};
	
	UserTaskItemView.View.UserTaskItemView = Backbone.View.extend({
		tagName: 'tr',
		initialize:function(opt){
			
			var self = this;
			var tpl_task_item = tpl_task_item_1;
			
			switch(opt.taskItemTpl){
				case	1:tpl_task_item = tpl_task_item_1;break;
				case	2:tpl_task_item = tpl_task_item_2;break;
			}
			
			require.async(tpl_task_item, function(tpl) {
				self.template = _.template(tpl),
				self.$el.html( self.template( self.model.toJSON() ) );
				self.render();
			});
		}
	});
	
	module.exports.UserTaskItemView = UserTaskItemView.View.UserTaskItemView;

});