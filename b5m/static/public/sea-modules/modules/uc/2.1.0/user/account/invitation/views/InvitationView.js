define(['$', 'common', 'underscore', 'backbone', 'modules/uc/2.1.0/user/account/invitation/models/InvitationDetailModel', 'modules/uc/2.1.0/user/account/invitation/tpl/invitation.html', 'jquery/ZeroClipboard/1.2.3/ZeroClipboard', 'share/1.0.0/share'], function(require, exports, module){

    this.COMMON = require('common');
    this._ = require('underscore');
    this.Backbone = require('backbone');
    
    this.InvitationDetailModel = require('modules/uc/2.1.0/user/account/invitation/models/InvitationDetailModel');
    this.ZeroClipboard = require('jquery/ZeroClipboard/1.2.3/ZeroClipboard');
    this.Share = require('share/1.0.0/share');
    this.tpl_invitation = require('modules/uc/2.1.0/user/account/invitation/tpl/invitation.html');
    
    
    var InvitationView = Backbone.View.extend({
        template: _.template(tpl_invitation),
        tagName: "div",
        initialize: function(opt){
        
            //初始化参数
            this.parameterData = this.initParameter(opt);
            this.invitationDetailModel = new InvitationDetailModel();
            var self = this;
            this.invitationDetailModel.fetch({
                success: function(){
                    self.parameterData = _.extend(self.parameterData, self.invitationDetailModel.toJSON());
                    self.render();
                }
            });
        },
        initParameter: function(para){
            return _.extend({
                title: '邀请好友来帮5买',
                summary: '有个网购省钱利器我必须告诉你，搜一下商品就能省很多Money！还有更多优惠方式，还不立刻行动？'
            }, para);
        },
        render: function(){
            this.$el.find('.ui-loading').hide();
            try {
                this.$el.append(this.template(this.parameterData));
            } catch (e) {
                COMMON.showTip('出错啦！！！赶紧去买六合彩吧！！！');
            }
            
            var clip = new ZeroClipboard(this.$el.find('#copy-but')[0], {
            	moviePath: B5M_JS.jsPath + "/jquery/ZeroClipboard/1.2.3/ZeroClipboard.swf"
            });
            // 复制内容到剪贴板成功后的操作
            clip.on('complete', function(client, args){
            	COMMON.showTip("复制成功!!!");
            });
            
            new Share({
            	id:this.$el.find('.deal-share')[0],
                href:this.invitationDetailModel.get('invitationUrl'),
                content:this.parameterData.summary
            });
            
        }
    });
    
    module.exports = InvitationView;
});
