define(['$','common','underscore','backbone','modules/uc/2.1.0/user/trade/common/models/ExchangeTradeCollection','modules/uc/2.1.0/user/trade/common/views/ExchangeTradeItemView'],function(require, exports, module){
	this._ = require('underscore');
	this.Backbone = require('backbone');
	this.COMMON = require('common');
	this.ExchangeTradeCollection = require('modules/uc/2.1.0/user/trade/common/models/ExchangeTradeCollection');
	var ExchangeTradeItemView = require('modules/uc/2.1.0/user/trade/common/views/ExchangeTradeItemView');
	
	var ExchangeTradeView = Backbone.View.extend({
			
			initialize: function () {
				this.categorysCollection =  new ExchangeTradeCollection.categorys();
				var self_this = this;
				this.categorysCollection.fetch({
		    		success:function(){
		    			self_this.render();
		    		},
		    		error:function(e){
		    			console.log(e);
		    		}
		    	});
		    },	    
		    render: function () {
		    	this.categorysCollection.each(function(item, index) {
		    		var myExchangeTradeItemView = new ExchangeTradeItemView({
						model : item
					});
					this.$el.append(myExchangeTradeItemView.el);
		        }, this );
		    }
	});
	
	module.exports = ExchangeTradeView;
});