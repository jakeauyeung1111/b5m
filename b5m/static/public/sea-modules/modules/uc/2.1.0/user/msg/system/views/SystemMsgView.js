define(['$','underscore','backbone','common','modules/uc/default/common/Page','modules/uc/2.1.0/user/msg/system/models/SystemMsgCollection','modules/uc/2.1.0/user/msg/system/views/SystemMsgViewItem'],function(require, exports, module){
	
	this._ = require('underscore');
	this.Backbone = require('backbone');
	this.COMMON = require('common');
	
	this.Page = require('modules/uc/default/common/Page');
	
	this.tpl_account_history = 'modules/uc/2.1.0/user/msg/system/tpl/system-msg.html';
	this.SystemMsgCollection = require('modules/uc/2.1.0/user/msg/system/models/SystemMsgCollection');
	this.SystemMsgViewItem = require('modules/uc/2.1.0/user/msg/system/views/SystemMsgViewItem');
	
	var SystemMsgView = {
			Views:{},
			Urls:{}
	};
	
	SystemMsgView.Views.SystemMsgView = Backbone.View.extend({
		initialize:function(opt){
			
			//初始化参数
			this.parameterData = this.initParameter(opt);
			this.el = opt.el;
			
			var tpl_addr = tpl_account_history, self = this;
			if(this.parameterData.tplName == 'default')
				tpl_addr = tpl_account_history;
			require.async(tpl_addr, function(tpl) {
				
				self.$el.append(tpl);
				
				//初始化交易记录
				self.systemMsgCollection = new SystemMsgCollection();
				self.pageHandler({
					pageNum:1
				});
				
			});
			
		},
		initParameter:function(para){
			return _.extend({
				pageNum:1,
				pageSize:5,
				showPage:false,
				tplName:'default'
			},para);
		},
		render:function(){
			this.systemMsgCollection.each(function( item, index) {
	            this.renderData( item, index );
	        }, this );
		},
		renderData:function(item, index){
			
			var itemClass = '';
			if(index%2 != 0)
				itemClass = 'trEvenBg';
			
			var systemMsgViewItem = new SystemMsgViewItem({
	            model: item,
	            SystemMsgView:this,
	            attributes: {
	    			'class':itemClass
	    		}
	        });
	        this.$el.find('#user-message-list').append( systemMsgViewItem.el );
		},
		pageHandler:function(opt){
			
			//信息总条数修改
			this.refreshMsgCount();
			
			//加载系统消息
			var self = this;
			this.systemMsgCollection.fetch({
				data:{
					currentPage:opt.pageNum,
					pageSize:self.parameterData.pageSize
				},
                success: function(){
                	
                	self.hideLodingData();
                	
                	//设置总消息数量
                	self.$el.find('#message-count').empty().html(self.systemMsgCollection.dataTotal);
        			
        			//数据为空时处理
        			if(self.systemMsgCollection.length == 0){
    					self.$el.find('.center-msg-none').show();
        				return;
        			}
        			
        			//渲染系统消息数据
        			self.render();
        			
        			//刷新分页控件
        			if(self.systemMsgCollection.length > 0)
        				self.refreshPage(opt);
                }
            });
		},
		refreshMsgCount:function(){
			$.getJSON(B5M_UC.rootPath+'/user/message/data/count.htm?jsonpCallback=?',function(response){
				COMMON.checkJSonData(response);
				if(!response.ok){
					COMMON.showTip(response.data);
				}else{
					var $newMsgCount = $('.new-msg-count').empty();
					if(response.data != 0)
						$newMsgCount.html('(<em>'+response.data+'</em>)');
				}
			});
		},
		refreshPage:function(opt){
			
			//初始化分页控件
			if(!this.page){
				var self = this;
				this.page = new Page({
					onClick:function(pageNum, dom){
						self.pageHandler({
							pageNum:pageNum
						});
					}
				});
			}
			
			this.$el.find('.page-view').empty().append(this.page.getPageDom({
				pageNum : opt.pageNum,
				total : this.systemMsgCollection.dataTotal,
				pageSize : this.parameterData.pageSize
			}));
		},
		showLoadingData:function(){
			this.$el.find('.ui-loading').show();
			this.$el.find('.page-view').empty();
			this.$el.find('#user-message-list').empty();
			this.$el.find('.center-msg-none').hide();
		},
		hideLodingData:function(){
			this.$el.find('.ui-loading').hide();
			this.$el.find('.page-view').empty();
			this.$el.find('#user-message-list').empty();
			this.$el.find('.center-msg-none').hide();
		}
	});
	
	module.exports = SystemMsgView.Views.SystemMsgView;

});