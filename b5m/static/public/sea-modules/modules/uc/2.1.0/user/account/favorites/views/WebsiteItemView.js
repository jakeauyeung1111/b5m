define(['$','underscore','backbone','common'],function(require, exports, module){
	
	this._ = require('underscore');
	this.Backbone = require('backbone');
	this.COMMON = require('common');
	
	this.tpl = '<a href="javascript:void(0)" attr-code="<%=code%>" class="webSite-But"><%=name%></a>';
	
	var WebsiteItemView = {
			Views:{},
			Urls:{}
	};
	
	WebsiteItemView.Views.WebsiteItemView = Backbone.View.extend({
		template: _.template(tpl),
		tagName:'li',
		initialize:function(opt){
			this.render();
		},
		render:function(){
			this.$el.html(this.template(this.model.toJSON()));
			return this;
		}
	});
	
	module.exports = WebsiteItemView.Views.WebsiteItemView;

});