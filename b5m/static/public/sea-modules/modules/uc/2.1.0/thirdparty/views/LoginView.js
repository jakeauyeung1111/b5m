define(['$','underscore','backbone','common'],function(require, exports, module){
	
	this._ = require('underscore');
	this.Backbone = require('backbone');
	this.COMMON = require('common');
	
	var LoginView = {
			Views:{},
			Urls:{}
	};
	
	LoginView.Views.LoginView = Backbone.View.extend({
		tagName:'div',
		attributes : {
			'class':'platform-link'
		},
		initialize:function(opt){
			//初始化参数
			this.parameterData = this.initParameter(opt);
			var root = this;
			require.async('modules/uc/2.1.0/thirdparty/tpl/login-'+this.parameterData.tpl+'.html',function(tpl){
				root.template = _.template(tpl);
				root.render();
			});
		},
		initParameter:function(para){
			return _.extend({
				rootPath:B5M_UC.rootPath,
				iconSize:46,
				redirectUrl:'',
				tpl:1
			},para);
		},
		render:function(){
			this.$el.find('.ui-loading').hide();
			try{
				this.$el.html(this.template(this.parameterData));
			}catch (e) {
				COMMON.showTip('出错啦！！！赶紧去买六合彩吧！！！');
			}
		}
	});
	
	module.exports = LoginView.Views.LoginView;

});