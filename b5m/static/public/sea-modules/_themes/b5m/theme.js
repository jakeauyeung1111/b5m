var _ = require('underscore');

exports.filters = {
  getArchiveMenu: function(items) {
    
    var treeMenu = new Array(),treeMenuTemp,treeMenuTemp2,menuTemplate = {
      name:'This is Menu Name',
      anchor:undefined,
      items:new Array(),
      children:new Array()
    };
    
    var temp = true, temp2 = true;
    for (var i=0; i<items.length; i++) {
//      var regData = items[i].meta.directory.match(/.*\/(\d+\.\d+\.\d)\/(\w+)\/.*/);
        var regData = items[i].meta.directory.match(/modules\/([^\/]+)\/([^\/]+).*/);
      
      for (var j=0; j<treeMenu.length; j++) {
        if (treeMenu[j].name === regData[1]) {
          for (var k=0; k<treeMenu[j].children.length; k++) {
            if (treeMenu[j].children[k] && treeMenu[j].children[k].name === regData[2]) {
              treeMenu[j].children[k].items.push(items[i])
              temp2 = false;
            }
          }
          if (temp2) {
            treeMenuTemp = {
                              name:'This is Menu Name',
                              anchor:undefined,
                              items:new Array(),
                              children:new Array()
                            };
            treeMenuTemp.name = regData[2];
            treeMenuTemp.items.push(items[i]);
            treeMenuTemp.anchor = regData[1]+'-'+regData[2];
            treeMenu[j].children.push(treeMenuTemp);
          }
          temp = false;
          temp2 = true;
        }
      }
      if (temp) {
        treeMenuTemp = {
                          name:'This is Menu Name',
                          anchor:undefined,
                          items:new Array(),
                          children:new Array()
                        };
        treeMenuTemp.name = regData[1];
        treeMenuTemp.anchor = regData[1];
        treeMenuTemp2 = {
                          name:'This is Menu Name',
                          anchor:undefined,
                          items:new Array(),
                          children:new Array()
                        };
        treeMenuTemp2.name = regData[2];
        treeMenuTemp2.items.push(items[i]);
        treeMenuTemp2.anchor = regData[1]+'-'+regData[2];
        treeMenuTemp.children.push(treeMenuTemp2);
        treeMenu.push(treeMenuTemp);
      }      
      temp = true;
      temp2 = true;
    }
    var temp3;
    for(var i=0; i< treeMenu.length; i++){
      if (i<treeMenu.length-1 && treeMenu[i].name.localeCompare(treeMenu[i+1].name)) {
        temp3 = treeMenu[i];
        treeMenu[i] = treeMenu[i+1];
        treeMenu[i+1] = temp3
      }
    }
    return treeMenu;
  }
};