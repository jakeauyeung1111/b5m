var fs = require('fs');
var url = require('url');

exports.createUiDoc = function (callback, req) {

    var DocUi = {

        //所有str
        globalStr: {
            data: '',  //生成数据
            page: 'ui',
            path: './modules/',  //路径
            url: url.parse(req.url, true).query.mod
        },

        //init
        init: function () {
            var _this = this;
            _this.globalStr.ui = [];
            _this.globalStr.demos = [];
            _this.openFile();
        },


        //打开文件
        openFile: function () {

            var _this = this;

            fs.readdir(_this.globalStr.path + 'ui', function (err, files) {
                if (err) throw err;

                appendUiFiles(files);

                function appendUiFiles(files) {

                    if (files.length === 0) {
                        callback(_this.globalStr);
                        return;
                    }

                    _this.openApp(_this.globalStr.path + 'ui/' + files[0] + '/' + files[0] + '.js', function (str) {

                        _this.globalStr.ui.push({});

                        var i = _this.globalStr.ui.length - 1;

                        var id = _this.globalStr.ui[i].id = /\*\s?@id\s?([^\r\n]+)/.exec(str)[1];   //查询utils id

                        //如果id== 当前url参数 并且url无mod参数
                        if (id === _this.globalStr.url || ( !_this.globalStr.url && i===0 )) {

                            _this.globalStr.mod = {};
                            _this.globalStr.mod.id = id;
                            _this.globalStr.mod.description = /\*\s?@description\s?([^\r\n]+)/.exec(str)[1];   //查询utils description
                            _this.globalStr.mod.namespace = /\*\s?@namespace\s?([^\r\n]+)/.exec(str)[1];   //查询utils namespace
                            _this.globalStr.mod.return = /\*\s?@return\s?([^\r\n]+)/.exec(str)[1];   //查询utils 返回类型
                            _this.globalStr.mod.void = /\*\s?@void\s?([^\r\n]+)/.exec(str)[1];    //查询utils example
                            _this.globalStr.mod.params = str.match(/\*\s?@param\s[^\r\n]+/g);  //查询utils params

                            //查询demo files
                            _this.openDemo(_this.globalStr.path + 'ui/' + files[0] + '/demo',function() {
                                files.shift();
                                return appendUiFiles(files);
                            });

                        }else {
                            files.shift();
                            return appendUiFiles(files);
                        }



                    });

                }

            });

        },

        //操作demo文件
        openDemo: function (url,democallback) {

            var _this = this;

            fs.readdir(url, function (err, files) {
                if (err) throw err;

                appendDemoFiles(files);

                function appendDemoFiles(files) {

                    if (files.length === 0) {
                        //完成数据，回调
                        if (democallback) {
                            democallback();  //获取数据成功，回调参数返回index.js
                        }
                        return;
                    }

                    _this.openApp(url + '/' + files[0], function (str) {

                        _this.globalStr.demos.push({});

                        var  i = _this.globalStr.demos.length-1;
                        _this.globalStr.demos[i].url = files[0];
                        _this.globalStr.demos[i].codes = str;
                        _this.globalStr.demos[i].title = /<title>(.+)<\/title>/.exec(str)[1];

                        files.shift();
                        return appendDemoFiles(files);

                    });

                }

            });
        },


        //打开文件code
        openApp: function (url, callbackapp) {
            //打开所有ui .js文件
            fs.open(url, 'r', 0666, function (err, fd) {
                if (err) throw err;
                fs.fstat(fd, function (err, stats) {
                    if (err) throw err;
                    var len = stats.size;  //获取长度
                    var buffer = new Buffer(len);  //缓存整个fn.js字符串长度
                    //读取util.js内容，整合文件用
                    fs.read(fd, buffer, 0, buffer.length, 0, function (err, written, str) {
                        if (err) throw err;
                        str = str.toString();
                        fs.close(fd);
                        return  callbackapp(str);
                    });
                });
            });
        }

    };


    DocUi.init();

}