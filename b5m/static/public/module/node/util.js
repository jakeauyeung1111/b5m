var fs = require('fs');

exports.createUtilsDoc = function(callback) {

    var DocUtil = {};

    DocUtil.globalStr= {
        data:'',  //生成数据
        page:'util',
        path:'./modules/'  //路径
    };

    DocUtil.init = function() {
        var _this = this;
        _this.globalStr.utils = [];

        fs.open(_this.globalStr.path +'fn/util.js','r',0666,function(err,fd){
            if(err) throw err;

            fs.fstat(fd,function(err,stats) {
                if(err) throw err;

                var len = stats.size;  //获取长度
                var buffer = new Buffer(len);  //缓存整个fn.js字符串长度

                //读取util.js内容，整合文件用
                fs.read(fd,buffer,0,buffer.length,0,function(err,written,str) {

                    if(err) throw err;
                    str = str.toString();

                    var reItem = /(\*\s?@id[\s\S]*?)(?:\/\*\* @end \*\/)/g;
                    var utilsArr = [];


                    while((result = reItem.exec(str))!==null) {
                        utilsArr.push(result[1]);
                    }

                    for(var i= 0,len = utilsArr.length;i<len;i++) {
                        _this.globalStr.utils.push({});
                        _this.globalStr.utils[i].id = /\*\s?@id\s?([^\r\n]+)/.exec(utilsArr[i])[1];   //查询utils id
                        _this.globalStr.utils[i].description = /\*\s?@description\s?([^\r\n]+)/.exec(utilsArr[i])[1];   //查询utils description
                        _this.globalStr.utils[i].namespace = /\*\s?@namespace\s?([^\r\n]+)/.exec(utilsArr[i])[1];   //查询utils namespace
                        _this.globalStr.utils[i].return = /\*\s?@return\s?([^\r\n]+)/.exec(utilsArr[i])[1];   //查询utils 返回类型
                        _this.globalStr.utils[i].void = /\*\s?@void\s?([^\r\n]+)/.exec(utilsArr[i])[1];    //查询utils example
                        _this.globalStr.utils[i].code = /\*\*\s?@code\s?([^\*\/]+)/.exec(utilsArr[i])[1].replace(/\r\n/g,'<br/>');    //查询utils example

                    }

                    if(typeof callback !== 'undefined') {
                        callback(_this.globalStr);  //获取数据成功，回调参数返回index.js
                    }

                    fs.close(fd);

                });

            });

        });

    }

    DocUtil.init();

}