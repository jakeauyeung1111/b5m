/**
 * @fileoverview 此文件是前端公用方法、定义。包含常用方法、命名空间、接口定义等
 * @version 1.1
 * @author B2C F2E team
 * @public
 */

/**
 * @description 定义b5m.com前端公共对象，所有b5m开发模块、插件都基于此main对象
 */
(function(window,$) {

    var slice = Array.prototype.slice;

    /**
     * @description b5m命名空间管理
     */
    $.namespace = function(ns_string) {
        var parts = ns_string.split('.'),
            parent = $,i;
        for(i=0;i<parts.length;i++) {
            if(typeof parent[parts[i]] === 'undefined') {
                parent[parts[i]] = {};
            }
            parent = parent[parts[i]];
        }
        return parent;
    };


    /**
     * @description 前端接口定义，所有接口存储在window.INTERFACE全局变量下,提供man方法管理
     * @namespace window
     * @param {string} type set|get|del
     * @param {string} name 要操作的接口名
     * @param {all} value 接口内容，仅在type为set的时候起效
     * @return {window.INTERFACE}
     * @example window.INTERFACE.man('set','comment',function(){alert('commint')});//这样 其他地方就可以调用到 window.INTERFACE.comment方法了
     */
    window.INTERFACE = window.INTERFACE || {
        length: 0,
        b5m: function(type, name, value) {
            switch (type) {
                case 'set' :
                    if (arguments.length === 3) {
                        this[name] = value;
                        this.length++;
                    }
                    break;
                case 'get' :
                    return this[name];
                    break;
                case 'del' :
                    delete this[name];
                    this.length--;
                    break;
                default:
            }
            return this;
        }
    };


    /**
     * @description 兼容console调试代码，如果觉得没控制台的浏览器alert麻烦，可以设置 .window.console.debug = false;
     */
    if (!window['console']) {
        window.console = {
            debug: true,
            log: function() {
                this.debug && alert(slice.call(arguments));
            },
            dir: function() {
                if (!this.debug) {
                    return;
                }
                var args = slice.call(arguments);
                for (var k in args) {
                    for (var j in args[k]) {
                        alert([args[k], args[k][j], j]);
                    }
                }
            }
        };
    }


    /**
     * @description 注册jquery namespace
     */
     $.extend({utils:{},ui:{}});

    /*codes*/

/**
 * @id isIE6
 * @description 判断当前浏览器是否IE6
 * @namespace jQuery.utils
 * @return {Boolean}
 * @void $.utils.isIE6()
 */

/** @code
  alert($.utils.isIE6());
 */

$.utils.isIE6 = function() {
    return typeof document.body.style.maxHeight === 'undefined';
}

/** @end */




/**
 * @id supportCss3
 * @description 判断浏览器是否支持某项css3属性
 * @namespace jQuery.utils
 * @return {Boolean} function(css3Name){} 返回的function，使用了闭包
 * @void $.utils.supportsCss3('box-shadow')
 */

/** @code
   if($.utils.supportsCss3('box-shadow')) {
     alert('该浏览器支持css3 box-shadow');
   }
 */

$.utils.supportsCss3 = (function() {

    var div = document.createElement('div'),
        vendors = 'Khtml O Moz Webkit'.split(' '),
        len = vendors.length;
    /**
     * @param {String} css3AttrName css3的属性名
     * @return {Boolean}
     */
    return function(prop) {
        if (window.ActiveXObject && parseInt(navigator.userAgent.toLowerCase().match(/msie ([\d.]+)/)[1], 10) <= 8) {
            return false;
        }
        if (prop in div.style)
            return true;
        if ('-ms-' + prop in div.style)
            return true;
        prop = prop.replace(/^[a-z]/, function(val) {
            return val.toUpperCase();
        });
        while (len--) {
            if (vendors[len] + prop in div.style) {
                return true;
            }
        }
        return false;
    };
})();


/** @end */



/**
 * @id isIE
 * @description 判断当前浏览器是否IE6
 * @namespace jQuery.utils
 * @return {Boolean}
 * @void $.utils.isIE()
 */

/** @code
 alert($.utils.isIE());
 */

$.utils.isIE = function() {
    return typeof window.attachEvent === 'undefined';
}

/** @end */

/**
 * @description 实现slider的函数，依赖jQuery
 * @namespace b5m.ui
 */
$.fn.slider = function(elem,w) {


    alert('slider');
    return;

    var elem = elem,
        container = elem.find('.container'),
        prev =elem.find('.prev'),
        next = elem.find('.next'),
        len = container.find('li').length,
        width = w,
        index =0;

    container.find('ul').width(width*len);

    if(len<=1) {
        next.addClass('last');
    }

    prev.click(function() {
        if(index > 0) {
            index --;
            container.animate({scrollLeft:index*width});
            if(index === 0) {
                $(this).addClass('first');
            }
            if(index!=len-1) {
                next.removeClass('last');
            }
        }
    });

    next.click(function(){
        if(index< len-1) {
            index ++;
            container.animate({scrollLeft:index*width});
            if(index === len-1) {
                $(this).addClass('last');
            }
            if(index!=0) {
                prev.removeClass('first');
            }
        }
    });

}

/**
 * @description 实现tab的函数，依赖jQuery
 * @namespace b5m.ui
 */
$.fn.tab = function() {

    alert('tab');

    return;

}



})(window,jQuery);