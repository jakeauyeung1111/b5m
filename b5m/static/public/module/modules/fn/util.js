/**
 * @id isIE6
 * @description 判断当前浏览器是否IE6
 * @namespace jQuery.utils
 * @return {Boolean}
 * @void $.utils.isIE6()
 */

/** @code
  alert($.utils.isIE6());
 */

$.utils.isIE6 = function() {
    return typeof document.body.style.maxHeight === 'undefined';
}

/** @end */




/**
 * @id supportCss3
 * @description 判断浏览器是否支持某项css3属性
 * @namespace jQuery.utils
 * @return {Boolean} function(css3Name){} 返回的function，使用了闭包
 * @void $.utils.supportsCss3('box-shadow')
 */

/** @code
   if($.utils.supportsCss3('box-shadow')) {
     alert('该浏览器支持css3 box-shadow');
   }
 */

$.utils.supportsCss3 = (function() {

    var div = document.createElement('div'),
        vendors = 'Khtml O Moz Webkit'.split(' '),
        len = vendors.length;
    /**
     * @param {String} css3AttrName css3的属性名
     * @return {Boolean}
     */
    return function(prop) {
        if (window.ActiveXObject && parseInt(navigator.userAgent.toLowerCase().match(/msie ([\d.]+)/)[1], 10) <= 8) {
            return false;
        }
        if (prop in div.style)
            return true;
        if ('-ms-' + prop in div.style)
            return true;
        prop = prop.replace(/^[a-z]/, function(val) {
            return val.toUpperCase();
        });
        while (len--) {
            if (vendors[len] + prop in div.style) {
                return true;
            }
        }
        return false;
    };
})();


/** @end */



/**
 * @id isIE
 * @description 判断当前浏览器是否IE6
 * @namespace jQuery.utils
 * @return {Boolean}
 * @void $.utils.isIE()
 */

/** @code
 alert($.utils.isIE());
 */

$.utils.isIE = function() {
    return typeof window.attachEvent === 'undefined';
}

/** @end */