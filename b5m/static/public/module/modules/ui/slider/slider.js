/**
 * @id slider
 * @description slider幻灯片，带ajax功能
 * @namespace jQuery.fn
 * @return null
 * @void $.slider(elem);
 * @param {String} 11111时间 如'2013/9/13 18:48'
 * @param {jQuery expr} jq 2222dom选择器表达式
 * @param {fn} 倒计时结束后回调 可选3333
 */


$.slider = function(elem,w) {

    var elem = elem,
        container = elem.find('.container'),
        prev =elem.find('.prev'),
        next = elem.find('.next'),
        len = container.find('li').length,
        width = w,
        index =0;

    container.find('ul').width(width*len);

    if(len<=1) {
        next.addClass('last');
    }

    prev.click(function() {
        if(index > 0) {
            index --;
            container.animate({scrollLeft:index*width});
            if(index === 0) {
                $(this).addClass('first');
            }
            if(index!=len-1) {
                next.removeClass('last');
            }
        }
    });

    next.click(function(){
        if(index< len-1) {
            index ++;
            container.animate({scrollLeft:index*width});
            if(index === len-1) {
                $(this).addClass('last');
            }
            if(index!=0) {
                prev.removeClass('first');
            }
        }
    });

}