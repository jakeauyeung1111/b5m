/**
 * @id insertFace
 * @description tab切换，带ajax功能
 * @namespace jQuery.fn
 * @return null
 * @void $('.tabs').tab();
 * @param {String} 时间 如'2013/9/13 18:48'
 * @param {jQuery expr} jq dom选择器表达式
 * @param {fn} 倒计时结束后回调 可选 *
 */


var isIE = document.all?true:false;

function faceReplace(url,str){	  //url 图片相对路径， str：传入发布内容字符串  如：	$('dd:last').html(faceReplace('images/v1/face/','aklghkah[怒骂]内斯塔[抓狂]'));
	
 var ATxt = ["[挤眼]","[亲亲]","[怒骂]","[太开心]","[懒得理你]","[打哈气]","[生病]","[书呆子]","[失望]","[可怜]","[挖鼻屎]","[黑线]","[花心]","[可爱]","[吐]","[委屈]","[思考]","[哈哈]","[嘘]","[右哼哼]","[左哼哼]","[疑问]","[阴险]","[做鬼脸]","[爱你]","[馋嘴]","[顶]","[钱]","[嘻嘻]","[汗]","[呵呵]","[睡觉]","[困]","[害羞]","[悲伤]","[鄙视]","[抱抱]","[拜拜]","[怒]","[吃惊]","[闭嘴]","[泪]","[偷笑]","[哼]","[晕]","[衰]","[抓狂]","[愤怒]","[感冒]","[鼓掌]","[酷]","[来]","[good]","[haha]","[不要]","[ok]","[拳头]","[弱]","[握手]","[赞]","[耶]","[最差]","[右抱抱]","[左抱抱]"] ;

 var aImg = ["zy_thumb.gif","qq_thumb.gif","nm_thumb.gif","mb_thumb.gif","ldln_thumb.gif","k_thumb.gif","sb_thumb.gif","sdz_thumb.gif","sw_thumb.gif","kl_thumb.gif","kbs_thumb.gif","h_thumb.gif","hs_thumb.gif","tz_thumb.gif","t_thumb.gif","wq_thumb.gif","sk_thumb.gif","laugh.gif","x_thumb.gif","yhh_thumb.gif","zhh_thumb.gif","yw_thumb.gif","yx_thumb.gif","zgl_thumb.gif","love.gif","cz_thumb.gif","d_thumb.gif","money_thumb.gif","tooth.gif","sweat.gif","smile.gif","sleep_thumb.gif","sleepy.gif","shame_thumb.gif","bs_thumb.gif","bs2_thumb.gif","bb_thumb.gif","88_thumb.gif","angry.gif","cj_thumb.gif","bz_thumb.gif","sad.gif","hei_thumb.gif","hate.gif","dizzy.gif","cry.gif","crazy.gif","fn_thumb.gif","gm_thumb.gif","gz_thumb.gif","cool_thumb.gif","come_thumb.gif","good_thumb.gif","ha_thumb.gif","no_thumb.gif","ok_thumb.gif","o_thumb.gif","sad_thumb.gif","ws_thumb.gif","z2_thumb.gif","ye_thumb.gif","bad_thumb.gif","right_thumb.gif","left_thumb.gif"] ;
var aImgEnded = [];

 $.each(aImg,function(i,n){ 
	aImgEnded.push('<img src="'+url+n+'"/>') ;
 })
 
	return   str_replace(ATxt,aImgEnded,str);	
}

function str_replace (search, replace, subject, count) { 
    var i = 0,
        j = 0,
        temp = '',
        repl = '',
        sl = 0,
        fl = 0,
        f = [].concat(search),
        r = [].concat(replace),
        s = subject,
        ra = Object.prototype.toString.call(r) === '[object Array]',
        sa = Object.prototype.toString.call(s) === '[object Array]';
    s = [].concat(s);
    if (count) {
        this.window[count] = 0;
    }

    for (i = 0, sl = s.length; i < sl; i++) {
        if (s[i] === '') {
            continue;
        }
        for (j = 0, fl = f.length; j < fl; j++) {
            temp = s[i] + '';
            repl = ra ? (r[j] !== undefined ? r[j] : '') : r[0];
            s[i] = (temp).split(f[j]).join(repl);
            if (count && s[i] !== temp) {
                this.window[count] += (temp.length - s[i].length) / f[j].length;
            }
        }
    }
    return sa ? s : s[0];
}



function AddOnPos(myField, myValue)   {  		
		//IE support   
		if (document.selection)   
		{  
		myField.focus();  
		sel = document.selection.createRange(); 
		myValue = "["+myValue+"]";   
		sel.text = myValue;			
		sel.select();   
		 }   
		//MOZILLA/NETSCAPE support   
		else if (myField.selectionStart || myField.selectionStart == '0')   
		{   
		 var startPos = myField.selectionStart;   
		 var endPos = myField.selectionEnd;   
		// save scrollTop before insert   
		var restoreTop = myField.scrollTop;   
		 myValue = "["+myValue+"]";   
		 myField.value = myField.value.substring(0, startPos) + myValue + myField.value.substring(endPos,myField.value.length);   
		 if (restoreTop > 0)   
		 {   
		 // restore previous scrollTop   
		myField.scrollTop = restoreTop;   
		 }   
		 myField.focus();   
		 myField.selectionStart = startPos + myValue.length;   
		 myField.selectionEnd = startPos + myValue.length;   
		} else {   
		myField.value += myValue;   
		 myField.focus();   
		 }   		
	 } 
//ie焦点纠正；




function movePoint(obj,pn){	
	 var rng = obj .createTextRange();		
	 rng.moveStart("character",pn); 
	 rng.collapse(true);
	 rng.select();
	
	// returnCase(rng)      
}	

 function getPoint(textarea) {
		var rangeData = {text: "", start: 0, end: 0 };	
			textarea.focus();
			var i,
				oS = document.selection.createRange(),
				// Don't: oR = textarea.createTextRange()
				oR = document.body.createTextRange();
			oR.moveToElementText(textarea);
			
			rangeData.text = oS.text;
			rangeData.bookmark = oS.getBookmark();
			
			// object.moveStart(sUnit [, iCount]) 
			// Return Value: Integer that returns the number of units moved.
			for (i = 0; oR.compareEndPoints('StartToStart', oS) < 0 && oS.moveStart("character", -1) !== 0; i ++) {
				// Why? You can alert(textarea.value.length)
				if (textarea.value.charAt(i) == '\r' ) {
					i ++;
				}
			}
			rangeData.start = i;
			rangeData.end = rangeData.text.length + rangeData.start;		
		
		return rangeData;
	}


var LayerBox = function(){	

	this.options;
	this.obj ;
	this.layerbox;
	this.box = $('<div></div>');
	
}
LayerBox.prototype = {
	init:function(event,url,o){	
		var o = o || {};
		var _this = this;			
		this.setOptions(o);		
		this.clearLayerBox();
		
		event.stopPropagation();
		
		if(this.options.autoHide){
			$(document).one('click',function(e){
				if(e.which == 1){
					$('.layerBox').hide();
				}
			})
		}	
		
		if(!$('.layerBox').size()){			
			this.insertLayerBox();
		}	
		
		this.toLocate(event);		
		$('.layerBox').show();
		
		$.get(url,_this.options.data,function(data){ //this.box
				
				var oHtml = $(data);
				_this.clearLayerBox();
				_this.layerbox.html(oHtml);
				_this.options.loaded();	
			
			
			});			
		
		$('.layerBox').click(function(e){		
			e.stopPropagation();
		})
		
	},
	toLocate:function(event,x,y){  //layerBox position set;
		var oT = $(event.target);
		var x = oT.offset().left+5;
		var y = oT.offset().top;
		var offsetY = oT.height()+5;		
		$('.layerBox').css({top:(y+offsetY)+'px',left:x });			
	},
	addCloseButton:function(){
		var closeBtn  =$('<a class="close" href="javascript:void(0)">关闭</a>')
		$('.layerBox').append(closeBtn);
		$('.layerBox a.close').bind('click',function(){			
			$('.layerBox').hide();
		})	
	},
	insertLayerBox:function(){			
		
		this.layerbox = $('<div class="layerBox"><div class="loading"><img  src="images/loading.gif"/></div></div>');		
				
		$('body').append(this.layerbox );	
	},
	clearLayerBox:function(){
		
		$('.layerBox').empty().append('<div class="loading"><img  src="images/loading.gif"/></div>')
		
	},
	setOptions:function(o){
		this.options = {
			autoHide:false,
			data:{},
			loaded:function(){}
		}
		this.extend	(this.options,o || {})
	},
	 extend :function(destination, source) {
		for (var property in source) {
			destination[property] = source[property];
		}
	}
	
}

$(function(){
		
		var oLayerbox = new  LayerBox()		
		
		var  oTextArea  = $('#content')[0];		
		var pos = 1000;
		if(isIE){
			if(oTextArea){
				oTextArea.onclick = function(){
					if($(oTextArea).val().length){
						pos = getPoint(oTextArea).start;	
					}							
				}
			}
		}

		$('.face').bind('click',function(event){//LayerBox实例化对象 init()参数1:事件对象； 参数2：显示内容（html.html内jq选择器）; [参数3]: {date:Object,loaded:Function}; (data:load方法传入参数；loaded:载入成功后回调函数；)；
	
				
		oLayerbox.init(event,'txt/face2.txt',{
		autoHide:true,
		loaded:function(){			
				$('.face img').click(function(){					
					if(isIE){movePoint(oTextArea,pos);}
				
					AddOnPos(oTextArea,$(this).attr('alt'));						
					$('.layerBox').hide();  						
					return false;
				});	
			}				
		});		
	}); 	


})