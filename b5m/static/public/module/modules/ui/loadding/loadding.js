/**
 * @id loadding
 * @description tab切换，带ajax功能
 * @namespace jQuery.fn
 * @return null
 * @void $('.tabs').tab();
 * @param {String} 时间 如'2013/9/13 18:48'
 * @param {jQuery expr} jq dom选择器表达式
 * @param {fn} 倒计时结束后回调 可选 *
 */


$.fn.loading: function(arg,imgUrl){
        if (this.size() > 1)
            return;
        var _this = this;
        var oImg = $('<img class="loading" src="'+imgUrl+'">');
        $('body').append(oImg);
        if (arg instanceof Array) {
            var addX = arg ? arg[0] : 0;
            var addY = arg ? arg[1] : 0;
            oImg.css({
                position: 'absolute',
                left: this.offset().left + addX + 'px',
                top: this.offset().top + addY + 'px',
                'zIndex': 999
            });
        } else {
            oImg.css({
                position: 'absolute',
                left: this.offset().left + this.outerWidth() / 2 + 'px',
                top: this.offset().top + this.outerHeight() / 2 + 'px',
                margin: -oImg.outerHeight() / 2 + 'px 0 0 ' + -oImg.outerWidth() / 2 + 'px',
                'zIndex': 999
            });
        }
        return oImg;
  }
