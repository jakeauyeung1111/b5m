/**
 * @id formText
 * @description tab切换，带ajax功能
 * @namespace jQuery.fn
 * @return null
 * @void $('.tabs').tab();
 * @param {String} 时间 如'2013/9/13 18:48'
 * @param {jQuery expr} jq dom选择器表达式
 * @param {fn} 倒计时结束后回调 可选 *
 */

    formText: function(){

        return this.each(function(){

            $(this).wrap('<span class="formText_con"></span>').before('<label>' + $(this).attr('txt') + '</label>');
            var con = $(this).parent('.formText_con');
            var label = con.find('label');
            var o = con.find('input,textarea');
            var lh;
            if ($(this).is('input')) {
                lh = o.outerHeight()
            } else if ($(this).is('textarea')) {
                lh = 30;
            }

            setStyle(this);


            if (o.val()) {
                label.hide();
            }

            label.bind('click', function(){
                o.focus();
            });

            o.bind('keyup', function(){
                if (o.val().length) {
                    label.hide();
                } else {
                    label.show();
                    //label.css({color:''});
                }
            });

            o.bind('focus', function(){
                label.css({
                    color: '#ccc'
                });
            });

            o.bind('blur', function(){
                if (o.val().length == 0) {
                    label.show();
                    label.css({
                        color: ''
                    });
                }

            });

            function setStyle(){
                o.css('float', 'left');

                o.parent('.formText_con').css({
                    position: 'relative',
                    display: 'inline-block',
                    'verticalAlign': 'middle',
                    width: o.outerWidth() + 'px',
                    height: o.outerHeight() + 'px'
                    // overflow: 'hidden'
                }).end().prev('label').css({
                        position: 'absolute',
                        height: o.outerHeight() + 'px',
                        'lineHeight': lh + 'px',
                        left: '8px',
                        top: 0
                    });
            }

        });
    }