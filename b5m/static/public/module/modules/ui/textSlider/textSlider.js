/**
 * @id textSlider
 * @description tab切换，带ajax功能
 * @namespace jQuery.fn
 * @return null
 * @void $('.tabs').tab();
 * @param {String} 时间 如'2013/9/13 18:48'
 * @param {jQuery expr} jq dom选择器表达式
 * @param {fn} 倒计时结束后回调 可选 *
 */

if($('ul',$obj).height()>($obj.height())){
		var scrtime;
		$obj.hover(function(){
			clearInterval(scrtime);			
		},function(){
		
		scrtime = setInterval(function(){
			var $ul = $("ul",$obj);
			var liHeight = $ul.find("li:last").height();		
			$ul.find("li:last").prependTo($ul);	
			$ul.css({marginTop:-(liHeight)+'px'});			
			$ul.find("li:first").css({opacity:'0'});
		
			
			$ul.animate({'marginTop' : 0 },'slow',function(){	
									
				$ul.find("li:first").animate({opacity:'1'},'slow');
			//$ul.css({marginTop:0});
			});	
		},3000);
		
		}).trigger("mouseleave");
	}