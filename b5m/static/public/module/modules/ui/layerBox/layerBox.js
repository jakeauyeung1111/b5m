/**
 * @id layerBox
 * @description tab切换，带ajax功能
 * @namespace jQuery.fn
 * @return null
 * @void $('.tabs').tab();
 * @param {String} 时间 如'2013/9/13 18:48'
 * @param {jQuery expr} jq dom选择器表达式
 * @param {fn} 倒计时结束后回调 可选 *
 */


function LayerBox(){
    this.options;
    this.obj;
    this.layerbox = $();
    this.defaultConfig = {};
    this.bg;
    this.closebtn;
    this.con;
    this.isIE6 = window.XMLHttpRequest ? false : true;
}

LayerBox.prototype = {
    options: {
        autoHide: false,
        width: 300,
        x: 'auto',
        y: 'auto',
        hasBg: false,
        bgOpacity: 0,
        delay: '0',
        id: '',
        className: '',
        data: {},
        parent: '',
        loaded: function(){
        },
        closed: function(){
        }
    },
    open: function(html, arg, o){
        var o = o || {};
        var _this = this;
        $.extend(this.defaultConfig, this.options)


        //alert(this.defaultConfig )
        $.extend(this.options, o);
        //this.clearLayerBox();
        //this.addCloseButton();

        if (this.options.autoHide) {
            $(document).one('click.lb', function(e){
                if (e.which == 1) {
                    _this.layerBox.hide();
                }
            })
        }

        this.insertLayerBox();

        this.layerbox.attr('id', this.options.id);
        this.layerbox.addClass(this.options.className)
        this.con.html(html)

        this.toLocate(arg);


        this.layerbox.css({
            width: this.options.width + 'px'
        })

        this.layerbox.show();

        if (this.options.hasBg) {
            this.showBg();
            this.layerbox.data('only', true)
        };

        this.layerbox.find(' i.i_close').bind('click.lb', function(){

            _this.close(true);
        })
        this.layerbox.bind('click.lb', function(e){

            $(this).css('zIndex', parseInt($(this).css('zIndex')) + 1);
            e.stopPropagation();
        })

        $(this.layerBox).click(function(e){
            e.stopPropagation();
        })
        $.extend(this.options, this.defaultConfig)

        //return this;
    },

    insertLayerBox: function(n){

        if (!this.layerbox.size()) {
            this.layerbox = $('<div class="layerBox"><i class="i_close" title="关闭"></i><div class="txt"></div></div>');
            this.closebtn = this.layerbox.find('.i_close');
            this.con = this.layerbox.find('.txt');


            $('body').append(this.layerbox);
           

        }
    },
    showBg: function(){
        var _this = this;
        this.bg = $('<div class="layer_bg"></div>');

        $('body').append(this.bg)
        this.bg.css({
            background: '#000',
            width: Math.max(document.documentElement.scrollWidth, document.documentElement.clientWidth) + 'px',
            height: Math.max(document.documentElement.scrollHeight, document.documentElement.clientHeight) + 'px',
            position: 'absolute',
            'z-index': '1001',
            left: '0',
            top: '0',
            opacity: this.options.bgOpacity,
            display: 'none'
        });
        $(window).bind('resize.lb', function(){
            _this.bg.css({
                width: Math.max(document.documentElement.scrollWidth, document.documentElement.clientWidth) + 'px',
                height: Math.max(document.documentElement.scrollHeight, document.documentElement.clientHeight) + 'px'
            });
        });

        this.bg.show();
        this.bindESCToClose();

    },

    toLocate: function(){ //layerBox position set;

        if (typeof arguments[0] == 'object' && !$.isArray(arguments[0])) {
            var iEvent = arguments[0];
            this.layerbox.css({
                left: (iEvent.pageX + 5),
                top: iEvent.pageY
            });
        } //center
        else if (arguments[0] == 'center') {
            this.layerbox.css({
                'zIndex': '1002',
                'marginLeft': -this.options.width / 2 + 'px',
                'marginTop': -(this.layerbox.height() / 2 + 100) + 'px',
                left: '50%',
                top: '50%',
                position: this.isIE6 ? 'absolute' : 'fixed'
            });
        } //array
        else if ($.isArray(arguments[0])) {
            var osL = 0, osT = 0;
            if (this.options.parent && $(this.options.parent).size() == 1) {

                osL = $(this.options.parent).offset().left;
                osT = $(this.options.parent).offset().top;

            }
            this.layerbox.css({
                left: osL + arguments[0][0] + 'px',
                top: (arguments[0][1] == 'auto') ? 'auto' : osT + arguments[0][1] + 'px',
                bottom: arguments[0][2] ? arguments[0][2] + 'px' : 'auto'
            });
        } else {
            // throw new Error('参数错误')
        }
    },

    close: function(mk){
        this.layerbox.hide();

        if (this.bg) {
            this.bg.hide();
        };
        if (mk)
            this.remove();
        $(window).unbind('.lb')
        this.options.closed();
        $(this).trigger('layerclose.layerbox');
    },
    remove: function(){
        if (this.bg)
            this.bg.remove();
        this.layerbox.remove();
    },
    show: function(){
        //console.log(this.layerBox)
        if (this.bg) {
            this.bg.show();
        };
    },

    bindESCToClose: function(){
        var _this = this;
        $(document).bind('keyup', function(event){
            if (event.keyCode == 27) {
                _this.close(true);
            }
        })
    },

    clearLayerBox: function(){
        this.layerbox.empty();
    },
    confirm: function(html, event, sure, cancel){
        var _this = this;

        this.open(html, event, {
            className: 'confirm'
        });
        this.con.after('<div class="sbm"><a class="btn13 sure" href="javascript:void(0)"><span>确定</span></a><a class="btn13 cancel" href="javascript:void(0)"><span>取消</span></a></div>');
        this.layerbox.find('.sure').bind('click', function(){
            if (sure)
                sure();
            _this.close(true);
        })
        this.layerbox.find('.cancel').bind('click', function(){
            if (cancel)
                cancel();
            _this.close(true);
        })
        return this.layerbox;
    },
    alert: function(html, o, sure){
        o = o ? o : {};
        var _this = this;

        this.open(html, 'center', o);
        this.con.after('<div class="sbm"><a class="btn13 sure" href="javascript:void(0)"><span>确定</span></a></div>');
        this.layerbox.addClass('myAlert')
        this.layerbox.find('.sure').bind('click', function(){
            if (sure)
                sure(true);
            _this.close();
        })
        return this.layerbox;
    },
    tips: function(txt, aA, parent, o){
        o = o ? o : {};

        aA = aA ? aA : [0, 0]
        if (!o.className) {
            o.className = '';
        }
        o.className += ' tips noshadow';
        o.width = 200;
        o.parent = parent;

        this.open(txt, aA, o);

        return this.layerbox;
    }
}