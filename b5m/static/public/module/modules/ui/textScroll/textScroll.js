/**
 * @id textScroll
 * @description tab切换，带ajax功能
 * @namespace jQuery.fn
 * @return null
 * @void $('.tabs').tab();
 * @param {String} 时间 如'2013/9/13 18:48'
 * @param {jQuery expr} jq dom选择器表达式
 * @param {fn} 倒计时结束后回调 可选 *
 */


	$.fn.textScroll = function(v){		
		this.each(function(){
			var _this = this;				
			stop.call(this);
			if(v == 'stop'){
				$(this).scrollLeft(0);	
				$(this).unbind();
				return this;
			}
			if(v =='pause'){
				//$(this).scrollLeft(0);	
				$(this).unbind();
				return this;
			}
			
			this.speed= typeof v =='number'? v : 1;	
			
			
			if(!$(this).find('.mk_con').size())	{			
				$(this).wrapInner('<div class="mk_con"><span class="mk_con_txt"></span></div>');
				this.con =$(this).find('.mk_con');
				this.contxt = this.con.find('.mk_con_txt')
			
			}
			this.con.find('.mk_con_txt').eq(0).clone().appendTo(this.con);			
			
			if($(this).innerWidth()>this.con.innerWidth()) return;
			
			setStyle.call(this);				
			start.call(this);				
			$(this).hover(function(){				
				stop.call(this);
			},function(){
				start.call(this);
			})			
		});				
		function start(){	
			var _this = this;
			stop.call(this);
			this.time =setInterval(function(){scroll.call(_this)},50);				
		};
		function stop(){					
			clearInterval(this.time);
			this.time = null;
		};
		function scroll(){				
			if(Math.abs($(this).scrollLeft() - this.con.innerWidth()/2)>this.speed){				
				$(this).get(0).scrollLeft += this.speed;
			}else{
				$(this).scrollLeft(0);	
			}
		};	
			
		function setStyle(){					
			$(this).css({
				'whiteSpace':'nowrap',
				cursor:'default'
			})
			this.con.css({
				float:'left'					
			});
			this.con.find('.mk_con_txt').css({
				margin:0
			})
		}			
		return this;	
	}