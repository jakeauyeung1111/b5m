var fs = require('fs');

exports.index = function(req, res){
    var obj = ExportModule.prototype.globalStr;
    new ExportModule(function() {
        res.send(obj.jslast);
    });
};

exports.export = function() {
  return new ExportModule();
}


var ExportModule = function(callback) {
    this.callback = callback;
    this.files.openFn.call(this);
};

//存储操作字符串
ExportModule.prototype.globalStr = {
    sort: ['head','util','ui','foot'],  //输出段落排序
    jslast:'',  //最后输出js string
    csslast:'',  //最后输出css string
    path:'./modules/'  //路径
};

//文件操作
ExportModule.prototype.files = {

    //打开fn/fn.js，初始化
    openFn:function() {

        var _this = this;

        fs.open(_this.globalStr.path +'fn/fn.js','r',0666,function(err,fd){

            if(err) throw err;

            //读取fn.js状态，获得fn.js属性和字符串长度
            fs.fstat(fd,function(err,stats) {

                if(err) throw err;

                var len = stats.size;  //获取长度
                var buffer = new Buffer(len);  //缓存整个fn.js字符串长度

                //读取fn.js内容，并截取字符串，整合文件用
                fs.read(fd,buffer,0,buffer.length,0,function(err,written,str) {

                    if(err) throw err;

                    str = str.toString();

                    var sp = str.search(/\/\*codes\*\//)+9;

                    _this.globalStr['head'] = str.substring(0,sp);
                    _this.globalStr['foot'] = str.substring(sp);

                    fs.closeSync(fd);

                    //合并fn/util.js内容
                    _this.files.mergeUtil.call(_this);

                });

            });

        });

    },

    //合并fn/util.js内容
    mergeUtil:function() {
        var _this = this;
        fs.readFile(_this.globalStr.path+'fn/util.js',function(err,data) {
            if(err) throw err;
            _this.globalStr['util'] = '\r\n\r\n' + data.toString();

            //合并ui/*内容
            _this.files.mergeUi.call(_this);
        });
    },


    //合并ui/*.js *.css内容
    mergeUi:function() {

        var _this = this;
        _this.globalStr['ui'] = '';

        fs.readdir(_this.globalStr.path+ 'ui',function(err,files) {
            if(err) throw err;
            appendUiFiles(files);
            _this.exportModuleCss.call(_this,files.slice());
        });

        function appendUiFiles(files) {
            if(files.length===0) {
                _this.exportModuleJavascript();
                return;
            }
            fs.readFile(_this.globalStr.path+'ui/'+files[0]+'/'+files[0] +'.js',function(err,data) {
                if(err) throw err;
                _this.globalStr['ui'] += '\r\n\r\n' +  data;
                files.shift();
                return appendUiFiles(files);
            });
        }

    }

};

//输出生成后的Javascript文档
ExportModule.prototype.exportModuleJavascript = function() {

    if(this.globalStr['sort'].length===0) {

        if(typeof this.callback !=='undefined') {
          this.callback();
        }

        fs.writeFile('./export/module.js',this.globalStr['jslast'],'utf8',function(err,data) {
            console.log('exprot javascript file complete!');
        });
        return;
    }

    for(var i in this.globalStr) {
        if (i === this.globalStr['sort'][0]) {
            this.globalStr['jslast'] += this.globalStr[i];
            this.globalStr['sort'].shift();
            return this.exportModuleJavascript();
        }
    }

};

//输出生成后的Css文档
ExportModule.prototype.exportModuleCss = function(files) {

    var _this = this;

    appendUiFiles(files);

    function appendUiFiles(files) {

        if(files.length===0) {
            fs.writeFile('./export/module.css',_this.globalStr['csslast'],'utf8',function(err,data) {
                console.log('exprot css file complete!');
            });
            return;
        }

        fs.readFile(_this.globalStr.path+'ui/'+files[0]+'/'+files[0] +'.css',function(err,data) {
            if(err) throw err;
            _this.globalStr['csslast'] += '\r\n\r\n' +  data;
            files.shift();
            return appendUiFiles(files);
        });

    }

};

