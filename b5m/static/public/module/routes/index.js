exports.index = function(req, res){
    var utils = require('../node/util');
    utils.createUtilsDoc(function(data) {
        res.render('index',data);
    });
};


exports.ui = function(req,res) {
  var ui = require('../node/ui');
  ui.createUiDoc(function(data) {
      res.render('ui',data);
  },req);
};
