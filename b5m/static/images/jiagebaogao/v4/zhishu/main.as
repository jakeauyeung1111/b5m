﻿package  {
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.display.Sprite;
	
	import com.greensock.*;
	import com.greensock.plugins.*;
	import com.greensock.easing.*;
	
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	
	public class main extends MovieClip {
		
		private var arr:Array = [0,0,.5];  //值，角度，透明度
		
		private var pointerValue:Number = 79;  //初始值
		
		private var textNum:TextField;
		private var colorMask:Sprite;
		private var spriteColor:Sprite;
		private var spritePointer:Sprite;
		 
		private var colorInit:Number = 0xf38f01;
		private var colorEnd:Number = 0xf38f01;
		private var radiusInit:Number = 75;
		
		var format:TextFormat = new TextFormat();
		
		public function main() {
			
			
			pointerValue = loaderInfo.parameters['value'] != null ? loaderInfo.parameters['value'] : pointerValue;
			
			if(loaderInfo.parameters['pageindex'] != null) {
				bg1.alpha = 0;
				bg2.alpha = 1;
			}
			
			spritePointer = _pointer;
			spriteColor = _color;
			
			colorEnd = getColorValue();
			
			colorMask = createColorMask();
			colorMask.mask = spriteColor;
			
			textNum = createTextNum();
			
			TweenPlugin.activate([EndArrayPlugin]);
			TweenPlugin.activate([TintPlugin]);
			
			TweenLite.to(spritePointer,3, {rotation: pointerValue/100*360 ,ease:Expo.easeInOut});
			TweenLite.to(colorMask,3, {tint:colorEnd,ease:Expo.easeInOut});

			TweenLite.to(arr,3, {endArray:[pointerValue,pointerValue/100*360,1],onUpdate:updateHandler,ease:Expo.easeInOut});
			TweenLite.to(textNum,3, {tint:colorEnd,ease:Expo.easeInOut});

			addEventListener(Event.ENTER_FRAME,enterFrameHandler);
			
		}
		
		private function createTextNum():TextField {
			var _text:TextField = new TextField();
			
            format.size = 60;
			format.align = 'center';
			format.font= 'Arial';
			format.color = colorInit;
			
			_text.width = 100;
			_text.height = 90;
			_text.x = 65;
			_text.y = 80;
			_text.mouseEnabled = false;
			_text.defaultTextFormat = format;
			
			if(pointerValue == 100) {
			  _text.y += 12;
			  format.size = 40;
			  _text.defaultTextFormat = format;
			}
			addChildAt(_text,5);
			return _text;
			
		}
		
		
		private function getColorValue():Number {
			if(pointerValue > 50 && pointerValue < 80) {
			  colorEnd = 0xd2df00;
			}else if(pointerValue >=80) {
			  colorEnd = 0x00d5a9;
			}
			return colorEnd;
		}
		
		private function drawSector(angle:Number,_alpha:Number):void {  
		
			var startFrom:Number = - 90;
  
  	        colorMask.graphics.clear();
   		    colorMask.graphics.beginFill(colorInit,_alpha);  
  		    colorMask.graphics.lineStyle(0,colorInit);   //使用传递进来的颜色  
     	    colorMask.graphics.moveTo(x,y);  
	 
    		angle=(Math.abs(angle)>360)?360:angle;  
    	    var n:Number=Math.ceil(Math.abs(angle)/45);  
   	        var angleA:Number=angle/n;  
    		angleA=angleA*Math.PI/180;  
     	    startFrom=startFrom*Math.PI/180;  
     		colorMask.graphics.lineTo(x+ radiusInit *Math.cos(startFrom),y+ radiusInit *Math.sin(startFrom));  
			
     		for (var i=1; i<=n; i++) {  
        		 startFrom+=angleA;  
       			 var angleMid=startFrom-angleA/2;  
        		 var bx=x+radiusInit/Math.cos(angleA/2)*Math.cos(angleMid);  
        		 var by=y+radiusInit/Math.cos(angleA/2)*Math.sin(angleMid);  
         		 var cx=x+radiusInit*Math.cos(startFrom);  
         		 var cy=y+radiusInit*Math.sin(startFrom);  
         		 colorMask.graphics.curveTo(bx,by,cx,cy);  
     		}  
			
     		if (angle!=360) {  
         		 colorMask.graphics.lineTo(x,y);  
     		}  
			
     		colorMask.graphics.endFill();
 		}  
		
		//生成mask
		private function createColorMask():Sprite {
			var _mask:Sprite = new Sprite();
			addChildAt(_mask,2);
			_mask.graphics.beginFill(0xebebeb,0); 
			_mask.graphics.drawCircle(0, 0, 75); 
			_mask.graphics.endFill();
			_mask.x = 115;
			_mask.y = 115;
			_mask.width = 150;
			_mask.height = 150;
			return _mask;
		}
		
		private function updateHandler():void {
			  textNum.text = (int(arr[0])).toString();
			  drawSector(arr[1],arr[2]);
	    }
		
		private function enterFrameHandler(event:Event):void
		{	
		
		}
		
	}
	
	
}