var you_site = location.host.split('.')[0];

var autofill_type = window.Cookies.get('autofill_type');
if(!autofill_type){
    var val = Math.random().toFixed('1');
    if(val > 0.5){
        window.Cookies.set('autofill_type','CA',new Date("December 31, 2020"),'/','.b5m.com');
        autofill_type = 'CA';
    }else{
        window.Cookies.set('autofill_type','CB',new Date("December 31, 2020"),'/','.b5m.com');
        autofill_type = 'CB';
    }
}

if(you_site == 'you' || autofill_type == 'CA'){
    document.write('<link rel="stylesheet" href="http://staticcdn.b5m.com/static/css/common/autoFill_A.css?v=20133491551"><script type="text/javascript" src="http://staticcdn.b5m.com/static/scripts/common/autoFill_A.js?v=20133491551"></script>');
}else{
    document.write('<link rel="stylesheet" href="http://staticcdn.b5m.com/static/css/common/autoFill_B.css?v=20133491551"><script type="text/javascript" src="http://staticcdn.b5m.com/static/scripts/common/autoFill_B.js?v=20133491551"></script>');
}