var weatherFed = {
    /*初始*/
    init:function(){
        var navIndex = $('.wea-nav'),
            isie6 = typeof document.body.style.maxHeight ==='undefined',
            floorArr = [],
            floorIndex=$('.floor');
        floorIndex.each(function(i,n) {
            floorArr.push($(this).offset().top);
        });
        navIndex.data('top',navIndex.offset().top);
        navIndex.find('a').each(function(i,n) {
            $(n).click(function(){
                if(i<1){
                    $('html,body').animate({scrollTop:floorArr[i]-(isie6 ? 0 : 160)});
                }else{
                    $('html,body').animate({scrollTop:floorArr[i]-(isie6 ? 0 : 280)});
                }
            });
        });
        $(".cent-items li").eq(0).find("a").click(function(){
            $('html,body').animate({scrollTop:floorArr[0]-(isie6 ? 0 : 160)});
        });
        $(window).scroll(function() {
            var scroll = document.documentElement.scrollTop || document.body.scrollTop || 0;
            if(scroll>navIndex.data('top')) {
                navIndex.addClass('fix');
            }else {
                navIndex.removeClass('fix');
            }
        }).trigger('scroll');
    },
    /*go top*/
    goTop:function(){
        $(".go-top a").click(function(){
            $('html,body').animate({scrollTop: 0});
            return false;
        });
    },
    /**
     * 字符串转为JSON
     * @param jsonStr
     * @returns {*} JSON数据
     */
    jsonStr:function(jsonStr){
        if(window.JSON){
           return JSON.parse(jsonStr);
        }else{
            return (new Function("return ("+jsonStr+");"))();
        }
    },
    /**
     * 日期参数兼容所有浏览器
     * @param 日期字符串
     */
    dateHoursFun:function(time){
        if (!document.all) {
             return new Date(time).getHours();
        }else{
            var date = +time.match(/(\d{1,2})\:/)[1];
            return date;
        }
    },
    weatherShow:function(wData){

        var _this = this
            ,data = wData['data']
            ,wtData = data['weatherinfo']
            ,nWind = wtData['wind1']
            ,nowTimer = wData['time']
            ,nData = new Date(nowTimer)
            ,arrTemp = wtData['temp1'].split("~")
            ,hours = nData.getHours()
            ,nCodeCity =data['cityCode'];

        /**
         * 设置城市cookie
         */
        Cookies.set('codeCity',nCodeCity);
        /**
         * 获取现在的温度
         * @param lowTemperate最低温度
         * @param highTemperate最高温度
         * @returns 当前温度
         */
        var ntempDeal=function(lowTemperate,highTemperate){
            var ntempValue= 0,
                dValue=highTemperate-lowTemperate;

            if(hours <= 12){
                ntempValue=dValue/12*hours+lowTemperate;
            }else{
                ntempValue=dValue/12*(24-hours)+lowTemperate;
            };
            return parseInt(ntempValue)+"℃";
        };
        /**
         * 获取PM2.5
         * @param 数值
         * @returns 颜色和质量情况
         */
        var pmDeal = function(pmData){
            if(pmData){
                var pmColor="",
                    pmStr="",
                    wPmDate=pmData['pm2_5'];
                if(wPmDate<=35){
                    pmColor="#6bca1e";
                    pmStr="优";
                }else if(wPmDate>35 && wPmDate<=75){
                    pmColor="#6bca1e";
                    pmStr="良";
                }else if(wPmDate>75 && wPmDate<=115){
                    pmColor="#d6c617";
                    pmStr="轻度污染";
                }else if(wPmDate>115 && wPmDate<=150){
                    pmColor="#d6c617";
                    pmStr="中度污染";
                }else if(wPmDate>150 && wPmDate<=250){
                    pmColor="#c42626";
                    pmStr="重度污染";
                }else{
                    pmColor="#c42626";
                    pmStr="严重污染";
                };
                var pmwText='<div class="pm25 cf"><p class="kq">PM2.5指数:<b> '+data['pm25']['pm2_5']+'</b></p><p class="wr">'+'<s style="background:'+pmColor+'"></s>'+pmStr+'</p></div>';
                return pmwText;
            }else{
                $(".weather-date").find(".pm25").hide();
                return "";
            }
        };
        /**
         * 天气图片
         * @param imgStr
         * @returns 图片数据
         */
        var imgDeal=function(imgData){
            var imgNum=0;
            if(hours>6&&hours<=12){
                imgNum=imgData['imgAM'];
            }else{
                if(imgData['imgPM']==99){
                    imgNum=imgData['imgAM'];
                }else{
                    imgNum=imgData['imgPM'];
                }
            };
            var imgHtml='<img src="http://staticcdn.b5m.com/images/360weather/weatherpic/b'+imgNum+'.png">';
            return imgHtml;
        };
        /**
         * 城市
         * @param jsonStr
         * @returns 城市数据
         */
        var cityFun=function(cityData){
            var arrCity=cityData.split(" ");
            if(arrCity.length>1){
                var cityHtml='<p class="icon city"><b>'+arrCity[0]+'</b><span>'+arrCity[1]+'</span></p>';
            }else{
                var cityHtml='<p class="icon city"><b>'+arrCity[0]+'</b></p>';
            }
            return cityHtml;
        }
        var indexTemp = '<p class="wea-icon">'+imgDeal(wtData['img1'])+'</p><p class="temp">'+ntempDeal(parseInt(arrTemp[1]),parseInt(arrTemp[0]))+'</p><p class="weath">'+wtData['weather1']+'</p><p class="wind">';

        indexTemp += hours<=12?[nWind['windAM'],nWind['windSizeAM']].join(" "):[nWind['windPM'],nWind['windSizePM']].join(" ");

        indexTemp += '</p>'+pmDeal(data['pm25'])+'<p class="date"><span class="timer"> '+(wtData['date1'].replace("日","").replace(/[年月]+/gi,"-"))+'  '+wtData['week']+'</span><span class="lunar"> '+wtData['date_nongli']+'</span></p>'+cityFun(wtData['city']);
        $(".weather-date").html(indexTemp);

    },
    /**
     * 天气预报
     */
    weatherFun:function(){
        var _this = this,
            cityCode = null;
        if(Cookies.get('codeCity')){
            cityCode = Cookies.get('codeCity');
        };
        $.ajax({
            url:'http://weather.ruyisou.net/weather?jsonp=true',
            type:"GET",
            data:{cityCode:cityCode},
            dataType:"jsonp",
            success:function(data){
                _this.weatherShow(data);
            }
        });


    },
    loadFun:function(){
        this.init();
        this.goTop();
        this.weatherFun();
    }
};