;(function($,win,doc,undefined){
	/**
	 * [Autofill 构造函数]
	 * @param {[object]} elem [input框，绑定的dom元素]
	 * @param {[object]} options [自定义参数]
	 * site:当前站点
	 * url:搜索接口
	 * callback:回调函数
	 */
	function Autofill(elem, url, site, callback){
		this.elem = elem;
		this.$elem = $(elem);
		this.elem.autocomplete = 'off';
		this.url = url || 'http://search.b5m.com/allAutoFill.htm';
		this.site = site || 'b5mo';
		this.callback = callback;
		this.autofill = null;
		this.autofillContent = null;

		this.ipCity = '';

		this.tuanCity = '';
		this.piaoCity = '';
		this.hotelCity = '';
		this.travelCity = '';
		this.travelCityId;

		this.init();
	}
	Autofill.prototype = {

		init:function(){
			this.createAutofill();
			this.getCity();
		},

		/**
		 *（1）旅游、酒店、团购、票务四个频道需要确定某一城市，其他频道取城市为 “全国”
         *（2）用户未选择城市时，根据IP自动判断城市，获取IP的城市作为用户当前城市
         *（3）用户在旅游、酒店、团购、票务选择某城市后，需要记录该城市，在cross autofill跳转到相应的频道时，
         *	   根据用户之前选择的城市进行跳转（未选择城市的频道取IP城市）
		 */
		getCity:function(){
			var _this = this;

            //根据ip获取城市
            $.ajax({
                url:'http://int.dpool.sina.com.cn/iplookup/iplookup.php?format=js',
                async:true,
                dataType:"script",
                success: function() {
                    _this.ipCity = remote_ip_info.city;
                }
            });
		},
		/**
		 * [getCookieCity 获取cookie的城市值]
		 */
		getCookieCity:function(){
			var _this = this;

			//取得团的cookie
			if(!_this.tuanCity)
	            if(Cookies.get('tcity_sn')){
	                _this.tuanCity = decodeURIComponent(Cookies.get('tcity_sn'));
	            }

			//取得票的cookie
			if(!_this.piaoCity)
	            if(Cookies.get('city_sn')){
	                _this.piaoCity = decodeURIComponent(Cookies.get('city_sn'));
	            }

			//取得旅游的城市
            if(!_this.travelCity)
	            if(Cookies.get('travelCity')){
	                var travel = decodeURIComponent(Cookies.getEn('travelCity')).split('|');

	                _this.travelCity = travel[0];
	                _this.travelCityId = travel[travel.length - 1];
	            }
            //取得酒店的城市
            if(!_this.hotelCity)
	            if(Cookies.get('hotelCity')){
	            	var hotel = decodeURIComponent(Cookies.getEn('hotelCity')).split('|');

	                //注意这里的 逗号 为中文下的
	                _this.hotelCity = hotel[0].split('，')[0];
	                _this.hotelCityId = hotel[hotel.length - 1];
	            }
		},
		/**
		 * [createAutofill 创建autofill容器]
		 * @return {[type]} [description]
		 */
		createAutofill:function(){
				this.autofill = $('<div class="autofill">' 
								+ '<div class="search-history"><p class="search-history-title cf"><span class="r del-his J_del_His">删除历史</span><span class="l">最近搜过</span></p><ul class="key-list"></ul></div>' 
								+ '<div class="autofill-box"><div class="autofill-content">' 
								+ '<div class="autofill-slider cf"></div></div>' 
								+ '<div class="sub-box"></div>' 
								+ '<div class="autofill-arrow autofill-left autofill-left-disable"></div><div class="autofill-arrow autofill-right autofill-right-disable"></div><div class="autofill-close"></div><div class="autofill-tips"></div></div></div>')
							.appendTo(doc.body);
				this.autofillContent = this.autofill.find('.autofill-slider');
				this.searchHis = this.autofill.find('.key-list');
				this.close = this.autofill.find('.autofill-close');
				this.left = this.autofill.find('.autofill-left');
				this.right = this.autofill.find('.autofill-right');
				this.subBox = this.autofill.find('.sub-box');
				this.tips = this.autofill.find('.autofill-tips');
				//绑定事件
				this.bindEvent();
				//设置autofill的样式
				this.setStyle();
				//是否是第一次按下向下键。
				this.flag = true;
				//二位数组下标
				this.x = 0;
				this.y = -1;
				//历史搜索记录
				this.getSearchHistory();
		},
		/**
		 * [setStyle 设置autofill样式，位置、大小等]
		 */
		setStyle:function(){
			//取得input框的位置信息，设置autofill样式
			var style = {
					left:this.$elem.offset().left,
					top:this.$elem.offset().top + this.$elem.outerHeight()
				};
			this.autofill.css(style);
		},

		/**
		 * 搜索历史
		 */
		setSearchHistory:function(key,url,cate){
			var	searchKey = key,
				url = url,
				category = cate,
				hisMax = 10;

			var cookieKey = category + 'His',
				cookieVal = searchKey + '-' + url + '-' + category;

			var hisInfo = Cookies.get(cookieKey);
			if(hisInfo){
				var hisArr = hisInfo.split(';'),
					hisArrNum = hisArr.length,
					flag = true;

				for(var i = 0; i < hisArrNum; i++){
					var item = hisArr[i].split('-');

					if(item[0] == searchKey && item[1] == url){
						flag = false;
					}
				}
				if(flag){
					hisArr.unshift(cookieVal)
					Cookies.set(cookieKey,hisArr.join(';'),new Date(new Date().getTime()+  1000*60*60*24*30),'','.b5m.com');
				}
				if(hisArrNum >= hisMax){
					hisArr.pop();
					Cookies.set(cookieKey,hisArr.join(';'),new Date(new Date().getTime()+  1000*60*60*24*30),'','.b5m.com');
				}
			}else{
				Cookies.set(cookieKey,cookieVal,new Date(new Date().getTime()+  1000*60*60*24*30),'','.b5m.com');
			}
		},
		//删除历史记录
		delSearchHistory:function(){
			var cookieKey = this.site + 'His',
				autoFill = this.autofill;
				
            autoFill.hide().find('.search-history').hide().find('.key-list').empty();
			Cookies.set(cookieKey,'',new Date(),'','.b5m.com');
		},
		//获取搜索历史记录
		getSearchHistory:function(){
			var cookieKey = this.site + 'His',
				cookieVal = Cookies.get(cookieKey);

			if(!cookieVal){
				return false;
			}

			var hisArr = cookieVal.split(';'),
				hisArrNum = hisArr.length;

			var hisHtml = '';

			for(var i = 0; i < hisArrNum; i++){
				var item = hisArr[i].split('-');
				hisHtml += '<li><a data-attr="1001" target="_self" title=' + item[0] + ' href=' + item[1] + '>' + item[0] + '</a></li>';
			}

			this.searchHis.append(hisHtml);
		},
		/**
		 * [显示搜索历史]
		 */
		showSearchHis:function(){
           	this.autofill.find('.search-history').show();
           	this.autofill.find('.autofill-box').hide();
		},
		/**
		 * [隐藏搜索历史]
		 */
		hideSearchHis:function(){
           	this.autofill.find('.search-history').hide();
           	this.autofill.find('.autofill-box').show();
		},
		/**
		 * [bindEvent description]
		 * @return {[type]} [description]
		 */
		bindEvent:function(){
			var _this = this,
				autoFill = _this.autofill,
				timeFlag = null;

			_this.$elem.on('keyup',function(e){
				var keyVal = this.value,
					keyCode = e.which;

				if(keyCode == 37 || keyCode == 38 || keyCode == 39 || keyCode == 40){
					return false;
				}



				clearTimeout(timeFlag);

				timeFlag = setTimeout(function(){
					_this.getCookieCity();
					_this.getData(keyVal,autoFill);
				},200)

            	_this.hideSearchHis();

                //没有输入关键词,显示历史记录
                if(!keyVal && Cookies.get(_this.site + 'His')){
                	_this.showAutoFill();
                	_this.showSearchHis();
                }
			});
			_this.$elem.on('keydown',function(e){
				var keyVal = this.value,
					keyCode = e.which;

				if(!_this.flag && (keyCode == 37 || keyCode == 38 || keyCode == 39 || keyCode == 40)){
					_this.changeHighlight(keyCode,keyVal);
				}

				//第一次只接收向下键盘事件
				if(_this.flag && keyCode == 40){
					_this.changeHighlight(keyCode,keyVal);
					_this.flag = false;
				}

				if(keyCode == 13){
					var $target = _this.autofillContent.find('.cur').find('a'),
						target = $target[0];

					if($target.length <= 0){
						var url = _this.getUrl(_this.site,keyVal);
						_this.setSearchHistory(keyVal,url,_this.site)
					}else{
						var	searchKey = $target.text(),
							url = $target.attr('href');
							category = $target.parents('.category').attr('class').split(' ')[1];

						_this.setSearchHistory(searchKey,url,category);
					}

					try
					{
						var evObj = document.createEvent('MouseEvents');
						evObj.initEvent('click', true, false);
						target.dispatchEvent(evObj);
					}
					catch(e)
					{
						target.click();
					}
					_this.hideAutoFill();
					return false;
				}
			});

			_this.$elem.on('click.af',function(e){
				var keyVal = this.value,
                    $autoFill = _this.autofill,
                    $autoFillContent = _this.autofillContent;


                if(keyVal && $autoFill.is(':hidden') && ($autoFillContent.children().length > 0)){
                    _this.showAutoFill();
                }else if(keyVal && $autoFill.is(':hidden') && ($autoFillContent.children().length == 0)){
                    _this.getData(keyVal);
                }
                //显示历史记录
                if(!keyVal && Cookies.get(_this.site + 'His')){
                	_this.showAutoFill();
                	_this.showSearchHis();
                }
                e.stopPropagation();
			});

			autoFill.on('click','a',function(){
				var $this = $(this);

				var	searchKey = $this.text(),
					url = $this.attr('href'),
					category = $this.parents('.category').attr('class').split(' ')[1];
				
				_this.setSearchHistory(searchKey,url,category);
			});

			//清除历史记录
			autoFill.find('.J_del_His').on('click',function(){
				_this.delSearchHistory();
			});

			autoFill.on('click.af',function(e){
				e.stopPropagation();
			});

			autoFill.on('mouseenter','dl',function(){
				$(this).addClass('category-hover').siblings('dl').removeClass('category-hover');
			});

			autoFill.on('mouseleave','dl',function(){
				$(this).removeClass('category-hover');
			});

			$(doc).on('click.af',function(){
				_this.hideAutoFill();
			});

			_this.close.on('click.af',function(){
				_this.hideAutoFill();
			});

			//改变窗口后，重新定位autofill
			$(window).resize(function(){
				var flag = null;
				clearTimeout(flag);
				flag = setTimeout(function(){
					_this.setStyle();
				},400);
			});
		},
		/**
		 * [changeHighlight 判断键盘键值]
		 * @param  {[Number]} keycode [键值]
		 */
		changeHighlight:function(keycode,keyval){
			if(!keycode || !keyval) return;
			var left = 37,
				top = 38,
				right = 39,
				down = 40,
				direction = '';
			switch(keycode){
				case left:
					direction = 'left';
					this.x--;
					break;
				case top:
					this.y--;
					break;
				case right:
					direction = 'right';
					this.x++;
					break;
				case down:
					this.y++;
					break;
			}
			this.setHighlight(direction);	
		},
		/**
		 * [setHighlight description]
		 */
		setHighlight:function(direction){
			var $dls = this.autofillContent.find('dl'),
				$dds = this.autofillContent.find('dd'),
				items = [];
				
			if(!$dls.length){
				return false;
			}

			/**
			 * 把每条数据填充到二位数组里面
			 * 一条数据对应一个下标
			 */
			$dls.each(function(i){
				var $dls_dds = $(this).find('dd'),
					len = $dls_dds.length;
				items[i] = [];
				for(var j = 0; j < len; j++){
					items[i][j] = $dls_dds[j];
				}
			});

			dl_len = items.length;

			if(this.x >= dl_len){
				this.x = dl_len - 1;
			}else if(this.x < 0){
				this.x = 0;
			}

			dd_cur_len = items[this.x].length;

			if(this.y >= dd_cur_len){
				this.y = 0;
			}else if(this.y < 0){
				this.y = dd_cur_len - 1;
			}

			$dls.eq(this.x).addClass('category-hover').siblings().removeClass('category-hover');
			$dds.removeClass('cur');
			$(items[this.x][this.y]).addClass('cur');
			this.setInputVal($(items[this.x][this.y]));

			/**
			 * [键盘控制滚动]
			 */
			if(this.x !== 0 && this.x % 3 === 0 && direction == 'right'){
				this.right.trigger('click');
			}else if(this.x !== 0 && ((this.x + 1) % 3 === 0) && direction == 'left'){
				this.left.trigger('click');
			}
		},

		/**
		 * [setInputVal 设置input值]
		 * @param {[jQuery]} obj [当前对象]
		 */
		setInputVal:function(obj){
			if(!obj.length) return;
			this.elem.value = obj.find('a').text();
		},

		/**
		 * [bindSlider 滚动事件]
		 */
		bindSlider:function(len){
			var _this = this,
				_step = _this.autofillContent.find('.category').outerWidth()*3,
				count = Math.ceil(len / 3),
				i = 0;

			_this.right.on('click',function(){
				if(i == count - 1) {
	                return false;
	            }
				i++;
	            _this.autofillContent.stop(true,true).animate({left:-_step*i}, 400);

	            _this.left.removeClass('autofill-left-disable');
	            if(i==count-1) {
	                _this.right.addClass('autofill-right-disable');
	            }
			});

			_this.left.on('click',function(){
	            if(i>=count-1) {
	             	i=count-1;
	            }
	            if(i==0) {
	              	return false;
	            }

	 		    i--;

			    _this.autofillContent.stop(true,true).animate({left:-_step*i}, 400);
			    _this.right.removeClass('autofill-right-disable');
                if(i==0) {
                    _this.left.addClass('autofill-left-disable');
                }
			});
		},
		/**
		 * [hideAutoFill 隐藏autofill]
		 * 单独提出隐藏方法，方便以后扩展：比如隐藏之后执行callback
		 */
		hideAutoFill:function(){
			this.autofill.hide();
		},
		/**
		 * [showAutoFill 显示autofill]
		 * 单独提出显示方法，方便以后扩展：比如隐藏之后执行callback
		 */
		showAutoFill:function(){
			this.setStyle();
			this.autofill.show();
		},
		/**
		 * [handleKeyVal 屏蔽特殊字符]
		 * @return {[string]}
		 */
		handleKeyVal:function(value){
            var val = $.trim(value);

           	if(!val){
           		return
           	}

           	var pattern =  new RegExp("[`~!@#$^&*%()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]"),
                rs = '';

            //特殊字符转为空
            for (var i = 0,len = val.length; i < len; i++) { 
				rs = rs + val.substr(i, 1).replace(pattern, ''); 
			} 
      	    return rs;
		},
		/**
		 * [getData 获取数据]
		 */
		getData:function(value,autoFill){
			var _this = this,
				keyVal = _this.handleKeyVal(value);

			if(!keyVal){
				return;
			}

			var params = '',
				localCity = _this.ipCity;
				_this.travelCity = _this.travelCity || localCity;
				_this.piaoCity = _this.piaoCity || localCity;
				_this.tuanCity = _this.tuanCity || localCity;

				_this.hotelCity = _this.hotelCity || localCity;
				params = 'keyWord=' + encodeURIComponent(keyVal) + '&' + 'tourpcity=' + _this.travelCity + '&hotelcity=' + _this.hotelCity + '&ticketpcity=' + _this.piaoCity + '&tuanmcity=' + _this.tuanCity;

			$.ajax({
				url: this.url,
				async: false,
				dataType: 'jsonp',
				data: params,
				jsonp:'jsoncallback',
				success:function(data){

					//先判断返回的对象是否为空
					if(_this.isEmptyObject(data)){
						_this.hideAutoFill();
						_this.autofillContent.empty();
						return;
					}
					_this.fillAutoFill(data)
				}
			});
		},
		/**
		 * [isEmptyObject 判断对象是否有数据]
		 * @param  {[type]}  obj [传入的对象，此处对象结构必须是
		 * {
		 * 	a:[
		 * 		{count:0,hl_value:"阿依莲"},
		 * 	 	{count:0,hl_value:"按摩器"}
		 * 	  ],
		 * 	b:[
		 * 		{count:0,hl_value:"阿依莲"},
		 * 	 	{count:0,hl_value:"按摩器"}
		 * 	  ]
		 * }
		 * 一般方法判断对象是否为空的标识是对象里面有没有内容,如：var obj = {},则为空，var obj = {a:""}不为空。
		 * 考虑到autofill数据结构问题，优化性能。下面：
		 * var obj = {a:[],b:[]} 也为空。
		 * @return {Boolean}     [true为空，false反之]
		 */
		isEmptyObject:function(obj){
			for(var i in obj){
				if(obj[i].length){
					return false;
				}
			}
			return true;
		},
        /**
         * [reset 搜索结果重设]
         */
        reset:function() {
            this.autofillContent.css({'left':0});
            this.right.removeClass('autofill-right-disable');
            this.left.addClass('autofill-left-disable');
            this.left.off('click');
            this.right.off('click');
            this.x = 0;
            this.y = -1;
            this.flag = true;
        },
		/**
		 * [fillAutoFill 拼接，排序html]
		 */
		fillAutoFill:function(data){
            this.reset();

			var html = '',
				//存储分类返回的数据
				arrItem = [],
				arrTitle = [],
				arrLength = 0,
				_this = this;

			/**
			 * 站点排序规则：
			 * 1.当前站点有数据时，始终排在第一位。
			 * 2.若当前站点是海外商品，海外信息就排在第二个。
			 * 3.其余站点，按照返回的数据条目由多到少排序。
			 */
			var siteArr = ['b5mo','tejia','hotel','ticketp','tourguide','tourp','tuanm','zdm','she','haiwaip','haiwaiinfo','doctor','usa'];

			for(var i in data){

				//排除下列站点
				if (!data[i].length || i == "correction" || i == this.site || i == "guang" || i == "doctor" || i == "she") {
				    continue
				}

				if(i != 'b5mo' && i != 'tejia' && i != 'hotel' && i != 'ticketp' && i != 'tourguide' && i != 'tourp' && i != 'tuanm' && i != 'zdm' && i != 'she' && i != 'usa' && i != 'haiwaip' && i != 'haiwaiinfo' && i != 'korea' && i != 'koreainfo'){
					continue;
				}

				/**
				 * 对海外商品和海外咨询进行特殊处理
				 */
				if(this.site == 'haiwaip' && i == 'haiwaiinfo'){
					continue;
				}
                /**
                 * 对韩国商品和韩国咨询进行特殊处理
                 */
                if(this.site == 'korea' && i == 'koreainfo'){
                    continue;
                }

				arrItem.push({title:i,content:data[i]});
			}

			//按返回数据数目，从多到少排序
			arrItem.sort(function(a,b){
				return b.content.length - a.content.length;
			});

			//拼接一级数据
			for(var i in arrItem){
				var itemContent = '';
				itemContent = this.serializeData(arrItem[i].title,arrItem[i].content);
				html += itemContent;
			}
			arrLength = arrItem.length;

			/**
			 * 处理特殊排序：
			 * 1.海外商品和海外资讯。
			 */
			if(this.site == 'haiwaip' && data['haiwaiinfo'] && data['haiwaiinfo'].length){
				html = this.serializeData('haiwaiinfo',data['haiwaiinfo']) + html;
				arrLength = arrItem.length + 1;
			}
            /**
             * 处理特殊排序：
             * 1.韩国商品和韩国资讯。
             */
            if(this.site == 'korea' && data['koreainfo'] && data['koreainfo'].length){
                html = this.serializeData('koreainfo',data['koreainfo']) + html;
                arrLength = arrItem.length + 1;
            }

			if(data[this.site] && data[this.site].length){
				html = this.serializeData(this.site,data[this.site]) + html;
				arrLength = arrItem.length + 1;
			}
			//二级关键词遍历输出
			// var subHtml = '';
			// $.each(data[this.site],function(key,obj){
			// 	var subItem = '';
			// 	if(obj.subKeywords){
			// 		for(var i in obj.subKeywords){
			// 			subItem += '<a href="' + _this.getUrl(_this.site,obj.value + obj.subKeywords[i].value) +'">' + obj.subKeywords[i].value + '</a>';
			// 		}
			// 	}
			// 	subItem = '<div class="sub-item"><div class="tags clear-fix">' + subItem + '</div></div>';
			// 	subHtml += subItem;
			// });
			/**
			 * subBox：二级关键词的容器
			 * autofillContent：分类的容器
			 */
			// this.subBox.empty().append(subHtml);

			this.autofillContent.empty().append(html);
			this.showAutoFill();

			if(arrLength > 3){
				this.right.removeClass('autofill-right-disable');
				this.bindSlider(arrLength);
			}else{
				this.right.addClass('autofill-right-disable');
			}
			_this.right.on('click',function(){
				if($(this).hasClass('autofill-right-disable')){
	            	_this.tips.stop(true,true).fadeIn( 600 ).delay(1000 ).fadeOut( 800 );
	            }
			})
			// $('.b5mo').find('dd').mouseenter(function(event) {
   //              var index = $('.b5mo').find('dd').index($(this));
   //              $('.b5mo').find('dd.cur').removeClass('cur');
   //              _this.subBox.hide().find('.sub-item.on').removeClass('on');
   //              if($(this).hasClass('item-sub')) {
   //                  _this.subBox.show().find('.sub-item').eq(index).addClass('on');
   //                  $(this).addClass('cur');
   //              }
			// });
			// _this.subBox.mouseleave(function(event) {
   //              $('.b5mo').find('.cur').removeClass('cur');
   //              $(this).hide();
			// });
		},

		/**
		 * [serializeData 格式化数据]
		 * @param  {[object]} data [类目对象]
		 * @return {[string]} content [每一条类目对应的html]
		 */
		serializeData:function(site,data){
			var content = '',
				hlValue = '',
				keyVal = this.handleKeyVal(this.elem.value),
				_this = this;

			$.each(data,function(key,obj){

				//高亮代码
				hlValue = obj.value.replace(keyVal,'<b>' + keyVal + '</b>');
				// if(obj.subKeywords && _this.site == 'b5mo' && site == 'b5mo'){
				// 	content += '<dd class="item item-sub" data-attr="1001"><a class="item-txt" href="' + _this.getUrl(site,obj.value) + '" target="_blank" title="'+ obj.value +'">' + hlValue + '</a><span class="item-count">' + obj.count + '</span></dd>';
				// }else{
					content += '<dd class="item"><a data-attr="1001" target="_self" class="item-txt" href="' + _this.getUrl(site,obj.value) + '" title="'+ obj.value +'">' + hlValue + '</a><span class="item-count">' + obj.count + '</span></dd>';
				// }
			});
			content = '<dl class="category ' + site + '"><dt>'+ _this.getItemTitle(site) +'</dt>' + content + '</dl>';
			return content;
		},
		/**
		 * [getItemTitle 获得类目的中文]
		 * @param  {[type]} title [类目title的拼音]
		 */
		getItemTitle:function(title){
            switch (title) {
                case 'b5mo':
                    return '商品';
                    break;
                case 'tejia':
                    return '特价';
                    break;
                case 'hotel':
                    return '酒店';
                    break;
                case 'ticketp':
                    return '票务';
                    break;
                case 'tourguide':
                    return '攻略';
                    break;
                case 'tourp':
                    return '旅游';
                    break;
                case 'tuanm':
                    return '团购';
                    break;
                case 'zdm':
                    return '值得买';
                    break;
                case 'she':
                    return '讨论区';
                    break;
                case 'haiwaip':
                    return '海外馆-商品';
                    break;
                case 'haiwaiinfo':
                    return '海外馆-资讯';
                    break;
                case 'korea':
                    return '韩国-商品';
                    break;
                case 'koreainfo':
                    return '韩国-资讯';
                    break;
                case 'guang':
                    return '逛';
                    break;
                case 'doctor':
                	return '找医生';
                	break;
                case 'usa':
                	return '美国馆';
                	break;
                default:
                    return '';
            }			
		},
		getUrl:function(txt,keyWord){
            var url = '';
            txt = $.trim(txt.replace(/open/g, ''));

            var tourp_city = this.travelCityId || -1,
            	hotel_city = this.hotelCityId || -1,
                hotelType = $('#J_tab_inner .layout-youSearch-tit').find('.tabCur').index();
            switch (txt) {
                case 'b5mo':
                    url = 'http://s.b5m.com/' + encodeURIComponent(keyWord) + '.html';
                    break;
                case 'hotel':
                    url = 'http://you.b5m.com/searcher?keyword=' + encodeURIComponent(keyWord) + '&type=1&city=' + hotel_city + '&hs=&he=&hl=&hw=' + hotelType + '&page=1';
                    break;
                //攻略
                case 'tourguide':
                    url = 'http://you.b5m.com/searcher?keyword='+ encodeURIComponent(keyWord) + '&type=2&city=-1&page=1';
                    break;
                case 'ticketp':
                    url = 'http://piao.b5m.com/search_' + encodeURIComponent(keyWord) + '.html';
                    break;
                //旅游
                case 'tourp':
                    url = 'http://you.b5m.com/searcher?keyword=' + encodeURIComponent(keyWord) + '&type=0&city=' + tourp_city + '&page=1';
                    break;
                case 'tejia':
                    url = 'http://tejia.b5m.com/taoPage_index_showIndex_' + encodeURIComponent(keyWord);
                    break;
                case 'tuanm':
                    url = 'http://tuan.b5m.com/__' + encodeURIComponent(keyWord);
                    break;
                case 'zdm':
                    url = 'http://zdm.b5m.com/keyword_' + encodeURIComponent(keyWord);
                    break;
                case 'she':
                    url = 'http://www.b5m.com/portal.php?mod=list&catid=1&keyword=' + encodeURIComponent(keyWord);
                    break;
                case 'haiwaip':
                    url = 'http://haiwai.b5m.com/search/item/'+ encodeURIComponent(keyWord) +'________1___i'
                    break;
                case 'haiwaiinfo':
                    url = 'http://haiwai.b5m.com/search/info/' + encodeURIComponent(keyWord) + '__1';
                    break;
                case 'korea':
                    url = 'http://korea.b5m.com/search/item/'+ encodeURIComponent(keyWord) +'________1___i'
                    break;
                case 'koreainfo':
                    url = 'http://korea.b5m.com/search/info/' + encodeURIComponent(keyWord) + '__1';
                    break;
                case 'guang':
                    url = 'http://guang.b5m.com/s/p?k=' + encodeURIComponent(keyWord);
                    break;
                case 'doctor':
                    url = 'http://yisheng.b5m.com/list.html?kw=' + encodeURIComponent(keyWord);
                    break;
                case 'usa':
                	url = 'http://usa.b5m.com/search/item/' + encodeURIComponent(keyWord) + '________1___i';
                	break;
                default:
                    url = 'http://s.b5m.com/' + encodeURIComponent(keyWord) + '.html';
            }
            return url;			
		}
	}

	$.fn.autoFill = function(url, site, callback){
		return this.each(function() {
			new Autofill(this,url, site, callback);
		});
	};
	/**
	 * [defaults 默认参数]
	 * 默认参数暴露出去
	 */
//	$.fn.autoFill.defaults = {
//		site:'b5mo',
//		url:'http://search.stage.bang5mai.com/allAutoFill.htm',//'http://search.b5m.com/allAutoFill.htm'
//		callback:function(){}
//	}
})(jQuery,window,document,undefined);
