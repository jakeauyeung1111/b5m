;(function($) {
    $.fn.autoFill = function(url, site, callback) {
        function Autofill(obj, url, site, callback) {
            this.input = $(obj);
            if (this.input.data('initted')) {
                return false;
            }
            this.input.data('initted', 'true');
            this.input.attr('autocomplete', 'off');
            this.site = site || 'www';
            this.ddindex = -1;
            this.url = url || 'http://search.stage.bang5mai.com/allAutoFill.htm';//'http://search.b5m.com/allAutoFill.htm';
            this.callback = callback || '';
            this.vesion = '1.2';
            this.address = '';
            this.getCity();
            this.init();
        }
        Autofill.prototype.getCity = function() {
            var _this =this;
            if (Cookies.get('tcity_sn')) {
                _this.address = decodeURIComponent(decodeURIComponent(Cookies.get('tcity_sn')));
            } else {
                $.ajax({
                    url:'http://int.dpool.sina.com.cn/iplookup/iplookup.php?format=js',
                    async:false,
                    dataType:"script",
                    success: function() {
                        _this.address = remote_ip_info.city;
                    }
                });
            }

        };
        Autofill.prototype.init = function() {
            //创建autofill容器
            this.creatDom();
            //判断cookie值，决定是否创建向导弹出框
            // if (!Cookies.get('firstlead') && this.input.is(':visible')) {
            //     //创建向导弹出框
            //     this.createlead();
            //     Cookies.set('firstlead', 'true', new Date("December 31, 2020"), '', '.b5m.com');
            // }
            this.bindEvent();
            this.setStyle();
        };
        /**
         * [createlead 创建向导弹出框]
         */
        Autofill.prototype.createlead = function() {
            var _this = this;
            this.lead = $('<div class="autofill-firstlead"><em class="at-arrow"></em><em class="at-close"></em><div class="at-con"></div><div class="at-sbm"><a class="at-btn"></a></div></div>');
            if (!$('.autofill-firstlead').length) {
                $(document.body).append(this.lead);
            }
            this.lead.find('.at-close,.at-btn').click(function(){
                _this.lead.hide();
            });

            $(document).on('click.af', function() {
                _this.lead.hide();
            });

            $(this.lead).on('click.af', function() {
                return false;
            });

            $(window).on('resize.af', function() {
                _this.resetleadPosition();
            });

            this.lead.css({
                position: 'absolute',
                left: this.input.offset().left + 'px',
                top: this.input.offset().top + this.input.outerHeight() + 10 + 'px',
                'zIndex': 9998,
                background: '#fff',
                display: 'none'
            });
            this.lead.show();
        };
        /**
         * [resetleadPosition 重置向导位置]
         */
        Autofill.prototype.resetleadPosition = function() {
            this.lead.css({
                left: this.input.offset().left + 'px',
                top: this.input.offset().top + this.input.outerHeight() + 10 + 'px'
            });
        };

        // Autofill.prototype.getCity = function() {
        //     var city = '';
        //     if (Cookies.get('tcity_sn')) {
        //         city = decodeURIComponent(decodeURIComponent(Cookies.get('tcity_sn')));
        //     } else {
        //         $.getScript('http://int.dpool.sina.com.cn/iplookup/iplookup.php?format=js', function() {
        //             city = remote_ip_info.city;
        //         });
        //     }
        //     return city;
        // };
        /**
         * [bindEvent 绑定事件 键盘向上向下键]
         */
        Autofill.prototype.bindEvent = function() {
            var _this = this;
            this.time = null;
            var obj = this.input,
                con = this.con;

            obj.on('keyup', function(e) {
                //搜索关键词
                // var params = "keyWord=" + _this.getJsParams($.trim(obj.val())) + '&city=' + _this.getCity();
                var params = "keyWord=" + _this.getJsParams($.trim(obj.val())) + '&city=' + _this.address;

                if (!$.trim(obj.val()).length) {
                    _this.hide();
                    return;
                }
                if (e.keyCode == 40 || e.keyCode == 38 || e.keyCode == 37 || e.keyCode == 39) {
                    return;
                }
                if (_this.time != null) {
                    clearTimeout(_this.time);
                }

                _this.time = setTimeout(function() {
                    _this.hide();
                    //获取数据
                    _this.getData(_this.url, params);
                }, 100);
                return false;
            });

            obj.on('focus', function() {
                _this.setStyle();
                obj.addClass('act');
            });

            con.off('click.af').on('click.af', function(e) {
                e.stopPropagation();
            });

            $(document).on('click.af', function() {
                _this.input.removeClass('af-cur');
                _this.hide();
            });

            $(window).on('resize.af', function() {
                if (_this.con.is(':visible')) {
                    _this.setPosition();
                }
            });
            //更多按钮事件
            con.delegate('.autofill-more span', 'click', function(e) {
                var prt = $(this).parent('.autofill-more');
                var index = _this.con.find('dl').index($(this).parents('dl'));

                if ($(this).parents('dl').hasClass('open')) {
                    if (index) {
                        $(this).parents('dl').find('dd:gt(1)').not(':last').hide();
                        $(this).parents('dl').removeClass('open');
                    } else {
                        $(this).parents('dl').find('dd:gt(3)').not(':last').hide();
                        $(this).parents('dl').removeClass('open');
                    }

                } else {
                    prt.siblings('dd').show();
                    prt.parent('dl').siblings('dl').removeClass('open').end().addClass('open');
                }

                _this.moreHideHandle();

            });

            con.delegate('dd:not(.autofill-more)', 'click.at', function(e) {

                if (obj.hasClass('act')) {
                    obj.val($.trim($(this).find('.txt').text())).removeClass('act');
                }

                var url = _this.locationTO($(this).find('span')[0]);

                _this.hide();
            });

            obj.on('keydown', function(e) {

                var dd = con.find('dd'),
                    ddSize = dd.filter(':visible').not('.autofill-more').size();

                if (e.keyCode == 13) {
                    _this.enterHandler();
                }

                if (!_this.input.val().length) {
                    _this.hide();
                    return;
                }

                if (e.keyCode == 40 && con.is(':visible')) {

                    _this.disableInputDft();

                    if (_this.ddindex < ddSize - 1) {
                        _this.ddindex++;

                    } else {
                        _this.ddindex = 0;
                    }
                    dd.removeClass('curr').filter(':visible').not('.autofill-more').eq(_this.ddindex).addClass('curr');

                    obj.val(dd.filter(':visible').not('.autofill-more').eq(_this.ddindex).find('.txt').text());

                    return false;

                } else {
                    if (e.keyCode == 38 && con.is(':visible')) {
                        //按向下键时移动光标
                        _this.disableInputDft();
                        if (_this.ddindex > 0) {
                            _this.ddindex--;
                        } else {
                            _this.ddindex = ddSize - 1;
                        }
                        dd.removeClass('curr').filter(':visible').not('.autofill-more').eq(_this.ddindex).addClass('curr');
                        obj.val(dd.filter(':visible').not('.autofill-more').eq(_this.ddindex).find('.txt').text());

                        return false;
                    }
                }
            });
        };

        Autofill.prototype.moreHideHandle = function() {
            this.con.find('dl:not(.open)').each(function(i) {
                if (i == 0) {
                    if ($(this).find('dd').length > 4) {
                        $(this).find('dd:gt(3)').not(':last').hide();
                    }
                } else {
                    if ($(this).find('dd').length > 2) {
                        $(this).find('dd:gt(1)').not(':last').hide();
                    }
                }
            });
        };
        /**
         * [hide 隐藏autofill]
         */
        Autofill.prototype.hide = function() {
            this.con.hide().html('');
            this.enableInputDft();
            this.ddindex = -1;
            $(this.input).trigger('listhide');
        };
        /**
         * [show 显示autofill]
         * @return {[type]} [description]
         */
        Autofill.prototype.show = function() {
            if (this.input.val().length) {
                this.con.show();
                this.setPosition();
                $(this.input).trigger('listshow');
            }
        };
        /**
         * [enterHandler 处理enter键]
         * @return {[type]} [description]
         */
        Autofill.prototype.enterHandler = function() {
            if (this.con.find('dd.curr').size()) {
                this.con.find('dd.curr').trigger('click');
                this.input.off('keyup');
                this.input.unbind('keyup');
                return false;
            }
        };
        /**
         * [disableInputDft 禁止输入框]
         * @return {[type]} [description]
         */
        Autofill.prototype.disableInputDft = function() {
            this.input.off('keydown.enter').on('keydown.enter', function(e) {
                if (e.keyCode == 13) {
                    e.preventDefault();
                    return false;
                }
            });
        };

        Autofill.prototype.enableInputDft = function() {

            this.input.off('keydown.enter');
        };
        /**
         * [creatDom 创建autofill容器]
         */
        Autofill.prototype.creatDom = function(arr) {

            this.con = $('<div class="autofill"></div>');

            $(document.body).append(this.con);

            this.input.addClass('af-input');
        };
        /**
         * [setStyle 设置autofill样式]
         */
        Autofill.prototype.setStyle = function() {

            var _this = this;
            this.con.css({
                position: 'absolute',
                left: this.input.offset().left + 'px',
                top: this.input.offset().top + this.input.outerHeight() + 'px',
                'zIndex': 9999,
                background: '#fff',
                width: this.input.outerWidth() - 2 + 'px',
                display: 'none'
            });

        };
        /**
         * [setPosition 设置autofill位置]
         */
        Autofill.prototype.setPosition = function() {

            this.con.css({
                left: this.input.offset().left + 'px',
                top: this.input.offset().top + this.input.outerHeight() + 'px'
            });

        };
        /**
         * [htmlSet 类目排序]
         * @param  {[type]} html [description]
         * @return {[type]}      [description]
         */
        Autofill.prototype.htmlSet = function(html) {
            /**
             * [con autofill容器]
             * [_this input框]
             */
            var con = this.con;
            var _this = this;
                con.html(html);
            var dl = con.find('dl'),
                dd = dl.find('dd'),
                correction = con.find('.correction'),
                maxsize = 6,
                n,
                otad,
                adl;

            /**
             * [处理类目名称以及添加一些属性]
             */
            con.find('dt').each(function() {
                var $this = $(this),
                    title = $.trim($this.text()),//类目名称 比如：'tejia' 对应 '特价'
                    $item = $this.parent('dl');
                $item.addClass(title);
                $item.find('dd').attr('ct',title);
                $this.text(_this.getCTitle(title));
            });

            var haiwaiinfo = con.find('.haiwaiinfo');
            /**
             * [获得当前站点的下标]
             */
            n = dl.index(dl.filter(function(inx) {
                return $(this).hasClass(_this.site);
            }));
            $(dd).each(function() {
                if (parseInt($(this).find('em').text()) > 9999) {
                    $(this).find('em').text('9999+');
                }
            });
            //把当前站点的下拉添加到第一个
            dl.eq(n).prependTo(con);
            dl = con.find('dl');
            /**
             * [数目条数判断，是否添加 "+" ]
             */
            dl.each(function(i){
                var $this = $(this);
                //当前站点下显示四条结果，其余显示两条结果
                if(correction.length){
                    if (i == 0) {
                        if($this.find('dd').length > 4) {
                            $this.find('dd:gt(3)').hide();
                            $this.append('<dd class="autofill-more"><span>+</span></dd>');
                        } else if ($this.find('dd').length == 0) {
                            $this.remove();
                        }
                    }else if(i != 1){
                        if ($this.find('dd').length > 2) {
                            $this.find('dd:gt(1)').hide();
                            $this.append('<dd class="autofill-more"><span>+</span></dd>');
                        } else if ($this.find('dd').length == 0){
                            $this.remove();
                        }
                    }
                }else{
                    if (i == 0){
                        if($this.find('dd').length > 4) {
                            $this.find('dd:gt(3)').hide();
                            $this.append('<dd class="autofill-more"><span>+</span></dd>');
                        } else if ($this.find('dd').length == 0) {
                            $this.remove();
                        }
                    }else{
                        if ($this.find('dd').length > 2) {
                            $this.find('dd:gt(1)').hide();
                            $this.append('<dd class="autofill-more"><span>+</span></dd>');
                        } else if ($this.find('dd').length == 0) {
                            $this.remove();
                        }
                    }
                }
            });
            /**
             * [adl 返回的频道数目]
             * maxsize为autofill显示的类目数，大于maxsize的将被截取
             */
            adl = con.find('dl');
            var flag = adl.filter('.' + this.site).length;
            if (adl.length > maxsize){
                //清空autofill
                con.empty();
                //判断是否存在当前站点的分类
                if (flag && (this.site == 'haiwaip') && haiwaiinfo.length) {
                    //去除当前站点，剩余的分类按子条目的个数排序由多到少。
                    otad = adl.not(':first').not(haiwaiinfo).sort(function(a, b) {
                        return $(b).find('dd').length - $(a).find('dd').length;
                    });

                    //添加五个分类到autofill,最后把当前站点的分类添加到第一位。
                    con.append(Array.prototype.slice.apply(otad, [0, maxsize - 2])).prepend(haiwaiinfo).prepend(dl.first());
                }else if(flag){
                    //去除当前站点，剩余的分类按子条目的个数排序由多到少。
                    otad = adl.not(':first').sort(function(a, b) {
                        return $(b).find('dd').length - $(a).find('dd').length;
                    });
                    //添加五个分类到autofill,最后把当前站点的分类添加到第一位。
                    con.append(Array.prototype.slice.apply(otad, [0, maxsize - 1])).prepend(dl.first());
                }else{
                    otad = adl.sort(function(a, b) {
                        return $(b).find('dd').length - $(a).find('dd').length;
                    });
                    con.append(Array.prototype.slice.apply(otad, [0, maxsize]));
                }
            }else{
                //清空autofill
                if(flag && (this.site == 'haiwaip') && haiwaiinfo.length){
                    con.empty();
                    //去除当前站点，剩余的分类按子条目的个数排序由多到少。
                    otad = adl.not(':first').not(haiwaiinfo).sort(function(a, b) {
                        return $(b).find('dd').length - $(a).find('dd').length;
                    });
                    //添加五个分类到autofill,最后把当前站点的分类添加到第一位。
                    con.append(Array.prototype.slice.apply(otad, [0, otad.length - 2])).prepend(haiwaiinfo).prepend(dl.first());
                }
            }
            //添加纠错此到第一位。
            con.prepend(correction);
            this.show();
            this.input.addClass('af-cur');
        };
        /**
         * [getData 获取数据]
         * @param  {[string]} url    [数据源地址]
         * @param  {[string]} params [搜索关键词]
         */
        Autofill.prototype.getData = function(url, params) {
            var _this = this;
            $.ajax({
                url: url,
                async: false,
                data: params,
                dataType: "jsonp",
                timeout: 200,
                jsonp: 'jsoncallback',
                success: function(data) {
                    //创建下拉列表
                    _this.createList(data);
                }
            });
        },
        /**
         * [createList 创建下拉列表]
         * @param  {[json]} data [数据]
         */
        Autofill.prototype.createList = function(data) {
            var html = '',
                correction_html = '',
                _this = this;
            for (var i in data) {
                if(!data[i].length || i == 'guang'){
                    continue;
                }
                if(i == 'correction'){
                    correction_html = '<dl class="correction"><dt></dt><dd data-attr="1001"><span class="txt">' + data[i][0].value + '</span></dd></dl>';
                }else{
                    var html_content = '',
                        keywords = _this.getJsParams($.trim(this.input.val()));
                    $(data[i]).each(function(j){
                        if(data[i][j].value){
                            var hlValue = data[i][j].value.replace(keywords, "<font style='color:red'>"+keywords+"</font>");
                            html_content += '<dd data-attr="1001" data-keyword="' + data[i][j].value + '"><em>' + data[i][j].count + '</em><span  class="txt">' + hlValue + '</span></dd>';
                        }
                    });
                    html += '<dl><dt>' + i + '</dt>' + html_content + '</dl>';
                }
            }
            if(!html){
                this.hide();
                return false;
            }
            //拼装html代码，纠错词条目放到最前面。
            html = correction_html + html;
            this.htmlSet(html);
        };

        Autofill.prototype.getJsParams = function(str) {
            var reg = /[']/g;
            var str = str.replace(reg, "\\'");

            str = str.replace(/\&/g, "&amp;");

            var reg = /["]/g;
            var str = str.replace(reg, "&quot;");

            return str;
        };
        Autofill.prototype.getCTitle = function(text) {
            switch (text) {
                case 'b5mo':
                    return '商品';
                    break;
                case 'tejia':
                    return '特价';
                    break;
                case 'hotel':
                    return '酒店';
                    break;
                case 'ticketp':
                    return '票务';
                    break;
                case 'tourguide':
                    return '攻略';
                    break;
                case 'tourp':
                    return '旅游';
                    break;
                case 'tuanm':
                    return '团购';
                    break;
                case 'zdm':
                    return '值得买';
                    break;
                case 'she':
                    return '帮社区';
                    break;
                case 'haiwaip':
                    return '海外馆-商品';
                    break;
                case 'haiwaiinfo':
                    return '海外馆-资讯';
                    break;
                case 'guang':
                    return '逛';
                    break;
                default:
                    return '';
            }
        };


        Autofill.prototype.getUrl = function(txt,value) {

            var url = '';
            txt = $.trim(txt.replace(/open/g, ''));

            switch (txt) {
                case 'b5mo':
                    url = 'http://s.b5m.com/search/s/___image________________' + value + '.html';
                    break;
                case 'hotel':
                    url = 'http://you.b5m.com/taoPage_-1_hotelSearchresult_' + value + '_1_search';
                    break;
                case 'tourguide':
                    url = 'http://you.b5m.com/taoPage_-1_noteSearchresult_' + value + '_1_hotNotes_search';
                    break;
                case 'ticketp':
                    url = 'http://piao.b5m.com/search_' + value + '.html';
                    break;
                case 'tourp':
                    url = 'http://you.b5m.com/taoPage_-1_searchresult_' + value + '_1_search';
                    break;
                case 'tejia':
                    url = 'http://tejia.b5m.com/taoPage_index_showIndex_' + value;
                    break;
                case 'tuanm':
                    url = 'http://tuan.b5m.com/__' + value;
                    break;
                case 'zdm':
                    url = 'http://zdm.b5m.com/keyword_' + value;
                    break;
                case 'she':
                    url = 'http://www.b5m.com/portal.php?mod=list&catid=1&keyword=' + value;
                    break;
                case 'haiwaip':
                    url = 'http://haiwai.b5m.com/search/item/'+ encodeURIComponent(value) +'________1___i'
                    break;
                case 'haiwaiinfo':
                    url = 'http://haiwai.b5m.com/search/info/' + encodeURIComponent(value) + '__1';
                    break;
                case 'guang':
                    url = 'http://guang.b5m.com/s/p?k=' + value;
                    break;
                default:
                    url = 'http://s.b5m.com/search/s/___image________________' + value + '.html';
            }

            return url;

        };


        Autofill.prototype.locationTO = function(target,spm) {

            var txt = (this.site !='www' ? $(target).parents('dl').attr('class') : $.trim($(target).parent().attr('ct'))) || '', 
                value = this.input.val();

            var _url = this.getUrl(txt,value);

            var urlConcat = _url.indexOf('?') !=-1 ? '&' : '?';
            var _spm = typeof Stat === 'object' ? (urlConcat+'spm='+Stat.spm()) : '';

            var url = _url + _spm;

            if(spm) {
                return url;
            }

            if (this.callback && this.site != 'www' && txt == this.site) {
                return this.callback();
            }

            location.href = url;

        };

        return this.each(function() {
            this.autofill = new Autofill(this, url, site, callback);
        });

    };



}(jQuery));


function getAutoFillDataUrl(target) {
    var _af = jQuery('.af-cur')[0].autofill;
    return _af.locationTO.call(_af,target,true);
}
