/*
     * jQuery Carousel plugin
     *
     * 图片滚动插件
     *
     * 参数：
     * 参数1  autoplay: true 是否开启自动播放功能 {true | false}
     * 参数2  autoplaySpeed: 3000 自动播放速度 {int}
     * 参数3  Points: true 是否开启记数状态游标 {true | false}
     * 参数4  arrows: true 是否开启翻页箭头  {true | false}
     * 参数5  reroll: true 是否开启循环播放  {true | false}
     * 参数6  speed: 300  // 播放速度 {int}
     * 参数7  maction: 'mouseover' // 选择鼠标行为方式 {'click' | 'mouseover'}
     * 测试代码在../../html/demo.marquee.html
     * 使用方法：
     * `$(element).carousel({autoplay: true, autoplaySpeed: 3000, Points: true, speed: 500});`
     */
     
// 全局定义

var b5m = window.b5m || {};
// 绑定函数器
var b5mBinder = function(fn, me) {
    return function () {
        return fn.apply(me, arguments);
    };
};

b5m.Carousel = (function() {
    function Carousel(element, options) {
        var defaults = { // 定义默认值
            autoplay: false, // 是否开启自动播放功能 {true | false}
            autoplaySpeed: 3000, // 自动播放速度 {int}
            Points: false, // 是否开启记数状态游标 {true | false}
            arrows: true, // 是否开启翻页箭头  {true | false}
            reroll: true, // 是否开启循环播放  {true | false}
            speed: 300, // 播放速度 {int}
            maction: 'mouseover' // 选择鼠标行为方式 {'click' | 'mouseover'}
        };

        this.animType = null;
        this.autoPlayTimer = null;
        this.currentSlide = 0;
        this.currentLeft = null;
        this.direction = 1;
        this.Points = null;
        this.loadIndex = 0;
        this.nextArrow = null;
        this.prevArrow = null;
        this.slideCount = null;
        this.sliderWidth = null;
        this.slideTrack = null;
        this.slides = null;
        this.sliding = false;
        this.slideOffset = 0;
        this.slider = $(element);
        this.swipeLeft = null;
        this.list = null;
        this.touchObject = {};
        this.transformsEnabled = false;
        this.options = $.extend({}, defaults, options);
        this.changeSlide = b5mBinder(this.changeSlide, this);
        this.setPosition = b5mBinder(this.setPosition, this);
        this.autoPlayIterator = b5mBinder(this.autoPlayIterator, this);
        this.init();

    }

    Carousel.prototype.init = function() {

        if (!$(this.slider).hasClass('sliderInitialized')) {
            $(this.slider).addClass('sliderInitialized');
            this.setValues();
            this.buildOut();
            this.getAnimType();
            this.setPosition();
            this.intializeEvents();
            this.startLoad();
        }
    };

    Carousel.prototype.getAnimType = function() {
        if (document.body.style.MozTransform !== undefined) {
            this.animType = 'MozTransform';
        } else if (document.body.style.webkitTransform !== undefined) {
            this.animType = "webkitTransform";
        } else if (document.body.style.msTransform !== undefined) {
            this.animType = "msTransform";
        }
        if (this.animType !== null) {
            this.transformsEnabled = true;
        }
    };

    Carousel.prototype.autoPlay = function() { 
        if(this.currentSlide != this.slideCount){
        if(!!this.autoPlayTimer){  //fix
            this.autoPlayTimer = clearInterval(this.autoPlayTimer);
        }
        this.autoPlayTimer = setInterval(this.autoPlayIterator, this.options.autoplaySpeed);
        }

    };

    Carousel.prototype.autoPlayIterator = function() {
        if (this.options.reroll === false) {
            if (this.direction === 1) {
                if ((this.currentSlide + 1) === this.slideCount - 1) {
                    this.direction = 0;
                }
                this.slideHandler(this.currentSlide + 1);
            } else {
                if ((this.currentSlide - 1 === 0)) {
                    this.direction = 1;
                }
                this.slideHandler(this.currentSlide - 1);
            }
        } else {
            this.slideHandler(this.currentSlide + 1);
        }

    };

    Carousel.prototype.startLoad = function() {
        this.list.find('img').css('opacity', 0);
        if (this.options.arrows === true) {
            this.prevArrow.hide();
            this.nextArrow.hide();
        }
        if (this.options.Points === true) {
            this.Points.hide();
        }
        this.slider.addClass('bt-loading');
    };

    Carousel.prototype.checkLoad = function() {
        var self = this, totalImages = null;
        if (this.options.reroll === true) {
            totalImages = self.slideCount + 2;
        } else {
            totalImages = self.slideCount;
        }
        if (self.loadIndex === totalImages) {
            self.list.find('img').animate({ opacity: 1 }, this.options.speed, function() {
                self.setPosition();
            });
            if (self.options.arrows === true) {
                self.prevArrow.show();
                self.nextArrow.show();
            }

            if (self.options.Points === true) {
                self.Points.show();
            }
            self.slider.removeClass('bt-loading');
            if (self.options.autoplay === true) {
                self.autoPlay();
            }
        }
    };

    Carousel.prototype.stopLoad = function() {
        this.loadIndex += 1;
        this.checkLoad();
    };

    Carousel.prototype.setValues = function() {
        this.list = this.slider.find('ul:first').addClass('bt-list');
        this.sliderWidth = this.list.width();
        this.slides = $('li:not(.cloned)', this.list).addClass('slide');
        this.slideCount = this.slides.length;
    };

    Carousel.prototype.buildOut = function() {
        var i;
        this.slider.addClass("bt-slider");
        this.slideTrack = this.slides.wrapAll('<div class="bt-track"/>').parent();

        if (this.options.arrows === true) {
            this.prevArrow = $('<a href="javascript:void(0)">上一页</a>').appendTo(this.slider).addClass('bt-prev');
            this.nextArrow = $('<a href="javascript:void(0)">下一页</a>').appendTo(this.slider).addClass('bt-next');
            if (this.options.reroll !== true) {
                this.prevArrow.addClass('disabled');
            }
        }

        if (this.options.Points === true) {
            this.Points = $('<ul class="bt-dots"></ul>').appendTo(this.slider);
            for (i = 1; i <= this.slideCount; i += 1) {
                $('<li><a href="javascript:void(0)">' + i + '</a></li>').appendTo(this.Points);
            }
            this.Points.find('li').first().addClass('active');
        }

        if (this.options.reroll === true) {
            this.slides.first().clone().appendTo(this.slideTrack).addClass('cloned');
            this.slides.last().clone().prependTo(this.slideTrack).addClass('cloned');
        }

    };

    Carousel.prototype.setDimensions = function() {
        this.list.find('li').width(this.sliderWidth);
        this.slideTrack.width(this.sliderWidth * this.slider.find('li').length);
    };

    Carousel.prototype.setPosition = function() {
        var animProps = {}, targetLeft = ((this.currentSlide * this.sliderWidth) * -1) + this.slideOffset;
        this.setValues();
        this.setDimensions();
        if (this.options.reroll === true) {
            this.slideOffset = this.sliderWidth * -1;
        }
        if (this.transformsEnabled === false) {
            this.slideTrack.css('left', targetLeft);
        } else {
            animProps[this.animType] = "translate(" + targetLeft + "px, 0px)";
            this.slideTrack.css(animProps);
        }
    };

    Carousel.prototype.intializeEvents = function() {
        var self = this;
        if (this.options.arrows === true) {
            this.prevArrow.on('click', {message: 'previous'}, this.changeSlide);
            this.nextArrow.on('click', {message: 'next'}, this.changeSlide);
        }
        if (this.options.Points === true) {
            $('li a', this.Points).on(this.options.maction, {message: 'index'}, this.changeSlide);
        }
        $(window).on('resize', this.setPosition);
        this.list.find('img').load(function() { self.stopLoad(); });
    };

    Carousel.prototype.changeSlide = function(event) {
        switch (event.data.message) {
        case 'previous':
            this.slideHandler(this.currentSlide - 1);
            break;
        case 'next':
            this.slideHandler(this.currentSlide + 1);
            break;
        case 'index':
            this.slideHandler($(event.target).parent().index());
            break;
        default:
            return false;
        }
    };

    Carousel.prototype.updatePoints = function() {
        this.Points.find('li').removeClass('active');
        $(this.Points.find('li').get(this.currentSlide)).addClass('active');

    };

    Carousel.prototype.slideHandler = function(index) {
        var animProps = {}, targetSlide, slideLeft, targetLeft = null, self = this;
        targetSlide = index;
        targetLeft = ((targetSlide * this.sliderWidth) * -1) + this.slideOffset;
        slideLeft = ((this.currentSlide * this.sliderWidth) * -1) + this.slideOffset;
        if (self.options.autoplay === true) {
            clearInterval(this.autoPlayTimer);
        }
        if (this.swipeLeft === null) {
            this.currentLeft = slideLeft;
        } else {
            this.currentLeft = this.swipeLeft;
        }
        if (targetSlide < 0) {
            if (this.options.reroll === true) {
                if (this.transformsEnabled === false) {
                    this.slideTrack.animate({
                        left: targetLeft
                    }, self.options.speed, function() {
                        self.currentSlide = self.slideCount - 1;
                        self.setPosition();
                        if (self.options.Points) {
                            self.updatePoints();
                        }
                        if (self.options.autoplay === true) {
                            self.autoPlay();
                        }
                    });
                } else {
                    $({animStart: this.currentLeft}).animate({
                        animStart: targetLeft
                    }, {
                        duration:  self.options.speed,
                        step: function(now) {
                            animProps[self.animType] = "translate(" + now + "px, 0px)";
                            self.slideTrack.css(animProps);
                        },
                        complete: function() {
                            self.currentSlide = self.slideCount - 1;
                            self.setPosition();
                            if (self.swipeLeft !== null) {
                                self.swipeLeft = null;
                            }
                            if (self.options.Points) {
                                self.updatePoints();
                            }
                            if (self.options.autoplay === true) {
                                self.autoPlay();
                            }
                        }
                    });
                }
            } else {
                return false;
            }
        } else if (targetSlide > (this.slideCount - 1)) {
            if (this.options.reroll === true) {
                if (this.transformsEnabled === false) {
                    this.slideTrack.animate({
                        left: targetLeft
                    }, self.options.speed, function() {
                        self.currentSlide = 0;
                        self.setPosition();
                        if (self.options.Points) {
                            self.updatePoints();
                        }
                        if (self.options.autoplay === true) {
                            self.autoPlay();
                        }
                    });
                } else {
                    $({animStart: this.currentLeft}).animate({
                        animStart: targetLeft
                    }, {
                        duration:  self.options.speed,
                        step: function(now) {
                            animProps[self.animType] = "translate(" + now + "px, 0px)";
                            self.slideTrack.css(animProps);
                        },
                        complete: function() {
                            self.currentSlide = 0;
                            self.setPosition();
                            if (self.swipeLeft !== null) {
                                self.swipeLeft = null;
                            }
                            if (self.options.Points) {
                                self.updatePoints();
                            }
                            if (self.options.autoplay === true) {
                                self.autoPlay();
                            }
                        }
                    });
                }
            } else {
                return false;
            }
        } else {
            if (this.transformsEnabled === false) {
                this.slideTrack.animate({
                    left: targetLeft
                }, self.options.speed, function() {
                    self.currentSlide = targetSlide;
                    if (self.options.Points) {
                        self.updatePoints();
                    }
                    if (self.options.autoplay === true) {
                        self.autoPlay();
                    }
                    if (self.options.arrows === true && self.options.reroll !== true) {
                        if (self.currentSlide === 0) {
                            self.prevArrow.addClass('disabled');
                        } else if (self.currentSlide === self.slideCount - 1) {
                            self.nextArrow.addClass('disabled');
                        } else {
                            self.prevArrow.removeClass('disabled');
                            self.nextArrow.removeClass('disabled');
                        }
                    }
                });
            } else {
                $({animStart: this.currentLeft}).animate({
                    animStart: targetLeft
                }, {
                    duration:  self.options.speed,
                    step: function(now) {
                        animProps[self.animType] = "translate(" + now + "px, 0px)";
                        self.slideTrack.css(animProps);
                    },
                    complete: function() {
                        self.currentSlide = targetSlide;
                        if (self.swipeLeft !== null) {
                            self.swipeLeft = null;
                        }
                        if (self.options.Points) {
                            self.updatePoints();
                        }
                        if (self.options.autoplay === true) {
                            self.autoPlay();
                        }
                        if (self.options.arrows === true && self.options.reroll !== true) {
                            if (self.currentSlide === 0) {
                                self.prevArrow.addClass('disabled');
                            } else if (self.currentSlide === self.slideCount - 1) {
                                self.nextArrow.addClass('disabled');
                            } else {
                                self.prevArrow.removeClass('disabled');
                                self.nextArrow.removeClass('disabled');
                            }
                        }
                    }
                });
            }
        }
    };
    return Carousel;
}());

$.fn.carousel = function (options) {
    var carousels = [];
    return this.each(function (index) {
        carousels[index] = new b5m.Carousel(this, options);
    });
};
