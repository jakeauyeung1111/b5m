(function(window, $) {


    var b5m = window.b5m = {};
    $.b5m = {};

    function _namespace(ns_string) {
        var parts = ns_string.split('.'),
            parent = this,
            i;


        for(i=0;i<parts.length;i++) {
            if(typeof parent[parts[i]] === 'undefined') {
                parent[parts[i]] = {};
            }
            parent = parent[parts[i]];
        }
        return parent;
    }

    b5m.namespace = function(s) {
        return _namespace.call(this,s);
    }

    $.b5m.namespace = function(s) {
        return _namespace.call(this,s);
    }

    b5m.ui = {};
    b5m.utils = {};
    b5m.info = {};



    /* cookie */
    window.Cookies = {
        set:function(name,value) {
            var argv = arguments;
            var argc = arguments.length;
            var expires = (argc > 2) ? argv[2] : null;
            var path = (argc > 3) ? argv[3] : '/';
            var domain = (argc > 4) ? argv[4] : null;
            var secure = (argc > 5) ? argv[5] : false;
            document.cookie = name + "=" + escape(value) + ((expires == null) ? "" : ("; expires=" + expires.toGMTString())) + ((path == null) ? "" : ("; path=" + path)) + ((domain == null) ? "" : ("; domain=" + domain)) + ((secure == true) ? "; secure" : "");
        },
        get:function(name) {
            var arg = name + "=";
            var alen = arg.length;
            var clen = document.cookie.length;
            var i = 0;
            var j = 0;
            while (i < clen) {
                j = i + alen;
                if (document.cookie.substring(i, j) == arg)
                    return Cookies.getCookieVal(j);
                i = document.cookie.indexOf(" ", i) + 1;
                if (i == 0)
                    break;
            }
            return null;
        },
        clear:function(name) {
            if (Cookies.get(name)) {
                var expdate = new Date();
                expdate.setTime(expdate.getTime() - (86400 * 1000 * 1));
                Cookies.set(name, "", expdate);
            }
        },
        getCookieVal:function(offset) {
            var endstr = document.cookie.indexOf(";", offset);
            if (endstr == -1) {
                endstr = document.cookie.length;
            }
            return unescape(document.cookie.substring(offset, endstr));
        },
        //编码不同
        setEn:function(name,value) {
            var argv = arguments;
            var argc = arguments.length;
            var expires = (argc > 2) ? argv[2] : null;
            var path = (argc > 3) ? argv[3] : '/';
            var domain = (argc > 4) ? argv[4] : null;
            var secure = (argc > 5) ? argv[5] : false;
            document.cookie = name + "=" + encodeURI(value) + ((expires == null) ? "" : ("; expires=" + expires.toGMTString())) + ((path == null) ? "" : ("; path=" + path)) + ((domain == null) ? "" : ("; domain=" + domain)) + ((secure == true) ? "; secure" : "");
        },
        getEn:function(name) {
            var arg = name + "=";
            var alen = arg.length;
            var clen = document.cookie.length;
            var i = 0;
            var j = 0;
            while (i < clen) {
                j = i + alen;
                if (document.cookie.substring(i, j) == arg)
                    return Cookies.getCookieValEn(j);
                i = document.cookie.indexOf(" ", i) + 1;
                if (i == 0)
                    break;
            }
            return null;
        },
        clearEn:function(name) {
            if (Cookies.get(name)) {
                var expdate = new Date();
                expdate.setTime(expdate.getTime() - (86400 * 1000 * 1));
                Cookies.set(name, "", expdate);
            }
        },
        getCookieValEn:function(offset) {
            var endstr = document.cookie.indexOf(";", offset);
            if (endstr == -1) {
                endstr = document.cookie.length;
            }
            return decodeURI(document.cookie.substring(offset, endstr));
        }
    };




    //顶部通栏生成
    (function() {

        var _url = 'ucenter.b5m.com';
        var _domain = '.b5m.com';

        _url = (function () {
            if (location.hostname.indexOf('stage.bang5mai.com') !== -1) {
                _domain = '.bang5mai.com';
                return 'ucenter.stage.bang5mai.com';
            }
            if (location.hostname.indexOf('prod.bang5mai.com') !== -1) {
                _domain = '.bang5mai.com';
                return 'ucenter.prod.bang5mai.com';
            }
            if (location.hostname.indexOf('ucenter.test.com') !== -1) {
                _domain = '.test.com';
                return 'ucenter.test.com';
            }

            if(/^\d+\./.test(location.hostname)) {
                _domain = location.hostname;
                return location.hostname;
            }

            return 'ucenter.b5m.com';
        })();

        //登录标识
        var userType = (function() {

            if(location.href.indexOf('www.b5m.com/forum.php') !== -1) {
                return 17;
            }

            switch(location.host) {
                case 'www.b5m.com':
                    return 8;
                case 's.b5m.com':
                    return 22;
                case 'haiwai.b5m.com':
                    return 23;
                case 'zdm.b5m.com':
                    return 20;
                case 'tejia.b5m.com':
                    return 6;
                case 'tuan.b5m.com':
                    return 16;
                case 'you.b5m.com':
                    return 13;
                case 'piao.b5m.com':
                    return 19;
                case 'she.b5m.com':
                    return 17;
                case 't.b5m.com':
                    return 5;
                case 'korea.b5m.com':
                    return 18;
                case 'plus.gwmei.com':
                    return 15;
                case 'www.gwmei.com':
                    return 4;
                case 'hao.b5m.com':
                    return 7;
                case 'tiao.b5m.com':
                    return 9;
                case 'guang.b5m.com':
                    return 25;
                case 'daikuan.b5m.com':
                    return 24;
                case 'gzx.b5m.com':
                    return 26;
                case 'yisheng.b5m.com':
                    return 21;
                case 'usa.b5m.com':
                    return 27;
                case 'tao.b5m.com':
                    return 29;
                default:
                    return 0;
            }
        })();

        var topHdbanner = $('.top-hdbanner');
      //  if(!Cookies.get('topbarbanner-close')) {
            topHdbanner.empty().append('<a href="javascript:void(0)" class="top-banner-close">关闭</a><a href="http://oreg.jj.cn/gpbd/bang5w.html?siteid=4501182" target="_blank"><img src="http://staticcdn.b5m.com/static/images/huodong/jj/bop-banner.jpg" alt="“帮5买杯”第1届JJ斗地主帮豆回馈赛" width="980" height="60" /></a>');
            $('.top-banner-close').one('click',function() {
                topHdbanner.animate({height:0});
                return false;


              //  Cookies.set('topbarbanner-close','true',new Date(new Date().getTime()+  1000*60*60*24*30),'','.b5m.com');
            });
    //    }


        var topBarContent = $('.topbar').eq(0),
            isLogin = Cookies.get('login') === 'true' && Cookies.get('token'),
            userData= {};

        b5m.info.isLogin = isLogin;

        //topbar reset
        if($('.topbar-inner').length) {
            $('.topbar-inner').detach();
        }
        //登录之后跳回当前页面地址
        var refererUrl = (function() {
            try {
                if(location.search.indexOf('loginReferer') !== -1) {
                    return /loginReferer=([^&]*)/.exec(location.search)[1];
                }
            } catch (e) {}
            return encodeURIComponent(location.href);
        })();



        //生成topbar
        topBarContent.append(function() {
            var _content = '<div class="topbar-inner wp cf">';
            //加载左部链接
            _content += '<div class="topbar-nav"><a class="home" href="http://www.b5m.com">帮5买首页</a><span class="line"></span><a class="buy" href="http://hao.b5m.com">购物导航</a><span class="line"></span><a class="korea" href="http://korea.b5m.com">韩国馆</a><span class="line"></span><a class="tao" href="http://t.b5m.com">帮5淘</a><span class="line"></span><a class="app" href="http://app.b5m.com">手机帮5买</a></div>';

            //已登录状态
            if(isLogin) {

                _content += '<div class="topbar-user topbar-user-login">';

                $.ajax({
                    url:'http://'+ _url +'/user/user/data/info.htm?isSimple=1',
                    // url:'http://ucenter.stage.bang5mai.com/user/user/data/info.htm?isSimple=1',
                    dataType:'jsonp',
                    jsonp:'jsonpCallback',
                    success:function(data) {
                        userData = data;

                        if(!userData.ok) {
                            Cookies.set('login','false',new Date(),'',_domain);
                            location.href = 'http://'+ _url;
                            return false;
                        }

                        //用户名
                        $('#b5muser').text(decodeURIComponent(userData.data.showName));
                        //签到
                        if(userData.data.isSign){
                            topBarContent.find('.userreg').addClass('off');
                        }
                    }
                });

                $.ajax({
                    // url:'http://ucenter.stage.bang5mai.com/user/message/data/count.htm',
                    url:'http://'+ _url +'/user/message/data/count.htm',
                    dataType:'jsonp',
                    jsonp:'jsonpCallback',
                    success:function(data) {
                        userDataMessage = data;
                        //消息数目
                        $('.topbar-msg-num').text(userDataMessage.data);
                    }
                });

                _content += '<span class="hi">你好，</span><span data-hover="user-hover" class="user"><a target="_self" id="b5muser" href="http://'+ _url +'"></a> <em class="arr"></em><span class="user-link"><a class="userreg" target="_self" href="javascript:void(0)">签到</a><a href="http://'+ _url +'/forward.htm?method=/user/trade/common/record/index" target="_self">帮豆</a><a href="http://'+ _url +'/user/user/logout.htm" target="_self">退出</a></span></span><span class="line"></span>';

                _content += '<span class="user msg" data-hover="user-hover"><a href="http://'+ _url +'/forward.htm?method=/user/msg/system/index" target="_self" class="msg-t">消息<em class="topbar-msg-num"></em></a><em class="arr"></em><span class="user-link"><a href="http://'+ _url +'/forward.htm?method=/user/msg/system/index" target="_self">系统消息<em class="topbar-msg-num"></em></a></span></span>';

            }
            //未登录状态
            else {
                _content += '<div class="topbar-user topbar-user-unlogin">';
                _content += '<a href="http://'+ _url +'/user/third/login/auth.htm?type=2&amp;refererUrl=' + refererUrl + '&amp;userType='+ userType +'" rel="fav" class="weibo" target="_self">微博登录</a><span class="line"></span><a href="http://'+ _url +'/user/third/login/auth.htm?type=1&amp;refererUrl=' + refererUrl +'&amp;userType='+ userType +'" rel="fav" class="qq" target="_self">QQ登录</a><span class="line"></span>';
                _content += '<a rel="nofollow" target="_self" href="http://'+ _url +'?loginReferer=' + refererUrl +'" class="high">登录</a><span class="line"></span><a rel="nofollow" target="_self" href="http://'+ _url +'/forward.htm?method=/user/info/register&amp;userType='+ userType +'&amp;url=' + refererUrl +'">注册</a>';
            }

            _content += '<span class="line"></span><a target="_self" class="J_addFav" rel="fav" href="javascript:void(0)">收藏本站</a>';
            _content += '<span class="line"></span>';
            _content += '<div class="topbar-more" data-hover="topbar-more-hover"><a href="javascript:void(0)" id="J_addFav" target="_self" class="more">网站导航 <em class="arr"></em></a><div class="topbar-prod"><div class="top-border"></div><div class="item"><a target="_blank" href="http://www.b5m.com">帮5买</a><a target="_blank" href="http://tejia.b5m.com">淘特价</a><a target="_blank" href="http://you.b5m.com">帮5游</a><a target="_blank" href="http://guang.b5m.com">帮5逛</a><a target="_blank" href="http://tuan.b5m.com/">帮团购</a><a target="_blank" href="http://piao.b5m.com">帮票务</a><a target="_blank" href="http://www.b5m.com">比价网</a><a target="_blank" href="http://haiwai.b5m.com">海外馆</a><a target="_blank" href="http://t.b5m.com/">帮5淘</a><a target="_blank" href="http://s.b5m.com/">购物搜索</a><a class="lar" target="_blank" href="http://app.b5m.com/">手机帮5买</a></div><div class="item weixin"><img src="http://staticcdn.b5m.com/static/images/common/weixin_b5m.png"><span>扫二维码，加帮5买微信好友</span></div></div></div>';

            _content += '</div></div>';
            return _content;
        });


        //签到
        topBarContent.find('.userreg').click(function(e) {

            if(userData.data.isSign) {
                return false;
            }

            $.ajax({
                url:'http://'+ _url +'/user/task/data/sign.htm',
                dataType:'jsonp',
                jsonp:'jsonpCallback',
                success:function(data) {
                    if(data.ok) {
                        if(location.href.indexOf(_url) != -1) {
                            location.href = location.href;
                        }else {
                            var dialog = $('<div class="dialog-qiandao"><a href="javascript:void(0)" class="close"></a><span></span><b></b><i></i><em></em></div>').appendTo('body').show();
                            var _signday = userData.data.signDay || 0;
                            dialog.find('span').text(decodeURIComponent(userData.data.showName));
                            dialog.find('b').text(data.data == 5 ? 1 : (_signday + 1));
                            dialog.find('i').text('+' + data.data);
                            dialog.find('em').text('+' + (data.data === 50 ? 50 : data.data+5));
                            dialog.find('a').click(function() {
                                dialog.detach();
                                return false;
                            });
                            $(e.currentTarget).addClass('off').attr('href','javascript:void(0)').off();
                            if($('#signscroe1').length) {
                                $('#signscroe1').hide();
                                $('#signscroe2').show();
                            }
                        }
                    }
                }
            });

            return false;

        });


        //收藏功能
        var $J_addFav = $(".J_addFav");
        if (!$J_addFav.length) {
            $J_addFav = $(".topbar a[rel=fav]");
        }
        $J_addFav.length && $J_addFav.on('click', function(e) {
            e.preventDefault();
            var url = encodeURI(window.location.host);
            var title = $('title').html();
            if (window.external) {
                try {
                    window.external.addFavorite(window.location.href, document.title);
                } catch (e) {
                    alert("加入收藏失败，请使用Ctrl+D进行添加");
                }

            } else if (window.sidebar) {
                window.sidebar.addPanel(document.title, window.location.href, "");
            } else {
                alert("加入收藏失败，请使用Ctrl+D进行添加");
            }
        });

    })();



})(window, window.jQuery);