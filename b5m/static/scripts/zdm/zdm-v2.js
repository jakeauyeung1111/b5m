$(function() {
    $('.zdm-item').each(function(i,n) {
        bShare.addEntry({
            product:$(n).find('h2 a').text(),
            url:$(n).find('h2 a').attr('href'), 
            pic:$(n).find('.pic').children('img').attr('lazy-src'),
            summary:$(n).find('.text').html(),
            price:$(n).find('input.product_price').val() || "暂无报价"
        });
    });
    /*innner share*/
    bShare.addEntry({
            product:$(".zdm-article").find("h1").text(),
            url:window.location, 
            pic:$("#detail_pic").val(),
            summary:$("#detail_summary").val(),
            price:$("#detail_price").val()
        });

    /*pic slider s*/
    var $slider = $('#zdm_slider'),
        $slider_box = $slider.find('ul'),
        $slider_item = $slider.find('li'),
        len = $slider_item.length - 1,
        step = $slider_item.width(),
        $trigger = $('#zdm_slider_trigger').find('a'),
        i = 0;
    $slider_box.width($slider_item.outerWidth(true)*$slider_item.length);
    $trigger.on('mouseover',function(){
        var $this = $(this),
            i = $trigger.index($this);
        move(i);
    });

    function move(i){
        $slider_box.stop(true,true).animate({'left':-i*step});
        $trigger.eq(i).addClass('cur').siblings('a').removeClass('cur');
    }

    function autoMove(){
        flag = setInterval(function(){
            if(i >= len){
                i = 0;
            }else{
                i++;
            }
            move(i)
        },3000);
    }

    autoMove();

    $slider.hover(function(){
        clearInterval(flag);
    },function(){
        autoMove();
    });
    /*pic slider e*/
    /*left filter class s*/
    if ($('.filter-class').length) {
        fixDom('.filter-class','filter-fix');
    };
    function fixDom(obj,className) {
        var dom = $(obj),
            h = $(obj).offset().top,
            isIE6 = window.XMLHttpRequest ? false : true;
        if (!dom.length) return false;
        if (!isIE6) {
            $(window).on('scroll.zdm', function() {

                if ($(window).scrollTop() >= h) {
                    dom.addClass(className);
                } else {
                    dom.removeClass(className);
                }
            });
        }
    }
    /*left filter class e*/
    /*baoliao s*/
    /*$(".zdm-say").find(".bl-btn").on("click",function(){
        $(".say-popbox").stop(true,true).fadeIn();
        return false;
    });
    $('#close').click(function(){
        $('.say-popbox').stop(true,true).fadeOut();
        return false;
    });*/

     $('#baoliao').click(function(){
        var url = $('#url').val(),
            summary = $('#summary').val(),
            strRegex = "[a-zA-z]+://[^\s]*",
            re=new RegExp(strRegex);
        if(re.test(url)){
            $.ajax({
                url:'/baoliao?url='+url+'&summary='+summary,
                type:'Get',
                dataType:'html',
                success:function(data){
                    if(data){
                        alert(data);
                        $('#say-popbox').hide();
                    }
                }
            });
        }else{
            alert("请输入正确地址");
        }
    })

    $(".zdm-say").find(".close-btn").on("click",function(){
        $(".say-popbox").stop(true,true).fadeOut();
    });
    /*baoliao e*/
    $(".zdm-item").find("dt").hover(function(){
        $(this).children("span").show();
    },function(){
        $(this).children("span").hide();
    })
    /*ranking s*/
    $(".rank-tiems:first").find("dd").show();
    $(".rank-tiems").hover(function(){
        $(".rank-tiems").find("dd").hide();
        $(this).find("dd").show();
    });
    /*ranking e*/
    /*commont s*/
    var $comtit=$(".zdm-comment").find(".tab-tit");
    var $comcent=$(".zdm-comment").find(".con-list");
    $comtit.find("a").on("click",function(){
        $(this).addClass("cur").siblings('a').removeClass("cur");
    });
    /*$comcent.find(".replay").on("click",function(){
        $(this).parent("p").siblings('.user-comm').children("dt").eq(0).siblings('dt').hide().end().stop(true,true).toggle();

    });*/
    $comcent.find('.hf-text').on('click',function(){
        var $this = $(this);
        $this.parent('p').parent('dd').next('dt').siblings('dt').hide().end().toggle();
    });
    /*commont e*/
    /*cloud share nav s*/
    $(".cloud-nav").hover(function(){
        $(this).find("dd").stop(true,true).slideDown();
    },function(){
        $(this).find("dd").stop(true,true).slideUp();
    });
    if ($('.cloud-nav').length) {
        fixDom('.cloud-nav','cloud-fix');
    };
    /*cloud share nav e*/
    /*zdm topic*/
    if ($('.top-class').length) {
        fixDom('.top-class','topic-fix');
    };

    // vote
    $(".zdm-info").delegate('.i-praise a', 'click', function() {
        var _this = $(this),
            text = _this.text(),
            textArr=[];
            textArr=text.split("(");

        var dataid = $('#article_id').length > 0 ? $('#article_id').val() : _this.attr('dataid');

        $.post("/vote.htm", {dataid: dataid}, function(data) {
            if(data.code === 100){       
                
                _this.html(textArr[0]+'('+data.data+')').addClass("cur");
            }else{
                _this.addClass("cur");
            }
        },'json');

        return false;

    });
    /*$('.com-items').find('.smb-btn').click(function(event) {
         $(this).parent("p").siblings(".bangdou-tips").stop(true,true).fadeIn();
         setInterval(function(){
            $(".bangdou-tips").stop(true,true).fadeOut();
         },2000)
     });*/
});