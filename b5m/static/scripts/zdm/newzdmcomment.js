var zdmCommentsFed = {
    /*是否执行评论*/
	commentsFlag:true,
    /*是否可评论*/
    canSub:true,
    /*检查是否可评论*/
    getCanSum:function(){
        if(this.canSub){
            this.canSub = false;
            this.resetCanSub();
            return true;
        }
        return false;
    },
    /*定时处理可评论*/
    resetCanSub:function(){
        var _this = this;
        setTimeout(function(){
            _this.canSub = true;
        },5000);
    },
    /**
     * 判断字符串是否为空
     * @param args 要检测的字符串
     * @returns {boolean}
     */
	isEmpty:function(args){
		return !args || !$.trim(args);
	},
    /**
     * 判断是否 数据包含 某数据
     * @param inData 包含数据
     * @param data 数据
     * @returns {boolean}
     */
    isIn:function(inData,data){
        for(var i in data){
            if(inData==data[i]) return true;
        }
        return false;
    },
    /**
     * 获取当前页面 文章 ID
     */
	pageId:function(){
		var _id = location.pathname.substring(1, location.pathname.indexOf('.'))
		,pageId = Number(_id.substring(_id.lastIndexOf('/') + 1))||false;
		if(!pageId){
			this.commentsFlag = false;
			return false;
		}
		return pageId;
	}(),
    /**
     * 时间戳转时间
     * @param stamp 时间戳
     * @returns {string} 时间
     */
    add0:function(Num){
        var Temp=0;
        if (parseInt(Num,10)<10)
        {
            Temp="0"+Num; 
        }else 
        {
            Temp=Num;
        };
        return Temp;
    },
	getCommentDate:function(stamp){
		var newData = new Date(stamp*1000);
        function numberDeal(num){
            return num>9?num:"0"+num;
        }
		return newData.getFullYear()+"-"+numberDeal(newData.getMonth()+1)+"-"+numberDeal(newData.getDate())+" "+numberDeal(newData.getHours())+":"+numberDeal(newData.getMinutes())+":"+numberDeal(newData.getSeconds());
	},
    /**
     * 生成 回复列表
     * @param rData 回复数据
     * @returns {string} 回复列表HTML字符串
     */
	getCommentsReplay:function(rData){
		var replayHtml = ''
			,nData;
		for(var i = 0;i<rData.length;i++){
			nData = rData[i];
			replayHtml += '<dd id="'+nData['id']+'"><p class="name"><span><em>'+nData['nick_name']+'</em>'+this.getCommentDate(nData['create_time'])+'</span></p><p class="com-text">'+nData['content']+'</p></dd>';	
		}		
		return replayHtml;
	},
    /**
     * 获取评论列表
     * @param datas 评论数据
     * @returns {string} 评论列表HTML字符串
     */
	getListLi:function(datas,total){
		var htmlStr=""
			,data
            ,pageBox = $(".page_num",".zdm-brd>.page")
            ,nowPage = 0;
        if(pageBox.length>0){
            nowPage = Number(pageBox.val())-1;
        }
		for(var i = 0; i<datas.length;i++){
			data = datas[i];
			htmlStr += '<li replayid="'+data['id']+'"><span class="pic"><img src="'+data['avatar']+'"></span><p class="name"><span><em>'+(data['uid']!=""?data['nick_name']:data['ip'])+'</em>'
                +this.getCommentDate(data['create_time'])+'</span><a class="replay" title="回复" href="javascript:void(0);" target="_self">回复</a>'+(total-(nowPage*6+i))+'楼</p>' +
                '<div class="com-text mart10">'+data['content']+'</div><dl class="user-comm"><dt class="clear-fix"><a class="hf-btn" target="_self" title="回 复" href="javascript:void(0);">回 复</a>' +
                '<input type="text" class="in-txt"></dt>';
			htmlStr += (data['replyData'].length>0?this.getCommentsReplay(data['replyData']):"")+'</dl></li>';//(data['replyData'].length>0?this.getCommentsReplay():"")+
		}		
		return htmlStr;	
	},
    /**
     * 获取评论
     * @param args 评论设置
     */
	getComments:function(args){
		var _this = this
			,_Args = $.extend({},{page:1,order:"id",id:_this.pageId||1},args||{});
		$.ajax({
			url:'http://zdm.b5m.com/commentpage_' + _Args['id'] + '.htm?t=' + Math.random(10000, 9999),
			type:"GET",
			async:true,
			data:_Args,
			dataType:"jsonp", 
			jsonp: 'jsonpCallback',
			success:function(json){
                if(!_this.isIn(json['code'],[100,103])){
                    return false;
                }
                if(json['data']['list'].length){
                    $(".zdm-article").siblings(".page").html(json['data']['page']).find("div>a").attr("href","javascript:void(0);").addClass("pageAbt");
                    $(".con-list",".zdm-comment").html("").prepend(_this.getListLi(json['data']['list'],json['data']['total'])).find("div.c-n").remove();
                }else{
                    $(".page",".zdm-comment").html("").prepend('<p style="text-align:center">没有评论数据</p>');
                }
                $(".page").on({
                    click:function(){
                        _this.getComments({page:$(this).attr("page")});
                        return false;
                    }
                },".pageAbt");
                $(".page").on({
                    click:function(){
                        _this.getComments({page:$(this).siblings('input').val()||1});
                        return false;
                    }
                },".go_page");
			},
			error:function(){ 
				
			} 
		});	
	},
    /*回复*/
    replayComments:function(){
        var _this = this;
        $(".con-list").on({
            click:function(){
                $(this).parent("p").siblings('.user-comm').children("dt").stop(true,true).toggle();
                $("dt",'.user-comm').on({
                    click:function(){
                        var replayTxt = $(this).siblings("input:text")
                            ,replayVal = replayTxt.val()
                            ,replyId;
                        // 判断登录状态
                        if (Cookies.get('login') != "true") {
                            alert('亲！你还没有登录');
                            return false;
                        }
                        if(_this.isEmpty(replayVal)){
                            // alert('请输入评论内容！');
                            return false;
                        }
                        replyId = $(this).parents("li").attr("replayid");
                        if(!_this.getCanSum())return false;
                        $.ajax({
                            url:'http://zdm.b5m.com/Commentreply.htm',
                            type:"POST",
                            data:{replyId: replyId, reply_content: replayVal, id: _this.pageId},
                            dataType:"jsonp",
                            jsonp: 'jsonpCallback',
                            success:function(data){
                                if(!_this.isIn(data['code'],[100,103])){
                                    return false;
                                }
                                _this.getComments();

                            }
                        });
                        return false;
                    }
                },".hf-btn");
                return false;
            }
        },".replay");
    },
    /*发表评论*/
	sendComments:function(){
        var _this = this
            ,txtArea = "textarea.com_content";
        $(txtArea).siblings('p').find('.smb-btn').click(function(){
            var sendVal = $(txtArea).val();
            // 判断登录状态
            if (Cookies.get('login') != "true") {
                alert('亲！你还没有登录');
                return false;
            }
            if(_this.isEmpty(sendVal)){
               // alert('请输入评论内容！');
                return false;
            }
            if(!_this.getCanSum())return false;
            $.ajax({
                url:"http://zdm.b5m.com/ajaxcomment.htm",
                type:"POST",
                data:{content: sendVal, id:_this.pageId},
                dataType:"jsonp",
                jsonp: 'jsonpCallback',
                success:function(data){
                    if(!_this.isIn(data['code'],[100,103])){
                        return false;
                    }
                    if(data['code']==100){
                        $(".bangdou-tips").stop(true,true).fadeIn();
                        setInterval(function(){
                            $(".bangdou-tips").stop(true,true).fadeOut();
                        },2000)
                    }
                    $(txtArea).val('');
                   _this.getComments();
                }
            });
            return false;
        });
    },
    /**
     * 表达读后感
     */
    readFeed:function(){
        var _this = this;
        $("li",'.book-review').on({
            click:function(){
               /* var txtArea = $('dd>.com_content','.zdm-comment')
                    ,txtMsg = $(this).text();*/
                var txtMsg = $(this).text();;
                if(!_this.canSub)return false;
                //txtArea.val($(this).text()).siblings("p").find("a.smb-btn").trigger("click");
                $.ajax({
                    url:"http://zdm.b5m.com/ajaxcomment.htm",
                    type:"POST",
                    data:{content: txtMsg, id:_this.pageId},
                    dataType:"jsonp",
                    jsonp: 'jsonpCallback',
                    success:function(data){
                        if(!_this.isIn(data['code'],[100,103])){
                            return false;
                        }
                        if(data['code']==100){
                            $(".bangdou-tips").stop(true,true).fadeIn();
                            setInterval(function(){
                                $(".bangdou-tips").stop(true,true).fadeOut();
                            },2000)
                        }
                        _this.getComments();
                    }
                });
                return false;
            }
        },'a');
    },
	loadFun:function(){
		var _this = this;
		if(this.commentsFlag){
            _this.getComments();
            _this.sendComments();
            _this.replayComments();
            _this.readFeed();
        }
	}
};
zdmCommentsFed.loadFun();
$('.J_autofill').autoFill('', 'zdm');


