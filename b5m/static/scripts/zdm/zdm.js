$(function() {

    // setLeftNav();

    oSlider = new ToSlide('.top-slider .b-l', '.top-slider .b-r', '.top-slider .mask');
    oSlider.l.addClass('sl_disable');
    $(oSlider).bind('disable', function(e, n) {
        if (n) {
            this.r.addClass('sl_disable');

        } else {
            this.l.addClass('sl_disable');
        }
    });



    $('.page-input input').on('keyup', function(e) {
        this.value = this.value.replace(/\D/g, "");
    });
    $('#go_page').on('click', function(e) {
        var val = $('.page-input input').val();
        pageEvent(val);
    });

    /*    $('.ic-ht.av').each(function() {
        $(this).one('click', function() {
            plus1(this);
            $(this).removeClass('av')
        });
    })
*/

    if ($('.right .right-fix').length) {
        fixDom('.right .right-fix');
    }

/*   $('.c-list .reply').each(function(i){
        $(this).insertRely(i);
    })*/
    //帮豆加分提示
    // var bangdouTips = function(){
    //     var tips;
    //     function init(num){
    //         return $('<div class="bangdou-tips" style="color:red"><div class="bangdou-tips__in">' + "+" + num + '</div></div>').appendTo($('.bangdou-target'));
    //     }
    //     return {
    //         getTips:function(num){
    //             return tips || (tips = init(num));
    //         }
    //     }
    // }();

    // function addBangtou(num){
    //     bangdouTips.getTips(num)
    //     .css({opacity:1})
    //     .animate({
    //         top:-75,
    //         right:-60,
    //         opacity:.75
    //     },250)
    //     .animate({
    //         top:-80,
    //         right:-75,
    //         opacity:.5
    //     },200)
    //     .animate({
    //         top:-85,
    //         right:-75,
    //         opacity:.5
    //     },150)
    //     .animate({
    //         top:-95,
    //         right:-90,
    //         opacity:0
    //     },100)
    //     .animate({
    //         top:-100,
    //         right:-95,
    //         opacity:0
    //     },200,function(){$(this).css({top:-65,right:-50})});
    // }

    // // $('#comment_sub').click(function(event) {
    // //     /* Act on the event */
    // //     addBangtou(1);
    // // });
});



function plus1(obj, iC) {
    var ps = $(obj).offset();
    var w = $(obj).width();
    var myColor = iC ? '#eb7e00' : '#f70040';
    var dom = $('<span class="num">+1</span>');
    $(obj).css({
        position: 'relative',
        'zIndex': '2'
    })
    $(document.body).append(dom);
    dom.css({
        font: 'bold 20px/22px microsoft yahei',
        color: myColor,
        position: 'absolute',
        left: ps.left + 'px',
        top: (ps.top - 5) + 'px',
        opacity: 0,
        width: w + 'px',
        'textAlign': 'center',
        'zIndex': 1
    });

    dom.animate({
        top: '-=' + 30 + 'px',
        opacity: 1
    }, 500).delay(1000).fadeIn('normal', function() {
        $(this).remove();
    });

}

$.fn.insertRely = function(id) { 
    var dom;
    if(!$('.comment .reply-con').length){
          dom = $('<div id="replycomment" class="reply-con"><div class="tp"><textarea id="reply_content"></textarea></div><div class="sbm"><button onclick="ajaxreply()" class="btn">提交</button></div></div>').hide();
          $('.comment .list-con').after(dom);
    }else{
      dom =$('.comment .reply-con');  
    }
   
     dom.find('textarea').off('click.is').on('click.is',function(){
            $(this).addClass('act');
    })

    $(document).off('click.is').on('click.is',function(){
        dom.hide();
        dom.find('textarea').removeClass('act');
        dom.removeAttr('user');
       
    });

    dom.on('click.is',function(){return false;})

    return this.each(function(){
        $(this).off('click.is').on('click.is',function(){
            dom.css({
                top:$(this).position().top+25+'px',
                position:'absolute',
                'zINdex':'100',
                background:'#fff',
                width:'100%'
            })
            dom.attr('user',id)
            dom.show();
            return false;
        });

    });
}


$.fn.creatShare = function(url, title, summary) {
    var domStr = '<span class="share"><a class="qqzone"></a><a class="tqq"></a><a class="t163"></a><a class="rr"></a><a class="kx"></a><a class="tsina"></a></span>';
    var o = {
        url: function() {
            return encodeURIComponent(url ? url : top.location.href);
        },
        title: function() {
            return encodeURIComponent('我在@帮5买 发现了一条超值折扣，真的很划算，' + title + '（分享自 @帮5买)');
        },
        summary: function() {
            return encodeURIComponent(summary ? summary : '');
        },
        qqzone: function() {
            window.open('http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?url=' + this.url() + '&title=' + this.title() + '&summary' + this.summary())
        },
        tqq: function() {
            window.open('http://share.v.t.qq.com/index.php?c=share&a=index&url=' + this.url() + '&title=' + this.title() + '&summary' + this.summary())
        },
        t163: function() {
            window.open('http://t.163.com/article/user/checkLogin.do?&info=' + this.title() + this.summary() + this.url())
        },
        rr: function() {
            window.open('http://widget.renren.com/dialog/share?resourceUrl=' + this.url() + '&title=' + this.title() + '&description' + this.summary())
        },
        kx: function() {
            window.open("http://www.kaixin001.com/repaste/share.php?rtitle=" + this.title() + "&rurl=" + this.url() + "&rcontent=" + this.summary());
        },
        tsina: function() {
            window.open('http://service.weibo.com/share/share.php?url=' + this.url() + '&title=' + this.title())
        }
    };


    return this.each(function() {

        $(this).append(domStr);
        $(this).find('a').each(function() {
            $(this).on('click', function() {

                o[$(this).attr('class')]();
                // return false;
            })
        })

    });


}



function fixDom(obj) {

    var dom = $(obj),
        h = $(obj).offset().top,
        isIE6 = window.XMLHttpRequest ? false : true;

    if (!dom.length) return false;

    if (!isIE6) {
        $(window).on('scroll.zdm', function() {

            if ($(window).scrollTop() >= h) {
                dom.addClass('nav-fix');
            } else {
                dom.removeClass('nav-fix');
            }
        });
    }
}



function ToSlide(oL, oR, slideDiv) {

    this.oS = $(slideDiv);
    this.list = this.oS.find('li')
    this.l = $(oL);
    this.r = $(oR);
    this.w = this.list.first().outerWidth(true);
    this.index = 0;
    this._init();

}

ToSlide.prototype = {

    _init: function() {

        var _this = this;

        var initedW;
        var chilredSize = this.list.size();
        this.list.last().css({
            'marginRight': '0'
        });

        initedW = chilredSize * this.w - parseInt(this.list.eq(0).css('marginRight'));
        this.oS.children().eq(0).width(initedW);

        this.oS.scrollLeft(0);

        this.l.bind('click', function() {
            _this.toRight(_this.w)
        });
        this.r.bind('click', function() {
            _this.toLeft(_this.w)
        });

    },

    toLeft: function(w) {

        var _this = this;

        if (this.oS.is(':animated')) return;
        if (this.index == this.list.length - 4) return;

        this.index++
        this.l.removeClass('sl_disable');
        if (this.index == this.list.length - 4) $(this).trigger('disable', 1);

        this.oS.stop(true, true).animate({
            'scrollLeft': '+=' + w + 'px'
        }, 'fast');
    },

    toRight: function(w) {

        var _this = this;
        if (this.oS.is(':animated')) return;

        if (this.index == 0) return;
        this.r.removeClass('sl_disable');
        this.index--

        if (this.index == 0) $(this).trigger('disable', 0);

        this.oS.stop(true, true).animate({
            'scrollLeft': '-=' + w + 'px'
        }, 'fast');


    }
}