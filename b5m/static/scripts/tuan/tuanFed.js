var tuanFed = {
    /*城市列表 - 鼠标经过*/
    cityListHover:function(){
        $('.cityList>li','.cityListBx').hover(function(){
            $(this).addClass('cur').siblings().removeClass('cur');
        },function(){
            $(this).removeClass('cur');
        });
    },
    /*搜索页页面加载完成检测分类列表是否展开*/
    checkTypeOpen:function(){
        var typeAs;
        $('li','.searchTypeSlt').each(function(){
            if($('a.sltAll',this).index()>9){
                $('span',this).trigger('click');
            }
        });
    },
    /*分类列表*/
    searchType:function(){
        $('span','.searchTypeSlt').on({
            click:function(){
                var singleElm = $('s',this);
                if(singleElm.hasClass('cur')){
                    $(this).parent().height(23);
                    singleElm.removeClass('cur');
                }else{
                    singleElm.addClass('cur');
                    $(this).parent().height('auto');
                }
                return false;
            }
        });
    },
    /*分类列表-新*/
    classifyHot:function(){
        var _listHeight = 24;
        $('.classify-hot-item').each(function(){
            var _this = $(this);
            _this.removeClass('classify-hot-item-load');
            var pHeight = $(".classify-hot-item-list>p",_this).height();
            if(pHeight>40){
                $('.classify-hot-item-list',_this).attr("h",pHeight);//.height(_listHeight);
                $("<sup></sup>").appendTo(_this);
            }
        });
        $("sup",'.classify-hot-item').on("click",function(){
            var _this = $(this)
                ,listBox = _this.siblings('.classify-hot-item-list')
                ,thisHeight = listBox.attr('h');
            if(!_this.hasClass('cur')){
                listBox.animate({height:thisHeight},240,function(){
                    _this.addClass('cur');
                });
            }else{
                listBox.animate({height:_listHeight},240,function(){
                    _this.removeClass('cur');
                });
            }

        });
    },
    /*详情页Tab*/
    detailTab:function(id){
        var tabId = id||'.tuan-layout-tab';
        $('.tuan-layout-tab-tit>li',tabId).hover(function(){
            var _this = $(this)
                ,thisTabCon = _this.parents('.tuan-layout-tab').find('.tuan-layout-tab-cont');
            _this.addClass('cur').siblings().removeClass('cur');
            thisTabCon.hide();
            thisTabCon.eq(_this.index()).show();
        });
    },
    /*详情拉起展开*/
    detailListSlideUpDown:function(){
        $(".ico-slide-up").click(function(){
            var _this = $(this)
                ,listDom = _this.siblings('.ov')
                ,height = _this.siblings('.ov').children('ul').height();
            if(!_this.hasClass('ico-slide-up-cur')){
                _this.addClass('ico-slide-up-cur').html("拉起<s></s>");
                listDom.animate({height:height},120);
            }else{
                _this.removeClass('ico-slide-up-cur').html("展开<s></s>");
                listDom.animate({height:86},120);
            }
        });
    },
    /*详情页评论*/
    detailComment:function(){
        var comBox = '.J_tuan_comment'
            ,limitNum = 200
            ,comTxt,comNum = 200;
        $("textarea",'.J_tuan_comment').keydown(function(){
            var _this = $(this);
            comTxt = _this.val();
            comNum = limitNum-comTxt.length;
            if(comNum<20){
                $('.sbm_txt_num',comBox).css("color","#f00");
            }else{
                $('.sbm_txt_num',comBox).removeAttr('style');
            }
            if(comNum<0){
                comTxt = comTxt.substr(0,limitNum);
                _this.val(comTxt);
                comNum = limitNum;
                $('.sbm_txt_num',comBox).text(0);
                return;
            }
            $('.sbm_txt_num',comBox).text(comNum);
        });
        $("textarea",'.J_tuan_comment').focus(function(){
            var $this = $(this)
                ,thisMsg = $(this).attr("default").replace(/\s+/gi,"")
                ,thisVal = $this.val().replace(/\s+/gi,"");
            if(thisVal==""||thisVal==thisMsg){
                $this.val("").css("color","#333");
                $('.sbm_txt_num',comBox).text(limitNum);
            }
        });
        $("textarea",'.J_tuan_comment').blur(function(){
            var $this = $(this)
                ,thisMsg = $(this).attr("default")
                ,thisVal = $this.val().replace(/\s+/gi,"");
            if(thisVal==""||thisVal==thisMsg.replace(/\s+/gi,"")){
                $this.val(thisMsg).css("color","#999");
                $('.sbm_txt_num',comBox).text(limitNum);
            }
        }).trigger("blur");
        $('.btn-tuan-scomment',comBox).click(function(){
            if(typeof window['detailCommentCallback']!="undefined"){
                window['detailCommentCallback']($("textarea",'.J_tuan_comment').val());
            }
        });
    },
    prodItemHover:function(){
        $(".mod-proItem").hover(function(){
            $(this).addClass("mod-proItem-cur");
        },function(){
            $(this).removeClass("mod-proItem-cur");
        });
    },
    /**
     * Scroll滚动
     * @param args(json){
     *  box:(str{class||id}，默认:.js_tab) Scroll Box选择
     *  nav:(boolen{true|false},默认:true) Scroll 是否有导航
     *  auto:(boolen{true|false},默认:true) Scroll 是否自动滚动
     *  time:(init,默认：4) Scroll 自动滚动时间间隔
     *  speed:(init,默认：180) Scroll 滚动动画执行时间
     * }
     * @returns {boolean}
     */
    scroll:function(args){
        var _Args = $.extend({},{nav:true,auto:true,time:4,speed:180,box:".layout-scroll"},args||{})
            ,_autoScroll = true;
        /*左滚动画*/
        var scrollLeft = function(){
            var scrollBox = $('.layout-scroll-cont>ul',_Args['box'])
                ,scrollDom = scrollBox.children(":first")
                ,scrollWidth = scrollDom.outerWidth(true);
            scrollBox.animate({marginLeft:"-"+scrollWidth},_Args['speed'],function(){
                scrollDom.appendTo(scrollBox).parent().css("marginLeft",0);
            });
        };
        /*有滚动画*/
        var scrollRight = function(){
            var scrollBox = $('.layout-scroll-cont>ul',_Args['box'])
                ,scrollDom = scrollBox.children(":last")
                ,scrollWidth = scrollDom.outerWidth(true);
            scrollBox.css("marginLeft","-"+scrollWidth+"px").prepend(scrollDom).animate({marginLeft:0},_Args['speed']);
        };
        if(_Args['nav']){
            $(_Args['box']).on({
                "click":function(){
                    var $this = $(this);
                    if($this.hasClass('layout-scroll-prev')){
                        scrollLeft();
                    }else{
                        scrollRight();
                    }
                },
                mouseover:function(){
                    clearInterval(_autoScroll);
                }
            },".layout-scroll-prev,.layout-scroll-next");
        };
        /*自动滚动*/
        if(!_Args['auto'])return false;
        $(_Args['box']).hover(function(){
            clearInterval(_autoScroll);
            return false;
        },function(){
            _autoScroll = setInterval(function(){
                scrollLeft();
            },_Args['time']*1000);
        }).trigger("mouseout");
    },
    /*首页Tab切换列表*/
    indexTab:function(){
        $('.index-tab-nav>span','.J_indexTab').hover(function(){
            var _this = $(this)
                ,tabContDom = $('.index-tab-content', _this.parents('.J_indexTab'))
                ,tabData = {};
            if(_this.hasClass('cur')){
                return false;
            }
            _this.addClass('cur').siblings().removeClass('cur');
            tabContDom.eq(_this.index()).show().siblings('.index-tab-content').hide();
            tabData['cindex'] = _this.parents('.index-floor1').index();
            tabData['txt'] = _this.text();
            tabData['index'] = _this.index();
            if(typeof indexTabCallBack!="undefined"){
                indexTabCallBack(tabData);
            }
        });
    },
    /*Tab切换*/
    Tab:function(args){
        var Args = $.extend({},{id:".tabBox",outhide:true,not:null},args)
            ,tabNav = $(Args['id']+" > .tab-nav").children()
            ,tabCont = $(Args['id']+"> .tab-cont");
        if(Args.not){
            tabNav = tabNav.not(Args.no);
        }
        tabNav.hover(function(){
            var $this = $(this);
            $this.addClass('tabCur').siblings().removeClass('tabCur');
            tabCont.hide().eq($this.index()).show();
        },function(){
                if(Args['outhide'])$(this).removeClass('tabCur');
        });
        $(Args['id']).hover(function(){
        },function(){
            if(Args['outhide'])tabCont.hide();
        });
        return false;
    },
    indexLoad:function(){
        this.searchType();
        this.indexTab();
        this.classifyHot();
        this.prodItemHover();
        this.scroll();
    },
    /*团购搜索列表执行*/
    searchNoResultLoad:function(){
        this.prodItemHover();
    },
    /*团购搜索列表执行*/
    searchLoad:function(){
       this.classifyHot();
   //     this.checkTypeOpen();
        this.prodItemHover();
    },
    /*城市列表加载执行*/
    cityListLoad:function(){
        this.cityListHover();
        $(".cityListTit>span").hover(function(){
            $(this).addClass("cur").siblings().removeClass('cur');
            $(".cityList").hide().eq($(this).index()).show();
        });
    },
    /*详情加载执行*/
    detailLoad:function(){
        this.prodItemHover();
        this.detailListSlideUpDown();
        this.detailComment();
        $('.tuan-layout-comments .reply').each(function(i){
            $(this).insertRely(i);
        });
        $("#tuan-layout-comments_bt").click(function(){
            $("html,body").animate({scrollTop:$("#tuan-layout-comments").offset().top-20},240);
            return false;
        });
       // this.detailTab();
    },
    /*地图加载执行*/
    mapLoad:function(){
        var _this = this;
        _this.Tab({id:".J_map-navTabbx"});
        _this.Tab({id:".J_map-navallcityTab",outhide:false});
        //_this.mapLayout();
    }
};

$.fn.insertRely = function(id) {
    var dom;
    if(!$('.tuan-layout-comments .reply-con').length){
        dom = $('<div id="replycomment" class="reply-con"><div class="tp"><textarea id="reply_content"></textarea></div><div class="sbm"><button onclick="ajaxreply()" class="btn">提交</button></div></div>').hide();
        $('.tuan-layout-comments .list-con').after(dom);
    }else{
        dom =$('.tuan-layout-comments .reply-con');
    }

    dom.find('textarea').off('click.is').on('click.is',function(){
        $(this).addClass('act');
    })

    $(document).off('click.is').on('click.is',function(){
        dom.hide();
        dom.find('textarea').removeClass('act');
        dom.removeAttr('user');

    });
    dom.on('click.is',function(){return false;})
    return this.each(function(){
        $(this).off('click.is').on('click.is',function(){
            dom.css({
                top:$(this).position().top+25+'px',
                position:'absolute',
                'zINdex':'100',
                background:'#fff'
            })
            dom.attr('user',id)
            dom.show();
            return false;
        });

    });
};


var tuanData = {
	dom :$('#tuan-search-params') ,
	setParam : function(name,value){
		this.dom.find('input#'+name).val(value);
	},
	params : function(){
		var _params = {};
		this.dom.find('input').each(function(){
			_params[this.name] = this.value;
		});
		return _params;
	},
	fetch: function(properties){
		var _params = this.params();
		if(!properties){
			properties = ['products'];
		}else {
			properties.push('products');
		}

		_params.properties = properties.join(',');
		$.ajax(
			{
				url:'/dituData',
				type:'get',
				dataType:'json',
				data : _params,
				success : function(data){
					if(data){
						if(data.categories){
							$('#tuan-category-filter').empty().append(data.categories);
						}

						if(data.prices){
							$('#tuan-price-filter').empty().append(data.prices);
						}

						if(data.products){
							$('#tuan-product-list').empty().append(data.products);
							tuanMap.refreshMap();
						}

						if(data.districts){
							$('#tuan-district-filter').empty().append(data.districts);
						}

						if(data.merchantAreas){
							$('#tuan-merchant-area-filter').empty().append(data.merchantAreas);
						}

						tuanMap.getWinHeight();
					}


				}
			}
		);
	}
};

var tuanPage = {
	total : function(){
		var pageList = $('#tuan-list-page');
		var pages= pageList.find('span');


		var n = parseInt(pages.eq(pages.length-2).html());
		return n;
	},
	current : function(){
		var n = parseInt($('#tuan-list-page').find('span.cur:first').html());
		return n;
	},
	go : function(num){
		num = parseInt(num);
		if(num > 0){
			tuanData.setParam('page',num);
			tuanData.fetch();
		}
	},
	prev : function(){
		var current = this.current();
		if(current != 1){
			this.go(current-1);
		}
	},
	next : function(){
		var current = this.current();
		var total = this.total();
		if(current != total){
			this.go(current+1);
		}
	}
};


var tuanOrder =  {
	init : function(){
		var dom = $('#tuan-order-list');
		var _this = this;
		dom.find('span').bind('click',function(){
			var currentDom = dom.find('span.cur:first');
			var element = $(this);

			var orderStr = '';
			var orderName = element.attr('name');

			if(orderName != 'Default'){
				orderStr = _this.getOrderStr(element,orderName,currentDom.attr('name') == orderName);
			}

			currentDom.removeClass('cur');
			element.addClass('cur');
			tuanData.setParam('page',1);
			tuanData.setParam('order',orderStr);

			tuanData.fetch();
		});
	},
	getOrderStr : function(orderDom,orderName,change){
		var iconDom = orderDom.find('s:first');
		var direction = '';
		if(iconDom.hasClass('ico_up')){
			if(change){
				iconDom.removeClass('ico_up').addClass('ico_down');
				direction = 'DESC';
			}else {
				direction = 'ASC';
			}
		}else if(iconDom.hasClass('ico_down')){
			if(change){
				iconDom.removeClass('ico_down').addClass('ico_up');
				direction = 'ASC';
			}else {
				direction = 'DESC';
			}
		}

		return orderName + '_' + direction;
	}
};

var tuanFilter = {
	init : function(){
		this._initCity();
	},
	filterCategory:function(e){
		var categoryName = $(e).attr('title');

		if($('#tuan-category-filter').find('span.sltCur:first').length > 0){
			$('#tuan-category-filter').find('span.sltCur:first').removeClass('sltCur');
		}
		if(categoryName != '全部'){
			$(e).addClass('sltCur');
		}


		var categoryStr = categoryName != '全部' ? categoryName : '';

		tuanData.setParam('page',1);
		tuanData.setParam('category',categoryStr);
		tuanData.fetch(['prices']);
	},
	filterPrice:function(e){
		var priceName = $(e).attr('title');
		if(priceName == '1000以上'){
			priceName = '1000-99999999';
		}

		if($('#tuan-price-filter').find('span.sltCur:first').length > 0){
			$('#tuan-price-filter').find('span.sltCur:first').removeClass('sltCur');
		}
		if(priceName != '全部'){
			$(e).addClass('sltCur');
		}


		var priceStr = priceName != '全部' ? priceName : '';

		tuanData.setParam('page',1);
		tuanData.setParam('price',priceStr);
		tuanData.fetch();
	},
	_initCity : function(){
		var callback = function(){
			var city = $(this).html();

			$('#selected-city').empty().append(city+'<u></u>');
			$('#selected-district').empty().append('选择城区<u></u>');
			$('#selected-merchant-area').empty().append('选择商圈<u></u>');

			$('#tuan-category-filter').empty();
			$('#tuan-merchant-area-filter').empty();



			tuanData.setParam('page',1);
			tuanData.setParam('price','');
			tuanData.setParam('category','');
			tuanData.setParam('order','');
			tuanData.setParam('area','');
			tuanData.setParam('city',city);
			tuanData.fetch(['categories','prices','districts']);
		};
		$('ul.map-province-hotcity').find('li').bind('click',callback);
		$('ul.letter-cities').find('li').bind('click',callback);
	},
	filterDistrict : function(e){
		var area = $(e).html();

		$('#selected-district').empty().append(area+'<u></u>');
		$('#selected-merchant-area').empty().append('选择商圈<u></u>');

		$('#tuan-merchant-area-filter').empty();

		tuanData.setParam('page',1);
		tuanData.setParam('price','');
		tuanData.setParam('category','');
		tuanData.setParam('order','');
		tuanData.setParam('area',area);
		tuanData.fetch(['categories','prices','merchantAreas']);
	},
	filterMerchantArea:function(e){
		var merchantArea = $(e).html();

		$('#selected-merchant-area').empty().append(merchantArea + '<u></u>');

		tuanData.setParam('page',1);
		tuanData.setParam('price','');
		tuanData.setParam('category','');
		tuanData.setParam('order','');
		tuanData.setParam('merchantArea',merchantArea);
		tuanData.fetch(['categories','prices']);
	}
};
