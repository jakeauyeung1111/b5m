
var map = null;
var tuanMap = {
	markers : [],
	infoWindows : [],
	getWinHeight : function(){
		var  winHeight = 0
			,mapBx = document.getElementById("map-box")
			,sideListContBox = document.getElementById('tuan-list-cont')
			,mapHeight;
		if (window.innerWidth){
			winHeight = window.innerHeight;
		} else if ((document.body) && (document.body.clientWidth)){
			winHeight = document.body.clientHeight;
		}
		if (document.documentElement  && document.documentElement.clientHeight && document.documentElement.clientWidth){
			winHeight = document.documentElement.clientHeight;
		}
		mapHeight = winHeight-mapBx.offsetTop-1;
		mapBx.style.height = mapHeight+"px";
		mapBx.children[0].style.height = mapHeight+"px";
		sideListContBox.style.height = mapHeight-sideListContBox.offsetTop+"px";
	},

	getMapData : function(){
		var mapData = [];

		var ico = "http://staticcdn.b5m.com/images/tuan/map_ico";


		$('#tuan-list-cont').find('dd').each(function(i){
			var _dom = $(this);
			var a = _dom.find('a.tit:first');
			var lat_lon = _dom.find('span.lon-lat:first').text(); 
			if(lat_lon){
				lat_lon = $.trim(lat_lon);
			}
			var o = {
				point : lat_lon,
				title : a.text(),
				pic : _dom.find('span.pic:first').text(),
				ico : ico + (i+1) + '.png',
				link : a.attr('href'),
				dlink : a.attr('href'),
				price : _dom.find('b.txt-red:first').text(),
				oprice : _dom.find('b.oprice:first').text()
			};

			mapData.push(o);
		});


		return mapData;
	},
	createPoint : function(pointStr){
		if(pointStr){
			var _point = pointStr.split(",")//经度;
			return new BMap.Point(_point[0],_point[1]);
		}
	},
	centerMap : function(){
		/*
		var point = {lng:"116.404",lat:"39.915"};
		map.centerAndZoom(new BMap.Point(point.lng,point.lat),12);
		*/

		var points = [];

		$.each(this.markers,function(i,m){
			if(m){
				points.push(m.getPosition());
			}
		});


		if(points && points.length > 0){
			var port = map.getViewport(points);
			map.centerAndZoom(port.center,port.zoom);
		}

	},
	addMarker : function(markerInfo){
		var _this = this;
		if(markerInfo && markerInfo['point']){
			var _point = this.createPoint(markerInfo['point']);
			var _icon = new BMap.Icon(markerInfo['ico'], new BMap.Size(25,33),{infoWindowAnchor:new BMap.Size(12,0) });
			var _marker = new BMap.Marker(_point,{ icon:_icon});
			map.addOverlay(_marker);
			//添加信息窗口
			var openInfoWindow = function(){
				var _win = _this.createInfoWin(markerInfo);
				this.openInfoWindow(_win);
			}

			var _win = _this.createInfoWin(markerInfo);
			_marker.addEventListener("click",function(){
				
				
				this.openInfoWindow(_win);
			});
			this.infoWindows.push(_win);
			this.markers.push(_marker);
		}else {
			this.markers.push(null);
			this.infoWindows.push(null);
		}
	},
	removeMarkers : function(){
		map.clearOverlays();
		this.markers.splice(0,this.markers.length);
		this.infoWindows.splice(0,this.infoWindows.length);
	},
	createInfoWin : function(info){
		var	html = "<div class='map-proPopWind'>" + 
						"<a href='" + info['link'] + "' class='pic' target='_blank'>" + 
							"<img src='" + info['pic'] + "'>"+
						"</a>" +
						"<div class='titbox'>" + 
							"<a class='tit' href='" + info['link'] + "' target='_blank'>" + 
								info['title'] + 
							"</a>" + 
							'<a href="'+ info["dlink"] + '" class="link_detail" target="_blank">详情 ></a>'+
						'</div>'+
						'<div class="price">'+
							'价格：<s>¥</s><b class="txt-red">'+ info["price"] + '</b>   '+
							'原价：<s>¥</s><b>'+ info["oprice"] + '</b>'+
						'</div>'+
					'</div>';
		return new BMap.InfoWindow(html);
	},
	refreshMap : function(){
		var _this = this;
		this.removeMarkers();
		var mapData = this.getMapData();

		//var openInfoWinFuns = [];
		//添加信息点
		for(var i=0;i<mapData.length;i++){//添加标注点
			this.addMarker(mapData[i]);
			//openInfoWinFuns.push(_marker);
		}

		$("dd>ins","#tuan-list-cont").click(function(){
			var thisIndex = $("dd>ins","#tuan-list-cont").index(this);
			var marker = _this.markers[thisIndex];
			var win = _this.infoWindows[thisIndex];
			if(marker && win){
				marker.openInfoWindow(win);
			}
		});

		this.centerMap();
	},
	init : function(){
		this.getWinHeight();
		window.onresize = this.getWinHeight;
		map = new BMap.Map("map-container");//获取地图容器
		map.enableScrollWheelZoom();// 添加滚轮缩放
		map.addControl(new BMap.NavigationControl({anchor:BMAP_ANCHOR_BOTTOM_LEFT,type:BMAP_NAVIGATION_CONTROL_LARGE}));//添加默认缩放平移控件


		this.refreshMap();
	}
};


