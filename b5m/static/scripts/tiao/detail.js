$(function() {

    //点击喜欢
    var likeClick = 0,likePanel = $('.like-panel'),isLikePlay = false,likeCount =   $('em.like').find('strong');
    $('.btn-like').click(function() {

        if(isLikePlay) {
            return;
        }

        isLikePlay = true;

        if(likeClick==1) {
            likePanel.find('.haslike').addClass('on');
            setTimeout(function() {
                likePanel.find('.haslike').removeClass('on');
                isLikePlay = false;
            },3000);
        }else {
            likePanel.find('.actionlike').addClass('on').append('<img src="../../images/tiao/like.gif?'+Math.random()+'"/>');
            likeCount.text(Number(likeCount.text())+1);
            likeClick=1;
            setTimeout(function() {
                likePanel.find('.actionlike').removeClass('on');
                isLikePlay = false;
            },3000);
        }
    });



    //评论
    (function(comment) {

        var commentList = $('#J_commentList ul');

        //初始化
        comment.init = function() {

            //说点什么吧
            var commentText = $('#commentText'),commentLab = $('.lab'),commentBtn = $('.startsub'),yy=$('.yy'),maxCommentLen = 140;
            commentText.textlimit(yy,maxCommentLen);
            commentText.val('').blur().on({
                focusin:function() {
                    if($.trim(this.value)=='') {
                        commentLab.hide();
                    }
                },
                focusout:function() {
                    if($.trim(this.value)=='') {
                        commentLab.show();
                    }
                }
            });
            commentBtn.click(function() {
                if($.trim(commentText.val())=='') {
                    alert('内容不能为空！');
                    return false;
                }
                comment.addComment(function() {
                    commentText.val('');
                    yy.text(maxCommentLen);
                });
            });


            //回复评论
            var replyBox = $('.reply-box'),replyText = replyBox.find('textarea');
            commentList.delegate('.reply a','click',function() {
                if($(this).attr('data-show')=='true') {
                    replyBox.hide();
                    $(this).attr('data-show','false');
                }else {
                    replyBox.show().insertAfter($(this).parent());
                    var val ='回复@：'+replyBox.parents('.text').find('.name').text() +":";
                    replyText.val('').focus().val(val);
                    $('a[data-show=true]',commentList).attr('data-show','false');
                    $(this).attr('data-show','true');

                }
            });
            replyBox.find('a').click(function() {
                comment.addComment(function() {
                    replyText.val('');
                    replyBox.hide();
                    $('a[data-show=true]',commentList).attr('data-show','false');
                });
            });

            //查看所有评论
            $('#J_commentList .more a').click(function() {
                //ajax
                commentList.append('<li class="cf"><div class="head"><img src="http://tiao.b5m.com/sns/images/nophoto.jpg" width="50" height="50" alt=""/></div><div class="text"><span class="name">aaaaa</span><em class="date">今天 14:03</em><p class="content">newnew</p><div class="reply"><a href="javascript:void(0)">回复</a></div></div></li><li class="cf"><div class="head"><img src="http://tiao.b5m.com/sns/images/nophoto.jpg" width="50" height="50" alt=""/></div><div class="text"><span class="name">bbbbb</span><em class="date">今天 14:03</em><p class="content">newnew</p><div class="reply"><a href="javascript:void(0)">回复</a></div></div></li><li class="cf"><div class="head"><img src="http://tiao.b5m.com/sns/images/nophoto.jpg" width="50" height="50" alt=""/></div><div class="text"><span class="name">ccccc</span><em class="date">今天 14:03</em><p class="content">newnew</p><div class="reply"><a href="javascript:void(0)">回复</a></div></div></li>');
            });

        };


        //添加评论
        comment.addComment = function(callback) {
            //ajax:  commentText.val()
            //ajax callback:
            //<li class="cf"><div class="head"><img src="http://tiao.b5m.com/sns/images/nophoto.jpg" width="50" height="50" alt=""/></div><div class="text"><span class="name">匿名</span><em class="date">今天 14:03</em><p class="content">老子老子</p><div class="reply"><a href="javascript:void(0)">回复</a></div></div></li>
            commentList.prepend('<li class="cf"><div class="head"><img src="http://tiao.b5m.com/sns/images/nophoto.jpg" width="50" height="50" alt=""/></div><div class="text"><span class="name">匿名</span><em class="date">今天 14:03</em><p class="content">newnew</p><div class="reply"><a href="javascript:void(0)">回复</a></div></div></li>');
            callback();
        };

        comment.init();

    })(window.detailComment = window.detailComment || {});

});



//最多可输入多少字符
$.fn.textlimit=function(counter_el, thelimit, speed) {
    var charDelSpeed = speed || 15;
    var toggleCharDel = speed != -1;
    var toggleTrim = true;
    var that = this[0];
    var isCtrl = false;

    function updateCounter(){
        if(typeof that == "object")
            $(counter_el).text(thelimit - that.value.length);
    };

    this.keydown (function(e){
        if(e.which == 17) isCtrl = true;
        var ctrl_a = (e.which == 65 && isCtrl == true) ? true : false;
        var ctrl_v = (e.which == 86 && isCtrl == true) ? true : false;

        if( this.value.length >= thelimit && e.which != '8' && e.which != '46' && ctrl_a == false && ctrl_v == false)
            e.preventDefault();
    })
        .keyup (function(e){
        updateCounter();
        if(e.which == 17)
            isCtrl=false;

        if( this.value.length >= thelimit && toggleTrim ){
            if(toggleCharDel){
                if ( (this.value.length - thelimit) > 10 )
                    that.value = that.value.substr(0,thelimit+100);
                var init = setInterval
                (
                    function(){
                        if( that.value.length <= thelimit ){
                            init = clearInterval(init); updateCounter()
                        }
                        else{
                            that.value = that.value.substring(0,that.value.length-1); $(counter_el).text(thelimit - that.value.length);
                        }
                    } ,charDelSpeed
                );
            }
            else this.value = that.value.substr(0,thelimit);
        }
    });

};