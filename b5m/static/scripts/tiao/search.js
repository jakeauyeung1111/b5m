(function(water) {

  var container = $('.water');

  water.init = function() {


      //生成瀑布图
      container.waterfall({
          getResource:function(index,render){
              setTimeout(function() {
                  var html = '';
                  $('.cell').filter(function() {
                      return $('.item',this)[0];
                  }).each(function(i,n) {
                          if(i<8) {
                              html += '<div class="cell">'+ n.innerHTML +'</div>';
                          }
                      });
                  render($(html));
              },1000);

          },
          auto_imgHeight:true,
         insert_type:1
      });


      //绑定事件
      container.on('mouseover','.info a',this.handlers.enterHanler);  //浏览喜欢等显示提示事件
      container.on('mouseout','.info a',this.handlers.leaveHandler);

      container.on('click','.likes',this.handlers.likeHandler);  //点击喜欢事件

      container.on('click','.dises',this.handlers.disHandler);  //点击评论事件

      container.on('focus','.comment .field',this.handlers.commentFocusHandler);  //评论框事件
      container.on('blur','.comment .field',this.handlers.commentBlurHandler);
      container.on('keypress','.comment .field',this.handlers.commentEnterHandler);

      container.on('click','.comment a',this.handlers.commentSubmit);

  };


  water.handlers = {

      enterHanler:function() {
          $(this).addClass('hover');
      },

      leaveHandler:function() {
          $(this).removeClass('hover');
      },

      commentFocusHandler:function() {
          $(this).parent().addClass('hover');
      },

      commentBlurHandler:function() {
          $(this).parent().removeClass('hover');
      },

      //点击喜欢
      likeHandler:function(e) {

              var $this = $(this);
              if($this.data('play'))  return;

              $this.data('play',true);

              if($this.data('click')) {
                  $this.find('.haslike').addClass('on');
                  setTimeout(function() {
                      $this.data('play',false).find('.haslike').removeClass('on');
                  },3000);
              }else {
                  $this.data('click',true).find('.actionlike').addClass('on').append('<img src="../../images/tiao/like.gif?'+Math.random()+'"/>');
                  $this.find('b').text(Number($this.find('b').text())+1);
                  setTimeout(function() {
                      $this.data('play',false).find('.actionlike').removeClass('on');
                  },3000);
              }

      },
      //点击评论
      disHandler:function(e) {
          var target = $(this).parents('.item').find('.comment');
          if(target.data('click')===1) {
              target.data('click',0).hide();
          }else {
              target.data('click',1).show().find('.field').val('').focus();
          }
      },
      //提交评论
      commentSubmit:function(e) {
          var parent = $(this).parents('.item'),
              field = $(this).prev(),
              msg =parent.find('.msg'),
              num = parent.find('.dises b');
          if($.trim(field.val())=='') {
              alert('写点东西吧，评论内容不能为空哦！');
              field.focus();
              return;
          }

          //ajax
          setTimeout(function() {
              msg.prepend('<li class="cf"><a href="#"><img alt="" src="http://tiao.b5m.com/sns/images/nophoto.jpg"></a><p><a href="#">abc</a> '+field.val()+'</p></li>');
              num.text(Number(num.text() || 0) +1 );
              water.handlers.disHandler.call(field);
          },200);

      },
      //回车提交评论
      commentEnterHandler:function(e) {
        if(e.which==13) {
            water.handlers.commentSubmit.call($(this).next());
        }
      }

  }


  water.init();

})(window.water = window.water || {});