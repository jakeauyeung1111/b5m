(function($) {

    //slider分页 和 切换
    var $sliderList = $('.slider');
    $sliderList.length && $('.slider').each(function() {
        var $slider = $(this);
        var $sliderNav = $slider.find('.slider-nav > .slider-nav-li');
        window.NS.publics.tab($sliderNav, $slider.find('.slider-item'), 'cur', 'slider-item-cur', $slider.find('>.slider-prev'), $slider.find('>.slider-next'), function(pageHTML) {
            $slider.find('.slider-page').html(pageHTML);
        });
        $slider.find('.slider-page').on('click', 'a[class!=more]', function(e) {
            e.preventDefault();
            var pageCont = $slider.find('.slider-item:visible');
            var pageHeight = pageCont.innerHeight();
            if (!$(this).hasClass('cur')) {
                var page = ($(this).html() | 0) - 1;
                pageCont.animate({
                    scrollTop: page * pageHeight
                }, 300);
                $(this).addClass('cur').siblings('.cur').removeClass('cur');
            }
        });
    });


    var bindHoverFix = function(elements) {
        if (elements.length) {
            elements.each(function() {
                var className = $(this).attr('data-hover');
                $(this).on('mouseenter.hover', function() {
                    $(this).addClass(className);
                }).on('mouseleave.hover', function() {
                        $(this).removeClass(className);
                    });
            });
        }
    };
    $('[data-hover]').each(function() {
        bindHoverFix($(this));
    });
    bindHoverFix($('[data-hover]'));
    return window.ATTACH_HOVER = bindHoverFix;


})(jQuery);