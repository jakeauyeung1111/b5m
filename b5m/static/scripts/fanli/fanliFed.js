/**
 *Document:返利网
 *Creat:13-11-4 下午2:20
 *Author:junli.shen@b5m.com
 */
;var fanliFed = {
    /**
     * Tab切换展示
     * @param args (json){
     *  box:(str{class||id}，默认：.js_tab) Tab Box选择
     *  not:(str{class||id||tag}，默认：false) Tab 切换菜单".js_tab_nav"Box中非菜单DOM
     *  navClass(str，默认：cur) Tab 切换菜单选中样式
     *  event(str{event},默认：mouseover) Tab切换展示激活事件
     *  callback(fun，默认：false) Tab切换后执行动画
     * }
     * */
    tabShow:function(args){
        var _Args = $.extend({},{event:"mouseover",navClass:"cur",box:".js_tab",callBack:false,not:false},args||{})
            ,tabNav = !_Args['not']||_Args['not'].replace(/\s*/gi,"")==""?$(".js_tab_nav",_Args['box']).children():$(".js_tab_nav",_Args['box']).children(":not("+_Args['not']+")");
        tabNav.on(_Args['event'],function(){
            var $this = $(this)
                ,_thisIndex = tabNav.index(this);
            $this.addClass(_Args['navClass']).siblings().removeClass(_Args['navClass']);
            $(".js_tab_cont",_Args['box']).hide().eq(_thisIndex).show();
            if(_Args['callBack']!=="false")_Args['callBack'](_thisIndex);
            return false;
        });
    },
    /**
     * Scroll滚动
     * @param args(json){
     *  box:(str{class||id}，默认:.js_tab) Scroll Box选择
     *  nav:(boolen{true|false},默认:true) Scroll 是否有导航
     *  auto:(boolen{true|false},默认:true) Scroll 是否自动滚动
     *  time:(init,默认：4) Scroll 自动滚动时间间隔
     *  speed:(init,默认：180) Scroll 滚动动画执行时间
     * }
     * @returns {boolean}
     */
    scroll:function(args){
        var _Args = $.extend({},{nav:true,auto:true,time:4,speed:180,box:".layout-scroll"},args||{})
            ,_autoScroll = true;
        /*左滚动画*/
        var scrollLeft = function(){
           var scrollBox = $('.layout-scroll-box>ul',_Args['box'])
               ,scrollDom = scrollBox.children(":first")
               ,scrollWidth = scrollDom.outerWidth(true);
            scrollBox.animate({marginLeft:"-"+scrollWidth},_Args['speed'],function(){
                scrollDom.appendTo(scrollBox).parent().css("marginLeft",0);
            });
        };
        /*有滚动画*/
        var scrollRight = function(){
            var scrollBox = $('.layout-scroll-box>ul',_Args['box'])
                ,scrollDom = scrollBox.children(":last")
                ,scrollWidth = scrollDom.outerWidth(true);
            scrollBox.css("marginLeft","-"+scrollWidth+"px").prepend(scrollDom).animate({marginLeft:0},_Args['speed']);
        };
        if(_Args['nav']){
            $(_Args['box']).on({
                "click":function(){
                    var $this = $(this);
                    if($this.hasClass('layout-scroll-prev')){
                        scrollLeft();
                    }else{
                        scrollRight();
                    }
                    return false;
                },
                mouseover:function(){
                    clearInterval(_autoScroll);
                }
            },".layout-scroll-prev,.layout-scroll-next").click(function(){
                return false;
            });
        };
        /*自动滚动*/
        if(!_Args['auto'])return false;
        $(_Args['box']).hover(function(){
            clearInterval(_autoScroll);
            return false;
        },function(){
            _autoScroll = setInterval(function(){
                scrollLeft();
            },_Args['time']*1000);
        }).trigger("mouseout");
    },

    /**
     * 类目鼠标经过（www复制）
     */
    menu:function(){
        var $menu = $('#J_menu'),
            $trigger_menu = $menu.children('li'),
            $subMenu = $menu.find('.sub_item'),
            w_len = $menu.find('.sub-wp').outerWidth(true);
        $menu.hover(function(){
            $subMenu.stop(true,true).animate({display:'block',width:w_len},{duration:300})
        },function(){
            $subMenu.stop(true,true).animate({display:'none',width:0},{duration:200})
        });
        $trigger_menu.hover(function(){
            var $this = $(this),
                $subContent = $this.find('.sub-in'),
                $subMenuRel = $this.find('.sub_item');
            $subMenu.hide();
            $subContent.show();
            $subMenuRel.show();
            $this.addClass('open');
            $subMenuRel.find('img').trigger('lazyload');
        },function(){
            var $this = $(this),
                $subMenuRel = $this.find('.sub_item'),
                $subContent = $this.find('.sub-in');
            $this.removeClass('open');
            $subMenuRel.hide();
            $subContent.hide();
        });
    },
    /*详情提示弹窗*/
    detailMsg:function(){
        $('.detail_info_detail_bt').click(function(){
            $(".detail_info_detail").show();
        });
        $(".ico_close",".detail_info_detail").click(function(){
            $(".detail_info_detail").hide();
        });
    },
    /*详情页评论*/
    comments:function(){
        var comBox = '.J_comment'
            ,limitNum = 200
            ,comTxt,comNum = 200;
        $("textarea",'.J_comment').keydown(function(){
            var _this = $(this);
            comTxt = _this.val();
            comNum = limitNum-comTxt.length;
            if(comNum<20){
                $('.sbm_txt_num',comBox).css("color","#f00");
            }else{
                $('.sbm_txt_num',comBox).removeAttr('style');
            }
            if(comNum<0){
                comTxt = comTxt.substr(0,limitNum);
                _this.val(comTxt);
                comNum = limitNum;
                $('.sbm_txt_num',comBox).text(0);
                return;
            }
            $('.sbm_txt_num',comBox).text(comNum);
        });
        $('.bt-comment',comBox).click(function(){
            if(typeof window['detailCommentCallback']!="undefined"){
                window['detailCommentCallback']($("textarea",'.J_comment').val());
            }
        });

    },
    /*详情产品图片切换*/
    picTabShow:function(args){
        var _Args = $.extend({},{event:"click",big:".js_pictabshow_big",nav:".js_pictabshow_nav",box:".js_pictabshow"},args);
        $(_Args['nav'],_Args['box']).find("img").on(_Args['event'],function(){
            var $this = $(this);
            $this.parent().addClass("cur").siblings().removeClass('cur');
            $(_Args['big'],_Args['box']).find("img").attr("src",$(this).attr("pic"));
        });
    },
    indexLoadFun:function(){
        var _this = this;
        _this.menu();
        _this.scroll();
        _this.tabShow({not:"a",callBack:function(i){}});
    },
    detailLoadFun:function(){
        var _this = this;
        _this.picTabShow();
        _this.comments();
        $('.fanli-layout-comments .reply').each(function(i){
            $(this).insertRely(i);
        });
        /*详情提示*/
        _this.detailMsg();
    }
};
$.fn.insertRely = function(id) {
    var dom;
    if(!$('.fanli-layout-comments .reply-con').length){
        dom = $('<div id="replycomment" class="reply-con"><div class="tp"><textarea id="reply_content"></textarea></div><div class="sbm"><button onclick="ajaxreply()" class="btn">提交</button></div></div>').hide();
        $('.fanli-layout-comments .list-con').after(dom);
    }else{
        dom =$('.fanli-layout-comments .reply-con');
    }

    dom.find('textarea').off('click.is').on('click.is',function(){
        $(this).addClass('act');
    })

    $(document).off('click.is').on('click.is',function(){
        dom.hide();
        dom.find('textarea').removeClass('act');
        dom.removeAttr('user');

    });
    dom.on('click.is',function(){return false;})
    return this.each(function(){
        $(this).off('click.is').on('click.is',function(){
            dom.css({
                top:$(this).position().top+25+'px',
                position:'absolute',
                'zINdex':'100',
                background:'#fff'
            })
            dom.attr('user',id)
            dom.show();
            return false;
        });

    });
}