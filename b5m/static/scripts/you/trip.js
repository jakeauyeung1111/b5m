(function($){


    $(function(){
        //autoFill
        $('.trip-search-box').find('.J_autofill').autoFill('', 'tourp');
        $('.hotel-search-all').find('.J_autofill').autoFill('', 'hotel');
        //城市列表弹出框
        var trip_search_place = $('.trip-search-place'),
            layout_cityCountryPop = $('.layout-cityCountryPop'),
            selectFromCity = parseInt($('#selectFromCity').val());
        trip_search_place.each(function(){
            var $this = $(this);
            $this.click(function(e){
                if ($this.siblings().find('.layout-cityCountryPop').length > 0) {
                    $this.siblings().find('.layout-cityCountryPop').remove();
                }
                e.stopPropagation();
                var city = $this.find('.layout-cityCountryPop');
                if (city.length < 1) {
                    layout_cityCountryPop.clone(true).appendTo($this).css({
                        display: 'block',
                        top: '26px',
                        left: '-2px'
                    });
                    city = $this.find('.layout-cityCountryPop');
                    if ($this.index() == 0) {
                        city.find('.list-citypop').eq(0).find('a').eq(0).remove();
                    }
                    if ($this.index() == 1 && selectFromCity) {
                        city.find('.tabCont').find('a').each(function(){
                            var $self = $(this),
                                _data = $self.attr('data').split('|'),
                                _id = _data[1];
                            $self.attr({
                                href: '/searcher?type=0&city=' + selectFromCity + '&to_city='+ _id +'&page=1'
                            });
                        });
                    }
                }
                var _tab = city.find('.nav-citypop li'),
                    _tabFrame = city.find('.tabCont');
                city.tabSwitch({
                    tab: _tab,
                    tabClass: 'tabCur',
                    tabFrame: _tabFrame,
                    tabEvent: 'click'
                });
                city.click(function(e){
                    e.stopPropagation();
                });
                city.find('.ico-close-cityPop').click(function(e){
                    e.stopPropagation();
                    city.remove();
                });
                $(document).click(function(){
                    city.remove();
                });
            });
        });
        //酒店列表弹出框
        var J_hotel_pos = $('.J_hotel_pos'),
            layout_cityCountryPop = $('.layout-cityCountryPop');
        J_hotel_pos.each(function(){
            var $this = $(this);
            $this.click(function(e){
                if ($this.siblings().find('.layout-cityCountryPop').length > 0) {
                    $this.siblings().find('.layout-cityCountryPop').remove();
                }
                e.stopPropagation();
                var city = $this.find('.layout-cityCountryPop');
                if (city.length < 1) {
                    layout_cityCountryPop.clone(true).appendTo($this).css({
                        display: 'block',
                        top: '25px',
                        left: '0'
                    });
                    city = $this.find('.layout-cityCountryPop');
                    city.find('.list-citypop').eq(0).find('a').eq(0).remove();
                }
                city.find('.tabCont').find('a').click(function(e){
                    e.preventDefault();
                    e.stopPropagation();
                    var _data = $(this).attr('data').split('|'),
                        _cn = _data[0],
                        _id = _data[1];
                    $this.find('.txt-input').val(_cn);
                    $('#hidden_city0').val(_id);
                    city.remove();
                });
                var _tab = city.find('.nav-citypop li'),
                    _tabFrame = city.find('.tabCont');
                city.tabSwitch({
                    tab: _tab,
                    tabClass: 'tabCur',
                    tabFrame: _tabFrame,
                    tabEvent: 'click'
                });
                city.click(function(e){
                    e.stopPropagation();
                });
                city.find('.ico-close-cityPop').click(function(e){
                    e.stopPropagation();
                    city.remove();
                });
                $(document).click(function(){
                    city.remove();
                });
            });
        });
        //酒店概况
        $('#js_hotelINfo_descShowMore').click(function(){
            $('#js_hotelInfo_descBrief').toggle();
            $('#js_hotelInfo_descAll').toggle();
        });
        $('.js_hotelInfo_efacilitylist').each(function(){
            var $this = $(this),
                _li = $(this).find('li');
            if (_li.length > 8) {
                $this.find('dd').append('<a href="javascript:void(0);" title="展开" class="icon_jt js_hotelInfo_showMore"></a>');
                _li.slice(8).addClass('expanfacilityicon');
                $this.find('.js_hotelInfo_showMore')
                    .hover(function(){
                        $(this).parent().addClass('hvfacility');
                    }, function(){
                        $(this).parent().removeClass('hvfacility');
                    }).click(function(){
                        $(this).parent().toggleClass('expanfacility');
                        var _title = $(this).attr('title');
                        _title = _title === '展开' ? '收起' : '展开';
                        $(this).attr('title', _title);
                        _li.slice(8).toggleClass('expanfacilityicon');
                    });
            }
        });
        //酒店图片
        var _hotel_img = $('.J_hotel_img'),
            _hotel_img_list = _hotel_img.find('.hotel-pic-sm').find('li'),
            _hotel_img_first = _hotel_img_list.eq(0),
            _hotel_img_item = _hotel_img.find('.hotel-pic-big').find('img');
        _hotel_img_first.addClass('current');
        _hotel_img_item.attr({
            src: _hotel_img_first.find('img').attr('data-img'),
            alt: _hotel_img_first.find('img').attr('alt'),
            title: _hotel_img_first.find('img').attr('alt')
        });
        _hotel_img_list.each(function(){
            var $this = $(this),
                _img = $this.find('img');
            $this
                .mouseenter(function(){
                    $this.siblings().removeClass('current').end().addClass('current');
                    _hotel_img_item.attr({
                        src: _img.attr('data-img'),
                        alt: _img.attr('alt'),
                        title: _img.attr('alt')
                    });
                });
        });
    });
    //选择日期
    $( ".dateStart").each(function(i){
        $( ".dateStart").eq(i).datepicker({
            onClose: function( selectedDate ) {
                $( ".dateEnd").eq(i).datepicker( "option", "minDate", selectedDate, "yyyy-mm-dd" );
            },
            dateFormat: 'yy-mm-dd'
        });
        $( ".dateEnd").eq(i).datepicker({
            onClose: function( selectedDate ) {
                $( ".dateStart" ).eq(i).datepicker( "option", "maxDate", selectedDate, "yyyy-mm-dd" );
            },
            dateFormat: 'yy-mm-dd'
        });
    });
    $('.icon-search-date').click(function(){
        $(this).siblings('.txt-input').triggerHandler('focus');
    });
    $('.hotel-search-date').click(function(){
        setTimeout(function(){
            if ($('.ui-datepicker-prev').length) {
                $('.ui-datepicker-prev').attr({
                    href: 'javascript:void(0);'
                });
            }
            if ($('.ui-datepicker-next').length) {
                $('.ui-datepicker-next').attr({
                    href: 'javascript:void(0);'
                });
            }
        },10);
    });
    //酒店筛选
    $('.js_cpf_from, .js_cpf_to').on('focus', function(){
        $('.js_cpf_submit').show();
    });
    /*
    var $default = $('#wrapper_default'),
        DC_default = $('#wrapper_DC-default'),
        DC_desc = $('#wrapper_DC-desc'),
        DC_asc = $('#wrapper_DC-asc'),
        SCORE_desc = $('#wrapper_SCORE-desc'),
        PRICE_default = $('#wrapper_PRICE-default'),
        PRICE_asc = $('#wrapper_PRICE-asc'),
        PRICE_desc = $('#wrapper_PRICE-desc');
    $default.click(function(){*//* 默认排序 *//*
        $(this).addClass('selected');
        DC_default.show();
        DC_desc.hide();
        DC_asc.hide();
        PRICE_default.show();
        PRICE_desc.hide();
        PRICE_asc.hide();
        SCORE_desc.removeClass('selected');
    });
    SCORE_desc.click(function(){*//* 按评分 *//*
        $(this).addClass('selected');
        DC_default.show();
        DC_desc.hide();
        DC_asc.hide();
        PRICE_default.show();
        PRICE_desc.hide();
        PRICE_asc.hide();
        $default.removeClass('selected');
    });
    DC_default.click(function(){*//* 按星级，降序 *//*
        $(this).hide();
        $default.removeClass('selected');
        SCORE_desc.removeClass('selected');
        DC_asc.show();
        PRICE_default.show();
        PRICE_desc.hide();
        PRICE_asc.hide();
    });
    DC_asc.click(function(){*//* 按星级，升序 *//*
        $(this).hide();
        DC_desc.show();
        PRICE_default.show();
        PRICE_desc.hide();
        PRICE_asc.hide();
    });
    DC_desc.click(function(){*//* 按星级，降序 *//*
        $(this).hide();
        DC_asc.show();
        PRICE_default.show();
        PRICE_desc.hide();
        PRICE_asc.hide();
    });
    PRICE_default.click(function(){*//* 按价格，降序 *//*
        $(this).hide();
        $default.removeClass('selected');
        SCORE_desc.removeClass('selected');
        PRICE_asc.show();
        DC_default.show();
        DC_desc.hide();
        DC_asc.hide();
    });
    PRICE_asc.click(function(){*//* 按价格，升序 *//*
        $(this).hide();
        PRICE_desc.show();
        DC_default.show();
        DC_desc.hide();
        DC_asc.hide();
    });
    PRICE_desc.click(function(){*//* 按价格，降序 *//*
        $(this).hide();
        PRICE_asc.show();
        DC_default.show();
        DC_desc.hide();
        DC_asc.hide();
    });*/
})(jQuery);