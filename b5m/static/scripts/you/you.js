(function() {
    // 游表单搜索
     $('form.search-form').each(function() {
        
        var form = $(this);
        var keywords = form.find('input[name=keywords]');
        var city = form.find('input[name=city]');
        var placeholder = keywords.data('placeholder');
        //var action = 'http://you.b5m.com/taoPage_{city}_searchresult_{keywords}_1';
        $(this).submit(function(e) {
            e.preventDefault();
           
            $(this).find('.sub').trigger('click');
        });
        form.find('.sub').unbind().click(function(e) {
            e.preventDefault();
            var val = $.trim(keywords.val());

            var cityId = city.val();
            var action = form.attr('action');
            var action = action.replace('[##]', val);
            var action = action.replace('[**]', cityId);
            //alert(val)
            //alert(action);

            if (val == '' || val == placeholder) {
                keywords.focus();
                return false;
            }
           	if(this.id == 'sub_btn_map'){
           	return false;
           	}
            // 新窗口打开，防止 form action 会带name参数
            window.open(action.replace('{keywords}', encodeURIComponent(val)).replace('{city}', cityId), '_blank');
        });

         keywords.on('keyup',function(e){
            if(e.keyCode ==13){
                form.find('.sub').trigger('click');
            } 
         })
    });
    var J_order = $("#J_order");
    var J_orderInput = J_order.find('input[name=order]')
    J_order.find('a[rel]').on('click', function(e) {
        J_orderInput.val($(this).attr('rel'));
        J_order.submit();
    });
    var $J_citySelect;
    var $selectedEl = null;
    var citySelect = {
        isShow: false,
        show: function() {
            if (this.isShow) {
                return false;
            }
            this.isShow = true;
            $J_citySelect.css({
                marginTop: 20,
                opacity: 0
            }).show().animate({
                marginTop: 0,
                opacity: 1
            });
        },
        hide: function() {
            this.isShow = false;
            $J_citySelect.animate({
                marginTop: 20,
                opacity: 0
            }, function() {
                $(this).hide();
            });
        }
    };
    $("#J_filterCont,#J_secondFiletr").find('[data-local]').on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
        $J_citySelect = $(this).parents('.rows').find('.cityselect');
        citySelect.show();
        $('.hotel-tip').hide();
        $selectedEl = $(this);
    });
    $('.cityselect').on('click', 'a', function(e) {
        e.stopPropagation();
        var self = $(this);
        if ($(this).hasClass('close')) {
            citySelect.hide();
        } else if ($(this).attr('data-city')) {
            $selectedEl.find('span').eq(0).html($(this).html());
            $(this).parents('.rows:first').find('input[name=city]').val($(this).attr('data-city'));
            citySelect.hide();
            self.trigger('jump', $(this).attr('data-city'));
        }
    });
    $('.cityselect').on('click', function(e) {
        e.stopPropagation();
    });
    $(document).on('click', function() {
        citySelect.isShow && citySelect.hide();
    });
   $("#hotel_keyword_text").on('click', function() {
        citySelect.isShow && citySelect.hide();
    });

    //酒店、攻略4个部分切换
    var $J_filterNav = $("#J_filterNav");
    if ($J_filterNav.length) {
        var $J_filterCont = $J_filterNav.next();
        $J_filterNav.find('>li').on('click', function(e) {
            e.preventDefault();
            if (!$(this).find('a').hasClass('cur')) {
                $J_filterNav.find('.cur').removeClass('cur');
                $(this).find('a').addClass('cur');
                var k = $J_filterNav.find('>li').index($(this));
                $J_filterCont.find('>div').hide().eq(k).show();

                var input = $J_filterCont.find('>div').eq(k).find('.filter-text[placeholder]');
                if (!input.data('initPlaceholder')) {
                    input.placeholder({
                        className: 'filter-text-placeholder'
                    }).on('focusin', function() {
                        $(this).addClass('filter-text-focusin');
                    }).on('focusout', function() {
                        $(this).removeClass('filter-text-focusin');
                    });
                }



            }
        });
        var k = $J_filterNav.find('a').index($J_filterNav.find('.cur'));
        $J_filterCont.find('>div').hide().eq(k).show();
        $J_filterCont.find('>div').eq(k).find('.filter-text[placeholder]').placeholder({
            className: 'filter-text-placeholder'
        }).on('focusin', function() {
            $(this).addClass('filter-text-focusin');
        }).on('focusout', function() {
            $(this).removeClass('filter-text-focusin');
        });
    }
    $("#J_secondFiletr>input").placeholder({
        className: 'filter-text-placeholder'
    }).on('focusin', function() {
        $(this).addClass('filter-text-focusin');
    }).on('focusout', function() {
        $(this).removeClass('filter-text-focusin');
    });
    //slider分页 和 切换
    var $sliderList = $('.slider');
    $sliderList.length && $('.slider').each(function() {
        var $slider = $(this);
        var $sliderNav = $slider.find('.slider-nav > .slider-nav-li');
        window.NS.publics.tab($sliderNav, $slider.find('.slider-item'), 'cur', 'slider-item-cur', $slider.find('>.slider-prev'), $slider.find('>.slider-next'), function(pageHTML) {
            $slider.find('.slider-page').html(pageHTML);
        });
        $slider.find('.slider-page').on('click', 'a[class!=more]', function(e) {
            e.preventDefault();
            var pageCont = $slider.find('.slider-item:visible');
            var pageHeight = pageCont.innerHeight();
            if (!$(this).hasClass('cur')) {
                var page = ($(this).html() | 0) - 1;
                pageCont.animate({
                    scrollTop: page * pageHeight
                }, 300);
                $(this).addClass('cur').siblings('.cur').removeClass('cur');
            }
        });
    });
})();