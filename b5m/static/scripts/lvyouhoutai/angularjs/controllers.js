manageApp.controller('welcomeCtrl', ['$scope', function ($scope) {

}]);


manageApp.controller('datalistCtrl', ['$scope','datalistSer', function ($scope,datalistSer) {

	$scope.dataInfo = {
		page:1
	}

	//加载指定页码的数据
	// $scope.loadPage = function(){
		datalistSer.getDataList(1).success(function(data){
			console.log(data)
			$scope.dataInfo.datas = data.all;
			$scope.dataInfo.pages = data.totalPages;
		});
	// }

	// 下一页
	$scope.nextPage = function(){
		if($scope.dataInfo.page < $scope.dataInfo.pages){
			$scope.dataInfo.page++;
			$scope.loadPage();
		}
	}

	//上一页
	$scope.prevPage = function(){
		if($scope.dataInfo.page > 1){
			$scope.dataInfo.page--;
			$scope.loadPage();
		}
	}
}])