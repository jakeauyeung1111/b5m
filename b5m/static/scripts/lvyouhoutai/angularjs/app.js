var manageApp = angular.module('manageApp', [
		'ngRoute'
	]);

manageApp.config(['$routeProvider',function ($routeProvider) {
	$routeProvider
	.when('/', {
		templateUrl: 'partials/welcome.html',
		controller: 'welcomeCtrl'
	})
	.when('/datalist', {
		templateUrl: 'partials/datalist.html',
		controller: 'datalistCtrl'
	})
	.when('/addmapdata',{
		templateUrl:'partials/addlvdata.html',
		controller:''
	})
}]);