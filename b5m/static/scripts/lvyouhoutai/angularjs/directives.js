manageApp.directive('addMap', ['$location',function ($location) {
	return {
		restrict: 'A',
		link: function (scope, iElement, iAttrs) {
			iElement.bind('click',function(){
				$location.path('/addmapdata');
			});
		}
	};
}])