/**
 * slider.js
 * www.html
 * main
 */
$(function() {
	var $slider = $('#J_slider_min'),
		$slider_box = $slider.find('ul'),
		$slider_item = $slider.find('li'),
		len = $slider_item.length - 1,
		step = $slider_item.width(),
		$trigger = $('#J_slider_trigger').find('a'),
		i = 0;
	$slider_box.width($slider_item.outerWidth(true)*$slider_item.length);
	$trigger.on('mouseover',function(){
		var $this = $(this),
			i = $trigger.index($this);
		move(i);
	});

	function move(i){
		$slider_box.stop(true,true).animate({'left':-i*step});
		$trigger.eq(i).addClass('cur').siblings('a').removeClass('cur');
	}

	function autoMove(){
		flag = setInterval(function(){
			if(i >= len){
				i = 0;
			}else{
				i++;
			}
			move(i)
		},3000);
	}

	autoMove();

	$slider.hover(function(){
		clearInterval(flag);
	},function(){
		autoMove();
	});

    // menu
    var $menu = $('#J_menu'),
        $trigger_menu = $menu.children('li'),
        $subMenu = $menu.find('.sub_item'),
        w_len = $menu.find('.sub-wp').outerWidth(true),
        timer;
    $menu.hover(function(){
        $subMenu.stop(true,true).animate({
            display:'block',
            width:w_len
        },{
            duration:300
        })
    },function(){        
        $subMenu.stop(true,true).animate({
            display:'none',
            width:0
        },{ 
            duration:200
        })
    });
    $trigger_menu.hover(function(){
        var $this = $(this),
            $subContent = $this.find('.sub-in'),
            $subMenuRel = $this.find('.sub_item');
        $subMenu.hide();
        $subContent.show();
        $subMenuRel.show();
        $this.addClass('open');
        $subMenuRel.find('img').trigger('lazyload');
    },function(){
        var $this = $(this),
            $subMenuRel = $this.find('.sub_item'),
            $subContent = $this.find('.sub-in');
        $this.removeClass('open');  
        $subMenuRel.hide();      
        $subContent.hide();
    });

    $(".J_search").placeholder({
        className: 'filter-text-placeholder'
    }).on('focusin', function() {
        $(this).addClass('focusin');
    }).on('focusout', function() {
        $(this).removeClass('focusin');
    });



    // tao tab
    $('.Tab').find('.tab-hd li').on('mouseover',function(e){
        e.preventDefault();
        var $this = $(this),
            index = $this.index(),
            $item = $this.parents('.Tab').find('.tab-box .tab-item');
        $this.addClass('cur').siblings('li').removeClass('cur');
        $item.eq(index).show().siblings('.tab-item').hide();
        $item.eq(index).find('img').trigger('lazyload');
    })
    //tao history
    var $slider = $('.tao-slider');
    var $sliderNav = $slider.find('.tao-slider-nav > .tao-slider-nav-li');
    var $sliderCont = $slider.find('.tao-slider-item');
    window.NS.publics.tab($sliderNav, $slider.find('.tao-slider-item'), 'cur', 'tao-slider-item-cur', $slider.find('>.slider-prev'), $slider.find('>.slider-next'), function() {
        var link = $(this).find('a').attr('href');
    });
    var link = $sliderNav.eq(0).find('a').attr('href');
    var $this = $('#J_other_view');
    var lineW = 58;
    var totleNum = $this.find('img').length;

    $('#J_prev').click(function(){
        if(!$this.is(":animated")){
            $this.animate({
                left: '+=58'
            }, 300,function(){
                $this.css('left',0).find('a:last').prependTo($this);
            });
        }
    });
    $('#J_next').click(function(){
        if(!$this.is(":animated")){
            $this.animate({
                left: '-=58'
            }, 300,function(){
                $this.css('left',0).find('a:first').appendTo($this);
            });
        }
    });
    // 友情链接滚动
    //单行应用
    var settingWrap=$('ul.line');//定义滚动区域
    var settingInterval=3000;//定义滚动间隙时间
    var clearMoving;//需要清除的动画
    settingWrap.hover(function(){
        clearInterval(clearMoving);//当鼠标在滚动区域中时,停止滚动
    },function(){
        clearMoving=setInterval(function(){
            var field=settingWrap.find('li:first');//此变量不可放置于函数起始处,li:first取值是变化的
            var h=field.height();//取得每次滚动高度
            field.animate({marginTop:-h+'px'},600,function(){//通过取负margin值,隐藏第一行
                field.css('marginTop',0).appendTo(settingWrap);//隐藏后,将该行的margin值置零,并插入到最后,实现无缝滚动
            })
        },settingInterval)//滚动间隔时间取决于interval
    }).trigger('mouseleave');//函数载入时,模拟执行mouseleave,即自动滚动
    /*F5鼠标经过*/
    $('.tuan-index-main').on({
        mouseover:function(){
            $(this).addClass('mod-proItem-cur');
        },
        mouseout:function(){
            $(this).removeClass('mod-proItem-cur');
        }
    },"dl");
});
//slider分页 和 切换
var $sliderList = $('.slider');
$sliderList.length && $('.slider').each(function() {
    var $slider = $(this);
    var $sliderNav = $slider.find('.slider-nav > .slider-nav-li');
    window.NS.publics.tab($sliderNav, $slider.find('.slider-item'), 'cur', 'slider-item-cur', $slider.find('>.slider-prev'), $slider.find('>.slider-next'), function(pageHTML) {
        $slider.find('.slider-page').html(pageHTML);
    });
    $slider.find('.slider-page').on('click', 'a[class!=more]', function(e) {
        e.preventDefault();
        var pageCont = $slider.find('.slider-item:visible');
        var pageHeight = pageCont.innerHeight();
        if (!$(this).hasClass('cur')) {
            var page = ($(this).html() | 0) - 1;
            pageCont.animate({
                scrollTop: page * pageHeight
            }, 300);
            $(this).addClass('cur').siblings('.cur').removeClass('cur');
        }
    });
});
//帮豆加分提示
var bangdouTips = function(){
    var tips;
    function init(num){
        return $('<div class="bangdou-tips" style="color:red"><div class="bangdou-tips__in">' + "+" + num + '</div></div>').appendTo($('.bangdou-target'));
    }
    return {
        getTips:function(num){
            return tips || (tips = init(num));
        }
    }
}();

function addTips(num){
    bangdouTips.getTips(num).animate({opacity:1},400).delay(1000).animate({opacity:0},800);
}
