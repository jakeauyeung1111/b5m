/**
 * slider.js
 * www.html
 * main
 */

$(function() {
	var $slider = $('#J_slider_min'),
		$slider_box = $slider.find('ul'),
		$slider_item = $slider.find('li'),
		len = $slider_item.length - 1,
		step = $slider_item.width(),
		$trigger = $('#J_slider_trigger').find('a'),
		i = 0;
		
	$trigger.on('mouseover',function(){
		var $this = $(this),
			i = $trigger.index($this);
		move(i);
	});

	function move(i){
		$slider_box.stop(true,true).animate({'left':-i*step});
		$trigger.eq(i).addClass('cur').siblings('a').removeClass('cur');
	}

	function autoMove(){
		flag = setInterval(function(){
			if(i >= len){
				i = 0;
			}else{
				i++;
			}
			move(i)
		},3000);
	}

	autoMove();

	$slider.hover(function(){
		clearInterval(flag);
	},function(){
		autoMove();
	})
});
//slider分页 和 切换
var $sliderList = $('.slider');
$sliderList.length && $('.slider').each(function() {
    var $slider = $(this);
    var $sliderNav = $slider.find('.slider-nav > .slider-nav-li');
    var $sliderCont = $slider.find('.slider-item');

    var getPageHtml = function(moreLink) {
        var pageH = $sliderCont.eq(0).height();
        var height = $sliderCont.filter(':visible').find('ul').height();
        var pageLen = Math.ceil(height / pageH);
        var html = '';
        var i = pageLen - 1;
        while (i >= 0) {
            html = '<a ' + (i == 0 ? 'class="cur"' : '') + ' href="javascript:void(0)">' + (i + 1) + '</a>' + html;
            i--;
        }
        return html + '<a class="more" href="' + moreLink + '">更多&gt;</a>';
    }
    window.NS.publics.tab($sliderNav, $slider.find('.slider-item'), 'cur', 'slider-item-cur', $slider.find('>.slider-prev'), $slider.find('>.slider-next'), function() {
        var link = $(this).find('a').attr('href');
        $slider.find('.slider-page').html(getPageHtml(link));

    });
    var link = $sliderNav.eq(0).find('a').attr('href');
    $slider.find('.slider-page').html(getPageHtml(link));
    $slider.find('.slider-page').on('click', 'a[class!=more]', function(e) {
        e.preventDefault();
        var pageCont = $slider.find('.slider-item:visible');
        var ul = pageCont.find('ul');
        var pageHeight = pageCont.innerHeight();
        if (!$(this).hasClass('cur')) {
            var page = ($(this).html() | 0) - 1;
            pageCont.animate({
                scrollTop: page * pageHeight
            }, 300);
            $(this).addClass('cur').siblings('.cur').removeClass('cur');
        }
    });
});	