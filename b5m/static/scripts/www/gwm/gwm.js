/*
 * Created with Sublime Text 2.
 * User: song.chen
 * Date: 2013-10-22
 * Time: 13:16:47
 * Contact: song.chen@qunar.com
 */
var gwmFE = {
	slider:function(){
		var $slider = $('#js-slider'),
			$slider_wrap = $('#js-slider__content__in'),
			$trigger = $slider.children('a'),
			$slider_item = $slider_wrap.find('img'),
			$slider_item_w = $slider_item.outerWidth();
		$trigger.on('click',function(e){
			e.preventDefault();
			var direction = $(this).data('dir'),
				appendType = '',
				distance = 0,
				position = '';
			if(direction == 'prev'){
				distance = $slider_item_w;
				appendType = "prependTo";
				position = "last";
			}else if(direction == 'next'){
				distance = -$slider_item_w;
				appendType = "appendTo";
				position = "first";
			}
			$slider_wrap.stop(true,true).animate({
				'margin-left':distance
			},300,function(){
				$slider_wrap.css({
					'margin-left':0
				}).find('img:'+ position)[appendType]($slider_wrap);
			})
		});
	}
}