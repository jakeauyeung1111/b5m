$(function() {

    // 图片无缝播放
    $('.pic-slider').jcarousel({wrap: 'circular'}).jcarouselAutoscroll({
        interval: 3000,
        autostart: true
    });

    $('.pagination').on('jcarouselpagination:active', 'a',function () {
        $(this).addClass('active');
    }).on('jcarouselpagination:inactive', 'a',function () {
            $(this).removeClass('active');
        }).jcarouselPagination();


//    var $slider = $('#J_slider_min'),
//        $slider_box = $slider.find('ul'),
//        $slider_item = $slider.find('li'),
//        len = $slider_item.length - 1,
//        step = $slider_item.width(),
//        $trigger = $('#J_slider_trigger').find('a'),
//        i = 0;
//    $slider_box.width($slider_item.outerWidth(true)*$slider_item.length);
//    $trigger.on('mouseover',function(){
//        var $this = $(this),
//            i = $trigger.index($this);
//        move(i);
//    });
//
//    function move(i){
//        // console.log(slider_item.length);
//        $slider_box.stop(true,true).animate({'left':-i*step});
//        $trigger.eq(i).addClass('cur').siblings('a').removeClass('cur');
//    }
//
//    function autoMove(){
//        flag = setInterval(function(){
//            if(i >= len){
//                i = 0;
//            }else{
//                i++;
//            }
//            move(i)
//        },4000);
//    }
//
//    autoMove();
//
//    $slider.hover(function(){
//        clearInterval(flag);
//    },function(){
//        autoMove();
//    });

    // menu
    var $menu = $('#J_menu'),
        $trigger_menu = $menu.children('li'),
        $subMenu = $menu.find('.sub_item'),
        w_len = $menu.find('.sub-wp').outerWidth(true),
        timer;
    $menu.hover(function(){
        $subMenu.stop(true,true).animate({
            display:'block',
            width:w_len
        },{
            duration:300
        })
    },function(){
        $subMenu.stop(true,true).animate({
            display:'none',
            width:0
        },{
            duration:200
        })
    });
    $trigger_menu.hover(function(){
        var $this = $(this),
            $subContent = $this.find('.sub-in'),
            $subMenuRel = $this.find('.sub_item');
        $subMenu.hide();
        $subContent.show();
        $subMenuRel.show();
        $this.addClass('open');
        $subMenuRel.find('img').trigger('lazyload');
    },function(){
        var $this = $(this),
            $subMenuRel = $this.find('.sub_item'),
            $subContent = $this.find('.sub-in');
        $this.removeClass('open');
        $subMenuRel.hide();
        $subContent.hide();
    });

    $(".J_search").placeholder({
        className: 'filter-text-placeholder'
    }).on('focusin', function() {
            $(this).addClass('focusin');
        }).on('focusout', function() {
            $(this).removeClass('focusin');
        });



    // tao tab
    $('.Tab').find('.tab-hd li').on('mouseover',function(e){
        e.preventDefault();
        var $this = $(this),
            index = $this.index(),
            $item = $this.parents('.Tab').find('.tab-box .tab-item');
        $this.addClass('cur').siblings('li').removeClass('cur');
        $item.eq(index).show().siblings('.tab-item').hide();
        $item.eq(index).find('img').trigger('lazyload');
    })
    //tao history
    var $slider = $('.tao-slider');
    var $sliderNav = $slider.find('.tao-slider-nav > .tao-slider-nav-li');
    var $sliderCont = $slider.find('.tao-slider-item');
    window.NS.publics.tab($sliderNav, $slider.find('.tao-slider-item'), 'cur', 'tao-slider-item-cur', $slider.find('>.slider-prev'), $slider.find('>.slider-next'), function() {
        var link = $(this).find('a').attr('href');
    });
    var link = $sliderNav.eq(0).find('a').attr('href');
    var $this = $('#J_other_view');
    var lineW = 58;
    var totleNum = $this.find('img').length;

    $('#J_prev').click(function(){
        if(!$this.is(":animated")){
            $this.animate({
                left: '+=58'
            }, 300,function(){
                $this.css('left',0).find('a:last').prependTo($this);
            });
        }
    });
    $('#J_next').click(function(){
        if(!$this.is(":animated")){
            $this.animate({
                left: '-=58'
            }, 300,function(){
                $this.css('left',0).find('a:first').appendTo($this);
            });
        }
    });

    /*F5鼠标经过*/
    $('.tuan-index-main').on({
        mouseover:function(){
            $(this).addClass('mod-proItem-cur');
        },
        mouseout:function(){
            $(this).removeClass('mod-proItem-cur');
        }
    },"dl");
});
//slider分页 和 切换
var $sliderList = $('.slider');
$sliderList.length && $('.slider').each(function() {
    var $slider = $(this);
    var $sliderNav = $slider.find('.slider-nav > .slider-nav-li');
    window.NS.publics.tab($sliderNav, $slider.find('.slider-item'), 'cur', 'slider-item-cur', $slider.find('>.slider-prev'), $slider.find('>.slider-next'), function(pageHTML) {
        $slider.find('.slider-page').html(pageHTML);
    });
    $slider.find('.slider-page').on('click', 'a[class!=more]', function(e) {
        e.preventDefault();
        var pageCont = $slider.find('.slider-item:visible');
        var pageHeight = pageCont.innerHeight();
        if (!$(this).hasClass('cur')) {
            var page = ($(this).html() | 0) - 1;
            pageCont.animate({
                scrollTop: page * pageHeight
            }, 300);
            $(this).addClass('cur').siblings('.cur').removeClass('cur');
        }
    });
});
//帮豆加分提示
var bangdouTips = function(){
    var tips;
    function init(num){
        return $('<div class="bangdou-tips" style="color:red"><div class="bangdou-tips__in">' + "+" + num + '</div></div>').appendTo($('.bangdou-target'));
    }
    return {
        getTips:function(num){
            return tips || (tips = init(num));
        }
    }
}();

function addTips(num){
    bangdouTips.getTips(num).animate({opacity:1},400).delay(1000).animate({opacity:0},800);
}



(function(window, $) {


    //顶部tab切换
    var contTab = $('.cont-tab'),
        contTabTitle = contTab.find('.tab-title'),
        contTabContent = contTab.find('.tab-content');
    contTabTitle.find('a').each(function(i) {
        $(this).mouseover(function() {
            contTab.find('.cur').removeClass('cur');
            $(this).addClass('cur');
            contTabContent.eq(i).addClass('cur');
        }).click(function() {
                return false;
            });
    });

    //site count
    $('.logo-rank a:not(:last)').click(function() {
        return false;
    });

    var SiteCount = ({
        init:function() {

            var _this  = this;

            this.site = $('.count-site');
            this.prod = $('.count-prod');
            this.seller = $('.count-seller');

            //如果有cookie prodcount，则读取
            var _count = Cookies.get('count-prod-date');
            if(_count && parseInt(_count)) {
                this.prod.attr('data-num',_count);
            }

            this.build.call(this.site);
            this.build.call(this.prod);
            this.build.call(this.seller);

            this.animate.call(this.site);
            this.animate.call(this.prod);
            this.animate.call(this.seller);

            setInterval(function() {
                _this.prodCount();
                _this.build.call(_this.prod,'add');
                _this.animate.call(_this.prod);
            },10000);

        },
        //生成dom
        build:function() {
            var num = this.attr('data-num');
            if(arguments[0]) {
                Cookies.set('count-prod-date',num,new Date(new Date().getTime()+  1000*60*60*24), '', '.b5m.com');
            }
            for(var i=0;i<num.length;i++) {
                this.find('em').eq(i).data('next-count',num.charAt(i));
            }
        },
        //重新统计收录量
        prodCount:function() {
            var addCount = Math.ceil(Math.random()*9);
            return this.prod.attr('data-num',parseInt(this.prod.attr('data-num')) + addCount);
        },
        //动画
        animate:function() {
            var elem = $(this).find('em'),
                scroll = 22,
                step = 0;
            elem.each(function(i,n) {
                var nextCount = $(n).data('next-count'),
                    prevCount = $(n).data('prev-count');

                if(nextCount < prevCount) {
                    step = 220;
                }

                $(n).animate({top:'-'+(nextCount*scroll + step)+'px'},2000).promise().done(function() {
                    var _top = parseInt($(n).css('top'));
                    if(_top <= -220) {
                        $(n).css('top',(_top+220) +'px');
                    }
                    $(n).data('prev-count',nextCount);
                });

            });
        }

    }).init();



    //6个小banner
    var bannerRowSlider = ({
        init:function() {
            var _this = this;
            this.cur = 0;
            this.pos = 0;
            this.play = false;
            this.playTimer = null;

            this.container = $('.banner-m');
            this.list = this.container.find('ul');
            this.lists = this.list.find('li');
            this.arrowL = this.container.find('.a-l');
            this.arrowR = this.container.find('.a-r');

            this.clone();

            _this.playTimer = setTimeout(function () {
                _this.slide();
            }, 5000);

            this.arrowL.click($.proxy(this.arrowLHandler,this));
            this.arrowR.click($.proxy(this.arrowRHandler,this));

        },
        arrowLHandler:function() {
            this.slide(-1);
            return false;
        },
        arrowRHandler:function() {
            this.slide(1);
            return false;
        },
        slide:function(path) {

            var _this = this;

            if(_this.play) {
                return;
            }

            path = path || 1;

            //如果到底
            if(_this.cur === 0 && path === -1) {
                _this.list.css({'left': -6 * 256});
                _this.pos = -6 * 256;
                _this.cur = 6;
            }

            //如果到第一个
            if((_this.cur + 3) % 12 ===0 && path === 1) {
                _this.list.css({'left': - 3 * 256});
                _this.pos = -3 * 256;
                _this.cur = 3;
            }

            _this.pos = _this.pos + -path * 256;
            _this.cur += path;
            _this.play = true;

            _this.list.animate({'left':_this.pos}, function () {
                _this.play = false;
                if(_this.playTimer) {
                    clearTimeout(_this.playTimer);
                }
                _this.playTimer = setTimeout(function () {
                    _this.slide();
                }, 5000);
            });

        },
        clone:function() {
            var _this = this;
            this.list.append(this.lists.clone());
            this.cloned = true;
        }
    }).init();



   /* (function() {

        // 友情链接滚动
        //单行应用
        var settingWrap=$('ul.line');//定义滚动区域
        var settingInterval=3000;//定义滚动间隙时间
        var clearMoving;//需要清除的动画
        settingWrap.hover(function(){
            clearInterval(clearMoving);//当鼠标在滚动区域中时,停止滚动
        },function(){
            clearMoving=setInterval(function(){
                var field=settingWrap.find('li:first');//此变量不可放置于函数起始处,li:first取值是变化的
                var h=field.height();//取得每次滚动高度
                field.animate({marginTop:-h+'px'},600,function(){//通过取负margin值,隐藏第一行
                    field.css('marginTop',0).appendTo(settingWrap);//隐藏后,将该行的margin值置零,并插入到最后,实现无缝滚动
                })
            },settingInterval)//滚动间隔时间取决于interval
        }).trigger('mouseleave');//函数载入时,模拟执行mouseleave,即自动滚动

    })();*/


    //帮贷款
    (function(){
        $('#type_sel').click(function(event) {
            k = $(this).attr('k');
            setList('#' + k + '_u');
            event.stopPropagation();
        });
        $('#moeny_sel').click(function(event) {
            k = $(this).attr('k');
            setList('#' + k + '_u');
            event.stopPropagation();
        });
        $('#month_sele').click(function(event) {
            k = $(this).attr('k');
            setList('#' + k + '_u');
            event.stopPropagation();
        });
        $(document).click(function(event) {
            var eo = $(event.target);
            if ($("#type_sel").is(":visible") && eo.attr("class") != "xiala" &&
                !eo.parent(".xiala").length)
            {closeList();}
            if ($("#moeny_sel").is(":visible") && eo.attr("class") != "xiala" &&
                !eo.parent(".xiala").length)
            {closeList();}
            if ($("#month_sele").is(":visible") && eo.attr("class") != "xiala" &&
                !eo.parent(".xiala").length)
            {closeList();}
        });
        $('.xiala a').click(function() {
            k = $(this).attr('k');
            val = $(this).text();
            reval = $(this).attr('reval');
            $('#' + k).val(val);
            $('#' + k).attr('reval', reval);
            closeList();
        });

        $('#tab_title a').click(function(){
            $(this).addClass("cur").siblings().removeClass();
            $("#tab_ul li").hide().eq($('#tab_title a').index(this)).show();
        });

        $('#J_piao_hot').find('li').mouseover(function(){
            $(this).addClass('piao-hot-hover').siblings('li').removeClass('piao-hot-hover');
            $(this).find('img').imglazyload({fadeIn:true});
        });
    })();
    /**
     * 电商logo切换 zibu 20140310
     */
    var incLogoTabFun=({
        init:function(){
            var iNow = 0,
                _this = this,
                timer = null;
            /*上一页按钮*/
            $('.tab-content').find('.logo-tab').on('click','.prev',function(event){
                var  _thisItems = $('.logo-items')
                if( iNow < 1){
                    _thisItems.find('.logo-rank').eq(4).clone().prependTo(_thisItems);
                    _thisItems.css('left','-198px');
                    $('.logo-tab').find('span').eq(4).addClass('cur').siblings('span').removeClass('cur');
                    $('.tab-content').find('.logo-items').stop(true,true).animate({'left':'0px'},function(){
                        _thisItems.css('left','-792px');
                        _thisItems.find('.logo-rank').eq(0).remove();
                        iNow=4;
                    });
                }else{
                    iNow --;
                    _this.tabFun(iNow);
                };
                event.stopPropagation();
            });
            /*下一页按钮*/
            $('.tab-content').find('.logo-tab').on('click','.next',function(event){
                var  _thisItems = $('.logo-items')
                iNow ++;
                if(iNow > 4){
                    _thisItems.find('.logo-rank').eq(0).clone().appendTo(_thisItems);
                    $('.logo-tab').find('span').eq(0).addClass('cur').siblings('span').removeClass('cur');
                    $('.tab-content').find('.logo-items').stop(true,true).animate({'left':-iNow*198+'px'},function(){
                        _thisItems.css('left',0);
                        _thisItems.find('.logo-rank').eq(5).remove();
                        iNow = 0;
                    });
                }else{
                    _this.tabFun(iNow);
                };
                event.stopPropagation();
            });
            /*点击数字切换*/
            $('.tab-content').find('.logo-tab').on('click','span',function(event){
                iNow = $(this).index();
                _this.tabFun(iNow);
                event.stopPropagation();
            });
            timer = setInterval(function(){
                $('.logo-tab').find('.next').trigger('click');
            },4000);
            $('.tab-content').find('.logo-tab').hover(function(){
                clearInterval(timer);
            },function(){
                timer = setInterval(function(){
                    $('.logo-tab').find('.next').trigger('click');
                },4000);
            });
        },
        tabFun:function(curIndex){
            $('.logo-tab').find('span').eq(curIndex).addClass('cur').siblings('span').removeClass('cur');
            $('.tab-content').find('.logo-items').stop(true,false).animate({'left':-curIndex*198+'px'},500);
        }
    }).init();

    /**
     * 帮5淘视频播放 zibu 20140328
     */
    var playVideo =({
        init:function(){
            var flvStr =  '<div class="movie"><embed src="http://staticcdn.b5m.com/static/public/video/Flvplayer.swf" id="flashPlayer" allowfullscreen="true"  flashvars="vcastr_file=http://cdn.bang5mai.com/upload/plugin/flash/b5t.flv&LogoText=www.b5m.com&IsAutoPlay=1" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="518" height="290"></div>',
                $videoCont =$('.b5t-plugin');
            $videoCont.on('click','a',function(){
                $(this).hide();
                $(flvStr).appendTo($videoCont);
                return false;
            });
        }
    }).init()
})(window, window.jQuery);