;var jiheFed={
    apply:function(){
        $(".apply-sl").on("click","a",function(){
            var sorollTop=$(document).scrollTop(),
                winHeight=$(window).height(),
                domHeight=$(document).height();
            $(".layer-main").css("top",sorollTop+(winHeight/2)+"px");
            $(".layer-bg").css("height",domHeight+"px");
            $(".layer-bg,.layer-main").show();
        });
        $(".layer-bg").on("click",function(){
            $(".layer-bg").hide();
            $(".layer-main").hide();
        });
        $(".layer-main").on("click",".close",function(){
            $(".layer-bg").hide();
            $(this).parents(".layer-main").hide();
        });
        $('.layer-main').find('.qx-btn').click(function(){
            $('.layer-main').find('.close').trigger("click");
        });
    },
    asideTip:function(obj){
        var _this=$(obj).find(".goods-tiems");
        _this.eq(0).find("dd").show();
        _this.hover(function(){
            $(this).find("dd").show().end().siblings("dl").find("dd").hide();

        });
    },
    autoFill:function(){
        $(".J_autofill").autoFill("","b5mo");
    }
    ,
    loadFn:function(){
        this.apply();
        this.asideTip("#zr-tip");
        this.asideTip("#zg-tip");
        this.autoFill();
    }
};