/**
 * User: dongzhuo@b5m.com
 * Date: 13-10-21
 * Time: 下午5:34
 */

(function($,window) {


  var sideNav = $('#J_side_nav')
      ,sideNavTop = sideNav.offset().top;


  sideNav.find('a').click(function() {
      $('html,body').animate({scrollTop:$('#'+$(this).attr('href').slice(1)).offset().top});
      $('.common_question').removeClass('cur');
     return false;
  });

    $('.common_question').click(function() {
        $('html,body').animate({scrollTop:$('#common_question').offset().top});
        $(this).addClass('cur');
    });

    $(window).scroll(function() {
        var scroll = document.documentElement.scrollTop || document.body.scrollTop || 0;
        if(scroll>=sideNavTop) {
            sideNav.addClass('fix');
        }else {
            sideNav.removeClass('fix');
        }
    });

    $("#J_toggle").on("click",".s_title",function(){
        var $this = $(this),$par_node = $this.parents('.sub_detail');
        if(!$par_node.hasClass('open')){
            $('.open').animate({height:"25px"},500,'linear').removeClass('cur open');
            var cur_height = $par_node.height(),auto_height = $par_node.css({height:'auto'}).height();
            $par_node.height(cur_height).animate({height:auto_height+'px'},500,'linear').addClass('open');
        }
    });

  $('a.play').click(function() {

      $('.main-banner .movie').show();

  });

})(jQuery,window);