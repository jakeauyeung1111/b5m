/**
 * User: dongzhuo@b5m.com
 * Date: 13-10-17
 * Time: 下午4:31
 */

var flvStr;

function flvFun(){
     flvStr = '<div class="movie"><embed src="http://staticcdn.b5m.com/static/public/video/Flvplayer.swf" id="flashPlayer" allowfullscreen="true"  flashvars="vcastr_file=http://cdn.bang5mai.com/upload/plugin/flash/b5t.flv&LogoText=www.b5m.com&IsAutoPlay=1" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="606" height="342"></div>'
        ,FlvBox = '.banner4';
    Playering = false;
    $('.play',FlvBox).click(function(){
        $(flvStr).appendTo($(".content",FlvBox)).show();
        Playering = true;
        $(this).hide();
    });
}
(function(window,$) {

    //main banner
    $('.banner-index').mainBanner();

    flvFun();

    $('.view-video').click(function() {
        $('.role span:first').click();
         setTimeout(function() {
             $('.play').click();
         },200);
    });


    var navIndex = $('.nav-index'),
        isie6 = typeof document.body.style.maxHeight ==='undefined';

    navIndex.data('top',navIndex.offset().top);

    navIndex.find('a').each(function(i,n) {

        $(n).click(function(){

            navIndex.find('.cur').removeClass('cur');
            $(n).addClass('cur');
            $('html,body').animate({scrollTop:floorArr[i]-(isie6 ? 0 : 200)});

        });

    });


    var floorArr = [],floorArrScroll=[];
    var floorIndex = $('.index-floor');
    floorIndex.each(function(i,n) {
        floorArr.push($(this).offset().top);
    });
    floorArrScroll = floorArr.slice();




    $(window).scroll(function() {

        var scroll = document.documentElement.scrollTop || document.body.scrollTop || 0;
        if(scroll>=navIndex.data('top')) {
            navIndex.addClass('fix');
        }else {
            navIndex.removeClass('fix');
        }

        $.each(floorArr,function(i,n) {
            if(floorArrScroll[i] && scroll >= floorArrScroll[i]-(isie6 ? 0 : 800)) {
                floorIndex.eq(i).addClass('animate');
                delete floorArrScroll[i];
            }
        });

    }).trigger('scroll');


  $('.t-detail a').click(function() {
      $(this).next().show();
      $(this).hide();
  });

})(window,jQuery);