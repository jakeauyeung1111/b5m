;var banggemangFed={
    /**
     * 分享
     */
    bgmShare:function(){
        popWindowShareFed.init({
            id:".bgm-share",
            href:window.location.href,
            content:'分享内容'
        });
    },
    picShow:function(parendId,btn){
        $(parendId).on("click",btn,function(){
            var bigBox=$(this).parents(parendId).siblings(".big-pic"),
                imgSrc=$(this).find("img").attr("src");
            $(".big-pic").hide();
            $(parendId).find(".item").css("z-index","1");
            $(this).parents(".item").css("z-index","2");
            bigBox.show();
            bigBox.find("img").attr("src",imgSrc);
        });
        $(".big-pic").on("click","img",function(){
            $(".big-pic").hide();
        });
    },
    /**
     * 弹出框
     * @param 点击按钮父级
     * @param 被点击按钮
     * @param 弹出框ID
     * @param 关闭按钮
     */
    popTip:function(parentId,btn,popId,closeBtn){
        $(parentId).on("click",btn,function(){
            var sorollTop=$(document).scrollTop(),
                winHeight=$(window).height(),
                domHeight=$(document).height(),
                popHeight=$(popId).height();
            $(popId).css("top",sorollTop+(winHeight/2)-popHeight/2+"px");
            $(".layer-bg").css("height",domHeight+"px");
            $(".layer-bg,"+popId).show();
        });
        $(popId).on("click",closeBtn,function(){
            $(".layer-bg").hide();
            $(this).parents(popId).hide();
        });
    },
    loadFun:function(){
         var _this=this;
        _this.bgmShare();
        _this.popTip(".ans-tiems",".an-btn a",".layer-main",".close");
        _this.picShow(".pic-items","a");
    }
};


$(function(){
	/*aside menu s*/
	$(".b-aside").find("a").on("click",function(){
		$(this).parent("li").siblings('li').find("a").removeClass("cur");
		$(this).addClass("cur");
	});

	/*help sele*/
	$(".help-sele").hover(function(){
		$(this).addClass("open");
	},function(){
		$(this).removeClass("open");
	});
	$(".help-sele").find("a").on("click",function(){
		var text=$(this).text();
		$(this).parent("dd").siblings("dt").html(text);
		$(".help-sele").removeClass("open");
	});

	/*help btn*/
	$(".header-main").on("click",".help a",function(){
		// 判断登录状态 
        if (Cookies.get('login') != "true") {            
			$(this).parents("ul").siblings(".help-cent").show();
            return false;
        }
	});
	$(".header-main").on("click",".close",function(){
		$(this).parents(".help-cent").hide();
	});
	$(".header-main").on("click",".btn a",function(){
		$(this).parents(".help-cent").hide();
	});

	/*pic click big*/
	/*$(".pic-items").on("click","a",function(){
		var bigBox=$(this).parents(".pic-items").siblings(".big-pic"),
			imgSrc=$(this).find("img").attr("src");
		$(".big-pic").hide();
		$(".bgm-items").find(".item").css("z-index","1");
		$(this).parents(".item").css("z-index","2");
		bigBox.show();
		bigBox.find("img").attr("src",imgSrc);
	});
	$(".big-pic").on("click","img",function(){
		$(".big-pic").hide();
	});
*/
	/*answer btn*/
	/*$(".ans-tiems").on("click",".an-btn a",function(){
		var sorollTop=$(document).scrollTop(),
			winHeight=$(window).height(),
			domHeight=$(document).height();
		$(".layer-main").css("top",sorollTop+(winHeight/2)-88+"px");
		$(".layer-bg").css("height",domHeight+"px");
		$(".layer-bg,.layer-main").show();
	});

	$(".layer-main").on("click",".btn a",function(){
		$(".layer-bg").hide();
		$(this).parents(".layer-main").hide();
	});
	$(".layer-main").on("click",".close",function(){
		$(".layer-bg").hide();
		$(this).parents(".layer-main").hide();
	});*/

    /*按悬赏帮豆*/
    $(".sele-radio").on("click","li",function(){
        $(this).addClass("cur").siblings("li").removeClass("cur");
    });
});