(function(window, $){

    var koreaTripFed = window.koreaTripFed = {};

    koreaTripFed.tripInit = function(){
        //享受生活图片切换
        var J_trip_food = $('.J_trip_food');
        J_trip_food.each(function(){
            var $this = $(this),
                J_food_img = $this.find('.J_food_img'),
                J_food_list = $this.find('.J_food_list'),
                _item = J_food_list.find('dl'),
                J_food_tit = $this.find('.J_food_tit');
            J_food_img.attr({
                src: _item.eq(0).find('a').attr('data-pic'),
                alt: _item.eq(0).find('a').text(),
                title: _item.eq(0).find('a').text()
            });
            J_food_tit.html(_item.eq(0).find('a').text());
            _item.mouseenter(function(){
                var $self = $(this),
                    _data_pic = $self.find('a').attr('data-pic'),
                    _title = $self.find('a').text();
                J_food_img.attr({
                    src: _data_pic,
                    alt: _title,
                    title: _title
                });
                J_food_tit.html(_title);
            });
        });
        //汇率换算
        var _rate = parseFloat($('#J_rate').val()),
            _money = parseFloat($('#J_money').val()),
            _exchange_result = $('#J_exchange_result');
        _exchange_result.html(_money / _rate);
        //地图
        var swfdata = {
            "data":[
                {"name":"首尔","x":337,"y":80,"position":"top","url":"http://korea.b5m.com/lyxd/md/106-1.html","info": "世界十大国际大都市之一，韩国的经济政治文化中心。"},
                {"name":"江原道","x":412,"y":56,"position":"bottom","url":"http://korea.b5m.com/lyxd/jyd/102-1.html","info": "韩剧里不可或缺的取景地，是个浪漫风景秀丽的地方。"},
                {"name":"仁川","x":306,"y":88,"position":"bottom","url":"http://korea.b5m.com/lyxd/rc/96-1.html","info": "韩国的第三大城市，拥有最先进的大型国际机场。"},
                {"name":"京畿道","x":346,"y":110,"position":"bottom","url":"http://korea.b5m.com/lyxd/jjd/100-1.html","info": "在汉江滋润下形成了肥沃的平野，自然风景秀丽。"},
                {"name":"郁陵岛","x":561,"y":86,"position":"bottom","url":"http://korea.b5m.com/lyxd/yld/99-1.html","info": "郁陵岛为一火山岛，是火山喷发后形成的钟状火山岛。"},
                {"name":"大邱","x":430,"y":176,"position":"top","url":"http://korea.b5m.com/lyxd/dq/98-1.html","info": "四周有群山环抱，琴湖江穿过市区，流入洛东江。"},
                {"name":"庆州","x":469,"y":215,"position":"top","url":"http://korea.b5m.com/lyxd/qz/97-1.html","info": "是古代新罗王国首都金城，有“无围墙的博物馆”之称。"},
                {"name":"釜山","x":459,"y":269,"position":"top","url":"http://korea.b5m.com/fsg/89-1.html","info": "韩国的第二大城市，“釜山国际电影节”闻名世界。"},
                {"name":"济州岛","x":308,"y":395,"position":"top","url":"http://korea.b5m.com/lyxd/jzd/101-1.html","info": "是韩国最大的岛屿，是世界新七大自然奇观之一。"}
            ]
        };

        console.info(JSON.stringify(swfdata));

        swfobject.embedSWF("/static/opt/swf/travelkorea/mainv1.swf","swftravel", "700", "430", "11.0.0", "expressInstall.swf",{'datas':JSON.stringify(swfdata)},{'wmode':'transparent'});
    };

    koreaTripFed.tripFun = function(){
        var _this = this;

        _this.tripInit();

        //轮播图
        $('.gallerypic').b5mGalleryPic({
            maction: 'mouseover'
        });
    };
})(window, jQuery);