$(function() {
	$('#recharge').click(function() {
		if (!$('#agree').is(":checked")) {
			alert('请认真阅读协议, 有任何疑问请和客服人员联系');
			return false;
		}
		var price = $('.mn-text').val();
		var id = $("input[name='priceId']:checked").val();
		var need = $('#need').is(":checked") ? 1 : 0;
		if (price == 0) {
			price = null;
		}
		if (!price && !id) {
			alert("请选择充值金额");
			return false;
		}
		$('#price').val(price);
		$('#rechargeDataId').val(id);
		$('#needInvoice').val(need);
		$('#order').submit();
		return false;
	});
	$('.mn-text').keydown(function(event) {
		return keyPressLimitNumber(event, this);
	});
});
function keyPressLimitNumber(event, obj) {
	var e = window.event || event;
	var code = parseInt(e.keyCode || e.which);
	if (code >= 48 && code <= 57) {
		var val = $(obj).val();
		if (parseInt(val + (code - 48)) > 50000) {
			$(obj).val('50000');
			return false;
		}
		return true;
	}
	if (code == 46 || code == 8 || (code >= 96 && code <= 105))
		return true;
	return false;
}