Object.prototype.Clone = function() {
    var b;
    if (this.constructor == Object) {
        b = new this.constructor()
    } else {
        b = new this.constructor(this.valueOf())
    }
    for (var a in this) {
        if (b[a] != this[a]) {
            if (typeof(this[a]) == "object") {
                b[a] = this[a].Clone()
            } else {
                b[a] = this[a]
            }
        }
    }
    b.toString = this.toString;
    b.valueOf = this.valueOf;
    return b
};
function clone(c) {
    if (typeof(c) != "object") {
        return c
    }
    if (c == null) {
        return c
    }
    var b = new Object();
    for (var a in c) {
        b[a] = clone(c[a])
    }
    return b
}
var numbpic = $(".numb-pic");
function changeShow(a) {
    var_data = json_data.Clone();
    if (var_data[a] != undefined && var_data[a] != null) {
        numbpic.find("#grap").remove();
        numbpic.append("<div id='grap'></div>");
        myData = var_data[a];
        myChart = new JSChart("grap", "line");
		if ( myData.length > 1) {
			myChart.setDataArray(myData);
			myChart.setTitle("");
			myChart.setTitleColor("#8E8E8E");
			myChart.setTitleFontSize(11);
			myChart.setAxisNameX("");
			myChart.setAxisNameY("");
			myChart.setAxisColor("#EB7E00");
			myChart.setAxisValuesColor("#949494");
			myChart.setAxisPaddingLeft(60);
			myChart.setAxisPaddingRight(10);
			myChart.setAxisPaddingTop(20);
			myChart.setAxisPaddingBottom(50);
			myChart.setAxisValuesDecimalsY(0);
			myChart.setAxisValuesNumberX(myData.length);
			myChart.setAxisValuesAngle(myData.length);
			myChart.setLineColor("#EB7E00");
			myChart.setLineWidth(2);
			myChart.setFlagColor("#EB7E00");
			myChart.setFlagFillColor("#EB7E00");
			if (!+ [1]) {
				num = 1
			} else {
				num = 4
			}
			myChart.setFlagRadius(num);
			for (var b in myData) {
				myChart.setTooltip([myData[b][0], myData[b][1]])
			}
			myChart.setSize(680, 250);
			myChart.draw()
		}
    }
}
changeShow(0);