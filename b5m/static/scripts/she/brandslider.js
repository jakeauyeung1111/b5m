(function(window, $) {

    var $sliderList = $('.slider');
    $sliderList.length && $('.slider').each(function() {
        var $slider = $(this);
        var $sliderNav = $slider.find('.slider-nav > .slider-nav-li');
        window.NS.publics.tab($sliderNav, $slider.find('.slider-item'), 'cur', 'slider-item-cur', $slider.find('>.slider-prev'), $slider.find('>.slider-next'), function(pageHTML) {
            $slider.find('.slider-page').html(pageHTML);
        });
        $slider.find('.slider-page').on('click', 'a[class!=more]', function(e) {
            e.preventDefault();
            var pageCont = $slider.find('.slider-item:visible');
            var ul = pageCont.find('ul');
            var pageHeight = pageCont.height();
            if (!$(this).hasClass('cur')) {
                var page = ($(this).html() | 0) - 1;
                pageCont.animate({
                    scrollTop: page * pageHeight
                }, 300);
                $(this).addClass('cur').siblings('.cur').removeClass('cur');
            }
        });
    });



    //本周明星
    var weekStarLen = $('.info-scroll li').length,
        curWeekStar = weekStarLen - 1,
        infoScroll = $('.info-scroll'),
        infoNext = $('.info .next'),
        infoPrev = $('.info .prev');
    window.onload = function() {
        infoScroll.scrollTop(curWeekStar * 130);
    }

    infoNext.click(function() {
        if (curWeekStar < weekStarLen - 1) {
            curWeekStar++;
            infoScroll.animate({
                scrollTop: curWeekStar * 130
            });
            if (curWeekStar === weekStarLen - 1) {
                $(this).addClass('last');
            }
            if (curWeekStar != 0) {
                infoPrev.removeClass('last');
            }
        }
    });
    infoPrev.click(function() {
        if (curWeekStar > 0) {
            curWeekStar--;
            infoScroll.animate({
                scrollTop: curWeekStar * 130
            });
            if (curWeekStar === 0) {
                $(this).addClass('last');
            }
            if (curWeekStar != weekStarLen - 1) {
                infoNext.removeClass('last');
            }
        }
    });

    function ToSlide(oL, oR, slideDiv, vNum) {

        this.oS = $(slideDiv);
        this.vNum = vNum;
        this.list = this.oS.find('li');
        this.l = $(oL);
        this.r = $(oR);
        this.w = this.list.first().outerWidth(true);
        this.index = 0;
        this._init();

    }

    ToSlide.prototype = {

        _init: function() {

            var _this = this;

            var initedW;
            var chilredSize = this.list.size();
            this.list.last().css({
                'marginRight': '0'
            });

            initedW = chilredSize * this.w;

            this.oS.children().eq(0).width(initedW);

            this.oS.scrollLeft(0);

            this.l.bind('click', function() {
                _this.toRight(_this.w);
            });
            this.r.bind('click', function() {
                _this.toLeft(_this.w);
            });

        },

        toLeft: function(w) {

            var _this = this;

            if (this.oS.is(':animated')) return;
            if (this.index == this.list.length - this.vNum) return;

            this.index++;
            this.l.removeClass('sl_disable');
            if (this.index == this.list.length - this.vNum) $(this).trigger('disable', 1);

            this.oS.stop(true, true).animate({
                'scrollLeft': '+=' + w + 'px'
            }, 'fast');
        },

        toRight: function(w) {

            var _this = this;
            if (this.oS.is(':animated')) return;

            if (this.index === 0) return;
            this.r.removeClass('sl_disable');
            this.index--;

            if (this.index === 0) $(this).trigger('disable', 0);

            this.oS.stop(true, true).animate({
                'scrollLeft': '-=' + w + 'px'
            }, 'fast');


        }
    };


    $(function() {
        var oSlider = new ToSlide('.brand-slider .b-l', '.brand-slider .b-r', '.brand-slider .mask', 3);
        oSlider.l.addClass('sl_disable');
        $(oSlider).bind('disable', function(e, n) {
            if (n) {
                this.r.addClass('sl_disable');

            } else {
                this.l.addClass('sl_disable');
            }
        });

        $('.brand-slider li').hover(function() {
                $(this).find('.bg').show();
                $(this).find('.text').show();
            },
            function() {
                $(this).find('.bg').hide();
                $(this).find('.text').hide();
            });


        var oSlider2 = new ToSlide('.slider2 .b-l', '.slider2 .b-r', '.slider2 .mask', 4);
        oSlider2.l.addClass('sl_disable');
        $(oSlider2).bind('disable', function(e, n) {
            if (n) {
                this.r.addClass('sl_disable');

            } else {
                this.l.addClass('sl_disable');
            }
        });

    });

})(window, jQuery);