/*@name: 自定义提示,
 *@version: 1.0,
 *@author: jimmy.zhang.
 */
$.fn.Tips = function(option) {
    option = $.extend({}, {x: 0, y: 0}, option);
    $.each(this, function() {
        this.customTitle = this.title;
        $(this).mouseenter(function() {
            this.title = '';
            var customTitle = '<div class="custom-title">' + this.customTitle + '</div>';
            $(this).append(customTitle);
            $('.custom-title').css({
                'top': option.y + 'px',
                'left': option.x + 'px'
            }).show();
        }).mouseleave(function() {
            this.title = this.customTitle;
            $('.custom-title').remove();
        });
    });
};

function go(obj)
{
    if (!obj)
        return false;
    var iTarget = $(obj).offset().top;
    $('html,body').animate({scrollTop: iTarget}, 'fast');
}

/* 插件提示*/
$(window).load(function()
{
    setTimeout(function()
    {
        if (!$('#b5mmain').length)
        {
            $("#plug-bar").slideDown("slow");
        }
    }, 500);
});

/*关闭插件*/
function closed()
{
    $("#plug-bar").slideUp("normal");
}

$(function() {
 
    var t = null;
    $(window).resize(function() {
        if (t) {
            clearTimeout(t);
        }
        ;
        t = setTimeout(function() {
            setSilde();
        }, 50);
    });
    var price_input = $('.filter-price-input');
    price_input.find('input').bind('focusin', function(e) {
        price_input.addClass('filter-price-input_on');
    }).end().find('a.ok').bind('click', function(e) {
        e.preventDefault();
        price_input.removeClass('filter-price-input_on');
        alert('确定');
    }).end().find('a.clear').bind('click', function(e) {
        e.preventDefault();
        price_input.find('input').val('');
    }).end().find('input').bind('input propertychange', function(e) {
        if (e.propertyName && e.propertyName != 'value') {
            return;
        }
        var val = $.trim($(this).val()).replace(/\D/g, '');
        $(this).val(val);
    }).eq(1).bind('focusout', function() {
        var val = parseInt(price_input.find('input:first').val(), 10) || 0;
        val > 0 && $(this).val(Math.max(parseInt($(this).val(), 10) || 0, val));
    });
    $(document).bind('click.price', function(e) {
        var target = $(e.target);
        if ($('.filter-price-input_on').length && !target.is('.filter-price-input_on') && !target.parents('.filter-price-input_on').length) {
            price_input.removeClass('filter-price-input_on');
        }
    });


    function rankHover(obj)
    {
        var oParent = $(obj);
        var aLi = oParent.find('li');
        aLi.hover(function() {
            $(this).css({'background': '#ffede4'});
        }, function() {
            $(this).css({'background': ''});
        });
    }




    function addClassPos()
    {
        $('#pos a').click(function(e) {
            e.preventDefault();
            $(this).addClass('cur').siblings().removeClass('cur');
        });
    }

    function startMove()
    {
        var oPlugTools = $('.plug-tools img');
        oPlugTools.hover(function() {
            oPlugTools.stop(true).animate({'margin-top': '0'});

        }, function() {
            oPlugTools.stop(true).animate({'margin-top': '10px'});
        });
    }

    function modHover()
    {
        $('.mod').hover(function() {
            $(this).addClass('cur');
        }, function() {
            $(this).removeClass('cur');
        });
    }

    $('.logged .per').hover(function() {
        $(this).addClass('hover');
    }, function() {
        $(this).removeClass('hover');
    });

    rankHover('.rank');

    addClassPos();
    startMove();

    //ie6不支持:hover选择器，由jquery添加，
    if (typeof document.body.style.maxHeight === "undefined") {
        modHover();
    }

    $('#back-top a').Tips({x: 27, y: -24});

    function fillZero(num, digit)
    {
        var str = '' + num;

        while (str.length < digit)
        {
            str = '0' + str;
        }

        return parseInt(str, 10);
    }

    function countDown()
    {
        var oNowTime = new Date().getTime();
        var oStartTime = new Date().setHours(9, 59, 59);
        var oEndTime = new Date().setHours(23, 59, 59);
        var iRemain = 0;
        var oStopTime;

        iRemain = (oNowTime - oStartTime) / 1000;

        if (iRemain > 0)
        {
            $('.down-now').addClass('down-end');
            oStopTime = (oEndTime - oNowTime) / 1000;
            formatTime(oStopTime);
        }
        else
        {
            $('.down-now').removeClass('down-end');
            formatTime(-iRemain);
        }
    }

    function formatTime(iRemain)
    {
        var iHour = 0;
        var iMin = 0;
        var iSec = 0;

        iHour = parseInt((iRemain / 3600), 10);
        iRemain %= 3600;

        iMin = parseInt((iRemain / 60), 10);
        iRemain %= 60;

        iSec = iRemain;
        $('#J_dealCountDown').html('<span>' + fillZero(iHour, 2) + '<i></i></span><span>' + fillZero(iMin, 2) + '<i></i></span><span>' + fillZero(iSec, 2) + '<i></i></span>')
        $('.count-down').html('<span class="hour">' + fillZero(iHour, 2) + '</span><span class="minute">' + fillZero(iMin, 2) + '</span><span class="second">' + fillZero(iSec, 2) + '</span>');
    }
    countDown();
    setInterval(countDown, 1000);


    //兑换中心

    function popUpBox()
    {
        var iNv = $('#J_nominal_value');
        var iNv2 = $('#J_nominal_value2');
        var iNte = $('#J_nominal_inte');
        var iNPic = $('#J_nominal_pic');

        var iQv = $('#Q_nominal_value');
        var iQv2 = $('#Q_nominal_value2');
        var iQte = $('#Q_nominal_inte');
        var iQPic = $('#Q_nominal_pic');
        var bBtn = true;

        $('.m-calls a').on('click', function(e) {
            e.preventDefault();
            $('.pop-mask').show();
            $('#J_calls').show();
            if (bBtn)
            {
                $('.opt-suc').show();
                $('.opt-err').hide();
            }
            else
            {
                $('.opt-suc').hide();
                $('.opt-err').show();
            }
            var id = ($(this).attr('id') || '').toString().indexOf('J_nv_') > -1 ? $(this).attr('id').split('_')[2] : 0;
            if (id) {
                var id = $(this).attr('id').indexOf('J_nv_') > -1 && $(this).attr('id').split('_')[2];
                iNv.html(id);
                iNv2.html(id);
                iNte.html(id + '00');
                iNPic.html('<img src="../images/tao/m_' + id + '.png" alt="">');
            }

            $('.pop-goto').on('click', function() {
                $('.pop-suc').show();
                setTimeout(function() {
                    $('.pop-suc,.pop,.pop-mask').hide();
                }, 3000);
            });
        });

        $('.q-calls a').on('click', function() {
            $('.pop-mask').show();
            $('#Q_calls').show();

            if (bBtn)
            {
                $('.opt-suc').show();
                $('.opt-err').hide();
            }
            else
            {
                $('.opt-suc').hide();
                $('.opt-err').show();
            }

            if ($(this).attr('id') == 'Q_nv_20')
            {
                iQv.html('20');
                iQv2.html('20');
                iQte.html('20000');
                iQPic.html('<img src="../images/tao/q_20.png" alt="">');
            }

            if ($(this).attr('id') == 'Q_nv_30')
            {
                iQv.html('30');
                iQv2.html('30');
                iQte.html('30000');
                iQPic.html('<img src="../images/tao/q_30.png" alt="">');
            }

            if ($(this).attr('id') == 'Q_nv_50')
            {
                iQv.html('50');
                iQv2.html('50');
                iQte.html('50000');
                iQPic.html('<img src="../images/tao/q_50.png" alt="">');
            }

            if ($(this).attr('id') == 'Q_nv_100')
            {
                iQv.html('100');
                iQv2.html('100');
                iQte.html('100000');
                iQPic.html('<img src="../images/tao/q_100.png" alt="">');
            }

            $('.pop-goto').on('click', function() {
                $('.pop-suc').show();
                setTimeout(function() {
                    $('.pop-suc,.pop,.pop-mask').hide();
                }, 3000);
            });
        });
    }
    popUpBox();
});

function popUpBoxClosed()
{
    $('.pop-mask').hide();
    $('#J_calls').hide();
    $('#Q_calls').hide();
}
// check window width if more than 1400 change class
// (function check_win(){
//     var win_w = window.screen.availWidth;
//     if(win_w >= 1400){
//         $('.w980').addClass('w1200').removeClass('w980');
//     }
// })();

$(function(){
    $('#page_num').keyup(function(e){
        this.value = this.value.replace(/\D/g,"")
    });
});