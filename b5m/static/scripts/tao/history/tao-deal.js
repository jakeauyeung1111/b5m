$(function() {
    $('.deal-sub-main ul li:last').css('border-bottom', 'none');
    var maxlengthTimer = false;
    var commentText = $("#commentText");
    var commentTextNum = $("#commentTextNum");
    var updateNum = function(num) {
        commentTextNum.html(num);
    }
    commentText.val('').bind('input propertychange', function(e) {
        if (e.originalEvent && e.originalEvent.propertyName && e.originalEvent.propertyName != 'value') { //取到原始的event，判断是否修改了值
            return;
        }
        var val = commentText.val()
        if (val.length > 140) {
            maxlengthTimer = setTimeout(function() {
                commentText.val(val.substr(0, 140));
            }, 50);
        } else {

        }
        updateNum(140 - Math.min(val.length, 140));
    });
        var placeholder = commentText.attr('placeholder');
        commentText.removeAttr('placeholder','');
        commentText.bind('focus', function() {
            if ($(this).val() == placeholder) {
                $(this).val('').css('color', '#333')
            }
        }).bind('blur', function() {
            if ($.trim($(this).val()) == '') {
                $(this).val(placeholder).css('color', '#999');
                updateNum(140);
            }
        }).trigger('blur');

    if (typeof document.body.style.maxHeight != 'undefined') {
        var $deal_now = $(".deal-bott").parent();
        var deal_left = $('.deal:first').offset().left;
        var offsetTop = $("#J_wrap").height() - 18;
        var setFixed = function(){
            var top = $(window).scrollTop();
            if (top > 517 && top < offsetTop - $(window).height() + 210 && !$deal_now.data('fixed')) {
                $deal_now.data('fixed', true).css({
                    position: 'fixed',
                    bottom: '0',
                    left: deal_left,
                    margin: 0,
                    zIndex: 999
                });
            } else if ((top <= 517 || top >= offsetTop - $(window).height() + 210) && $deal_now.data('fixed')) {
                $deal_now.removeData('fixed').css({
                    position: 'static',
                    margin: '0 0 18px 0',
                    left: 0
                });
            }
        }
        $(window).bind('scroll.deal', function() {
            setFixed();
        });
        $(window).resize(function() {
            deal_left = $('.deal:first').offset().left;
            $deal_now.data('fixed') && $deal_now.css('left', deal_left);
            
        });
        setTimeout(function() {
            offsetTop += 38;
            setFixed();
        }, 1100);
    }


});