$(function() {


    //定位
    var $pos = $("#pos > div"), $filter = $("#J_filterOrderby");
    $pos.length && scrollFixed();//仅在有这两个层的页面才绑定scroll.fixed
    function  scrollFixed() {
        if(typeof document.body.style.maxHeight == 'undefined'){
            return;
        }
        if (!$filter || !$filter.length) {
            $filter = null;
        }
        if(!$pos.length && !$filter){
            return;
        }
        var Fix_top = [$pos.parent().offset().top, $filter ? $filter.next().offset().top : 0];
        setTimeout(function() {
            var change = $("#plug-bar:visible").length;
            if (change) {
                Fix_top = [Fix_top[0] + 38, Fix_top[1] + 38];//显示插件安装提示的层slidedown后 需要修正高度
            }
        }, 1100);
        var fixed = false;//fixed状态 以免scroll过程一直设置样式

        $(window).unbind('scroll.fixed').bind('scroll.fixed', function() {
            var t = $(window).scrollTop();
            if (t > Fix_top[0]) {
                $pos.css({
                    position: 'fixed',
                    top: 0
                });
            } else {
                $pos.css({
                    position: 'static'
                });
            }
            if ($filter && t > Fix_top[1] && fixed === false) {
                $filter.css({
                    position: 'fixed',
                    top: 0,
                    left: $("#filter").offset().left,
                    'z-index': 5,
                    bottom: 'auto'
                }).next().css('top', 0);
                fixed = true;
            } else if (fixed === true && t <= Fix_top[1]) {
                $filter.css({
                    position: 'absolute',
                    top: 'auto',
                    left: 0,
                    'z-index': 0,
                    bottom: 0
                }).next().css('top', -40);
                fixed = false;
            }
        }).trigger('scroll.fixed');
    }

    var setSilde = function() {
        if ($(window).width() < 1260) {
            $('.filter-classify').addClass('filter-classify-show');
            $("#pos").hide();
        } else {
            $('.filter-classify').removeClass('filter-classify-show');
            $("#pos").show();
        }
        $("#pos > div").length && scrollFixed();
    };
    setSilde();

    //价格区间
    var t = null;
    $(window).resize(function() {
        if (t) {
            clearTimeout(t);
        }
        ;
        t = setTimeout(function() {
            setSilde();
        }, 50);
    });
    var price_input = $('.filter-price-input');
    var timer = false;
    price_input.find('input').bind('focusin', function(e) {
        price_input.addClass('filter-price-input_on');
    }).end().find('a.ok').bind('click', function(e) {
            e.preventDefault();
            price_input.removeClass('filter-price-input_on');
            var $first = price_input.find('input:first');
            var $last = price_input.find('input:last');
            var firstVal = $first.val();
            var lastVal = $last.val();
            if(parseInt(firstVal) == Math.max(firstVal, lastVal)){
                $last.val(Math.max(firstVal, lastVal));
                $first.val(lastVal);
            }
            changePrice($('#sPrice').val(),$('#ePrice').val());
        }).end().find('a.clear').bind('click', function(e) {
            e.preventDefault();
            price_input.find('input').val('');
        }).end().find('input').bind('input propertychange', function(e) {
            if (e.propertyName && e.propertyName != 'value') {
                return;
            }
            var $this = $(this);
            clearTimeout(timer);
            timer = setTimeout(function(){
                var val = $.trim($this.val()).replace(/\D/g,'');
                $this.val(val);
            },50);
        }).eq(1).bind('focusout', function() {
            var $val = price_input.find('input:first');
            var focuVal = $(this).val();
            if(parseInt($val.val()) == Math.max(focuVal, $val.val())){
                $(this).val(Math.max(focuVal, $val.val()));
                $val.val(focuVal);
            }
        });
    $(document).bind('click.price', function(e) {
        var target = $(e.target);
        if ($('.filter-price-input_on').length && !target.is('.filter-price-input_on') && !target.parents('.filter-price-input_on').length) {
            price_input.removeClass('filter-price-input_on');
        }
    });


    //mod hover
    $('.mod-list .mod').hover(function() {
        $(this).addClass('cur');
    }, function() {
        $(this).removeClass('cur');
    });



    //右边分类点击效果
    $('#pos a').click(function(e) {
        e.preventDefault();
        $('#pos a').removeClass('cur');
        $(this).addClass('cur');
    });

    //水印
    (function(){
        var deal_img_big = $('.deal-img'),
            $img = deal_img_big.find('.water'),
            _w = deal_img_big.width();
        if ($img.length) {
            deal_img_big.append('<img src="http://staticcdn.b5m.com/images/common/logo-water.png" class="logo-water-icon" />');
            var logo_water_icon = deal_img_big.find('.logo-water-icon');
            logo_water_icon.css('left', (_w - 128) / 2 + 'px');
        }
    })();

});
