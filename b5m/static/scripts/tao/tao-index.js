(function(window,$) {


    //tab 切换效果
    var tabTitle = $('.tab-title'),tabContent = $('.tab-content');
    tabTitle.each(function(i,n) {

        var content = tabContent.eq(i);
        $(n).find('span').each(function(i1,n1) {
            $(n1).click(function() {

                $(n).find('span.cur').removeClass('cur');
                $(n1).addClass('cur');
                content.find('.content.cur').removeClass('cur');
                content.find('.content').eq(i1).addClass('cur');

            });
        });

    });


    //左边slider
    $('.main-slider').each(function() {

        var elem = $(this),
            container = elem.find('.container'),
            prev =elem.find('.prev'),
            next = elem.find('.next'),
            len = container.find('li').length,
            width = 700,
            index =0;

        container.find('ul').width(700*len);

         if(len<=1) {
             next.addClass('last');
         }
 
        prev.click(function() {
      
            if(index > 0) {
                index --;
                container.animate({scrollLeft:index*width});
                if(index === 0) {
                    $(this).addClass('first');
                }
                if(index!=len-1) {
                    next.removeClass('last');
                }
            }
        });

        next.click(function(){

            if(index< len-1) {
                index ++;
                container.animate({scrollLeft:index*width});
                if(index === len-1) {
                    $(this).addClass('last');
                }
                if(index!=0) {
                    prev.removeClass('first');
                }
            }
        });

    });


  //右边slider
  var subSliderElem = $('.sub-slider'),subSliderNum = $('.sub-slider-num'),subScrollWidth=200,surSliderDesc = $('.today').find('.desc');
    subSliderElem.each(function(i,elem) {

      var len = $(elem).find('li').length,
          subIndex=len,
          subNums = subSliderNum.eq(i).find('ol'),
          subSliderTimer = null,
          desc =surSliderDesc.eq(i) ;

      $(elem).find('ul').width(len * 200);

      for(var li =0; li <len;li++) {
          subNums.append('<li>'+ i +'</li>');
      }

      var subSlider = function subslider() {

          clearTimeout(subSliderTimer);

          if(subIndex===len) {
              subIndex=0;
          }

          $(elem).animate({scrollLeft:subIndex*subScrollWidth});

          subIndex++;
          subNums.find('.cur').removeClass('cur');
          subNums.find('li').eq(subIndex-1).addClass('cur');

          desc.find('.cur').removeClass('cur');
          desc.find('div').eq(subIndex-1).addClass('cur');

          subSliderTimer =  setTimeout(function() {
              subslider();
          },6000);

          return subslider;

      }();

      subNums.find('li').each(function(i,n) {
          $(n).click(function() {
              subIndex = i;
              subSlider();
          });
      });

  });







    /**
     * @description class TimeCD，依赖jQuery
     * @param {String} 时间 如'2013/9/13 18:48'
     * @param {jQuery expr} jq dom选择器表达式
     * @param {fn} 倒计时结束后回调 可选 *
     * @example new TimeCD('2013/9/15 18:00','#cd2',function(){alert('倒计时结束')});
     *  主要实例方法 :getTime():int// 获取当前距离结束时间秒数；
     */

    TimeCD.prototype = {

        init:function(endTime,dom,fn){

            var _this = this;
            this.version= '1.0';
            this.dom = $(dom);
            this.bF = true;
            this.fnTimeover = fn?fn:$.noop;

            this.creatDom();

            this.$domh = this.dom.find('.t0');
            this.$domm = this.dom.find('.t1');
            this.$doms = this.dom.find('.t2');
            this.lay1 = this.dom.find('.lay1');
            this.lay2 = this.dom.find('.lay2');
            this.b = this.dom.find('b');

            this.setStyle();

            this.expireTime =Math.floor((Date.parse(endTime) - new Date().getTime())/1000);
            this.time = setInterval(function(){_this.expireCheck()},1000);

        },

        getTime:function(){

            return this.expireTime;
        },

        creatDom:function(){

            for(var i=0;i<3;i++){
                this.dom.append('<div class="t-box t'+i+'"><div class="lay1"><b>00</b></div><div class="lay2"><b>00</b></div><div class="lay1"><b>00</b></div><div class="lay2"><b>00</b></div></div><div class="dot"></div>')
            }
            this.dom.find('.dot:last').remove();

        },

        setStyle:function(){

            this.dom.find('.t-box').css({position:'relative','minWidth':'20px','minHeight':'20px'});
            this.lay1.css({position:'absolute',left:0,top:0,width:'100%',height:'49%',overflow:'hidden'});
            this.lay2.css({position:'absolute',left:0,top:'52%',width:'100%',height:'49%',overflow:'hidden'});
            this.lay2.find('b').css({position:'relative',top:'-100%'});

        },

        expireCheck:function (){

            this.expireTime--;
            if(this.expireTime <= 0){
                clearInterval(this.time );
                this.$doms.find('b').html('00');
                this.fnTimeover();
            }else{
                var aTime = this.secToTime(this.expireTime);
                this.insertTime(aTime);
            }
        },

        secToTime:function(sec){
            var aTime = [];
            aTime[0] = Math.floor(sec/3600);

            if(parseInt(aTime[0])>99){
                clearInterval(this.time );
                throw new Error('相隔时间太长了,把时间写短点吧 错误对象：'+this);
                return false;
            };
            sec %=3600;
            aTime[1] = Math.floor(sec/60);
            sec %=60;
            aTime[2]  = sec;

            return aTime;
        },

        insertTime:function(aTime){
            var _this = this;
            var st =this.getSt(aTime);
            $.each(aTime,function(i){
                if(aTime[i]<10){
                    aTime[i]='0'+aTime[i];
                };
            });
            this.wtTime(st,aTime);
        },
        wtTime:function(n,aTime){

            switch(n){
                case 0:
                    this.$domh.find('b').html(aTime[0]);
                    this.$domm.find('b').html(aTime[1]);
                    this.$doms.find('b').html(aTime[2]);
                    break;
                case 1:
                    this.$domm.find('b').html(aTime[1]);
                    this.$doms.find('b').html(aTime[2]);
                    break;
                default:
                    this.$doms.find('b').html(aTime[2]);
            }

            this.dom.find('.t-box').filter(function(index){return index>=n}).each(function(){

                var $domActTop = $(this).find('>div').eq(2).addClass('acttop').css({'zIndex':10});
                var $domActbot = $(this).find('>div').eq(3).addClass('actbot').css({'zIndex':10});
                setTimeout(function(){
                    $domActTop.removeClass('acttop');
                    $domActbot.removeClass('actbot');
                },50);
            })

        },
        getSt:function(aTime){
            if(aTime[1] == 59 && aTime[2] == 59)return 0;
            if(aTime[2] == 59)return 1;

            if(this.bF){
                this.bF = false;
                return 0;
            }
            return 2;
        }
    }

})(window,jQuery);


function TimeCD(){
  this.init.apply(this,arguments);
}