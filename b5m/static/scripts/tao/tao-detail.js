$(function() {
	//comment
	$('.deal-sub-main ul li:last').css('border-bottom', 'none');
    var maxlengthTimer = false;
    var commentText = $("#commentText");
    var commentTextNum = $("#commentTextNum");
    var updateNum = function(num) {
        commentTextNum.html(num);
    }
    commentText.val('').bind('input propertychange', function(e) {
        if (e.originalEvent && e.originalEvent.propertyName && e.originalEvent.propertyName != 'value') { //取到原始的event，判断是否修改了值
            return;
        }
        var val = commentText.val()
        if (val.length > 140) {
            maxlengthTimer = setTimeout(function() {
                commentText.val(val.substr(0, 140));
            }, 50);
        } else {

        }
        updateNum(140 - Math.min(val.length, 140));
    });
    var placeholder = commentText.attr('placeholder');
    commentText.removeAttr('placeholder','');
    commentText.bind('focus', function() {
        if ($(this).val() == placeholder) {
            $(this).val('').css('color', '#333')
        }
    }).bind('blur', function() {
        if ($.trim($(this).val()) == '') {
            $(this).val(placeholder).css('color', '#999');
            updateNum(140);
        }
    }).trigger('blur');
	// price fix effect
    if (typeof document.body.style.maxHeight != 'undefined') {
        var $deal_now = $(".deal-bott").parent();
        var deal_left = $('.deal:first').offset().left;
        var offsetTop = $("#J_wrap").height() - 18;
        var setFixed = function(){
            var top = $(window).scrollTop();
            if (top > 517 && top < offsetTop - $(window).height() + 210 && !$deal_now.data('fixed')) {
                $deal_now.data('fixed', true).css({
                    position: 'fixed',
                    bottom: '0',
                    left: deal_left,
                    margin: 0,
                    zIndex: 999
                });
            } else if ((top <= 517 || top >= offsetTop - $(window).height() + 210) && $deal_now.data('fixed')) {
                $deal_now.removeData('fixed').css({
                    position: 'static',
                    margin: '0 0 18px 0',
                    left: 0
                });
            }
        }
        $(window).bind('scroll.deal', function() {
            setFixed();
        });
        $(window).resize(function() {
            deal_left = $('.deal:first').offset().left;
            $deal_now.data('fixed') && $deal_now.css('left', deal_left);
            
        });
        setTimeout(function() {
            offsetTop += 38;
            setFixed();
        }, 1100);
    }	
});
function fillZero(num, digit)
{
    var str = '' + num;

    while (str.length < digit)
    {
        str = '0' + str;
    }

    return parseInt(str, 10);
}

function countDown()
{
    var oNowTime = new Date().getTime();
    var oStartTime = new Date().setHours(9, 59, 59);
    var oEndTime = new Date().setHours(23, 59, 59);
    var iRemain = 0;
    var oStopTime;

    iRemain = (oNowTime - oStartTime) / 1000;

    if (iRemain > 0)
    {
        $('.down-now').addClass('down-end');
        oStopTime = (oEndTime - oNowTime) / 1000;
        formatTime(oStopTime);
    }
    else
    {
        $('.down-now').removeClass('down-end');
        formatTime(-iRemain);
    }
}

function formatTime(iRemain)
{
    var iHour = 0;
    var iMin = 0;
    var iSec = 0;

    iHour = parseInt((iRemain / 3600), 10);
    iRemain %= 3600;

    iMin = parseInt((iRemain / 60), 10);
    iRemain %= 60;

    iSec = iRemain;
    $('#J_dealCountDown').html('<span>' + fillZero(iHour, 2) + '<i></i></span><span>' + fillZero(iMin, 2) + '<i></i></span><span>' + fillZero(iSec, 2) + '<i></i></span>')
    $('.count-down').html('<span class="hour">' + fillZero(iHour, 2) + '</span><span class="minute">' + fillZero(iMin, 2) + '</span><span class="second">' + fillZero(iSec, 2) + '</span>');
}
countDown();
setInterval(countDown, 1000);