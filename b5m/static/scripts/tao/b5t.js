$(function(){
    function AutoSwitch(obj)
    {
        var oParent = $(obj),
            aLi = oParent.find('.tab-hd li'),
            oBd = oParent.find('.tab-bd-container'),
            aBoxLen = oBd.find('.tab-pannel').length,
            oBoxWidth = parseInt(oParent.find('.tab-pannel').outerWidth(),10),
            allWidth = aBoxLen*oBoxWidth +'px',
            timer = null,
            iNow = 0;

        oBd.css({'width':allWidth});

        aLi.click(function(){
            clearInterval(timer);
            iNow = $(this).index();
            tab();
        });

        function tab()
        {
            aLi.removeClass('cur');
            aLi.eq(iNow).addClass('cur');
            if(aLi !==1) oBd.animate({'left':-oBoxWidth*iNow});
        }

        function timerInner()
        {
            iNow++;
            if(iNow == aLi.size())
            {
                iNow = 0;
            }
            tab();
        }

        timer = setInterval(timerInner,5000);

        oParent.hover(function(){
            clearInterval(timer);
        },function(){
            timer = setInterval(timerInner,5000);
        });

    }

    tankuangEach();
});

function tankuangEach(){
    $('.tankuang').each(function(){
//		$(this).attr('href','/toDialogLogin.do?showUrl='+encodeURIComponent($(this).attr('clickUrl'))+'&url='+encodeURIComponent($(this).attr('sourceUrl')));
        $(this).attr('href',$(this).attr('sourceUrl'));
    });
}

var countPageNumber = typeof countPageNumber === 'undefined'?'':countPageNumber;
//商品分类显示
function loadWare(urlWare,flag,curPageTag){
    $.ajax({
        url : urlWare,
        type : 'get',
        dataType:'json'
    })
        .done(function(data){
            $('.filter-page').remove();
            $(".home-mod").remove();
            $(".page").remove();
            if(curPageTag!='taoTomorrowPreview'){
                $('#wrap-all').html(data);
            }else{
                $('.col-main').html(data);
            }
            tankuangEach();
        });
}


var timeout=null;
function scrollEvent(urlWare){
    if(timeout!=null)return;
    timeout=setTimeout(function(){
        loadWare(urlWare,true);
        timeout=null;
    },500);
};

function tomorrowScroll(urlWare,flag){
    if(timeout!=null)return;
    timeout=setTimeout(function(){
        loadWare(urlWare,flag);
        timeout=null;
    },500);
};

function getCookie(name,url){
    var old_arr = document.cookie.match(new RegExp("(^| )"+name+"=([^;]*)(;|$)"));
    var arr = 'b5m00ttj';
    if(old_arr != null){
        arr = old_arr[0];
        arr = arr.replace('.',0);
    }
    var new_url;
    if(url.indexOf('unid=b5m00ttj') > 0){
        new_url = url.replace('b5m00ttj',arr.substring(arr.indexOf('utmcsr=')+7,arr.indexOf('|')));
    }else if(url.indexOf('&e=') > 0){
        new_url = url.replace('&e=','&e='+arr.substring(arr.indexOf('utmcsr=')+7,arr.indexOf('|')));
    }else{
        new_url = url + "&" + arr.substring(arr.indexOf('utmcsr=')+7,arr.indexOf('|'));
    }
    return new_url || url;
}

window.modInitUrl = function(){
    $('.mod[data-url],li[data-url]').each(function(){
        var url = getCookie('__utmz',$(this).attr('data-url'));
        if($(this).find('.no-mask').length<=0)
            $(this).find('a').attr('href',url);
    });
};
