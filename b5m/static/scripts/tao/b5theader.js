/*
 * 搜索提交------------------------------------
 */
function doSubmit() {
    var keyWordObj = document.getElementById("query");
    var keyWord = trim(keyWordObj.value);

    //判断空
    if (keyWord == "" || keyWord == "*") {
        keyWordObj.value = "";
        keyWordObj.focus();
        return;
    }

    while(true){
        if(keyWord.indexOf('_')>=0){
            keyWord = keyWord.replace("_"," ");
        }else{
            break;
        }
    }

    while(true){
        if(keyWord.indexOf('\\')>=0){
            keyWord = keyWord.replace("\\"," ");
        }else{
            break;
        }
    }

    while(true){
        if(keyWord.indexOf('<')>=0){
            keyWord = keyWord.replace("<","&lt;");
        }else{
            break;
        }
    }

    while(true){
        if(keyWord.indexOf('>')>=0){
            keyWord = keyWord.replace(">","&gt;");
        }else{
            break;
        }
    }

    while(true){
        if(keyWord.indexOf('"')>=0){
            keyWord = keyWord.replace('"','&quot;');
        }else{
            break;
        }
    }

    if(keyWord.length>100){
        alert("关键词不能超过100个字符，请重新输入");
        return;
    }

    //appendHistoryCookie(keyWord);
    //keyWord = encodeURIComponent(keyWord);

    //while(true){
    //	if(keyWord.indexOf('%2F')>=0){
    //		keyWord = keyWord.replace("%2F","/");
    //	}else{
    //		break;
    //	}
    //}
    var url = "/n/ttj/s/"+keyWord;
    goUrlByATag(url);

}


function AddFavorite(){
    var sURL=window.location;
    var sTitle=document.title;
    try{
        window.external.addFavorite(sURL, sTitle);
    }catch (e){
        try{
            window.sidebar.addPanel(sTitle, sURL, "");
        }catch (e){
            alert("您的浏览器不支持此操作,PC用户请使用\"Ctrl+D\"、Mac用户请使用\"Command+D\"进行添加!");
        }}
};

/*
 * 去掉字符串前后空格
 * @param keyWord 没转换前字符串
 * @return 去掉前后空格后的字符串
 */
function trim(keyWord) {
    var regex = /^\s*|\s*$/g;
    if(typeof(keyWord)!='undefined') {
        var trimstr = keyWord.replace(regex, "");
        return trimstr;
    } else {
        return "";
    }
}


/**
 * 通过标签处理url跳转
 * @param url
 */
function goUrlByATag(url){
    var el = document.createElement("a");
    document.body.appendChild(el);
    el.href=url;

    if(el.click) {
        el.click();
    }else{//safari浏览器click事件处理
        try{
            var evt = document.createEvent('Event');
            evt.initEvent('click',true,true);
            el.dispatchEvent(evt);
        }catch(e){//alert(e)

        };
    }
}


/**
 * 搜索框中的标签
 * @returns {Boolean}
 */
function label(){
    var _LABEL = document.getElementById('label');
    if(!_LABEL)return false;
    var _LABEL_VALUE = _LABEL.innerHTML;

    switch (_LABEL_VALUE)
    {
        case '购物':
            _LABEL.innerHTML = '输入想买的东西，购物享优惠';
            break;

        case '逛街':
            _LABEL.innerHTML = '输入关键词，发现购物最流行';
            break;

        case '团购':
            _LABEL.innerHTML = '发现附近的团购';
            break;

        case '搜票':
            _LABEL.innerHTML = '找最近的演出，轻松在线购';
            break;

        case '专题':
            _LABEL.innerHTML = '输入关键字，寻找您喜欢的专题';
            break;

        case '品牌':
            _LABEL.innerHTML = '输入品牌名称，发现大牌商品';
            break;

        case '色界':
            _LABEL.innerHTML = '在27,640,365张色界没图中搜索~';
            break;

        default:
            _LABEL.innerHTML ='快速搜索 全网特价';
            break;
    }
};

/**
 * 搜索框 x 按钮 与 框内文字提示 脚本控制
 */
function odefault(){
    var ie=!-[1,];
    var oVal=document.getElementById("label");
    var oBox=document.getElementById("query");
    var oClose=document.getElementById("closes");
    toChange();
    ie?oBox.onpropertychange=toChange:oBox.oninput=toChange;
    function toChange(){
        if(oBox.value.length>0){
            oVal.style.display="none";
            oClose.style.display="block";
        }else{
            oVal.style.display="block";
            oClose.style.display="none";
        }
    };
    oVal.onclick=oBox.onclick=function(){f();};
    function f(){
        oBox.style.borderColor="#e87e01";
        oBox.focus();
    };
    oBox.onblur=function(){
        oBox.style.borderColor="#9c9c9c";
    };
    //f();
    oClose.onclick=function(){
        oBox.value="";
        oBox.focus();
        //oVal.style.display="block";
        oVal.style.display="none";
        this.style.display="none";
    };
};

/**
 * 高亮标题
 * @param hlLab   includePage
 * @param pnLab   页面名称 pageName
 */
function hightitle(hlLab,pnLab){
    if ('hlSuppliser' == hlLab) {
        $("#hlSuppliser").addClass('cur');
        if ('spl_home' == pnLab) {
            $("#pnSpl").addClass('cur');
        }
    }
}


/**
 * 将淘特价的二级标签 进行高亮现实
 * @param b5tLab  二级标签的名称
 */
function hightb5tLabel(b5tLab){
    if ('b5tH' == b5tLab) {
        $("#b5tH").addClass('cur');
    }else if('b5t9'== b5tLab){
        $("#b5t9").addClass('cur');
    }else if('b5t19'== b5tLab){
        $("#b5t19").addClass('cur');
    }else if('b5tt'== b5tLab){
        $("#b5tt").addClass('cur');
    }

}

/**
 * 搜索框的enter快捷键
 * @param eve
 */
function submitKey(eve) {
    if (!eve) {
        eve = window.event;
    }//火狐中是 window.event
    if ((eve.keyCode || eve.which) == 13) {
        doSubmit();
    }
}


/*********************************tao header js*****************************************/
//分类
function changeCategory(categoryId,page) {
    $("#categoryId").val(categoryId);
    //urlField($("#categoryId").val(),$("#sortField").val(),false);
    ajaxUrlField($("#categoryId").val(),$("#sortField").val(),$("#startPrice").val(),$("#endPrice").val(),page);
}
$("#pos a[data-id]").on('click',function(e){
    e.preventDefault();
    var id = $(this).attr('data-id').split(',');
    $("#categoryId").val(id[0]);
    ajaxUrlField($("#categoryId").val(),$("#sortField").val(),$("#startPrice").val(),$("#endPrice").val(),id[1]);
    $(window).scrollTop(277);
});
//排序
function changeSort(sortField) {
    $("#sortField").val(sortField);
    urlField($("#categoryId").val(),$("#sortField").val(),$("#startPrice").val(),$("#endPrice").val(),true);
}
//价格
function changePrice(startPrice,endPrice) {
    $("#startPrice").val(startPrice);
    $("#endPrice").val(endPrice);
    urlField($("#categoryId").val(),$("#sortField").val(),$("#startPrice").val(),$("#endPrice").val(),true);
}

function urlField(categoryId,sortField,sPrice,ePrice,sortUrl){
    var keyWord = $('#keyWord').val();
    var curPageTag = $('#curPageTag').val();
    var menuCurPageTag = $('#menuCurPageTag').val();
    var url = '/taoPage_'
        + menuCurPageTag
        + '_'
        + categoryId
        + '_'
        + sortField
        + '_'
        + curPageTag;
    if(menuCurPageTag == 'discount'||menuCurPageTag == 'cutprice')
        url = url
            + '_'
            + sPrice
            + '_'
            + ePrice;
    if(curPageTag == 'searchresult')
        url = url
            + '_'
            + keyWord
            + '_'
            + 1;
    else
        url = url + '_1';
    if(sortUrl)
        location.href = url;
    else
        location.href = url + '#category';
}

function ajaxUrlField(categoryId,sortField,sPrice,ePrice,page){
    var keyWord = $('#keyWord').val();
    var curPageTag = $('#curPageTag').val();
    var menuCurPageTag = $('#menuCurPageTag').val();
    var categoryflag = true;
    var url = 'ajaxReturnHtml.do?categoryId='
        + categoryId
        + '&pageType='
        + menuCurPageTag
        + '&sortField='
        + sortField
        + '&ajaxType='
        + curPageTag;
    if(menuCurPageTag == 'discount'||menuCurPageTag == 'cutprice')
        url = url
            + '&startPrice='
            + sPrice
            + '&endPrice='
            + ePrice;
    if(curPageTag == 'searchresult')
        url = url
            + '&keyWord='
            + keyWord
            + '&currPageNo='
            + page;
    else
        url = url + '&currPageNo='+page;
    if(curPageTag=='taoTomorrowPreview'){
        categoryflag= false;
    }
    loadWare(url,categoryflag,curPageTag);
}

////插件提示
//$(window).load(function(){
//	setTimeout(function(){
//		var href = location.href;
//		if(href.indexOf("#category") == -1){
//			if(!$('#b5mmain').length){
//				$("#plug-bar").slideDown("slow"); 
//			}
//		}
//	},500);
//});
//
////关闭插件提示
//function closed(){
//	$("#plug-bar").slideUp("normal");
//}

$(function(){
    //搜索框文字
    label();

    //搜索框 x 按钮 与 框内文字提示 脚本控制
    //odefault();

    $('#taoH').css('color','#e87d01');

    var searchFun=function(){
        var val=$.trim($("#query").val());
        if(val=="")return false;
        while(true){
            if(val.indexOf('\\')>=0){
                val = val.replace("\\"," ");
            }else{
                break;
            }
        }
        val="/taoPage_all_searchresult_"+encodeURIComponent(val);
        $("#btnSearch").attr("href",val);
        location.href=val;
    };
    $("#query").focusin(function(){
        $("#search").addClass("search-hover");
    });
    $("#query").focusout(function(){
        $("#search").removeClass("search-hover");
    });
    $("#btnSearch").click(function(event){
        event.preventDefault();
        searchFun();
    });
    $("#query").keydown(function(event){
        if(event.keyCode==13){
            $("#btnSearch").click();
        }
    });

//	initQueryInput();

    $('.logged .per').hover(function(){
        $(this).addClass('hover');
    },function(){
        $(this).removeClass('hover');
    });

    googleAnalyticsInit(false);//google登录
});

function pageEvent(pageNo){
    var newPageNo = pageNo;
    var reg = /^[1-9][0-9]*$/;
    if(!reg.test(newPageNo)){
        newPageNo = 1;
    }
    var categoryId = $('#categoryId').val();
    var sortField = $('#sortField').val();
    var curPageTag = $('#curPageTag').val();
    var keyWord = $('#keyWord').val();
    var menuCurPageTag = $('#menuCurPageTag').val();
    var sPrice = $("#startPrice").val();
    var ePrice = $("#endPrice").val();
    var totalPages = $('#totalPages').val();
    if(parseInt(newPageNo) > parseInt(totalPages)){
        newPageNo = totalPages;
    }

    var urlWare = '/taoPage_'
        + menuCurPageTag
        + '_'
        + categoryId
        + '_'
        + sortField
        + '_'
        + curPageTag
        + '_';
    if(menuCurPageTag == 'discount'||menuCurPageTag == 'cutprice')
        urlWare = urlWare
            + sPrice
            + '_'
            + ePrice
            + '_';
    urlWare = urlWare + keyWord + '_' + newPageNo;
    location.href = urlWare;
}

//用户已浏览的页面点击事件
function pageEventNew(pageNo){
    var newPageNo = pageNo;
    var reg = /^[1-9][0-9]*$/;
    if(!reg.test(newPageNo)){
        newPageNo = 1;
    }
    $("#hid_page_no").attr("value", newPageNo);
    $("#scan_form").submit();
}

//用户已浏览的页面点击事件
function pageEventNew1(pageNo){
    var newPageNo = pageNo;
    var reg = /^[1-9][0-9]*$/;
    if(!reg.test(newPageNo)){
        newPageNo = 1;
    }
    $("#hid_page_no").attr("value", newPageNo);
    $("#scan_form").submit();
}

//function initQueryInput(){
//
//    $("#label").show();
//    if($("#query").val().length > 0){
//        $('#label').hide();
//    }
//    $("#query").bind("blur",function(){
//        if($("#query").val() == ''){
//            $('#label').show();
//            $(this).css({'border-color':'#9c9c9c'});
//        }
//    }).bind("focus",function(){
//    	$(this).css({'border-color':'#e87e01'});
//        $('#label').hide();
//    });
//    
//    $("#label").bind("click",
//    function(){
//        $('#label').hide();
//        $("#query").focus();
//    });
//}
