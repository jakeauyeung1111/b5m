var b5m=window.b5m||{};
b5m.brand=function(){
$(function(){
  
  setFloatBarFix_init();
  if($(window).width()>=1024){
    $("#TopHotBar").hide(10);
	$("#floatBar").show(10);
	
	setFloatBarFix();
  }
  else{
    
    $("#TopHotBar").show(10);
    $("#floatBar").hide(10);
  }
  $("#Tao_Goto_Top").click(function(){
  var _top=$("#floatBar").attr("TOP")+"px";
    $("#floatBar").css("top",_top);
    $(window).scrollTop(0);
          
  });
  /*$("#saleTabs").find("li").mouseenter(function(){
     var idx=$(this).index();
	 var wd=$(this).width();
     var _left=(0-(idx*wd)-1).toString()+"px";	
     $(this).find("dl").css("left",_left);	 
    $(this).addClass("cur");
	$(this).siblings().removeClass("cur");
  });
  */
 /*鼠标移入移出事件*/ 
  $("#hotBars").find("a").click(function(){
    $(this).addClass("active");
	$($(this).parent().siblings("dd")).find("a").removeClass("active");
  });
  $("#TopHotBar").find("dd").click(function(){
    $(this).addClass("cur");$(this).siblings().removeClass("cur");
  });
  $(".wc-items").mouseenter(function(){
    $(this).css("border","1px solid #b2d82e");
  });
  $(".wc-items").mouseleave(function(){
    $(this).css("border","1px solid #efefef");
  });
  $("#crazy .products").mouseenter(function(){
    $(this).css("border","1px solid #b2d82e");
  });
  $("#crazy .products").mouseleave(function(){
    $(this).css("border","1px solid #efefef");
  });
  /*鼠标移入移出事件 end*/ 
  //倒计时
  $("[time]").each(function(){
    var _this = $(this),
            _start = _this.attr("start"),
			_end=_this.attr("end");
        _this.countTime({
		  startTime:_start,
		  endTime:_end
		  
		});
    });
  
});
function setTopAttrTop(){
$("#TopHotBar").attr("top",$("#TopHotBar").offset().top);

}
//锁定顶部浮动
function setTopFix(){
    var _top=parseInt($("#TopHotBar").attr("top"));
	var _window=$(window).scrollTop();
	if(_window>_top){
	  $("#TopHotBar").addClass("fixs");
	}
	else{
	  $("#TopHotBar").removeClass("fixs");
	}
}
//锁定浮动条
function setSaleTabsFix(){
 var scoll=$(window).scrollTop();
   var realH=$("#saleTabs").find("dl").outerHeight()+$("#saleTabs").outerHeight();
   var minTop=$("#saleTabs").parent().offset().top;
   var maxTop=$("#crazy").offset().top-realH-10;
   var left=$("#saleTabs").offset().left;
   //console.log();
   //console.log();
   //console.log($("#saleTabs").position().top);
   if(scoll>=minTop&&scoll<maxTop){
     $("#saleTabs").css("left",left.toString()+"px");
     $("#saleTabs").addClass("fixs").css("margin-top","0px");
	 
   }
   else if(scoll>=maxTop){
    
     //var mt=$("#saleTabs").position().top-realH+($("#TopHotBar").css("display")=="none"?112:70);
	  var mt=(($("#crazy").offset().top-minTop)-realH)>0?(($("#crazy").offset().top-minTop)-realH-20):0;
	   $("#saleTabs").animate({marginTop:mt.toString()+"px"},12);
	     $("#saleTabs").removeClass("fixs");
	/* if((realH+$("#saleTabs").position().top)<$("#BarRight").outerHeight()){
	     $("#saleTabs").animate({marginTop:mt.toString()+"px"},12);
	     $("#saleTabs").removeClass("fixs");
	 }
     else if((realH+$("#saleTabs").position().top)>$("#BarRight").outerHeight()){
	    mt=$("#BarRight").outerHeight()-realH-40;
		$("#saleTabs").animate({marginTop:mt.toString()+"px"},12);
	    $("#saleTabs").removeClass("fixs");
	 }
	  
   }
   */
   }
   else if(scoll<=minTop){
        $("#saleTabs").removeClass("fixs").css("margin-top","0px");
   }
  
   
}
//浮动bar初始化
function setFloatBarFix_init(){
  
  var _Top=$("#BarRight").offset().top-42;
  var _Left=$("#autoTop").offset().left+$("#autoTop").width()+10;
  $("#floatBar").css({"top":_Top.toString()+"px","left":_Left.toString()+"px"}).attr("TOP",_Top.toString());
}
//浮动bar
function setFloatBarFix(){
  var _this=$("#floatBar");
  var Top=$("#autoTop").offset().top+29;
  var Left=$("#autoTop").offset().left+$("#autoTop").width()+10;
  var scoll=$(window).scrollTop();
   if(scoll>0){
   
	       var _t=Top-scoll;
		   if(_t>0){
		   
             $("#floatBar").css({"top":_t.toString()+"px"});
			 
		   }
		   else{
		   
		    $("#floatBar").css({"top":"0px"});
		   }
	  
	
  }
 else{
    $("#floatBar").css({"top":_this.attr("TOP")+"px"});
 }
}
$(window).scroll(function(){
  if($(window).width()>1024){
    setFloatBarFix();
	setSaleTabsFix();
  }
  else{
    setTopFix();
	setSaleTabsFix();
  }
  
  
});
$(window).resize(function(){
   if($(window).width()>=1024){
    $("#TopHotBar").hide(10);
	$("#floatBar").show(10);
	setFloatBarFix_init();
  }
  else{
   
    $("#TopHotBar").show(10);
    $("#floatBar").hide(10);
	 setTopAttrTop();
	setFloatBarFix_init();
  }
 
});
}
b5m.brandDetail=function(){
  $(function(){
     //倒计时
  $("#time").countTime({
		  startTime:"2014-3-18 16:30:00",
		  endTime:"2014-3-25 16:30:00",
		  stampSet:
				{  model:"after",
					set:{"day":'<em>天</em>', 
					  "hour":'<em>时</em>', 
					  "min":'<em>分</em>', 
					  "sec":'<em>秒</em>'
					 }
				}
		});
	//
    $("[items]").mouseenter(function(){
	if($(this).hasClass("failure")==false){
	  $(this).removeClass("items").addClass("items-hover");
	  $(this).siblings().removeClass("items-hover").addClass("items");
	  }
	});
	//
	 $("[items]").mouseleave(function(){
	if($(this).hasClass("failure")==false){
	  $(this).removeClass("items-hover").addClass("items");
	  
	  }
	});
  });
}