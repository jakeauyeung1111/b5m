/**
 * User: dongzhuo@b5m.com
 * Date: 13-10-21
 * Time: 下午5:35
 */

(function(window,$) {

    $('.banner-index').mainBanner();

    $('.update-content h1 em,.update-content h2 em').attr('slide','on').click(function() {

        var content = $(this).parent().parent().next();

        if($(this).attr('slide')==='on') {
            content.slideUp();
            $(this).attr('slide','off');
        }else {
            content.slideDown();
            $(this).attr('slide','on');
        }

    });

})(window,jQuery);