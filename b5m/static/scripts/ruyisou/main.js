(function($,window) {


    //main banner
    $.fn.mainBanner = function() {

        var container = $(this),
            index= 0,
            elem = container.find('.main-banner'),
            len = elem.length,
            timer = null,
            role = container.find('.role');

        function mainbanner() {

            var times =5000;

            if(timer) {
                clearTimeout(timer);
                timer = null;
                times = 0;
            }

            timer =  setTimeout(function() {

                index = index == len-1 ? 0 : ++index;

                container.find('.curscroll').fadeOut(function() {
                    $(this).removeClass('curscroll');
                });

                elem.eq(index).fadeIn(function() {
                    $(this).addClass('curscroll');
                });

                role.find('.cur').removeClass('cur');
                role.find('li').eq(index).addClass('cur');

                clearTimeout(timer);
                timer = null;

                return mainbanner();

            },times);

        };

        role.find('li').each(function(i,n) {
            $(n).click(function() {
                index = i-1;
                return mainbanner();
            });
        });


        mainbanner();

    }





})(jQuery,window);