/**
 * User: dongzhuo@b5m.com
 * Date: 13-10-17
 * Time: 下午4:31
 */

(function(window,$) {


    var navIndex = $('.nav-index'),
        isie6 = typeof document.body.style.maxHeight ==='undefined';

    navIndex.data('top',navIndex.offset().top);

    navIndex.find('a').each(function(i,n) {

        $(n).click(function(){

            navIndex.find('.cur').removeClass('cur');
            $(n).addClass('cur');
            $('html,body').animate({scrollTop:floorArr[i]-(isie6 ? 0 : 200)});

        });

    });


    var floorArr = [],floorArrScroll=[];
    var floorIndex = $('.index-floor');
    floorIndex.each(function(i,n) {
        floorArr.push($(this).offset().top);
    });
    floorArrScroll = floorArr.slice();




    $(window).scroll(function() {


        var scroll = document.documentElement.scrollTop || document.body.scrollTop || 0;

     //   if(scroll>=navIndex.data('top')) {
         //   navIndex.addClass('fix');
     //   }else {
         //   navIndex.removeClass('fix');
     //   }

        $.each(floorArr,function(i,n) {
            if(floorArrScroll[i] && scroll >= floorArrScroll[i]-(isie6 ? 0 : 900)) {
                floorIndex.eq(i).addClass('animate');
                delete floorArrScroll[i];
            }
        });

    }).trigger('scroll');

})(window,jQuery);