(function($){
	function isEmail(str){
	    return /^\w+([\-+\.]\w+)*@\w+([\-+\.]\w+)*\.\w+([\-+\.]\w+)*$/.test(str);
	}
	function isPass(str){
		return /^[\x21-\x7e]{6,}$/.test(str);
	}
	function verifyCode(str){
		return /^[\w\d]{4}$/i.test(str);
	}
	function setFail (b, c, el){
		var $fail = el.parent().find(c);
		if (!b) {
			$fail.show();
		} else {
			$fail.hide();
		}
	}
	function getAgree(t, n) {
		n.prop("disable", !0).addClass("btn-grey"), t.change(function() {
			var t = $(this).prop("checked");
			t ? n.attr("disable", !1).removeClass("btn-grey").addClass('btn-primary') : n.attr("disable", !0).removeClass('btn-primary').addClass("btn-grey");
		});
	}

	var $chk = $("#agree").prop('checked'),
		obj = {
			'email': '^\\w+([\\-+\\.]\\w+)*@\\w+([\\-+\\.]\\w+)*\\.\\w+([\\-+\.]\\w+)*$',
			'psw1' : '^[\\x21-\\x7e]{6,}$',
			'psw2' : function(){
				var n = $.trim($('#go-psw1').val()),
					r = $.trim($('#go-psw2').val());
				return (r !='') && (n === r);
			},
			'code' : '^[\\w\\d]{4}$'
		};
	$.each(obj, function(n, t){
		$('#go-' + n).on('blur', function(){
			var v = $.trim($(this).val()),
				b = !1;
			if (typeof t == 'function') {
				b = t();
			} else if (typeof t == 'string') {
				var reg = new RegExp(t);
				b = reg.test(v);
			}
				
			setFail(b, '.fail', $(this));
		});
	});

	getAgree($("#agree"), $('.frm-btn .btn'));
})(jQuery);