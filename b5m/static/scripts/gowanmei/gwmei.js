(function(window,$) {
    $('.banner').addClass('animate');
    var t1 = 0,
        t2 = $('.fun').offset().top,
        t3 = $('.search').offset().top,
        up1 = $('.up1'),
        up2 = $('.up2'),
        download = $('.download').find('a');

    backTop(up1, t1);
    backTop(up2, t2);

    download.hover(function(){
        $(this).parents('dl').animate({
            top: -5
        }, 300);
    }, function(){
        $(this).parents('dl').animate({
            top: 0
        }, 300);
    });
    /**
     *返回指定位置
     *@param el点击元素
     *@param t位置高度
     */
    function backTop(el, t){
        el.click(function(e){
            e.preventDefault();
            $('html, body').animate({
                scrollTop: t
            }, 200);
        });
    }


    /*var pagePos = [0,t2,t3],
        curScroll = 0,
        curPage = 0,
        scrollOn = true;*/
    var  win_h = $(window).height();

    $(window).scroll(function() {
        var _scroll = $(this).scrollTop();

        if ((_scroll + win_h >= t2) && (_scroll + win_h < t3)) {
            $('.fun').addClass('animate');
        } else if (_scroll + win_h >= t3) {
            $('.search').addClass('animate');
        }
        /*if(!scrollOn) return false;

        var _scroll = $(this).scrollTop();

        if(_scroll > t3) {
            _scroll = t3;
            return false;
        }

        scrollOn = false;
        if(_scroll > curScroll) {
            curPage++;
        }else if(_scroll < curScroll) {
            curPage--;
        }

        $('html,body').animate({scrollTop:pagePos[curPage]},500,function() {
            setTimeout(function() {
                scrollOn = true;
                curScroll = $(this).scrollTop();
                if(curPage == 1) {
                    $('.fun').addClass('animate');
                } else if (curPage == 2) {
                     $('.search').addClass('animate');
                }
            },1);
        });*/

    });
})(window,jQuery);