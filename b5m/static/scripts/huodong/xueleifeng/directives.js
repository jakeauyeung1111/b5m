/**
 * 抢红包
 */
packetApp.directive('getPacket', ['packetSer','ngDialog',function (packetSer,ngDialog) {
	return {
		restrict: 'A',
		link: function (scope, iElement, iAttrs) {
			iElement.bind('click', function(event){
				var type = iAttrs.packettype;

				//若返回false,则退出
				if(!type){
					return false;
				}

				//请求抢购状态码
				// A 不在10-11点、15-16点时间段内——抢购尚未开始   ===》 400
				// B时间段内抢到红包——抢购成功   ===》 200
				// C时间段内红包抢完——抢完了   ===》 300
				// D时间段内重复抢购——只能抢一次   ===》 406
				// E时间段内没有帮豆——帮豆不足   ===》 303
				// F未登录——提醒登录   ===》 301
				// G登录帐号未绑定邮箱——提醒绑定   ===》 302

				packetSer.getStatus(type).success(function(data){
					var popupType = data.code,
						popTpl = '';

					switch(popupType){
						case 400:
							popTpl = '<div class="hd_lf_tanchuceng"><div class="tanchuc_wenzi"> <span class="tanchuc_pic1"></span> <span class="tanchuc_zitir">对不起，抢购尚未开始<br/>活动时间：每天11点，15点</span> </div> <a class="tanchu_bottun" href="http://www.b5m.com/jijiehao.php">去别的地方逛逛</a> </div>';
							break;
						case 200:
							popTpl = '<div class="hd_lf_tanchuceng"><div class="tanchuc_wenzi"> <span class="tanchuc_pic2"></span> <span class="tanchuc_zitir">恭喜您抢到红包,<br/>我们的工作人员会在24小时内<br/>联系您安排发放红包</span> </div> <p class="tanchu_tixing">提醒：请完善您的用户手机，邮箱资料，方便我们联系您哦！<a href="http://ucenter.b5m.com/forward.htm?method=/user/account/info/index&mps=0.0.0.0.0">去完善</a></p> </div>';
							break;
						case 300:
							popTpl = '<div class="hd_lf_tanchuceng"><div class="tanchuc_wenzi"> <span class="tanchuc_pic3"></span> <span class="tanchuc_zitir">哎呀，晚了一步，红包被抢光啦！<br/>下个时间段再来哦</span> </div> <a class="tanchu_bottun" href="http://www.b5m.com/jijiehao.php">去别的地方逛逛</a> </div>';
							break;
						case 406:
							popTpl = '<div class="hd_lf_tanchuceng"><div class="tanchuc_wenzi"> <span class="tanchuc_pic4"></span> <span class="tanchuc_zitir">每个帐号一天只能抢一次哦！</span> </div> <a class="tanchu_bottun tanchu_bott" href="http://ucenter.b5m.com/forward.htm?method=/user/account/invitation/index&mps=0.0.0.0.0">去邀请</a> </div>';
							break;
						case 303:
							popTpl = '<div class="hd_lf_tanchuceng"><div class="tanchuc_wenzi"> <span class="tanchuc_pic5"></span> <span class="tanchuc_zitir tanchu_ziti">您的帮豆数不足，赚足了帮豆再来吧！</span> </div> <a class="tanchu_bottun tanchu_bott" href="http://ucenter.b5m.com/forward.htm?method=/user/trade/common/record/index&mps=0.0.0.0.0">去赚帮豆</a> </div>';
							break;
						case 301:
							popTpl = '<div class="hd_lf_tanchuceng"><div class="tanchuc_wenzi"> <span class="tanchuc_pic6"></span> <span class="tanchuc_zitir tanchu_ziti">登录后才能抢购哦！</span> </div> <a class="tanchu_bottun tanchu_log" href="" go-login>登录</a> <a class="tanchu_bottun tanchu_regis" href="" go-regist>注册</a> </div>';
							break;
						case 302:
							popTpl = '<div class="hd_lf_tanchuceng"><div class="tanchuc_wenzi"> <span class="tanchuc_pic6"></span> <span class="tanchuc_zitir tanchu_ziti">绑定了邮箱才能参与哦！</span> </div> <a class="tanchu_bottun tanchu_bott" href="http://ucenter.b5m.com/forward.htm?method=/user/account/info/index&mps=0.0.0.0.0">去绑定</a> </div>';
							break;
						default:
							popTpl = 'default';
							break;
					}

					ngDialog.open({
					    template:popTpl,
					    plain:true
					})
				})
				event.preventDefault();
			});
		}
	}
}]);
/**
 * 注册
 */
packetApp.directive('goRegist', ['$location',function ($location) {
	return {
		restrict: 'A',
		link: function (scope, iElement, iAttrs) {
			iElement.bind('click',function(event){
				var baseUrl = 'http://ucenter.b5m.com/forward.htm?method=/user/info/register&userType=8&url=';
				window.location.href = baseUrl + encodeURIComponent($location.absUrl());

				event.preventDefault();
			})
		}
	}
}]);
/**
 * 登录
 */
packetApp.directive('goLogin', ['$location',function ($location) {
	return {
		restrict: 'A',
		link: function (scope, iElement, iAttrs) {
			iElement.bind('click',function(event){
				var baseUrl = 'http://ucenter.b5m.com?loginReferer=';
				window.location.href = baseUrl + encodeURIComponent($location.absUrl());
				
				event.preventDefault();
			})
		}
	}
}]);
/**
 * 邀请
 */
packetApp.directive('goInvite', [function () {
	return {
		restrict: 'A',
		link: function (scope, iElement, iAttrs) {
			iElement.on('click',function(event){
				var baseUrl = 'http://ucenter.b5m.com/forward.htm?method=/user/account/invitation/index&mps=0.0.0.0.0';
				window.location.href = baseUrl;
				
				event.preventDefault();
			})
		}
	}
}]);