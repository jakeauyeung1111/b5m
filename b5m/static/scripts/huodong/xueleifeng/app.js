var packetApp = angular.module('packetApp',['ngDialog']).config(function($sceProvider) {
  // Completely disable SCE to support IE7.
  $sceProvider.enabled(false);
});