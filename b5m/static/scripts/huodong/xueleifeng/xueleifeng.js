var packetApp = angular.module('packetApp',['ngDialog']).config(function($sceProvider) {
  // Completely disable SCE to support IE7.
  $sceProvider.enabled(false);
});
;packetApp.controller('prizeCtrl', ['$scope','prizeSer',function ($scope,prizeSer) {
		prizeSer.getPize().success(function(data){
			if(!data){
				return false;
			}

			$scope.prizes = data;
		})
}]);

function getById(elem){
    return elem ? document.getElementById(elem):'';
}

var speed = 40,
    origin = getById('J_container_origin'),
    copy = getById('J_container_copy'),
    container = getById('J_container');

copy.innerHTML = origin.innerHTML + origin.innerHTML;

function Marquee(){
    if(copy.offsetLeft - container.scrollLeft <= 0){
        container.scrollLeft -= origin.offsetWidth;
    }else{
        container.scrollLeft++;
    }
}

var sliderFlag = setInterval(Marquee,speed);

container.onmouseover = function(){
    clearInterval(sliderFlag);
}

container.onmouseout = function(){
    sliderFlag = setInterval(Marquee,speed);
}
    
;/**
 * 奖品信息
 * @return jsonp对象
 */
packetApp.provider('packetSer', [function () {
	this.$get = ['$http',function($http) {
		return {
			// baseUrl:'http://172.16.2.96/qianghongbao.htm?type=',
			baseUrl:'http://www.b5m.com/qianghongbao.htm?type=',
			jsonpCallback:'&jsonpCallback=JSON_CALLBACK',
			getStatus:function(type){
				if(!type){
					return false;
				}

				var url = this.baseUrl + type + this.jsonpCallback;

				return $http.jsonp(url);
			}
		}
	}];
}]);
/**
 * 获得中奖名单
 * @return jsonp 对象
 */
packetApp.provider('prizeSer', [function () {
	this.$get = ['$http',function($http) {
		return {
			baseUrl:'http://www.b5m.com/qianghongbao.htm?info=info',
			jsonpCallback:'&jsonpCallback=JSON_CALLBACK',
			getPize:function(){
				var url = this.baseUrl + this.jsonpCallback;	
				return $http.jsonp(url);
			}
		};
	}];
}]);
