(function(window, $) {


    /* cookie */
    window.Cookies = {
        set:function(name,value) {
            var argv = arguments;
            var argc = arguments.length;
            var expires = (argc > 2) ? argv[2] : null;
            var path = (argc > 3) ? argv[3] : '/';
            var domain = (argc > 4) ? argv[4] : null;
            var secure = (argc > 5) ? argv[5] : false;
            document.cookie = name + "=" + escape(value) + ((expires == null) ? "" : ("; expires=" + expires.toGMTString())) + ((path == null) ? "" : ("; path=" + path)) + ((domain == null) ? "" : ("; domain=" + domain)) + ((secure == true) ? "; secure" : "");
        },
        get:function(name) {
            var arg = name + "=";
            var alen = arg.length;
            var clen = document.cookie.length;
            var i = 0;
            var j = 0;
            while (i < clen) {
                j = i + alen;
                if (document.cookie.substring(i, j) == arg)
                    return Cookies.getCookieVal(j);
                i = document.cookie.indexOf(" ", i) + 1;
                if (i == 0)
                    break;
            }
            return null;
        },
        clear:function(name) {
            if (Cookies.get(name)) {
                var expdate = new Date();
                expdate.setTime(expdate.getTime() - (86400 * 1000 * 1));
                Cookies.set(name, "", expdate);
            }
        },
        getCookieVal:function(offset) {
            var endstr = document.cookie.indexOf(";", offset);
            if (endstr == -1) {
                endstr = document.cookie.length;
            }
            return unescape(document.cookie.substring(offset, endstr));
        }
    };

    //全网寻宝
    var activeFed = {
        /*全网寻宝*/
        quanwangXb:function(){
            var _content = this.content();
            var xbHtml = '<div id="huodong-quanwangXb" style="display:none;"><div class="huodong-quanwangXb-bg"></div><a href="http://www.b5m.com/huodong.php?mod=zajindan" target="_blank"></a><s></s></div>';
            $(xbHtml).appendTo($("body")).css("top",_content.offset().top+ _content.height() + parseInt(_content.css('padding-top')) + parseInt(_content.css('padding-bottom')) ).show().animate({height:300},320);

            var closeBtn = $("s","#huodong-quanwangXb").click(function(){
                $("#huodong-quanwangXb").slideUp();
                var val = window.hddatacount == 30 ? 'full' : window.hddatacount;
                Cookies.set('hddatacount',val,new Date(parseInt(window.hddatatime*1000)),'','.b5m.com');
                return false;
            });

            $('#huodong-quanwangXb').find('a').click(function() {
                closeBtn.click();
            });
        },
        loadFun:function(){

            var _count = Cookies.get('hddatacount');

            if(_count == 'full' && window.hddatacount==30) {
                return;
            }

            if(_count && _count == window.hddatacount) {
                return;
            }

            if($('body.lotterypage').length) {
                return;
            }

            var _this = this;
            setTimeout(function(){
                $(document).trigger("click");
                _this.quanwangXb();
            },2000);

        },
        content:function() {
            //判断头部dom

            //韩国馆,淘特价 等
            var head1= $('.header-content .header-search');
            if(head1.length) {
                return head1;
            }

            //www,zdm 等
            var head2 = $('.header .search-bar');
            if(head2.length) {
                return head2;
            }

            //帮5淘
            var head3 = $('.header-content .search-bar');
            if(head3.length) {
                return head3;
            }

            //you
            return $('.header .nav');

        }
    };



    //ajax request
    window.hddata = 0;
    var _url = encodeURIComponent(location.href);
    $.ajax({
        url:'http://www.b5m.com/hd-ajax.php',
        data: {mod:'index',url:_url},
        dataType: "jsonp",
        jsonp: 'jsonpCallback',
        success: function(data) {
            window.hddata = data.code;
            window.hddatacount = data.data;
            window.hddatatime = data['time_end'];
            if(window.hddata==='1') {
                activeFed.loadFun();
            }
        }
    });
    

})(window, window.jQuery);

