var questionApp = angular.module('questionApp', ['ngRoute', 'questionServices', 'resultServices']);

questionApp.config(['$routeProvider',
    function($routeProvider) {

        $routeProvider
        .when('/question', {
            templateUrl: '/huodong/zuiqiangdanao/question.html',
            controller: 'questionController'
        }).
      /*  when('/question/:questionId', {
            templateUrl: '/huodong/zuiqiangdanao/question.html',
            controller: 'questionController'
        }).*/
        when('/result/:question_id/:option_id', {
            templateUrl: '/huodong/zuiqiangdanao/result.html',
            controller: 'resultController'
        });

    }
]);

/*
angular.module('ie7support', []).config(function($sceProvider) {
// Completely disable SCE to support IE7.
    $sceProvider.enabled(false);
});
*/

/*
$(function() {
    var isLogin = Cookies.get('login') === 'true' && Cookies.get('token');
    if(!isLogin) {

    }
});*/
