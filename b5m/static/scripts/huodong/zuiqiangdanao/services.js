//注册做题services
var questionServices = angular.module('questionServices', ['ngResource']);
questionServices.factory('Question',['$resource', function($resource) {
    //static/html/huodong/zuiqiangdanao/datas/question.php
    return $resource('/zuiqiangdanao.htm', {
        operation:'getQuestionInfoById'
       // questionId: '@questionId'
    }, {
        get: {
            method: 'GET',
            isArray: false
        }
    });
}]);

//注册结果services
var resultServices = angular.module('resultServices', ['ngResource']);
questionServices.factory('Result',['$resource', function($resource) {
    
    return $resource('/zuiqiangdanao.htm', {
        operation:'answer',
        question_id: '@question_id',
        option_id: '@option_id'
    }, {
        get: {
            method: 'GET',
            isArray: false
        }
    });

}]);