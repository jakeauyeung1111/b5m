// var lottery_type = location.href.split('=')[1];
var lottery_type = location.href.split('=')[1].substring(0,location.href.split('=')[1].indexOf('&'))?location.href.split('=')[1].substring(0,location.href.split('=')[1].indexOf('&')):location.href.split('=')[1];
$(function(){

	/**
	 * 抽奖入口
	 */
	var flag = true;
	var lotteryFunc = function(){
		//取掉点击旋转事件
		// $('#lotteryBtn').off('click',lotteryFunc);
		if(!flag) return false;
		flag = false;
		$.ajax({
			url: '/lottery.php?t='+Math.random(10000, 9999),
			type: 'POST',
			async:false,
			dataType: 'json',
			data: {'ajax_mod':'award','lottery_type':lottery_type},
	        success:function(data){
	        	if(data.number ==='login_false'){
	        		noChance(1);
	        		return false;
	        	}
	        	if(data.number ==='bean_false'){
	        		noChance(2,data.bean_num);
	        		return false;
	        	}
	        	if(data.number ==='notTime'){
	        		noChance(3);
	        		return false;
	        	}
	        	switch(data.number){
					case 1:
						rotateFunc(1,data.name,data.type,data.beanNum,data.name,data.id);
						break;
					case 2:
						rotateFunc(2,data.name,data.type,data.beanNum,data.name,data.id);
						break;
					case 3:
						rotateFunc(3,data.name,data.type,data.beanNum,data.name,data.id);
						break;
					case 4:
						rotateFunc(4,data.name,data.type,data.beanNum,data.name,data.id);
						break;
					case 5:
						rotateFunc(5,data.name,data.type,data.beanNum,data.name,data.id);
						break;
					case 6:
						rotateFunc(6,data.name,data.type,data.beanNum,data.name,data.id);
						break;
					case 7:
						rotateFunc(7,data.name,data.type,data.beanNum,data.name,data.id);
						break;
					case 8:
						rotateFunc(8,data.name,data.type,data.beanNum,data.name,data.id);
						break;
				}
			}
		})
		
	}
	/**
	 * awards:奖项编号
	 */
	var rotateFunc = function(awards,txt,type,beanNum,name,lottery_id){
		var angle = (awards-1)*45 + 22;
		$('#lotteryBtn').addClass('active');
		$('#lotteryBtn').stopRotate();
		$("#lotteryBtn").rotate({
			angle:angle, 
			duration: 5000, 
			animateTo: angle+1440, //angle是图片上各奖项对应的角度，1440是我要让指针旋转4圈。所以最后的结束的角度就是这样子^^
			callback:function(){
				// 获得奖品显示
				lotteryTips(awards,txt,type,beanNum,name,lottery_id);
				// $('#lotteryBtn').on('click',lotteryFunc);
				flag = true;
				// if(type!='noprize') showLotteryUser();
			}
		}); 
	};



	/**
     * 用户中奖刷新列表
	 */
	 /*
	var showLotteryUser = function(){

	 	$.ajax({
			url: '/lottery.php?t='+Math.random(10000, 9999),
			type: 'POST',
			dataType: 'json',
			data:{'ajax_data_mod':'showLottery','lottery_type':lottery_type},
	        success:function(response){

	        	var awardData = '';
				awardData += '<li>';
				awardData += '<h2 class="title">红包转盘中奖名单</h2>';
				awardData += '</li>';
				for(var v in response){

					awardData += '<li>';
					awardData += '<div class="img-con">';
					awardData += '<img style="width:40px;height:100%" src="'+response[v].avatar+'" alt="">';
					awardData += '</div>';
					awardData += '<div class="rt">';
					awardData += '<div class="tp">'+response[v].showname+'</div>';
					awardData += '<div class="bt">';
					awardData += '抽中 ';
					awardData += '<span class="orange">'+response[v].prize_name+'</span>';
					awardData += '</div>';
				    awardData += '</div>';
					awardData += '</li>';

		        }

		       	$('.lt-list').html(awardData);
		       	$('.lt-list').find('li:not(:first)').hide().fadeIn(2000);
		       }


		});
		
	}
	*/

	/**
	 * 绑定旋转事件
	 */
	$("#lotteryBtn").rotate({ 
	   bind: 
		{ 
			click: lotteryFunc
		} 
	});

	/**
	 * 插入弹出框
	 */
	var Popup = {
		$popup:null,
		create:function(){
			$popup = $('<div class="popup" id="J_popup"><div class="popup-content"></div><span class="close vh">关闭</span></div>').appendTo($('body'));
			$popup.find('.close').on('click',function(){
				$popup.hide();
			})
		},
		show:function(){
			$popup.show();
		},
		close:function(){
			$popup.hide();
		}
	}
	/**
	 * 检查弹出框是否存在
	 */
	function checkPopup(){
		if(!$('#J_popup').length){
			Popup.create();
		}else{
			Popup.show();
		}
	}
	/**
	 * 奖品判断
	 */
	function lotteryTips(awards,txt,type,beanNum,name,lottery_id){
	 	var _i = awards,
	 		_txt = txt;
	 		_type = type;
		checkPopup();
		var $content = $('#J_popup').find('.popup-content');

		if(_i == 5){
			tryAgain($content);
		}else{
			getAward($content,_txt,_type,beanNum,name,lottery_id);
		}
	 }

	/**
	 * 不能抽奖
	 * 1.未登录
	 * 2.帮豆不足
	 */
	function noChance(type,bean_num){
		checkPopup();
		var $content = $('#J_popup').find('.popup-content'),
			_type = type;
		switch(_type){
			case 1:
				$content.html('<p class="title">不好意思，您当前尚未登录!</p><div class="btn-box mt50"><a href="http://ucenter.b5m.com/" class="btn btn-2">请点击登录/注册</a></div>');
				break;
			case 2:
				$content.html('<p class="title">您的帮豆剩余数不足，请赚足'+bean_num+'个帮豆后再来哦！</p> <div class="btn-box mt15"> <a href="http://ucenter.b5m.com/forward.htm?method=/user/trade/common/record/index" class="btn btn-2">返回并赚取帮豆</a> </div> <p class="warning-tips mt10">赚取帮豆方法：发帖、回帖都可以获得相应帮豆数，多劳多得哦！</p>');
				break;		
			case 3:
                $content.html('<p class="title">不要心急哦，活动还没开始呐！</p> <div class="btn-box mt15"> <a href="http://ucenter.b5m.com/forward.htm?method=/user/trade/common/record/index" class="btn btn-2">返回并赚取帮豆</a> </div> <p class="warning-tips mt10">赚取帮豆方法：发帖、回帖都可以获得相应帮豆数，多劳多得哦！</p>');
            break;
		}
        $content.next().off('click.update',updatePage);
	}


	if (Cookies.get('login') !== 'true') {
            noChance(1);
            return;
    }

	/**
	 * 再来一次
	 */
	function tryAgain($content,txt){
		$content.html('<p class="title">恭喜您！获得了再来一次的机会。</p><div class="btn-box mt50"><a href="javascript:void(0);" class="btn btn-1">点击再次抽奖</a></div>');
		// 关闭弹窗
		$('#J_popup').find('.btn').on('click',function(e){
			e.preventDefault();
			Popup.close();
		});
        $content.next().off('click.update',updatePage);
	}
	/**
	 * 手机、QQ币、帮豆礼品等
	 */
	function getAward($content,txt,type,beanNum,name,lottery_id){
		$content.html('<p class="title">恭喜您！获得了'+ txt +'</p><div class="btn-box mt50"><span class="click-here">请点击</span><a href="javascript:void(0);" class="btn btn-1">领奖</a> </div>');
		$('#J_popup').find('.btn').on('click',function(e){
			e.preventDefault();
			getUseInfo($content,type,beanNum,name,lottery_id)
		});
        $content.next().on('click.update',updatePage);
	}
	/**
	 *填写表单
	 */
	function getUseInfo($content,type,beanNum,name,lottery_id){
		var _type = type;
		switch(_type){
			case 'dou':
				$content.html('<div class="popup-in mt40">帮豆已经充入您的账户，目前您的帮豆总数为：<span class="cl-ffd">'+beanNum+'</span>个帮豆。</div><div class="btn-box mt15"><a href="javascript:void(0);" class="btn btn-2">确定</a></div>');
					// 关闭弹窗
					$('#J_popup').find('.btn').on('click',function(){
						Popup.close();
                        updatePage();
					});	
				break;
			case 'qq':
				$content.html('<h2 class="form-title">请填写您详细的联系方式</h2> <div class="popup-in"> <form action="" class="form"> <div class="item"><label for="" class="label">姓名：</label><input class="user-name input-txt" type="text"></div> <div class="item"><label for="" class="label">QQ：</label><input class="user-qq input-txt" type="text"></div> <div class="item"><label for="" class="label">联系电话：</label><input class="user-mobile input-txt" type="text"></div> </form> </div> <div class="btn-box mt15"> <a href="javascript:void(0);" class="btn btn-2">提交</a> </div>');
				break;
			case 'huafei':
				$content.html('<h2 class="form-title">请填写您详细的联系方式</h2><div class="popup-in"><form action="" class="form"><div class="item"><label for="" class="label">姓名：</label><input class="user-name input-txt" type="text"></div><div class="item"><label for="" class="label">联系电话：</label><input class="user-mobile input-txt" type="text"></div></form> </div><div class="btn-box mt15"><a href="javascript:void(0);" class="btn btn-2">提交</a> </div>');
				break;
			case 'phone':
				$content.html('<h2 class="form-title">请填写您详细的联系方式</h2> <div class="popup-in"> <form action="" class="form"> <div class="item"><label for="" class="label">姓名：</label><input class="user-name input-txt" type="text"></div> <div class="item"><label for="" class="label">联系电话：</label><input class="user-mobile input-txt" type="text"></div> <div class="item"><label for="" class="label">收件地址：</label><input class="user-addr input-txt" type="text"></div> </form> </div> <div class="btn-box mt15"> <a href="javascript:void(0);" class="btn btn-2">提交</a> </div>');
				break;
		}

		$('#J_popup').find('.btn').on('click',function(){

			if(type == 'qq' || type == 'huafei' || type == 'phone'){
				formLottery(lottery_id);
			}

			awardsTips($content,_type,beanNum,name);
		});
	}


	var formLottery=function(lottery_id){

		var user_name = $('.user-name').val();
		var user_mobile = $('.user-mobile').val();
		var user_qq = $('.user-qq').val();
		var user_addr = $('.user-addr').val();

		$.ajax({
			url: '/lottery.php?t='+Math.random(10000, 9999),
			type: 'GET',
			dataType: 'json',
			data: {'ajax_data_mod':'formLottery','lottery_id':lottery_id,'lottery_type':lottery_type,'user_name':user_name,'user_mobile':user_mobile,'user_addr':user_addr,'user_qq':user_qq},
			success:function(data){

			}

		})

	}
	/**
	 * 友情提示
	 */
	function awardsTips($content,type,beanNum,name){
		var _type = type;
		switch(_type){
			case 'phone':
				$content.html('<p class="title">感谢您的配合，我们会在7个工作日内寄出奖品，请您注意查收。</p> <div class="btn-box mt50"> <a href="javascript:void(0);" class="btn btn-1">点击再次抽奖</a> <a href="http://ucenter.b5m.com/forward.htm?method=/user/trade/common/record/index" class="btn btn-2">进入个人中心查看记录</a> </div>');
				break;
			case 'qq':
				$content.html('<div class="popup-in mt40">您已经抽到'+name+'个，我们会在一个工作日内把奖品打入到您的账户。</div> <div class="btn-box mt15"> <a href="javascript:void(0);" class="btn btn-2">确定</a> </div>');
				break;
			case 'huafei':
				$content.html('<div class="popup-in mt40">您已经抽到'+name+'，我们会在一个工作日内把奖品打入到您的账户。</div> <div class="btn-box mt15"> <a href="javascript:void(0);" class="btn btn-2">确定</a> </div>');
				break;
		}
		// 关闭弹窗
		$('#J_popup').find('.btn').on('click',function(){
			Popup.close();
            updatePage();
		});			
	}


    function updatePage() {
        flag = false;
        setTimeout(function() {
            location.href = location.href;
        },1000);
    }

});
