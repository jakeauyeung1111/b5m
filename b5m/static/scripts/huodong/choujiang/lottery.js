$(function(){
	/**
	 * 抽奖入口
	 */
	var flag = true;
	var lotteryFunc = function(){
		//取掉点击旋转事件
		// $('#lotteryBtn').off('click',lotteryFunc);
		if(!flag) return false;
		flag = false;

		var data = [1,2,3,4,5,6,7,8],
			data = data[Math.floor(Math.random()*data.length)],
			data = 3,
			txt = '100帮豆',
			type = 'dou';



		$.ajax({
			url: 'www.baidu.com',
			type: 'post',
			dataType: 'json',
			data: {param1: 'value1'},
		})
		.fail(function() {

			/**
			 * data:奖品编号
			 * txt:为奖品名称
			 * type:中奖种类（phone为手机，qq为QQ币，dou为帮豆）
			 */
			switch(data){
				case 1:
					rotateFunc(1,txt,type);
					break;
				case 2:
					rotateFunc(2,txt,type);
					break;
				case 3:
					rotateFunc(3,txt,type);
					break;
				case 4:
					rotateFunc(4,txt,type);
					break;
				case 5:
					rotateFunc(5,txt,type);
					break;
				case 6:
					rotateFunc(6,txt,type);
					break;
				case 7:
					rotateFunc(7,txt,type);
					break;
				case 8:
					rotateFunc(8,txt,type);
					break;
			}
		});

	}
	/**
	 * awards:奖项编号
	 */
	var rotateFunc = function(awards,txt,type){
		var angle = (awards-1)*45 + 22;
		$('#lotteryBtn').addClass('active');
		$('#lotteryBtn').stopRotate();
		//转
		$("#lotteryBtn").rotate({
			angle:angle,
			duration: 5000, 
			animateTo: angle+1440, //angle是图片上各奖项对应的角度，1440是我要让指针旋转4圈。所以最后的结束的角度就是这样子^^
			callback:function(){
				// 获得奖品显示
				lotteryTips(awards,txt,type);
				flag = true;
				// $('#lotteryBtn').on('click',lotteryFunc);
			}
		});
	};
	/**
	 * 绑定旋转事件
	 */
	$("#lotteryBtn").rotate({ 
	   bind: 
		{ 
			click: lotteryFunc
		} 
	});

	/**
	 * 插入弹出框
	 */
	var Popup = {
		$popup:null,
		create:function(){
			$popup = $('<div class="popup" id="J_popup"><div class="popup-content"></div><span class="close vh">关闭</span></div>').appendTo($('body'));
			$popup.find('.close').on('click',function(){
				$popup.hide();
			})
		},
		show:function(){
			$popup.show();
		},
		close:function(){
			$popup.hide();
		}
	}
	/**
	 * 检查弹出框是否存在
	 */
	function checkPopup(){
		if(!$('#J_popup').length){
			Popup.create();
		}else{
			Popup.show();
		}
	}
	/**
	 * 奖品判断
	 */
	 function lotteryTips(awards,txt,type){
	 	var _i = awards,
	 		_txt = txt;
	 		_type = type;
		checkPopup();
		var $content = $('#J_popup').find('.popup-content');
		if(_i == 5){
			tryAgain($content);
		}else{
			getAward($content,_txt,_type);
		}
	 }

	/**
	 * 不能抽奖
	 * 1.未登录
	 * 2.帮豆不足
	 */
	function noChance(type){
		checkPopup();
		var $content = $('#J_popup').find('.popup-content'),
			_type = type;
		switch(_type){
			case 1:
				$content.html('<p class="title">不好意思，您当前尚未登录!</p><div class="btn-box mt50"><a href="#" class="btn btn-2">请点击登录/注册</a></div>');
				break;
			case 2:
				$content.html('<p class="title">您的帮豆剩余数不足，请赚足68个帮豆后再来哦！</p> <div class="btn-box mt15"> <a href="#" class="btn btn-2">返回社区赚取帮豆</a> </div> <p class="warning-tips mt10">赚取帮豆方法：发帖、回帖都可以获得相应帮豆数，多劳多得哦！</p>');
				break;
            case 3:
                $content.html('<p class="title">不要心急哦，活动还没开始呐！</p> <div class="btn-box mt15"> <a href="#" class="btn btn-2">返回社区赚取帮豆</a> </div> <p class="warning-tips mt10">赚取帮豆方法：发帖、回帖都可以获得相应帮豆数，多劳多得哦！</p>');
                break;
		}
	}

	/**
	 * 再来一次
	 */
	function tryAgain($content,txt){
		$content.html('<p class="title">恭喜您！获得了再来一次的机会。</p><div class="btn-box mt50"><a href="#" class="btn btn-1">点击再次抽奖</a></div>');
		// 关闭弹窗
		$('#J_popup').find('.btn').on('click',function(e){
			e.preventDefault();
			Popup.close();
		});	
	}
	/**
	 * 手机、QQ币、帮豆礼品等
	 */
	function getAward($content,txt,type){
		$content.html('<p class="title">恭喜您！获得了'+ txt +'</p><div class="btn-box mt50"><span class="click-here">请点击</span><a href="#" class="btn btn-1">领奖</a> </div>');
		$('#J_popup').find('.btn').on('click',function(e){
			e.preventDefault();
			getUseInfo($content,type)
		});
	}
	/**
	 *填写表单
	 */
	function getUseInfo($content,type){
		var _type = type;
		switch(_type){
			case 'dou':
				$content.html('<div class="popup-in mt40">帮豆已经充入您的账户，目前您的帮豆总数为：<span class="cl-ffd">1060</span>个帮豆。</div><div class="btn-box mt15"><a href="#" class="btn btn-2">确定</a></div>');
					// 关闭弹窗
					$('#J_popup').find('.btn').on('click',function(e){
						e.preventDefault();
						Popup.close();
					});	
				break;
			case 'qq':
				$content.html('<h2 class="form-title">请填写您详细的联系方式</h2> <div class="popup-in"> <form action="" class="form"> <div class="item"><label for="" class="label">姓名：</label><input class="input-txt" type="text"></div> <div class="item"><label for="" class="label">QQ：</label><input class="input-txt" type="text"></div> <div class="item"><label for="" class="label">联系电话：</label><input class="input-txt" type="text"></div> </form> </div> <div class="btn-box mt15"> <a href="#" class="btn btn-2">提交</a> </div>');
				break;
			case 'huafei':
				$content.html('	<h2 class="form-title">请填写您详细的联系方式</h2><div class="popup-in"><form action="" class="form"><div class="item"><label for="" class="label">姓名：</label><input class="input-txt" type="text"></div><div class="item"><label for="" class="label">联系电话：</label><input class="input-txt" type="text"></div></form> </div><div class="btn-box mt15"><a href="#" class="btn btn-2">提交</a> </div>');
				break;
			case 'phone':
				$content.html('<h2 class="form-title">请填写您详细的联系方式</h2> <div class="popup-in"> <form action="" class="form"> <div class="item"><label for="" class="label">姓名：</label><input class="input-txt" type="text"></div> <div class="item"><label for="" class="label">联系电话：</label><input class="input-txt" type="text"></div> <div class="item"><label for="" class="label">收件地址：</label><input class="input-txt" type="text"></div> </form> </div> <div class="btn-box mt15"> <a href="#" class="btn btn-2">提交</a> </div>');
				break;
		}

		$('#J_popup').find('.btn').on('click',function(){
			awardsTips($content,_type);
		});
	}
	/**
	 * 友情提示
	 */
	function awardsTips($content,type){
		var _type = type;
		switch(_type){
			case 'phone':
				$content.html('<p class="title">感谢您的配合，我们会在x个工作日内寄出奖品，请您注意查收。</p> <div class="btn-box mt50"> <a href="#" class="btn btn-1">点击再次抽奖</a> <a href="#" class="btn btn-2">进入个人中心查看记录</a> </div>');
				break;
			case 'qq':
				$content.html('<div class="popup-in mt40">QQ币已经充入您的账户，目前您的帮豆总数为：<span class="cl-ffd">1060</span>个帮豆。</div> <div class="btn-box mt15"> <a href="#" class="btn btn-2">确定</a> </div>');
				break;
			case 'huafei':
				$content.html('<div class="popup-in mt40">话费已经充入您的账户，目前您的帮豆总数为：<span class="cl-ffd">1060</span>个帮豆。</div> <div class="btn-box mt15"> <a href="#" class="btn btn-2">确定</a> </div>');
				break;
		}
		// 关闭弹窗
		$('#J_popup').find('.btn').on('click',function(){
			Popup.close();
		});			
	}

    function fillZero(num, digit)
    {
        var str = '' + num;

        while (str.length < digit)
        {
            str = '0' + str;
        }

        return parseInt(str, 10);
    }

    function countDown()
    {
        var oNowTime = new Date().getTime();
        var oStartTime = new Date().setHours(9, 59, 59);
        var oEndTime = new Date().setHours(23, 59, 59);
        var iRemain = 0;
        var oStopTime;

        iRemain = (oNowTime - oStartTime) / 1000;

        if (iRemain > 0)
        {
            $('.down-now').addClass('down-end');
            oStopTime = (oEndTime - oNowTime) / 1000;
            formatTime(oStopTime);
        }
        else
        {
            $('.down-now').removeClass('down-end');
            formatTime(-iRemain);
        }
    }

    function formatTime(iRemain)
    {
        var iHour = 0;
        var iMin = 0;
        var iSec = 0;

        iHour = parseInt((iRemain / 3600), 10);
        iRemain %= 3600;

        iMin = parseInt((iRemain / 60), 10);
        iRemain %= 60;

        iSec = iRemain;
        // $('#J_dealCountDown').html('<span>' + fillZero(iHour, 2) + '<i></i></span><span>' + fillZero(iMin, 2) + '<i></i></span><span>' + fillZero(iSec, 2) + '<i></i></span>')
        $('.time-countdown').html(fillZero(iHour, 2) + '时' + fillZero(iMin, 2) + '分' + fillZero(iSec, 2) + '秒');
    }
    countDown();
    // setInterval(countDown, 1000);


	var timeElem = $('.time-countdown')[0];
	/*countdownTime(Date.parse(new Date('2013/11/15 14:08:20')) - Date.parse(new Date()),timeElem,function() {
	    timeElem.innerHTML = '活动进行中...';
	});*/
var nowData = new Date();
countdownTime(Date.parse(new Date(nowData.getFullYear()+'/'+(nowData.getMonth()+1)+'/'+nowData.getDate()))+(3600000*24)-Date.parse(nowData),timeElem,function() {
	    timeElem.innerHTML = '活动进行中...'
	})
	
})