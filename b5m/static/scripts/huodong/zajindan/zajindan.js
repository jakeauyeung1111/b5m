(function($,window) {

    var aggs = $('.agg1,.agg2,.agg3');
    var aggBreaks = $('.agg1-break,.agg2-break,.agg3-break');
    var aggHot = $('.agg-hot1,.agg-hot2,.agg-hot3');

    var infoText = $('.popup').find('div');

    var aggHotFlag = true;

    aggHot.each(function(i,n) {

        var agg = $('.agg'+(i+1)),
            aggHover = agg.next();

        $(n).on({
            mouseenter:function() {

                if(!aggHotFlag) return false;

                agg.hide();
                aggHover.show();
            },
            mouseleave:function() {

                if(!aggHotFlag) return false;

                agg.show();
                aggHover.hide();
            },
            click:function() {

                if(!aggHotFlag) return false;
                aggHotFlag = false;

                switch(hddata) {
                    case '-1':
                        infoText.text('您未登录，不符合活动条件，请登录后再来参加活动，活动细则见下');
                        popup();
                        break;
                    case '-3':
                        infoText.text('听说逛多个页面就会召唤幸运女神哦~ 而且在每个页面的停留时间越长，获奖机会越大呢~');
                        popup();
                        break;

                    case '-2':
                        infoText.text('没有浏览页面');
                        popup();
                        break;

                    case '1':

                        $.ajax({
                            url:'http://www.b5m.com/hd-ajax.php',
                            data: {mod:'award'},
                            dataType: "jsonp",
                            jsonp: 'jsonpCallback',
                            success: function(data) {

                                switch(data.code) {
                                    case '1':

                                        aggs.hide();
                                        aggBreaks.show();
                                        showPrize(i,data.data);

                                        break;
                                    case '-1':
                                        infoText.text('您未登录，不符合活动条件，请登录后再来参加活动，活动细则见下');
                                        popup();
                                        break;
                                    case '0':
                                        infoText.text('听说逛多个页面就会召唤幸运女神哦~ 而且在每个页面的停留时间越长，获奖机会越大呢');
                                        popup();
                                        break;
                                }
                            }
                        });
                        break;
                    default:
                        return false;
                }

            }
        });
    });


    var panel = $('.zjd'),
        mouse = $('.zjd .mouse'),
        areaTop = panel.offset().top,
        areaBottom = areaTop+620
    mouseState = false;

    panel.on({
        mousedown:function() {
            mouseState = true;
            mouse.addClass('on');
        },
        mouseup:function() {
            mouseState = false;
            mouse.removeClass('on');
        }
    });

    $(document).on({
        mousemove:function(e) {

            if(mouseState===true) {
                mouse.removeClass('on');
                mouseState = false;
            }

            if(e.pageY >= areaTop && e.pageY <= areaBottom) {
                mouse.css({
                    left: e.pageX-90,
                    top: e.pageY-270
                });
            }
        }
    });

    var prizeBangdou =$('.price-bangdou'),
        prizes = $('.price-iphone,.price-calls50,.price-mini,.price-calls100,.price-q30,.price-q50,.price-dou1000,.price-dou500,.price-dou200,.price-dou100'),
        prizesArr = [].slice.call(prizes);


  //  showPrize(0,10);

    function showPrize(index,value) {

        var random1 = getPrize();
        var random2 = getPrize(random1);

        prizeBangdou.addClass('b'+value);

        var posArr = $.map(aggHot,function(n) {
            return parseInt($(n).css('left'));
        });

        prizeAnimate.call(prizeBangdou,posArr[index]);
        prizeAnimate.call(prizesArr[random1],posArr[index = index != 2 ? (++index) : 0]);
        prizeAnimate.call(prizesArr[random2],posArr[index != 2 ? (++index) : 0]);



        function getPrize(num) {
            var r = Math.floor(Math.random()*(prizesArr.length));
            if(r===num) {
                return getPrize(num);
            }
            return r;
        }
    }

    function prizeAnimate(pos) {
        $(this).show().css('opacity','*1');
        $(this).css('left',pos).animate({
            top:140,
            opacity:1
        },1500);

        setTimeout(function() {
            prizeBangdou.fadeOut(1000);
            prizes.fadeOut(1000,function() {
                location.href = location.href;
            });
        },8000);
    }

    function popup() {
        var popup = $('.popup');
        aggHotFlag = false;
        popup.fadeIn(1000);
        popup.find('.close').click(function() {
            popup.fadeOut(1000,function() {
                aggHotFlag = true;
            });
        });
    }

    /*var timeElem = $('.times span')[0];
     countdownTime(Date.parse(new Date($('#ser_time').val()))-Date.parse(new Date()),timeElem,function() {
     timeElem.innerHTML = '活动进行中...';
     });*/

})(jQuery,window);