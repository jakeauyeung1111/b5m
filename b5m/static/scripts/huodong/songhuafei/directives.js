phoneChargeApp.directive('modalDialog',['$location','$http',function($location,$http) {
 return {
        restrict: 'A',
        scope: {
          show: '=',
          type:'@',
          showType:'='
        },
        replace: true,
        link: function(scope, element, attrs) {

            scope.hideModal = function() {
                scope.show = false;
            };

            scope.contentUrl = 'huodong/songhuafei/partials/' + attrs.type + '.html';
            // scope.contentUrl = 'partials/' + attrs.type + '.html';

            scope.gotoBindEmail = function(){
                $location.path('/email');
            }
            scope.gotoBindMobile = function(){
                $location.path('/mobile');
            }

            scope.shareWeiBo = function(event){
                var url = 'http://ucenter.b5m.com/user/third/getToken.htm?state=2&jsonpCallback=JSON_CALLBACK';

                $http.jsonp(url).success(function(data){
                    if(data.ok){
                        
                        var content = encodeURIComponent($.trim($('.share-text').text())),
                            title = '测试';

                        var shareUrl = 'http://ucenter.b5m.com/user/third/share.htm?state=2&summary='+ content + '&title=' + title + '&jsonpCallback=JSON_CALLBACK';

                        $http.jsonp(shareUrl).success(function(data){});

                        //分享微博的url
                        var successUrl = 'http://t.b5m.com/event/huafei?op=isShare&csuccess=ok';

                        //点击分享按钮后弹出成功领取话费
                        $http({
                            method:'GET',
                            url:successUrl
                        }).success(function(){
                            $('#J_success').show();
                            $('#J_share').hide();
                        })



                    }else{
                        var weiboUrl = "http://ucenter.b5m.com/user/third/login/auth.htm?type=2&refererUrl=&api=/setToken";
                        var win = window.open();
                        setTimeout(function() {
                            win.location = weiboUrl;
                        }, 100)
                    }
                })
                event.preventDefault();
            }

            scope.gotoLogin = function(){
                $location.path('/');
            }
            scope.BindedPhone = attrs.telephone;
        },
        template:'<div ng-include="contentUrl" ></div>'
    };
}]);