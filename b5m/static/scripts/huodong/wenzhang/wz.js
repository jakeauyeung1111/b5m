var wenZhangFed = {
    /*详情页图片切换*/
    dataPicSlide:function(){
        var iNow=0,
            nextUrl=$("#next_pic").val();

        $this=$("#data-slide"),
            liW=$this.find(".pic-main li").width(),
            allNumb=$this.find(".pic-main li").length;
        $this.on("click",".right-btn",function(){
            if(iNow<allNumb-1){
                iNow++;
            }else{
                iNow = 0;
            };
            picSlide();
        });
        $this.on("click",".left-btn",function(){
            if(iNow>0){
                iNow--;
            }else{
                iNow=allNumb-1;
            };
            picSlide();
        });
        $(".pic-main").find("img").click(function(){
            $this.find(".right-btn").trigger("click");
        });
        function add0(numb){
            if(numb<10){
                numb="0"+numb;
            };
            return numb;
        };
        function picSlide(){
            var numNow=iNow+1,
                sLi=$this.find(".pic-main ul li");
            //$this.css("height",sLi.eq(iNow).height());
            $this.find(".now-numb").text(add0(numNow));
            $this.find(".all-numb").text(add0(allNumb));
            sLi.eq(iNow).show().siblings("li").hide();
            sLi.eq(iNow).find('img').imglazyload({fadeIn:true});
        };
        picSlide();
    },
    /**
     * 大家都在看
     */
    allLookPic:function(){
        var $picCont=$("#al-pic-cont");
        var $this = $picCont.find('ul');
        var $LiL = $this.find('li').length;
        var $LiW = $this.find('li').width()+10;
        $this.width($LiL*$LiW+"px");

        if($LiL<1){
            $picCont.hide();
        }else{
            $picCont.show();
        };

        if($LiL>3){
            $picCont.find('span').show();
        }else{
            $picCont.find('span').hide();
        };
        $picCont.on('click','.left-btn',function(){
            if(!$this.is(":animated")){
                $this.animate({
                    left: '+=202'
                }, 300,function(){
                    $this.css('left',0).find('li:last').prependTo($this);
                });
            }
        });
        $picCont.on('click','.right-btn',function(){
            if(!$this.is(":animated")){
                $this.animate({
                    left: '-=202'
                }, 300,function(){
                    $this.css('left',0).find('li:first').appendTo($this);
                });
            }
        });
    },
    loadFun:function(){
        this.dataPicSlide();
        this.allLookPic();
    }
};