/**
 *Document:个人中心
 *Creat:13-10-9 下午6:13
 *Author:junli.shen@b5m.com
 */
var userFed = {
    ie6:typeof document.body.style.maxHeight=="undefined",
    /**
     * Tab切换特效
     * @param 非Tab导航类名
     */
    tabFun:function(noTabClass){
        var _navBx = '.layout-tab-tit'
            ,_tabBx = $(_navBx).parent()
            ,tabNav = $(_navBx).children().not(noTabClass||'.noTab')
            ,i = window.location['href'].lastIndexOf('#')
            ,num = '';
        if (i != -1) {
            num = parseInt(window.location['href'].substr(i+1, 1));
            if (num && num != NaN) {
                num = num - 1;
                $(_navBx).find('span').removeClass('tabCur');
                $(_navBx).find('span').eq(num).addClass('tabCur');
                _tabBx.find('.layout-tab-content').hide().eq(num).show();
            }
        }
        tabNav.hover(function(){
            var _index = tabNav.index(this);
            $(this).addClass('tabCur').siblings().removeClass('tabCur');
            _tabBx.find('.layout-tab-content').hide().eq(_index).show();
        });
    },
    /**
     * Accordion切换特效
     * @param type
     */
    accordionFun:function(type){
        var $accordion = $('.accordion'),
            _accordionTrigger = $accordion.children('h4'),
            _accordionContent = $accordion.children('div'),
            _cur = 'open',
            _type = type || 'click';

        _accordionContent.not(':first').hide();

        _accordionTrigger[_type](function(){
            var _this = $(this);
            _accordionContent.hide();;
            _accordionTrigger.removeClass(_cur);
            _this.addClass(_cur).next('div').show();;
        });
    },
    LoadAccordionFun:function(type){
        var _this = userFed;
        _this.accordionFun(type);
    },    
    /**
     * Select切换特效
     */
    selectFun:function(){
        var $select = $('.select');

        $select.each(function(){
            var _this = $(this),
                $selectDefault = _this.find('.txt'),
                $selectItemBox = _this.find('dd'),
                $selectItem = $selectItemBox.find('a');

            _this.on('click',function(){
                $(this).toggleClass('open');
                $selectItemBox.toggle();
                return false;
            });
            $selectItem.click(function(e){
                e.preventDefault();
                var $this = $(this),
                    _txt = $this.text();
                    _val = $this.data('val');
                $this.addClass('cur').siblings('a').removeClass('cur');
                $selectDefault.text(_txt);
                $('#select_val').val(_val);
            });
        });

        $(document).on('click',function(){
            $select.removeClass('open');
            $select.find('dd').hide();
        });
    },
    /*
     * dialog
     * @param :title
     */
    dialogFun:function(content,title){
        var _title = title || "提示",
            _content = $(content),
            _html = "",
            doc_w = $(document).width(),
            doc_h = $(document).height(),
            win_h = $(window).height(),
            scroll_top = $(window).scrollTop(),
            $dialog_mask = $('.dialog-mask');
            _this = userFed;

        if(!_content) return false;

        _html = '<div class="dialog"><div class="dialog__head"><h3 class="title">';
        _html += _title;
        _html += '</h3><a href="javascript:void(0);" class="close">x</a></div><div class="dialog__body">';
        _html += '</div></div>';

        // open the dialog
        if(!$dialog_mask.length){
            $dialog_mask = $('<div class="dialog-mask"></div>').css('height',doc_h).appendTo($('body'));
            $(_html).appendTo($('body'));
            $('.dialog__body').append(_content);
        }else{
            $dialog_mask.show();
            $('.dialog').show();
        }

        if(userFed.ie6){
            $(".dialog").css({
                'width':500
            });
        }

        var $dialog = $(".dialog"),
            $close = $dialog.find('.close'),
            $btn = $dialog.find('.btn'),
            dialog_w = $dialog.outerWidth(),
            dialog_h = $dialog.outerHeight(),
            // position
            pos_x = parseInt((doc_w - dialog_w)/2),
            pos_y = parseInt((win_h + scroll_top - dialog_h)/2);

        $dialog_mask.css({'height':doc_h});

        $dialog.css({
            'left':pos_x,
            'top':pos_y
        });

        // close the  dialog
        $close.on('click',function(e){
            e.preventDefault();
            _this.hideDialog($dialog,$dialog_mask);
        });

        
    },
    hideDialog:function(objDialog,objMask){
        if(!objDialog.length || !objMask.length) return false;
        objDialog.remove();
        objMask.remove();
    },
    /*帮豆-任务列表*/
    bangdouTaskActLoadFun:function(){
        var _this = userFed;
        _this.tabFun();
    },
    /*帮豆-帮豆订单记录*/
    bangdouOrderLoadFun:function(){
        var _this = userFed;
        _this.tabFun();
        _this.selectFun();
        _this.viewSuccess();
        $( ".dateStart").each(function(i){
            $( ".dateStart").eq(i).datepicker({
                onClose: function( selectedDate ) {
                    $( ".dateEnd").eq(i).datepicker( "option", "minDate", selectedDate, "yyyy-mm-dd" );
                }
            });
            $( ".dateEnd").eq(i).datepicker({
                onClose: function( selectedDate ) {
                    $( ".dateStart" ).eq(i).datepicker( "option", "maxDate", selectedDate, "yyyy-mm-dd" );
                }
            });
        });
    },
    /*帮币-帮币订单记录*/
    bangbiOrderLoadFun:function(){
        var _this = userFed;
        _this.tabFun();
        _this.selectFun();

        $( ".dateStart").each(function(i){
            $( ".dateStart").eq(i).datepicker({
                onClose: function( selectedDate ) {
                    $( ".dateEnd").eq(i).datepicker( "option", "minDate", selectedDate, "yyyy-mm-dd" );
                }
            });
            $( ".dateEnd").eq(i).datepicker({
                onClose: function( selectedDate ) {
                    $( ".dateStart" ).eq(i).datepicker( "option", "maxDate", selectedDate, "yyyy-mm-dd" );
                }
            });
        });


        
    },
    /*我的订单*/
    myorderLoadFun:function(){
        var _this = userFed;
        _this.selectFun();
        $( ".dateStart").each(function(i){
            $( ".dateStart").eq(i).datepicker({
                onClose: function( selectedDate ) {
                    $( ".dateEnd").eq(i).datepicker( "option", "minDate", selectedDate, "yyyy-mm-dd" );
                }
            });
            $( ".dateEnd").eq(i).datepicker({
                onClose: function( selectedDate ) {
                    $( ".dateStart" ).eq(i).datepicker( "option", "maxDate", selectedDate, "yyyy-mm-dd" );
                }
            });
        });
    },
    /*我的订单交易记录*/
    myordeTradeDetailLoadFun:function(){
        var _this = userFed;
        if(_this.ie6){
            $('.myorder-trade-control').hover(function(){
                $(this).css('zIndex',11).children('div').show();
            },function(){
                $(this).removeAttr('style').children('div').hide();
            });
        }
    },
    /*已领优惠券*/
    mydiscuponLoadFun:function(){
        var _this = userFed;
        $( ".dateStart").each(function(i){
            $( ".dateStart").eq(i).datepicker({
                onClose: function( selectedDate ) {
                    $( ".dateEnd").eq(i).datepicker( "option", "minDate", selectedDate, "yyyy-mm-dd" );
                }
            });
            $( ".dateEnd").eq(i).datepicker({
                onClose: function( selectedDate ) {
                    $( ".dateStart" ).eq(i).datepicker( "option", "maxDate", selectedDate, "yyyy-mm-dd" );
                }
            });
        });
    },
    /*帮币帮豆兑换-兑换记录*/
    forrecordLoadFun:function(){
        var _this = userFed;
        _this.selectFun();
        $( ".dateStart").each(function(i){
            $( ".dateStart").eq(i).datepicker({
                onClose: function( selectedDate ) {
                    $( ".dateEnd").eq(i).datepicker( "option", "minDate", selectedDate, "yyyy-mm-dd" );
                }
            });
            $( ".dateEnd").eq(i).datepicker({
                onClose: function( selectedDate ) {
                    $( ".dateStart" ).eq(i).datepicker( "option", "maxDate", selectedDate, "yyyy-mm-dd" );
                }
            });
        });
    },
    /*获取验证码*/
    getCodeFun:function(obj,num){
        if(!obj) return;
        var second = num,
            flag = null,
            _this = userFed;
        obj.addClass('waiting').val("重新发送验证码（" + second +"）");
        obj.attr({'disabled':true});

        flag = setInterval(function(){
            if(second <= 0){
                obj.removeClass('waiting').val('重新获取验证码');
                obj.attr({'disabled':false});
                clearInterval(flag);
                return;
            }
            obj.val("重新发送验证码（" + (--second) +"）");
        },1000);
    },
    /*交易成功*/
    viewSuccess:function() {
        $('.view-success').click(function() {
            var $this = $(this);
            if($this.hasClass('on')) {
                $this.removeClass('on');
                $this.parent().parent().next(".view-success-panel").removeClass("show");
            }else {
                $this.addClass('on');
                $this.parent().parent().next(".view-success-panel").addClass("show");
            }
        });
    },
    //提示添加头像
    addHead: function(){
        var userPic = $('.user-pic'),
            defaultUrl = '../../images/center/default-avatar-large.png';
        userPic.mouseenter(function(){
            var curUrl = $(this).find('img').attr('src');
            if (curUrl == defaultUrl) {
                userPic.find('a').animate({
                    bottom: 21
                }, 200);
            }
        }).mouseleave(function(){
            userPic.find('a').animate({
                bottom: -1
            }, 200);
        });
    },
    //消息中心
    showDetail: function(){
        var msgList = $('.user-message-list');
        msgList.on('click', '.message-title', function(){
            var _self = $(this);
            if (_self.parent().hasClass('message-bold')) {
                _self.parent().removeClass('message-bold');
            }
            _self.parent().parent().next().find('.message-detail').toggle();
        });
    },
    /*我的收藏*/
    myfavoritesLoadFun:function(){
        var _this = userFed;
        _this.tabFun();
        //商家选择
        $('.layout-favorites-corp a').on('click', function(){
            var $this = $(this);
            $('.layout-favorites-corp a').removeClass('current');
            $this.addClass('current');
        });
        //宝贝筛选条件
        var $opt = $('.favorites-sel').find('span');
        $opt.on('click', function(){
            var $this = $(this);
            if (!$this.hasClass('current')) {
                $this.siblings().removeClass('current');
                $this.addClass('current');
            } else {
                $this.find('i').toggleClass('down');
            }
        });
        //选择商品
        var $selAll = $('#selectAll'),
            $chk = $('.fav-chk');
        $selAll.change(function(){
            var state = this.checked;
            $chk.each(function(){
                this.checked = state;
            });
        }).change();
        $chk.change(function(){
            var count = $chk.filter(':checked').length;
            if (count < $chk.length) {
                $selAll.prop('checked', false);
            } else {
                $selAll.prop('checked', true);
            }
        }).change();
    },
    //道具仓库
    mypropLoadFun: function(){
        var _this = userFed;
        _this.tabFun();
        $('.prop-used').find('tbody').find('tr').filter(':odd').addClass('even');
    }
};