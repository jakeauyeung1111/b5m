;(function($){
    $(function(){
        $('#J_goods_list').find('.pic').b5m_getComment();
    });

    $.b5m = $.b5m || {};

    $.b5m.getComment = function(el,options){
        var base = this;
        base.$el = $(el);
        base.el = el;

        base.init = function(){
            var opts = $.extend({},$.b5m.getComment.defaultOptions || {});

            base.bindEvent();
        }

        base.bindEvent = function(){
            base.$el.hover(function(){
                // var flag = base.$el.data('loaded');
                // if(!flag){
                //     base.getContent();
                //     base.$el.data('loaded',true);
                // }else{
                    base.$el.parents('.grid-mod').find('.goods-comments').show();
                // }
            },function(){
                var flag = setTimeout(function(){
                    base.$el.parents('.grid-mod').find('.goods-comments').hide();
                },200);

                base.$el.parents('.grid-mod').find('.goods-comments').hover(function(){
                    clearTimeout(flag);
                },function(){
                    var flag = setTimeout(function(){
                        base.$el.parents('.grid-mod').find('.goods-comments').hide();
                    },200);
                });
            })
        }

        base.getContent = function(){
            // var url = '';

            // $.ajax({
            //     url: url,
            //     type: 'get',
            //     dataType: 'html'
            // })
            // .done(function(data) {
            //     var html = [];

            //     html.push('<div class="goods-comments"><h3>热门评论</h3><ul class="content-comments">');
            //     html.push(data);
            //     html.push('</ul><div class="show-more-comment"><span>展开所有的评论内容</span></div><span class="arrow-comment"></span></div>');
            //     html.join('');


            //     $box.append(html);

            //     base.checkPos();

            // })          
                // var data = '<li> <p class="goods-info"> <span class="goods-item">颜色：</span>黑色 <span class="goods-item">尺码：</span>s码 <span class="goods-item">建议：</span>25-2 </p> <p class="comments-detail"> 发货很快啊！草草草 </p> <p class="commment-info"> <span class="time r">2014.02.13</span> <span class="uname l">小绵羊</span> </p> </li>'
                // var htmlArr = [],
                //     $box = base.$el.parents('.grid-mod');

                // htmlArr.push('<div class="goods-comments"><h3>热门评论</h3><ul class="content-comments">');
                // htmlArr.push(data);
                // htmlArr.push('</ul><div class="show-more-comment"><span>查看所有评论内容</span></div><span class="arrow-comment"></span></div>');
                
                // htmlStr = htmlArr.join('');


                // $box.append(htmlStr);

                base.checkPos();
        }
        
        base.checkPos = function(){

            var win = $(window).width(),
                $box = base.$el.parents('.grid-mod'),
                $comment = $box.find('.goods-comments');
                offsetLeft = $comment.offset().left + $comment.outerWidth(),
                $showMore = $box.find('.show-more-comment');

            if(win < offsetLeft){
                $comment.addClass('goods-comments-left');
            }else{
                $comment.removeClass('goods-comments-left');
            }

            $showMore.on('click',function(){

                $(this).toggleClass('show-more-comment-open');
            });
        }
    }

    $.b5m.getComment.defaultOptions = {

    }

    $.fn.b5m_getComment = function(options){
        return this.each(function() {
            new $.b5m.getComment(this,options).init();
        });
    }    

    $.b5m.miniSlider = function(el,options){
        var base = this;
        base.$el = $(el);
        base.el = el;

        base.init = function(){
            var opts = $.extend({},$.b5m.miniSlider.defaultOptions,options || {});
            base.trigger = opts.trigger;

            base.bindEvent();
        }

        base.bindEvent = function(){
            var $container = base.$el,
                $slider = $container.find('ul'),
                $slider_item = $slider.find('li'),
                $slider_a = $slider.find('a'),
                $img = $slider_item.find('img'),
                item_w = $slider_item.outerWidth(true),
                step = $slider_item.length - 5,
                n = 0,
                $trigger = $container.find('.' + base.trigger),
                $targetImg = $container.siblings('.pic-mod').find('img');

            $img.on('mouseover',function(e){
                var src = this.src;

                $targetImg[0].src = src;
                $slider_a.removeClass('active');
                $(this).parent('a').addClass('active');
                e.preventDefault();
            });

            //若缩略图小于4个就隐藏左右箭头
            if(step <= 0){
                $trigger.hide();
                return false;
            }

            $trigger.eq(0).addClass('arrow-left-disable');

            $trigger.on('click',function(){
                if($(this).hasClass('arrow-right') && (n < step)){
                    n++;
                    $slider.animate({left:-item_w*n});
                }else if($(this).hasClass('arrow-left') && (n >= 1)){
                    n--;
                    $slider.animate({left:-item_w*n});
                }

                if(n > 0 && n < step ){
                    $trigger.removeClass('arrow-left-disable arrow-right-disable');
                }else if( n == 0){
                    $trigger.eq(0).addClass('arrow-left-disable');
                    $trigger.eq(1).removeClass('arrow-right-disable')
                }else if( n == step){
                    $trigger.eq(1).addClass('arrow-right-disable');
                    $trigger.eq(0).removeClass('arrow-left-disable')
                }
            });


        }

    }

    $.b5m.miniSlider.defaultOptions = {
        trigger:'arrow'
    }

    $.fn.b5m_miniSlider = function(options){
        return this.each(function(){
            new $.b5m.miniSlider(this,options).init();
        })
    }


    $('.mini-slider').b5m_miniSlider();

})(jQuery)