var callbackIsEamilUse = {
	resultData : true,
	requestP : function(value, type) {
		$.ajax({
			type : 'post',
			dataType : "jsonp",
			data : type + "=" + value + "&jsonpCallback=successIsEamilUse",
			async : false,
			url : rootPath + '/user/info/data/isNameOrEmailUse.do'
		});
	},
	resultFunction : function() {
		return this.resultData;
	}
};
var successIsEamilUse = function(data) {
	callbackIsEamilUse.resultData = eval(data).code == 0 ? true : false;
};
$(function(){
	
	var $loginForm = $('#loginForm');
	
	$('#refreshCode').click(function(){
		refreshCode();
	});
	$('#submit_btn').click(function(e){
		e.preventDefault();
		submitHandler();
	});

	$('input[name="code"],input[name="password"]').keydown(function(event){
		if(event.keyCode == 13)
			submitHandler();
	});
	initData();
	function submitHandler(){
		// if($loginForm.valid()){
			console.log($loginForm.serialize());
			$.ajax({
				type : "post",
				dataType : "jsonp",
				url : $loginForm.attr('action'),
				data : $loginForm.serialize(),
				jsonp:'jsonpCallback',
				async : false,
				success:function(data){
					success1(data);
				}
			});
		// }
	}
	
	function initData(){
		$('#serror').empty();
		$loginForm.find('.errorBox').empty().css('display','none');
		$loginForm.find('input').each(function(){
			$(this).removeClass('text_error');
		});
		
		// var type = $('.login_tab > .cur').attr('data-type') || 'login';
		var type = 'login';
		if(type == 'login'){
			$loginForm.attr('action',rootPath+'/user/user/data/login.htm');
			$loginForm.find('input[name="code"]').hide();
			$('#verify').css("display","none");
			$('.login-form').css('margin-top',35);
			$('#uNameRow').hide();
			
		}else if(type == 'register'){
			$loginForm.attr('action',rootPath+'/user/info/data/register.htm');
			$loginForm.find('input[name="code"]').show();
			$loginForm.find('input[name="email"]').rules("add", {
				isEmail: true
			});
			$('#verify').css("display","block");
			$('.login-form').css('margin-top',35);
			$('#uNameRow').show();
		}else{
			alert('出错啦！！');
		}
	}
	
	// $(".login_tab a").bind('click', function(e) {
	//     e.preventDefault();
	//     var parent = $(this).parent();
	//     if (parent.hasClass('cur')) {
	//         return false;
	//     }
	//     parent.parent().find('.cur').removeClass();
	//     parent.addClass('cur');
	//     $('.submit_rows a').hide();
	//     $("#" + parent.attr('data-type') + '_btn').show();
	    
	//     $('input[name="email"]').val('');
	//     $('input[name="password"]').val('');
	//     $('input[name="code"]').val('');
	//     if(parent.attr('data-type') == "register"){
	//     	$('.login').addClass('register');
	//     	$('.login_tab_cont').css('height','207px');
	//     }else{
	//     	$('.login').removeClass('register');
	//     	$('.login_tab_cont').css('height','207px');
	//     }
	    
	//     initData();
	// });
});

var rootPath = 'http://ucenter.b5m.com';

function refreshCode(){
	$('#code').attr('src',rootPath+'/validateCode.do?f='+Math.random());
}

var success1 = function(datas) {
	var data = eval(datas);
	if(data.ok){
		if (data.code == '10011') {
			var count = 0;
			var $scripts = $('<span>' + data.data + '</span>').find('script');
			$scripts.each(function() {
				$.getScript($(this).attr('src'), function() {
					count = count + 1;
				});
			});
			var si = setInterval(function() {
				if (count == $scripts.size()) {
					window.location.href = $('#dialog_redirectUrl').val();
					clearInterval(si);
				}
			}, 500);
		} else if (data.code == '10012') {
			window.location.href = data.data + '&loginReferer=' + encodeURIComponent($('#dialog_redirectUrl').val());
		} else {
			closeDialog(); 
		}
	}else{
		$.ajax({
			type:'post',
			dataType : "jsonp",
			url:rootPath+'/user/user/data/loginNum.do',
			data : "email="+$('input[name="email"]').val()
					+ "&jsonpCallback=successNum",
			async : false
		});
		$('#serror').empty().append('<img src="'+rootPath+'/images/ucenter/blank.png" class="ui-icon ui-icon-error">&nbsp;'+data.data);
		refreshCode();
	}
};

var successNum = function(datas){
	var data = eval(datas);
	var ok = data.ok;
	var num = data.data;
	var type = $('.login_tab > .cur').attr('data-type');
	if(type == 'register'){
		if(ok && parseInt(num) < 3){
			$('#verify').css("display","block");
			$('.login-form').css('margin-top',10);
		}else{
			$('#verify').css("display","block");
			$('.login-form').css('margin-top',10);
		}
	}else if(type == 'login'){
		if(ok && parseInt(num) < 3){
			$('#verify').css("display","none");
			$('#loginForm').find('input[name="code"]').hide();
			$('.login-form').css('margin-top',10);
		}else{
			$('#verify').css("display","block");
			$('#loginForm').find('input[name="code"]').show();
			$('.login-form').css('margin-top',10);
		}
	}
};
// $(function(){

// 	var $loginForm = $('#loginForm');

// 	$('#submit_btn').click(function(e){
// 		e.preventDefault();
// 		submitHandler();
// 	});

// 	$('input[name="code"],input[name="password"]').keydown(function(event){
// 		if(event.keyCode == 13)
// 			submitHandler();
// 	});
// 	function submitHandler(){
// 		$.ajax({
// 			type : "post",
// 			dataType : "jsonp",
// 			url : $loginForm.attr('action'),
// 			data : $loginForm.serialize()
// 					+ "&jsonpCallback=success1",
// 			async : false
// 		});
// 	}

// });
// var success1 = function(datas) {
// 	var rootPath = 'http://ucenter.b5m.com';
// 	var data = eval(datas);

// 	if(data.ok){
// 		if (data.code == '10011') {
// 			var count = 0;
// 			var $scripts = $('<span>' + data.data + '</span>').find('script');
// 			$scripts.each(function() {
// 				$.getScript($(this).attr('src'), function() {
// 					count = count + 1;
// 				});
// 			});
// 			var si = setInterval(function() {
// 				if (count == $scripts.size()) {
// 					window.location.href = $('#dialog_redirectUrl').val();
// 					clearInterval(si);
// 				}
// 			}, 500);
// 		} else if (data.code == '10012') {
// 			window.location.href = data.data + '&loginReferer=' + encodeURIComponent($('#dialog_redirectUrl').val());
// 		} else {
// 			closeDialog(); 
// 		}
// 	}else{
// 		$.ajax({
// 			type:'post',
// 			dataType : "jsonp",
// 			url:rootPath+'/user/user/data/loginNum.do',
// 			data : "email="+$('input[name="email"]').val()
// 					+ "&jsonpCallback=successNum",
// 			async : false
// 		});
// 		$('#serror').empty().append('<img src="'+rootPath+'/images/ucenter/blank.png" class="ui-icon ui-icon-error">&nbsp;'+data.data);
// 	}
// };

// var successNum = function(datas){
// 	var data = eval(datas);
// 	var ok = data.ok;
// 	var num = data.data;
// 	var type = $('.login_tab > .cur').attr('data-type') || 'login';
// 	if(type == 'register'){
// 		if(ok && parseInt(num) < 3){
// 			$('#verify').css("display","block");
// 			$('.login-form').css('margin-top',10);
// 		}else{
// 			$('#verify').css("display","block");
// 			$('.login-form').css('margin-top',10);
// 		}
// 	}else if(type == 'login'){
// 		if(ok && parseInt(num) < 3){
// 			$('#verify').css("display","none");
// 			$('#loginForm').find('input[name="code"]').hide();
// 			$('.login-form').css('margin-top',10);
// 		}else{
// 			$('#verify').css("display","block");
// 			$('#loginForm').find('input[name="code"]').show();
// 			$('.login-form').css('margin-top',10);
// 		}
// 	}
// };