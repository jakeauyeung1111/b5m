;(function($,window,document){

    $(function(){
        //有奖调查
        searchFed.indexFun();
        //调用弹出框
        $('.popup-trigger').b5m_popup();
        $('.feed-awards').b5m_popup({fixed:true,cName:'arrow-right'});

        $('#J_filter').b5m_toggleItem();

        MiniFilter.init();

        $('.side-l').b5m_fixTop({
            placeWrap:false,
            className:'fixed',
            offsetBottom:200
        });

        //左侧展开收起
        $('#J_category_nav').b5m_cateNav();
    });

    var searchFed = window.searchFed || {};

    searchFed = {
        indexInit:function(){
            $('.gotop').before('<div class="feed-awards" data-target="feed-popup"></div>');

            this.setPos();
        },
        //调整返回顶部按钮位置
        setPos:function(){
            var posLeft = parseInt($('.wp').width()/2 + 55);
            $('.gotop,.gotofushi,.feed-awards').css({
                'marginLeft':posLeft
            });
        },
        indexFun:function(){
            this.indexInit();
        }
    };

    $(window).resize(function(){
        var flag = null;
        clearTimeout(flag);

        flag = setTimeout(function(){
            searchFed.setPos();
        },400);
    });

	$.b5m = $.b5m || {};
	/**
	 * 展开收起更多属性值
	 */
	$.b5m.toggleItem = function(el,options){
		var base = this;
		base.$el = $(el);
		base.el = el;

		$(window).resize(function(){
			clearTimeout(flag);
			var flag = setTimeout(function(){
				base.winResize();
			},600);
		});

		base.init = function(){
			base.options = $.extend({},$.b5m.toggleItem.defaultOptions,options);
			$box = base.$el.find('.' + base.options.box);
			classFilter = base.options.classFilter;
			classMultiple = base.options.classMultiple;
			classOpen = base.options.classOpen;
			num = base.options.num;

			base.bindEvent();
			base.bindMore();
			base.winResize();
		}

		//绑定事件
		base.bindEvent = function(){

			//绑定展开收起事件
			$box.find('.show-more').on('click.showmore',function(){
				var $this = $(this);
					$parent = $this.parent();

				$this.toggleClass(classOpen)
					 .parent().toggleClass(classFilter);

				if($this.hasClass('open')){
                    $this.html('<span>收起</span>');
			    }else{

			    	//收起时，去掉多选
					$this
						.parent().removeClass(classMultiple);
			        $this.html('<span>展开</span>');
			    }
			});

			//绑定多选事件
			$box.find('.btn-multiple').on('click',function(e){
				$(this)
					.parents('.filter-item').addClass(classMultiple + ' ' + classFilter);
					$(this).parent('.filter-act').siblings('.show-more').addClass("open").text("收起");
				e.preventDefault();
			});

			//取消多选事件
			$box.find('.btn-cancle').on('click',function(e){
				var $this = $(this);
				$this
					.parents('.' + base.options.box).removeClass(classMultiple)
					.find('.show-more').trigger('click.showmore');
				e.preventDefault();
			});

			//确定多选事件
			$box.find('.btn-sure').on('click',function(e){
				var $this = $(this),
					$parent = $this.parents('.filter-item'),
					$filter = $parent.find('.filter-lists').find('.cur'),
					len = $filter.length,
					title = $parent.find('dt').attr('title'),
					data = '';

				$filter.each(function(index, el) {
					if(index == len - 1){
						data += title + ':' + $(el).text();
					}else{
						data += title + ':' + $(el).text() + ',';
					}
				});

				//提交数据
				searchMulti(data);
				e.preventDefault();

			});

			//每个条件添加事件
			$box.find('.filter-lists a').not('.not-filter').on('click',function(e){
				var $this = $(this);
				$this.toggleClass('cur');

				//多选情况下，阻止a的默认事件
				if($this.parents('.filter-item').hasClass('filter-multiply')){
					e.preventDefault();
				}
			});
		}

		/**
		* 展开收起其余属性值
		*/
		base.bindMore = function(){
			var $this = $('#J_more'),
                $toggbleBar = $this.parent('.filter-more'),
				items_len = $box.length;

            //筛选条件小于5个则删除展开更多按钮
            var lenFlag = items_len < 5;
            
            if(lenFlag){
                $toggbleBar.remove();
            }

            //标识最后一个是否为 ‘价格’
            var flag = $box.last().find('dt').text().slice(0,2) == '价格';

			$this.on('click',function(e){

                if(flag){
                    $box.filter(function(index){
        					return (index > (num - 2)) && index != (items_len - 1);
       				}).toggle();
                }else{
                    $box.filter(function(index){
                        return (index > (num - 2));
                    }).toggle();
                }

				$(this).toggleClass('open');

				//展开和收起在初次加载和window resize时显隐切换
				base.winResize();
				e.preventDefault();
			});
		},

		//展开和收起在初次加载和window resize时显隐切换
		base.winResize = function(){
			$box.each(function(){
				var $this = $(this),
				    $filtersBox = $this.find('.filter-lists'),
					box_width = $filtersBox.width(),
					$filters = $filtersBox.find('a'),
					filters_width = $filters.length * $filters.outerWidth(true);

				if(filters_width > box_width){
					$this.find('.show-more').show();
				}else{
					$this.find('.show-more').hide();
				}
			});
		}
	};

	$.b5m.toggleItem.defaultOptions = {
		box:'filter-item',
		classFilter:'filter-open',
		classMultiple:'filter-multiply',
		classOpen:'open',
		num:5
	};

    //插件入口
	$.fn.b5m_toggleItem = function(options){
		return this.each(function(){
			new $.b5m.toggleItem(this,options).init();
		});
	}


	/**
	 * 小屏幕下面顶部分类展开、收起
	 */
	var MiniFilter = {

		init:function(){
			var $box = $('#J_filter_cate'),
				$trigger = $box.find('.show-more');
			this.bindEvent.apply($trigger,arguments);
			this.winResize($trigger);
		},

		bindEvent:function(){

			//这里的this已经是$trigger jquery对象
			var $this = this;
			$this.on('click.showmore',function(){
				var $this = $(this);
				$this.toggleClass(classOpen)
					 .parent().find('ul,dl').toggleClass('filter-open');
				if($this.hasClass('open')){
                    $this.html('<span>收起</span>');
			    }else{
			        $this.html('<span>展开</span>');
			    }
			});
		},

		//展开和收起在初次加载和window resize时显隐切换
		winResize:function(obj){
			obj.each(function(){
				var $this = $(this),
				    $filtersBox = $this.parent().find('ul,dl'),
					box_width = $filtersBox.width(),
					$filters = $filtersBox.find('dd,li'),
					filters_width = $filters.length * $filters.outerWidth(true);

				if(filters_width > box_width){
					$filtersBox
						.parent()
						.find('.show-more').show();
				}else{
					$filtersBox
						.parent()
						.find('.show-more').hide();
				}
			});
		}
	}


	$(window).resize(function(){
		var $box = $('#J_filter_cate'),
			$trigger = $box.find('.show-more');
		MiniFilter.winResize($trigger);
	})

	/**
	 * 置顶/置底效果
	 */
    $.b5m.fixTop = function(el,options){

        var base = this;

        base.el = el;
        base.$el = $(el);

        base.init = function(){
            options = $.extend({}, $.b5m.fixTop.Defaults,options || {});

            base.placeWrap = options.placeWrap;
            base.className = options.className;
            base.offsetBottom = options.offsetBottom;

            base.bindEvent();
        }

        /**
         * 获得元素的信息：宽、高和距离屏幕左、上的距离
         * @returns {{size: {width: *, height: *}, position: {left: *, top: *}}}
         */
        base.getEleInfo = function(){
            return {
                size:{
                    width:base.$el.width(),
                    height:base.$el.outerHeight(true)
                },
                position:{
                    left:base.$el.offset().left,
                    top:base.$el.offset().top
                }
            };
        }

        /**
         * 判断元素是否在可视区域内
         * @returns {boolean}
         */
        base.isInWin = function(){
            var isShowTop = (base.getEleInfo().position.top + base.getEleInfo().size.height) > $(window).scrollTop(),
                isShowBottom = base.getEleInfo().position.top < ($(window).scrollTop() + $(window).height());
            return isShowTop && isShowBottom;
        }

        base.bindEvent = function(){
            var $win = $(window),
                winH = $(window).height(),
                defaultPos = base.$el.css('position'),
                defaultTop = base.getEleInfo().position.top;
                zIndex = base.$el.css('zIndex');

            $win.scroll(function(){
                var scrollTop = $(window).scrollTop(),
                    elemH = base.$el.outerHeight();

                //当元素的高度大于浏览器高度
                if(elemH > winH){

                    if((defaultTop + elemH) <= (scrollTop + winH)){

                        base.$el.css({
                            position:'fixed',
                            bottom:0,
//                            top:auto,
                            zIndex:70,
                            width:base.getEleInfo().size.width
                        }).addClass(base.className);

                        //当footer进入页面时，左侧位置不在吸底
                        if($('.footer').offset().top <= scrollTop + winH){
                            base.$el.css({
                                position:'fixed',
                                bottom:scrollTop + winH - $('.footer').offset().top,
                                top:'auto',
                                zIndex:70,
                                width:base.getEleInfo().size.width
                            })
                        }

                    }else{

                        base.$el.css({
                            position:defaultPos,
                            bottom:'auto',
                            top:'auto',
                            zIndex:zIndex
                        }).removeClass(base.className);

                    }

                }else{
                    if(defaultTop <= scrollTop){
                        base.$el.css({
                            position:'fixed',
                            top:0,
                            zIndex:70,
                            width:base.getEleInfo().size.width
                        }).addClass(base.className);

                        //判断父类是否有占位标签
                        if(!base.$el.next('div').hasClass('placeWrap') && base.placeWrap){
                            base.$el.after('<div class="placeWrap" style="height:' + base.getEleInfo().size.height + 'px"></div>');
                        }
                    }else{
                        base.$el.css({
                            position:defaultPos,
                            top:'auto',
                            bottom:'auto',
                            zIndex:zIndex,
                            width:base.getEleInfo().size.width
                        }).removeClass(base.className);

                        if(base.$el.next('div').hasClass('placeWrap') && base.placeWrap){
                            base.$el.next('.placeWrap').remove();
                        }

                    }

                }
            })
        }
    }


    $.b5m.fixTop.Defaults = {
        placeWrap:true,
        className:"",
        offsetBottom:0
    }

	$.fn.b5m_fixTop = function(options){
        return this.each(function(){
            new $.b5m.fixTop(this,options).init();
        })
	}



    /**
     * @param el
     * @param options
     */
    $.b5m.cateNav = function(el,options){
        var base = this;
        base.el = el;
        base.$el = $(el);

        base.init = function(){
            base.options = $.extend({}, $.b5m.cateNav.Defaults,options || {}),
            base.className =  base.options.className;
            base.nodeElement = base.options.nodeElement;
            base.showMore = base.options.showMore;
            base.moreOpen = base.options.moreOpen;
            base.num = base.options.num;

            base.bindEvent();
        }

        base.bindEvent = function(){

            var $items = base.$el.find(base.nodeElement),
            	$itemLinks = $items.find('a'),
                itemLen = $items.length;


            //展开点击元素，收起其余兄弟节点。
            $items.on('click',function(){
                var _this = $(this);

                //当此元素展开时，再次点击就收起。
                _this.parent().toggleClass(base.className)
                    .siblings().removeClass(base.className);
                $(window).trigger('scroll');
            });

            //阻止冒泡
            $itemLinks.on('click',function(e){
            	e.stopPropagation();
            });

            if(itemLen <= base.num){
                base.$el.find('.' + base.showMore).hide();
                return false;
            }

            var $targetItem = $items.parent().filter(function(index){
                return index > (base.num - 1);
            });

            base.$el.find('.' + base.showMore).on('click',function(){
                var _this = $(this);

                if(_this.hasClass(base.moreOpen)){
                    _this.removeClass(base.moreOpen);
                    $targetItem.hide().removeClass(base.className);
                }else{
                    _this.addClass(base.moreOpen);
                    $targetItem.show().removeClass(base.className);
                }
            });
        }
    }

    $.b5m.cateNav.Defaults = {
        className:'cur',
        nodeElement:'dl dt',
        showMore:'more-category',
        moreOpen:'more-category-open',
        num:5
    }


    $.fn.b5m_cateNav = function(options){
        return this.each(function(){
            new $.b5m.cateNav(this,options).init();
        });
    }

    /**
     * 弹窗
     * @param el
     * @param options
     */
	$.b5m.popup = function(el,options){
        var base = this;
        base.el = el,
        base.$el = $(el);

        base.init = function(){
            var opts = $.extend({}, $.b5m.popup.defaultOptions,options || {});
            var $target = $('.' + base.$el.data('target')).length ? $('.' + base.$el.data('target')):'';

            base.$target = $target || $('.' + opts.target);
            base.$close = $('.' + opts.close);
            base.$arrow = $('.' + opts.arrow);
            base.fixed = opts.fixed;
            base.cName = opts.cName;

            this.bindEvent();
        };

        base.bindEvent = function(){

            base.$el.on('click',function(){

//                if(base.$target.is(':hidden')){
                    base.show();

                    base.$target.on('click',function(e){
                        e.stopPropagation();
                    });

                    $(document).on('click',function(){
                        base.hide();
                    });

                    base.$close.on('click',function(){
                        base.hide();
                    })

                    $(window).resize(function(){
                        var flag = null;
                        clearTimeout(flag);

                        setTimeout(function(){
                            base.setPos();
                        },1000);
                    });
//                }else{
//                    base.hide();
//                }
                return false;
            });
        };

        base.getPos = function(){
            var coord = null,
                x = parseInt(base.$el.offset().left),
                y = parseInt(base.$el.offset().top);

            return coord = {
                x:x,
                y:y
            };
        };

        base.setPos = function(){
            var coord = base.getPos();

            base.$target.css({
                'position':'absolute',
                'left':coord.x - parseInt(base.$target.width() - base.$el.width()),
                'top':coord.y + 40,
                'bottom':'auto'
            })

            if(base.fixed){
                base.$target.css({
                    'position':'fixed',
                    'left':coord.x - parseInt(base.$target.width()) - 30,
                    'top':'auto',
                    'bottom':parseInt(base.$el.css('bottom'))
                })
            }
        };

        base.show = function(){
            base.$target.css({
                'display':'block'
            });

            base.$arrow.removeClass().addClass('arrow ' + base.cName);

            base.setPos();
        };

        base.hide = function(){
            base.$target.css({'display':'none'});
        };

	}

    $.b5m.popup.defaultOptions = {
        target:'feed-popup',
        close:'close',
        fixed:false,
        arrow:'arrow',
        cName:'arrow-top'
    };

    $.fn.b5m_popup = function(options){
        return this.each(function(){
            new $.b5m.popup(this,options).init();
        });
    };

    /*帮豆加分提示s*/
    $.fn.b5m_bangdouTips = function(){
        var tips;
        function init(num){
            return $('<div class="bangdou-tips" style="color:red"><div class="bangdou-tips__in">' + "+" + num + '</div></div>').appendTo($('.feed-popup').find('.btn-box'));
        }
        return {
            getTips:function(num){
                return tips || (tips = init(num));
            }
        }
    }();

    $.fn.b5m_addTips=function (num){
        $.fn.b5m_bangdouTips.getTips(num).stop(true,true).animate({opacity:1},400).delay(500).animate({opacity:0},800,function(){
            $('.popup-in').html('<div class="tips"><h5>您的意见已提交成功!</h5><p>谢谢参与，感谢您长久对帮5买的支持~</p></div>');
        });
    };
    /*帮豆加分提示e*/

    var validate = {
        check:function(element,url){
            var $target = $('.' + element),
                $items = $target.find('.required'),
                $submit_btn = $('#J_submit');

            $items.each(function(){
                var $this = $(this),
                    value = $.trim($this.val());

                if(!value){
                    $submit_btn.addClass('btn-disable').removeClass('btn-enable');
                    return false;
                }

                $submit_btn.addClass('btn-enable').removeClass('btn-disable');
            });

            $submit_btn.off().on('click',function(e){
                e.preventDefault();
                validate.submit($target,url);
            })
        },
        submit:function(obj,url){
            var data = obj.serialize();
                url = url + "?" + data;

            $.ajax({
                url: url,
                data: {},
                success:function(data){
                    var codes=data['code'];
                    if (Cookies.get('login') != "true") {
                        $('.popup-in').html('<div class="tips"><h5>您的意见已提交成功!</h5><p>谢谢参与，感谢您长久对帮5买的支持~</p></div>');
                    }else{
                        if(codes == 1){
                            $.fn.b5m_addTips('6');
                        };
                    }
                }
            })
            .always(function() {
                   /* if (Cookies.get('login') != "true") {
                        $('.popup-in').html('<div class="tips"><h5>您的意见已提交成功!</h5><p>谢谢参与，感谢您长久对帮5买的支持~</p></div>');
                    }else{
                        $.fn.b5m_addTips('6');
                    }*/
               // $('.popup-in').html('<div class="tips"><h5>您的意见已提交成功!</h5><p>谢谢参与，感谢您长久对帮5买的支持~</p></div>');
                var flag = setTimeout(function(){
                    $('.feed-popup').hide();
                    clearTimeout(flag);
                },4000);
            })

        }
    }

    $('.feed-form').find('.required').keyup(function(event) {

        if(typeof _basePath !== 'undefined'){
            validate.check('feed-form',_basePath + "searchRecommend.htm");
//            validate.check('feed-form',"searchRecommend.htm");
        }
    });
})(jQuery,window,document);
