;(function($){
    var searchFed = window.searchFed || {};

    searchFed.isLogin = function(){
        var isLogin = Cookies.get('login') === 'true' && Cookies.get('token');
        return isLogin;
    }
    //显示登录框
    searchFed.showLoginDialog = function(){
        var dialog = dialogLogin.getDialog(),
            scrollTop = $(window).scrollTop(),
            winH = $(window).height(),
            top = scrollTop + parseInt(winH/2) - $('.dialog-login').outerHeight(true);
        if(dialog){
            $('.dialog-login').show().css('top',top);
        }
        
        $('.dialog-login-close').on('click',function(e){
            $('.dialog-login').hide();
            e.preventDefault();
        });
    }    

    //请求用户中心数据
    searchFed.getUcenterInfo = function(type,target){
        var userId = Cookies.get('token'),
            $parent = target.parents('.grid-in'),
            goodsUrl = $parent.find('.pic').attr('href'),
            picUrl = $parent.find('.pic img').attr('src'),
            webSite = goodsUrl.match(/[^\.]+\.com/)[0],
            title = $parent.find('.summary a').text(),
            price = $parent.find('.price span').text();

        var ucenterUrl = searchFed.getUcenterUrl();

        $.ajax({
            url : 'http://' + ucenterUrl + '/gc/user/favorites/data/add.htm',
            type : 'POST',
            data : {
                userId : userId,
                goodsUrl : goodsUrl,
                picUrl : picUrl,
                title : title,
                webSite : webSite,
                price : price,
                source : 22
            },
            dataType : 'jsonp',
            jsonp : 'jsonpCallback',
            success : function(result) {
                if(type == 1){
                    searchFed.getRecommendGoods(result);
                }else if(type == 2){
                    var isSuccess = result.ok,
                        status = result.code;
                    if(!isSuccess && status != 40006){
                        alert('添加失败')
                    }else if(!isSuccess && status == 40006){
                        //多次添加提示已经添加过此商品
                        alert('重复添加');
                    }else if(isSuccess){
                        //提示添加降价提醒成功
                        alert('添加成功');
                    }
                }
            }
        })
    }  

    searchFed.getUcenterUrl = function(){

        var ucenterUrl = (function () {
            if (location.hostname.indexOf('stage.bang5mai.com') !== -1) {
                _domain = '.bang5mai.com';
                return 'ucenter.stage.bang5mai.com';
            }
            if (location.hostname.indexOf('prod.bang5mai.com') !== -1) {
                _domain = '.bang5mai.com';
                return 'ucenter.prod.bang5mai.com';
            }
            if (location.hostname.indexOf('ucenter.test.com') !== -1) {
                _domain = '.test.com';
                return 'ucenter.test.com';
            }
            return 'ucenter.b5m.com';
        })();

        return ucenterUrl;
    }

    var dialogLogin = (function(){
        var dialog = null,
            loginUrl = $('.topbar-user-unlogin').find('a').eq(2).attr('href'),
            registerUrl = $('.topbar-user-unlogin').find('a').eq(3).attr('href');

        function Dialog(){
            var html = '';
                html = '<div class="dialog-login">';
                html += '<div class="dialog-login-in">';
                html += '<h3>为了能及时更新您收藏的商品，需要登录网站</h3>';
                html += '<div class="login-register-mod">';
                html += '<div class="login-mod">';
                html += '<p>已有账号</p>';
                html += '<a href="' + loginUrl +'" class="dialog-btn btn-login">去登录</a>';
                html += '</div>';
                html += '<div class="register-mod">';
                html += '<p>暂无账号</p>';
                html += '<a href="' + registerUrl + '" class="dialog-btn btn-register">去注册</a>';
                html += '</div>';
                html += '</div>';
                html += '</div>';
                html += '<a href="#" class="dialog-login-close">x</a>';
                html += '</div>';
            return html;
        }

        return {
            getDialog:function(){
                return dialog || (dialog = $('body').append(Dialog()));
            }
        }
    })();

    //添加收藏
    $('.goods-add').on('click',function(e){
        e.preventDefault();

        var isLogin = searchFed.isLogin(),
            base = $(this);

        //是否登录
        if(!isLogin){
            searchFed.showLoginDialog();
        }else{
            searchFed.getUcenterInfo(2,base)
        }

    })    
})(jQuery);