$(function(){
	function toggleProd(){
		$('.opt').each(function(){
			var $this = $(this),
				flag = true;

			$this.click(function(){
					$target_box = $this.parent('.tab-shop-item'),
					target_box_h = $target_box.css('height','auto').height();
					target_ls_h = $target_box.find('li').outerHeight(),
					txt = $this.html();
				if(flag){
					$target_box.css('height',target_ls_h).stop(true,true).animate({'height':target_box_h});
					flag = false;
					txt = txt.replace('展开','收起');
					$this.html(txt);
					$this.addClass('close');
				}else{
					$target_box.stop(true,true).animate({'height':target_ls_h-1});
					flag = true;
					txt = txt.replace('收起','展开');
					$this.html(txt);
					$this.removeClass('close');
				}	
			});
		});
	}
	toggleProd();
	function fixedPos(obj){
		if (!obj) return false;
	    var iTarget = $(obj).offset().top;
	    $('html,body').animate({scrollTop: iTarget}, 'fast');
	}
	$('#detail-tabs').on('click','li',function(){
		$(this).addClass("cur").siblings('li').removeClass('cur');
		fixedPos("#detail-tabs");
		var index = $(this).index();
		$('.tab-box').eq(index).show().siblings().hide();
	});
	// 定位Tab信息
	$('.target-tab').on('click',function(){
		var $target = $('#'+$(this).data('type'));
		targetTab($target);
	});
	function targetTab(obj){
		if(!obj) return;
		obj.trigger('click');
	};
	$('.details-parameters tr:gt(10)').hide();
	$('.all-xq').on('click',function(){
		$(this).hide();
		$(this).siblings('.details-parameters').find('tr').show();
	});

	//查看平稳价格趋势
	$('#J_view_history_price').click(function(event) {
		event.preventDefault();
		var removenode = $('.adv-flat-pic')[0];
		removenode.parentNode.removeChild(removenode);
		this.style.cssText = 'text-decoration:none;cursor:default;';
		$('.flat-price-chart')[0].style.cssText = 'display:block';
	});
	// //广告轮播图
	// var $slider = $('#J_slider_min'),
	//     $slider_box = $slider.find('ul'),
	//     $slider_item = $slider.find('li'),
	//     len = $slider_item.length - 1,
	//     step = $slider_item.width(),
	//     $trigger = $('#J_slider_trigger').find('a'),
	//     i = 0;
	// $slider_box.width($slider_item.outerWidth(true)*$slider_item.length);
	// $trigger.on('mouseover',function(){
	//     var $this = $(this),
	//         i = $trigger.index($this);
	//     move(i);
	// });
	// function move(i){
	//     $slider_box.stop(true,true).animate({'left':-i*step});
	//     $trigger.eq(i).addClass('cur').siblings('a').removeClass('cur');
	// }
	// function autoMove(){
	//     flag = setInterval(function(){
	//         if(i >= len){
	//             i = 0;
	//         }else{
	//             i++;
	//         }
	//         move(i)
	//     },4000);
	// }
	// autoMove();
	// $slider.hover(function(){
	//     clearInterval(flag);
	// },function(){
	//     autoMove();
	// });

    var searchFed = window.searchFed || {};

    //弹出框
    var searchFed = {
        ie6: typeof document.body.style.maxWidth == 'undefined',
        showDialog: function(content, title){
            var _title = title,
                _content = $(content),
                _html = '',
                doc_h = $(document).height(),
                win_w = $(window).width(),
                win_h = $(window).height(),
                scroll_top = $(window).scrollTop(),
                $dialog_mask = $('.dialog-mask'),
                _this = searchFed;
            if (!_content) return false;
            if (!_title.length) {
                _html += '<div class="dialog"><a href="javascript:void(0);" class="dialog-close" title="关闭">X</a><div class="dialog-body"></div></div>';
            } else {
                _html += '<div class="dialog"><h3>';
                _html += _title;
                _html += '</h3><a href="javascript:void(0);" class="dialog-close" title="关闭">X</a><div class="dialog-body"></div></div>';
            }
            //open the dialog
            if (!$dialog_mask.length) {
                $dialog_mask = $('<div class="dialog-mask"></div>').css('height', doc_h).appendTo('body');
                $(_html).appendTo('body');
                $('.dialog-body').append(_content);
            } else {
                $dialog_mask.show();
                $('.dialog').show();
            }
            if (_this.ie6) {
                $('.dialog').css('width', 560);
            }
            var $dialog = $('.dialog'),
                $dialog_close = $dialog.find('.dialog-close'),
                dialog_w = $dialog.outerWidth(),
                dialog_h = $dialog.outerHeight(),
                pos_x,
                pos_y;
            pos_x = parseInt((win_w - dialog_w) / 2);
            pos_y = parseInt((win_h + scroll_top - dialog_h) / 2);
            $dialog.css({
                top: pos_y,
                left: pos_x
            });

            var $fav_sel = $('.dialog-fav-sel');
            $fav_sel.on('click', function(e){
                e.stopPropagation();
                var $this = $(this),
                    $opts = $this.find('.dialog-fav-opt');
                $this.toggleClass('dialog-fav-sel-on');
                $opts.toggle();
                $opts.find('span').mouseenter(function(){
                    $(this).addClass('on');
                }).mouseleave(function(){
                    $(this).removeClass('on');
                }).click(function(){
                    $this.find('.fav-default').text($(this).text());
                });
                $(document).on('click',function(){
                    $opts.hide();
                });
            });

            $dialog_close.on('click', function(e){
                e.preventDefault();
                _this.hideDialog($dialog, $dialog_mask);
            });
        },
        hideDialog: function($dialog, $mask){
            if (!$dialog || !$mask) return false;
            $dialog.remove();
            $mask.remove();
        }
    };


    var dialogLogin = (function(){
    	var dialog = null,
    		loginUrl = $('.topbar-user-unlogin').find('a').eq(2).attr('href'),
    		registerUrl = $('.topbar-user-unlogin').find('a').eq(3).attr('href');

    	function Dialog(){
    		var html = '';
   				html = '<div class="dialog-login">';
			    html += '<div class="dialog-login-in">';
				html += '<h3>为了能及时更新您收藏的商品，需要登录网站</h3>';
				html += '<div class="login-register-mod">';
				html += '<div class="login-mod">';
				html += '<p>已有账号</p>';
				html += '<a href="' + loginUrl +'" class="dialog-btn btn-login">去登录</a>';
				html += '</div>';
				html += '<div class="register-mod">';
				html += '<p>暂无账号</p>';
				html += '<a href="' + registerUrl + '" class="dialog-btn btn-register">去注册</a>';
				html += '</div>';
				html += '</div>';
				html += '</div>';
				html += '<a href="#" class="dialog-login-close">x</a>';
				html += '</div>';
			return html;
		}

		return {
			getDialog:function(){
				return dialog || (dialog = $('body').append(Dialog()));
			}
		}
    })();



    //判断是否登录
    searchFed.isLogin = function(){
        var isLogin = Cookies.get('login') === 'true' && Cookies.get('token');
        return isLogin;
    }

    //显示登录框
    searchFed.showLoginDialog = function(){
		var dialog = dialogLogin.getDialog();

	    if(dialog){
	    	$('.dialog-login').show();
	    }
	    
	    $('.dialog-login-close').on('click',function(e){
	    	$('.dialog-login').hide();
	    	e.preventDefault();
	    })
    }

    //获得用户中心在不同环境下的url
    searchFed.getUcenterUrl = function(){

		var ucenterUrl = (function () {
			if (location.hostname.indexOf('stage.bang5mai.com') !== -1) {
				_domain = '.bang5mai.com';
				return 'ucenter.stage.bang5mai.com';
			}
			if (location.hostname.indexOf('prod.bang5mai.com') !== -1) {
				_domain = '.bang5mai.com';
				return 'ucenter.prod.bang5mai.com';
			}
			if (location.hostname.indexOf('ucenter.test.com') !== -1) {
				_domain = '.test.com';
				return 'ucenter.test.com';
			}
			return 'ucenter.b5m.com';
		})();

		return ucenterUrl;
    }

    //请求用户中心数据
    searchFed.getUcenterInfo = function(type){
        var userId = userId = Cookies.get('token');

        var currentUrl = $('#url').val() || window.location.href,
            price = $('#price').val() || $('#low-price-price').text();
            
       	var webSite = currentUrl.replace('http://',''),
			webSite = webSite.substring(webSite.indexOf(".")+1, webSite.indexOf("/"));
		
		var ucenterUrl = searchFed.getUcenterUrl();

    	$.ajax({
			url : 'http://' + ucenterUrl + '/gc/user/favorites/data/add.htm',
			type : 'POST',
			data : {
				userId : userId,
				goodsUrl : currentUrl,
				picUrl : $('.main-slider-pic>img').attr('src'),
				title : $('#productTitle').val(),
				webSite : webSite,
				price :price,
				source : 22
			},
			dataType : 'jsonp',
			jsonp : 'jsonpCallback',
			success : function(result) {
				if(type == 1){
					searchFed.getRecommendGoods(result);
				}else if(type == 2){
					var isSuccess = result.ok,
						status = result.code;
					if(!isSuccess && status != 40006){
						alert('添加失败')
					}else if(!isSuccess && status == 40006){
						//多次添加提示已经添加过此商品
						$('#J_count_down').removeClass('').text('已收藏该商品').addClass('btn-disabled').off('click');
					}else if(isSuccess){
						//提示添加降价提醒成功
						$('#J_count_down').removeClass('').text('已添加至我的收藏').addClass('btn-disabled').off('click');
					}
				}
			}
		})
    }

    //请求推荐商品
    searchFed.getRecommendGoods = function(result){

    	var docId = $('#docId').val(),
			title = $('#lastCategory').val(),
			pageSize = 5,
            resultStatus = '成功加入',
            nowStatus = result.message;

        if(!result.ok){
            if(nowStatus != resultStatus){
                resultStatus = nowStatus;
            };
        };

		$.ajax({
			url: '/api/shop/recommandProduces.htm',
			type: 'GET',
			dataType: 'jsonp',
			jsonp : 'jsonCallback',
			data: {docId:docId,title:title,pageSize:pageSize}
		})
		.done(function(data){
			var htmls = "";

			for(var i in data.val){
				var goodData = data.val[i],
					liHtml = '';

				liHtml = '<li>';
				liHtml += '<a href="' + goodData.shopList[0].Url + '">';
				liHtml += '<img width="100" height="100" src="' + goodData.shopList[0].Picture + '"/></a>';
				liHtml += '<p class="fav-recommend-title"><a href="' + goodData.shopList[0].Url + '">' + goodData.shopList[0].Title + '</a></p><p>&yen;<span>';
				liHtml += goodData.shopList[0].price.split('-')[0] + '</span></p></li>';
				htmls += liHtml;
			}

			searchFed.showDialog(
			    '<div class="fav-handle">' +
			        '<i class="fav-suc"></i>' +
			        '<span class="fav-status">' + resultStatus + '<a href="http://' + ucenterUrl +'/forward.htm?method=/user/account/favorites/index">收藏夹</a></span>' +
			        '</div>' +
			        '<div class="fav-recommend"><h6><a href="http://s.b5m.com/' + title + '.html">更多推荐</a>喜欢此宝贝的还喜欢</h6><ul class="clear-fix">' + htmls + '</ul></div>', ''
			)
			$('.dialog').data('flag',true);

			var timeFlag = setTimeout(function(){
				$('.dialog').remove();
				$('.dialog-mask').remove();
			},10000);
		})
    }

    searchFed.favIsEmpty = function(){

    	var ucenterUrl = searchFed.getUcenterUrl(),
    		userId = userId = Cookies.get('token');

    	$.ajax({
    		url: 'http://' + ucenterUrl + '/gc/user/favorites/data/query.htm',
    		type: 'GET',
    		dataType: 'jsonp',
    		jsonp:'jsonpCallback',
    		data: {userId:userId,priceType:0,pageSize:1}
    	})
    	.done(function(data) {
    		var isEmpty = data.data.page.total;

    		if(isEmpty <= 0){
    			var htmlArr = [];
    			
    			htmlArr.push('<div class="fav-empty-tips">');
				htmlArr.push('<p><strong>降价信息</strong> <span>在</span>账号管理 <span>></span> 我的收藏<span>中查看</span></p>');
				htmlArr.push('<a class="go-links" href="http://' + ucenterUrl + '/forward.htm?method=/user/account/favorites/index">立即点击查看</a>');
				htmlArr.push('<span class="fav-empty-close"></span>');
				htmlArr.push('</div>');

    			$('#J_count_down').before(htmlArr.join('')).fadeIn();

	    		function closeTips(){
	    			$('.fav-empty-tips').fadeOut();
	    		}

	    		$('.fav-empty-close').on('click',function(){
	    			closeTips();
	    		})
    		}

    		//添加商品收藏
    		searchFed.getUcenterInfo(2);
    	})
    	
    }
    //降价提醒
    $('#J_count_down').on('click',function(e){
    	e.preventDefault();

    	var isLogin = searchFed.isLogin();

    	if(!isLogin){
    		searchFed.showLoginDialog();
    	}else{
    		searchFed.favIsEmpty();
    	}
    });

   //  //添加收藏
   //  $('#J_add_favo').on('click',function(e){
   //      e.preventDefault();
   //      var isLogin = searchFed.isLogin();
   //      //是否登录
   //      if(!isLogin){
   //      	searchFed.showLoginDialog();
   //      }else{
			// searchFed.getUcenterInfo(1)
   //      }
   //  });


    var promotIndex = 0;
    $(document).on('click','.arrow-prod',function(e){
    	var $this = $(this),
    		$slider = $('#J_prod_slider'),
    		$firstChild = $slider.find('li:first'),
    		h = $firstChild.outerHeight(true);
            step = Math.ceil($slider.find('li').length / 4) - 1,
            $trigger = $('#J_trigger').find('.arrow-prod');


        if($this.hasClass('arrow-down') && (promotIndex < step)){
            promotIndex++;
            $firstChild.animate({marginTop:-h*promotIndex*4});
            $slider.find('li').find('img').each(function(){
                $(this).attr('src',$(this).attr('data-src'));
            })
        }else if($this.hasClass('arrow-up') && (promotIndex >= 1)){
            promotIndex--;
            $firstChild.animate({marginTop:-h*promotIndex*4});
        }


        if(promotIndex > 0 && promotIndex < step ){
            $trigger.removeClass('arrow-up-disable arrow-down-disable');
        }else if( promotIndex == 0){
            $trigger.eq(0).addClass('arrow-up-disable')
            $trigger.eq(1).removeClass('arrow-down-disable');
        }else if( promotIndex == step){
            $trigger.eq(0).removeClass('arrow-up-disable');
            $trigger.eq(1).addClass('arrow-down-disable');
        }
        e.preventDefault();
    });




    $.b5m.miniSlider = function(el,options){
        var base = this;
        base.$el = $(el);
        base.el = el;

        base.init = function(){
            var opts = $.extend({},$.b5m.miniSlider.defaultOptions,options || {});
            base.trigger = opts.trigger;

            base.bindEvent();
        }

        base.bindEvent = function(){
            var $container = base.$el,
                $slider = $container.find('ul'),
                $slider_item = $slider.find('li'),
                $slider_a = $slider.find('a'),
                $img = $slider_item.find('img'),
                item_w = $slider_item.outerWidth(true),
                step = $slider_item.length - 5,
                n = 0,
                $trigger = $container.find('.' + base.trigger),
                $targetImg = $container.siblings('.main-slider-pic').find('img'),

                $slider = $container.find('ul>li').eq(0);

            $img.on('mouseover',function(e){
                var src = this.src;
                
                $targetImg[0].src = src;
                $slider_a.removeClass('active');
                $(this).parent('a').addClass('active');
                e.preventDefault();
            });

            //若缩略图小于4个就隐藏左右箭头
            if(step <= 0){
                $trigger.hide();
                return false;
            }

            $trigger.eq(0).addClass('arrow-left-disable');

            $trigger.on('click',function(e){
                if($(this).hasClass('slider-right') && (n < step)){
                    n++;
                    $slider.animate({marginLeft:-item_w*n});
                }else if($(this).hasClass('slider-left') && (n >= 1)){
                    n--;
                    $slider.animate({marginLeft:-item_w*n});
                }

                if(n > 0 && n < step ){
                    $trigger.removeClass('arrow-left-disable arrow-right-disable');
                }else if( n == 0){
                    $trigger.eq(0).addClass('arrow-left-disable');
                    $trigger.eq(1).removeClass('arrow-right-disable')
                }else if( n == step){
                    $trigger.eq(1).addClass('arrow-right-disable');
                    $trigger.eq(0).removeClass('arrow-left-disable')
                }
                e.preventDefault();
            });
        }

    }

    $.b5m.miniSlider.defaultOptions = {
        trigger:'arrow'
    }

    $.fn.b5m_miniSlider = function(options){
        return this.each(function(){
            new $.b5m.miniSlider(this,options).init();
        })
    }


    $('.mini-slider').b5m_miniSlider({
        trigger:'slider-trigger'
    });    
});