var xltime = 40;//下拉时间全局定义
var JGPmoney = false;
var JGPmonth = false;
$(document).ready(function() {
    //点击空白处或者自身隐藏弹出层，下面分别为滑动和淡出效果。     
    $(document).click(function(event) {
        var yon = $('#xlk_ul').hasClass('xiala_no');
        if (yon == true) {
            $('#xlk_ul').hide(100);
        }
    });
});
function iptxlCK(obj) {//贷款金额 + 贷款期限 点击下拉
    var obj = $(obj);
    var xlcon = obj.parent().children('.wydk_xlk').children('.wydk_xiala');
    var bas = xlcon.hasClass('xiala_no');
    if (bas == true) {
        xlcon.removeClass('none');
        xlcon.removeClass('xiala_no');
    } else {
        xlcon.addClass('none');
        xlcon.addClass('xiala_no');
    }
}
function iptMouseOut(obj) {
    var obj = $(obj);
    var xlcon = obj.parent().children('.wydk_xlk').children('.wydk_xiala');
    xlcon.addClass('xiala_no');
    xlcon.addClass('none');
}
function xlMouseOver(obj) {
    var obj = $(obj);
    obj.removeClass('xiala_no');
    obj.removeClass('none');
}
function xlMouseOut(obj) {
    var obj = $(obj);
    obj.addClass('xiala_no');
    obj.addClass('none');
}
//function xdMouseover(obj) {
//    var obj = $(obj);
//    var xlcon = obj.parent().children('.wydk_xlk').children('.wydk_xiala');
//    var bas = xlcon.hasClass('xiala_no');
//    if (bas == true) {
//        xlcon.slideDown(xltime);
//        xlcon.removeClass('xiala_no');
//    } else {
//        xlcon.slideUp(xltime);
//        xlcon.addClass('xiala_no');
//    }
//}
function JGPageFoc(obj) {//鼠标 获取焦点 搜索输入框函数
    var obj = $(obj);
    var ts = obj.parent().parent().parent().children('.Tosearch_ts');
    ts.hide(200);//隐藏提示层
}
//搜索贷款金额提示框部分开始
function JGPageMoneyBlu() {//鼠标 离开焦点 搜索输入框函数---贷款金额
    JGPageMoneyYz();
}
function JGPageMoneyYz() {//验证 搜索输入框函数---贷款金额
    var obj = $('#money');
    var val = $.trim(obj.val());
    var ts = obj.parent().parent().parent().children('.Tosearch_ts');
    if (val.length == 0) {
        ts.show(300);
        ts.text('亲，金额别忘了填');
        JGPmoney = false;
    } else {
        if (!isNaN(val)) {
            if (val < 0 || val > 6000) {
                ts.show(300);
                ts.text('只限0~6000万');
                JGPmoney = false;
            } else {
                JGPmoney = true;
                var money = $(this).attr('reval', val);
            }
        } else {
            ts.show(300);
            ts.text('只填纯数字哟');
            JGPmoney = false;
        }
    }
}
function JGPageKeyup(obj) {//键盘按下事件
    var obj = $(obj);
    var val = $.trim(obj.val());
    var xlcon = obj.parent().parent().parent().children('.wydk_xlk').children('.wydk_xiala');
    xlcon.addClass('none');//隐藏下拉框-1
    xlcon.addClass('xiala_no');//隐藏下拉框-2
    var ts = obj.parent().parent().parent().children('.Tosearch_ts');
    ts.hide(500);//隐藏提示层
}
function JGPfirstpaySelect(tab) {//购车贷款首付 选取
    $('#wydk_FirstPay').unbind();
    //$('#xlk_ul').show();
    var tab = $(tab);
    var Li_a = $('#firstpay_ul li a');
    var this_reval = tab.attr('reval');//取得点击的reval值
    var this_html = tab.html();//取得点击的html值
    $('#wydk_FirstPay').html(this_html); //把点击的html值赋给显示的html值
    var sey_reval = $('#wydk_FirstPay').attr('reval');//定义显示的reval值
    $('#wydk_FirstPay').attr('reval', this_reval);//把点击的reval值赋给显示的reval值
    //stoptime();
    $('#wydk_FirstPay').text(this_html);
    $('#firstpay_ul').addClass('none');
    $('#firstpay_ul').addClass('xiala_no');
    Li_a.removeClass('mo');
    tab.addClass('mo');
    var ts = tab.parent().parent().parent().parent().children('.Tosearch_ts');
    ts.hide(200);//隐藏提示层
    
    var My = $.trim($('#money').val());
    var Mh = $.trim($('#month').val());
    if (My.length > 0 && Mh > 0 ) {
        PageSearch();
    }
}
function JGPmoneySelect(tab) {//贷款金额 选取
    $('#money').unbind();
    //$('#xlk_ul').show();
    var tab = $(tab);
    var Li_a = $('#xlk_ul li a');
    var this_reval = tab.attr('reval');//取得点击的reval值
    $('#money').val(this_reval); //把点击的reval值赋给显示的value值
    var sey_reval = $('#money').attr('reval');//定义显示的reval值
    $('#money').attr('reval', this_reval);//把点击的reval值赋给显示的reval值
    //stoptime();
    $('#xlk_ul').addClass('none');
    $('#xlk_ul').addClass('xiala_no');
    Li_a.removeClass('mo');
    tab.addClass('mo');
    var ts = tab.parent().parent().parent().parent().children('.Tosearch_ts');
    ts.hide(200);//隐藏提示层
    PageSearch();
}
//搜索贷款金额提示框部分结束

//搜索贷款期限提示框部分开始
function JGPageTimeBlu() {//鼠标 离开焦点 搜索输入框函数--贷款
    JGPageTimeYz();
}
function JGPageTimeYz() {//验证 搜索输入框函数---贷款期限
    var obj = $('#month');
    var val = $.trim(obj.val());
    var ts = obj.parent().parent().parent().children('.Tosearch_ts');
    var reval = obj.attr('reval');
    if (val.length == 0) {
        ts.show(300);
        ts.text('亲，期限别忘了填');
        JGPmonth = false;
    } else {
        if (!isNaN(val)) {
            //month --------------------------计算---start
            var time_dw = $('#time_dw').hasClass('geyue');
            if (time_dw == true) {
                //var month = $('#qixan').val();
                if (val <= 0 || val > 840) {
                    ts.show(300);
                    ts.text('只限0~840个月');
                    JGPmonth = false;
                } else {
                    JGPmonth = true;
                    var money = $(this).attr('reval', val);
                }
            } else {
                //var month_x = $('#qixan').val();
                //var month = month_x * 12;
                if (val <= 0 || val > 70) {
                    ts.show(300);
                    ts.text('只限0~70年');
                    JGPmonth = false;
                } else {
                    JGPmonth = true;
                    var money = $(this).attr('reval', val);
                }
            }
        } else {
            ts.show(300);
            ts.text('只填纯数字哟');
            JGPmonth = false;
        }
    }
}
function JGPmonthSelect(bba) {//贷款期限 选取
    var bba = $(bba);
    var Li_a = $('#qixan_ul li a');
    var this_s = bba.attr('s');
    $('#month').val(this_s);
    var sey_reval = $('#month').attr('reval');
    var This_reval = bba.attr('reval');
    $('#month').attr('reval', This_reval);
    var yon = bba.hasClass('dw_month');
    if (yon == true) {
        $('#time_dw').removeClass('nian');
        $('#time_dw').addClass('geyue');
    } else {
        $('#time_dw').removeClass('geyue');
        $('#time_dw').addClass('nian');
    }
    $('#qixan_ul').addClass('none');
    Li_a.removeClass('mo');
    bba.addClass('mo');
    $('#qixan_ul').addClass('xiala_no');
    var ts = bba.parent().parent().parent().parent().children('.Tosearch_ts');
    ts.hide(200);//隐藏提示层
}
//搜索贷款期限提示框部分结束
function PageSearch(jump) {
    JGPageMoneyYz();
    JGPageTimeYz();
    var type_id = $('#type_id').val();
    if (JGPmoney == true && JGPmonth == true) {
//        var type_id = 4;
        var money = $('#money').val();
        //month --------------------------计算---start
        var time_dw = $('#time_dw').hasClass('geyue');
        if (time_dw == true) {
            var month = $('#month').val();
        } else {
            var month_x = $('#month').val();
            var month = month_x * 12;
        }//alert(money);alert(month);
        var first_pay = $('#wydk_FirstPay').attr('reval');
        var interest = $('#interest').val();        //利息成本 默认0<input type="hidden" id="interest" value="0" />
        var give_time = $('#give_time').val();      //放款速度 默认0 <input type="hidden" id="give_time" value="0" />
        var bank_type = $('#bank_type').attr('reval');     //机构类型 默认9999 <input type="hidden" id="bank_type" value="9999" />
        //var fixed_url = money + 'x' + month + '-' + interest + 'x' + give_time + 'x' + bank_type;
        if (type_id == '2') {
            //var first_pay = $('#first_pay').attr('reval');
            fixed_url = money + 'x' + month + 'x' + first_pay + '-' + interest + 'x' + give_time + 'x' + bank_type;//购车计算
        } else {
            fixed_url = money + 'x' + month + '-' + interest + 'x' + give_time + 'x' + bank_type;
        }
        var site_url = $('#site_url_hidden').val();
        var url = site_url + '/s' + type_id + '-' + fixed_url + '/';
        if (jump == 'blank') {
            window.open(url, "_self");
        } else {
            window.open(url, "_self");
        }
        return false;
    }
}
