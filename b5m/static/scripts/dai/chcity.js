var x = $('.tipbox');
var ww = $(window).width();
var wh = $(window).height();
var cw = x.outerWidth();
var ch = x.outerHeight();
var cleft;
var ctop;
cleft = (ww - cw) / 2;
ctop = (wh - ch) / 2;
x.css({
    left: cleft, 
    top: ctop
});
$(document).ready(function() {
    YearMonthChange();//年月下拉框
    $(".detail_xdy_list .page a").live('click', function(e) {
        e.preventDefault();
        var self = $(this),
        url = self.attr('href'),
        parent = self.parent('.page');
        $.get(url, function(d) {
            if (d.status == 1) {
                $(".detail_xdy_list").html(d.data);
            } else {
                window.location.href = self.attr('href');
            }
        });
        return false;
    });
});
function YearMonthChange() { //年月下拉框 				
    var selectbtn = $('.wydk_ipt');
    selectbtn.mouseover(function() {// 点击弹出和隐藏
        var selectcon = $(this).parent().parent().children('.wydk_div').children('ul.wydk_li_selectcon'); //定义下拉层
        var yon = selectcon.hasClass('none');
        if (yon == true) {
            selectcon.removeClass('none');
        } else {
            selectcon.addClass('none');
        }
    });
    selectbtn.mouseout(function(){
        var selectcon = $(this).parent().parent().children('.wydk_div').children('ul.wydk_li_selectcon'); //定义下拉层
        selectcon.addClass('none');	
    });
    var select_con = $('.wydk_div');
    select_con.mouseover(function(){
        var selectcon = $(this).children('ul.wydk_li_selectcon'); //定义下拉层
        selectcon.removeClass('none');		  
    });
    select_con.mouseout(function(){
        var selectcon = $(this).children('ul.wydk_li_selectcon'); //定义下拉层
        selectcon.addClass('none');		  
    });
}
function YearSelect(obj) { //-------年份-------
    var obj = $(obj);
    var val = obj.html();
    var reval = obj.attr('reval');
    $('#wydk_money').attr('reval', reval);
    $('#wydk_money').text(val);
    var select_con = $('#wydk_money_ul');
    select_con.children('li').children('a').removeClass('mo');
    $(this).addClass('mo');
    select_con.addClass('none'); //隐藏下拉层ver
}
function MonthSelect(obj) { //-------月份-------
    var obj = $(obj);
    var val = obj.html();
    var reval = obj.attr('reval');
    $('#wydk_month').attr('reval', reval);
    $('#wydk_month').text(val);
    var select_con = $('#wydk_month_ul');
    select_con.children('li').children('a').removeClass('mo');
    $(this).addClass('mo');
    select_con.addClass('none'); //隐藏下拉层ver
}
function ProvSelect(obj) { //-------省份-------
    var obj = $(obj);
    var val = obj.html();
    var reval = obj.attr('reval');
    $('#wydk_money').attr('reval', reval);
    $('#wydk_money').text(val);
    var select_con = $('#wydk_money_ul');
    select_con.children('li').children('a').removeClass('mo');
    $(this).addClass('mo');
    select_con.addClass('none'); //隐藏下拉层ver
    var reval=obj.attr('reval');
    $('.zonec').hide();
    var nowcity = $('.zonearr_'+reval).eq(0);
    nowcityname = nowcity.text();
    nowreval = $(nowcity).find('a').attr('reval');
    //alert($(nowcity).find('a').attr('reval'));
    $('#wydk_month').attr('reval', nowreval);
    $('#zone_v').val(nowreval);
    $('#wydk_month').text(nowcityname);
    $('.zonearr_'+reval).show()
}
function CitySelect(obj) { //-------城市-------
    var obj = $(obj);
    var val = obj.html();
    var reval = obj.attr('reval');
    $('#wydk_month').attr('reval', reval);
    $('#wydk_month').text(val);
    var select_con = $('#wydk_month_ul');
    select_con.children('li').children('a').removeClass('mo');
    $(this).addClass('mo');
    select_con.addClass('none'); //隐藏下拉层ver
    var reval=obj.attr('reval');
    $('#zone_v').val(reval);
}
function tjCity(){
    var v=$('#zone_v').val();
    //var url=$('#zone_v').attr('url');
    //if(v!=''&& v!=undefined){
    if(v!=''&& v!=undefined){
          window.location.href='http://daikuan.b5m.com/city/'+v+'.html';
    }else{
        alert('请选择城市！');
    }
  
}