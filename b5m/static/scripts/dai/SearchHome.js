// JavaScript Document
var Smoney = false;
var Smonth = false;
//--------------用户中心部分-------------------
var user_nicheng = false;
$(document).ready(function() {
    wydk();  //我要贷款
    $('#wydk_money').focus(function() {
        $('#wydk_money').val('');
    });
    $('#wydk_month').focus(function() {
        $('#wydk_month').val('');
    });
    $('#wydk_money').keydown(function() {
        //$('#wydk_money_ul').hide();
        $('#wydk_money_ul').addClass('none');
    });
    $('#wydk_month').keydown(function() {
        //$('#wydk_month_ul').hide();
        $('#wydk_month_ul').addClass('none');
    });
});
function wydk() { //我要贷款 				
    var selectbtn = $('.wydk_li_selectbtn');
    selectbtn.click(function() {// 点击弹出和隐藏
        var selectcon = $(this).parent().children('.wydk_div').children('ul.wydk_li_selectcon'); //定义下拉层
        var yon = selectcon.hasClass('none');
        if (yon == true) {
            selectcon.removeClass('none');
        } else {
            selectcon.addClass('none');
        }
    });
    var select_con = $('.wydk_div');
    select_con.mouseover(function() {
        var selectcon = $(this).children('ul.wydk_li_selectcon'); //定义下拉层
        selectcon.removeClass('none');
    });
    select_con.mouseout(function() {
        var selectcon = $(this).children('ul.wydk_li_selectcon'); //定义下拉层
        selectcon.addClass('none');
    });
}
function dktypeSelect(obj) {
    var obj = $(obj);
    var val = obj.html();
    var reval = obj.attr('reval');
    $('#wydk_type').attr('reval', reval);
    $('#wydk_type').text(val);
    var select_con = $('#wydk_type_ul');
    select_con.children('li').children('a').removeClass('mo');
    obj.addClass('mo');
    select_con.addClass('none'); //隐藏下拉层ver
    //根据贷款类型，变化贷款金额和贷款期限---start
    var select_name = obj.attr('name');//当前点击的name值
    if (select_name == 'type_qiye') {//-------------------------------------------------------企业贷款------------------------------------
        //alert('ddd');
        var Smoney = $('#wydk_money'); //贷款金额---显示块
        Smoney.attr('reval', 50); //给贷款金额---显示块赋reval值50
        Smoney.val('50'); //给贷款金额---显示赋text内容
        var Hmoney_ul = $('#wydk_money_qiye').html(); //贷款金额---隐藏列表ul
        $('#wydk_money_ul').html(Hmoney_ul);// 给下拉列表赋新的html 代码

        var Stime = $('#wydk_month'); //贷款期限---显示块
        Stime.attr('reval', 12); //给贷款期限---显示块赋reval值12
        Stime.val('12'); //给贷款期限---显示赋text内容
        var Hmonth_ul = $('#wydk_month_qiye').html(); //贷款期限---隐藏列表ul
        $('#wydk_month_ul').html(Hmonth_ul);// 给下拉列表赋新的html 代码

    } else if (select_name == 'type_gouche') {//-------------------------------------------------------购车贷款------------------------------------
        var Smoney = $('#wydk_money'); //贷款金额---显示块
        var Hmoney_ul = $('#wydk_money_ul'); //贷款金额---隐藏列表ul
        Smoney.attr('reval', 15); //给贷款金额---显示块赋reval值50
        Smoney.val('15'); //给贷款金额---显示赋text内容
        var Hmoney_ul = $('#wydk_money_gouche').html(); //贷款金额---隐藏列表ul
        $('#wydk_money_ul').html(Hmoney_ul);// 给下拉列表赋新的html 代码

        var Stime = $('#wydk_month'); //贷款期限---显示块
        var Htime_ul = $('#wydk_month_ul'); //贷款期限---隐藏列表u
        Stime.attr('reval', 24); //给贷款期限---显示块赋reval值12
        Stime.val('24'); //给贷款期限---显示赋text内容
        var Hmonth_ul = $('#wydk_month_gouche').html(); //贷款期限---隐藏列表ul
        $('#wydk_month_ul').html(Hmonth_ul);// 给下拉列表赋新的html 代码

    } else if (select_name == 'type_goufang') {//-------------------------------------------------------购房贷款------------------------------------
        var Smoney = $('#wydk_money'); //贷款金额---显示块
        var Hmoney_ul = $('#wydk_money_ul'); //贷款金额---隐藏列表ul
        Smoney.attr('reval', 100); //给贷款金额---显示块赋reval值50
        Smoney.val('100'); //给贷款金额---显示赋text内容
        var Hmoney_ul = $('#wydk_money_goufang').html(); //贷款金额---隐藏列表ul
        $('#wydk_money_ul').html(Hmoney_ul);// 给下拉列表赋新的html 代码

        var Stime = $('#wydk_month'); //贷款期限---显示块
        var Htime_ul = $('#wydk_month_ul'); //贷款期限---隐藏列表u
        Stime.attr('reval', 240); //给贷款期限---显示块赋reval值12
        Stime.val('240'); //给贷款期限---显示赋text内容
        var Hmonth_ul = $('#wydk_month_goufang').html(); //贷款期限---隐藏列表ul
        $('#wydk_month_ul').html(Hmonth_ul);// 给下拉列表赋新的html 代码

    } else if (select_name == 'type_xiaofei') {//-------------------------------------------------------消费贷款 or 不限------------------------------------
        var Smoney = $('#wydk_money'); //贷款金额---显示块
        var Hmoney_ul = $('#wydk_money_ul'); //贷款金额---隐藏列表ul
        Smoney.attr('reval', 10); //给贷款金额---显示块赋reval值50
        Smoney.val('10'); //给贷款金额---显示赋text内容
        var Hmoney_ul = $('#wydk_money_xiaofei').html(); //贷款金额---隐藏列表ul
        $('#wydk_money_ul').html(Hmoney_ul);// 给下拉列表赋新的html 代码

        var Stime = $('#wydk_month'); //贷款期限---显示块
        var Htime_ul = $('#wydk_month_ul'); //贷款期限---隐藏列表u
        Stime.attr('reval', 12); //给贷款期限---显示块赋reval值12
        Stime.val('12'); //给贷款期限---显示赋text内容
        var Hmonth_ul = $('#wydk_month_xiaofei').html(); //贷款期限---隐藏列表ul
        $('#wydk_month_ul').html(Hmonth_ul);// 给下拉列表赋新的html 代码
    }//根据贷款类型，变化贷款金额和贷款期限---over
}
function moneySelect(obj) { //-------贷款金额------
    var obj = $(obj);
    var val = obj.val();
    var reval = obj.attr('reval');
    $('#wydk_money').attr('reval', reval);
    $('#wydk_money').val(reval);
    var select_con = $('#wydk_money_ul');
    select_con.children('li').children('a').removeClass('mo');
    $(this).addClass('mo');
    select_con.addClass('none'); //隐藏下拉层ver
//    $('#wydk_money').blur();
}
function monthSelect(obj) { //-------贷款期限-------
    var obj = $(obj);
    var this_s = obj.attr('s');
    $('#wydk_month').val(this_s);
    var reval = obj.attr('reval');
    $('#wydk_month').attr('reval', reval);
    var select_con = $('#wydk_month_ul');
    select_con.children('li').children('a').removeClass('mo');
    $(this).addClass('mo');
    select_con.addClass('none'); //隐藏下拉层ver
    //换单位
    var yonM = obj.hasClass('dw_month');
    if(yonM == true){
        $('#wydk_month').removeClass('wydk_month3ho');
        $('#wydk_month').removeClass('wydk_month3');
        $('#wydk_month').removeClass('wydk_month1');
        $('#wydk_month').addClass('wydk_month2');
        $('#wydk_month').attr('dw', 0);
    }else{
        $('#wydk_month').removeClass('wydk_month1');
        $('#wydk_month').removeClass('wydk_month2');
        $('#wydk_month').removeClass('wydk_month3ho');
        $('#wydk_month').addClass('wydk_month3');
        $('#wydk_month').attr('dw', 1);
    }
}
function FirstPaySelect(obj) {//首付
    var obj = $(obj);
    var val = obj.html();
    var reval = obj.attr('reval');
    $('#wydk_FirstPay').attr('reval', reval);
    $('#wydk_FirstPay').text(val);
    var select_con = $('#wydk_FirstPay_ul');
    select_con.children('li').children('a').removeClass('mo');
    obj.addClass('mo');
    select_con.addClass('none'); //隐藏下拉层ver
}
function MoneyFoc() { //贷款金额 获取焦点
    var val = $.trim($('#wydk_money').val());
    var ts = $('#MoneyTs');
    ts.text('不足一万可用小数点表示');
    ts.css('color', '#f4f4f4');
    $('#wydk_money').addClass('wydk_money1');
    $('#wydk_money').removeClass('wydk_money2');
}
function MoneyBluColor() { //贷款金额----换单位颜色
    var val = $.trim($('#wydk_money').val());
    var ts = $('#MoneyTs');
    $('#wydk_money').removeClass('wydk_money1');
    $('#wydk_money').addClass('wydk_money2');
    //$('#wydk_money_ul').addClass('none');
}
function MoneyBlu() { //贷款金额----验证
    var val = $.trim($('#wydk_money').val());
    var ts = $('#MoneyTs');
    $('#wydk_money').removeClass('wydk_money1');
    $('#wydk_money').addClass('wydk_money2');
    if (val.length == 0) {
        ts.text('贷款金额不能为空');
        ts.css('color', 'yellow');
        Smoney = false;
    } else {
        if (!isNaN(val)) {
            Smoney = true;
            var money = $('#wydk_money').attr('reval', val);
        } else {
            ts.text('贷款金额请填纯数字');
            ts.css('color', 'yellow');
            Smoney = false;
        }
    }
}
function MonthFoc() { //贷款期限 获取焦点
    var val = $.trim($('#wydk_month').val());
    var ts = $('#MonthTs');
    var dw = $('#wydk_month').attr('dw');
    if (dw == 0) {
        $('#wydk_month').addClass('wydk_month1');
        $('#wydk_month').removeClass('wydk_month2');
        ts.text('');
    } else if (dw == 1) {
        $('#wydk_month').addClass('wydk_month3ho');
        $('#wydk_month').removeClass('wydk_month3');
        ts.text('');
    }
}
function MonthBluColor() { //贷款期限----换单位颜色
    var val = $.trim($('#wydk_month').val());
    var ts = $('#MonthTs');
    var dw = $('#wydk_month').attr('dw');
    if (dw == 0) {
        $('#wydk_month').removeClass('wydk_month1');
        $('#wydk_month').addClass('wydk_month2');
        ts.text('');
    } else if (dw == 1) {
        $('#wydk_month').removeClass('wydk_month3ho');
        $('#wydk_month').addClass('wydk_month3');
        ts.text('');
    }
}
function MonthBlu() { //贷款期限----验证
    var val = $.trim($('#wydk_month').val());
    var ts = $('#MonthTs');
    $('#wydk_month').removeClass('wydk_month1');
    $('#wydk_month').addClass('wydk_month2');
    if (val.length == 0) {
        ts.text('贷款期限不能为空');
        ts.css('color', 'yellow');
        Smonth = false
    } else {
        if (!isNaN(val)) {
            Smonth = true;
            var money = $('#wydk_month').attr('reval', val);
        } else {
            ts.text('贷款期限请填纯数字');
            ts.css('color', 'yellow');
            Smonth = false;
        }
    }
}
function wydk_search(jump) { //我要贷款搜索按钮
    MoneyBlu();
    MonthBlu();
    if (Smoney == true && Smonth == true) {
        var type_id = $('#wydk_type').attr('reval');//贷款类型
        var money = $('#wydk_money').attr('reval');//贷款金额
        //贷款期限 --------------------------计算---start
        var time_dw = $('#wydk_month').attr('dw');
        if (time_dw == 0) {
            var month = $('#wydk_month').val();
        }else if(time_dw == 1) {
            var month_x = $('#wydk_month').val();
            var month = month_x * 12;
        }
        //贷款期限 --------------------------计算---over
        var first_pay = $('#wydk_FirstPay').attr('reval');//首付
        var interest = 0;
        var give_time = 0;
        var bank_type = 9999;
        //var fixed_url = money + '-' + month + '-' + interest + 'x' + give_time + 'x' + bank_type;
        var fixed_url = money + '-' + month + '-' + '1';
        if (type_id == '2') {
            //var first_pay = $('#first_pay').attr('reval');
            fixed_url = money + '-' + month + '-' + first_pay + '-' + '1';//购车计算
        } else {
            fixed_url = money + '-' + month + '-' + '1';
        }

        var type_id_str = '';
        switch (parseInt(type_id)) {
            case 1:
                type_id_str = 'qiye';
                break;
            case 2:
                type_id_str = 'gouche';
                break;
            case 3:
                type_id_str = 'goufang';
                break;
            case 4:
                type_id_str = 'xiaofei';
                break;
            default:
                return;
        }
        var def_site = 'http://daikuan.b5m.com/' + Cookies.get('city');
        var url = def_site + '/' + type_id_str + '-' + fixed_url + '/';
        // var url = 'http://www.haodai.com/s' + type_id + '-' + fixed_url + '/';
        if (jump == 'blank') {
            window.open(url);
        } else {
            window.open(url, "_self");
        }
        return false;
    }
}
function clearNoNum(obj) {  //只能输入数字和小数
    obj.value = obj.value.replace(/[^\d.]/g, "");  //清除“数字”和“.”以外的字符   
    obj.value = obj.value.replace(/^\./g, "");  //验证第一个字符是数字而不是.   
    obj.value = obj.value.replace(/\.{2,}/g, "."); //只保留第一个. 清除多余的.   
    obj.value = obj.value.replace(".", "$#$").replace(/\./g, "").replace("$#$", ".");
}