
var q_name = false;
var q_phone = false;
var q_money = false;
$(document).ready(function() {
    init();
    tabChange();    //调用切换tab选项卡函数
    quityz();   //快速验证
//    tabChange2();
});
function init() {
    
}
function tabChange() { //切换tab选项卡函数
    var tbli = $('ul.tabnav li');
    //tbli.parent().find('li.tbnon').css('border-left', '0px');
    tbli.click(function() {
        var yon = $(this).hasClass('tbnon'); //查看点击的li是否包含名为“tbnon”的class
        var tn = $(this).index();  //确定该li所在全部li中的位置序号
        if (tn == 0) {
            $(this).css('border-left', '0px');
        }
        if (yon == false) { //判断，如果点击的li不包含"tbnon" 则执行函数
            //导航区
            var del_li = $(this).parent().find('li');   //获取同级LI
            del_li.removeClass('tbnon');  //删除li中的"tbnon"的 class --执行范围全体Li
            $(this).addClass('tbnon');  //为当前li添加“tbnon”的 class
            var li_count = 0;
            del_li.each(function(index) {   //获取当前点击同级的LI数量
                li_count = (index + 1);
            });
            var temp_tab = $(this).parent().parent();
            for (var i = 1; i <= li_count; i++) {
                temp_tab.find('.tab' + i).addClass('none');     //循环逐个增加 none属性
            }
            //内容区
            $(this).parent().parent().find('.tab' + (tn + 1)).removeClass('none');
        }
    });
}
////手机号验证
//function checkMobile(s) {   //判断手机号码
//	var regu = /^(13[0-9]|15[0-9]|18[0-9])\d{8}$/; 
//	var re = new RegExp(regu);
//	if (re.test(s)) {
//		return true;
//	}
//	else {
//		return false;
//	}
//}
function quityz() {
    //您的姓名
    var q_name = $('#user_name');
    var q_name_val = $.trim(q_name.val());
    q_name.focus(function() {
        if (q_name_val == '如张先生') {
         q_name.val('');  
        }
        q_name.css('color', '#333');
    });
    q_name.blur(function() {
        q_name_val = $.trim(q_name.val());
        if (q_name_val == '') {
            q_name.val('如张先生');
            q_name.css('color', '#bbb');
        }
    });
    //手机号码
    var q_phone = $('#user_mobilenum');
    var q_phone_val = $.trim(q_phone.val());
    q_phone.focus(function() {
        if (q_phone_val == '您的手机号码') {
         q_phone.val('');  
        }
        q_phone.css('color', '#333');
    });
    q_phone.blur(function() {
        q_phone_val = $.trim(q_phone.val());
        if (q_phone_val == '') {
            q_phone.val('您的手机号码');
            q_phone.css('color', '#bbb');
        }
    });
    //申贷金额
    var q_money = $('#user_money');
    var q_money_val = $.trim(q_money.val());
    q_money.focus(function() {
        if (q_money_val == '请输入数字') {
        q_money.val('');
        }
        q_money.css('color', '#333');
    });
    q_money.blur(function() {
        q_money_val = $.trim(q_money.val());
        if (q_money_val == '') {
            q_money.val('请输入数字');
            q_money.css('color', '#bbb');
        }
    });
}
//点击申请贷款
//function sq_index(t) {
//    //------- 3秒后才能继续点击 -----
//    var i=3;           
//    t.disabled=true; 
//    var timer=setInterval(function(){
//        t.value = i + '秒后,继续申请';
//        i--;
//        if(i<0){
//            t.disabled=false;
//            i=3;
//            t.value="申请贷款";
//            clearInterval(timer);
//        }
//    },3000);         
//    //------- 3秒后才能继续点击 -----
//    
//    var q_name = $('#user_name');
//    var q_phone = $('#user_mobilenum');
//    var q_money = $('#user_money');
//    var q_name_val = $.trim(q_name.val());
//    var q_phone_val = $.trim(q_phone.val());
//    var q_money_val = $.trim(q_money.val());
//    //姓名验证
//    var reg = /^[u4E00-u9FA5]+$/;
//    if (q_name_val.length > 0 && !reg.test(q_name_val)) {
//        if(q_name_val == '如张先生'){
//            q_name.val('不能为空');
//            q_name.css('color', 'red');
//            q_name = false;
//        }else {
//            q_name.css('color', 'green');
//            q_name = true;
//        }
//    } else{
//        q_name.val('请输入真实的中文名');
//        q_name.css('color', 'red');
//        q_name = false;
//    }
//    //手机号验证
//    if(checkMobile(q_phone_val)){
//        q_phone.css('color', 'green');
//        q_phone = true;
//    }else {
//        q_phone.val('请输入正确的手机号');
//        q_phone.css('color', 'red');
//        q_phone = false;
//        
//    }
//    //贷款金额验证
//    if(!isNaN(q_money_val)){
//        q_money.css('color', 'green');
//        q_money = true;
//    }else{
//        q_money.val('请输入纯数字');
//        q_money.css('color', 'red');
//        q_money = false;
//    }   
//    
//    fastApply();
//}
//
//function fastApply(){   //快速申请贷款
//    var apply_box = $('#applySuccess').html();
//    var car_id = $.colorbox({
//        html:apply_box,
//        opacity:0.3,
//        transition:'fade',
//        overlayClose:false, //为true单击遮罩层就可以把ColorBox关闭
//        initialWidth:0,
//        initialHeight:0,
//        slideshowAuto:true
//    //        close:'/src/j/tipbox/images/cha.png'
//    });
//    setTimeout("$.fn.colorbox.close()", 4000);    //4秒后自动关闭
//}