var g_time;
$(document).ready(function() {
    yesno();
    hidetip();
});
(function($) {
    $("#cboxLoadedContent #xdvcode_img").live('click', function(e) {             //刷新校验码
        g_time = new Date();
        $(this).attr('src', '/Captcha/verify?who=0&t=' + g_time);
    });
})(jQuery);
function juzhong() {            //居中
    if (jQuery.browser.msie && (jQuery.browser.version == "6.0") && !jQuery.support.style) {
        $('.tipbox').css({});
    } else {
        $('.tipbox').css({
            top: (($(window).height() - $('.tipbox').outerHeight()) / 2),
            left: (($(window).width() - $(".tipbox").outerWidth()) / 2)
        });
    }
}
function hideTS() {
    $('.tishi').hide(); //隐藏所有提示框
}
function hideboxTS() {
    $('.tishi2').hide(); //隐藏所有提示框
}
function inpFocus(id_name) {
    var dom = $('#' + id_name + '_ul');
    var status = dom.hasClass('none');
    if (status == true) {       //被隐藏
        dom.removeClass('none');
    } else {
        dom.addClass('none');
    }
}
function inpSelect(id_name, obj) {
    var dom = $('#' + id_name + '_inp');
    var val = $(obj).html();
    var type_name = dom.attr('val');
    dom.val(val); //设置显示值
    var relval = type_name + ':' + val;
    dom.attr('relval', relval); //设置真正值
    $('#' + id_name + '_inp_hidden').val(relval); //隐藏 用于提交
}
function yearSelect(obj) {  //出生年份选择框
    var obj = $(obj);
    var val = obj.html();
    var reval = obj.attr('reval');
    if (val == '其他') {
        $('#year_born').attr('reval', '');
        $('#year_born').val('');
        $('#year_born').attr('class', 'sinp iptsr');
        $('#year_born').focus();
    } else {
        $('#year_born').attr('reval', reval);
        $('#year_born').val(reval);
        $('#year_born').attr('class', 'sinp iptsr');
    }
    var year_u = $('#year_u');
    var status = year_u.css('display');
    if (status == 'none') {       //被隐藏
        $('#year_u').show();
    } else {
        $('#year_u').hide();
    }
}

function applyNext(next_name) {   //下一步
    car_id.colorbox.resize();
    var xd_type = $('#xd_type').val();
    bool = true;
    if (next_name == 'xiaofei_two') {
        var bool = checkOneForm(); //验证成功  验证消费第一步
        yesno();
        hidetip();
        if (bool == true) {
            $('#one').hide();
            $('#xiaofei_two').show();
            car_id.colorbox.resize();
        }
    } else if (next_name == 'two') {
        if (xd_type != 4) {    //消费贷款
            var bool = checkOneForm(); //验证成功   验证其他第二步
        } else {
            var bool = xfCheckTwoForm(); //验证成功   验证消费第二步
            car_id.colorbox.resize();
        }
        if (bool == true) {
            if ($('#xiaofei_two').length > 0) {     //消费贷款
                $('#xiaofei_two').hide();
                car_id.colorbox.resize();
                $('.tboxtop').html('<div class="tboxa bac3">' +
                        '<span class="tbsp1 co3">1.填写个人职业信息</span>' +
                        '<span class = "tbsp2 co3" > 2.填写个人信用信息 </span>' +
                        '<span class = "tbsp3 cow" > 3.留下联系方式 </span>' +
                        '<span class = "tbsp4 co3" > 4.成功了！ </span></div>');
            }
            $('#one').hide();
            $('#two').show();
            car_id.colorbox.resize();
        }
    }
    else if (next_name == 'three') {
        var bool = checkTwoForm(); //验证成功
        if (bool == true) {
            $('#two').hide();
            car_id.colorbox.resize();
            if ($('#xiaofei_two').length > 0) {
                $('.tboxtop').html('<div class="tboxa bac4"><span class="tbsp1 co3">1.填写个人职业信息</span>' +
                        '<span class = "tbsp2 co3" > 2.填写个人信用信息 </span>' +
                        '<span class = "tbsp3 co3" > 3.留下联系方式 </span>' +
                        '<span class = "tbsp4 cow" > 4.成功了！ </span></div>');
            }
            $('#three').show();
            car_id.colorbox.resize();
            applySend(); //发送，联系信贷员 
        }
    }
    car_id.colorbox.resize();
//    juzhong();  //每次执行居中
}
//成功返回 true 失败 false

function checkOneForm() {   //验证第一步表单
    var xd_type = $('#xd_type').val();
    var bool = true;
    if (xd_type == 1) { //------------------------------------------- 企业 -------------------------------------------
        var qiye_type = $('#qiye_type_inp_hidden').val(); //公司类型
        if (qiye_type == '') {                        //您的公司类型
            var dom = $('#qiye_typeTip').show();
            bool = false;
        }
        //-----------start-----对公加对私月流水
        var monthly = $('#monthly').val();
        if (monthly == $('#monthly').attr('myplace')) {
            monthly = '';
        }
        if (monthly == '') {             //对公加对私月流水（万元）
            var dom = $('#monthlyTip').show();
            bool = false;
        }
        //-----------over-----
        //-----------start-----经营年限
        var BusinessTime = $('#BusinessTime').attr('reval');
//        if (BusinessTime == $('#BusinessTime').attr('myplace')) {
//            BusinessTime = '';
//        }
        if (BusinessTime == '') {             //经营年限（年）
            var dom = $('#BusinessTimeTip').show();
            bool = false;
        }
        //-----------over-----
        var has_house = $('input[name="has_house"]:checked').val(); //是否有本地商品房
        if (typeof (has_house) == 'undefined') {
            var dom = $('#has_houseTip').show();
            bool = false;
        }
//        var register = $('input[name="register"]:checked').val(); //户籍所在地
//        if (typeof (register) == 'undefined') {
//            var dom = $('#registerTip').show();
//            bool = false;
//        }
    } else if (xd_type == 2) { //------------------------------------------- 购车 -------------------------------------------
        var has_house = $('input[name="has_house"]:checked').val(); //是否有本地商品房
        if (typeof (has_house) == 'undefined') {
            var dom = $('#has_houseTip').show();
            bool = false;
        }
//        var car_number = $('input[name="car_number"]:checked').val(); //车辆牌照
//        if (typeof (car_number) == 'undefined') {
//            var dom = $('#car_numberTip').show();
//            bool = false;
//        }
//        var car_type = $('input[name="car_type"]:checked').val(); //购车类型
//        if (typeof (car_type) == 'undefined') {
//            var dom = $('#car_typeTip').show();
//            bool = false;
//        }
        var salary = $('#salary_inp_hidden').val(); //每月打入银行卡的工资
        var wage = $.trim($('#salary_inp').val());
        if (salary == '') {
            var dom = $('#salaryTip').show();
            bool = false;
        } else {
            if (!isNaN(salary)) {
                if (salary > 1000000) {
                    var dom = $('#salaryTip').show();
                    $('#salaryTip').text('限100万内');
                    bool = false;
                } else {
                    var dom = $('#salaryTip').hide();
//                    bool = true;
                }
            } else {
                var dom = $('#salaryTip').show();
                $('#salaryTip').text('输入纯数字');
                bool = false;
            }
        }
        var car_stage = $('input[name="car_stage"]:checked').val(); //购车阶段
        if (typeof (car_stage) == 'undefined') {
            var dom = $('#car_stageTip').show();
            bool = false;
        }
//        var car_use = $('input[name="car_use"]:checked').val(); //车辆用途
//        if (typeof (car_use) == 'undefined') {
//            var dom = $('#car_useTip').show();
//            bool = false;
//        }
    } else if (xd_type == 3) { //------------------------------------------- 购房 -------------------------------------------
        var goufang_type = $('#goufang_type_inp_hidden').val(); //房屋类型
        if (goufang_type == '') {
            var dom = $('#goufang_typeTip').show();
            bool = false;
        }
        var salary = $('#salary_inp_hidden').val(); //每月打入银行卡的工资
        var wage = $.trim($('#salary_inp').val());
        if (salary == '') {
            var dom = $('#salaryTip').show();
            bool = false;
        } else {
            if (!isNaN(salary)) {
                if (salary > 1000000) {
                    var dom = $('#salaryTip').show();
                    $('#salaryTip').text('限100万内');
                    bool = false;
                } else {
                    var dom = $('#salaryTip').hide();
                    //bool = true;
                }
            } else {
                var dom = $('#salaryTip').show();
                $('#salaryTip').text('输入纯数字');
                bool = false;
            }
        }
//        var first_house = $('input[name="first_house"]:checked').val(); //是否首套房
//        if (typeof (first_house) == 'undefined') {
//            var dom = $('#first_houseTip').show();
//            bool = false;
//        }
        var household = $('input[name="household"]:checked').val(); //户籍
        if (typeof (household) == 'undefined') {
            var dom = $('#householdTip').show();
            bool = false;
        }
        var id_name = 'second_hand_house'; //是否二手房
        var second_hand_house = $('input[name="' + id_name + '"]:checked').val();
        if (typeof (second_hand_house) == 'undefined') {
            var dom = $('#' + id_name + 'Tip').show();
            bool = false;
        }
    } else if (xd_type == 4) { //------------------------------------------- 消费 -------------------------------------------   
        bool = xfCheckOneForm(bool);    //消费贷款第一步验证
    }
    return bool;
}
function yesno() {
    var tab_yes = $('.ipttab_yes');
    var tab_no = $('.ipttab_no');
    tab_yes.click(function() {
        var tab_con = $(this).parent().parent().parent().parent().children('.xinpu');
        $(this).parent().parent().children('.tishi').hide();
        tab_con.show();
        xf_juzhong();
        car_id.colorbox.resize();
        placeholderBox();   //提示语绑定

    });
    tab_no.click(function() {
        var tab_con = $(this).parent().parent().parent().parent().children('.xinpu');
        $(this).parent().parent().children('.tishi').hide();
        tab_con.hide();
        xf_juzhong();
        car_id.colorbox.resize();
        placeholderBox();   //提示语绑定
    });
    var tab_yes2 = $('.ipttab_yes2');
    var tab_no2 = $('.ipttab_no2');
    tab_yes2.click(function() {
        $(this).parent().parent().children('.tishi').hide();
        xf_juzhong();
        car_id.colorbox.resize();
        placeholderBox();   //提示语绑定
    });
    tab_no2.click(function() {
        $(this).parent().parent().children('.tishi').hide();
        xf_juzhong();
        car_id.colorbox.resize();
        placeholderBox();   //提示语绑定
    });
}
function xf_juzhong() {            //居中
    if (jQuery.browser.msie && (jQuery.browser.version == "6.0") && !jQuery.support.style) {
        $('.tipbox').css({});
    } else {
        $('.tipbox').css({
            top: (($(window).height() - $('.tipbox').outerHeight()) / 2),
            left: ($(window).width() - $(".tipbox").outerWidth()) / 2
        });
    }
}

function hidetip() {
    var tip = $('.tishi');
    var ipt = $('.sinp');
    ipt.click(function() {
        $(this).parent().parent().children('.tishi').hide();
    });
}
function xfCheckOneForm(bool) { //消费贷款第一步验证
    var id_name = 'salary';
    var salary = $('#salary_inp_hidden').val(); //每月打入银行卡的工资 
    var wage = $.trim($('#salary_inp').val());
    if (salary == $('#salary_inp_hidden').attr('myplace')) {    //salary值与提示语相同，判定为空
        salary = '';
    }
    if (salary == '') {
        var dom = $('#' + id_name + 'Tip').show();
        bool = false;
    } else {
        if (!isNaN(salary)) {
            if (salary > 1000000) {
                var dom = $('#salaryTip').show();
                $('#salaryTip').text('限100万内');
                bool = false;
            } else {
                var dom = $('#salaryTip').hide();
                bool = true;
            }
        } else {
            var dom = $('#salaryTip').show();
            $('#salaryTip').text('输入纯数字');
            bool = false;
        }
    }
    var id_name = 'qiye_type';
    var qiye_type = $('#qiye_type_inp_hidden').val(); //公司类型
    if (qiye_type == '') {
        var dom = $('#' + id_name + 'Tip').show();
        bool = false;
    }
    var id_name = 'salary_type';
    var salary_type = $('input[name="' + id_name + '"]:checked').val();
    if (typeof (salary_type) == 'undefined') {
        var dom = $('#' + id_name + 'Tip').show();
        bool = false;
    }
    var id_name = 'year_born';
    var year_born = $('#year_born_inp').attr('val');  //出生年份
    if (year_born == 0) {
        $('#' + id_name + 'Tip').html('请回答问题');
        $('#' + id_name + 'Tip').show();
        bool = false;
    }

    var jobyear = $('#work_year').attr('val');        //-------------------您的工作时间 - 年 start ---------------------------
    var job_year_placeholder = $('input[name="job_year"]').attr('placeholder');   //IE6下提示语
    var id_name = 'jobtime';
    if (jobyear == $('#work_year').attr('myplace')) {
        jobyear = '';
    }
    if (job_year_placeholder == jobyear) {
        jobyear = '';
    }
    if (jobyear == '' && jobmonth == '') {
        var dom = $('#' + id_name + 'Tip').show();
        bool = false
    } else {
        if (!isNaN(jobyear)) {
            if ((jobyear < 0) || (jobyear > 100)) {
                var dom = $('.jobtime').show();
                $('.jobtime').text('0<年数<45');
                bool = false;
            }
        } else {
            var dom = $('.jobtime').show();
            $('.jobtime').text('输入纯数字');
            bool = false;
        }
    }//-------------------您的工作时间 - 年 over  ---------------------------
    var jobmonth = $('#work_month').attr('val');  //-------------------您的工作时间 - 月 start  ---------------
    var job_month_placeholder = $('input[name="job_month"]').attr('placeholder');   //IE6下提示语
    if (jobmonth == $('#work_month').attr('myplace')) {
        jobmonth = '';
    }
    if (job_month_placeholder == jobmonth) {
        jobmonth = '';
    }
    if (jobyear == '' && jobmonth == '') {
        var dom = $('#' + id_name + 'Tip').show();
        bool = false;
    } else {
        if (!isNaN(jobmonth)) {
            if ((jobmonth < 0) || (jobmonth > 11)) {
                var dom = $('.jobtime').show();
                $('.jobtime').text('0<=月数<12');
                bool = false;
            }
        } else {
            var dom = $('.jobtime').show();
            $('.jobtime').text('输入纯数字');
            bool = false;
        }
    }     //------------------- 您的工作时间 - 月 over ---------------------------
    return bool;
}
function xfCheckTwoForm(bool) {      //消费贷款验证第二步
    var bool = true;
    var has_blue_card = isHas($('input[name="has_blue_card"]:checked').val());  //1您是否有信用卡---------------start
    if (has_blue_card == '') {
        bool = false;
        $('#has_blue_card_tip').show();
    }
    if (has_blue_card == '有') {
        var count_blue_card = $('input[name="count_blue_card"]').val();             //1.1您有几张信用卡
        if (count_blue_card == $('input[name="count_blue_card"]').attr('myplace')) {
            count_blue_card = '';
        }
        if (count_blue_card == '') {
            bool = false;
            $('#count_blue_card_tip').show();
        } else {
            if (isNaN(count_blue_card)) {
                bool = false;
                $('#count_blue_card_tip').html('请输入纯数字');
                $('#count_blue_card_tip').show();
            } else {
                if (count_blue_card < 0) {
                    bool = false;
                    $('#count_blue_card_tip').html('不能为负数');
                    $('#count_blue_card_tip').show();
                } else if (count_blue_card > 10000) {
                    bool = false;
                    $('#count_blue_card_tip').html('最多10000张');
                    $('#count_blue_card_tip').show();
                } else {
                    $('#count_blue_card_tip').hide();
                }
            }
        }
        var money_blue_card = $('input[name="money_blue_card"]').val();            //1.2额度总额是多少
        if (money_blue_card == $('input[name="money_blue_card"]').attr('myplace')) {
            money_blue_card = '';
        }
        if (money_blue_card == '') {
            bool = false;
            $('#money_blue_card_tip').show();
        } else {
            if (isNaN(money_blue_card)) {
                bool = false;
                $('#money_blue_card_tip').html('请输入纯数字');
                $('#money_blue_card_tip').show();
            } else {
                if (money_blue_card < 0) {
                    bool = false;
                    $('#money_blue_card_tip').html('不能为负数');
                    $('#money_blue_card_tip').show();
                } else if (money_blue_card > 8000000) {
                    bool = false;
                    $('#money_blue_card_tip').html('800万内');
                    $('#money_blue_card_tip').show();
                } else {
                    $('#money_blue_card_tip').hide();
                }
            }
        }  //您是否有信用卡 ---------------------------------over
    } else {  //没有信用卡
        $('input[name="count_blue_card"]').val(''); //您有几张信用卡设置为空
        $('input[name="money_blue_card"]').val(''); //额度总额是多少设置为空
    }

    var has_debt_card = isHas($('input[name="has_debt_card"]:checked').val());  //2是否有负债（信用卡）---------------start
    if (has_debt_card == '') {
        bool = false;
        $('#has_debt_card_tip').show();
    }
    if (has_debt_card == '有') {
        var money_debt_card = $('input[name="money_debt_card"]').val();             //2.1负债多少
        if (money_debt_card == $('input[name="money_debt_card"]').attr('myplace')) {
            money_debt_card = '';
        }
        if (money_debt_card == '') {
            bool = false;
            $('#money_debt_card_tip').show();
        } else {
            if (isNaN(money_debt_card)) {
                bool = false;
                $('#money_debt_card_tip').html('请输入纯数字');
                $('#money_debt_card_tip').show();
            } else {
                if (money_debt_card < 0) {
                    bool = false;
                    $('#money_debt_card_tip').html('不能为负数');
                    $('#money_debt_card_tip').show();
                } else if (money_debt_card > 8000000) {
                    bool = false;
                    $('#money_debt_card_tip').html('800万内');
                    $('#money_debt_card_tip').show();
                } else {
                    $('#money_debt_card_tip').hide();
                }
            }
        }
    } //是否有负债（信用卡）-----------------------------over
    else {  //没有负债（信用卡）
        $('input[name="money_debt_card"]').val(''); //负债多少设置为空
    }
    var has_succ_reply = isHas($('input[name="has_succ_reply"]:checked').val());  //3您之前是否成功申请贷款---------------start
    if (has_succ_reply == '') {
        bool = false;
        $('#has_succ_reply_tip').show();
    } else {
        $('#has_succ_reply_tip').hide();
    }  //您之前是否成功申请贷款---------------over
    var has_debt_loan = isHas($('input[name="has_debt_loan"]:checked').val());  //4是否有负债（贷款）---------------start
    if (has_debt_loan == '') {
        bool = false;
        $('#has_debt_loan_tip').show();
    }
    if (has_debt_loan == '有') {
        var money_debt_loan = $('input[name="money_debt_loan"]').val();             //4.1负债多少
        if (money_debt_loan == $('input[name="money_debt_loan"]').attr('myplace')) {
            money_debt_loan = '';
        }
        if (money_debt_loan == '') {
            bool = false;
            $('#money_debt_loan_tip').show();
        } else {
            if (isNaN(money_debt_loan)) {
                bool = false;
                $('#money_debt_loan_tip').html('请输入纯数字');
                $('#money_debt_loan_tip').show();
            } else {
                if (money_debt_loan < 0) {
                    bool = false;
                    $('#money_debt_loan_tip').html('不能为负数');
                    $('#money_debt_loan_tip').show();
                } else if (money_debt_loan > 8000000) {
                    bool = false;
                    $('#money_debt_loan_tip').html('800万内');
                    $('#money_debt_loan_tip').show();
                } else {
                    $('#money_debt_loan_tip').hide();
                }
            }
        }  //是否有负债（贷款）---------------------over
    }
    else {  //没有负债（贷款）
        $('input[name="money_debt_loan"]').val(''); //负债多少（贷款）设置为空
    }
    return bool;
}
function isHas(val) {
    var res = '';
    if (val == 1) {
        res = '有';
    } else if (val == 2) {
        res = '没有';
    }
    return res;
}
function placeholderBox() {
    var doc = document, inputs = doc.getElementsByTagName('input'), supportPlaceholder = 'myplace'in doc.createElement('input'), placeholder = function(input) {
        var text = input.getAttribute('myplace'), defaultValue = input.defaultValue;
        if (defaultValue == '') {
            input.value = text
        }
        $(input).css('color', '#ccc');  //灰色
        input.onfocus = function() {
            if (input.value === text) {
                this.value = ''
            }
            $(this).css('color', '#333');   //黑色
        };
        input.onblur = function() {
            if (input.value === '') {
                this.value = text
                $(this).css('color', '#ccc');
            } else {
                $(this).css('color', '#666');
            }
        }
    };
    if (!supportPlaceholder) {
        for (var i = 0, len = inputs.length; i < len; i++) {
            var input = inputs[i], text = input.getAttribute('myplace');
            if (input.type === 'text' && text) {
                placeholder(input)
            }
        }
    }
}
function placeholderBoxText() {
    var doc = document, inputs = doc.getElementsByTagName('textarea'), supportPlaceholder = 'myplace'in doc.createElement('textarea'), placeholder = function(input) {
        var text = input.getAttribute('myplace'), defaultValue = input.defaultValue;
        if (defaultValue == '') {
            input.value = text
        }
        input.onfocus = function() {
            if (input.value === text) {
                this.value = ''
            }
            $(this).css('color', '#666');
        };
        input.onblur = function() {
            if (input.value === '') {
                this.value = text
                $(this).css('color', '#ccc');
            } else {
                $(this).css('color', '#666');
            }
        }
    };
    for (var i = 0, len = inputs.length; i < len; i++) {
        var input = inputs[i], text = input.getAttribute('myplace');
        placeholder(input);
    }
}
$('.tboxb').click(function() {   //第一步
    $('.tishi').hide(); //隐藏所有提示框
});
$('.tbsb').click(function() {    //第二步
    $('.tishi2').hide(); //隐藏所有提示框 
});
function inpTwoFocus(obj) {
    $(obj).css('color', '#666');
}
function inpVal(default_str, obj) {
    var obj = $(obj);
    var val = obj.val();
    if (val == default_str) {     //默认字符串
        obj.val('');
    } else if (val == '') {
        obj.val(default_str);
        obj.css('color', '#ccc');
    }
}
function checkTwoForm() {
    var bool = true;
    var nickname = $.trim($('#nickname').val());
    var nknet = $('#nicknameTip');
    var mobile = $('#mobile').val();

    if (nickname == $('#nickname').attr('myplace')) {
        nickname = '';
    }
    if (/[\s><,._\。\[\]\{\}\?\/\+\=\|\'\\\":;\~\!\@\#\*\$\%\^\&`\uff00-\uffff)(]+/.test(nickname) && nickname.length > 0) {
        nknet.show();
        nknet.text('输入只限中英文');
        bool = false;
    } else if (nickname == '') {
        nknet.show();
        bool = false;
    } else if (nickname.length == 0) {
        nknet.show();
        nknet.text('请填写您的称呼');
        bool = false;
    } else {
        nknet.hide();
    }
    if (mobile == '' || mobile == '用于接收信贷员联系方式') {
        $('#mobileTip').show();
        bool = false;
    } else {
        if (checkMobile(mobile) == false) {    //验证手机失败
            $('#mobileTip').show();
            bool = false;
        } else {
            $('#mobileTip').hide();
        }
    }
    var email = $('#email').val();
    var def_email = $('#email').attr('defval'); //默认值
    if (email != def_email) {
        if (checkEmail(email) == false) {
            $('#emailTip').show();
            bool = false;
        }
    }
    return bool;
}
function applySend() {   //立即联系信贷经理 
    var sendData = $('#applyForm').serialize();
    var xd_id = $('#xd_id').val();
    var xd_type = $('#xd_type').val();
    var zone_id = $('#zone_id').val();
    var remark = remarkApply(xd_type);      //（主要值）如：每月打入银行卡的工资:5000<br />工资发放形式:公司转帐或自己定期存银行卡<br />您的公司类型:个体工商户
    var bank_id = $('#bank_id').val();
    var money = $('#money_detail').val();
    var month = $('#month_detail').val();
    var source_host = $('#source_host').val();
    var ref = $('#ref').val();
    var cookie_has_apply_user_daily = $('#cookie_has_apply_user_daily').val();
//    alert(cookie_has_apply_user_daily);
    var num = parseInt(cookie_has_apply_user_daily);
    num += 1;
    sendData += ('&url=' + document.location.href + '&xd_id=' + xd_id + '&xd_type=' + xd_type + '&zone_id=' + zone_id +
            '&remark=' + remark + '&bank_id=' + bank_id + '&money=' + money + '&month=' + month + '&source_host=' + source_host +
            '&ref=' + ref);
    $.post('/xindai/applySend', sendData, function(data) {
        if (data != false) {  //添加成功 
            iu_id = data;
            $('#has_apply_daily_' + xd_id).val(1);    //设置cookie每名用户同个产品申请与否
            $('#cookie_has_apply_user_daily').val(num); //设置每名用户每天申请的次数
        } else {  //失败 

        }
    });
}
function remarkApply(xd_type) {
    var split_str = '<br />';
    var remark = '';
    if (xd_type == 1) { //------------------------------------------- 企业 -------------------------------------------
        var qiye_type = $('#qiye_type_inp_hidden').val(); //公司类型 
        var monthly = '对公加对私月流水:' + $('#monthly').val();
        var BusinessTime = '经营年限:' + $('#BusinessTime').attr('reval');
        var has_house = $('input[name="has_house"]:checked').val();
//        var register = $('input[name="register"]:checked').val();
//        remark = (qiye_type + split_str + monthly + BusinessTime + split_str + has_house + split_str + register);
        remark = (qiye_type + split_str + monthly + split_str + BusinessTime + split_str + has_house + split_str);
    } else if (xd_type == 2) { //------------------------------------------- 购车 -------------------------------------------
        var has_house = $('input[name="has_house"]:checked').val();//是否有本地房产
//        var car_number = $('input[name="car_number"]:checked').val();
//        var car_type = $('input[name="car_type"]:checked').val();
        var salary = $('#salary_inp_hidden').attr('val') + ':' + $('#salary_inp_hidden').val(); //每月打入银行卡的工资
        var car_stage = $('input[name="car_stage"]:checked').val();//购车阶段
//        var car_use = $('input[name="car_use"]:checked').val();
        remark = (has_house + split_str + car_stage + split_str + salary);
    } else if (xd_type == 3) { //------------------------------------------- 购房 -------------------------------------------
        var goufang_type = $('#goufang_type_inp_hidden').val(); //公司类型
        var salary = $('#salary_inp_hidden').attr('val') + ':' + $('#salary_inp_hidden').val(); //每月打入银行卡的工资 
//        var first_house = $('input[name="first_house"]:checked').val();
        var household = $('input[name="household"]:checked').val();
        var second_hand_house = $('input[name="second_hand_house"]:checked').val();
        remark = (goufang_type + split_str + salary + split_str + household + split_str + second_hand_house);
    } else if (xd_type == 4) { //------------------------------------------- 消费 -------------------------------------------        
        var salary = $('#salary_inp_hidden').attr('val') + ':' + $('#salary_inp_hidden').val(); //每月打入银行卡的工资 
        var salary_type = $('input[name="salary_type"]:checked').val();
//        var qiye_type = $('#qiye_type_inp_hidden').attr('val') + ':' + $('#qiye_type_inp_hidden').val();    //公司类型
        var qiye_type = $('#qiye_type_inp_hidden').val();   //公司类型
//        var year_born = $('#year_born_inp').attr('val') + ':' + $('#year_born_inp').val();
        var year_born = '您的出生年份' + ':' + $('#year_born_inp').attr('val');
//                $('#year_born_inp_hidden').val();  //出生年份
        var job_year = $('div[name="job_year"]').attr('val');       //工作年限
        var job_year_placeholder = $('div[name="job_year"]').attr('placeholder') + '年';   //IE6下提示语
        if (job_year_placeholder == job_year) {
            job_year = '';
        }
        var job_month = $('div[name="job_month"]').attr('val');     //工作月份
        var job_month_placeholder = $('div[name="job_month"]').attr('placeholder');   //IE6下提示语
        if (job_month_placeholder == job_month) {
            job_month = '';
        }
        var job_year_month = '';
        if ((job_year + job_month) != '') {
            var str = '';
            if (job_year != '') {
                str = job_year + '年 ';
            }
            if (job_month != '') {
                str += job_month + '月';
            }
            job_year_month = split_str + '您的工作时间是:' + str;
        }
        //-------------- 2013-08-07 新增的个人信用信息(多的第二步)--------------------------------
        var has_blue_card = isHas($('input[name="has_blue_card"]:checked').val());
        if (has_blue_card != '') {
            has_blue_card = split_str + '您是否有信用卡:' + has_blue_card + split_str;  //您是否有信用卡 :  
        }
        var count_blue_card = $('input[name="count_blue_card"]').val();     //您有几张信用卡
        var count_blue_card_placeholder = $('input[name="count_blue_card"]').attr('placeholder');   //IE6下提示语
        if (count_blue_card == count_blue_card_placeholder) {
            count_blue_card = '';
        }
        if (count_blue_card != '') {
            count_blue_card = '您有几张信用卡:' + count_blue_card + split_str;
        }
        var money_blue_card = $('input[name="money_blue_card"]').val();     //额度总额是多少
        var money_blue_card_placeholder = $('input[name="money_blue_card"]').attr('placeholder');   //IE6下提示语
        if (money_blue_card == money_blue_card_placeholder) {
            money_blue_card = '';
        }
        if (money_blue_card != '') {
            money_blue_card = '额度总额是多少:' + money_blue_card + split_str;
        }
        var has_debt_card = isHas($('input[name="has_debt_card"]:checked').val());   //是否有负债（信用卡）
        if (has_debt_card != '') {
            has_debt_card = '是否有负债（信用卡）:' + has_debt_card + split_str;
        }
        var money_debt_card = $('input[name="money_debt_card"]').val();     //负债多少
        var money_debt_card_placeholder = $('input[name="money_debt_card"]').attr('placeholder');   //IE6下提示语
        if (money_debt_card == money_debt_card_placeholder) {
            money_debt_card = '';
        }
        if (money_debt_card != '') {
            money_debt_card = '负债多少（信用卡）:' + money_debt_card + split_str;
        }
        var has_succ_reply = isHas($('input[name="has_succ_reply"]:checked').val());   //您之前是否成功申请贷款
        if (has_succ_reply != '') {
            has_succ_reply = '您之前是否成功申请贷款:' + has_succ_reply + split_str;
        }
        var has_debt_loan = isHas($('input[name="has_debt_loan"]:checked').val());   //是否有负债（贷款）
        if (has_debt_loan != '') {
            has_debt_loan = '是否有负债（贷款）:' + has_debt_loan + split_str;
        }
        var money_debt_loan = $('input[name="money_debt_loan"]').val();    //负债多少
        var money_debt_loan_placeholder = $('input[name="money_debt_loan"]').attr('placeholder');   //IE6下提示语
        if (money_debt_loan == money_debt_loan_placeholder) {
            money_debt_loan = '';
        }
        if (money_debt_loan != '') {
            money_debt_loan = '负债多少（贷款）:' + money_debt_loan + split_str;
        }
        var xiaofei_two = job_year_month + has_blue_card + count_blue_card + money_blue_card + has_debt_card + money_debt_card +
                has_succ_reply + has_debt_loan + money_debt_loan;   //新增的个人信用信息(多的第二步)

        remark = (salary + split_str + salary_type + split_str + year_born + split_str + qiye_type + xiaofei_two);
    }
    return remark;
}
function sendApplydetails() { //发送申请详情
    var details = $.trim($('#applydetails').val());
    if (details == $('#applydetails').attr('myplace')) {
        details = '';
    }
    if (details == '' || iu_id == null) {
        return false;
    }
    var xd_type = $('#xd_type').val();
    var sendData = ('id=' + iu_id + '&details=' + details + '&xd_type=' + xd_type);
    $.post('/xindai/applyDetails', sendData, function(data) {
    });
}
function XDYzm(obj) {         //验证码验证
    var obj = $('#cboxLoadedContent #xd_yzm');
    var val = $.trim(obj.val());
    var tsgang = $('#cboxLoadedContent #real_name_ret_1');
    var tsgou = $('#cboxLoadedContent #real_name_ret_2');
    if (val.length === 4) {
        $.post('/home/ajax/imgCommentverify', 'verify=' + val + '&r=' + g_time, function(msg) {
            if (msg == 1) {
                tsgou.removeClass('none');
                tsgang.addClass('none');
                yzYZM = true;
            } else {
                tsgang.removeClass('none');
                tsgou.addClass('none');
                tsgang.html('验证码有误');
                yzYZM = false;
            }
        });
    } else {
        if (val.length == '' || val.length < 4) {
//            tsgang.removeClass('none');
            yzYZM = false;
        }
    }
}
function XDYzmKd(obj) {
    var tsgang = $('#cboxLoadedContent #real_name_ret_1');
    var tsgou = $('#cboxLoadedContent #real_name_ret_2');
    tsgang.addClass('none');
    tsgou.addClass('none');
}
function XdBtn() {
    var obj = $('#cboxLoadedContent #xd_yzm');
    var val = $.trim(obj.val());
    var tsgang = $('#cboxLoadedContent #real_name_ret_1');
    var tsgou = $('#cboxLoadedContent #real_name_ret_2');
    if (val.length == '' || val == '请输入验证码') {
        tsgang.removeClass('none');
        tsgou.addClass('none');
        tsgang.html('验证码不能为空');
        return false;
    }

    $.post('/home/ajax/imgCommentverify', 'verify=' + $('#cboxLoadedContent #xd_yzm').val() + '&r=' + g_time, function(msg) {
        if (msg == 1) {
            dom_id = $('#product_id').val();
            $('#xd_id').val(dom_id);    //设置hidden xd_id为当前弹开的信贷ID
            var dom = $('#xdapplyBox');
            var html = dom.html();
            dom.empty();  //清理掉多余的部分
            car_id = $.colorbox({
                html: html,
                opacity: 0.3,
                transition: 'fade',
                speed: 130,
                overlayClose: false, //为true单击遮罩层就可以把ColorBox关闭
                slideshowAuto: true,
                scrolling: false, //不显示滚动栏
                onComplete: function() {
                    car_id.colorbox.resize();
                },
                onClosed: function() {
                    dom.html(html);
                }
            });
        } else {

            tsgang.removeClass('none');
            tsgou.addClass('none');
            return false;
        }
        myplaceBox();           //加入myplace框提示
        myplaceBoxText();       //加入myplace框提示
    });
}
function salarytypeSel(type) { //工资发放形式选项卡切换
    var salary_show = '每月打入银行卡的工资';
    if (type == 'two') {
        salary_show = '每月领取现金';
    }
    $('#salary_show').html(salary_show);
    $('#salary_inp_hidden').attr('val', salary_show);
}

function BirthYearCk(obj) {//出生年份下拉函数
    var obj = $(obj);
    var yon = $('#BirthYearSelect').hasClass('none');
    if (yon == true) {
        $('#BirthYearSelect').removeClass('none');
    } else {
        $('#BirthYearSelect').addClass('none');
    }
}
function yearliSelect(obj) {
    var obj = $(obj);
    $('#BirthYearSelect p a').removeClass('yearOK');
    obj.addClass('yearOK');
    //var B_val = $('#year_born_inp').val();
    obj_reval = obj.attr('yearval');
    $('#year_born_inp').attr('val', obj_reval);
    $('#year_born_inp').html(obj_reval);
    $('#BirthYearSelect').addClass('none');
}
function JobYearMonthCk(obj) {//工作年月下拉函数
    var obj = $(obj);
    var con = obj.parent().children('.JobYMSelect');
    var yon = con.hasClass('none');
    if (yon == true) {
        con.removeClass('none');
    } else {
        con.addClass('none');
    }
}
function JobYearMonthSelect(obj) {//工作年月具体项选择函数
    var obj = $(obj);
    var s_yearmonth = obj.parent().parent().parent('.time').children('.s_yearmonth');//alert(s_yearmonth.html())
    obj.parent('').parent('').find('a').removeClass('yearOK');
    obj.addClass('yearOK');
    var obj_reval = obj.attr('reval');
    var obj_html = obj.attr('st');
    s_yearmonth.attr('val', obj_html);
    s_yearmonth.html(obj_html);
    var xl_ul = obj.parent().parent('.JobYMSelect');
    xl_ul.addClass('none');
}
function BusinessTimeCK(obj){//经营年限---下拉
    var obj = $(obj);
    var yon = $('#BusinessTime_ul').hasClass('none');
    if(yon == true){
        $('#BusinessTime_ul').removeClass('none');
    }else{
        $('#BusinessTime_ul').addClass('none');
    }
}
function BusinessTime_select(obj){//经营年限---选择
    var obj = $(obj);
    var obj_reval = obj.attr('reval');
    var obj_html = obj.html();
    $('#BusinessTime').html(obj_html);
    $('#BusinessTime').attr('reval',obj_reval);
    $('#BusinessTime_ul li a').removeClass('btOK');
    obj.addClass('btOK');
    $('#BusinessTime_ul').addClass('none');
    //var s_val = $('#BusinessTime').val();
}
/***********************************新加的*************************************/
$(function(){
	$("#two,#three").hide();
	$("#one_click").click(function(){
	   /**第一个判断***/
		var reg=/^[0-9]{4}$/;
		if(!$("#born_year").val().match(reg)){
			$("#onetips").text("不正确");
				return false;
			}else if( $("#born_year").val()<1950){
				$("#onetips").text("不正确");
				return false;
				
			}else if($("#born_year").val()>2013){
				$("#onetips").text("不正确")
				return false;
				}
				else{$("#onetips").text("")}

	   /**第二个判断***/
	   var reg=/^[0-9]+$/;
	   if(!$("#salary_inp_hidden").val().match(reg)){
			$("#twotips").text("请填写");
			return false;
		   }
	   else{$("#twotips").text("")}
	   /**第三个判断***/
	   var reg=/^[0-9]+$/;
	   if(!$("#job_year").val().match(reg)){
		   $("#timetips").text("请填写");
		   return false;
		   }
	   $("#two").show().siblings().hide();
		});
		
	/**************第二步开始******************/	
	$("#two_click").click(function(){
	    var bool = true;
        var has_blue_card = isHas($('input[name="has_blue_card"]:checked').            val()); //1您是否有信用卡---------------start
        if (has_blue_card == '') {
            bool = false;
       		$('#has_blue_card_tip').show();
			return false;
  		  }
		  
		if(has_blue_card == '有'){
			var count_blue_card = $('input[name="count_blue_card"]').val();             //1.1您有几张信用卡
			if (count_blue_card == '') {
				bool = false;
				$('#count_blue_card_tip').show();
				return false;
				}
				/*******几张信用卡********/ 
 			   var reg=/^[0-9]+$/;
			   if(!$("#count_blue_card").val().match(reg)){
					$("#has_blue_card_tip").show();
					return false;
				   }
			   else{$("#has_blue_card_tip").text("")}

				/*******额度总额********/ 
 			   var reg=/^[0-9]+$/;
			   if(!$("#money_blue_card").val().match(reg)){
					$("#money_blue_card_tip").show();
					return false;
				   }
			   else{$("#money_blue_card_tip").text("")}
			}
			
	    var bool = true;
        var has_blue_card = isHas($('input[name="has_blue_card"]:checked').            val()); //1您是否有信用卡---------------start
        if (has_blue_card == '') {
            bool = false;
       		$('#has_debt_card_tip').show();
			return false;
  		  }
		  
		 /*************是否有负债的信用卡********************/
	    var bool = true;
        var has_debt_card = isHas($('input[name="has_debt_card"]:checked').            val()); //1您是否有信用卡---------------start
        if (has_debt_card == '') {
            bool = false;
       		$('#has_debt_card_tip').show();
			return false;
  		  }
		  
		 
		if(has_debt_card == '有'){
			var has_debt_card = $('input[name="has_debt_card"]').val();             //1.1您有几张信用卡
			if (has_debt_card == '') {
				bool = false;
				$('#has_debt_card_tip').show();
				return false;
				}
				/*******负债多少********/ 
 			   var reg=/^[0-9]+$/;
			   if(!$("#money_debt_card").val().match(reg)){
					$("#money_debt_card_tip").show();
					return false;
				   }
			   else{$("#money_debt_card_tip").text("")}

			}
			
		 /*************是否成功申请贷款********************/
	    var bool = true;
        var has_succ_reply = isHas($('input[name="has_succ_reply"]:checked').            val()); 
        if (has_succ_reply == '') {
            bool = false;
       		$('#has_succ_reply_tip').show();
			return false;
  		  }
		  
		 /*************是否有负债的贷款********************/
	    var bool = true;
        var has_debt_loan = isHas($('input[name="has_debt_loan"]:checked').            val()); //1您是否有信用卡---------------start
        if (has_debt_loan == '') {
            bool = false;
       		$('#has_debt_loan_tip').show();
			return false;
  		  }
		  
		 
		if(has_debt_loan == '有'){
			var has_debt_loan = $('input[name="has_debt_loan"]').val();             //1.1您有几张信用卡
			if (has_debt_loan == '') {
				bool = false;
				$('#has_debt_loan_tip').show();
				return false;
				}
				/*******负债多少********/ 
 			   var reg=/^[0-9]+$/;
			   if(!$("#money_debt_loan").val().match(reg)){
					$("#money_debt_loan_tip").show();
					return false;
				   }
			   else{$("#money_debt_loan_tip").text("")}

			}
		$("#three").show().prev().hide();
		});
});

		
		/****************最后表单验证*******************/
	function checkapplyform(){
		/*******称呼******/
		
		if($("#nickname").val()==""){
			
			$("#nicknameTip").show();
			return false;
			}
		else
		{
			$("#nicknameTip").text("");
		}
		
		/*******电话******/
		 var reg=/^(13[0-9]|14[0-9]|15[0-9]|18[0-9])\d{8}$/;
		 if(!$("#mobile").val().match(reg)){
			  $("#mobileTip").show();
			  return false;
			 }
		 else{$("#mobileTip").text("")}

		
		
		
		/*******邮箱******/
		 var reg=/^[\.a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/;
		 if(!$("#email").val().match(reg)){
			  $("#emailTip").show();
			  return false;
			 }
		 else{$("#emailTip").text("")}
		};



/************************购车贷*************************************/
	$(function(){
		$("#first_click").click(function(){
			  $("#three").show().siblings().hide();
			});
		
		
		
		
		
		
		
		});











