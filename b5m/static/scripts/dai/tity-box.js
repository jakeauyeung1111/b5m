
function changeCity(){
    var city_box = $('#city_box');
    var status = city_box.css("display");
    if(status == 'none'){   //隐藏着
        city_box.show();
    }else{
        city_box.hide();
    }
    $(document).bind("click",function(e){       //点击空白区域关闭
        var target  = $(e.target);
        if(target.closest("#city_box").length == 0){
            $("#city_box").hide();
        }
    });
}