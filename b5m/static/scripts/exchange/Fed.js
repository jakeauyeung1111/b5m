;var Fed = {
    ie:function(){
        return navigator.userAgent.indexOf("MSIE") > 0;
    }(),
    ie6:function(){
        return typeof document.body.style.maxHeight =="undefined"
    }(),
    /*打开链接*/
    wOpen:function(url){
        function o(){
            window.open(url,'帮5买');
            /*if(!window.open(url,'帮5买'))
             window.location.href = url;*/
        }
        if(/Firefox/.test(navigator.userAgent)) {
            setTimeout(o,0);
        } else {
            o();
        }
    },
    bnner:function(args){
        var Paper = Raphael(args['id'],args['width'],args['height'])
            ,imgPos = [
                {x:90,y:64,width:549,height:266}
                ,{x:20,y:44,width:170,height:170}
                ,{x:140,y:24,width:144,height:144}
                ,{x:300,y:6,width:130,height:130}
                ,{x:446,y:24,width:144,height:144}
                ,{x:540,y:44,width:170,height:170}
            ]
            ,imgArr = []
            ,rectArr = []
            ,sIndex = 0
            ,Len = args['images'].length
            ,isIe = window.attachEvent?true:false
            ,aTime = isIe?150:180
            ,autoPrev;
        function dom(id){
            return document.getElementById(id);
        }
        var box = dom(args['id']);
        /*图片布局*/
        for(var i =0;i<Len;i++){
            imgArr[i] = Paper.image(args['images'][i]['src']).attr(imgPos[i])//.glow({width:4,color:"#fff",offsetx:0,offsety:0,opacity:0});
            if(i>0&&i<5){
                imgArr[i].insertBefore(imgArr[i-1]);
            }
            if(i>5){
                imgArr[i].insertAfter(imgArr[i-1]);
            }
            var Rect = Paper.rect().attr({x:imgPos[i]['x']-2,y:imgPos[i]['y']-2,width:imgPos[i]['width']+4,height:imgPos[i]['height']+4,"stroke-width":5,stroke:"#fff","stroke-opacity":0.6,fill:"#fff","fill-opacity":0,"r":3,title:args['images'][i]['title']||""/*,href:args['images'][i]['link'],target: "_blank"*/,cursor: "pointer"}).data("hrefdata",args['images'][i]['link']);
            rectArr[i] = Rect;
            rectArr[i].insertAfter(imgArr[i]);
            /*绑定链接*/
            Rect.click(function(){
                Fed.wOpen(this.data("hrefdata"));
                return false;
            });
        }
        imgArr[0].insertAfter(rectArr[Len-1]);
        rectArr[0].insertAfter(imgArr[0]);
        /*左点击方法*/
        var prevFun = function(){
            var tIndex = 0;
            sIndex = sIndex == Len-1 ? 0 : sIndex+1;
            for(var i = 0;i< Len;i++){
                tIndex = i+sIndex;
                if(tIndex >= Len){
                    tIndex = tIndex-Len;
                }
                imgArr[i].animate(imgPos[tIndex],aTime);
                rectArr[i].animate({x:imgPos[tIndex]['x']-2,y:imgPos[tIndex]['y']-2,width:imgPos[tIndex]['width']+4,height:imgPos[tIndex]['height']+4},aTime);

            }
            var aIndex = sIndex==0?0:Len-sIndex
                ,pIndex = aIndex+1 >= Len ?0:aIndex+1
                ,nIndex = aIndex-1 < 0 ? Len-1 : aIndex-1;
            imgArr[aIndex].insertAfter(rectArr[pIndex]);
            imgArr[nIndex].insertBefore(imgArr[aIndex]);
            rectArr[aIndex].insertAfter(imgArr[aIndex]);
            rectArr[nIndex].insertAfter(imgArr[nIndex]);
        };
        var autoScroll = function(){
            autoPrev = setInterval(function(){
                prevFun();
            },3000);
        };
        box.onmouseover = function(){
            clearInterval(autoPrev);
        };
        box.onmouseout = autoScroll;
        autoScroll();
        /*前后翻页*/
        /*var btPrev = Paper.image("./images/bg_prev.png",0,190,64,65).attr({opacity:0.5,cursor:"pointer"})
         ,btNext = Paper.image("./images/bg_next.png",args['width']-65,190,65,65).attr({opacity:0.5,cursor:"pointer"});*/
        var btPrev = dom('bnns_prev')
            ,btNext = dom('bnns_next');
        btPrev.onclick = prevFun;
        btNext.onclick = function(){
            var tIndex = 0;
            sIndex = sIndex <= 0 ? Len-1 : sIndex-1;
            for(var i = 0;i< Len;i++){
                tIndex = i+sIndex;
                if(tIndex >= Len){
                    tIndex = tIndex-Len;
                }
                imgArr[i].animate(imgPos[tIndex],aTime);
                rectArr[i].animate({x:imgPos[tIndex]['x']-2,y:imgPos[tIndex]['y']-2,width:imgPos[tIndex]['width']+4,height:imgPos[tIndex]['height']+4},aTime);

            }
            var aIndex = Len-sIndex-1
                ,pIndex = aIndex+1==Len?0:aIndex+1
                ,npIndex = pIndex+1==Len?0:pIndex+1;
            imgArr[pIndex].insertAfter(rectArr[aIndex]);
            rectArr[pIndex].insertAfter(imgArr[pIndex]);
            imgArr[npIndex].insertBefore(imgArr[pIndex]);
            rectArr[npIndex].insertAfter(imgArr[npIndex]);
        };
    },
    scroll:function(args){
        var _Args = $.extend({},{auto:true},args||{})
            ,scrollBoxId = _Args['id']
            ,scrollContId = '.scroll-list'
            ,autoScroll
            ,scrollTim = 4000;
        if($('a',scrollContId).length<4){
            $(".next,.prev",scrollBoxId).css("opacity",'0.3');
            return this;
        }
        /*向前滚动*/
        var scrollNext = function(){
            var firstDom = $('a:first',scrollContId)
                ,$contBox = $(scrollContId);
            $contBox.animate({marginLeft:"-="+firstDom.outerWidth(true)},function(){
                $contBox.append(firstDom).removeAttr("style");
            });
        };
        /*向后滚动*/
        var scrollPrev = function(){
            var lastDom = $('a:last',scrollContId)
                ,$contBox = $(scrollContId)
                ,domWidth = lastDom.outerWidth(true);
            $contBox.prepend(lastDom).css("marginLeft",'-'+domWidth+"px").animate({marginLeft:"+="+domWidth});
        };
        if(_Args['auto']){
            $(scrollBoxId).hover(function(){
                clearInterval(autoScroll);
            },function(){
                autoScroll = setInterval(function(){scrollNext();},scrollTim)
            }).trigger("mouseout");
        }
        $(".next,.prev",scrollBoxId).on({
            "click":function(){
                var $this = $(this);
                if($this.hasClass('next')){
                    scrollPrev();
                }else{
                    scrollNext();
                }
                return false;
            }
        });
        return this;
    },
    tabBnner:function(args){
        var Paper = Raphael(args['id'],460,190)
            ,tPaper = Raphael("cbnn-box-pic",534,253)
            ,bnnrIndex = 0;
        function drawTabs(index){
            index = index||0;
            var r = 45
                ,circleArr = []
                ,topLong = 12
                ,imgData;
            for(var i = 0;i< args['images'].length;i++){
                imgData = args['images'][i];
                if(Fed.ie){
                    Paper.image(imgData['pics'],18 + (27+r*2)*i,topLong+6,80,80).attr({r:8});
                }else{
                    Paper.image(imgData['pics'],17 + (27+r*2)*i,topLong+5,80,80).attr({r:8});
                }

                var circleDom = Paper.circle(57+(20+97)*i,r+topLong,r).attr({"fill":"#fff","fill-opacity":0,"stroke":"#fff","stroke-width":11,"stroke-opacity":0.5,cursor:"pointer"})
                    ,txtDom = Paper.text(60+(25+r*2)*i, 125,imgData['title']).attr({fill:"#fff",font:"12px Microsoft Yahei",opacity:0.8})
                    ,txtDomb = Paper.text(62+(25+r*2)*i, 145,imgData['descript']).attr({font:"12px Microsoft Yahei",fill:"#fff",opacity:0.8})
                    ,txtLine = Paper.path(["M",9+(25+r*2)*i,120,"L",9+(25+r*2)*i,132]).attr({"stroke-width":5,stroke: "#ffc50f",opacity:0.8});
                circleArr[i] = circleDom;
                if(i == index){
                    circleDom.attr("stroke-opacity",0.9).glow({color:"#000",width:4,offsetx:0,offsety:2,opacity:1});
                    txtDom.attr({opacity:1});
                    txtDomb.attr({opacity:1});
                    txtLine.attr({opacity:1});
                    tPaper.image(args['images'][index]['pic'],0,0,486,204);
                }
                if(i!=0){
                    Paper.path(["M",2+(25+r*2)*i,30,"L",2+(25+r*2)*i,90]).attr({"stroke-width":1,"stroke":"#fff","stroke-linecap": "round","stroke-linejoin": "round",opacity:0.5});
                }
                circleDom.mouseover(function(){
                    for(var i = 0;i<circleArr.length;i++){
                        if(circleArr[i]==this){
                            if(bnnrIndex == i){
                                return false;
                            }
                            bnnrIndex = i;
                            Paper.clear();
                            tPaper.clear();
                        }
                    }
                    drawTabs(bnnrIndex);
                });
                circleDom.click(function(){
                    Fed.wOpen(args['images'][bnnrIndex]['link']);
                });
            }
        }
        drawTabs();
        document.getElementById("cbnn-box-pic").onclick = function(){
            Fed.wOpen(args['images'][bnnrIndex]['link']);
        };
    },
    listScroll:function(){
        function scrollFun(box){
            var autoScroll;
            box.hover(function(){
                clearInterval(autoScroll)
            },function(){
                autoScroll = setInterval(function(){
                    var liDom = $("li:eq(1)",box);
                    liDom.animate({marginTop:"-34px"},240,function(){
                        liDom.appendTo(box).css("margin",0);
                    })
                },2000)
            }).trigger("mouseout");
        }
        scrollFun($("ul",".sort-top"));
        scrollFun($("ul",".sort-partner"));
    },
    invit:function(){
        var btHtml = ''
            , sLink = "";
        $.getJSON("http://ucenter.b5m.com/user/invitation/data/detail.htm?jsonpCallback=?",function (data){
            if (data['ok']) {
                sLink = data['data']['invitationUrl'];
                $("p>a", ".box-nvite").text(sLink).attr("href", sLink);
                btHtml += '<a target="_blank" href="http://ucenter.b5m.com/forward.htm?method=/user/account/invitation/index&mps=0.0.0.0.0" class="nvite-botton">详细</a>';
                $(".nvite-bt").html(btHtml);
                popWindowShareFed.init({
                    id:".nvite-bt",
                    href:sLink,
                    content:'#邀请好友来帮5买#有个网购省钱利器我必须告诉你，搜一下商品就能省很多Money！还有更多优惠方式，还不立刻行动？'
                });
                return false;
            } else {
                btHtml += '<a href="http://ucenter.b5m.com/" class="nvite-botton2" title="立即登录"></a>';
                $("a", ".box-nvite").remove();
            }
            $(".nvite-bt").html(btHtml);
        });
    },
    loadFun:function(){
        var shareScript = document.createElement('script')
            ,shareLink = document.createElement('link')
            ,bodyDom = document.getElementsByTagName('body')[0]
            ,_this = this;
        shareScript.setAttribute('src',"http://staticcdn.b5m.com/scripts/common/popwindow-share.js");
        shareLink.setAttribute('href',"http://staticcdn.b5m.com/css/common/popwindow-share.css");
        shareLink.setAttribute('rel',"stylesheet");
        bodyDom.appendChild(shareScript);
        bodyDom.appendChild(shareLink);
        this.scroll({id:".box-scroll"});
        if(this.ie6){
            setTimeout(function(){
                $(".bnn-scroll").css("marginTop","-60px");
            },100)
        }
        this.listScroll();
        setTimeout(function(){
            _this.invit();
        },800)
    }
};