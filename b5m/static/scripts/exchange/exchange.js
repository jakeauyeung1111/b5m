var showPopup = function(mask, elem){
    mask.show();
    setMaskHeight(mask);
    setPos(elem);
    elem.show();
};
var hidePopup = function(mask, elem){
    mask.hide();
    elem.hide();
};
var getPos = function(elem){
    return {
        left: ($(window).width() - elem.outerWidth()) / 2 + $(window).scrollLeft() + 'px',
        top: ($(window).height() - elem.outerHeight()) / 2 + $(window).scrollTop() + 'px'
    };
};
var setPos = function(elem){
    elem.css({
        left: getPos(elem).left,
        top : getPos(elem).top
    });
};
var setMaskHeight = function(elem){
    var doc_h = $(document).height() + 'px';
    elem.css('height', doc_h);
};
(function($){
    $.fn.imgscroll = function(o){
        var defaults = {
            speed: 40,
            amount: 0,
            width: 1,
            dir: "left"
        };
        o = $.extend(defaults, o);

        return this.each(function(){
            var _li = $('li', this);
            _li.parent().parent().css({overflow: "hidden", position: "relative"}); //div
            _li.parent().css({margin: "0", padding: "0", overflow: "hidden", position: "relative", "list-style": "none"}); //ul
            _li.css({position: "relative", overflow: "hidden"}); //li
            if(o.dir == "left") _li.css({float: "left"});

            //初始大小
            var _li_size = 0;
            for(var i=0; i<_li.length; i++) {
                _li_size += (o.dir == "left") ? _li.eq(i).outerWidth(true) : _li.eq(i).outerHeight(true);
			}
            //循环所需要的元素
            if(o.dir == "left") _li.parent().css({width: (_li_size*3)+'px'});
            _li.parent().empty().append(_li.clone()).append(_li.clone()).append(_li.clone());
            _li = $('li', this);

            //滚动
            var _li_scroll = 0;
            function goto(){
                _li_scroll += o.width;
                if(_li_scroll > _li_size)
                {
                    _li_scroll = 0;
                    _li.parent().css(o.dir == "left" ? { left : -_li_scroll } : { top : -_li_scroll });
                    _li_scroll += o.width;
                }
                _li.parent().animate(o.dir == "left" ? { left : -_li_scroll } : { top : -_li_scroll }, o.amount);
            }

            //开始
            var move = setInterval(function(){ goto(); }, o.speed);
            _li.parent().hover(function(){
                clearInterval(move);
            },function(){
                clearInterval(move);
                move = setInterval(function(){ goto(); }, o.speed);
            });
        });
    };
})(jQuery);

$("#scroll").imgscroll({
    speed: 40,    //图片滚动速度
    amount: 0,    //图片滚动过渡时间
    width: 1,     //图片滚动步数
    dir: "top"   // "left" 或 "up" 向左或向上滚动
});
 
//分类
var filterList = $('.exchange-filter dl');
filterList.each(function(){
    var _this = $(this),
        _len = _this.find('.J_menu').find('li').length;
    if (_len > 9) {
        _this.find('dd').append('<a class="more open" href="javascript:void(0);">收起</a>');
    }
});
if (filterList.find('.J_menu').find('a').eq(0).attr('class') == 'current') {
    $('.exchange-submenu').show();
} else {
    $('.exchange-submenu').hide();
}
filterList.on('click', 'a.more', function(e){
    e.preventDefault();
    var _this = $(this),
        _html = _this.html();
    _this.toggleClass('open');
    _html == '收起' ? _this.html('展开') : _this.html('收起');
    _this.parent().find('ul').eq(0).toggleClass('hide');
});


/*
* 分享
* */
/*var shareUitl = {
    init: function(state, exTit, exImg){
        var type = state.attr('attr-type'),
            sync = state.attr('attr-sync'),
            _url = window.location,
            _content = '#帮5买帮豆兑换中心#我在帮5买兑换中心用帮豆轻松兑换了一个【' + exTit + '】呢！完成任务即能赚取大把帮豆，帮豆还可兑换100元话费，苹果电脑等大奖哦！快点来赚帮豆换奖品吧！' + _url;
        $.ajax({
            url: 'http://ucenter.b5m.com/user/third/getToken.htm',
            type: 'POST',
            async: false,
            dataType: 'jsonp',
            jsonp:'jsonpCallback',
            data: {
                state: type
            },
            success: function(result){
                if (result && result.ok == true) {
                    popWindowShareFed.popwindow({
                        title: '帮5买',
                        content: _content,
                        href: _url,
                        pic: [exImg]
                    }, function(res){
                        var url = 'http://ucenter.b5m.com/user/third/share.htm';
                        var data = {
                            state: type,
                            summary: res['content'],
                            title: res['title']
                        };
                        if (type == 1) {
                            data.url = res['href'];
                            if (sync) {
                                data.nswb = sync;
                            }
                        } else {
                            data.summary = data.summary + "  " + res['href'];
                        }
                        if (res['pic']) {
                            data.images = res['pic'];
                        }
                        $.ajax({
                            type: "POST",
                            url: url,
                            data: data,
                            success: function(httpObj){
                                if (httpObj.ok) {
                                    popWindowShareFed.popwindowClose();
                                    alert('分享成功');
                                } else {
                                    alert('分享失败，请稍后分享');
                                }
                            }
                        });
                    });
                } else if (result.ok == false) {
                    var newWindow = window.open();
                    setTimeout(function(){
                        newWindow.location = 'http://ucenter.b5m.com/user/third/login/auth.htm?type=' + type + '&refererUrl=&api=/setToken';
                    }, 10);
                }
            }
        });
    }
};

$('.J_share span').on('click', function(){
    var exTit, exImg;
    if ($obj != null) {
        exTit = $obj.find('h2').text();
        var coupons = $obj.find('.coupons').length;
        coupons > 0 ? exImg = 'http://staticcdn.b5m.com/images/exchange/coupon.jpg' : exImg = $obj.find('.ex-detail-pic').find('img').attr('src');
    }
    var _self = $(this);
    shareUitl.init(_self, exTit, exImg);
});*/
