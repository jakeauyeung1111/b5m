$(function(){
	/*3g top s*/
	$(".tp-g-header").hover(function() {
		$(this).find('.bg,.text').stop(true,true).fadeIn();
	}, function() {
		$(this).find('.bg,.text').stop(true,true).fadeOut();
	});
	/*3g top e*/
    if ($('.top-class').length) {
        fixDom('.top-class','topic-fix');
    };
    function fixDom(obj,className) {
        var dom = $(obj),
            h = $(obj).offset().top,
            isIE6 = window.XMLHttpRequest ? false : true;
        if (!dom.length) return false;
        if (!isIE6) {
            $(window).on('scroll.zdm', function() {

                if ($(window).scrollTop() >= h) {
                    dom.addClass(className);
                } else {
                    dom.removeClass(className);
                }
            });
        }
    }
	/*3g floor s*/
    var $allNum=$(".topic-wl").length;
    var $liWrap=$("#float-layer");
    var $conTopArr=[];
    $liWrap.height($allNum*60+"px");
    if($allNum>0){
        for(var i=0; i<$allNum;i++){
            var $tit=$("#cont-layer"+(i+1)).find('h2.tit').text();
            var $conText = "<li class='ly"+(i+1)+"'><em>"+(i+1)+"F</em><span>"+$tit+"</span></li>";
            $liWrap.find('ul').append($conText);
            var $contTop=$("#cont-layer"+(i+1)).position().top-100;
            $conTopArr.push($contTop);
            $liWrap.find("li").eq(i).css("top",i*60+"px");
        }
        
    }else{
        $liWrap.hide();
    }
    
    var offsetArr = [];
    $('.topic-wl').each(function(index, el) {
        var $this = $(this),
            offsetTopNum = 0,
            winH = $(window).height();
        offsetTopNum = $this.offset().top - parseInt(winH/2);
        offsetArr.push(offsetTopNum);
    });

	$(window).scroll(function(e) {
		var $scrTop = document.documentElement.scrollTop || document.body.scrollTop || 0,
            $floor = $("#float-layer").find('li');
        if(!$scrTop){
            $floor.removeClass('cur');
            return false;
        }
        for(var i in offsetArr){
            if($scrTop > offsetArr[i]){
                $floor.eq(i).addClass('cur').siblings('li').removeClass('cur');
            }
        }
	});
	$("#float-layer").find('li').on('click',function(){
		var $tIndex=$(this).index()+1;
		$('body,html').stop(true,false).animate({scrollTop:$("#cont-layer"+$tIndex).position().top},500);
	})
	/*3g floor e*/
	var $picCont=$("#al-pic-cont");
	var $this = $('#al-pic-main ul');
    var $LiL = $this.find('li').length;
    var $LiW = $this.find('li').width()+20;
    $this.width($LiL*$LiW+"px");

    $picCont.hover(function() {
    	if($LiL>4){
    		$picCont.find('span').children('a').show();
    	}else{
    		$picCont.find('span').children('a').hide();
    	}
    }, function() {
    	$picCont.find('span').children('a').hide();
    });
    $('.left-btn').click(function(){
        if(!$this.is(":animated")){
            $this.animate({
                left: '+=230'
            }, 300,function(){
                $this.css('left',0).find('li:last').prependTo($this);
            });
        }
    });
    $('.right-btn').click(function(){
        if(!$this.is(":animated")){
            $this.animate({
                left: '-=230'
            }, 300,function(){
                $this.css('left',0).find('li:first').appendTo($this);
            });
        }
    });

    /*dy tit*/
    $(".tpdy-cent").find("li.img").hover(function(){
        $(this).children('.price').show();
    },function(){
        $(this).children('.price').hide();
    })
});