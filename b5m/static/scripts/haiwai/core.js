(function(window,$) {

    //cookie
    window.NS.man('functions.cookies');
    window.NS.functions.cookies = {
        set:function(name,value) {
            var argv = arguments;
            var argc = arguments.length;
            var expires = (argc > 2) ? argv[2] : null;
            var path = (argc > 3) ? argv[3] : '/';
            var domain = (argc > 4) ? argv[4] : null;
            var secure = (argc > 5) ? argv[5] : false;
            document.cookie = name + "=" + escape(value) +
                ((expires == null) ? "" : ("; expires=" + expires.toGMTString())) +
                ((path == null) ? "" : ("; path=" + path)) +
                ((domain == null) ? "" : ("; domain=" + domain)) +
                ((secure == true) ? "; secure" : "");
        },
        get:function(name) {
            var arg = name + "=";
            var alen = arg.length;
            var clen = document.cookie.length;
            var i = 0;
            var j = 0;
            while (i < clen) {
                j = i + alen;
                if (document.cookie.substring(i, j) == arg)
                    return this.getCookieVal(j);
                i = document.cookie.indexOf(" ", i) + 1;
                if (i == 0)
                    break;
            }
            return null;
        },
        clear:function(name) {
            if (this.get(name)) {
                var expdate = new Date();
                expdate.setTime(expdate.getTime() - (86400 * 1000 * 1));
                this.set(name, "", expdate);
            }
        },
        getCookieVal:function(offset) {
            var endstr = document.cookie.indexOf(";", offset);
            if (endstr == -1) {
                endstr = document.cookie.length;
            }
            return unescape(document.cookie.substring(offset, endstr));
        }
    };


/* ************ 海外馆通用函数 ********************** */

    window.NS.man('haiwai');


    //public slider
    window.NS.haiwai.slider = function(elem,w) {

        var elem = elem,
            container = elem.find('.container'),
            prev =elem.find('.prev'),
            next = elem.find('.next'),
            len = container.find('li').length,
            width = w,
            index =0;

        container.find('ul').width(width*len);

        if(len<=1) {
            next.addClass('last');
        }

        prev.click(function() {
            if(index > 0) {
                index --;
                container.animate({scrollLeft:index*width});
                if(index === 0) {
                    $(this).addClass('first');
                }
                if(index!=len-1) {
                    next.removeClass('last');
                }
            }
        });

        next.click(function(){
            if(index< len-1) {
                index ++;
                container.animate({scrollLeft:index*width});
                if(index === len-1) {
                    $(this).addClass('last');
                }
                if(index!=0) {
                    prev.removeClass('first');
                }
            }
        });

    }

    //public menu
    window.NS.haiwai.menu = function() {

        var $menu = $('#J_menu'),
            $trigger_menu = $menu.children('li'),
            $subMenu = $menu.find('.sub_item'),
            w_len = $menu.find('.sub-wp').outerWidth(true),
            timer;
        $menu.hover(function(){
            $subMenu.stop(true,true).animate({
                display:'block',
                width:w_len
            },{
                duration:300
            })
        },function(){
            $subMenu.stop(true,true).animate({
                display:'none',
                width:0
            },{
                duration:200
            })
        });
        $trigger_menu.hover(function(){
            var $this = $(this),
                $subContent = $this.find('.sub-in'),
                $subMenuRel = $this.find('.sub_item');
            $subMenu.hide();
            $subContent.show();
            $subMenuRel.show();
            $this.addClass('open');
            $subMenuRel.find('img').trigger('lazyload');
        },function(){
            var $this = $(this),
                $subMenuRel = $this.find('.sub_item'),
                $subContent = $this.find('.sub-in');
            $this.removeClass('open');
            $subMenuRel.hide();
            $subContent.hide();
        });

    };
    window.NS.haiwai.menu();


})(window,jQuery);