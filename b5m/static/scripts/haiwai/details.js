(function($){
	
	$(function(){
		function scrollPic(obj)
		{
			if(!obj) return false;
			var oParent = $(obj),
				oPrevBtn = oParent.find('#prev'),
				oNextBtn = oParent.find('#next'),
				oPicNum = oParent.find('#pic-num'),
				oArea = oParent.find('.show-area'),
				oAreaList = oParent.find('.show-pic-list'),
				aLiLen = oAreaList.find('li').length,
				oLiWidth = parseInt(oAreaList.find('li').outerWidth(true),10),
				aEffLiLen = Math.ceil(parseInt(oArea.css('width'),10) / oLiWidth),
				iNow = 0,
				timer = null;
			
			oPicNum.html('<strong>'+(iNow+1)+'</strong>'+'/'+aLiLen);
			oAreaList.css({width:(oLiWidth*aLiLen)+'px'});

			if(aLiLen>aEffLiLen)
			{
				oNextBtn.addClass('active');
			}

			function startMove()
			{
				if(aLiLen>aEffLiLen)
				{
					oAreaList.animate({left:-iNow*oLiWidth});
				}

				if(iNow==(aLiLen-aEffLiLen))
				{
					oNextBtn.removeClass('active');
				}
				else
				{
					oNextBtn.addClass('active');
				}
				if(iNow>0)
				{
					oPrevBtn.addClass('active');
				}
				else
				{
					oPrevBtn.removeClass('active');
				}
			}

			function autoPlay()
			{
				iNow ++;
				if(iNow==aLiLen)
				{
					iNow = 0;
				}
				startMove();
				oPicNum.html('<strong>'+(iNow+1)+'</strong>'+'/'+aLiLen);
			}

			timer = setInterval(autoPlay,5000);

			oParent.hover(function(){
				clearInterval(timer);
			},function(){
				timer = setInterval(autoPlay,5000);
			});

			oNextBtn.click(function(){
				if(iNow<(aLiLen-aEffLiLen))
				{
					iNow ++;
					startMove();
					oPicNum.html('<strong>'+(iNow+1)+'</strong>'+'/'+aLiLen);
				}
			});

			oPrevBtn.click(function(){
				if(iNow>0)
				{
					iNow --;
					startMove();
					oPicNum.html('<strong>'+(iNow+1)+'</strong>'+'/'+aLiLen);
				}
			});
		}
		scrollPic('#show-pic');

		function toggleProd(){
			$('.opt').each(function(){
				var $this = $(this),
					flag = true;

				$this.click(function(){
						$target_box = $this.parent('.tab-shop-item'),
						target_box_h = $target_box.css('height','auto').height();
						target_ls_h = $target_box.find('li').outerHeight(),
						txt = $this.html();
					if(flag){
						$target_box.css('height',target_ls_h).stop(true,true).animate({'height':target_box_h});
						flag = false;
						txt = txt.replace('展开','收起');
						$this.html(txt);
						$this.addClass('close');
					}else{
						$target_box.stop(true,true).animate({'height':target_ls_h-1});
						flag = true;
						txt = txt.replace('收起','展开');
						$this.html(txt);
						$this.removeClass('close');
					}	
				});
			});
		}
		toggleProd();
		function fixedPos(obj){
			if (!obj) return false;
		    var iTarget = $(obj).offset().top;
		    $('html,body').animate({scrollTop: iTarget}, 'fast');
		}
		$('#J_fixed').on('click','li',function(){
			$(this).addClass("cur").siblings('li').removeClass('cur');
			fixedPos("#J_fixed");
			var index = $(this).index();
			$('.tab-box').eq(index).show().siblings().hide();
		});
		// 定位Tab信息
		$('.target-tab').on('click',function(){
			var $target = $('#'+$(this).data('type'));
			targetTab($target);
		});
		function targetTab(obj){
			if(!obj) return;
			obj.trigger('click');
		}

	    //弹出框
	    var searchFed = {
	        ie6: typeof document.body.style.maxWidth == 'undefined',
	        showDialog: function(content, title){
	            var _title = title,
	                _content = $(content),
	                _html = '',
	                doc_h = $(document).height(),
	                win_w = $(window).width(),
	                win_h = $(window).height(),
	                scroll_top = $(window).scrollTop(),
	                $dialog_mask = $('.dialog-mask'),
	                _this = searchFed;
	            if (!_content) return false;
	            if (!_title.length) {
	                _html += '<div class="dialog"><a href="javascript:void(0);" class="dialog-close" title="关闭">X</a><div class="dialog-body"></div></div>';
	            } else {
	                _html += '<div class="dialog"><h3>';
	                _html += _title;
	                _html += '</h3><a href="javascript:void(0);" class="dialog-close" title="关闭">X</a><div class="dialog-body"></div></div>';
	            }
	            //open the dialog
	            if (!$dialog_mask.length) {
	                $dialog_mask = $('<div class="dialog-mask"></div>').css('height', doc_h).appendTo('body');
	                $(_html).appendTo('body');
	                $('.dialog-body').append(_content);
	            } else {
	                $dialog_mask.show();
	                $('.dialog').show();
	            }
	            if (_this.ie6) {
	                $('.dialog').css('width', 560);
	            }
	            var $dialog = $('.dialog'),
	                $dialog_close = $dialog.find('.dialog-close'),
	                dialog_w = $dialog.outerWidth(),
	                dialog_h = $dialog.outerHeight(),
	                pos_x,
	                pos_y;
	            pos_x = parseInt((win_w - dialog_w) / 2);
	            pos_y = parseInt((win_h + scroll_top - dialog_h) / 2);
	            $dialog.css({
	                top: pos_y,
	                left: pos_x
	            });

	            var $fav_sel = $('.dialog-fav-sel');
	            $fav_sel.on('click', function(e){
	                e.stopPropagation();
	                var $this = $(this),
	                    $opts = $this.find('.dialog-fav-opt');
	                $this.toggleClass('dialog-fav-sel-on');
	                $opts.toggle();
	                $opts.find('span').mouseenter(function(){
	                    $(this).addClass('on');
	                }).mouseleave(function(){
	                    $(this).removeClass('on');
	                }).click(function(){
	                    $this.find('.fav-default').text($(this).text());
	                });
	                $(document).on('click',function(){
	                    $opts.hide();
	                });
	            });

	            $dialog_close.on('click', function(e){
	                e.preventDefault();
	                _this.hideDialog($dialog, $dialog_mask);
	            });
	        },
	        hideDialog: function($dialog, $mask){
	            if (!$dialog || !$mask) return false;
	            $dialog.remove();
	            $mask.remove();
	        }
	    };


	    var dialogLogin = (function(){
	    	var dialog = null;

	    	function Dialog(){
	    		var html = '';
	    			html = '<div class="dialog-login">';
				    html += '<div class="dialog-login-in">';
					html += '<h3>为了能及时更新您收藏的商品，需要登录网站</h3>';
					html += '<div class="login-register-mod">';
					html += '<div class="login-mod">';
					html += '<p>已有账号</p>';
					html += '<a href="#" class="dialog-btn btn-login">去登录</a>';
					html += '</div>';
					html += '<div class="register-mod">';
					html += '<p>暂无账号</p>';
					html += '<a href="#" class="dialog-btn btn-register">去注册</a>';
					html += '</div>';
					html += '</div>';
					html += '</div>';
					html += '<a href="#" class="dialog-login-close">x</a>';
					html += '</div>';
				return html;
			}

			return {
				getDialog:function(){
					return dialog || (dialog = $('body').append(Dialog()));
				}
			}
	    })();

	    //添加收藏
	    $('#J_add_favo').on('click',function(e){
	        var isLogin = Cookies.get('login') === 'true' && Cookies.get('token');
	        e.preventDefault();
	        //是否登录
	        if(!isLogin){
			    var dialog = dialogLogin.getDialog();

			    if(dialog){
			    	$('.dialog-login').show();
			    }
			    
			    $('.dialog-login-close').on('click',function(e){
			    	$('.dialog-login').hide();
			    	e.preventDefault();
			    });
	        }else{
	            searchFed.showDialog(
	                '<div class="fav-handle">' +
	                    '<i class="fav-suc"></i>' +
	                    '<span class="fav-status">成功加入<a href="#">收藏夹</a></span>' +
	                    '<strong class="dialog-fav-sel"><em class="fav-default">请选择分类</em><em class="fav-arr"><i></i></em><b class="dialog-fav-opt"><span>默认分类</span><span>xxx分类</span><span>xxx分类</span><span>xxx分类</span></b></strong>' +
	                    '</div>' +
	                    '<div class="fav-recommend"><h6><a href="#">更多推荐</a>喜欢此宝贝的还喜欢</h6><ul class="clear-fix"><li><a href="#"><img width="110" height="110" src="http://pic.junli.cc/110x110/ccc" /></a><p><a href="#">正品韩版秋冬大衣外套 欧美格子毛</a></p><p>￥<span>399.00</span></p></li><li><a href="#"><img width="110" height="110" src="http://pic.junli.cc/110x110/ccc" /></a><p><a href="#">正品韩版秋冬大衣外套 欧美格子毛</a></p><p>￥<span>399.00</span></p></li><li><a href="#"><img width="110" height="110" src="http://pic.junli.cc/110x110/ccc" /></a><p><a href="#">正品韩版秋冬大衣外套 欧美格子毛</a></p><p>￥<span>399.00</span></p></li><li><a href="#"><img width="110" height="110" src="http://pic.junli.cc/110x110/ccc" /></a><p><a href="#">正品韩版秋冬大衣外套 欧美格子毛</a></p><p>￥<span>399.00</span></p></li></ul></div>', '');
	        }

	    });	
	});
})(jQuery);
