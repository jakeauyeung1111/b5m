(function(window,$) {

  var $searchClass = $('.J_search_class'),
      $elUl = $searchClass.find('ul'),
      $elCur = $searchClass.find('em'),
      $elItem = $searchClass.find('a');

    $searchClass.on({
        mouseenter:function() {
            $(this).addClass('on');
        },
        mouseleave:function() {
            $(this).removeClass('on');
        }
    });

    $elItem.each(function(i,n){

        $(n).click(function() {
            $elCur.text($(n).text());
            $searchClass.trigger('mouseleave');
            $elUl.find('.cur').removeClass('cur');
            $(n).addClass('cur');

            $("#search_type").val(n.id==='search-class-item' ? 'item' : 'info');

        });

    });



    //placeholder
    $('.header-search-key[placeholder]').placeholder({
        useBrowserPlaceholder:true,
        className: 'filter-text-placeholder'
    }).on('focusin', function() {
          $(this).addClass('filter-text-focusin');
    }).on('focusout', function() {
          $(this).removeClass('filter-text-focusin');
    });



    $('.page-input input').on('keyup', function(e) {
        var mpage = parseInt($('.pagenum').attr('page'));
        this.value = this.value.replace(/\D/g, "");
        if(this.value >mpage){
            this.value = mpage;
        }
    });







})(window,jQuery);