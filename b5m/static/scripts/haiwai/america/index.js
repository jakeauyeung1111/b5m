var americaFed = window.americaFed || {};

americaFed.indexFun = function() {
  this.indexInit();
};

americaFed.indexInit = function() {

    // 检测IE6支持hover
    if(typeof document.body.style.maxHeight === 'undefined') {
        $('.column-con').find('span').css('opacity',0.8);
        $('.column-con').find('a').on({
            mouseenter:function() {
                $(this).find('span').show();
            },
            mouseleave:function() {
                $(this).find('span').hide();
            }
        });
    }
    // TAB标签切换
  $(document).b5mTab({
        tabbox:$('#J_setTabANav>li'),
        tabcon:$('#J_setTabABox>div'),
        tabhover:'hover',
        tabconhover:'hover'

    });
    $(document).b5mTab({
        tabbox:$('#J_setTabBNav>li'),
        tabcon:$('#J_setTabBBox>div'),
        tabhover:'hover',
        tabconhover:'hover'
    });

    // 图片无缝播放
    $('.jcarousel').jcarousel({wrap: 'circular'}).jcarouselAutoscroll({
        interval: 3000,
        autostart: true
    });
    $('.jcarousel-pagination')
        .on('jcarouselpagination:active', 'a', function() {
            $(this).addClass('active');
        })
        .on('jcarouselpagination:inactive', 'a', function() {
            $(this).removeClass('active');
        })
        .jcarouselPagination();

        $('.jcarousel-list').jcarousel({wrap: 'circular'}).jcarouselAutoscroll({
        interval: 6000,
        autostart: true
    });
    $('.jcarousel-pagination-list')
        .on('jcarouselpagination:active', 'a', function() {
            $(this).addClass('active');
        })
        .on('jcarouselpagination:inactive', 'a', function() {
            $(this).removeClass('active');
        })
        .jcarouselPagination();

        // tab切换懒加载
        // $('.column-tab-title ul li').hover(function(){
        //       $(this).parents('.column-tab').find('img').trigger('lazyload');
        // })



};
