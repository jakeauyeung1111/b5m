window.koreaComment = window.koreaComment || {};

(function(window,$) {

    var container = $('.commentad'),
        el_good = container.find(".good"),
        el_nogood = container.find('.nogood'),
        el_stars = container.find('.star'),
        el_starsArr = [],
        el_vcode = $("#txt_vcode"),
        el_textarea = container.find('textarea'),
        el_uploadimg = container.find('.upload-img'),
        el_submit = container.find('.submit'),
        el_reset = container.find('.reset'),
        el_uploadshow = $('.upload-img-show'),
        el_msg = container.find('.msg'),

        el_uploadimgbox = $('.upload-img-box'),
        el_close = el_uploadimgbox.find('.close'),
        el_upload = el_uploadimgbox.find('.btn-upload'),
        el_file =el_uploadimgbox.find('.file'),
        el_form = el_uploadimgbox.find('form'),
        el_loading =el_uploadimgbox.find('.loading');

    window.koreaComment = {

        //初始化
        init:function() {

            //handlers
            el_good.click(function() {
                $(this).addClass('cur');
                $("#good").val(1);
                $("#bad").val(0);
                el_nogood.removeClass('cur');
            });

            el_nogood.click(function() {
                $(this).addClass('cur');
                $("#good").val(0);
                $("#bad").val(1);
                el_good.removeClass('cur');
            });

            el_submit.click(this.submit);
            el_reset.click(this.reset);

            el_stars.each(function(i) {
                var star = new b5mKorea.Star(this);
                star.bindText(container.find('.c-gray').eq(i));
                el_starsArr.push(star);
            });

            //upload img
            this.uploadImg();
        },


        //上传图片功能
        uploadImg:function() {

            var _this = this;

            el_uploadimgbox.click(function(e) {
                e.stopPropagation();
            });

            el_uploadimg.click(function(e) {

                if(el_uploadshow.find('img').length===4) {
                    el_msg.eq(2).show();
                    return false;
                }

                e.stopPropagation();

                el_uploadimgbox.show().css({
                    left:el_uploadimg.offset().left +45,
                    top:el_uploadimg.offset().top + 18
                });

                $(document).one('click',function() {
                    el_uploadimgbox.hide();
                });
            });

            el_close.click(function() {
                el_uploadimgbox.hide();
            });

            el_file.change(function() {
                el_loading.addClass('on');
                el_form.submit();
            });

           // el_upload.click(function() {
            //    el_file.click();
           // });

            el_uploadshow.on('click','a',function() {
                var _this = this;
                var url = $(this).parent().find("input").val();
                $.ajax({
                    url: "/portal.php?mod=portalcp&ac=comment",
                    data: 'op=delete_img&url='+url,
                    type: 'POST',
                    dataType: 'html',
                    success: function(data){
                        $(_this).parent().detach();
                        el_msg.eq(2).hide();
                    }
                });

            });

        },

        //提交表单
        submit:function() {
            $.ajax({
                url: "/portal.php?mod=portalcp&ac=comment",
                data: 'op=submit_code&vcode='+el_vcode.val(),
                type: 'POST',
                dataType: 'html',
                success: function(data){
                    if (data === "success") {
                        if($.trim(el_textarea.val()) === '') {
                            el_msg.eq(0).show();
                            el_msg.eq(1).hide();
                            return false;
                        }
                        $('#cform').submit();
                    } else {
                        el_msg.eq(1).html(data).show();
                        if (data == "请先登录") {
                            window.location.href = "http://ucenter.b5m.com/forward.htm?method=/user/user/login&loginReferer="+ encodeURIComponent(window.location.href);
                        }
                    }
                }
            });
            return false;
        },

        //重置功能
        reset:function() {
            el_good.addClass('cur');
            el_nogood.removeClass('cur');
            el_textarea.val('');
            el_uploadshow.empty();
            for(var i=0;i<el_starsArr.length;i++) {
                el_starsArr[i].reset(3);
            }
            el_msg.eq(1).html("");
        },

        //图片上传成功回调
        uploadImgComplete:function(url, message) {
            if (message === 'success') {
                el_uploadshow.append('<div><img src="/data/attachment/'+ url +'" width="100" height="100" alt=""/><a href="javascript:void(0)">删除</a><input type="hidden" value="'+url+'" name="images[]" /></div>');
                el_loading.removeClass('on');
                el_uploadimgbox.hide();
                el_msg.eq(1).html("").show();
            } else {
                el_loading.removeClass('on');
                el_uploadimgbox.hide();
                el_msg.eq(1).html(message).show();
            }
        }
    };

    window.koreaComment.init();

})(window,jQuery);