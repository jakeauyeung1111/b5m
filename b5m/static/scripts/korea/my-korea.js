(function($){
    window.go = function(url, pages, id, isstatic) {
        var index = document.getElementById(id).value;
        if (index < 1) {
            index = 1;
        } else if (index > pages) {
            index = pages;
        }
        if (isstatic > 0) {
            url += "-"+index+".html";
        } else {
            url += "page="+index;
        }
        window.location.href = url;     //@贾诩 2013-08-15
    };

    window.replacevcode = function(id) {    //@贾诩 2013-08-23
        setTimeout(function() {
            $('#'+id).attr('src',"/api/vcode.php?r="+Math.random());
        },1);
    }
    
    window.iscode = function(value) {   //@贾诩 2013-08-23
        var url = "/api/vcode.php?v="+value;
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'html',
            success: function(data) {
                if (data < 1) {
                    alert("验证码错误!");
                }
            }
        });
    }
    
    window.add_addition = function() {
        var value = $("#sel_addition").val();
        var text = $("#sel_addition").find("option:selected").text();
        if (value > 0) {
            var content = "<li>"+ text +" <input style=\"width:200px;\" type=\"text\" name=\"addition_value[]\" />&nbsp;&nbsp;<input type=\"hidden\" value='' name=\"gaddid[]\" /><input type=\"hidden\" value='" + value +"' name=\"addition_id[]\" /><input type=\"hidden\" value='" + text + "' name=\"addition_text[]\" /><a href=\"javascript:;\" onClick=\"javascript:this.parentNode.remove();\">删除</a></li>";
            $("#div_addition").append(content);
        }
    }
    
    window.delete_addition = function(obj, id) {
        if (confirm("是否删除这个附加信息？")) {
            $.ajax({
                url: "/admin.php?action=goodsinfo&operation=delete&table=addition",
                data: 'gaddid=' + id,
                type: 'GET',
                dataType: 'html',
                success: function(data){                                
                    obj.parentNode.remove();
                }
            });
        }
                    
    }    
    
    
    
    var $searchClass = $('.J_search_class'),
    $elUl = $searchClass.find('ul'),
    $elCur = $searchClass.find('em'),
    $elItem = $searchClass.find('a');

    $searchClass.on({
        mouseenter:function() {
            $(this).addClass('on');
        },
        mouseleave:function() {
            $(this).removeClass('on');
        }
    });

    $elItem.each(function(i,n){

        $(n).click(function() {
            $elCur.text($(n).text());
            $searchClass.trigger('mouseleave');
            $elUl.find('.cur').removeClass('cur');
            $(n).addClass('cur');

            $("#search_type").val(n.id==='search-class-item' ? 'item' : 'info');

        });

    });
    
    $(".hw-data-news").find('dl:first').children("dd").show();
    $(".hw-data-news").find('dl').hover(function(){
        $(".hw-data-news").find('dd').hide();
        $(this).children('dd').show();
    });
    
    if (document.cookie.indexOf('kr_credit')) {        
        var name = 'kr_credit'; 
        var cookie_start = document.cookie.indexOf(name);
        var cookie_end = document.cookie.indexOf(";", cookie_start);
        if(cookie_start == -1) {
            return '';
        } else {
            var v = document.cookie.substring(cookie_start + name.length + 1, (cookie_end > cookie_start ? cookie_end : document.cookie.length));
            showPrompt(null, null, '<div id="creditpromptdiv"><i>文章评论</i> <span>帮豆<em>+'+v+'</em></span></div>', 0);  
            var date = new Date();   
            date.setTime(date.getTime() - 10000);   
            document.cookie = name + "=a; expires=" + date.toGMTString();
            setTimeout(function () { hideMenu(1, 'prompt'); }, 3000);
        }
        
    }
    
})(jQuery);

