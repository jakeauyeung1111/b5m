(function($,window) {
    ;(function(){
        $(window).resize(function(event) {
            mediaQuery();
        });
        var mediaQuery = function(){
            //窗口宽度
            var win_w = $(this).width(),
            //body
                b = $(document.body),
            //参考宽度

                width_arr = [1370,1470,1640,1770];

            if(win_w < width_arr[0]){
                b.removeClass().addClass('size1');
            }else if(win_w >= width_arr[0] && win_w < width_arr[1]){
                b.removeClass().addClass('size2');
            }else if(win_w >= width_arr[1] && win_w < width_arr[2]){
                b.removeClass().addClass('size3');
            }else if(win_w >= width_arr[2] && win_w < width_arr[3]){
                b.removeClass().addClass('size4');
            }else{
                b.removeClass().addClass('size5');
            }
        }
        mediaQuery();
    })();

})(jQuery,window);
