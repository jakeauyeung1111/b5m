var koreaDetail = window.koreaDetail = {};
(function($){
    koreaDetail={
        /*zibu 20140303 免费咨询弹出框*/
        mfzxPopFun:function(){
            var mfzxlogMap = new b5mKorea.utils.dialog('map','#mfzxPop',390,580),
                ziSuccesslog = new b5mKorea.utils.dialog('map','.zx-success',260,86),
                ziSuccesslogErr = new b5mKorea.utils.dialog('map','#fssb',260,86);

            $('#mfzx-btn').click(function(){
                var localUrl = window.location.href;
                /*是否登录*/
                if (Cookies.get('login') != "true") {
                    window.location.href= 'http://ucenter.b5m.com/?loginReferer='+localUrl;
                    return  false;
                }else{
                    mfzxlogMap.open();
                }
                return false;
            });
            $('#mfzxPop').on('click','.zx-btn',function(){
                var _this = koreaDetail,
                    verify = true,
                    nameValur = $('#mfzxPop').find('#name').val(),
                    mobileValur = $('#mfzxPop').find('#phone').val(),
                    emailValur = $('#mfzxPop').find('#email').val(),
                    contValur = $('#mfzxPop').find('#content').val(),
                    qqValur = $('#mfzxPop').find('#qq').val(),
                    emailValur1 = $('#mfzxPop').find('#email2').val(),
                    weixinValur1 = $('#mfzxPop').find('#weixin').val(),
                    aidValue = $('#mfzxPop').find('#aid').val(),
                    titleValue = $('#mfzxPop').find('#title').val(),
                    receiveValue = $('#mfzxPop').find('#receive_email').val(),
                    errorTip = $('#mfzxPop').find('.error-tip');

                /*姓名不能为空*/
                if(nameValur == ''){
                    errorTip.html('姓名不能为空');
                    return verify = false;
                };
                /*手机号码验证*/
                if(mobileValur == ''){
                    errorTip.html('手机不能为空');
                    return verify = false;
                }else{
                    var reg = /^0*(13|15|18)\d{9}$/;

                    if(!reg.test(mobileValur)){
                        errorTip.html('手机号格式有误！');
                        return verify = false;
                    };
                };
                /*邮箱验证*/
                if(emailValur == ''){
                    errorTip.html('邮箱不能为空');
                    return verify = false;
                }else{
                    var reg = /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;
                    if(!reg.test(emailValur)){
                        errorTip.html('邮箱格式有误！');
                        return verify = false;
                    };
                };
                /*咨询内容不能为空*/
                if(contValur == ''){
                    errorTip.html('咨询内容不能为空');
                    return verify = false;
                };
                /*答复方式*/
                if(!(qqValur != '' || emailValur1 != '' || weixinValur1 != '')){
                    errorTip.html('答复方式任选一项');
                    return verify = false;
                };
                errorTip.html('');
                if(verify == true){
                    $.ajax({
                        url:'http://korea.b5m.com/api/medical_inquire.php?op=add',
                        type:'post',
                        data:{name:nameValur,phone:mobileValur,email:emailValur,content:contValur,qq:qqValur,email2:emailValur1,weixin:weixinValur1,aid:aidValue,title:titleValue,receive_email:receiveValue},
                        success:function(data){
                            var resultCode = $.parseJSON(data)['code'];
                            if(resultCode == 0){
                                mfzxlogMap.onClose();
                                ziSuccesslog.open();
                            }else{
                                mfzxlogMap.onClose();
                                ziSuccesslogErr.open();
                            };
                        }
                    });
                }

                $('#mfzxPop').find('.zx-text').val('');
                $('#mfzxPop').find('.zx-area').val('');
            });
        },
        /*免费咨询按钮浮动*/
        floatMfzxBtn:function(){
            var sorTop = 580,
                flog = true;

            $(window).scroll(function(){
                var winScrollTop = $(window).scrollTop(),
                    $btnLayer = $('.mfzx-info');

                    if(winScrollTop > sorTop){
                        if(flog == true){
                            $btnLayer.show();
                        };
                    }else{
                        $btnLayer.hide();
                    };

                $btnLayer.off().on('click','.close',function(){
                    $btnLayer.hide();
                    flog = false;
                });

                $btnLayer.on('click','#mfzx-btn1',function(){
                    $('#mfzx-btn').trigger('click');
                });
            });

        }
    };
    koreaDetail.loginTipFun=function(){
        var loginlogMap = new b5mKorea.utils.dialog('map','.detail-login-tip',300,120),
            _this = koreaDetail.loginTipFun;

        _this.loginPop=function(){
            loginlogMap.open();
        }
        $('.detail-login-tip').on('click','.btn',function(){
            var localUrl = window.location.href;
            /*是否登录*/
            if (Cookies.get('login') != "true") {
                window.location.href= 'http://ucenter.b5m.com/?loginReferer='+localUrl;
                return  false;
            }else{
                loginlogMap.onClose();
            }
            return false;
        });
    }
    koreaDetail.mfzxPopFun();
    koreaDetail.loginTipFun();
    koreaDetail.floatMfzxBtn();


    /*点击评论提交按钮弹出代金券 s*/
    /*$('.commentad .submit').click(function(){
        $('.commentad ').find('.djq-warp').show();
    });*/
    $('.djq-warp').find('.close').click(function(){
        $(this).parent('.djq-warp').hide();
    });
    /*点击评论提交按钮弹出代金券 e*/

   $(".hw-data-news").find('dl:first').children("dd").show();
    $(".hw-data-news").find('dl').hover(function(){
        $(".hw-data-news").find('dd').hide();
        $(this).children('dd').show();
    });

    allLookPic();
    dataPicSlide();

    var dialogMap = new b5mKorea.utils.dialog('map','.dialog-map',870,600);
    var mapLink = $('.map .view a').click(function() {
        dialogMap.open();
    });


    var dialogImage = new b5mKorea.utils.dialog('image','.dialog-image',360,320);
    b5mKorea.utils.slider($('.dialog-image .dialog-image-slider'),300);
    $('.item-img-dialog').click(function() {
        dialogImage.open();
    });


    var dialogImages = new b5mKorea.utils.dialog('images','.dialog-images',360,320);

    /*$('.item-imgs-dialog img').click(function() {
        var listStr='<div class="dialog-image-slider"><div class="container"><ul class="cf">',
            items = $(this).parents('.item-imgs-dialog');

        items.find('img').each(function() {
            listStr +='<li><div class="item"><img src="'+ this.src +'" alt="" width="300" height="300" /></div></li>';
        });
        listStr += '</ul></div><a class="prev first" href="javascript:void(0)"><</a><a class="next" href="javascript:void(0)">></a></div>';
        dialogImages.open(function() {
            this.container.html(listStr);
            b5mKorea.utils.slider(this.container.find('.dialog-image-slider'),300);
        });
    });*/
    if(typeof window.google !== 'undefined') {
      google.maps.event.addDomListener(document.getElementById('link-view-map'), 'click',function() {
          setTimeout(function() {
              initializeMap.call(document.getElementById('map-canvas-large'));
          },0);

      });
   }

function allLookPic(){
  var $picCont=$("#al-pic-cont");
  var $this = $('#al-pic-main ul');
    var $LiL = $this.find('li').length;
    var $LiW = $this.find('li').width()+20;
    $this.width($LiL*$LiW+"px");
    $("#al-pic-cont").on('click','.left-btn',function(){
        if(!$this.is(":animated")){
            $this.animate({
                left: '+=212'
            }, 300,function(){
                $this.css('left',0).find('li:last').prependTo($this);
            });
        }
    });
    $("#al-pic-cont").on('click','.right-btn',function(){
        if(!$this.is(":animated")){
            $this.animate({
                left: '-=212'
            }, 300,function(){
                $this.css('left',0).find('li:first').appendTo($this);
            });
        }
    });
}
function dataPicSlide(){
  var iNow=0,
      nextUrl=$("#next_pic").val();
      $this=$("#data-slide"),
      liW=$this.find(".pic-main li").width(),
      allNumb=$this.find(".pic-main li").length;
  $this.on("click",".right-btn",function(){
    if(iNow<allNumb-1){
      iNow++;
    }else{
      window.location.href=nextUrl;
    };
    picSlide();
  });
  $this.on("click",".left-btn",function(){
    if(iNow>0){
      iNow--;
    }else{
      iNow=allNumb-1;
    };
    picSlide();
  });
    $(".pic-main").find("img").click(function(){
        $this.find(".right-btn").trigger("click");
    });
  function add0(numb){
    if(numb<10){
      numb="0"+numb;
    };
    return numb;
  };
  function picSlide(){
    var numNow=iNow+1,
        sLi=$this.find(".pic-main ul li");
    $this.css("height",sLi.eq(iNow).height());
    $this.find(".now-numb").text(add0(numNow));
    $this.find(".all-numb").text(add0(allNumb));
    sLi.eq(iNow).show().siblings("li").hide();
  };
  picSlide();
};

})(jQuery);