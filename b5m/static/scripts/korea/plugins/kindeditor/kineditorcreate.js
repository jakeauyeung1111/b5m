var editor;
var editorObj = {
  loaded:false,
  ctrlBtn:null,
  disabled:false,
  elements:{}
};

$.fn.createEditor = function(options) {
  	var settings = $.extend({
 	ctrlBtn:null,
	disabled:false
  },options || {});
  
  editorObj.ctrlBtn = settings.ctrlBtn ? settings.ctrlBtn : null;
  editorObj.disabled = settings.disabled ? true : false;
  
  var container = $(this);
  KindEditor.ready(function(K) { 
    newEditor(container,K);
  });
}
 
function createEditor(item) {
  KindEditor.ready(function(K) {
    newEditor(item,K);
  });
}

function loadEditor(item) {
  $.getScript('../../scripts/korea/plugins/kindeditor/kindeditor-min.js', function() {
      newEditor(item,KindEditor);
	//if(arguments[1]) {
	//  newEasyEditor(item,KindEditor);
	//}else {
	//  newEditor(item,KindEditor);
	//}
  });
}

function loadMultiEditor(item) {
  if(!item.data('loaded')) {
    $.getScript('../../scripts/korea/plugins/kindeditor/kindeditor-min.js', function() {
	  newEditor(item,KindEditor);
    });
	item.data('loaded',true);
  } 
}

function removeEditor() {
	if (editor) {
 	  editor.remove();
	  editor = null;
	}
}

function resetEditor(item) {
  removeEditor();
  loadEditor(item); 
}

function newEditor(item,K) {
     editor = K.create(item, {
	  resizeType : 1,
	  allowPreviewEmoticons : false,
 	  allowFileManager : true,
	  uploadJson : '/kindeditor/asp.net/upload_json.ashx',
      fileManagerJson : '/kindeditor/asp.net/file_manager_json.ashx',
	  afterCreate:function() {
	 
		this.readonly(editorObj.disabled);

		
		editorObj.elements[item[0].id] = this;

 	    if(editorObj.ctrlBtn) {
	      $(this.edit.doc).keypress(function(e) {
			if(e.ctrlKey && e.keyCode==13) {
			  editorObj.ctrlBtn.triggerHandler('click');
			}
		  });
	    }
	  },
	  items : ['fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline','removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist','insertunorderedlist', '|', 'emoticons', 'image', 'link']
	});
	editorObj.loaded = true;
}


function newEasyEditor(item,K) {
     editor = K.create(item, {
	  resizeType : 1,
	  allowPreviewEmoticons : false,
 	  allowFileManager : true,
	  minHeight:100,
	  uploadJson : '/kindeditor/asp.net/upload_json.ashx',
      fileManagerJson : '/kindeditor/asp.net/file_manager_json.ashx',
	  afterCreate:function() {
	 
		this.readonly(editorObj.disabled);

		
		editorObj.elements[item[0].id] = this;
		 
 	    if(editorObj.ctrlBtn) {
	      $(this.edit.doc).keypress(function(e) {
			if(e.ctrlKey && e.keyCode==13) {
			  editorObj.ctrlBtn.triggerHandler('click');
			}	  
		  });
	    }
	  },
	  items : ['fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline']
	});
	editorObj.loaded = true;
}