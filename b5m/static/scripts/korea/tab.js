;(function($){
    $.fn.tab = function(options){
        var opts = $.extend({}, $.fn.tab.defaults, options);
        return this.each(function(){
            var $this = $(this),
                $tab = $this.find('.tab'),
                $item = $tab.find('li'),
                $tabCon = $this.find('.tab-box').find('.tab-con'),
                timer;
            $item.removeClass('current').eq(opts.tabIndex).addClass('current');
            $tabCon.hide().eq(opts.tabIndex).show();

            var tabHandle = function(elem){
                elem.siblings().removeClass('current').end().addClass('current');
                $tabCon
                    .siblings()
                    .animate({
                        opacity: 0
                    }, 0)
                    .hide()
                .end()
                    .eq(elem.index())
                    .show()
                    .animate({
                        opacity: 1
                    }, 0);
                },
                delay = function(elem, time){
                    time ? setTimeout(function(){tabHandle(elem);}, time) : tabHandle(elem);
                },
                start = function(){
                    if (!opts.auto || !opts.speeds) return;
                    timer = setInterval(autoRun, opts.speeds);
                },
                autoRun = function(isPrev){
                    var $current = $item.filter('.current'),
                        $firstItem = $item.first(),
                        $lastItem = $item.last(),
                        len = $item.length,
                        index = $current.index(),
                        item, i;

                    if (isPrev) {
                        index -= 1;
                        item = index === -1 ? $lastItem : $current.prev();
                    } else {
                        index += 1;
                        item = index === len ? $firstItem: $current.next();
                    }

                    i = index === len ? 0 : index;

                    $current.removeClass('current');
                    item.addClass('current');

                    $tabCon
                        .siblings()
                        .animate({
                            opacity: 0
                        }, 0)
                        .hide()
                    .end()
                        .eq(i)
                        .show()
                        .animate({
                            opacity: 1
                        }, 0);
                }

            $item.on(opts.event, function(){
                delay($(this), opts.timeout);
                if (opts.callback) {
                    opts.callback.call($this);
                }
            });

            if (opts.auto) {
                start();
                $this.hover(function(){
                    clearInterval(timer);
                    timer = undefined;
                }, function(){
                    start();
                });
            }
            if (opts.switchBtn) {
                $this.append('<a href="javascript:void(0);" class="tab-prev">&gt;</a><a href="javascript:void(0);" class="tab-next">&lt;</a>');
                var prevBtn = $this.find('.tab-prev'),
                    nextBtn = $this.find('.tab-next');
                prevBtn.click(function(e){
                    e.preventDefault();
                    autoRun(true);
                });
                nextBtn.click(function(e){
                    e.preventDefault();
                    autoRun(true);
                });
            }
        });
    };
    $.fn.tab.defaults = {
        event: 'click',
        delay: 0,
        auto: 0,
        speeds: 0,
        tabIndex: 0,
        callback: null,
        switchBtn: false
    };
})(jQuery);