;(function($){
	
	$(function(){
		// function scrollPic(obj)
		// {
		// 	if(!obj) return false;
		// 	var oParent = $(obj),
		// 		oPrevBtn = oParent.find('#prev'),
		// 		oNextBtn = oParent.find('#next'),
		// 		oPicNum = oParent.find('#pic-num'),
		// 		oArea = oParent.find('.show-area'),
		// 		oAreaList = oParent.find('.show-pic-list'),
		// 		aLiLen = oAreaList.find('li').length,
		// 		oLiWidth = parseInt(oAreaList.find('li').outerWidth(true),10),
		// 		aEffLiLen = Math.ceil(parseInt(oArea.css('width'),10) / oLiWidth),
		// 		iNow = 0,
		// 		timer = null;
			
		// 	oPicNum.html('<strong>'+(iNow+1)+'</strong>'+'/'+aLiLen);
		// 	oAreaList.css({width:(oLiWidth*aLiLen)+'px'});

		// 	if(aLiLen>aEffLiLen)
		// 	{
		// 		oNextBtn.addClass('active');
		// 	}

		// 	function startMove()
		// 	{
		// 		if(aLiLen>aEffLiLen)
		// 		{
		// 			oAreaList.animate({left:-iNow*oLiWidth});
		// 		}

		// 		if(iNow==(aLiLen-aEffLiLen))
		// 		{
		// 			oNextBtn.removeClass('active');
		// 		}
		// 		else
		// 		{
		// 			oNextBtn.addClass('active');
		// 		}
		// 		if(iNow>0)
		// 		{
		// 			oPrevBtn.addClass('active');
		// 		}
		// 		else
		// 		{
		// 			oPrevBtn.removeClass('active');
		// 		}
		// 	}

		// 	function autoPlay()
		// 	{
		// 		iNow ++;
		// 		if(iNow==aLiLen)
		// 		{
		// 			iNow = 0;
		// 		}
		// 		startMove();
		// 		oPicNum.html('<strong>'+(iNow+1)+'</strong>'+'/'+aLiLen);
		// 	}

		// 	timer = setInterval(autoPlay,5000);

		// 	oParent.hover(function(){
		// 		clearInterval(timer);
		// 	},function(){
		// 		timer = setInterval(autoPlay,5000);
		// 	});

		// 	oNextBtn.click(function(){
		// 		if(iNow<(aLiLen-aEffLiLen))
		// 		{
		// 			iNow ++;
		// 			startMove();
		// 			oPicNum.html('<strong>'+(iNow+1)+'</strong>'+'/'+aLiLen);
		// 		}
		// 	});

		// 	oPrevBtn.click(function(){
		// 		if(iNow>0)
		// 		{
		// 			iNow --;
		// 			startMove();
		// 			oPicNum.html('<strong>'+(iNow+1)+'</strong>'+'/'+aLiLen);
		// 		}
		// 	});
		// }
		// scrollPic('#show-pic');
     function detailScrollPic(obj){
        var oParent = $(obj),
            oParentDiv =oParent.find('.small-pic'),
            oParentUl =oParent.find('.pic-ul'),
            oPrevBtn = oParent.find('#prev'),
            oNextBtn = oParent.find('#next'),
            bigPic = oParent.find('.geo-pic'),
            liLengths = oParentDiv.find('li').length,
            picTimer = null;

        oParentDiv.find('li').eq(0).addClass('active');
        oParentDiv.find('li').click(function(){
            var imgSrc = $(this).find('img').attr('src'),
                index = $(this).index();

            picSlide(index,imgSrc);
        });

        function picSlide(index,src){
            oParentDiv.find('li').eq(index).addClass('active').siblings('li').removeClass('active');
            bigPic.find('img').attr('src',src);
        };

        oParentDiv.find('li').hover(function(){
            var imgSrc = $(this).find('img').attr('src'),
                index = $(this).index();


            picTimer = setTimeout(function(){
                picSlide(index,imgSrc);
            },400)

        },function(){
            clearTimeout(picTimer);
        });

        if(liLengths>6){

            oPrevBtn.show();
            oNextBtn.show();
            oNextBtn.click(function(){
                oParentUl.stop(true,true).animate({'left':'-44px'},200,function(){
                    oParentDiv.find('li').eq(0).appendTo(oParentUl);
                    oParentUl.css('left','0');
                });
            });
            oPrevBtn.click(function(){
                oParentUl.stop(true,true).animate({'left':'44px'},200,function(){
                    oParentDiv.find('li').eq(liLengths-1).prependTo(oParentUl);
                    oParentUl.css('left','0');
                });
            });
        }else{
            oPrevBtn.hide();
            oNextBtn.hide();
        };
    };
    detailScrollPic('#show-pic');
    /*zibu 20140226 e*/
		function toggleProd(){
			$('.opt').each(function(){
				var $this = $(this),
					flag = true;

				$this.click(function(){
						$target_box = $this.parent('.tab-shop-item'),
						target_box_h = $target_box.css('height','auto').height();
						target_ls_h = $target_box.find('li').outerHeight(),
						txt = $this.html();
					if(flag){
						$target_box.css('height',target_ls_h).stop(true,true).animate({'height':target_box_h});
						flag = false;
						txt = txt.replace('展开','收起');
						$this.html(txt);
						$this.addClass('close');
					}else{
						$target_box.stop(true,true).animate({'height':target_ls_h-1});
						flag = true;
						txt = txt.replace('收起','展开');
						$this.html(txt);
						$this.removeClass('close');
					}	
				});
			});
		}
		toggleProd();
		function fixedPos(obj){
			if (!obj) return false;
		    var iTarget = $(obj).offset().top;
		    $('html,body').animate({scrollTop: iTarget}, 'fast');
		}
		$('#J_fixed').on('click','li',function(){
			$(this).addClass("cur").siblings('li').removeClass('cur');
			fixedPos("#J_fixed");
			var index = $(this).index();
			$('.tab-box').eq(index).show().siblings().hide();
		});
		// 定位Tab信息
		$('.target-tab').on('click',function(){
			var $target = $('#'+$(this).data('type'));
			targetTab($target);
		});
		function targetTab(obj){
			if(!obj) return;
			obj.trigger('click');
		}

		$('#score-rate').click(function(event) {
			/* Act on the event */
			$('#li_comment').trigger('click');
		});
	    //弹出框
	    var searchFed = {
	        ie6: typeof document.body.style.maxWidth == 'undefined',
	        showDialog: function(content, title){
	            var _title = title,
	                _content = $(content),
	                _html = '',
	                doc_h = $(document).height(),
	                win_w = $(window).width(),
	                win_h = $(window).height(),
	                scroll_top = $(window).scrollTop(),
	                $dialog_mask = $('.dialog-mask'),
	                _this = searchFed;
	            if (!_content) return false;
	            if (!_title.length) {
	                _html += '<div class="dialog"><a href="javascript:void(0);" class="dialog-close" title="关闭">X</a><div class="dialog-body"></div></div>';
	            } else {
	                _html += '<div class="dialog"><h3>';
	                _html += _title;
	                _html += '</h3><a href="javascript:void(0);" class="dialog-close" title="关闭">X</a><div class="dialog-body"></div></div>';
	            }
	            //open the dialog
	            if (!$dialog_mask.length) {
	                $dialog_mask = $('<div class="dialog-mask"></div>').css('height', doc_h).appendTo('body');
	                $(_html).appendTo('body');
	                $('.dialog-body').append(_content);
	            } else {
	                $dialog_mask.show();
	                $('.dialog').show();
	            }
	            if (_this.ie6) {
	                $('.dialog').css('width', 560);
	            }
	            var $dialog = $('.dialog'),
	                $dialog_close = $dialog.find('.dialog-close'),
	                dialog_w = $dialog.outerWidth(),
	                dialog_h = $dialog.outerHeight(),
	                pos_x,
	                pos_y;
	            pos_x = parseInt((win_w - dialog_w) / 2);
	            pos_y = parseInt((win_h + scroll_top - dialog_h) / 2);
	            $dialog.css({
	                top: pos_y,
	                left: pos_x
	            });

	            var $fav_sel = $('.dialog-fav-sel');
	            $fav_sel.on('click', function(e){
	                e.stopPropagation();
	                var $this = $(this),
	                    $opts = $this.find('.dialog-fav-opt');
	                $this.toggleClass('dialog-fav-sel-on');
	                $opts.toggle();
	                $opts.find('span').mouseenter(function(){
	                    $(this).addClass('on');
	                }).mouseleave(function(){
	                    $(this).removeClass('on');
	                }).click(function(){
	                    $this.find('.fav-default').text($(this).text());
	                });
	                $(document).on('click',function(){
	                    $opts.hide();
	                });
	            });

	            $dialog_close.on('click', function(e){
	                e.preventDefault();
	                _this.hideDialog($dialog, $dialog_mask);
	            });
	        },
	        hideDialog: function($dialog, $mask){
	            if (!$dialog || !$mask) return false;
	            $dialog.remove();
	            $mask.remove();
	        }
	    };


	    var dialogLogin = (function(){
	    	var dialog = null,
				loginUrl = $('.topbar-user-unlogin').find('a').eq(2).attr('href'),
				registerUrl = $('.topbar-user-unlogin').find('a').eq(3).attr('href');

	    	function Dialog(){
	    		var html = '';
	    			html = '<div class="dialog-login">';
				    html += '<div class="dialog-login-in">';
					html += '<h3>为了能及时更新您收藏的商品，需要登录网站</h3>';
					html += '<div class="login-register-mod">';
					html += '<div class="login-mod">';
					html += '<p>已有账号</p>';
					html += '<a href="'+ loginUrl +'" class="dialog-btn btn-login">去登录</a>';
					html += '</div>';
					html += '<div class="register-mod">';
					html += '<p>暂无账号</p>';
					html += '<a href="' + registerUrl + '" class="dialog-btn btn-register">去注册</a>';
					html += '</div>';
					html += '</div>';
					html += '</div>';
					html += '<a href="#" class="dialog-login-close">x</a>';
					html += '</div>';
				return html;
			}

			return {
				getDialog:function(){
					return dialog || (dialog = $('body').append(Dialog()));
				}
			}
	    })();

    //判断是否登录
    searchFed.isLogin = function(){
        var isLogin = Cookies.get('login') === 'true' && Cookies.get('token');
        return isLogin;
    }

    //显示登录框
    searchFed.showLoginDialog = function(){
		var dialog = dialogLogin.getDialog();

	    if(dialog){
	    	$('.dialog-login').show();
	    }
	    
	    $('.dialog-login-close').on('click',function(e){
	    	$('.dialog-login').hide();
	    	e.preventDefault();
	    });
    }

    //获得用户中心在不同环境下的url
    searchFed.getUcenterUrl = function(){

		var ucenterUrl = (function () {
			if (location.hostname.indexOf('stage.bang5mai.com') !== -1) {
				_domain = '.bang5mai.com';
				return 'ucenter.stage.bang5mai.com';
			}
			if (location.hostname.indexOf('prod.bang5mai.com') !== -1) {
				_domain = '.bang5mai.com';
				return 'ucenter.prod.bang5mai.com';
			}
			if (location.hostname.indexOf('ucenter.test.com') !== -1) {
				_domain = '.test.com';
				return 'ucenter.test.com';
			}
			return 'ucenter.b5m.com';
		})();

		return ucenterUrl;
    }

    searchFed.getOriginInfo = function(){
		var href = location.host.split('.')[0] || 'korea',
			source = 0;
		switch(href){
			case 'korea':
				source = 18;
				break;
			case 'haiwai':
				source = 23;
				break;
			case 'usa':
				source = 27;
				break;
			default:
				break;
		}	

		return {
			href:href,
			source:source
		}
    }

    //请求用户中心数据
    searchFed.getUcenterInfo = function(type){
        var userId = userId = Cookies.get('token'),
			ucenterUrl = searchFed.getUcenterUrl();
			source = searchFed.getOriginInfo().source;

		var webSite = $('#url').val() || '';
       	var webSite = webSite.replace('http://',''),
			webSite = webSite.substring(webSite.indexOf(".")+1, webSite.indexOf("/"));

		//请求用户中心数据
		$.ajax({
			url : 'http://' + ucenterUrl + '/gc/user/favorites/data/add.htm',
			type : 'GET',
			data : {
				userId : userId,
				goodsUrl : $('#url').val(),
				picUrl : $('.geo-pic img').attr('src'),
				title : $('.show-hd h1').text(),
				webSite : webSite,
				price : $('.show-price em').text(),
				source : source
			},
			dataType : 'jsonp',
			jsonp : 'jsonpCallback',
			success : function(result) {

				if(type == 1){
					searchFed.getRecommendGoods(result);
				}else if(type == 2){
					var isSuccess = result.ok,
						status = result.code;
					if(!isSuccess && status != 40006){
						alert('添加失败')
					}else if(!isSuccess && status == 40006){
						//多次添加提示已经添加过此商品
						$('#J_count_down').removeClass('').addClass('count-down already-in').off('click');
					}else if(isSuccess){
						//提示添加降价提醒成功
						$('#J_count_down').removeClass('').addClass('count-down goods-in').off('click');
					}
				}
			}
		})
    }

    //请求推荐商品
    searchFed.getRecommendGoods = function(result){

		var  cat = $('#cat').val() || '外套';
		//请求推荐商品
		var url = 'http://korea.b5m.com',
			href = searchFed.getOriginInfo().href;

		var ucenterUrl = searchFed.getUcenterUrl();
		
		$.ajax({
			url:  url + '/api/goods_recommend.php',
			type: 'GET',
			dataType: 'jsonp',
			jsonp : 'jsonpCallback',
			data: {cat:cat,from:href}
		})
		.done(function(data){

			var htmls = data;

	        searchFed.showDialog(
	            '<div class="fav-handle">' +
	                '<i class="fav-suc"></i>' +
	                '<span class="fav-status">成功加入<a href="http://' + ucenterUrl +'/forward.htm?method=/user/account/favorites/index">收藏夹</a></span>' +
	                '</div>' +
	                '<div class="fav-recommend"><h6>喜欢此宝贝的还喜欢</h6><ul class="clear-fix">' + htmls + '</ul></div>', ''
	        )
	        var timeFlag = setTimeout(function(){
				$('.dialog').remove();
				$('.dialog-mask').remove();
			},10000);

			$('.dialog').data('flag',true);
		})
    }

    searchFed.favIsEmpty = function(){

    	var ucenterUrl = searchFed.getUcenterUrl(),
    		userId = userId = Cookies.get('token');

    	$.ajax({
    		url: 'http://' + ucenterUrl + '/gc/user/favorites/data/query.htm',
    		type: 'GET',
    		dataType: 'jsonp',
    		jsonp:'jsonpCallback',
    		data: {userId:userId,priceType:0,pageSize:1}
    	})
    	.done(function(data) {
    		var isEmpty = data.data.page.total;

    		if(isEmpty <= 0){
    			var htmlArr = [];
    			
    			htmlArr.push('<div class="fav-empty-tips">');
				htmlArr.push('<p><strong>降价信息</strong> <span>在</span>个人中心 <span>></span> 我的收藏<span>中查看</span></p>');
				htmlArr.push('<a class="go-links" href="http://' + ucenterUrl + '/forward.htm?method=/user/account/favorites/index">立即点击查看</a>');
				htmlArr.push('<span class="fav-empty-close"></span>');
				htmlArr.push('</div>');

    			$('#J_count_down').before(htmlArr.join('')).fadeIn();

	    		function closeTips(){
	    			$('.fav-empty-tips').fadeOut();
	    		}

	    		$('.fav-empty-close').on('click',function(){
	    			closeTips();
	    		})
    		}

    		//添加商品收藏
    		searchFed.getUcenterInfo(2);
    	})
    	
    }


    //降价提醒
    $('#J_count_down').on('click',function(e){
    	e.preventDefault();

    	var isLogin = searchFed.isLogin();

    	if(!isLogin){
    		searchFed.showLoginDialog();
    	}else{
    		searchFed.favIsEmpty();
    	}
    });

    //添加收藏
    $('#J_add_favo').on('click',function(e){
        e.preventDefault();

        var isLogin = searchFed.isLogin();

        //是否登录
        if(!isLogin){
    		searchFed.showLoginDialog();
        }else{
			searchFed.getUcenterInfo(1)
        }
    })
})

})(jQuery);
