(function(window,$){
    var koreaFed = window.koreaFed = {};

    koreaFed.cateInit = function(){
        //图片列表
        var $cate_img = $('.cate-img').find('li');
        $cate_img.eq(0).addClass('hover');
        $cate_img.on('mouseenter', function(){
            $(this).siblings().removeClass('hover').end().addClass('hover');
        });
        //cafe-甜点
        var $cate_list_sweet = $('.cate-list-sweet'),
            $cate_pic = $cate_list_sweet.find('.cate-pic').find('img'),
            $opacity = $cate_list_sweet.find('.opacity'),
            $cate_img = $cate_list_sweet.find('ul').find('img');
        $opacity.text($cate_img.eq(0).attr('alt'));
        $cate_pic.attr('src', $cate_img.eq(0).attr('data-img'));
        $cate_img.on('mouseenter', function(){
            var $this = $(this),
                _title= $this.attr('alt'),
                _data_img = $(this).attr('data-img');
            $cate_pic.attr('src', _data_img);
            $opacity.text(_title);
        });
        //面食
        var $cate_sidebar = $('.cate-sidebar'),
            $dl = $cate_sidebar.find('dl');

        $dl.on('mouseenter', function(){
            var $this = $(this);
            $this.siblings().removeClass('hover');
            $this.addClass('hover');
        });
    };

    koreaFed.cateFun = function(){
        this.cateInit();
        //轮播图
        $('.gallerypic').b5mGalleryPic({
            maction: 'mouseover'
        });
    };
})(window,jQuery);


