(function($,window){


    var spans = $('.yl-bottom .sel'),
        spanItems = $('.yl-bottom .panel');


    $('.yl-bottom .sel span').each(function(i,n) {

        $(this).mouseenter(function() {
            spans.removeClass(spans[0].className.match(/check\d/)[0]).addClass('check'+(i+1));
            spanItems.find('.cur').removeClass('cur');
            spanItems.find('.item').eq(i).addClass('cur');
        });

    });

    $('.yl-slider').each(function() {
        b5mKorea.utils.slider($(this),770);
    });


    var tabTitle = $('.tab-title'),
        tabContent = $('.tab-content-box'),
        tabContainer = tabTitle.parent();

    tabTitle.find('a').each(function(i,n) {
        $(n).mouseenter(function() {

            tabTitle.find('.cur').removeClass('cur');
            $(n).addClass('cur');

            tabContent.find('.cur').removeClass('cur');
            tabContent.find('.tab-content').eq(i).addClass('cur');

            $('.tab-area.cur').removeClass('cur');
            $('.tab-area').eq(i).addClass('cur');

            tabContainer.find('.more.cur').removeClass('cur');
            tabContainer.find('.more').eq(i).addClass('cur');

        });
    });


})(jQuery,window);