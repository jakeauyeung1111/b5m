(function(window, $){

    var koreaFed = window.koreaFed = {};

    koreaFed.busanInit = function(){
        var _tab = $('.busan-tab');
        _tab.each(function(){
            var _this = $(this),
                _list = _this.find('li'),
                _cont = _this.siblings('.busan-tab_cont').find('.busan-tab-item');
            _list.each(function(){
                var _self = $(this),
                    _index = _self.index();
                _self.mouseenter(function(){
                    _list.removeClass('current');
                    _self.addClass('current');
                    _cont.hide();
                    _cont.eq(_index).show();
                });
            });
        });
    };

    koreaFed.busanFun = function(){
        this.busanInit();

        $('.J_tab').tab({
            event: 'hover',
            auto: true,
            speeds: 5000
        });

        //轮播图
        $('.gallerypic').b5mGalleryPic({
            maction: 'mouseover'
        });
    };
})(window, jQuery);

