var jq=$.noConflict();
var b5m=window.b5m||{};
b5m.korea={};
window.koreaComment = window.koreaComment || {};
//商品详情页
b5m.korea.itemDetail=function(){
 jq(function(){
    jq(".item-img li").click(function(){
	  var _idx=jq(this).index();
	  var a=jq(".item-pic").find("a").eq(_idx);
	  a.css("display","block");
	  a.siblings().css("display","none");
	  jq(this).addClass("cur");
	  jq(this).siblings().removeClass("cur");
	});
 });
 jq("#shopSort").find("dd").click(function(){
    jq(this).addClass("checked");
	 jq(this).siblings().removeClass("checked");
 });
 jq("#desTab li").click(function(){
   var wrap=jq(this).attr("wraps");
   var _sibling=jq(wrap).siblings();
   jq(this).addClass("cur");
   jq(this).siblings().removeClass("cur");
   jq(wrap).show(2);
   _sibling.hide(2);
 });
}
//用户中心-订单列表
b5m.korea.user=function(){
    var _this = this;
   jq(function(){
    jq("[orderCancel]").click(function(){
	  jq("#CancelOrder").empty().append((jq("#tpl").jqote())).show();
	});

    /*提交订单付款*/
    var $formCent = jq('#theForm');

       $formCent.on('click','.btn-submit',function(){
           console.log(_this);
            if(_this.MessageReg()){
                $formCent.submit();
            }else{
                return false;
            };
       });
  });

};
/*填写信息验证*/
b5m.korea.MessageReg=function(){
    var flag = true,
        nameVal = jq('#consignee').val(),
        mobileVal = jq('#mobile').val(),
        emailVal = jq('#email').val(),
        cardVal = jq('#id_card').val();

    /*姓名不能为空*/
    if(!nameVal){
        alert('姓名不能为空！');
        return flag  = false;
    };
    /*验证手机号*/
    if(nameVal == ''){
        alert('手机号码不能为空！');
        return flag = false;
    }else{
        var reg = /^0*(13|15|18)\d{9}$/;
        if(!reg.test(mobileVal)){
            alert('手机号格式有误！');
            return flag = false;
        };
    };
    /*验证邮箱*/
    if(emailVal == ''){
        alert('电子邮箱不能为空！');
        return flag = false;
    }else{
        var reg = /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;
        if(!reg.test(emailVal)){
            alert('电子邮箱格式有误！');
            return flag = false;
        };
    };
    if(cardVal == ''){
        return flag  = true;
    }else{
        var reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
        if(!reg.test(cardVal)){
            alert('身份证输入不合法！');
            return flag = false;
        };
    }
    /*验证身份证*/
};


(function(window,$) {

    $(function(){
    var _this = this,
        $container=$('#OrderDetail'),
        $fileImgBtn = $container.find('.btn-file'),
        $el_submit = $container.find('.btn-comment-submit'),
        $el_submit = $container.find('.btn-comment-submit'),
        $uploadImgBox = $('.upload-img-box'),
        $fileBtn = $uploadImgBox.find('.file'),
        $el_form = $uploadImgBox.find('#iform'),
        $closeBtn = $uploadImgBox.find('.close'),
        $el_loading = $uploadImgBox.find('.loading'),
        $el_uploadshow = $container.find('.upload-img-show'),
        $el_textarea = $container.find('textarea').val(),
        $starTip = $container.find('.comment-warnning');
    window.koreaComment = {
        init:function(){
            var $_this = this;
            $('.star-rating[grades = "shop_service_grades"]').on('click','li a',function(){
                var jqThis = $(this);
                jqThis.addClass('current-rating').parent('li').siblings('li').find('a').removeClass('current-rating');
                jq('input[name="service_rank"]').val(parseInt(jqThis.text()));
                $starTip.hide();
                return false;
            });
            $('.star-rating[grades = "shop_flow_grades"]').on('click','li a',function(){
                var jqThis = $(this);
                jqThis.addClass('current-rating').parent('li').siblings('li').find('a').removeClass('current-rating');
                $('input[name="comment_rank"]').val(parseInt(jqThis.text()));
                $starTip.hide();
                return false;
            });
            $_this.uploadImg();
        },
        //上传图片功能
        uploadImg:function() {
            $uploadImgBox.click(function(e) {
                e.stopPropagation();
            });

            $fileImgBtn.on('click',function(e){
                var $this =jq(this);
                $(this).attr('type','button');
                e.stopPropagation();
                $uploadImgBox.show().css({
                    left:$this.offset().left+70+'px',
                    top:$this.offset().top+25+'px'
                });
                $(document).one('click',function() {
                    $closeBtn.trigger('click');
                });
            });
            $closeBtn.on('click',function(){
                $uploadImgBox.hide();
            });

            $fileBtn.change(function(){
                var pics = $el_uploadshow.find('div').length;
                if(pics < 5){
                    $el_loading.addClass('on');
                    $el_form.submit();
                }else{
                    $uploadImgBox.hide();
                    $fileBtn.val('');
                }
            });
            /*删除图片*/
            $el_uploadshow.on('click','a',function() {
                var _this = this;
                var url = $(this).parent().find("input").val();
                $.ajax({
                    url: "http://korea.b5m.com/portal.php?mod=portalcp&ac=comment",
                    data: 'op=delete_img&url='+url,
                    type: 'POST',
                    dataType: 'html',
                    success: function(data){
                        $(_this).parent().detach();
                    }
                });

            });
        },
        //提交表单
        submit:function(){
            $.ajax({
                url: "http://korea.b5m.com/portal.php?mod=portalcp&ac=comment",
                data: 'op=submit_code&vcode=',
                type: 'POST',
                dataType: 'html',
                success: function(data){
                    if (data === "success") {
                        if($.trim($el_textarea.val()) === '') {
                            return false;
                        }
                        $('#iform').submit();
                    } else {
                        if (data == "请先登录") {
                            window.location.href = "http://ucenter.b5m.com/forward.htm?method=/user/user/login&loginReferer="+ encodeURIComponent(window.location.href);
                        }
                    }
                }
            });
            return false;
        },
        //图片上传成功回调
        uploadImgComplete:function(url, message) {
            if (message === 'success') {
                $el_uploadshow.append('<div><img src="/data/attachment/'+ url +'" width="100" height="100" alt=""/><a href="javascript:void(0)">删除</a><input type="hidden" value="'+url+'" name="images[]" /></div>');
                $el_loading.removeClass('on');
                $uploadImgBox.hide();
                $fileBtn.val('');
            } else {
                $el_loading.removeClass('on');
                $uploadImgBox.hide();
                window.location.href = "http://ucenter.b5m.com/forward.htm?method=/user/user/login&loginReferer="+ encodeURIComponent(window.location.href);
            }
        }
    };
        window.koreaComment.init();
    });
})(window,jQuery);