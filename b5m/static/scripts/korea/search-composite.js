;(function($,window,document,undefined) {

 window.searchCompositeFed = {
    /*搜索属性*/
    searchType:function(){
        /*查看其他属性*/
        $(".box-search").on({
            click:function(){
                var $this = $(this);
                if($this.hasClass('cur')){
                    $this.removeClass('cur');
                    $this.parent().find('.hide').hide();
                }else{
                    var $hideItem = $this.parent().find('.hide');
                    $this.addClass('cur');
                    $hideItem.show();
                    $hideItem.find(".ico-updown-u").parent().trigger("click");
                }
                return false;
            }
        },'.search-seeall');
        /*$(".search-seeall").click(function(){
            var $this = $(this);
            if($this.hasClass('cur')){
                $this.removeClass('cur');
                $this.parent().find('.hide').hide();
            }else{
                var $hideItem = $this.parent().find('.hide');
                $this.addClass('cur');
                $hideItem.show();
                $hideItem.find(".ico-updown-u").parent().trigger("click");
            }
        });*/
        /*搜索属性展开*/
        $(".search-item").on({
            click:function(){
                var $this = $(this);
                if($('.ico-updown-u',this).length){
                    $this.html('展开<u class="ico-updown"></u>');
                    $this.parent().height(23);
                }else{
                    $this.parent().height('auto');
                    $this.html('收起<u class="ico-updown ico-updown-u"></u>');
                }
                return false;
            }
        },".search-seemore");
        /*$('.search-seemore').click(function(){
            var $this = $(this);
            if($('.ico-updown-u',this).length){
                $this.html('展开<u class="ico-updown"></u>');
                $this.parent().height(23);
            }else{
                $this.parent().height('auto');
                $this.html('收起<u class="ico-updown ico-updown-u"></u>');
            }
        });*/
    },
    /*价格跳转*/
    searchPrice:function(){
        var boxId = '.search-sort-price';
        /*限制Input只能输入数字*/
        $("input",boxId).keyup(function(){
            var $this = $(this);
            $this.val($this.val().replace(/[^0-9.]+/gi,''));
        });
        /*提交价位*/
        $("strong",boxId).click(function(){
            var priceArr = []
                ,sPrice = Number($("input:first",boxId).val().replace(/[^0-9.]+/gi,''))||0
                ,ePrice = Number($("input:eq(1)",boxId).val().replace(/[^0-9.]+/gi,''))||999999;
            if(ePrice<=sPrice){
                priceArr[0] = ePrice;
                priceArr[1] = sPrice;
            }else{
                priceArr[0] = sPrice;
                priceArr[1] = ePrice;
            }
             window.location.href = $("#search_url").val().replace("{price}",priceArr.join("-"));
            //alert(priceArr.join("\n"));
        });
    },
    /*侧边导航*/
    sideNav:function(){
        var boxId = '#side-pageNav'
            ,navTop = 230;
        var $box = $('<ul id="side-pageNav" class="side-pageNav"><li class="cur"><s>1F</s><span>商品</span><u></u></li><li><s>2F</s><span>资讯</span><u></u></li></ul>').appendTo($("body"));
        $box.css("top",$(".wp>.layout-searchTit").offset().top+15+"px");
        if(typeof document.getElementsByTagName('body')[0].style.maxHeight=="undefined"){
            $(window).scroll(function(){
                $box.css("top",(navTop+$(this).scrollTop())+"px");
            });
        }
        /*导航滚动*/
        $("li",boxId).click(function(){
            var $this = $(this)
                ,thisIndex = $this.index();
            if(thisIndex==0){
                $("html,body").animate({scrollTop:140},180);
                //$("html,body").animate({scrollTop:$(".layout-cont").offset().top-40},180);
            }else{
                if($(".box-news").length)
                $("html,body").animate({scrollTop:$(".box-news").offset().top-20},180);
            }
            $(this).addClass('cur').siblings('.cur').removeClass('cur');
        });
    },
    loadFun:function(){
        this.searchType();
        this.searchPrice();
        this.sideNav();
    }
};


})(jQuery,window,document,undefined);