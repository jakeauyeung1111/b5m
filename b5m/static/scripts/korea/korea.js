window.b5mKorea = window.b5mKorea || {};

(function($,window){


    b5mKorea.namespace = function(ns_string) {
        var parts = ns_string.split('.'),
        parent = b5mKorea,i;
        if(parts[0]==='b5mKorea') {
            parts = parts.slice(1);
        }
        for(i=0;i<parts.length;i++) {
            if(typeof parent[parts[i]] === 'undefined') {
                parent[parts[i]] = {};
            }
            parent = parent[parts[i]];
        }
        return parent;
    }


    //star评分
    b5mKorea.Star = function(elem) {
        var _elem,_input;
        this.elem = $(elem);
        this.input = $('input[name='+ this.elem.attr('data-input') +']');
        this.elems =  $('span',this.elem);

        _elem = this.elem;
        _input = this.input;

        _elem.data('data-star',_elem.attr('data-star')).on({
            mouseout:function() {
                _elem.removeClass('s'+_elem.attr('data-star')).addClass('s'+_elem.data('data-star')).attr('data-star',_elem.data('data-star'));
            }
        });
        this.elems.each(function(i,n){
            $(n).on({
                mouseover:function() {
                    _elem.removeClass('s'+_elem.attr('data-star')).addClass('s'+(i+1)).attr('data-star',i+1);
                },
                click:function(e) {
                    _elem.data('data-star',i+1);
                    if(_input.length) _input.val(i+1);
                }
            });
        });
    };

    b5mKorea.Star.prototype = {
        reset:function(index) {
            this.elem.removeClass('s'+this.elem.attr('data-star')).addClass('s'+index).attr('data-star',index).data('data-star',index);
            this.input.val(index);
            this.elText.text('（'+ this.elTextArr[index-1] + '）');
        },
        bindText:function(el) {
            this.elText = el;
            var _elem = this.elem;
            var texts = ['差','一般','一般','好','好'];
            this.elTextArr = texts;
            this.elems.on({
                mouseover:function() {
                    el.text('（'+ texts[_elem.attr('data-star')-1] +'）');
                },
                mouseout:function() {
                    el.text('（'+ texts[_elem.data('data-star')-1] +'）');
                }
            });
        }
    };






    //b5mkorea 工具类
    b5mKorea.namespace('utils');

    b5mKorea.utils.ie6 = (function() {
        return  typeof document.body.style.maxHeight==='undefined' ? true :false;
    })();

    b5mKorea.utils.dialog = function(name,container,width,height) {
        this.name = name;
        this.width = width;
        this.height = height;
        this.container = $(container);
        this.init();
    }

    b5mKorea.utils.dialog.prototype = {
        init:function() {
            this.dialog = $('<div id="dialog-'+ this.name +'" class="dialog" style="width:'+ this.width +'px;height:'+ this.height +'px"><a href="javascript:void(0)" class="dialog-close">关闭</a></div>').appendTo('body').append(this.container);
            this.mask = this.createMask();
            this.close = this.dialog.find('.dialog-close').click($.proxy(this.onClose,this));
        },
        createMask:function() {
            if(b5mKorea.utils.dialog.mask) {
                return $('.dialog-mask');
            }
            b5mKorea.utils.dialog.mask = true;
            return $('<div class="dialog-mask"></div>').appendTo('body');
        },
        open:function(fun) {
            this.dialog.show();
            this.mask.show();
            this.setPos();
            $(window).on('resize',$.proxy(this.setPos,this));
            if(typeof fun === 'function') {
                fun.call(this);
            }
        },
        onClose:function() {
            this.dialog.hide();
            this.mask.hide();
            $(window).off('resize',this.setPos);
        },
        setPos:function() {
            var scrollTop = document.documentElement.scrollTop || document.body.scrollTop,
            scrollHeight =  document.documentElement.scrollHeight || document.body.scrollHeight,
            winHeight = document.documentElement.clientHeight || document.body.clientHeight,
            winWidth = document.documentElement.clientWidth || document.body.clientWidth,
            ie6 = b5mKorea.utils.ie6;
            this.dialog.css({
                top: (winHeight - this.height)/2-20 + (ie6 ? scrollTop : 0),
                left:(winWidth - this.width)/2-10
            });
            this.mask.height(ie6 ? scrollHeight : winHeight);
        }
    };




    b5mKorea.utils.slider = function(elem,w) {

        var elem = elem,
        container = elem.find('.container'),
        prev =elem.find('.prev'),
        next = elem.find('.next'),
        len = container.find('li').length,
        width = w,
        index =0;

        container.find('ul').width(width*len);

        if(len<=1) {
            next.addClass('last');
        }

        prev.click(function() {
            if(index > 0) {
                index --;
                container.animate({
                    scrollLeft:index*width
                    });
                if(index === 0) {
                    $(this).addClass('first');
                }
                if(index!=len-1) {
                    next.removeClass('last');
                }
            }
        });

        next.click(function(){
            if(index< len-1) {
                index ++;
                container.animate({
                    scrollLeft:index*width
                    });
                if(index === len-1) {
                    $(this).addClass('last');
                }
                if(index!=0) {
                    prev.removeClass('first');
                }
            }
        });

    }




    //search placeholder
    //    var isSearchResultPage = /mod\=goods&kw/.test(location.href);
    $('.header-search-key[placeholder]').placeholder({
        useBrowserPlaceholder:true,
        className: 'filter-text-placeholder'
    }).on('focusin', function() {
        $(this).addClass('filter-text-focusin');
    //         if(isSearchResultPage) {
    //          $(this).addClass('search');
    //       }
    }).on('focusout', function() {
        $(this).removeClass('filter-text-focusin');
    //     if(isSearchResultPage) {
    //        $(this).removeClass('search');
    //   }
    });


    //详情页video标签生成
    /*demo: <a class="player" href="http://www.b5m.com">
 <img src="http://cdn.b5m.com/upload/web/discuz/attachment/portal/201307/30/143841zl6jj8f8cmmwwm88.jpg">
 <span class="icon play"></span>
 </a>*/
    (function() {
        var detailHtml = $('.detail-korea .context');
        if(detailHtml[0]) {
            detailHtml.html(detailHtml.html().replace(/(<a[^>]*>\s*<img[^>]*>\s*<\/a>)[\s\S]*?(#videoE#)/gi,function(a,b) {
                b = b.replace(/(<a)/i,'$1 class="player" target="_blank" rel="nofollow" ');
                b = b.replace(/(<\/a>)/i,'<span class="icon play"></span>$1');
                return b;
            }));
        }
    })();
    //首页slider
    $('.home-content .main-slider').each(function() {
        b5mKorea.utils.slider($(this),905);
    });


    //收藏
    $('.box-addfav-content .close').click(function() {
        $('.box-addfav').hide();
        $('.box-addfav-content').hide();
    });

    /*左侧底部显示视频关闭 zibu 20140402 s*/
    b5mKorea.mideoLayer=function(){
        var $this = $('.box-video-layer'),
            $thisHtml = $this.find('.video-cent').html(),
            $repeatBtn = $('.repeat-play'),
            $videoCont = $('#incVideoShow'),
            $videoUrl = $videoCont.attr('data-video');

        $this.on('click','.close',function(){
            $this.animate({'left':'-460px'},500,function(){
                $this.find('.video-cent').html('');
                $repeatBtn.animate({'left':'0px'},300);
            })
        });
        $repeatBtn.on('click','a',function(){
            $repeatBtn.animate({'left':'-40px'},300,function(){
                $this.animate({'left':'0px'},500,function(){
                    $this.find('.video-cent').html($thisHtml);
                    videoPlay ();
                });
            });
        });
        /*插入视频*/
        function videoPlay (){
            var flashvars={
                f:$videoUrl,
                c:0,
                b:1,
                e:0,
                p:1
            };
            var params={bgcolor:'#FFF',allowFullScreen:true,allowScriptAccess:'always'};
            CKobject.embedSWF('http://staticcdn.b5m.com/static/opt/ckplayer/ckplayer.swf','incVideoShow','ckplayer_a1','400','240',flashvars,params);

            window.playerstop = function(){
                $this.find('.close').trigger('click');
            };
        };
        videoPlay ();
    };
    /*左侧底部显示视频关闭 zibu 20140402 e*/

    /*zibu 20140303 底部浮层 s*/
    b5mKorea.bottomLayer=function(){
        var html = '';
        html +='<div id="bottom-layer" class="bot-layer"><div class="wp"><h4 class="jingdan"></h4><ul class="ly-login"><li><i class="b5m"></i><a href="http://ucenter.b5m.com/">B5M帐号登录</a></li>';
        html +='<li><i class="weibo"></i><a href="http://ucenter.b5m.com/user/third/login/auth.htm?type=2&refererUrl=http%3A%2F%2Fkorea.b5m.com%2F%3Fmps%3D0.0.0.0.0&userType=18">微博登录</a> </li>';
        html +='<li><i class="qq"></i><a href="http://ucenter.b5m.com/user/third/login/auth.htm?type=1&refererUrl=http%3A%2F%2Fkorea.b5m.com%2F%3Fmps%3D0.0.0.0.0&userType=18">QQ登录</a></li>';
        html +='</ul><p class="close-w"><a href="javascript:void(0);" class="btn"></a></p></div></div>';
        $(html).appendTo('body');

        $('#bottom-layer').on('click','.close-w a',function(){
            $('#bottom-layer').hide();
            return false;
        });
    };

    b5mKorea.floatTagsFun=function(){
        var numb = 270,
            windowWidth = $(document).width(),
            $tagsLw = $('.korea-tags-l'),
            $tagsRw = $('.korea-tags-r');

        if(windowWidth > 1000){
            $tagsLw.show();
        }else{
            $tagsLw.hide();
        };
        $(window).scroll(function(){
            var scroTop = $(window).scrollTop();

            if( scroTop > numb){
                $tagsLw.addClass('tags-fis');
                if(windowWidth > 1000){
                    $tagsRw.hide();
                }else{
                    $tagsRw.show();
                };
            }else{
                $tagsLw.removeClass('tags-fis');
                $tagsRw.hide();
            }
            //console.log(scroTop);
        });
    };
    b5mKorea.mideoLayer();
    /*b5mKorea.bottomLayer();*/
    b5mKorea.floatTagsFun();
    /*zibu 20140303 底部浮层 e*/
	$('#J_topslider').slideBox({
		duration : 0.3,
		easing : 'linear',
		delay : 5,
		hideClickBar : false,
		clickBarRadius : 10
	});


    $('.header-search-key').autoFill('','korea');

    
})(jQuery,window);

