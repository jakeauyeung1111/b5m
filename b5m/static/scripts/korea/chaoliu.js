(function(widow,$) {

    var ranks = $('.ranks');

    //榜单切换
   $('.rank-sel').each(function(i,n) {
       $(n).find('a').each(function(i1,n1) {
           $(n1).mouseover(function() {
               $(n).find('.cur').removeClass('cur');
               $(this).addClass('cur');
               ranks.eq(i).find('ul.cur').removeClass('cur');
               ranks.eq(i).find('ul').eq(i1).addClass('cur');
           });
       });
   });

   //榜单详细简略切换
    ranks.on('mouseover','.brief',function() {
        $(this).parent().parent().find('.cur').removeClass('cur');
        $(this).parent().addClass('cur');
    });

    //时尚街拍滚动
    var scrollIndex= 3,
        scrollWidth= 254,
        scrollElem = $('.scroll-img .scroll'),
        scrollBtns = $('.scroll-img .btn a'),
        scrollDesc = $('.pic-desc'),
        scrollTimer= null;

    var scrollImg = function scroller() {
      clearTimeout(scrollTimer);
      if(scrollIndex==3) {
          scrollIndex=0;
      }
      scrollElem.animate({scrollLeft:scrollWidth*scrollIndex});
      $('.scroll-img .btn a.cur').removeClass('cur');
      $('.pic-desc.cur').removeClass('cur');
      scrollIndex++;
      scrollBtns.eq(scrollIndex-1).addClass('cur');
      scrollDesc.eq(scrollIndex-1).addClass('cur');
      scrollTimer =  setTimeout(function() {
          scroller();
      },6000);

      return scroller;
  }();

    scrollBtns.each(function(i,n) {
        $(n).mouseover(function() {
            scrollIndex = i;
            scrollImg();
        });
    });


})(window,jQuery);