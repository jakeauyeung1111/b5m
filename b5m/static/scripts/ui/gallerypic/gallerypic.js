/**
 * jQuery GalleryPic plugin
 * @param {boolean} autoplay  是否开启自动播放功能
 * @param {int} autoplaySpeed  自动播放速度
 * @param {string} maction  选择鼠标行为方式
 * @param {boolean} showtext  是否显示文章
 * @example $(element).b5mGalleryPic({autoplay: true,autoplaySpeed:5000,maction:'click',showtext:false});
 */
(function(window,$){
    $.b5m.namespace('galleryPic');
    $.b5m.galleryPic = function(element,options) {

        'use strict';

        var base = this;

        var settings = $.extend({
            autoplay:true, // 是否开启自动播放功能 {true | false}
            autoplaySpeed:5000, // 自动播放速度 {int}
            maction:'click', // 选择鼠标行为方式 {'click' | 'mouseover'}
            showtext:true //是否显示文字信息{true | false}
        },options || {});

        base.init = function() {

            base.index = 0; //当前展示的index
            base.total = 3; //图片展示总数
            base.conList = element.find('.con li');
            base.barList = element.find('.bar li');
            base.playTimer = null;

            //自动播放
            if(settings.autoplay) {
                base.playTimer = setTimeout(function() {
                    base.play();
                },settings.autoplaySpeed);
            }

            //文字信息
            if(settings.showtext) {
                element.find('p').show();
            }

            //event handler
            base.barList.each(function(i,n){
                $(n).on((settings.maction === 'mouseover' ? 'mouseenter': 'click'),function() {

                    if($(n).hasClass('cur')) {
                        return false;
                    }

                    if(settings.autoplay) {
                        clearTimeout(base.playTimer);
                        base.playTimer = setTimeout(function() {
                            base.play();
                        },settings.autoplaySpeed);
                    }

                    base.index = i;
                    base.handler.call(n);
                    return false;
                });
            });

        };

        //事件相应，轮播动画
        base.handler = function() {
            base.barList.filter(function() { return $(this).hasClass('cur'); }).removeClass('cur');
            base.barList.eq(base.index).addClass('cur');
            base.conList.filter(function() { return $(this).hasClass('cur'); }).fadeOut().removeClass('cur');
            base.conList.eq(base.index).fadeIn().addClass('cur');
        };

        //执行轮播
        base.play =  function() {
            if(!base.playTimer) {
                return false;
            }
            base.index ++;
            if(base.index === base.total) {
                base.index = 0;
            }
            base.handler.call(base.barList.eq(base.index));
            base.playTimer = setTimeout(function() {
                base.play();
            },settings.autoplaySpeed);
        };

    };

    $.fn.b5mGalleryPic = function (options) {
        return this.each(function () {
            new $.b5m.galleryPic($(this),options).init();
        });
    };
})(window,jQuery);
