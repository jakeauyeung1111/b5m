$(document).ready(function ()
{
    getGoodsData(0);
    getGoodsData(1);
    getGoodsData(2);
    getGoodsData(3);
    getGoodsData(4);
    $.setPlaceholder($('.J_search'), 'placeholder', {
        focus: function ()
        {
            $(this).addClass('focusin');
        },
        blur: function ()
        {
            $(this).removeClass('focusin');
        }
    });
    $('.page-input input').on('keyup', function(e) {
        this.value = this.value.replace(/\D/g, "");
    });
});
var floor = ['.slider-1', '.slider-2', '.slider-3', '.slider-4', '.slider-5'];
var floor_t = ['餐饮美食', '休闲娱乐', '旅游酒店', '美容保健', '网上购物'];
$('.slider').each(function (floor)
{
    $(this).find('.slider-nav > li').click(floor, function ()
    {
        getGoodsData(floor, $(this).index());
    });
});
function slPrice(pr)
{
    if (pr.indexOf('-') != -1)
    {
        return pr.split('-')[0];
    }
    else
    {
        return pr;
    }
}
function getGoodsData(floorof)
{
    if (-1 != floorof)
    {
        var cat = $(floor[floorof]).find('.cur').index();
        if ($(floor[floorof] + ' .slider-item').eq(cat).find('li').size() == 0)
        {
            var ctxt = $(floor[floorof] + ' .cur').find('a').html();
            ctxt = $.trim(ctxt.replace(/<[^>]+>/g, ""));
            ctxt = ctxt == '推荐' ? '' : encodeURI(ctxt);
            ctxt = ctxt == '' ? '' : '/keyword/' + ctxt;
            $.ajax({
                type: "post",
                dataType: "jsonp",
                async: false,
                url: 'http://tuan.b5m.com/goodsApi/tuan/category/' + encodeURI(floor_t[floorof]) + ctxt + '/cb/fillfloor/ext/' + floorof, //TODO 修改
                data: ""
            });
        }
    }

}
function fillfloor(data)
{
    if (data.msg == 'ok')
    {
        var buf = [];
        if (data.data)
        {
            for (var i = 0; i < data.data.length; i++)
            {
                if (typeof(data.data[i].PlayPicture) != "undefined")
                {
                    buf.push('<li><a class="pic" href="' + data.data[i].Url + '"><img class="midd" src="http://cdn.bang5mai.com/b5m_img/' + data.data[i].PlayPicture + '"></a>');
                    buf.push('<a class="tit" href="' + data.data[i].Url + '">' + data.data[i].PlayName + '</a><span class="cutprice">¥' + slPrice(data.data[i].LowestTicketPrice) + '</span></li>');
                } else
                {
                    buf.push('<li><a class="pic" href="' + data.data[i].Url + '"><img class="midd" src="http://cdn.bang5mai.com/b5m_img/' + data.data[i].Picture + '"></a>');
                    buf.push('<a class="tit" href="' + data.data[i].Url + '">' + data.data[i].Title + '</a><span class="cutprice">¥' + slPrice(data.data[i].Price) + '</span></li>');
                }
                var ext = data.ext;
            }
        }
        if (buf.length == 0)
        {
            buf.push('<li></li>');
        }
        var len = data.data === null ? 1 : data.data.length;
        var i = $(floor[ext]).find('.cur').index();
        $(floor[ext] + ' .slider-item:eq(' + i + ') ul').html(buf.join('')).hide().fadeIn(800);
        productpage(floor[ext], len);

    }
}
function productpage(chose, pageLen)
{
    pageLen = Math.ceil(pageLen / 4);
    var html = '';
    var i = pageLen - 1;
    while (i >= 0)
    {
        html = '<a ' + (i == 0 ? 'class="cur"' : '') + ' href="javascript:void(0)">' + (i + 1) + '</a>' + html;
        i--;
    }
    $(chose + ' .slider-page').html(html);
}
$('.slider .slider-prev').click(function ()
{
    getGoodsData($('.slider .slider-prev').index($(this)));
});
$('.slider .slider-next').click(function ()
{
    getGoodsData($('.slider .slider-next').index($(this)));
});



