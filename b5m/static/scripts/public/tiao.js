$(document).ready(function() {
    $.setPlaceholder($('.J_search'), 'placeholder', {
        focus: function() {
            $(this).addClass('focusin');
        },
        blur: function() {
            $(this).removeClass('focusin');
        }
    });
    $('.J_autofill').autoFill('','');
});