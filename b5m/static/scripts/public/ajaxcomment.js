/**
 * @version v1.0.0
 * @author shuai zou <weiyan@b5m.com>
 * @file ajaxcomment.js 
 */

// artile id
var id = location.pathname.substring(1, location.pathname.indexOf('.'));
var _timestamp;
$(function() {

    // bangdou tips from 前端 临时
    var bangdouTips = function(){
        var tips;
        function init(){
            return $('<div class="bangdou-tips"><div class="bangdou-tips__in">+2</div></div>').appendTo($('.bangdou-target'));
        }
        return {
            getTips:function(){
                return tips || (tips = init());
            }
        }
    }();

    function addBangtou(){
        bangdouTips.getTips()
        .css({opacity:1})
        .animate({
            top:-75,
            right:-60,
            opacity:.75
        },250)
        .animate({
            top:-80,
            right:-75,
            opacity:.5
        },200)
        .animate({
            top:-85,
            right:-75,
            opacity:.5
        },150)
        .animate({
            top:-95,
            right:-90,
            opacity:0
        },100)
        .animate({
            top:-100,
            right:-95,
            opacity:0
        },200,function(){$(this).css({top:-65,right:-50})});
    }
     // bangdou tips from 前端 临时


    // comment 提交
    $('#comment_sub').click(function() {
        var content = $('#comment_content').val();
        // 判断登录状态
        if (Cookies.get('login') !== 'true') {
            alert('亲！你还没有登录');
            return;
        }
        if (!content)
            return;

        // 5s内禁止重复评论
        var time = new Date().getTime();
        if (time - parseInt(_timestamp) < 5000) {
            alert('请5秒之后在评论');
            return;
        } else {
            _timestamp = time;
        }


        $.post("/ajaxcomment.htm", {content: content, id: id}, function(response) {
            response.msg === 'ok' ? ajaxcomment(1, 'id', id) : '';
            (response.msg === 'ok') && $('#comment_content').val('');
            (response.msg === 'ok') && strLength();
            (response.tips === 'ok') && addBangtou(2);
        },'json');
    });


    // comment page
    $('.page').delegate('div a', 'click', function() {
        var page = $(this).attr('page');
        if (isNaN(page))
            var page = $(this).parent().find('input').val();
        ajaxcomment(parseInt(page), 'id', id);
        // 阻止 href
        return false;
    });

    // comment page GO zs | 绑定回车事件
    $('.page').delegate('.page-input input', 'keyup', function(e) {
        var pageObj = $(this).parents('.page-input').find('.page_num');
        var page = pageObj.val();
        // page 超过，返回最大分页 | rubbish
        var maxpage = $('.pagenum').attr('page');
        if (page > parseInt(maxpage) || isNaN(page)) {
            pageObj.val(maxpage);
        }
        if (e.keyCode === 13) {
            page = pageObj.val();
            ajaxcomment(parseInt(page), 'id', id);
        }
    });


    // 排序 方式
    $('.tab-h').find('a').click(function() {
        var index = $(this).index();
        $('.tab-h').find('a').removeClass('curr');
        $(this).addClass('curr');
        if (index === 0) {
            ajaxcomment(1, 'id', id);
        } else {
            ajaxcomment(1, 'up_num', id);
        }
    });


    // reply
    $('.c-list .reply').each(function() {
        $(this).insertRely($(this).parents('.list-con-li').attr('replyid'));
    });

    // 计算 评论字符数
    strLength();


    // 同类比价,增加帮豆
    
    /*$('.btn-compare').on('click',function(){
        compareDou();
        return false;
    });
    $('.img-con').find('a').first().on('click',function(){
        compareDou();
        return false;
    });*/
    


});

/*
var compareDou = function(){ 

    $.post("/comparedou.htm", {id: id}, function(response) {
            (response.msg === 'success') && addBangtou(1);
        },'json');

}
*/

function strLength(){

    // 评论字符个数限制
    var counter = $(".comment textarea").val().length; //获取文本域的字符串长度
    $(".str_num").text('你还可以输入' + (200 - counter) + '个字符');
    $(document).keydown(function() {
        var text = $(".comment textarea").val();
        var counter = text.length;
        $(".str_num").text('你还可以输入' + (200 - counter) + '个字符');    //每次减去字符长度
        if ((200 - counter) <= 0)
            alert('超过最大字符数');
    });
}


// comment 回复 
function ajaxreply() {

    var replyId = $('#replycomment').attr('user');
    var reply_content = $('#reply_content').val();
    // 判断登录状态
    if (Cookies.get('login') !== 'true') {
        alert('亲！你还没有登录');
        return;
    }
    if (!reply_content)
        return;
    $.post("/Commentreply.htm", {replyId: replyId, reply_content: reply_content, id: id}, function(response) {
//         response === 'ok'?console.log('回复成功'):'';
        response === 'ok' ? ajaxcomment(1, 'id', id) : '';
        response === 'ok' ? $('#replycomment').css('display', 'none') : '';
        response === 'ok' ? $('#reply_content').val('') : '';
    });

}

// comment 顶 
function ajaxup(replyupId) {
    // 判断登录状态
    if (Cookies.get('login') !== 'true') {
        alert('亲！你还没有登录');
        return;
    }
    $.post("/Commentup.htm", {replyupId: replyupId}, function(response) {
        response === 'dingd' ? '' : $('.ding' + replyupId).html('顶（' + response + '）');
    });
}

// comment 踩
function ajaxdown(replydownId) {
    // 判断登录状态
    if (Cookies.get('login') !== 'true') {
        alert('亲！你还没有登录');
        return;
    }
    $.post("/Commentdown.htm", {replydownId: replydownId}, function(response) {
        response === 'caid' ? '' : $('.cai' + replydownId).html('踩（' + response + '）');
    });
}


// output page
function ajaxpage(datapage) {
    $('.page').html(datapage);
}


// 载入评论数据
function ajaxcomment(page, order, id) {


    var _this = $('.list-con');
    var url = '/commentpage_' + page + '.htm?t=' + Math.random(10000, 9999);
    var comment = '';
//    console.log(id);
    $.getJSON(url, {page: page, order: order, id: id}, function(response) {

        if (response === 'false') {
            _this.html('没有评论数据');
            return;
        }

        // output page
        ajaxpage(response.page);
        var response = response.list;
        for (var v in response) {
            var lou = page == 1 ? (parseInt(v) + 1) : (parseInt(v)) + (((page - 1) * 5) + 1);
            // lt user
            comment += '<li replyId="' + response[v].id + '" class="list-con-li"><div class="lt"><div class="floor">' + (lou) + '楼</div><div class="img-con"><img  src="' + response[v].avatar + '" alt=""></div></div>';

            // rt comment start tag
            comment += '<div class="rt" style="word-wrap: break-word;">';

            // rt comment user name
            comment += '<div class="c-tp"><span class="time fr gray">' + date('Y年m月d日 H时i分s秒', response[v].create_time) + '</span><span class="user-name">' + response[v].nick_name + '</span></div>';

            // rt comment content
            comment += response[v].content;
//            comment += '<div class="c-txt">'+ response[v].content +'</div>';


            // comment reply
            comment += '<div class="c-op"><a href="javascript:void(0);" class="ding' + response[v].id + '" onclick="ajaxup(' + response[v].id + ')">顶（' + response[v].up_num + '）</a>|<a href="javascript:void(0);" class="cai' + response[v].id + '" onclick="ajaxdown(' + response[v].id + ')">踩（' + response[v].down_num + '）</a>|<a href="javascript:void(0);" class="last reply">回复</a></div>';

            // rt comment end tag
            comment += '</div></li>';

        }
//        $('.list-con-li').remove();
        _this.html(comment);

        // reply
        $('.c-list .reply').each(function() {

            $(this).insertRely($(this).parents('.list-con-li').attr('replyid'));
        });

    });

}