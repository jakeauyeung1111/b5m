/**
 * @version v1.0.0
 * @author shuai zou <weiyan@b5m.com>
 * @file zdm.js 
 */
var loadTimesss = 0;
var flagzzz = 0;
var pages;
var artilce_id = [];

/**
 * 初始化全局变量并加载第一页
 */

$(window).scroll(function()
{

    _scroll();

});

function _scroll()
{

    var height = document.body.scrollHeight || document.documentElement.scrollHeight;
    var scrollTop = document.body.scrollTop || document.documentElement.scrollTop;
    var viewHeight = document.documentElement.clientHeight;
    if (height - scrollTop - viewHeight < 150 && flagzzz === 0)
    {
        //下次加载
        ++loadTimesss;
        flagzzz = 1;
        loadTimesss <= 2 && loadData();

    }
}


/**
 * 加载数据
 */
function loadData()
{
    var pagen = parseInt(location.pathname.substring(6,12));
    if (!isNaN(pagen)) {
        var pagen = ((pagen - 1) * 3) + 1;
    } else {
        var pagen = 1;
    }

    if (pagen + loadTimesss > pages || loadTimesss === 2) {
        $('.page').css({display: 'block'});
        $('.loading').css({display: 'none'});

    }

    if (pages > 0 && pagen + loadTimesss > pages)
        return;
    var url = '/page_' + (pagen + loadTimesss) + '.htm';

    $.getJSON(url, function(response) {
        flagzzz = 0;
        var html = '';
        for (var v in response.list)
        {

            var data = response.list;
            
            if($.inArray(data[v].id,artilce_id)==-1){
                
                var title2 = (data[v].second_title2)?'<h3>' + data[v].second_title2 + '</h3>':'';
                
                html += '<li class="rec-list">';
                // product
                html += '<div class="right-prd">';
                if (data[v].product !== undefined && data[v].product.image_url) {
                    html += '<div class="img-con"><a href="/' + data[v].id + '.html"><img alt="' + data[v].product.image_title + '" src="' + cdnUrl+data[v].product.image_url + '"></a></div><a rel="nofollow" href="' + data[v].product.link + '" class="btn-buy"><span class="min">￥</span>' + data[v].product.price + '</a>';
                }
                html += '</div>';
                // artilce
                html += '<div class="m"><h2><a href="/' + data[v].id + '.html">' + data[v].title + '<span class="ct">' + data[v].second_title + '</span></a></h2>'+ title2 +'<div class="info"><span class="ar firstar"><i class="ic ic-person"></i>' + data[v].username + '</span>|<span class="ar"><i class="ic ic-date"></i>'+ date('Y年m月d日',data[v]['create_time']) +'</span>|<span class="ar"><i class="ic ic-digit"></i>' + data[v].category + '</span></div><div class="text"><p><a href="/' + data[v].id + '.html">' + data[v].summary + '</a><a href="/' + data[v].id + '.html" class="more orange">查看详情&gt;</a></p></div><div class="opa"><span dataid=' + data[v].id + ' class="ar"><i class="ic ic-ht av"></i>' + data[v].votes + '人</span><span class="ar"><i class="ic ic-eye"></i>' + data[v].views + '人</span><span class="ar"><i class="arrow-r"></i><span class="share-con"></span></span></div></div>';

                html += '</li>';

                // 把返回数据的 ID 压入数组
                artilce_id.push(data[v].id);
            }


        }
        // console.log(artilce_id);
        $('#article_list').append(html);
        pages = response.pages;


        // 分享
        $('.share-con').empty().each(function() {
            var url = 'http://' + location.host + $(this).parents('.m').find('.text p a').attr('href');
            var title = $(this).parents('.m').find('h2').text();
            var summary = $.trim($(this).parents('.m').find('.text p').text()).replace(/查看详情>$/g, '');

            $(this).creatShare(url, title, summary);

            $(this).find('.share').hover(function() {
                if ($(this).is(':animated'))
                    return;
                $(this).stop(true, true).animate({left: 0});
            }, function() {
                // if ($(this).is(':animated')) return;
                $(this).stop(true, true).delay(1000).animate({left: -($(this).width() - 25) + 'px'});
            })

        });
    });
}

$(function()
{
    loadData();
   
});










