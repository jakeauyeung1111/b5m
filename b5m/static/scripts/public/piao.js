$(document).ready(function() {
    $.setPlaceholder($('.J_search'), 'placeholder', {
        focus: function() {
            $(this).addClass('focusin');
        },
        blur: function() {
            $(this).removeClass('focusin');
        }
    });
    $('.page-input input').on('keyup', function(e) {
        var mpage =  parseInt($('.pagenum').attr('page'));
		    this.value = this.value.replace(/\D/g, "");
		    if(this.value >mpage){
		    this.value = mpage;
		    }
    });
});
var floor_t = ['生活服务', '旅游酒店','网上购物'];
function slPrice(pr)
{
	if(pr.indexOf('-') != -1)
	{
		return pr.split('-')[0];
	}
	else {
		return pr;
	}
}
function indexFloorTabCallback(data){
  if (checkhasdata(data.cindex,data.index))
	{
		getGoodsData(data);	
	}  
}
function getGoodsData(data)
{
    if (-1 != data.cindex)
    {
        if (checkhasdata(data.cindex,data.index))
        {
           var ctxt = $('.floor-head:eq('+data.cindex+') span:eq('+data.index+')').html();
            ctxt = $.trim(ctxt.replace(/<[^>]+>/g, ""));
            ctxt = encodeURI(ctxt);
            ext = data.cindex+'@'+data.index+'@'+ctxt;
            ctxt = ctxt==''?'':'/keyword/'+ctxt;
            $.ajax({
                type: "post",
                dataType: "jsonp",
                async: false,
                url: '/goodsApi/piao/t/floor'+ ctxt + '/callback/fillfloor/ext/' + ext +'/num/10', //TODO 修改
                data: ""
            });
        }
    }
}
function top10(keyword,pos)
{
	$.ajax({
      type: "post",
      dataType: "jsonp",
      async: false,
      url: '/goodsApi/piao/t/search/keyword/'+ encodeURI(keyword) + '/callback/filltop/ext/' + pos +'/num/10', //TODO 修改
      data: ""
  });	
}
function filltop(data)
{
	 if (data.msg == 'ok')
    {
        var buf = [];
        var cname=['color-red','color-yellow','color-gray','','','','','','',''];
        if(data.data.resources)
        {
        	      
	        for (var i = 0; i != 10; i++)
	        {
	        	 buf.push('<li><a target="_blank" href="/view_' + data.data.resources[i].DOCID + '.html">');
	        	 buf.push(data.data.resources[i].PlayName.substring(0,12) + '</a><s class="'+cname[i]+'">'+(i!=10?(i+1):'0'+(i+1))+'</s></li>');
	        }
	        var f = data.ext.replace(/["']/g, "");
	        $('.J_sideOddListBg:eq('+f+')').html(buf.join('')).hide().fadeIn(800);
        }
    }	
}
function checkhasdata(ci,i)
{
	if ($('.index-floor:eq('+ci+') .floor-list:eq('+i+')').find('a').size() == 0)	{
		return true;
	}
	else {
		return false;	
	}
}
function fillfloor(data)
{
    if (data.msg == 'ok')
    {
        var buf = [];
        if(data.data)
        {
	        for (var i = 0; i < 10; i++)
	        {
	        	 buf.push('<a href="/view_' + data.data[i].DOCID + '.html" target="_blank">');
	        	 buf.push('<img alt="' + data.data[i].PlayName + '" _src="http://cdn.bang5mai.com/b5m_img/' + data.data[i].PlayPicture + '"/>');
	        	 buf.push('<span>' + data.data[i].PlayName.substring(0,10) + '</span></a>');
	        }
	        var f = data.ext.replace(/["']/g, "");
	        f = f.split('@');
	        var ct = $('.city-head b').text().split('[')[0];
	        buf.push(' <a class="floor-more" href="/'+encodeURI(ct)+'/'+encodeURI(f[2])+'/index.html" target="_blank">更多></a>');
	        $('.index-floor:eq('+f[0]+') .floor-list:eq('+f[1]+')').html(buf.join('')).hide().fadeIn(800);
          $('.index-floor:eq('+f[0]+') .floor-list:eq('+f[1]+') img[_src]:visible').imglazyload({fadeIn:true,attr:'_src'});
        }
    }
}




