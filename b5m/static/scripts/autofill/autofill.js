/**
 * [autofill]
 */
;(function($,win,doc,undefined){
	/**
	 * [Autofill 构造函数]
	 * @param {[object]} elem [input框，绑定的dom元素]
	 * @param {[object]} options [自定义参数]
	 * site:当前站点
	 * url:搜索接口
	 * callback:回调函数
	 */
	function Autofill(elem,options){
		this.elem = elem;
		this.$elem = $(elem);
		this.elem.autocomplete = 'off';
		//自定义参数和默认参数合并
		this.opts = $.extend({},$.fn.autofill.defaults,options || {});

		this.site = this.opts.site;
		this.url = this.opts.url;
		this.callback = this.opts.callback;
		this.autofill = null;
		this.autofillContent = null;
		//初始化
		this.init();
	}
	Autofill.prototype = {
		/**
		 * [init 初始化]
		 */
		init:function(){
			this.createAutofill();
		},
		/**
		 * [createAutofill 创建autofill容器]
		 * @return {[type]} [description]
		 */
		createAutofill:function(){
				this.autofill = $('<div class="autofill"><div class="autofill-content"><div class="autofill-slider clear-fix"></div></div><div class="sub-box"></div><div class="autofill-arrow autofill-left autofill-left-disable"></div><div class="autofill-arrow autofill-right autofill-right-disable"></div> <div class="autofill-close"></div></div>')
							.appendTo(doc.body);
				this.autofillContent = this.autofill.find('.autofill-slider');
				this.close = this.autofill.find('.autofill-close');
				this.left = this.autofill.find('.autofill-left');
				this.right = this.autofill.find('.autofill-right');
				this.subBox = this.autofill.find('.sub-box');
				//绑定事件
				this.bindEvent();
				//设置autofill的样式
				this.setStyle();
				this.oldValue = '';
				this.x = 0;
				this.y = -1;
		},
		/**
		 * [setStyle 设置autofill样式，位置、大小等]
		 */
		setStyle:function(){
			//取得input框的位置信息，设置autofill样式
			var style = {
					left:this.$elem.offset().left,
					top:this.$elem.offset().top + this.$elem.outerHeight()
				};
			this.autofill.css(style);
		},
		/**
		 * [bindEvent description]
		 * @return {[type]} [description]
		 */
		bindEvent:function(){
			var _this = this,
				autoFill = _this.autofill;
			//绑定keyup事件
			_this.$elem.on('keyup',function(e){
				var keyVal = this.value,
					keyCode = e.which;

				_this.oldValue = keyVal;

				if(keyCode == 37 || keyCode == 38 || keyCode == 39 || keyCode == 40){
					return false;
				}

				// _this.changeHighlight(keyCode,keyVal);
				//获取数据
				_this.getData(keyVal);
				if(!keyVal){
					_this.hideAutoFill();
				}
			});
			//绑定focus和click事件
			_this.$elem.on('focus click.af',function(e){
				var keyVal = this.value;
				if(keyVal &&　_this.oldValue == keyVal){
					_this.showAutoFill();
				}else{
					//获取数据
					_this.getData(keyVal);
				}
				e.stopPropagation();
			});

			autoFill.on('click.af',function(e){
				e.stopPropagation();
			});
			autoFill.on('mouseenter','dl',function(){
				$(this).addClass('category-hover').siblings('dl').removeClass('category-hover');
			});
			autoFill.on('mouseleave','dl',function(){
				$(this).removeClass('category-hover');
			});

			$(doc).on('click.af',function(){
				_this.hideAutoFill();
			});

			_this.close.on('click.af',function(){
				_this.hideAutoFill();
			});
			//改变窗口后，重新定位autofill
			$(window).resize(function(){
				_this.setStyle();
			});
		},
		/**
		 * [changeHighlight 判断键盘键值]
		 * @param  {[Number]} keycode [键值]
		 */
		changeHighlight:function(keycode,keyval){
			if(!keycode || !keyval) return;
			var left = 37,
				top = 38,
				right = 39,
				down = 40;

			switch(keycode){
				case left:
					this.x--;
					break;
				case top:
					this.y--;
					break;
				case right:
					this.x++;
					break;
				case down:
					this.y++;
					break;
				default:
					this.getData(keyval);
			}
			this.setHighlight(this.x,this.y);
		},
		/**
		 * [setHighlight description]
		 */
		setHighlight:function(x,y){
			var $dls = this.autofillContent.find('dl'),
				items = [];

			$dls.each(function(i){
				var $dds = $(this).find('dd'),
					len = $dds.length;

					items[i] = [];

				for(var j = 0; j < len; j++){
					items[i][j] = $dds[j];
				}
			});

			for(var i = 0; i < items.length; i++){
				for(var j = 0; j < items.length; j++){
					if(i == x && j == y){
						$(items[x][y]).addClass('cur');
						this.setInputVal($(items[x][y]));				
					}else{
						$(items[i][j]).removeClass('cur');
					}
				}
			}

			// if($dds.length){
			// 	len = $dds.length;
			// }

			// if(index < 0){
			// 	index = len - 1;
			// }else if(index >= len){
			// 	index = 0;
			// }

			// this.iHigh = Number(index);
			// this.setInputVal($dds.eq(index));
			// $dds.removeClass('cur').eq(index).addClass('cur');
		},
		/**
		 * [setInputVal 设置input值]
		 * @param {[jQuery]} obj [当前对象]
		 */
		setInputVal:function(obj){
			if(!obj.length) return;
			this.elem.value = obj.find('a').text();
		},
		/**
		 * [bindSlider 滚动事件]
		 */
		bindSlider:function(len){
			var _this = this,
				_step = _this.autofillContent.find('.category').outerWidth()*3,
				count = Math.ceil(len / 3),
				i = 0;

			console.log(count);
			_this.right.on('click',function(){
				if(i == count - 1) {
	                return false;
	            }
				i++;
	            _this.autofillContent.stop(true,true).animate({left:-_step*i}, 400);
	            _this.left.removeClass('autofill-left-disable');

	            if(i==count-1) {
	                _this.right.addClass('autofill-right-disable');
	            }
			});

			_this.left.on('click',function(){

	            if(i>=count-1) {
	             	i=count-1;
	            }

	            if(i==0) {
	              	return false;
	            }

	 		    i--;
			    _this.autofillContent.stop(true,true).animate({left:-_step*i}, 400);
			    _this.right.removeClass('autofill-right-disable');
                if(i==0) {
                    _this.left.addClass('autofill-left-disable');
                }
			});
		},
		/**
		 * [hideAutoFill 隐藏autofill]
		 * 单独提出隐藏方法，方便以后扩展：比如隐藏之后执行callback
		 */
		hideAutoFill:function(){
			this.autofill.hide();
		},
		/**
		 * [showAutoFill 显示autofill]
		 * 单独提出显示方法，方便以后扩展：比如隐藏之后执行callback
		 */
		showAutoFill:function(){
			this.autofill.show();
		},
		/**
		 * [handleKeyVal 屏蔽特殊字符]
		 * @return {[string]}
		 */
		handleKeyVal:function(value){
            var val = $.trim(value);
           	if(!val) return;
           	var pattern =  new RegExp("[`~!@#$^&*%()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]"),
                rs = '';
            //特殊字符转为空
            for (var i = 0,len = val.length; i < len; i++) { 
				rs = rs + val.substr(i, 1).replace(pattern, ''); 
			} 
      	    return rs;
		},
		/**
		 * [getData 获取数据]
		 */
		getData:function(value){
			var _this = this,
				keyVal = _this.handleKeyVal(value);
			if(!keyVal) return;

			var params = 'keyWord=' + keyVal + '&city=' + '';
			$.ajax({
				url: this.url,
				async: false,
				dataType: 'jsonp',
				data: params,
				jsonp:'jsoncallback',
				success:function(data){
					//先判断返回的对象是否为空
					!_this.isEmptyObject(data) && _this.fillAutoFill(data);
				}
			});
		},
		/**
		 * [isEmptyObject 判断对象是否有数据]
		 * @param  {[type]}  obj [传入的对象，此处对象结构必须是
		 * {
		 * 	a:[
		 * 		{count:0,hl_value:"阿依莲"},
		 * 	 	{count:0,hl_value:"按摩器"}
		 * 	  ],
		 * 	b:[
		 * 		{count:0,hl_value:"阿依莲"},
		 * 	 	{count:0,hl_value:"按摩器"}
		 * 	  ]
		 * }
		 * 一般方法判断对象是否为空的标识是对象里面有没有内容,如：var obj = {},则为空，var obj = {a:""}不为空。
		 * 考虑到autofill数据结构问题，优化性能。下面：
		 * var obj = {a:[],b:[]} 也为空。
		 * @return {Boolean}     [true为空，false反之]
		 */
		isEmptyObject:function(obj){
			for(var i in obj){
				if(obj[i].length){
					return false;
				}
			}
			return true;
		},
        /**
         * [reset 搜索结果重设]
         */
        reset:function() {
            this.autofillContent.css({'left':0});
            this.right.removeClass('autofill-right-disable');
            this.left.addClass('autofill-left-disable');
            this.left.off();
            this.right.off();
        },
		/**
		 * [fillAutoFill 拼接，排序html]
		 */
		fillAutoFill:function(data){
            this.reset();
			var html = '',
				//存储分类返回的数据
				arrItem = [],
				//存储分类的标题
				arrTitle = [],
				//条目数
				arrLength = 0,
				_this = this;
			//遍历除去纠错词和当前站点的搜索结果放入数组
			for(var i in data){
				if(!data[i].length || i == 'correction' || i == this.site)
					continue
				arrItem.push(data[i]);
				arrTitle.push(i);
			}
			//按返回数据数目，从多到少排序
			arrItem.sort(function(a,b){
				return b.length - a.length;
			})
			//拼接html
			for(var i in arrItem){
				var itemContent = '';
				itemContent = this.serializeData(arrTitle[i],arrItem[i]);
				html += itemContent;
			}
			//单独处理当前站点的数据，放到首位
			if(data[this.site].length){
				html = this.serializeData(this.site,data[this.site]) + html;
			}
			//二级关键词遍历输出
			var subHtml = '';
			$.each(data[this.site],function(key,obj){
				var subItem = '';
				if(obj.subKeywords){
					for(var i in obj.subKeywords){
						subItem += '<a href="' + _this.getUrl(_this.site,obj.value + obj.subKeywords[i].value) +'" target="_blank" >' + obj.subKeywords[i].value + '</a>';
					}
				}
				subItem = '<div class="sub-item"><div class="tags clear-fix">' + subItem + '</div></div>';
				subHtml += subItem;
			});
			/**
			 * subBox：二级关键词的容器
			 * autofillContent：分类的容器
			 */
			this.subBox.empty().append(subHtml);
			this.autofillContent.empty().append(html);
			this.autofill.show();

			arrLength = arrItem.length;
			if(arrLength > 3){
				this.right.removeClass('autofill-right-disable');
				this.bindSlider(arrLength + 1);
			}else{
				this.right.addClass('autofill-right-disable');
			}

			$('.b5mo').find('dd').mouseenter(function(event) {
                var index = $('.b5mo').find('dd').index($(this));
                $('.b5mo').find('dd.cur').removeClass('cur');
                _this.subBox.hide().find('.sub-item.on').removeClass('on');
                if($(this).hasClass('item-sub')) {
                    _this.subBox.show().find('.sub-item').eq(index).addClass('on');
                    $(this).addClass('cur');
                }
			});
			_this.subBox.mouseleave(function(event) {
                $('.b5mo').find('.cur').removeClass('cur');
                $(this).hide();
			});
		},
		/**
		 * [serializeData 格式化数据]
		 * @param  {[object]} data [类目对象]
		 * @return {[string]} content [每一条类目对应的html]
		 */
		serializeData:function(site,data){
			var content = '',
				hlValue = '',
				keyVal = this.handleKeyVal(this.elem.value),
				_this = this;
			$.each(data,function(key,obj){
				//高亮代码
				hlValue = obj.value.replace(keyVal,'<b>' + keyVal + '</b>');
				if(obj.subKeywords && _this.site == 'b5mo' && site == 'b5mo'){
					content += '<dd class="item item-sub" data-attr="1001"><a class="item-txt" href="' + _this.getUrl(site,obj.value) + '" target="_blank" title="'+ obj.value +'">' + hlValue + '</a><span class="item-count">' + obj.count + '</span></dd>';
				}else{
					content += '<dd class="item" data-attr="1001"><a class="item-txt" href="' + _this.getUrl(site,obj.value) + '" target="_blank" title="'+ obj.value +'">' + hlValue + '</a><span class="item-count">' + obj.count + '</span></dd>';
				}
			});
			content = '<dl class="category ' + site + '"><dt>'+ _this.getItemTitle(site) +'</dt>' + content + '</dl>';
			return content;
		},
		/**
		 * [getItemTitle 获得类目的中文]
		 * @param  {[type]} title [类目title的拼音]
		 */
		getItemTitle:function(title){
            switch (title) {
                case 'b5mo':
                    return '商品';
                    break;
                case 'tejia':
                    return '特价';
                    break;
                case 'hotel':
                    return '酒店';
                    break;
                case 'ticketp':
                    return '票务';
                    break;
                case 'tourguide':
                    return '攻略';
                    break;
                case 'tourp':
                    return '旅游';
                    break;
                case 'tuanm':
                    return '团购';
                    break;
                case 'zdm':
                    return '值得买';
                    break;
                case 'she':
                    return '帮社区';
                    break;
                case 'haiwaip':
                    return '海外馆-商品';
                    break;
                case 'haiwaiinfo':
                    return '海外馆-资讯';
                    break;
                case 'guang':
                    return '逛';
                    break;
                default:
                    return '';
            }			
		},
		getUrl:function(txt,keyWord){
            var url = '';
            txt = $.trim(txt.replace(/open/g, ''));
            switch (txt) {
                case 'b5mo':
                    url = 'http://s.b5m.com/search/s/___image________________' + keyWord + '.html';
                    break;
                case 'hotel':
                    url = 'http://you.b5m.com/taoPage_-1_hotelSearchresult_' + keyWord + '_1_search';
                    break;
                case 'tourguide':
                    url = 'http://you.b5m.com/taoPage_-1_noteSearchresult_' + keyWord + '_1_hotNotes_search';
                    break;
                case 'ticketp':
                    url = 'http://piao.b5m.com/search_' + keyWord + '.html';
                    break;
                case 'tourp':
                    url = 'http://you.b5m.com/taoPage_-1_searchresult_' + keyWord + '_1_search';
                    break;
                case 'tejia':
                    url = 'http://tejia.b5m.com/taoPage_index_showIndex_' + keyWord;
                    break;
                case 'tuanm':
                    url = 'http://tuan.b5m.com/__' + keyWord;
                    break;
                case 'zdm':
                    url = 'http://zdm.b5m.com/keyword_' + keyWord;
                    break;
                case 'she':
                    url = 'http://shang.b5m.com/portal.php?mod=list&catid=1&keyword=' + keyWord;
                    break;
                case 'haiwaip':
                    url = 'http://haiwai.b5m.com/search/item/'+ encodeURIComponent(keyWord) +'________1___i'
                    break;
                case 'haiwaiinfo':
                    url = 'http://haiwai.b5m.com/search/info/' + encodeURIComponent(keyWord) + '__1';
                    break;
                case 'guang':
                    url = 'http://guang.b5m.com/s/p?k=' + keyWord;
                    break;
                default:
                    url = 'http://s.b5m.com/search/s/___image________________' + keyWord + '.html';
            }
            return url;			
		}
	}

	$.fn.autofill = function(options){
		return this.each(function() {
			new Autofill(this,options);
		});
	};

	/**
	 * [defaults 默认参数]
	 * 默认参数暴露出去
	 */
	$.fn.autofill.defaults = {
		site:'b5mo',
		url:'http://search.stage.bang5mai.com/allAutoFill.htm',//'http://search.b5m.com/allAutoFill.htm',
		callback:function(){}
	}


})(jQuery,window,document,undefined);

$('.J_autofill').autofill(
	{	
		site:'b5mo',
		callback:function(){
			console.log("callback");
		}
	}
);
