/**
 *Document:票
 *Creat:13-9-16 下午5:08
 *Author:junli.shen@b5m.com
 */
var piaoFed = {
    /*检测IE*/
    isIE:window.ActiveXObject||false?true:false,
    /*检测IE 6.0*/
    ie6:typeof document.body.style.maxHeight =="undefined"?true:false,
    /*城市选择*/
    citySelect:function(){
        var citySelectFun = function(){
            var cityBx = $('.city-select-head','.city-head');
            /*城市选择 */
            cityBx.find('a').click(function(){
                $('.city-head b').html($('.city-head b').html().replace($('.city-head b').text().replace('[切换城市]',''),$(this).text()));
                Cookies.set('city_sn',encodeURI($(this).text()),null,'/','.b5m.com');

                window.location.href='/'+encodeURI($(this).text())+'/index-p1.html';
                return false;
            });
            /*关闭城市选择*/
            $('.city-close-ico').click(function(){
                cityBx.hide();
                return false;
            });
            /*显示选择城市*/
            $('.city-head').click(function(){
                cityBx.show();
            });
        };
        $(function(){
            citySelectFun();
        });
    }(),
    /*商家比较-详情*/
    detailMcompare:function(){
        var compBx = '.J_piao-merchant-compare'
            ,liBg = $('u',compBx)
            ,listTable = $('.merchant-content>table',compBx)
            ,tabUl = $('ul',compBx)
            ,clickFlag = false;
        if($('li',compBx).length<6){
            $('.merchant-prev,.merchant-next',compBx).addClass('noClick');
        }

        $('li',compBx).each(function(i){
            $(this).attr('index',i);
        });

        $('.merchant-prev,.merchant-next',compBx).hover(function(){
            var _this = $(this);
            if(_this.hasClass('merchant-prev')){
                _this.addClass('merchant-prev-cur');
            }else{
                _this.addClass('merchant-next-cur');
            }
        },function(){
            var _this = $(this);
            if(_this.hasClass('merchant-prev')){
                _this.removeClass('merchant-prev-cur');
            }else{
                _this.removeClass('merchant-next-cur');
            }
        }).on({
            click:function(){
                if(clickFlag){ return false;}else{ clickFlag = true;}
                var moveLong ,moveDom ,thisLeft
                    ,curDom = $('li.cur',compBx)
                    ,curIndex = curDom.index()
                    ,_this = $(this);
                if(_this.hasClass('noClick')||$('ul','.J_piao-merchant-compare').is(":animated"))return false;
                if($(this).hasClass('merchant-prev')){
                    if(tabUl.is(":animated"))return false;
                    moveDom = $('li',compBx).last();
                    moveLong = moveDom.width();
                    moveDom.prependTo(tabUl);
                    tabUl.animate({marginLeft:"-="+moveLong+"px"},0).animate({marginLeft:"+="+moveLong+"px"},120,function(){
                        if(curIndex<5){
                            thisLeft = curDom.position().left+54;
                            liBg.stop(true,true).animate({left:thisLeft+"px"},0);
                        }else{
                            $('li:eq(5)',compBx).trigger('click');
                        }
                    });
                }else{
                    moveDom = $('li',compBx).first();
                    moveLong = moveDom.width();
                    if(tabUl.is(":animated"))return false;
                    tabUl.animate({marginLeft:"-="+moveLong+"px"},120,function(){
                        moveDom.appendTo(tabUl);
                        if(curIndex>0){
                            thisLeft = curDom.position().left+54;
                            liBg.stop(true,true).animate({left:thisLeft+"px"},0);
                        }else{
                            $('li:eq(0)',compBx).trigger('click');
                        }

                    }).animate({marginLeft:"+="+moveLong+"px"},0);
                }
                setTimeout(function(){
                    clickFlag = false;
                },400);
                return false;
            }
        });

        /*鼠标经过切换选项*/
        $('li',compBx).click(function(){
            var _this = $(this)
                ,thisLeft = _this.position().left+54
                ,thisIndex = _this.attr('index');
            if(_this.index()>5){
                return false;
            }
            _this.addClass('cur').siblings().removeClass('cur');
            liBg.stop(true,true).animate({left:thisLeft+"px"});
            listTable.eq(thisIndex).show().siblings().hide();
        });
    },
    /*分类更多显示*/
    moreType:function(){
        $('.more','.piao-class-select').on({
            click:function(){
                var _moreBx = $(this).siblings('div:first');
                if(_moreBx.is(":animated")){
                    return false;
                }
                if(_moreBx.is(":visible")){
                    _moreBx.slideUp('fast');
                    return;
                }
                _moreBx.slideDown('fast');
            }
        });
    },
    /*标签云*/
    tagCloud:function(domId){
        var radius = 85
            ,dtr = Math.PI/1000
            ,d=180
            ,mcList = []
            ,active = false
            ,lasta = 1
            ,lastb = 1
            ,distr = true
            ,tspeed=5
            ,size=40
            ,mouseX= 0
            ,mouseY= 0
            ,howElliptical= 1
            ,oDiv=document.getElementById(domId)
            ,aA=oDiv.getElementsByTagName('a')
            ,oTag=null;
        for(var i=0;i<aA.length;i++){
            oTag={};
            oTag.offsetWidth=aA[i].offsetWidth;
            oTag.offsetHeight=aA[i].offsetHeight;
            mcList.push(oTag);
        }
        sineCosine(0,0,0);
        positionAll();
        oDiv.onmouseover=function (){
            active=true;
        };
        oDiv.onmouseout=function (){
            active=false;
        };
        oDiv.onmousemove=function (ev){
            var oEvent=window.event || ev;
            mouseX=oEvent.clientX-(oDiv.offsetLeft+oDiv.offsetWidth/2);
            mouseY=oEvent.clientY-(oDiv.offsetTop+oDiv.offsetHeight/2);
            mouseX/=6;
            mouseY/=8;
        };
        setInterval(update,20);
        function update(){
            var a,b;
            if(active){
                a = (-Math.min( Math.max( -mouseY, -size ), size ) / radius ) * tspeed;
                b = (Math.min( Math.max( -mouseX, -size ), size ) / radius ) * tspeed;
            } else {
                a = lasta * 0.98;
                b = lastb * 0.98;
            }
            lasta=a;
            lastb=b;
            if(Math.abs(a)<=0.01 && Math.abs(b)<=0.01){
                return;
            }
            var c=0;
            sineCosine(a,b,c);
            for(var j=0;j<mcList.length;j++){
                var rx1=mcList[j].cx
                    ,ry1=mcList[j].cy*ca+mcList[j].cz*(-sa)
                    ,rz1=mcList[j].cy*sa+mcList[j].cz*ca
                    ,rx2=rx1*cb+rz1*sb
                    ,ry2=ry1
                    ,rz2=rx1*(-sb)+rz1*cb
                    ,rx3=rx2*cc+ry2*(-sc)
                    ,ry3=rx2*sc+ry2*cc
                    ,rz3=rz2;
                mcList[j].cx=rx3;
                mcList[j].cy=ry3;
                mcList[j].cz=rz3;
                per=d/(d+rz3);
                mcList[j].x=(howElliptical*rx3*per)-(howElliptical*2);
                mcList[j].y=ry3*per;
                mcList[j].scale=per;
                mcList[j].zoom=per<1?per-0.6:per;
                mcList[j].alpha=per;
                mcList[j].alpha=(mcList[j].alpha-0.6)*(10/6);
            }
            doPosition();
            depthSort();
        }

        function depthSort(){
            var i = 0,aTmp=[];
            for(i=0;i<aA.length;i++)
                aTmp.push(aA[i]);
            aTmp.sort(function (vItem1, vItem2){
                if(vItem1.cz>vItem2.cz){
                    return -1;
                }else if(vItem1.cz<vItem2.cz){
                    return 1;
                }
                return 0;
            });
            for(i=0;i<aTmp.length;i++)
                aTmp[i].style.zIndex=i;
        }

        function positionAll(){
            var phi=0 ,theta= 0 ,max=mcList.length ,i= 0 ,aTmp=[] ,oFragment=document.createDocumentFragment();
            for(i=0;i<aA.length;i++)
                aTmp.push(aA[i]);
            aTmp.sort(function (){
                return Math.random()<0.5?1:-1;
            });
            for(i=0;i<aTmp.length;i++){
                oFragment.appendChild(aTmp[i]);
            }
            oDiv.appendChild(oFragment);
            for( var i=1; i<max+1; i++){
                if( distr ){
                    phi = Math.acos(-1+(2*i-1)/max);
                    theta = Math.sqrt(max*Math.PI)*phi;
                } else {
                    phi = Math.random()*(Math.PI);
                    theta = Math.random()*(2*Math.PI);
                }
                mcList[i-1].cx = radius * Math.cos(theta)*Math.sin(phi);
                mcList[i-1].cy = radius * Math.sin(theta)*Math.sin(phi);
                mcList[i-1].cz = radius * Math.cos(phi);
                aA[i-1].style.left=mcList[i-1].cx+oDiv.offsetWidth/2-mcList[i-1].offsetWidth/2+'px';
                aA[i-1].style.top=mcList[i-1].cy+oDiv.offsetHeight/2-mcList[i-1].offsetHeight/2+'px';
            }
        }

        function doPosition(){
            var l=oDiv.offsetWidth/2 ,t=oDiv.offsetHeight/2;
            for(var i=0;i<mcList.length;i++){
                with(aA[i].style){
                    left=mcList[i].cx+l-mcList[i].offsetWidth/2+'px';
                    top=mcList[i].cy+t-mcList[i].offsetHeight/2+'px';
                    fontSize=Math.ceil(12*mcList[i].scale/2)+8+'px';
                    opacity=mcList[i].alpha;
                }
            }
        }

        function sineCosine( a, b, c){
            sa = Math.sin(a * dtr);
            ca = Math.cos(a * dtr);
            sb = Math.sin(b * dtr);
            cb = Math.cos(b * dtr);
            sc = Math.sin(c * dtr);
            cc = Math.cos(c * dtr);
        }
    },
    /*首页楼层切换*/
    indexFloorTab:function(){
        $('span','.floor-head').hover(function(){
            var _this = $(this)
                ,thisParent = _this.parent()
                ,thisIndex = thisParent.children('span').index(this)
                ,thisFloors = thisParent.parent().find('.floor-list')
                ,tabData = {}
                ,_parent;
            if(_this.hasClass('tabCur')){
                return false;
            }
            _parent = _this.parent();
            _this.addClass('tabCur').siblings().removeClass('tabCur');
            thisFloors.hide().eq(thisIndex).show();
            tabData['cindex'] = $('.index-floor').index(_parent.parent());
            tabData['index'] = _parent.find('span').index(this);
            tabData['txt'] = _this.text();
            if(typeof indexFloorTabCallback!="undefined"){
                indexFloorTabCallback(tabData);
            }
        });
    },
    /*侧边栏列表-奇数背景为灰色*/
    sideOddListBg:function(tags,colors){
        if(this.isIE){
            $(">"+(tags||"li")+":even",".J_sideOddListBg").css("backgroundColor",colors||'#F5F5F5');
        }
    },
    /*城市页Tab切换内容*/
    cityTab:function(){
        $('span','.piao-tab-nav').hover(function(){
            var _this = $(this)
                ,thisIndex = $('span','.piao-tab-nav').index(this);
            $(this).addClass('cur').siblings().removeClass('cur');
            $('.piao-tab-list').hide().eq(thisIndex).show();
        });
    },
    /*首页-加载执行*/
    indexLoad:function(){
        this.sideOddListBg();
        this.indexFloorTab();
    },
    /*分类筛选页-加载执行*/
    classSelectLoadFun:function(){
        this.tagCloud('J_piao-hot-search');
        this.moreType();
    },
    /*搜索页-加载执行*/
    searchLoadFun:function(){
        this.moreType();
    },
    /*详情页-加载执行*/
    detailLoadFun:function(){
      this.detailMcompare();
    },
    /*城市页-加载执行*/
    cityLoadFun:function(){
        this.cityTab();
    }
};