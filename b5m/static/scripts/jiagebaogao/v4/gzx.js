var gzxFed = window.gzxFed || {};

gzxFed.indexFun = function() {
    this.indexlInit();
};
gzxFed.detailFun = function() {
    this.datailInit();
};

gzxFed.searchFun = function() {
    this.searchInit();
};

/**
 * 热销商品
 */
gzxFed.hotGoodsFun=function(){
    this.hotGoodsInit();
};
gzxFed.hotGoodsInit=function(){
    var _this = this;
    _this.aotuFill();
    _this.corclePic();
    _this.hotGoodsInit.menuSlide();
};

gzxFed.hotGoodsInit.menuSlide=function(){
    var $menu = $("#G_menu");

    $menu.on('mouseover',"ul li",function(){
        $(this).addClass('open');
    });
    $menu.on('mouseout',"ul li",function(){
        $(this).removeClass('open');
    });

    $('.crumb').on('mouseover',".cr-zs",function(){
        $(this).addClass('open');
    });
    $('.crumb').on('mouseout',".cr-zs",function(){
        $(this).removeClass('open');
    });

};

gzxFed.indexlInit=function(){
    var _this=this;

    _this.aotuFill();

    //画圆
    /*var $circle = $('.circle');
    _this.circleFun($circle);*/
    _this.corclePic();

    //Flash画圆
    $('.pop-goods').each(function(index,n){
        var value= $('#inc-pointerswf'+index).text(),
            id ='inc-pointerswf'+index ;
        swfobject.embedSWF("http://staticcdn.b5m.com/images/jiagebaogao/v4/zhishu/zhishu.swf",id, "230", "230", "9.0.0", "expressInstall.swf",{'value':value,'pageindex':true},{'wmode':'transparent'});
    });

    /*人气商品 floor s*/
    _this.indexlInit.incTab('#pop-tita','#pop-goods-w','.pop-goods','');
    /*人气商品 floor e*/
    /*高分商品 floor s*/
    _this.indexlInit.incTab('#scores-tita','#scores-goods-w','','.scores-goods');
    /*高分商品 floor e*/
    /*历史低价 floor s*/
    _this.indexlInit.incTab('#his-price-tita','#his-goods-w','','.scores-goods');
    /*历史低价 floor e*/
    /*热销精品 floor s*/
    _this.indexlInit.incTab('#hots-tita','#hots-goods-w','','.scores-goods');
    /*热销精品 floor e*/

    _this.indexlInit.preiceFun();
};
gzxFed.corclePic=function(){
    var $circleId = $('.circle'),
        arrPo =[];

    for(var i = 0; i<9; i++){
        for(var j = 0; j< 12 ; j++){
            var arrSo = [j*60,i*60];
            arrPo.push(arrSo);
        };
    };

    $circleId.each(function(i,n){
        var jsonStr = $(this).attr('data-info'),
            numb = $.parseJSON(jsonStr)['num'];

       // $(this).addClass('circle'+numb);

        $(this).css('background-position',-arrPo[numb][0]+'px ' + -arrPo[numb][1]+'px ');


    });


};

gzxFed.aotuFill=function(){
    $('.J_autofill').autoFill('', 'b5mo');
};
/*首页选项卡s*/
gzxFed.indexlInit.incTab=function(titId,contId,contItem,goodsItems){
    if(contItem){
        var $tabTits=$(contId).find(contItem).length,
            $titHtmls='';

        if($tabTits > 1){
            for(var i=0;i<$tabTits;i++){
                i==0?$titHtmls+='<a href="javascript:void(0);" class="cur"></a>': $titHtmls+='<a href="javascript:void(0);"></a>';
            }
            $(titId).html($titHtmls);
        }else{
            $(titId).hide();
        };
        $(titId).on('click','a',function(e){
            var idx=$(this).index();
            $(this).addClass('cur').siblings('a').removeClass('cur');
            $(contId).find(contItem).eq(idx).css('z-index','2').siblings(contItem).css('z-index','0');
            e.preventDefault();
        });
    }else{
        var $goodsItem=$(contId).find(goodsItems),
            $liLen=$goodsItem.find('li').length,
            $liW=$goodsItem.find('li').width()+6,
            $ulItems=$(contId).find(goodsItems).children('ul'),
            $tabTits=$liLen/5,
            $titHtmls='';
        if($tabTits > 1){
            for(var i=0;i<$tabTits;i++){
                i==0?$titHtmls+='<a href="javascript:void(0);" class="cur"></a>': $titHtmls+='<a href="javascript:void(0);"></a>';
            }
            $(titId).html($titHtmls);
        }else{
            $(titId).hide();
        };
        $ulItems.width($liW*$liLen+'px');
        $(titId).on('click','a',function(e){
            var idx=$(this).index();
            $(this).addClass('cur').siblings('a').removeClass('cur');
            $ulItems.stop(true,false).animate({'left':-idx*980+'px'},500);
            e.preventDefault();
        });
    };
};
/*首页选项卡e*/
/*价格报告s*/
gzxFed.indexlInit.preiceFun=function(){
    $('#jgbg-cont-w').find('.jgbg-tiem dt').hover(function(){
        var $this = $(this).parent('.jgbg-tiem');

       // $this.siblings('.jgbg-tiem').find('.jgbg-pic').css('display','none');
        $this.siblings('.jgbg-tiem').stop().animate({'width':'157'},300)
        $this.stop().animate({'width':'662'},300);

    });
};
/*价格报告e*/
/**
 * 商品详情页调用
 */
gzxFed.datailInit=function(){
    this.aotuFill();

    var swfValue= $('#pointerswf').attr('data'),
        swfId ='pointerswf' ;
    swfobject.embedSWF("http://staticcdn.b5m.com/images/jiagebaogao/v4/zhishu/zhishu.swf",swfId, "230", "230", "9.0.0", "expressInstall.swf",{'value':swfValue},{'wmode':'transparent'});
    this.datailInit.showAllGoods();
    this.datailInit.goodsSortFun();
    this.datailInit.seldectMall();
    this.datailInit.zhishuPop();
    this.datailInit.goodsPicSlide();
//    this.CircleFun();
    var zhishuJson=$.parseJSON($('#zhishu-pic').attr('data-info'));
    this.datailInit.drawFun({number:[zhishuJson['sales_amount'],zhishuJson['service_index'],zhishuJson['price_index'],zhishuJson['hot_index']]});

    this.corclePic();
    /*var $circle = $('.circle');

    $.each($circle,function(i, el){
        var $this = $(el);
        var metaData = $this.data('info'),
            id = $this.attr('id');
        new CircleFun(id,metaData.color,metaData.num).init();
    });*/

};
/**
 * 搜索结果页调用
 */
gzxFed.searchInit=function(){
    this.aotuFill();

    this.fixDom('.side-l','nav-fix');
    this.searchInit.showAllGoodsClass();
    this.searchInit.showOtherProperty();
    this.searchInit.priceSearchFun();
    /*var $circle = $('.circle');
    this.circleFun($circle);*/
    this.corclePic();
};
/**
 * 指数
 */
gzxFed.zhishuFun = function(id,value) {
    swfobject.embedSWF("http://staticcdn.b5m.com/images/jiagebaogao/v4/zhishu/zhishu.swf",id, "230", "230", "9.0.0", "expressInstall.swf",{'value':value,'pageindex':true},{'wmode':'transparent'});
};
/**
 * 帮我买指数弹出框
 */

gzxFed.datailInit.zhishuPop = function() {
    $('.zs-btn').find("a").click(function(){
        $('.pic-items').css('overflow','visible');
        $('.pop-zhishu').stop(true,false).animate({'left':'0px'},200);
    });
    $('.close-btn').find('a').click(function(){
        $('.pop-zhishu').stop(true,false).animate({'left':'-730px'},200,function(){
            $('.pic-items').css('overflow','hidden');
        });
    });
};
/**
 * 展示其余商品
 */
gzxFed.datailInit.showAllGoods=function(){
    var _this=this;
    $('.tab-shop-cont').on('click','.all-shop a',function(){
        var goodslist=$(this).parent('.all-shop').siblings('.shop-list'),
            $text =$(this).text();
        if($(this).attr('class') == 'icon add'){
            var $text1 = $text.replace('展开','收起');
            $(this).attr('class','icon remove');
            $(this).text($text1);
            //goodslist.slideDown(200);
            goodslist.css('height','auto');
        }else{
            var $text1 = $text.replace('收起','展开');
            $(this).attr('class','icon add');
            //goodslist.slideUp(200);
            goodslist.css('height','0');
            $(this).text($text1);
        };
    });
};
/**
 * 商品排序
 */
gzxFed.datailInit.goodsSortFun=function(){
    var _this=gzxFed;
    $('.shop-seq-item').find('li').click(function(){
        var $this=$(this),
            nUlr=window.location.pathname.substring(8).replace('.html',''),
            nId=$(this).find('a').attr('type'),
            index=$(this).index(),
            $cont=$('.tab-shop-cont'),
            $zpCheck = $('.mall-compare span').eq(0).children('i').hasClass('checkbox-checked'),
            $tmCheck = $('.mall-compare span').eq(1).children('i').hasClass('checkbox-checked'),
            $tText = $('.mall-compare span').eq(1).text();


        if($zpCheck){
            _this.datailInit.getGoodsSort(index,nId,nUlr,'',true);
        }else if($tmCheck){
            _this.datailInit.getGoodsSort(index,nId,nUlr,$tText);
        }else if($zpCheck && $tmCheck){
            _this.datailInit.getGoodsSort(index,nId,nUlr,$tText,true);
        }else{
            _this.datailInit.getGoodsSort(index,nId,nUlr);
        };
        $this.hasClass('cur')?$this.siblings('li').removeClass('cur'):$this.addClass('cur').siblings('li').removeClass('cur');
    });
};
/**
 * 商城选择
 */
gzxFed.datailInit.seldectMall=function(){
    var _this=gzxFed,
        $this=$('.mall-compare').find('span'),
        nUlr=window.location.pathname.substring(8).replace('.html','');

    $('.mall-compare').find('span').eq(0).click(function(){
        var index=$('.shop-seq-item').find('.cur').index(),
            nId=$('.shop-seq-item').find('.cur').children('a').attr('type'),
            _thisChild = $(this).children('i'),
            $tText = $('.mall-compare span').eq(1).text();

        if(_thisChild.hasClass('checkbox-checked')){
            $(this).children('i').removeClass('checkbox-checked');
            if($this.eq(1).children('i').hasClass('checkbox-checked')){
                _this.datailInit.getGoodsSort(index,nId,nUlr,$tText);
            }else{
                _this.datailInit.getGoodsSort(index,nId,nUlr);
            };

        }else{
            _thisChild.addClass('checkbox-checked');
            if($this.eq(1).children('i').hasClass('checkbox-checked')){
                _this.datailInit.getGoodsSort(index,nId,nUlr,$tText,true);
            }else{
                _this.datailInit.getGoodsSort(index,nId,nUlr,'',true);
            };

        };
    });

    $('.mall-compare').find('span').eq(1).click(function(){

        var index=$('.shop-seq-item').find('.cur').index(),
            nId=$('.shop-seq-item').find('.cur').children('a').attr('type'),
            _thisChild = $(this).children('i'),
            $tText = $('.mall-compare span').eq(1).text();

        if(_thisChild.hasClass('checkbox-checked')){
            $(this).children('i').removeClass('checkbox-checked');
            if($this.eq(0).children('i').hasClass('checkbox-checked')){
                _this.datailInit.getGoodsSort(index,nId,nUlr,'',true);
            }else{
                _this.datailInit.getGoodsSort(index,nId,nUlr);
            };
        }else{
            _thisChild.addClass('checkbox-checked');
            if($this.eq(0).children('i').hasClass('checkbox-checked')){
                _this.datailInit.getGoodsSort(index,nId,nUlr,$tText,true);
            }else{
                _this.datailInit.getGoodsSort(index,nId,nUlr,$tText);
            };

        };
    });
};
/**
 * 获取商品排序
 * @param curIndex点击当前的按钮索引值
 * @param nId当前按钮id值
 * @param nUlr浏览器地址
 */
gzxFed.datailInit.getGoodsSort=function(curIndex,nId,nUlr,mallStr,isGen){
    var _this=gzxFed;
    if(!arguments[3]) mallStr = null;
    if(!arguments[4]) isGen = false;
    $.ajax({
        url:'http://gzx.b5m.com/tabdata.html?t=' + Math.random(10000, 9999),
        type:"GET",
        async:true,
        data:{order:nId,o_id:nUlr,order_type:'ASC',source:mallStr,isgenuine:isGen},
        dataType:"jsonp",
        jsonp: 'jsonpCallback',
        beforeSend:function(){
            $('#priceTrendDiv').show();
            $('.tab-shop-cont').eq(0).html('');
        },
        success:function(json){
            if(json.code == 100){
                $('#priceTrendDiv').hide();
                _this.datailInit.goodsSortIndex(curIndex,json);
            };

        }
    });
};
/**
 * 所有商品排序列表方法
 * @param 数据
 */
gzxFed.datailInit.goodsSortIndex=function(curIndex,json){
    //console.log(data);
    var allHtmls='',
        _this=gzxFed,
        datas=json.data;
    for(var i in datas){
        var allHtml='<dl class="tab-shop-items">',
            len = datas[i].length,
            num = len - 1;
            allHtml +=_this.datailInit.goodsSortInow(datas[i],i,curIndex);
            if(num > 0){
                allHtml +='<dd class="icon all-shop"><a href="javascript:void(0);" class="icon add">展开其余' + num + '件商品</a></dd>';
            };
            allHtml +='</dl>';
            allHtmls +=allHtml;
    }

    $('.tab-shop-cont').eq(0).html(allHtmls);
    _this.corclePic();

};
/**
 * 获取某个商品排序列表
 * @param 数据
 */
gzxFed.datailInit.goodsSortInow=function(data,index,curIndex){
    var _this=gzxFed
        ,nDatas=data
        ,lens=nDatas.length
        ,allHtml='';
    for(var i = 0; i<lens; i++){
        /**
         * 当只有一条数据时
         */
        if(i == 0){
            allHtml += '<dt class="shop-first"><table class="shop-tiem"><tr><td width="415"><a href="' + nDatas[i]['url'] + '">' + nDatas[i]['title'] + '</a> </td>';
            allHtml += '<td width="130"><a href=""><img src="http://staticcdn.b5m.com/images/jiagebaogao/v4/' + index + '_logo.png"></a></td><td width="95"><div id="circle-' + i + '-' + index + '-' + curIndex + '" class="data-numb circle" data-info=\'{"num":"' + nDatas[i]['composite_index'] +'"}\'></div></td>';
            allHtml += '<td width="72"><p class="price"><b>￥</b>' + nDatas[i]['cur_price'] + '</p></td><td width="74" class="zs"><span class="' + _this.datailInit.trend(nDatas[i]['delta_price']) + '"></span></td><td width="122"><p>' + _this.datailInit.fuwu(nDatas[i]['s_guarantee']) + '</p></td>';
            allHtml += '<td width="72"><a href="" class="jy-c1">建议购买</a></td></tr></table></dt>';
        }

        allHtml += '<dd class="shop-list"><table class="shop-tiem">';

        if(lens > 0){
            if(i > 0){
                allHtml += '<tr><td width="130"></td><td width="415"><a href="' + nDatas[i]['url'] + '">' + nDatas[i]['title'] + '</a> </td>'
                allHtml += '<td width="95"><div id="circle-' + i + '-' + index + '-' +  curIndex + '" class="data-numb circle" data-info=\'{"num":"' + nDatas[i]['composite_index'] +'"}\'></div></td><td width="72"><p class="price"><b>￥</b>' + nDatas[i]['cur_price'] + '</p></td><td width="74" class="zs"><span class="' + _this.datailInit.trend(nDatas[i]['delta_price']) + '"></span></td>'
                allHtml += '<td width="122"><p>' + _this.datailInit.fuwu(nDatas[i]['s_guarantee']) + '</p></td><td width="72"><a href="" class="jy-c1">建议购买</a></td></tr>'
            }
        }

        allHtml += '</table></dd>';
    }

    return allHtml;
};
/**
 * 服务保障
 * @param data数据
 */
gzxFed.datailInit.fuwu=function(data){
   var fData=data,
       fHtml='',
       fDataArr=[];

    for( var i in fData){
        if(i == 'refunding'){
            fData[i] += '天退货';
        }else if(i == 'warranty'){
            fData[i] +='天保修';
        };
        fDataArr.push(fData[i]);
    };
    for(var i = 0; i<fDataArr.length; i++){
        if(i<3){
            fHtml +=fDataArr[i]+'<br>';
        };
    }
    return fHtml;

};
/**
 * 趋势图
 * @param 趋势数据
 * @returns 趋势的class名称
 */
gzxFed.datailInit.trend=function(data){
    var className = '';
    if(data < 0){
        className = 'icon p-down';
    }else if(data > 0){
        className = 'icon p-up';
    }else{
        className = 'icon p-flat';
    };
    return className;
};
/**
 * 商品图片切换
 */
gzxFed.datailInit.goodsPicSlide=function(){
    var iNow=0;
    $this=$(".pic-slide"),
        liW=$this.find(".pic-list li").width(),
        allNumb=$this.find(".pic-list li").length;
    $this.on("click",".r-btn",function(){
        if(iNow<allNumb-1){
            iNow++;
        }else{
            iNow=0;
        };
        picSlide();
    });
    $this.on("click",".l-btn",function(){
        if(iNow>0){
            iNow--;
        }else{
            iNow=allNumb-1;
        };
        picSlide();
    });
    function add0(numb){
        if(numb<10){
            numb="0"+numb;
        };
        return numb;
    };
    function picSlide(){
        var numNow=iNow+1,
            sLi=$this.find(".pic-list li");
        $('.pic-list').width(liW*allNumb);
        $this.find(".iNow").text(add0(numNow));
        $this.find(".allNumb").text(add0(allNumb));
        $('.pic-list').stop(true,false).animate({'left':-iNow*liW+'px'},400)
    };
    picSlide();
};

/**
 * 小圆圈指数
 * @param 元素id
 * @param 颜色
 * @param 指数
 * @constructor 小圆圈指数函数
 */
CircleFun = function(elem,color,num){
        this.elem = elem;
        this.color = color;
        this.num = num;
        this.R = 22;
        this.iTime = 5;
        this.width = 7;
        this.cPos = [26,25];
        this.colors = ["#F38F01","#D2DF00","#00D5A9"];
}

CircleFun.prototype.init = function(){

    var paper = Raphael(this.elem, 51,50),
        color = '';

    if(this.num >= 0 && this.num < 60){
        color = this.colors[0];
    }else if(this.num >= 60 && this.num < 80){
        color = this.colors[1];
    }else if(this.num >= 80 && this.num <= 100){
        color = this.colors[2];
    }

    var t = paper.text(25,25,this.num).attr({
        "font-size":20,
        "fill":color
    });


    if((document.all) ? true : false){
        this.iTime = parseInt(this.iTime/2);
        if(Number(navigator.appVersion.split(";")[1].replace(/[\s]+[^0-9]+/g,""))<9){
            this.cPos[0] -= 2;
            this.cPos[1] -= 2;
        }
    }

    this.drawCircle(paper,color);
}

CircleFun.prototype.drawCircle = function(paper,color){

    var _this = this,
        dom = paper.path(_this.sector(0,1)).attr({'stroke':color,'stroke-width':_this.width}),
        i = 0,
        maxAngle = parseInt(_this.num * 360 / 100);

    clearInterval(flag);

    var flag = setInterval(function(){

        if(i < maxAngle){
            i = i + 2;
        }

        dom.animate({"path":_this.sector(i,1)});

    },_this.iTime);
}

CircleFun.prototype.sector = function(startAngle,isArc,rs) {

    var startAngle = 91-startAngle,
        Rs = rs||this.R,
        rad = Math.PI / 180,
        x1 = this.cPos[0] + Rs * Math.cos(-startAngle * rad),
        x2 = this.cPos[0] + Rs * Math.cos(-90 * rad),
        y1 = this.cPos[1] + Rs * Math.sin(-startAngle * rad),
        y2 = this.cPos[1] + Rs * Math.sin(-90 * rad);

    if(startAngle<=-269){
        return ["M", x1, y1, "A", Rs, Rs, 0, +(90 - startAngle > 180), 0, x2, y2,"z"].join(",");
    }
    if(isArc){
        return ["M", x1, y1, "A", Rs, Rs, 0, +(90 - startAngle > 180), 0, x2, y2].join(",");
    }
    return ["M", cPos[0], cPos[1], "L", x1, y1, "A", R, R, 0, +(90 - startAngle > 180), 0, x2, y2, "z"].join(",");
}

CircleFun.prototype.isIE = function(){
    return (document.all) ? true : false;
}

gzxFed.circleFun = function(obj){

    $.each(obj,function(i, el){
        var $this = $(el);
        var metaData = $this.data('info'),
            id = $this.attr('id');
        new CircleFun(id,metaData.color,metaData.num).init();
    });
}
/**
 * 帮我买指数大圆
 * @param args指数函数
 */
gzxFed.datailInit.drawFun=function(args){
    var Paper = Raphael("zhishu-pic", 536,268)
        ,rad = Math.PI / 180
        ,cPos = [268,268]
        ,angles = [[135,180],[90,135],[45,90],[0,45]]
        ,fillColor = ['#ff4e00','#ffb424','#ff66ff','#33cc89']
        ,sFillColor = ['#ffcab3','#ffe9be','#ffd1ff','#c2f0dc']
        ,txts = ['热度','价格','服务','销量']
        ,txtDomArr = []
        ,nums = args['number']
        ,circleDomsTranslate = [[-82,-10],[-34,-72],[34,-72],[80,-10]]
        ,Rs = [254,172]
        ,angleDom = []
        ,Len = nums.length- 1
        ,r = Rs[1]-76
        ,rs = 228
        ,cX= 0
        ,cY=0
        ,zIndex = 0;
    function getActs(cx, cy, r, startAngle, endAngle,isArc) {
        var x1 = cx + r * Math.cos(-startAngle * rad),
            x2 = cx + r * Math.cos(-endAngle * rad),
            y1 = cy + r * Math.sin(-startAngle * rad),
            y2 = cy + r * Math.sin(-endAngle * rad);
        if(isArc){
            return ["M", x1, y1, "A", r, r, 0, +(endAngle - startAngle > 180), 0, x2, y2];
        }
        return ["M", cx, cy, "L", x1, y1, "A", r, r, 0, +(endAngle - startAngle > 180), 0, x2, y2, "z"];
    }
    for(var i =0;i<4;i++){
        angleDom[i] = Paper.path(getActs(cPos[0],cPos[1],Rs[0],angles[i][0],angles[i][1])).attr({"stroke":"#fff","stroke-width":1,fill:fillColor[i]});
        Paper.path(getActs(cPos[0],cPos[1],Rs[1]*nums[i]/88,angles[i][0],angles[i][1])).attr({"stroke":"#fff","stroke-width":0,fill:sFillColor[i]});
        /*Paper.text(cPos[0],cPos[1],nums[i]).attr({"font-size":24,"fill":fillColor[i]}).translate(circleDomsTranslate[i][0],circleDomsTranslate[i][1]-20);*/
    }
    for(var i =3;i>=0;i--){
        var nAngles = 22.5*(i*2+1);
        zIndex = 3-i;
        cX = Math.cos(nAngles*Math.PI/180) *r;
        cY= -Math.sin(nAngles*Math.PI/180) *r;
        Paper.circle(cPos[0],cPos[1],22).attr({"stroke-width":0,fill:"#fff"}).translate( cX,cY);
        Paper.text(cPos[0],cPos[1],nums[Len-i]).attr({"font-size":24,"fill":fillColor[3-i]}).translate(cX,cY);
        txtDomArr[i] = Paper.text(cPos[0],cPos[1],txts[i]).attr({"stroke-width":0,"fill":"#fff","font-size":16,"font-family":"Microsoft Yahei","font-weight":"normal"}).translate(Math.cos(nAngles*rad) *rs,-Math.sin(nAngles*rad) *rs).rotate(90-nAngles);

    };
    function domBindEvent(dom,index){
        dom.hover(function(){
            var nAngles = 22.5*((3-index)*2+1);
            angleDom[index].animate({path:getActs(cPos[0],cPos[1],Rs[0]+14,angles[index][0],angles[index][1])},120);
            txtDomArr[3-index].animate({transform:"t"+Math.cos(nAngles*rad) *(rs+7)+",-"+Math.sin(nAngles*rad) *(rs+7)+"r"+(90-nAngles)+"s1.2,1.2"},100);
            $('.zhishu-items').find('li').eq(index).show().siblings('li').hide();
        },function(){
            var nAngles = 22.5*((3-index)*2+1);
            angleDom[index].animate({path:getActs(cPos[0],cPos[1],Rs[0],angles[index][0],angles[index][1])},120);
            txtDomArr[3-index].animate({transform:"t"+Math.cos(nAngles*rad) *rs+",-"+Math.sin(nAngles*rad) *rs+"r"+(90-nAngles)+"s1,1"},100);
        });
    };
    setTimeout(function(){
        for(var i =3;i>=0;i--){
            domBindEvent(Paper.path(getActs(cPos[0],cPos[1],Rs[0],angles[i][0],angles[i][1])).attr({"stroke":"#fff","stroke-width":0,fill:"#fff",opacity:0}),i);
        }
    },0)

};
/**
 * 固定位置
 * @param obj 绑定元素Id
 * @param className  增加的class名字
 * @returns {boolean}
 */
gzxFed.fixDom=function(obj,className){
    var dom = $(obj),
        h = $(obj).offset().top,
        isIE6 = window.XMLHttpRequest ? false : true;
    if (!dom.length) return false;
    if (!isIE6) {
        $(window).on('scroll', function() {

            if ($(window).scrollTop() >= h) {
                dom.addClass(className);
            } else {
                dom.removeClass(className);
            }
        });
    }
};
/**
 * 显示商品分类
 */
gzxFed.searchInit.showAllGoodsClass=function(){
    var $obj=$('#J_category_nav'),
        $itemLinks = $obj.find('a'),
        $hideDls=$obj.find('dl:hidden');
    $obj.find('dt').not('.no-bg').click(function(){
        var parDl=$(this).parent('dl');
        if(parDl.hasClass('cur')){
            parDl.removeClass('cur');
        }else{
            parDl.addClass('cur');
        };
    });
    //阻止冒泡
    $itemLinks.on('click',function(e){
        e.stopPropagation();
    });
    /**
     * 查看更多分类
     */
    $('.more-category').on('click',function(){
        if($(this).hasClass('more-category-open')){
            $(this).removeClass('more-category-open');
            $hideDls.hide();
        }else{
            $(this).addClass('more-category-open');
            $hideDls.show();
        };
    });

};
/**
 * 过滤选择属性
 */
gzxFed.searchInit.showOtherProperty=function(){
    var $hidePropers=$('#J_filter').find('.filter-item:hidden'),
        $filter=$('#J_filter'),
        $filItem=$filter.find('.filter-item'),
        $more_c = $filter.find('.filter-more');
    /**
     * 显示其他属性
     */
    if($filItem.length>7){
        $more_c.show();
    }else{
        $more_c.hide();
    };

    $('#J_more').on('click',function(e){
        e.stopPropagation();
        if($(this).hasClass('open')){
            $(this).removeClass('open');
            $hidePropers.show();
        }else{
            $(this).addClass('open');
            $hidePropers.hide();
        };
    });
    /**
     *显示更多
     */
    $filItem.each(function(i){
       var $this=$(this),
           aLinks=$this.find('.filter-lists a'),
           $moreChage=$this.find('.filter-act');
        /**
         * 显示展开
         */
        if(aLinks.length>7){
            $this.children('.show-more').show();
        };
        console.log(aLinks.length);
        /**
         * 展开/收起事件
         */
        $this.on('click','.show-more',function(e){
            if($this.hasClass('filter-open')){
                $this.removeClass('filter-open filter-multiply');
                $(this).removeClass('open').find('span').text('展开');
            }else{
                $this.addClass('filter-open');
                $(this).addClass('open').find('span').text('收起');
            };
            e.stopPropagation();
        });
        /**
         * 更多绑定事件
         */
        $moreChage.on('click','.btn-multiple',function(e){
            $this.hasClass('filter-open')?$this.addClass('filter-multiply'):$this.addClass('filter-open filter-multiply');
            e.stopPropagation();
        });
        /**
         * 取消绑定事件
         */
        $moreChage.on('click','.btn-cancle',function(e){
            $filItem.find('.show-more').trigger('click');
            e.preventDefault();
        });
        /**
         * 属性绑定事件
         */
        aLinks.click(function(e){
            $(this).hasClass('cur')?$(this).removeClass('cur'):$(this).addClass('cur');
            //多选情况下，阻止a的默认事件
            if($(this).parents('.filter-item').hasClass('filter-multiply')){
                e.preventDefault();
            }
        });
        //确定多选事件
        $this.on('click','.btn-sure',function(e){
            var $this = $(this),
                $parent = $this.parents('.filter-item'),
                $filter = $parent.find('.filter-lists').find('.cur'),
                len = $filter.length,
                title = $parent.find('dt').attr('title'),
                data = '',
                url = location.pathname;

            $filter.each(function(index, el) {
                data += title + ',' + $(el).text() + ';';
            });

            //提交数据
            location.href= url.replace(/(__[^_]*)(_)/,'$1'+ data + '_' );
            $parent.hide();
            e.preventDefault();

        });
    });

};
/**
 * 自定义价格搜索按钮
 */
gzxFed.searchInit.priceSearchFun=function(){
    $('.price-input').on('click','input.btn',function(){
        var data = '',
            url = location.pathname,
            $inputValue = $(this).siblings('.txt'),
            v1 = $inputValue.eq(0).val(),
            v2 = $inputValue.eq(1).val(),
            re = /^[0-9]*$/;
        if( !v1 || !v2){
            alert('不能为空');
        }else{
            if(!re.test(v1) || !re.test(v2)){
                alert('您输入的格式不正确！请输入数字。');
            }else{
                if( v1 > v2 ){
                    alert('请从小到大！')
                }else if( v1 == v2 ){
                    alert('请输入不同的数字');
                }else{
                    data += $inputValue.eq(0).val() + '-' +$inputValue.eq(1).val();
                    location.href= url.replace(/(__[^_]*__)[^_]*_/,'$1'+ data + '_' );
                }
            };
        }
    });
};