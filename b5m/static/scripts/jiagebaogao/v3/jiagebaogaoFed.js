var jiagebaogaoFed = {
    ie:function(){
        return typeof window.ActiveXObject!="undefined";
    }(),
    ie6:function(){
        return typeof document.getElementsByTagName('body')[0].style.maxHeight == "undefined"
    }(),
    ieLower:function(){
        return navigator.appName == "Microsoft Internet Explorer"&&Number(navigator.appVersion.match(/[6-9]+./i))<10;
    }(),
    /*初始*/
    init:function() {

        var _this = this;
        var floorArr = [],floorArrScroll=[];
        var floorIndex = $('.index-floor');
        floorIndex.each(function(i,n) {
            floorArr.push($(this).offset().top);
        });
        floorIndex.eq(0).addClass('animate');
        floorArrScroll = floorArr.slice();

        if($(window).scrollTop() > 300) {
            floorIndex.eq(0).addClass('animate');
            delete floorArrScroll[0];
            _this.ballon();
        }

        $(window).scroll(function() {

            if(floorArrScroll[0]) {
                floorIndex.eq(0).addClass('animate');
                delete floorArrScroll[0];
                _this.ballon();
            }

            var scroll = document.documentElement.scrollTop || document.body.scrollTop || 0;

            $.each(floorArr,function(i,n) {
                if(floorArrScroll[i] && scroll >= floorArrScroll[i]-500) {

                    if(i==0) {
                        _this.ballon();
                    }

                    floorIndex.eq(i).addClass('animate');
                    delete floorArrScroll[i];
                }
            });

        });

    },
    /*热销商品*/
    hotGoods:function(obj){
        var _this=$(obj).find(".goods-tiems");
        _this.eq(0).find("dd").show();
        _this.hover(function(){
            $(this).find("dd").show().end().siblings("dl").find("dd").hide();

        });
    },
    /*autofll*/
    autofll:function(){
        $(".J_autofill").autoFill("","b5mo");
    },
    /*转载分享*/
    shareFn:function(){
        $('.zdm-item').each(function(i,n) {
            bShare.addEntry({
                product:$(n).find('h2 a').text(),
                url:$(n).find('h2 a').attr('href'), 
                pic:$(n).find('.pic').children('img').attr('lazy-src'),
                summary:$(n).find('.text').html(),
                price:$(n).find('input.product_price').val() || "暂无报价"
            });
        });
    },
    /*侧边导航
    sideShare:function(){
        var _this=$(".cloud-nav"),
            oh=_this.offset().top,
            isIE6 = window.XMLHttpRequest ? false : true;
        _this.hover(function(){
            _this.find("dd").stop(true,true).slideDown();
        },function(){
            _this.find("dd").stop(true,true).slideUp();
        });
        if (!_this.length) return false;
        if (!isIE6) {
            $(window).on('scroll.zdm', function() {
                if ($(window).scrollTop() >= oh) {
                    _this.addClass("cloud-fix");
                } else {
                    _this.removeClass("cloud-fix");
                }
            });
        }
    },*/
    ballon:function() {
        var Balloon = function(el){
            this.oBalloon = {
                obj: $(el),
                strength: {
                    x: 0.02 + Math.random()/10,
                    y: 0.02 + Math.random()/10
                },
                amplifier: Math.random()*3,
                angle: 0
            }
            this.setTimer();
        };
        Balloon.prototype.setTimer = function(){
            this.move();
            var oSelf = this;
            setTimeout(function(){ oSelf.setTimer() }, 20);
        };
        Balloon.prototype.move = function(){
            var oBalloon = this.oBalloon;
            oBalloon.angle += oBalloon.strength.x;
            oBalloon.obj.css({
                top:oBalloon.amplifier * Math.sin(oBalloon.angle),
                left:oBalloon.amplifier * Math.sin(oBalloon.angle)
            });
        };
        var times = [1,0.9,0.8,0.7,0.6,0.5,0.4,0.3],
            elems = $('.g-picre01 div').not('.a1');
            elems.each(function(i,n) {
          setTimeout(function(){
            $(n).addClass('dis');
            new Balloon(n);
          },1500+times[i]*1000);

        });

    },
    /*最后执行*/
    lishizuidiLoadFun:function(){
        var _this=this;
        _this.init();
        _this.hotGoods(".gustiry-low");
        _this.hotGoods(".hot-goods");
        _this.shareFn();
        /*_this.sideShare();*/
        _this.autofll();
    }
};
$(function(){
    jiagebaogaoFed.lishizuidiLoadFun();
});