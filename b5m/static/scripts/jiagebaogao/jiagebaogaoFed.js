var jiagebaogaoFed = {
    ie:function(){
        return typeof window.ActiveXObject!="undefined";
    }(),
    ie6:function(){
        return typeof document.getElementsByTagName('body')[0].style.maxHeight == "undefined"
    }(),
    ieLower:function(){
        return navigator.appName == "Microsoft Internet Explorer"&&Number(navigator.appVersion.match(/[6-9]+./i))<10;
    }(),
    /*初始*/
    init:function() {

        var floorArr = [],floorArrScroll=[];
        var floorIndex = $('.index-floor');
        floorIndex.each(function(i,n) {
            floorArr.push($(this).offset().top);
        });
        floorIndex.eq(0).addClass('animate');
        floorArrScroll = floorArr.slice();

        if($(window).scrollTop() > 300) {
            floorIndex.eq(0).addClass('animate');
            delete floorArrScroll[0];
        }

        $(window).scroll(function() {

            var scroll = document.documentElement.scrollTop || document.body.scrollTop || 0;

            if(floorArrScroll[0]) {
                floorIndex.eq(0).addClass('animate');
                delete floorArrScroll[0];
            }

            $.each(floorArr,function(i,n) {
                if(floorArrScroll[i] && scroll >= floorArrScroll[i]-500) {
                    floorIndex.eq(i).addClass('animate');
                    delete floorArrScroll[i];
                }
            });

        });

    },
    /*侧边导航*/
    sideMenu:function(){
        var _this = this;
        if(_this.ie){
            $("li:odd",".side-menu").addClass("odd");
        }
    },
    /*首页banner*/
    indexBnner:function(){
        if(!jiagebaogaoFed.ieLower)return false;
        var indexBnnerSunLine = function(){
            $('.bnn-v1-sunline').animate({opacity:0},1000,function(){
                $('.bnn-v1-sunline').animate({opacity:1},1000,indexBnnerSunLine);
            });
        };
        indexBnnerSunLine();
    },
    /*横幅广告-升幅最大*/
    shengfuZuiDa:function(){
        var animateS = $('<s></s>').appendTo($(".shengfu-arrows"));
        if(jiagebaogaoFed.ieLower&&!jiagebaogaoFed.ie6){
            animateS.animate({zoom:1},1000);
        }
    },
    /*横幅广告-降幅最大*/
    jiangfuZuiDa:function(){
        var animateS = $('<s></s>').appendTo($(".jiangfu-arrows"));
        if(jiagebaogaoFed.ieLower&&!jiagebaogaoFed.ie6){
            animateS.animate({zoom:1},1000);
        }
    },
    lishizuidiLoadFun:function(){
        var _this = this;

        _this.init();
        _this.sideMenu();
        _this.indexBnner();
        _this.shengfuZuiDa();
        _this.jiangfuZuiDa();

    }
};
