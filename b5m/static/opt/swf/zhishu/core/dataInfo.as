﻿package core  {
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.display.Sprite;
	
	import com.greensock.*;
	import com.greensock.plugins.*;
	import com.greensock.easing.*;
	
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import flash.utils.setTimeout;
	
	import flash.geom.Point;
	import flash.media.Video;
 
	
	public class dataInfo extends MovieClip  {


		private var paramInfo:Array;  //数据
		private var initPos:int = 115;  //初始info坐标
		private var arrAngle:Array = [15,5,-5,-15];  //初始角度
		
		private var currentInfo:int;
		
		private var tagFormat:TextFormat = new TextFormat();
		private var infoFormat:TextFormat = new TextFormat();
		
		private var infoBg:Sprite = new infobg();
		private var infoBgIconArr:Array = [];
		
		
		public function dataInfo(paramInfo:Array) {
			
			 this.paramInfo = paramInfo;
			 
			 createInfoText();
			 
			 createInfoLine();
			 
			 createInfoBg();
			 
		}
		
		
		private function createInfoBg():void {
			infoBg.x = 442;
			infoBg.alpha=0;
			for(var i:int = 0;i<3;i++) {
				infoBg.getChildAt(i+3).alpha=0;
			}
			addChild(infoBg);
		}
		
		private function createInfoText():void {
			
			tagFormat.size = 14;
			tagFormat.align = 'center';
			tagFormat.font= '\5FAE\8F6F\96C5\9ED1';
			tagFormat.color = 0x666666;
			
			infoFormat.size = 18;
			infoFormat.align = 'center';
			infoFormat.font= 'Arial';
			infoFormat.color = 0xffffff;
			 
		
			var arrText:Array = ['价格','热度','销量','服务'];
			var distance:Number = 138;
			
			for(var i:int =0;i<4;i++) {
				
				var _textBox:Sprite = new Sprite();
				var _text:TextField = new TextField();
				
				//if(i===0) {
					//tagFormat.bold = true;
				//}else {
					//tagFormat.bold = false;
				//}
				
				_text.text = arrText[i];
				_text.width = 40;
				_text.height = 25;
				_text.mouseEnabled = false;
				_text.setTextFormat(tagFormat);
				 
				_textBox.addChild(_text);
				 
				
				_textBox.buttonMode = true;
				_textBox.x = initPos + distance*Math.sin((arrAngle[i]+90)*Math.PI/180) - 20;
				_textBox.y = initPos + distance*Math.cos((arrAngle[i]+90)*Math.PI/180) - 10;
				 

				 _textBox.name = 'textBox'+i;
				
				addChildAt(_textBox,0);
			}
			
		}
		
		
		private function textBoxMouseOverHandler(event:MouseEvent):void {
			tagFormat.bold = true;
			((event.target as Sprite).getChildAt(0) as TextField).setTextFormat(tagFormat);
		    infoMouseOverHandler(getIndex(event.target as Sprite));
		}
		
		
		private function textBoxMouseOutHandler(event:MouseEvent):void {
			tagFormat.bold = false;
			((event.target as Sprite).getChildAt(0) as TextField).setTextFormat(tagFormat);
			infoMouseOutHandler(getIndex(event.target as Sprite));
		}
		
		
		private function infoMouseOverHandler(index:int):void {
			
			TweenMax.to(getChildByName('circleInfo'+index),.2, {scaleX:1.2,scaleY:1.2});
			infoBg.alpha = 1;
			 
			infoBg.getChildAt(1).y = infoBgIconArr[index]+6;
			for(var i:int = 0;i<4;i++) {
				infoBg.getChildAt(i+2).alpha=0;
				infoBg.getChildAt(index+2).alpha=1;
			}
		}
		
		
		private function infoMouseOutHandler(index:int):void {
			TweenMax.to(getChildByName('circleInfo'+index),.2, {scaleX:1,scaleY:1});
			//infoBg.alpha = 0;
		}
		
		
		private function getIndex(target:Sprite):int {
			var _i:String = target.name;
			 _i = _i.replace(/[^\d]+/,'');
			return int(_i);
		}
		
		
		private function createInfoLine():void {
			
			TweenPlugin.activate([EndArrayPlugin]);
			
			var distance:Number = 162;
			
			var posArr:Array = [];
			 
			for(var i:int =0;i<4;i++) {
				
			var x1:Number = initPos + distance * Math.sin((arrAngle[i]+90)* Math.PI/180);
			var y1:Number = initPos + distance * Math.cos((arrAngle[i]+90)* Math.PI/180);
			
			var x2:Number = initPos + (distance + (paramInfo[i]/100) * 150) * Math.sin((arrAngle[i]+90)*Math.PI/180);
			var y2:Number = initPos + (distance + (paramInfo[i]/100) * 150) * Math.cos((arrAngle[i]+90)*Math.PI/180);
			
			posArr.push([x1,y1],[x2,y2]);
			infoBgIconArr.push(y2);
  
 			var lineBox:Sprite = new Sprite();
			lineBox.name = 'lineBox'+i;
			 
			addChildAt(lineBox,1);
				
			}
			
			drawLineInfo(this.getChildByName('lineBox0') as Sprite,0,posArr[0],posArr[1]);
			drawLineInfo(this.getChildByName('lineBox1') as Sprite,1,posArr[2],posArr[3]);
			drawLineInfo(this.getChildByName('lineBox2') as Sprite,2,posArr[4],posArr[5]);
			drawLineInfo(this.getChildByName('lineBox3') as Sprite,3,posArr[6],posArr[7]);
			  
			 
		}
		
		private function drawLineInfo(line:Sprite,index:int,arr1:Array,arr2:Array):void {
			
			line.graphics.clear();
			line.graphics.moveTo(arr1[0],arr1[1]);
			setTimeout(function() {
					
			TweenLite.to(arr1,.6, {endArray:arr2,onUpdate:function() {
  			 line.graphics.lineStyle(3,getLineColor(paramInfo[index]),1);
   			 line.graphics.lineTo(arr1[0],arr1[1]);
   			 line.graphics.endFill(); 
			},onComplete:drawCircleInfo,onCompleteParams:[index,arr2[0],arr2[1]]});   
			},index*100);
			
		}
		
		private function drawCircleInfo(index:int,x1:Number,y1:Number):void {
			
			var circle:Sprite = new Sprite();
			
			circle.x = x1;
			circle.y = y1;
			
			circle.name = 'circleInfo' + index;
			
			var _text:TextField = new TextField();
			_text.text = paramInfo[index];
			
			_text.width = 60;
			_text.height = 40;
			_text.x = -30;
			
			
			var radius:Number;
			if(paramInfo[index] < 60) {
				infoFormat.size = 16;
				_text.y = -10;
				radius = 60*.3;
			}else if (paramInfo[index] >= 60 && paramInfo[index] < 80){
				infoFormat.size = 22;
				_text.y = -14;
				radius = paramInfo[index]*.3;
			}else if (paramInfo[index] >=80) {
				infoFormat.size = 28;
				_text.y = -18;
				radius = 80*.3;
			}
			
			  
			_text.mouseEnabled = false;
			_text.setTextFormat(infoFormat);
			
			circle.scaleX = .1;
			circle.scaleY = .1;
			circle.alpha = .1;
			circle.addChild(_text);
			 
			circle.graphics.beginFill(getLineColor(paramInfo[index])); 
			circle.graphics.drawCircle(0, 0,radius); 
			circle.graphics.endFill();
			
			circle.buttonMode = true;
			addChild(circle);
			
			
			//if(index===0) {
				
			//}else {
				
			//}
			TweenMax.to(circle,1, {scaleX:1,scaleY:1,alpha:1,ease:Back.easeOut});
			 
			TweenMax.to(infoBg,1, {alpha:1,ease:Back.easeOut});
			
			circle.addEventListener(MouseEvent.MOUSE_OVER,function(event:MouseEvent) {  infoMouseOverHandler(getIndex(event.target as Sprite)); });
			circle.addEventListener(MouseEvent.MOUSE_OUT,function(event:MouseEvent) {  infoMouseOutHandler(getIndex(event.target as Sprite)); });
			 
			getChildByName('textBox'+index).addEventListener(MouseEvent.MOUSE_OVER,textBoxMouseOverHandler);
			getChildByName('textBox'+index).addEventListener(MouseEvent.MOUSE_OUT,textBoxMouseOutHandler);
			
		}
		 
		
		private function getLineColor(val:Number):Number {
			var _color:Number = 0xf38f01;
			if(val > 50 && val < 80) {
			  _color = 0xd2df00;
			}else if(val >=80) {
			  _color = 0x00d5a9;
			}
			return _color;
		}
		
		
	}
	
}