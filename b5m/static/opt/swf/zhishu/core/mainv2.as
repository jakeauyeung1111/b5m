﻿package core {
	
	import flash.display.MovieClip;

	import core.dataShow;
	import core.dataInfo;

	public class mainv2 extends MovieClip {
		
		
		public function mainv2() {
			
			
			var _data:String = '{"value":65,"pageindex":true,"info":[88,60,97,50]}';
			var datas:Object = loaderInfo.parameters['datas'] !== undefined ? JSON.parse(loaderInfo.parameters['datas']) : JSON.parse(_data);
			  
		    addChild(new dataShow(datas.value,datas.pageindex));
			
			addChild(new dataInfo(datas.info));
			
		}
	}

}