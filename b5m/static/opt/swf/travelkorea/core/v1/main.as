﻿package core.v1  {
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.display.Sprite;
 
	
	import com.greensock.*;
	import com.greensock.plugins.*;
	import com.greensock.easing.*;
	
	
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.events.MouseEvent;
	
	import flash.net.navigateToURL;
	import flash.net.URLRequest;
	
	
	public class main extends MovieClip {
		
		private var datas:Object; 
		private var startClick:Boolean = false; //点击的城市是否为起点
		private var startCity:String = null; //选中起点的城市sprite
		
		private var _cityContainer:Sprite = new cityContainer(); //city sprite container
		private var _plane:Sprite = new plane();
		
		private var orientToBezier:Boolean;
		
		public function main() {
			 
			//test data
			var __data:String = '{"data":[{"name":"首尔","x":337,"y":80,"position":"top","url":"http://www.qq.com","info":"Flash中的绘图是通过Graphics类实现的，Graphics类直接继承Object类，有两种绘图方法，一是定义绘图样式的方法；一种是用于绘制和清除图形的方法。"},{"name":"江原道","x":412,"y":56,"position":"bottom","url":"http://www.b5m.com","info":"首尔"},{"name":"仁川","x":306,"y":88,"position":"bottom","url":"http://www.baidu.com","info":"仁川仁川仁川仁川仁川仁川仁川仁川仁川仁川仁川仁川仁川仁川仁川仁川仁川仁川仁川仁川仁川仁川仁川仁川仁川仁川仁川仁川仁川仁川仁川仁川"},{"name":"京畿道","x":346,"y":110,"position":"bottom","url":"http://www.baidu.com","info":"首尔首尔首尔京畿道京畿道京畿道京畿道京畿道京畿道京畿道京畿道首尔首尔首尔首尔首尔首尔首尔首尔首尔首尔首尔首尔首尔"},{"name":"郁陵岛","x":561,"y":86,"position":"bottom","url":"http://www.baidu.com","info":"首尔"},{"name":"大邱","x":430,"y":176,"position":"top","url":"http://www.baidu.com","info":"首尔"},{"name":"庆州","x":469,"y":215,"position":"top","url":"http://www.baidu.com","info":"首尔"},{"name":"釜山","x":459,"y":269,"position":"top","url":"http://www.baidu.com","info":"首尔"},{"name":"济州岛","x":308,"y":395,"position":"top","url":"http://www.baidu.com","info":"首首尔首尔首尔首尔首尔首尔首尔首尔首尔首尔首尔首尔首尔首尔首尔首尔首尔首尔首尔首尔首尔首尔首尔首尔首尔首尔首尔首尔首尔首尔首尔首尔首尔首尔首尔首尔首尔首尔首尔首尔首尔首尔首尔首尔首尔首尔首尔首尔首尔首尔首尔尔"}]}';
         
			datas = loaderInfo.parameters['datas'] != null ? JSON.parse(loaderInfo.parameters['datas']) : JSON.parse(__data);
			  
			addChildAt(_cityContainer,2);
			 
			 
			//遍历数据，定位城市x,y
			for(var i:Number =0;i < datas.data.length; i++) {
				
				var item:Sprite = new city();
				item.mouseEnabled = false;
				item.name = 'item'+i;
				
				item.x = datas.data[i].x;
				item.y = datas.data[i].y;
				 
				var _text:TextField = item.getChildAt(0) as TextField;
				_text.text = datas.data[i].name;
				_text.mouseEnabled = false;
				
				if(datas.data[i].position === 'bottom') {
				  _text.y = 5;
				}
				
				//绘制弹出框
				drawPop(datas.data[i],i);
				
				var _point:Sprite = item.getChildAt(1) as Sprite;
				_point.buttonMode = true;
				_point.mouseEnabled = true;
				_point.addEventListener(MouseEvent.MOUSE_OVER,pointMouseOverHandler);
				_point.addEventListener(MouseEvent.MOUSE_OUT,pointMouseOutHandler);
				_point.addEventListener(MouseEvent.CLICK,pointClickHandler);
				_cityContainer.addChild(item);

			}
			
			container.addEventListener(MouseEvent.CLICK,containerClickHandler);
			
			
		}
		
		private function drawPop(data:Object,i:Number):void{
			
				var data:Object = data;
				var info:String = data.info;
				var offsetX:Number = data.x;
				var offsetY:Number = data.y;
				var infoW:Number = 197;
				var infoH:Number = 52;
				var infoR:Number = 10;
				
				var mySprite:Sprite = new Sprite(); 
				
				mySprite.graphics.beginFill(0xFFFFFF); 
				mySprite.graphics.lineStyle(2,0x70CFF8,1);
				mySprite.x = offsetX - (infoW/2);
				mySprite.y = offsetY - infoH - 20;
				
				//添加灯泡
				var _lamp:Sprite = new lamp();
				_lamp.x = 15;
				_lamp.y = 25;
				_lamp.mouseEnabled = false;

				mySprite.addChild(_lamp);
				
				mySprite.alpha = 0;
				mySprite.mouseEnabled = false;
				mySprite.name = 'labelbox' + i;
				
				var label:TextField = new TextField(); 
				label.width = 160; 
				label.text = info; 
				label.wordWrap = true;
				label.multiline = true;
				label.x = 30; 
				label.y = 0; 
				label.mouseEnabled = false;
				
				var textFormat:TextFormat = new TextFormat();
				textFormat.size = 12;
				textFormat.leading = 2;
				label.setTextFormat(textFormat);
				
				label.height = label.textHeight + 10;
				
				var shape:Sprite=new Sprite();
				mySprite.addChild(shape);
				shape.graphics.beginFill(0xFFFFFF);
				 
				mySprite.addChild(label);
				
				
				
				//动态调整弹出框的高度
				if(label.textHeight < 50){
					mySprite.graphics.drawRoundRect(0,0,infoW,infoH,infoR,infoR);
				}else{
					mySprite.graphics.drawRoundRect(0,0,infoW,label.textHeight + 6,infoR,infoR);
					 
				}
				
				this.addChild(mySprite);
				
				//调整箭头方向
				if(offsetY < 100 ){
					mySprite.y = offsetY + 20;
					shape.graphics .lineStyle(2,0x70CFF8,1);
					shape.graphics .moveTo(infoW/2-10,2);
					shape.graphics .lineTo(infoW/2,-6);
					shape.graphics .lineTo(infoW/2+10,2);
					shape.graphics .lineStyle(2,0xFFFFFF,1);
					shape.graphics .lineTo(infoW/2+10,2);
				}else{
					
					if(label.textHeight >= 50) {
						infoH = label.textHeight+6;
						mySprite.y = mySprite.y - label.textHeight +50;
					}
					 
					shape.graphics .lineStyle(2,0x70CFF8,1);
					shape.graphics .moveTo(infoW/2-10,infoH -2);
					shape.graphics .lineTo(infoW/2,infoH + 8);
					shape.graphics .lineTo(infoW/2+10,infoH -2);
					shape.graphics .lineStyle(2,0xFFFFFF,1);
					shape.graphics .lineTo(infoW/2+10,infoH - 2);
				}
		}
		
		private function pointMouseOverHandler(event:MouseEvent):void {
			setCountryMouseEffect(true,event.target as Sprite);
			this.getChildByName('labelbox' + event.target.parent.name.replace('item','')).alpha = 1;
		}
		
		private function pointMouseOutHandler(event:MouseEvent):void {
			setCountryMouseEffect(false,event.target as Sprite);
			this.getChildByName('labelbox' + event.target.parent.name.replace('item','')).alpha = 0;
		}
		
		private function pointClickHandler(event:MouseEvent):void {
			
			var element:Sprite = event.target as Sprite;
			 
			/* setCountryClickEffect(true,element);
			 
			if(!startClick) {
			  startClick = true;
			  startCity = element.parent.name;
			  
			}else {
				
			  var firstElement:Sprite = (_cityContainer.getChildByName(startCity) as Sprite).getChildAt(1) as Sprite;
				 
			  if(element !==  firstElement) {
				setPlane(element,firstElement);
			  }else {
				navigateToURL(new URLRequest(datas.data[element.parent.name.replace('item','')].url),'_self');
			  }
			  
			}*/
			navigateToURL(new URLRequest(datas.data[element.parent.name.replace('item','')].url),'_self');
			
		}
		
		private function setPlane(element:Sprite,firstElement:Sprite):void {
			
			var firstIndex:Number = Number(firstElement.parent.name.replace('item',''));
				var index:Number = Number(element.parent.name.replace('item',''));
				  
				addChild(_plane);
				
				var x1:Number = datas.data[firstIndex].x;
				var x2:Number = datas.data[index].x;
				var y1:Number = datas.data[firstIndex].y;
				var y2:Number = datas.data[index].y;
				
				_plane.x = x1;
				_plane.y = y1;
				
				 _cityContainer.graphics.lineStyle(1,0xffffff,1); 
   			     
				if(Math.abs(x2 - x1) <= 40 || Math.abs(y2 - y1) <= 40) {
					_plane.rotation = Math.atan2(y2-y1,x2-x1)*180/Math.PI + 90;
					orientToBezier = false;
					TweenMax.to(_plane,2, {bezier:[{x:x2, y:y2}],onUpdate:planeUpdateHandler,onUpdateParams:[x1,y1,x2,y2], orientToBezier:orientToBezier,ease:Expo.easeInOut,onComplete:planeCompleteHandler,onCompleteParams:[index]}); 
				}else {
					orientToBezier = true;
					TweenMax.to(_plane,2, {bezier:[{x:x1,y:y2},{x:x2, y:y2}],onUpdate:planeUpdateHandler,onUpdateParams:[x1,y1,x2,y2], orientToBezier:orientToBezier,ease:Expo.easeInOut,onComplete:planeCompleteHandler,onCompleteParams:[index]});
				}
				  
			
		}
		
		private function planeUpdateHandler(x1:Number,y1:Number,x2:Number,y2:Number):void {
			
			   if(orientToBezier) {
				_plane.rotation += 90;
			   }
			 
			 
			//画线性路径
			  _cityContainer.graphics.moveTo(x1,y1);
			  _cityContainer.graphics.lineStyle(1,0xf4f728,2); 
   			 // _cityContainer.graphics.lineTo(_plane.x,_plane.y);
			  _cityContainer.graphics.beginFill(0xf4f728);
			  _cityContainer.graphics.drawCircle(_plane.x,_plane.y,2); 
			  _cityContainer.graphics.endFill();
			
		}
		
		private function planeCompleteHandler(index:Number):void {
			//跳转
			navigateToURL(new URLRequest(datas.data[index].url),'_self');
		}
		
		
		private function containerClickHandler(event:MouseEvent):void {
			if(startCity) {
				setCountryClickEffect(false,(_cityContainer.getChildByName(startCity) as Sprite).getChildAt(1) as Sprite);
				startCity = null;
				startClick = false;
			}
		}
		
		
		private function setCountryMouseEffect(flag:Boolean,element:Sprite):void {
			if(flag) {
				TweenMax.to(element,.5, {glowFilter:{color:0xff0012, alpha:1, blurX:10, blurY:10}});
			}else {
				TweenMax.to(element,.5, {glowFilter:{alpha:0}});
			}
		}
		
		private function setCountryClickEffect(flag:Boolean,element:Sprite):void {
			if(flag) {
				TweenLite.to(element, .5, {scaleX:1.5,scaleY:1.5, ease:Back.easeInOut});
				//TweenLite.to(element, .5, {tint:0xfdae06});
			}else {
				TweenLite.to(element, .5, {scaleX:1,scaleY:1, ease:Back.easeInOut});
				//TweenLite.to(element, .5, {removeTint:true});
			}
		}
		
		
	}
	
}
