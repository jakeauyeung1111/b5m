/**
 *引流页面-前端JS
 *  @author:Gaotian
 *  @time:2013-10-31
 */
/*页面模版*/
;var _Templates_ = {
    /*banner模版*/
    bnnr:"<a href='./redirect.html?dst=$link' target='_blank' title='$title'><img src='$src' alt='$title'/></a>"
    /*列表模版*/
    ,list:'<li i="$index"><div class="gd"><div class="img-con"><a href="./redirect.html?dst=$href" target="_blank"><img src="$src"></a></div><div class="ins"><a href="./redirect.html?dst=$href" target="_blank">$title</a></div><div class="bt"><a class="more" target="_blank" style="display: inline;" href="./redirect.html?dst=$more">更多</a><span class="prize" style="display: inline;">¥<b>$price</b></span></div></div></li>'
};
;var yinLiuFed = {
    config:null,//页面显示数据
    urlParam:null,//当前页面URL参数
    id:function(id){//通过id选择Dom
        return document.getElementById(id);
    },
    /**
     * 模版解析函数
     * @param configData {
     *     tpl:模版代码，
     *     data:解析数据
     *     minNum:解析的最小索引
     *     maxNum:解析最大索引
     * }
     * */
    jsTemplate:function(configData){
        if(!configData['tpl']||!configData['data']){
            alert("请确认解析的模版和数据是否填写完整！");
            return;
        }
        var tmp = configData['tpl']
            ,data = configData['data']
            ,getStr = ''
            ,minNum
            ,maxNum
            ,nData;
        if(configData['maxnum']){
            maxNum = configData['maxnum']<data.length?configData['maxnum']:data.length;
        }else{
            maxNum = data.length;
        }
        if(configData['maxnum']){
            minNum = configData['minnum']<maxNum?configData['minnum']:0;
        }else{
            minNum = 0;
        }

        if(typeof data !="object")return getStr;
        for(var i = minNum;i<maxNum;i++){
            nData = data[i];
            getStr += tmp;
            getStr = getStr.replace(/(<\$num>)+/gi,i+1);
            for(var z in nData){
                getStr = getStr.replace(new RegExp('(\\$'+z+')+','gi'),nData[z]);
            }
        }
        return getStr;
    },
    /*获取url参数*/
    getReq:function(url){
        var theRequest = new Object()
            ,str = url.substr(1);
        strs = str.split("&");
        for(var i = 0; i < strs.length; i ++) {
            theRequest[strs[i].split("=")[0]]=strs[i].split("=")[1];
        }
        return theRequest;
    },
    /*url重置*/
    urlRewrite:function(urlStr){
        var _this = this
            ,mpsArr
            ,urlmps
            ,urlRes;
        urlStr = urlStr.replace(/(amp;)+/g,"");
        if(typeof _this['urlParam']['hmsr']=="undefined"||_this['urlParam']['hmsr']==""){//页面URL中不含有hmsr
            if(urlStr.indexOf("?")>-1){
                urlRes = urlStr.indexOf("mps=")<0?urlStr+"&mps=0.0.0.0.0":urlStr;
            }else{
                urlRes = urlStr+"?mps=0.0.0.0.0";
            }
        }else{
            if(/\?[\s\S]*?mps=/.test(urlStr)){
                urlmps = /(mps=(?:[^&|\b|\s])*)/.exec(urlStr)[1];
                mpsArr = (urlmps.replace("mps=","")).split(".");
                if(mpsArr[0]==0){
                    mpsArr[0] = _this['urlParam']['hmsr'];
                    urlRes = urlStr.replace(urlmps,"mps="+mpsArr.join('.'));
                }else{
                    urlRes = urlStr;
                }
            }else{
                if(urlStr.indexOf("?")>-1){
                    urlRes = urlStr+"&mps="+_this['urlParam']['hmsr']+".0.0.0.0";
                }else{
                    urlRes = urlStr+"?mps="+_this['urlParam']['hmsr']+".0.0.0.0";
                }
            }
        }

        return encodeURIComponent(urlRes);
    },
    /**
     * banner广告位数据解析
     */
    banner:function(){
        var bnnrData = this.config['banner']
            ,_this = this;
        for(var i =0;i<bnnrData.length;i++){
            bnnrData[i]["link"] = _this.urlRewrite(bnnrData[i]["link"]);
        };
        _this.id("banner").innerHTML = _this.jsTemplate({
            data:bnnrData,
            tpl:_Templates_["bnnr"]
        });
    },
    /**
     * 商品列表
     */
    productList:function(){
        var _this = this
            ,proData = _this.config['product']
            ,prodListData = []
            ,ndata
            ,moreLink
            ,listIndex = 0;
        /*数据整理*/
        for(var i =0;i<proData.length;i++){
            ndata = proData[i];
            moreLink = _this.urlRewrite(ndata['link']);
            ndata = ndata["list"];
            var z = Math.floor(ndata.length*Math.random());
            prodListData.push({title:ndata[z]['title'],price:ndata[z]['price'],src:ndata[z]['src'],href:_this.urlRewrite(ndata[z]['link']),more:moreLink,index:listIndex});
        }
        _this.id("listBox").innerHTML = _this.jsTemplate({
            data:prodListData,
            tpl:_Templates_["list"]
        });
    },
    loadFun:function(){
        var _this = this;
        _this.config = ylConfig;
        _this.urlParam = _this.getReq(window.location.search)||{};//获取当前页面URL参数
        _this.banner();
        _this.productList();
    }
};
function dataLoadFun(){
    if(typeof ylConfig !="undefined"){
        yinLiuFed.loadFun();
        return;
    }
    setTimeout(function(){
        dataLoadFun();
    },300);
};

dataLoadFun();
