fis.config.merge({
    modules : {
        parser : {less : ['less']}
    },

    roadmap : {
        ext: {
            less : 'css'
        },

        path : [

            //发布map.json -> map-common.json
            {
                reg : 'map.json',
                release : '/static/maps/map-huodong-xueleifeng.json'
            },

            //排除所有conf.js项目配置文件
            {
                reg:/\/conf-[^\.]*\.js/i,
                release:false
            },

            //排除public目录
            {
                reg:/\/public\//i,
                release:false
            },


            //只命中/static/html
            {
                reg:/\/static\/html\/(?!huodong\/)/i,
                release:false
            },
            {
                reg:/\/static\/html\/huodong\/(?!xueleifeng\/)/i,
                release:false
            },

            //只命中/static/css
            {
                reg:/\/static\/css\/(?!huodong\/)/i,
                release:false
            },
            {
                reg:/\/static\/css\/huodong\/(?!xueleifeng\/)/i,
                release:false
            },


            //只命中/static/scripts
            {
                reg:/\/static\/scripts\/(?!huodong\/)/i,
                release:false
            },
            {
                reg:/\/static\/scripts\/huodong\/(?!xueleifeng\/)/i,
                release:false
            },

            //只命中/static/images
            {
                reg:/\/static\/images\/(?!huodong\/)/i,
                release:false
            },
            {
                reg:/\/static\/images\/huodong\/(?!xueleifeng\/)/i,
                release:false
            },


            //将自动合并图片的文件release到images/相应目录下
            {
                reg:/\/static\/css\/huodong\/xueleifeng\/([^\/]+\.png)/i,
                release:'/static/images/huodong/xueleifeng/$1'
            }
        ]
    },
    settings : {},
    pack : {
        '/static/css/huodong/xueleifeng/xueleifeng.css' : [
            '/static/css/huodong/xueleifeng/ngDialog.css',
            '/static/css/huodong/xueleifeng/ngDialog-theme-default.css',
            '/static/css/huodong/xueleifeng/xueleifeng.less'
        ],
        '/static/scripts/huodong/xueleifeng/xueleifeng.js':[
            '/static/scripts/huodong/xueleifeng/app.js',
            '/static/scripts/huodong/xueleifeng/controllers.js',
            // '/static/scripts/huodong/xueleifeng/directives.js',
            '/static/scripts/huodong/xueleifeng/services.js'
            // '/static/scripts/huodong/xueleifeng/ngDialog.min.js'
        ]
    },
    deploy : {
        local : {
            to : '../b5m'
        },
        xushu:{
            to:'F:/xushu'
        },
		linlocal:{
			to:'../b5mtest'
		}

    }

});
