fis.config.merge({
    modules : {
        parser : {less : ['less']}
    },

    roadmap : {
        ext: {
            less : 'css'
        },

        path : [

            //发布map.json -> map-common.json
            {
                reg : 'map.json',
                release : '/static/maps/map-minipop.json'
            },

            //排除所有conf.js项目配置文件
            {
                reg:/\/conf-[^\.]*\.js/i,
                release:false
            },

            //排除public目录
            {
                reg:/\/public\//i,
                release:false
            },

            //只命中/static/html/minipop/
            {
                reg:/\/static\/html\/(?!minipop\/)/i,
                release:false
            },

            //只命中/static/css/minipop/
            {
                reg:/\/static\/css\/(?!minipop\/)/i,
                release:false
            },

            //只命中/static/scripts/minipop/
            {
                reg:/\/static\/scripts\/(?!minipop\/)/i,
                release:false
            },

            //只命中/static/images/minipop/
            {
                reg:/\/static\/images\/(?!minipop\/)/i,
                release:false
            },

            //将自动合并图片的文件release到images/相应目录下
            {
                reg:/\/static\/css\/minipop\/([^\/]+)\/([^\/]+\.png)/i,
                release:'/static/images/minipop/$1/$2'
            }
        ]
    },
    settings : {},
    pack : {
        '/static/css/minipop/20140227/minipop.css' : ['/static/css/minipop/20140227/*.less'],//合并所有的less到star.css
        '/static/css/minipop/20140320/minipop.css' : ['/static/css/minipop/20140320/*.less']//合并所有的less到star.css
    },
    deploy : {
        local : {
            to : '../b5m'
        },
		wuchen:{
		    to:'C:/xampp/htdocs/b5m-web-frontpage/b5m'
		}
    }

});