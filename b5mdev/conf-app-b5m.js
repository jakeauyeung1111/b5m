fis.config.merge({
    modules : {
        parser : {less : ['less']}
    },

    roadmap : {
        ext: {
            less : 'css'
        },

        path : [

            //发布map.json -> map-common.json
            {
                reg : 'map.json',
                release : '/static/maps/map-app-b5m.json'
            },

            //排除所有conf.js项目配置文件
            {
                reg:/\/conf-[^\.]*\.js/i,
                release:false
            },

            //排除public目录
            {
                reg:/\/public\//i,
                release:false
            },


            //只命中/static/html
            {
                reg:/\/static\/html\/(?!app\/)/i,
                release:false
            },

            {
                reg:/\/static\/html\/app\/(?!b5m\/)/i,
                release:false
            },

            //只命中/static/css
            {
                reg:/\/static\/css\/(?!app\/)/i,
                release:false
            },
            {
                reg:/\/static\/css\/app\/(?!b5m\/)/i,
                release:false
            },


            //只命中/static/scripts
            {
                reg:/\/static\/scripts\/(?!app\/)/i,
                release:false
            },
            {
                reg:/\/static\/scripts\/app\/(?!b5m\/)/i,
                release:false
            },

            //只命中/static/images
            {
                reg:/\/static\/images\/(?!app\/)/i,
                release:false
            },
            {
                reg:/\/static\/images\/app\/(?!b5m\/)/i,
                release:false
            },


            //将自动合并图片的文件release到images/相应目录下
            {
                reg:/\/static\/css\/app\/b5m\/([^\/]+\.png)/i,
                release:'/static/images/app/b5m/$1'
            }
        ]
    },
    settings : {},
    pack : {
        '/static/css/app/b5m/download.css' : [
            '/static/css/app/b5m/download.less'
        ],
        '/static/scripts/app/b5m/download.js':[
            '/static/scripts/app/b5m/download.js'
        ]
    },
    deploy : {
        local : {
            to : '../b5m'
        }
    }

});