fis.config.merge({
    modules : {
        parser : {less : ['less']}
    },

    roadmap : {
        ext: {
            less : 'css'
        },

        path : [

            //发布map.json -> map-common.json
            {
                reg : 'map.json',
                release : '/static/maps/map-huodong-songhuafei.json'
            },

            //排除所有conf.js项目配置文件
            {
                reg:/\/conf-[^\.]*\.js/i,
                release:false
            },

            //排除public目录
            {
                reg:/\/public\//i,
                release:false
            },


            //只命中/static/html
            {
                reg:/\/static\/html\/(?!huodong\/)/i,
                release:false
            },
            {
                reg:/\/static\/html\/huodong\/(?!songhuafei\/)/i,
                release:false
            },

            //只命中/static/css
            {
                reg:/\/static\/css\/(?!huodong\/)/i,
                release:false
            },
            {
                reg:/\/static\/css\/huodong\/(?!songhuafei\/)/i,
                release:false
            },


            //只命中/static/scripts
            {
                reg:/\/static\/scripts\/(?!huodong\/)/i,
                release:false
            },
            {
                reg:/\/static\/scripts\/huodong\/(?!songhuafei\/)/i,
                release:false
            },

            //只命中/static/images
            {
                reg:/\/static\/images\/(?!huodong\/)/i,
                release:false
            },
            {
                reg:/\/static\/images\/huodong\/(?!songhuafei\/)/i,
                release:false
            },


            //将自动合并图片的文件release到images/相应目录下
            {
                reg:/\/static\/css\/huodong\/songhuafei\/([^\/]+\.png)/i,
                release:'/static/images/huodong/songhuafei/$1'
            }
        ]
    },
    settings : {},
    pack : {
        '/static/css/huodong/songhuafei/shf.css' : [
            '/static/css/huodong/songhuafei/*.less'
        ],
        '/static/scripts/huodong/songhuafei/sfh.js':[
            '/static/scripts/huodong/songhuafei/app.js',
            '/static/scripts/huodong/songhuafei/directives.js',
            '/static/scripts/huodong/songhuafei/services.js',
            '/static/scripts/huodong/songhuafei/controllers.js',
            '/static/scripts/huodong/songhuafei/angular-timer.min.js'
        ]
    },
    deploy : {
        local : {
            to : '../b5m'
        },
		xushu:{
			to:'F:/xushu'
		}

    }

});
