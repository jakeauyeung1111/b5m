fis.config.merge({
    modules : {
        parser : {less : ['less']}
    },

    roadmap : {
        ext: {
            less : 'css'
        },

        path : [

            //发布map.json -> map-common.json
            {
                reg : 'map.json',
                release : '/static/maps/map-huodong-zuiqiangdanao.json'
            },

            //排除所有conf.js项目配置文件
            {
                reg:/\/conf-[^\.]*\.js/i,
                release:false
            },

            //排除public目录
            {
                reg:/\/public\//i,
                release:false
            },

            //只命中/static/html
            {
                reg:/\/static\/html\/(?!huodong\/)/i,
                release:false
            },
            {
                reg:/\/static\/html\/huodong\/(?!zuiqiangdanao\/)/i,
                release:false
            },

            //只命中/static/css
            {
                reg:/\/static\/css\/(?!huodong\/)/i,
                release:false
            },
            {
                reg:/\/static\/css\/huodong\/(?!zuiqiangdanao\/)/i,
                release:false
            },


            //只命中/static/scripts
            {
                reg:/\/static\/scripts\/(?!huodong\/)/i,
                release:false
            },
            {
                reg:/\/static\/scripts\/huodong\/(?!zuiqiangdanao\/)/i,
                release:false
            },

            //只命中/static/images
            {
                reg:/\/static\/images\/(?!huodong\/)/i,
                release:false
            },
            {
                reg:/\/static\/images\/huodong\/(?!zuiqiangdanao\/)/i,
                release:false
            },


            //将自动合并图片的文件release到images/相应目录下
            {
                reg:/\/static\/css\/huodong\/zuiqiangdanao\/([^\/]+\.png)/i,
                release:'/static/images/huodong/zuiqiangdanao/$1'
            }
        ]
    },
    settings : {},
    pack : {
        '/static/css/huodong/zuiqiangdanao/zqdn.css' : [
            '/static/css/huodong/zuiqiangdanao/*.less'
        ],
        '/static/scripts/huodong/zuiqiangdanao/zqdn.js':[
            '/static/scripts/huodong/zuiqiangdanao/app.js',
            '/static/scripts/huodong/zuiqiangdanao/services.js',
            '/static/scripts/huodong/zuiqiangdanao/controllers.js'
        ]
    },
    deploy : {
        local : {
            to : '../b5m'
        }
    }

});