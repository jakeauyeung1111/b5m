fis.config.merge({
    modules : {
        parser : {less : ['less']}
    },

    roadmap : {
        ext: {
            less : 'css'
        },

        path : [

            //发布map.json -> map-common.json
            {
                reg : 'map.json',
                release : '/static/maps/map-center.json'
            },

            //排除所有conf.js项目配置文件
            {
                reg:/\/conf-[^\.]*\.js/i,
                release:false
            },

            

            //只命中/static/html
            {
                reg:/\/static\/html\/(?!center\/)/i,
                release:false
            },

            //只命中/static/css
            {
                reg:/\/static\/css\/(?!center\/)/i,
                release:false
            },

            //只命中/static/scripts
            {
                reg:/\/static\/scripts\/(?!center\/)/i,
                release:false
            },

            //只命中/static/images
            {
                reg:/\/static\/images\/(?!center\/)/i,
                release:false
            },

            //只命中/static/public/sea-modules
            {
                reg:/\/static\/public\/(?!sea-modules\/)/i,
                release:false
            },


            //将自动合并图片的文件release到images/相应目录下
            {
                reg:/\/static\/css\/center\/([^\/]+\.png)/i,
                release:'/static/images/center/$1'
            }
        ]
    },
    settings : {},
    pack : {
        '/static/css/center/center.css' : [
            '/static/css/center/center.less'
        ],
        '/static/scripts/center/centerFed.js' : [
            '/static/scripts/center/centerFed.js'
        ]
    },
    deploy : {
        local : {
            to : '../b5m'
        }
    }

});