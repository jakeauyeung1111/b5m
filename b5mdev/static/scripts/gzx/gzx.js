var gzxFed = window.gzxFed || {};

gzxFed.indexFun = function() {
    this.indexlInit();
};
gzxFed.searchFun = function() {
    this.searchInit();
};
gzxFed.detailFun = function() {
    var _this = gzxFed;
    _this.datailInit();
};


gzxFed.aotuFill=function(){
    $('.J_autofill').autoFill('', 'b5mo');
};
/**
 * 首页调用
 */
gzxFed.indexlInit=function(){
    var _this = gzxFed;
    _this.aotuFill();

    /*人气商品 floor s*/
    _this.indexlInit.incTab('#rqsp-tit','#rqsp-cont-w','.reqi-items');
    /*人气商品 floor e*/
    /*高分商品 floor s*/
    _this.indexlInit.incTab('#gfsp-tit','#gfsp-goods-w','','.scores-goods');
    /*高分商品 floor e*/
    /*历史低价 floor s*/
    _this.indexlInit.incTab('#lsdj-tit','#lsdj-goods-w','','.scores-goods');
    /*历史低价 floor e*/
    /*热销精品 floor s*/
    _this.indexlInit.incTab('#rxjp-tit','#rxjp-goods-w','','.scores-goods');
    /*热销精品 floor e*/

    _this.indexlInit.preiceFun();
    _this.corclePic();

};
/**
 * 商品详情页调用
 */
gzxFed.datailInit=function(){
    var _this = gzxFed;
    _this.aotuFill();
    _this.datailInit.goodsPicSlide();
    _this.datailInit.allGoodstab();
    _this.corclePic();
    _this.datailInit.fixedTopLyaer();
    _this.datailInit.priceTrandFun('#priceTrendDiv');
    _this.datailInit.swfZhiShu('pointerswf');
    _this.datailInit.moreTypeFun();
    _this.datailInit.otherBand();
    _this.datailInit.otherType();
    _this.datailInit.showAllGoods();
    _this.datailInit.getComment(1);
    _this.datailInit.getGoodsFun();
    _this.datailInit.seldectMall();
};
/**
 * 搜索结果页调用
 */
gzxFed.searchInit=function(){
    var _this = gzxFed;
    _this.aotuFill();

    _this.fixDom('.side-l','nav-fix');
    _this.searchInit.showAllGoodsClass();
    _this.searchInit.showOtherProperty();
    _this.searchInit.priceSearchFun();
    _this.corclePic();
};
/*首页选项卡s*/
gzxFed.indexlInit.incTab=function(titId,contId,contItem,goodsItems){
    if(contItem){
        var $tabTits=$(contId).find(contItem).length,
            $titHtmls='';

        if($tabTits > 1){
            for(var i=0;i<$tabTits;i++){
                i==0?$titHtmls+='<a href="javascript:void(0);" class="cur"></a>': $titHtmls+='<a href="javascript:void(0);"></a>';
            }
            $(titId).html($titHtmls);
        }else{
            $(titId).hide();
        };
        $(titId).on('click','a',function(e){
            var idx=$(this).index();
            $(this).addClass('cur').siblings('a').removeClass('cur');
            $(contId).find(contItem).eq(idx).show().siblings(contItem).hide();
            e.preventDefault();
        });
    }else{
        var $goodsItem=$(contId).find(goodsItems),
            $liLen=$goodsItem.find('li').length,
            $liW=$goodsItem.find('li').width()+6,
            $ulItems=$(contId).find(goodsItems).children('ul'),
            $tabTits=$liLen/5,
            $titHtmls='';
        if($tabTits > 1){
            for(var i=0;i<$tabTits;i++){
                i==0?$titHtmls+='<a href="javascript:void(0);" class="cur"></a>': $titHtmls+='<a href="javascript:void(0);"></a>';
            }
            $(titId).html($titHtmls);
        }else{
            $(titId).hide();
        };
        $ulItems.width($liW*$liLen+'px');
        $(titId).on('click','a',function(e){
            var idx=$(this).index();
            $(this).addClass('cur').siblings('a').removeClass('cur');
            $ulItems.stop(true,false).animate({'left':-idx*980+'px'},500);
            e.preventDefault();
        });
    };
};
/*首页选项卡e*/
/*价格报告s*/
gzxFed.indexlInit.preiceFun=function(){
    $('#jgbg-cont-w').find('.jgbg-tiem dt').hover(function(){
        var $this = $(this).parent('.jgbg-tiem');

        // $this.siblings('.jgbg-tiem').find('.jgbg-pic').css('display','none');
        $this.siblings('.jgbg-tiem').stop().animate({'width':'157'},295)
        $this.stop().animate({'width':'662'},300);

    });
};
/*价格报告e*/
/**
 * 显示商品分类
 */
gzxFed.searchInit.showAllGoodsClass=function(){
    var $obj=$('#J_category_nav'),
        $itemLinks = $obj.find('a'),
        $hideDls=$obj.find('dl:hidden');
    $obj.find('dt').not('.no-bg').click(function(){
        var parDl=$(this).parent('dl');
        if(parDl.hasClass('cur')){
            parDl.removeClass('cur');
        }else{
            parDl.addClass('cur');
        };
    });
    //阻止冒泡
    $itemLinks.on('click',function(e){
        e.stopPropagation();
    });
    /**
     * 查看更多分类
     */
    $('.more-category').on('click',function(){
        if($(this).hasClass('more-category-open')){
            $(this).removeClass('more-category-open');
            $hideDls.hide();
        }else{
            $(this).addClass('more-category-open');
            $hideDls.show();
        };
    });

};
/**
 * 过滤选择属性
 */
gzxFed.searchInit.showOtherProperty=function(){
    var $hidePropers=$('#J_filter').find('.filter-item:hidden'),
        $filter=$('#J_filter'),
        $filItem=$filter.find('.filter-item'),
        $more_c = $filter.find('.filter-more');
    /**
     * 显示其他属性
     */
    if($filItem.length>7){
        $more_c.show();
    }else{
        $more_c.hide();
    };

    $('#J_more').on('click',function(e){
        e.stopPropagation();
        if($(this).hasClass('open')){
            $(this).removeClass('open');
            $hidePropers.show();
        }else{
            $(this).addClass('open');
            $hidePropers.hide();
        };
    });
    /**
     *显示更多
     */
    $filItem.each(function(i){
        var $this=$(this),
            aLinks=$this.find('.filter-lists a'),
            $moreChage=$this.find('.filter-act');
        /**
         * 显示展开
         */
        if(aLinks.length>7){
            $this.children('.show-more').show();
        };
        console.log(aLinks.length);
        /**
         * 展开/收起事件
         */
        $this.on('click','.show-more',function(e){
            if($this.hasClass('filter-open')){
                $this.removeClass('filter-open filter-multiply');
                $(this).removeClass('open').find('span').text('展开');
            }else{
                $this.addClass('filter-open');
                $(this).addClass('open').find('span').text('收起');
            };
            e.stopPropagation();
        });
        /**
         * 更多绑定事件
         */
        $moreChage.on('click','.btn-multiple',function(e){
            $this.hasClass('filter-open')?$this.addClass('filter-multiply'):$this.addClass('filter-open filter-multiply');
            e.stopPropagation();
        });
        /**
         * 取消绑定事件
         */
        $moreChage.on('click','.btn-cancle',function(e){
            $filItem.find('.show-more').trigger('click');
            e.preventDefault();
        });
        /**
         * 属性绑定事件
         */
        aLinks.click(function(e){
            $(this).hasClass('cur')?$(this).removeClass('cur'):$(this).addClass('cur');
            //多选情况下，阻止a的默认事件
            if($(this).parents('.filter-item').hasClass('filter-multiply')){
                e.preventDefault();
            }
        });
        //确定多选事件
        $this.on('click','.btn-sure',function(e){
            var $this = $(this),
                $parent = $this.parents('.filter-item'),
                $filter = $parent.find('.filter-lists').find('.cur'),
                len = $filter.length,
                title = $parent.find('dt').attr('title'),
                data = '',
                url = location.pathname;

            $filter.each(function(index, el) {
                data += title + ',' + $(el).text() + ';';
            });

            //提交数据
            location.href= url.replace(/(__[^_]*)(_)/,'$1'+ data + '_' );
            $parent.hide();
            e.preventDefault();

        });
    });

};
/**
 * 自定义价格搜索按钮
 */
gzxFed.searchInit.priceSearchFun=function(){
    $('.price-input').on('click','input.btn',function(){
        var data = '',
            url = location.pathname,
            $inputValue = $(this).siblings('.txt'),
            v1 = parseInt($inputValue.eq(0).val()),
            v2 = parseInt($inputValue.eq(1).val()),
            re = /^[0-9]*$/;
        if( !v1 || !v2){
            alert('不能为空');
        }else{
            if(!re.test(v1) || !re.test(v2)){
                alert('您输入的格式不正确！请输入数字。');
            }else{
                if( v1 > v2 ){
                    alert('请从小到大！')
                }else if( v1 == v2 ){
                    alert('请输入不同的数字');
                }else{
                    data += $inputValue.eq(0).val() + '-' +$inputValue.eq(1).val();
                    location.href= url.replace(/(__[^_]*__)[^_]*_/,'$1'+ data + '_' );
                }
            };
        }
    });
};

gzxFed.pageId = function(){
    var nId = window.location.pathname.substring(8).replace('.html','');

    return nId;
};

gzxFed.datailInit.getGoodsFun=function(){
   var $parentItems = $('.shop-seq-item'),
       _this = gzxFed,
       nUlr=_this.pageId(),
       $cont=$('.tab-shop-cont'),
       $tTextStr = $('.mall-compare span').eq(1).text(),
       $tText = $tTextStr.replace(/(^\s+)|(\s+$)/g,"");;
    $parentItems.find('li').click(function(){
        var $this = $(this),
            index=$(this).parent('li').index(),
            nId=$(this).attr('data'),
            $zpCheck = $('.mall-compare').find('i[name="zhengpin"]').hasClass('checkbox-checked'),
            $tmCheck = $('.mall-compare').find('i[name="tianmao"]').hasClass('checkbox-checked');

        if($zpCheck){
            _this.datailInit.getGoodsSort(index,nId,nUlr,'',true);
        }else if($tmCheck){
            _this.datailInit.getGoodsSort(index,nId,nUlr,$tText);
        }else if($zpCheck && $tmCheck){
            _this.datailInit.getGoodsSort(index,nId,nUlr,$tText,true);
        }else{
            _this.datailInit.getGoodsSort(index,nId,nUlr);
        };
        $this.hasClass('cur')?$this.siblings('li').removeClass('cur'):$this.addClass('cur').siblings('li').removeClass('cur');
    });
};
/**
 * 商城选择
 */
gzxFed.datailInit.seldectMall=function(){
    var _this=gzxFed,
        $this=$('.mall-compare').find('span'),
        nUlr=window.location.pathname.substring(8).replace('.html','');

    $('.mall-compare').find('span').eq(0).click(function(){
        var index=$('.shop-seq-item').find('.cur').index(),
            nId=$('.shop-seq-item').find('.cur').attr('data'),
            _thisChild = $(this).children('i'),
            $tTextStr = $('.mall-compare span').eq(1).text(),
            $tText = $tTextStr.replace(/(^\s+)|(\s+$)/g,"");

        if(_thisChild.hasClass('checkbox-checked')){
            $(this).children('i').removeClass('checkbox-checked');
            if($this.eq(1).children('i').hasClass('checkbox-checked')){
                _this.datailInit.getGoodsSort(index,nId,nUlr,$tText);
            }else{
                _this.datailInit.getGoodsSort(index,nId,nUlr);
            };

        }else{
            _thisChild.addClass('checkbox-checked');
            if($this.eq(1).children('i').hasClass('checkbox-checked')){
                _this.datailInit.getGoodsSort(index,nId,nUlr,$tText,true);
            }else{
                _this.datailInit.getGoodsSort(index,nId,nUlr,'',true);
            };

        };
    });

    $('.mall-compare').find('span').eq(1).click(function(){

        var index=$('.shop-seq-item').find('.cur').index(),
            nId=$('.shop-seq-item').find('.cur').children('a').attr('data'),
            _thisChild = $(this).children('i'),
            $tTextStr = $('.mall-compare span').eq(1).text(),
             $tText = $tTextStr.replace(/(^\s+)|(\s+$)/g,"");

        if(_thisChild.hasClass('checkbox-checked')){
            $(this).children('i').removeClass('checkbox-checked');
            if($this.eq(0).children('i').hasClass('checkbox-checked')){
                _this.datailInit.getGoodsSort(index,nId,nUlr,'',true);
            }else{
                _this.datailInit.getGoodsSort(index,nId,nUlr);
            };
        }else{
            _thisChild.addClass('checkbox-checked');
            if($this.eq(0).children('i').hasClass('checkbox-checked')){
                _this.datailInit.getGoodsSort(index,nId,nUlr,$tText,true);
            }else{
                _this.datailInit.getGoodsSort(index,nId,nUlr,$tText);
            };

        };
    });
};
/**
 * 获取商品排序
 * @param curIndex点击当前的按钮索引值
 * @param nId当前按钮id值
 * @param nUlr浏览器地址
 */
gzxFed.datailInit.getGoodsSort=function(curIndex,nId,nUlr,mallStr,isGen){
    var _this=gzxFed;
    if(!arguments[3]) mallStr = null;
    if(!arguments[4]) isGen = false;
    $.ajax({
        url:'http://gzx.b5m.com/tabdata.html?t=' + Math.random(10000, 9999),
        type:"GET",
        async:true,
        data:{order:nId,o_id:nUlr,order_type:'ASC',source:mallStr,isgenuine:isGen},
        dataType:"jsonp",
        jsonp: 'jsonpCallback',
        beforeSend:function(){
            $('.ajax_loading').show();
            $('.tab-shop-cont').eq(0).html('');
        },
        success:function(json){
            if(json.code == 100){
                $('.ajax_loading').hide();
                _this.datailInit.goodsSortIndex(curIndex,json);
            }else if(json.code == 101){
                $('.ajax_loading').hide();
                $('.tab-shop-cont').eq(0).html('<p style="padding: 30px 0; text-align: center; font-size: 16px;">暂无数据！</p>');
            };

        }
    });
};
/**
 * 所有商品排序列表方法
 * @param 数据
 */
gzxFed.datailInit.goodsSortIndex=function(curIndex,json){
    var allHtmls='',
        _this=gzxFed,
        datas=json.data;

    for(var i in datas){
        var allHtml='<dl class="tab-shop-items">',
            len = datas[i].length,
            num = len - 1;
        allHtml +=_this.datailInit.goodsSortInow(datas[i],i,curIndex);
        if(num > 0){
            allHtml +='<dd class="icon all-shop"><a href="javascript:void(0);" class="icon add">展开其余' + num + '件商品</a></dd>';
        };
        allHtml +='</dl>';
        allHtmls +=allHtml;
    }


    $('.tab-shop-cont').eq(0).html(allHtmls);
    _this.corclePic();

};
/**
 * 服务保障
 * @param data数据
 */
gzxFed.datailInit.fuwu=function(data){
    var fData=data,
        fHtml='',
        fDataArr=[];

    for( var i in fData){
        if(i == 'refunding'){
            fData[i] += '天退货';
        }else if(i == 'warranty'){
            fData[i] +='天保修';
        };
        fDataArr.push(fData[i]);
    };
    for(var i = 0; i<fDataArr.length; i++){
        if(i<3){
            fHtml +=fDataArr[i]+'<br>';
        };
    }
    return fHtml;

};
/**
 * 趋势图
 * @param 趋势数据
 * @returns 趋势的class名称
 */
gzxFed.datailInit.trend=function(data){
    var className = '';
    if(data < 0){
        className = 'icon p-down';
    }else if(data > 0){
        className = 'icon p-up';
    }else{
        className = 'icon p-flat';
    };
    return className;
};
/**
 * 获取某个商品排序列表
 * @param 数据
 */
gzxFed.datailInit.goodsSortInow=function(data,index,curIndex){

    var _this=gzxFed
        ,nDatas=data
        ,lens=nDatas.length
        ,allHtml='';

    for(var i = 0; i<lens; i++){
        /**
         * 当只有一条数据时
         */
        if(i == 0){


            allHtml += '<dt class="shop-first"><table class="shop-tiem"><tr><td width="415"><a href="' + nDatas[i]['url'] + '">' + nDatas[i]['title'] + '</a> </td>';
            allHtml += '<td width="130"><a href=""><img src="http://staticcdn.b5m.com/images/jiagebaogao/v4/' + index + '_logo.png"></a></td><td width="95"><div class="data-numb circle" data-info=\'{"num":"' + nDatas[i]['composite_index'] +'"}\'></div></td>';
            allHtml += '<td width="72"><p class="price"><b>￥</b>' + nDatas[i]['cur_price'] + '</p></td><td width="74" class="zs"><span class="' + _this.datailInit.trend(nDatas[i]['delta_price']) + '"></span></td><td width="122"><p>' + _this.datailInit.fuwu(nDatas[i]['s_guarantee']) + '</p></td>';
            allHtml += '<td width="72"><a href="" class="jy-c1">建议购买</a></td></tr></table></dt>';
        }

        allHtml += '<dd class="shop-list"><table class="shop-tiem">';

        if(lens > 0){
            if(i > 0){
                allHtml += '<tr><td width="130"></td><td width="415"><a href="' + nDatas[i]['url'] + '">' + nDatas[i]['title'] + '</a> </td>'
                allHtml += '<td width="95"><div  class="data-numb circle" data-info=\'{"num":"' + nDatas[i]['composite_index'] +'"}\'></div></td><td width="72"><p class="price"><b>￥</b>' + nDatas[i]['cur_price'] + '</p></td><td width="74" class="zs"><span class="' + _this.datailInit.trend(nDatas[i]['delta_price']) + '"></span></td>'
                allHtml += '<td width="122"><p>' + _this.datailInit.fuwu(nDatas[i]['s_guarantee']) + '</p></td><td width="72"><a href="" class="jy-c1">建议购买</a></td></tr>'
            }
        }


        allHtml += '</table></dd>';
    }

    return allHtml;
};

/**
 * 价格趋势
 */
gzxFed.datailInit.priceTrandFun=function(id){
   //var dataNew = [{y:1553.02,x:1388553956000},{y:1553.02,x:1388640356000},{y:1553.02,x:1388726756000},{y:1553.02,x:1388813156000},{y:1553.02,x:1388899556000},{y:1553.02,x:1388985956000},{y:1553.02,x:1389072356000},{y:1553.02,x:1389158756000},{y:1553.02,x:1389245156000},{y:1553.02,x:1389331556000},{y:1553.02,x:1389417956000},{y:1553.02,x:1389504356000},{y:1553.02,x:1389590756000},{y:1553.02,x:1389677156000},{y:1553.02,x:1389763556000},{y:1553.02,x:1389849956000},{y:1553.02,x:1389936356000},{y:1553.02,x:1390022756000},{y:1553.02,x:1390109156000},{y:1553.02,x:1390195556000},{y:1553.02,x:1390281956000},{y:1553.02,x:1390368356000},{y:1553.02,x:1390454756000},{y:1553.02,x:1390522297000},{y:1553.02,x:1390627556000},{y:1553.02,x:1390713956000},{y:1553.02,x:1390800356000},{y:1553.02,x:1390886756000},{y:1553.02,x:1390973156000},{y:1553.02,x:1391059556000},{y:1553.02,x:1391145956000},{y:1553.02,x:1391232356000},{y:1553.02,x:1391318756000},{y:1999,x:1391411157000},{y:1999,x:1391491556000},{y:1999,x:1391577956000},{y:1999,x:1391664356000},{y:1999,x:1391750756000},{y:1999,x:1391837156000},{y:1999,x:1391923556000},{y:1999,x:1392009956000},{y:1999,x:1392096356000},{y:1999,x:1392182756000},{y:1999,x:1392269156000},{y:1999,x:1392355556000},{y:1999,x:1392441956000},{y:1999,x:1392528356000},{y:1553.02,x:1392594626000},{y:1553.02,x:1392701156000},{y:1553.02,x:1392787556000},{y:1553.02,x:1392873956000},{y:1553.02,x:1392960356000},{y:1553.02,x:1393046756000},{y:1553.02,x:1393133156000},{y:1553.02,x:1393219556000},{y:1497.05,x:1393273593000},{y:1497.05,x:1393392356000},{y:1497.05,x:1393478756000},{y:1497.05,x:1393565156000},{y:1449.08,x:1393618444000},{y:1449.08,x:1393737956000},{y:1449.08,x:1393824356000},{y:1449.08,x:1393910756000},{y:1449.08,x:1393997156000},{y:1449.08,x:1394083556000},{y:1449.08,x:1394169956000},{y:1449.08,x:1394256356000},{y:1479.06,x:1394313298000},{y:1479.06,x:1394429156000},{y:1479.06,x:1394515556000},{y:1479.06,x:1394601956000},{y:1479.06,x:1394688356000},{y:1479.06,x:1394774756000},{y:1479.06,x:1394861156000},{y:1479.06,x:1394947556000},{y:1479.06,x:1395033956000},{y:1999,x:1395140973000},{y:1999,x:1395206756000},{y:1999,x:1395293156000},{y:1999,x:1395379556000},{y:1999,x:1395465956000},{y:1999,x:1395552356000},{y:1999,x:1395638756000},{y:1419.09,x:1395702173000},{y:1419.09,x:1395811556000},{y:1399.1,x:1395889845000},{y:1399.1,x:1395984356000},{y:1399.1,x:1396070756000},{y:1399.1,x:1396157156000},{y:1399.1,x:1396243556000},{y:1399.1,x:1396329956000}];
    //data = data.replace('"', '');
    var $this = $(id),
        dataNew = eval($this.attr('data'));
        datas = [];
    console.log(dataNew);

    $this.highcharts({
        title: {
            text: '',
            x: -20 //center
        },
        colors : [ '#ea7f02', "#ea7f02", "#64AD10", "#0C7F1A", "#1C98C5", "#4F90B5", "#EECFA1", "#EE82EE", "#DB7093", "#BFEFFF" ],
        credits:{
            text:''
        },
        xAxis: {
            type : "datetime",
            dateTimeLabelFormats : {
                year: '%Y',
                day: '%e - %b',
                week : "%m - %d",
                month : "%m - %d"
            }
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        tooltip: {
            valuePrefix: '￥',
            xDateFormat : "%Y-%m-%d",
            backgroundColor : "#ffffff",
            borderWidth : 1,
            borderColor : "#c0e8df",
            crosshairs : [ {
                color : "#0fb996",
                width : 1
            }],
            style : {
                color : '#999',
                fontWeight:'normal',
                fontSize : '12px',
                padding : '8px'
            }
        },
        legend: {
            enabled:false
        },
        series: [{
            name: '价格',
            data:dataNew
        }]
    });
};
/**
 * 获取评论
 */
gzxFed.datailInit.getComment=function(page){
    var _this = gzxFed,
        _id ='a9525611cf3f6a4536943c29bd29fdf6',//_this.pageId()
        _Arg = $.extend({},{o_id:_id,ajax_page:page,limit:5});
    $.ajax({
        url:'http://gzx.b5m.com/comment_request.html?t=' + Math.random(10000, 9999),
        type:"GET",
        async:true,
        data:_Arg,
        dataType:"jsonp",
        jsonp: 'jsonpCallback',
        success:function(data){
            var $this = $('.shop-comments');

            if(data['data']['gzx_comment_data']){
                $this.siblings('.page').html('').append(data['data']['page']).find("div>a").attr("href","javascript:void(0);").addClass("pageAbt");
                $this.find('ul').html('').append(data['data']['gzx_comment_data']);
            };
            $(".page").off().on({
                click:function(){
                    _this.datailInit.getComment($(this).attr('page'));
                    return false;
                }
            },".pageAbt");
            $(".page").off().on({
                click:function(){
                    _this.datailInit.getComment($(this).siblings('input').val()||1);
                    return false;
                }
            },".go_page");
            $('.page-input').find('.page_num').keydown(function(event) {
                if (event.which == 13) {
                    event.preventDefault();
                    _this.datailInit.getComment($(this).val());
                }
            });
        }
    });

};
/**
 * 详情页商品图片切换
 */
gzxFed.datailInit.goodsPicSlide=function(){
    var iNow=0;
    $this=$(".pic-slide"),
        liW=$this.find(".pic-list li").width(),
        allNumb=$this.find(".pic-list li").length;
    $this.on("click",".r-btn",function(){
        if(iNow<allNumb-1){
            iNow++;
        }else{
            iNow=0;
        };
        picSlide();
    });
    $this.on("click",".l-btn",function(){
        if(iNow>0){
            iNow--;
        }else{
            iNow=allNumb-1;
        };
        picSlide();
    });
    function add0(numb){
        if(numb<10){
            numb="0"+numb;
        };
        return numb;
    };
    function picSlide(){
        var numNow=iNow+1,
            sLi=$this.find(".pic-list li");
        $('.pic-list').width(liW*allNumb);
        $this.find(".iNow").text(add0(numNow));
        $this.find(".allNumb").text(add0(allNumb));
        $('.pic-list').stop(true,false).animate({'left':-iNow*liW+'px'},400)
    };
    picSlide();
};
/** * 更多属性 */
gzxFed.datailInit.moreTypeFun=function(){
    var $textCent = $('.text-items');

    $textCent.on('click','.more-btn',function(){
       if($textCent.hasClass('text-items-more')){
           $textCent.removeClass('text-items-more');
       }else{
           $textCent.addClass('text-items-more');
       }
    });
};
/** * 其他属性 */
gzxFed.datailInit.otherType=function(){
    var $btn =$('.tab-box').find('.all-xq');

    $btn.click(function(){
        $this = $(this);
        $this.hide().siblings('.details-parameters').find('tr:hidden').show();
    });
};
/**
 * 展示其余商品
 */
gzxFed.datailInit.showAllGoods=function(){
    var _this=this;
    $('.tab-shop-cont').on('click','.all-shop a',function(){
        var goodslist=$(this).parent('.all-shop').siblings('.shop-list'),
            $text =$(this).text();
        if($(this).attr('class') == 'icon add'){
            var $text1 = $text.replace('展开','收起');
            $(this).attr('class','icon remove');
            $(this).text($text1);
            //goodslist.slideDown(200);
            goodslist.css('height','auto');
        }else{
            var $text1 = $text.replace('收起','展开');
            $(this).attr('class','icon add');
            //goodslist.slideUp(200);
            goodslist.css('height','0');
            $(this).text($text1);
        };
    });
};
/** * 其他商家 */
gzxFed.datailInit.otherBand=function(){
    var $this = $('.other-sh'),
        ulItems = $this.find('.other-items').children('ul'),
        liLen = ulItems.length,
        $btnDom = $this.find('.other-btn');

    if(liLen > 4){
        $btnDom.on('click','.right-btn',function(){
            ulItems.animate({'left':"190px"},200,function(){
                ulItems.find('li:last').prependTo(ulItems);
                ulItems.css('left','0');
            });
        });
        $btnDom.on('click','.left-btn',function(){
            ulItems.animate({'left':"-190px"},200,function(){
                ulItems.find('li:first').appendTo(ulItems);
                ulItems.css('left','0');
            });
        });
    }else{
        $btnDom.find('a').css('cursor','default');
    }
};
/**
 * 详情页指数
 */
gzxFed.datailInit.swfZhiShu=function(){
    var $this = $('#pointerswf'),
        swfValue= $this.attr('data'),
        datesStr =$this.attr('data-info'),
        datesJson = $.parseJSON(datesStr),
        dates =[datesJson['price_index'],datesJson['hot_index'],datesJson['sales_amount'],datesJson['service_index']];
    var swfdata = {
        "value":swfValue,
        "pageindex":true,
        "info":dates
    };
    swfobject.embedSWF("http://staticcdn.b5m.com/static/opt/swf/zhishu/zhishuv2.swf","pointerswf", "650", "225", "11.0.0", "expressInstall.swf",{'datas':encodeURIComponent(JSON.stringify(swfdata))},{'wmode':'transparent'});
};
/**
 * 详情页全部商品/详情参数/评论/价格趋势tab切换
 */
gzxFed.datailInit.allGoodstab=function(){
    $('#detail-tabs').find('li').click(function(){
        var _this = $(this),
            $idx = $(this).index(),
            $cont = _this.parents('.tab-hd').siblings('.tab-bd').find('.tab-box');

        _this.addClass('cur').siblings('li').removeClass('cur');
        $cont.eq($idx).show().siblings('.tab-box').hide();
    });
};
/**
 * 顶部固定浮动
 */
gzxFed.datailInit.fixedTopLyaer=function(){
    var domTop = $('.other-sh').offset().top,
        $fixedDom = $('.topfix-layer');

    $(window).scroll(function(){
        var scrTop = $(window).scrollTop();

        if(scrTop > domTop){
            $fixedDom.show();
        }else{
            $fixedDom.hide();
        }
    });
}
/**
 * 固定位置
 * @param obj 绑定元素Id
 * @param className  增加的class名字
 * @returns {boolean}
 */
gzxFed.fixDom=function(obj,className){
    var dom = $(obj),
        h = $(obj).offset().top,
        isIE6 = window.XMLHttpRequest ? false : true;
    if (!dom.length) return false;
    if (!isIE6) {
        $(window).on('scroll', function() {

            if ($(window).scrollTop() >= h) {
                dom.addClass(className);
            } else {
                dom.removeClass(className);
            }
        });
    }
};
/**
 * 指数图片数字
 */
gzxFed.corclePic=function(){
    var $circleId = $('.circle'),
        arrPo =[];

    for(var i = 0; i<9; i++){
        for(var j = 0; j< 12 ; j++){
            var arrSo = [j*60,i*60];
            arrPo.push(arrSo);
        };
    };

    $circleId.each(function(i,n){
        var jsonStr = $(this).attr('data-info'),
            numb = $.parseJSON(jsonStr)['num'];

        $(this).css('background-position',-arrPo[numb][0]+'px ' + -arrPo[numb][1]+'px ');
    });
};

/**
 * 热销商品
 */
gzxFed.hotGoodsFun=function(){
    var _this = this;
    _this.hotGoodsInit();
};
gzxFed.hotGoodsInit=function(){
    var _this = this;
    _this.aotuFill();
    _this.corclePic();
    _this.hotGoodsInit.menuSlide();
};

gzxFed.hotGoodsInit.menuSlide=function(){
    var $menu = $("#G_menu");

    $menu.on('mouseover',"ul li",function(){
        $(this).addClass('open');
    });
    $menu.on('mouseout',"ul li",function(){
        $(this).removeClass('open');
    });

    $('.crumb').on('mouseover',".cr-zs",function(){
        $(this).addClass('open');
    });
    $('.crumb').on('mouseout',".cr-zs",function(){
        $(this).removeClass('open');
    });

};
gzxFed.pageValNum=function(){
    var _pageCent = $('.page');
    if(_pageCent){
        _pageCent.find('.page-input input').on('keyup',function(e){
            $(this).val($(this).val().replace(/\D/g,""));
        });
    }
};

$(function(){
    gzxFed.pageValNum();
});