(function($){
    var $opacity = $('.opacity'),
        $dowload_elastic_laye = $('.dowload_elastic_laye'),
        _w = $dowload_elastic_laye.outerWidth(),
        _h = $dowload_elastic_laye.outerHeight(),
        win_w = $(window).width(),
        win_h = $(window).height();
    $('.int_for_android').mouseenter(function(){
        $(this).addClass('int_for_android_hover');
        $('.int_for_hover').show();
    }).mouseleave(function(){
            $(this).removeClass('int_for_android_hover');
            $('.int_for_hover').hide();
        });
    $('.for_hover_top, .int_for_iphone').click(function(e){
        e.preventDefault();
        var $this = $(this),
            _index = $this.index(),
            _st = $(document).scrollTop(),
            _sl = $(document).scrollLeft()
        $opacity.show().css({
            width: $(document).width(),
            height: $(document).height()
        });
        $dowload_elastic_laye.eq(_index).show().css({
            left: (win_w - _w) / 2 + _sl + 'px',
            top: (win_h - _h) / 2 + _st + 'px'
        });
        var $del = $dowload_elastic_laye.eq(_index).find('.elastic_laye_dele');
        $del.click(function(e){
            e.preventDefault();
            $(this).parent().hide();
            $opacity.hide();
        });
    });
})(jQuery);