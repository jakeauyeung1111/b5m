(function($){
    $('.bottom_andr').mouseenter(function(){
        $(this).addClass('bottom_andr_hover');
        $('.mk_he_lhover').show();
    }).mouseleave(function(){
            $(this).removeClass('bottom_andr_hover');
            $('.mk_he_lhover').hide();
        });
    $(window).scroll(function(){
        var t = document.documentElement.scrollTop || document.body.scrollTop;//滚动到上面看不到的高度
        var circle = $('.content_fixd').find('a');

        if( t>300 && t<2103){
            $(".content_fixd").fadeIn();
        }else{
            $(".content_fixd").fadeOut();
        }

        if(t>300 && t<600) {
            circle.eq(0).addClass('on').siblings().removeClass('on');
        }

        if(t>600 && t<1400) {
            circle.eq(1).addClass('on').siblings().removeClass('on');
        }

        if(t>1400 && t<1650) {
            circle.eq(2).addClass('on').siblings().removeClass('on');
        }
        if(t>1650 && t<1800) {
            circle.eq(3).addClass('on').siblings().removeClass('on');
        }
    });
    //鼠标点击随便哪个按键停止正在执行的函数.
    $("body").mousedown(function(){
        $('body,html').stop();
    });




    var scrollFunc = function (e) {
        var direct = 0;
        e = e || window.event;
        if (e.wheelDelta) {  //判断浏览器IE，谷歌滑轮事件
            if (e.wheelDelta < 0) { //当滑轮向上滚动时
                $('body,html').stop();
            }
            if (e.wheelDelta > 0) { //当滑轮向下滚动时
                $('body,html').stop();
            }
        } else if (e.detail) {  //Firefox滑轮事件
            if (e.detail> 0) { //当滑轮向上滚动时
                $('body,html').stop();
            }
            if (e.detail< 0) { //当滑轮向下滚动时
                $('body,html').stop();
            }
        }
    }
    //给页面绑定滑轮滚动事件
    if (document.addEventListener) {
        document.addEventListener('DOMMouseScroll', scrollFunc, false);
    }
    //滚动滑轮触发scrollFunc方法
    window.onmousewheel = document.onmousewheel = scrollFunc;
})(jQuery)