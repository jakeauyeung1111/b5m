(function($){

    /*获奖用户列表滚动*/
    var userScrollPanel =  $('.user-scroll-box'),
        userScrollBox = userScrollPanel.find('.box'),
        pos = 0,
        action = true;

    userScrollBox.find('li').clone().appendTo(userScrollBox.find('ul'));


    userScrollPanel.on({
        mouseenter:function() {
            action = false;
        },
        mouseleave:function() {
            action = true;
        }
    });
    /*获奖用户列表*/
    function getUserMsn(){
        $.ajax({
            url: 'http://haiwai.b5m.com/event/luckdraw?op=winuserlist',
            type: 'post',
            dataType:"jsonp",
            jsonp: 'jsonpCallback',
            success:function(data){
                if(data['code'] == 200){
                    getUserList(data['data']);
                    var liHeight = userScrollBox.find('li').height()+14,
                        liLength = userScrollBox.find('li').length;

                    var interval  = setInterval(function() {
                        if(!action) return false;
                        if(pos == -liHeight*liLength) {
                            pos = 0;
                        }
                        pos--;
                        userScrollBox.css('top',pos);
                    },50);
                };
            }
        });
    };
    function getUserList(data){
        var Datas = data,
            htmls ='';
            for(var i = 0; i<Datas.length; i++){
                htmls +='<li><div class="img-con"><img src="'+Datas[i]["avatar"]+'" alt=""></div><div class="rt"><div class="tp">'+Datas[i]["uname"]+'</div><div class="bt">抽中<span class="orange">'+Datas[i]["txt"]+'</span></div></div></li>'
            }
        userScrollBox.find('ul').html(htmls);
    };
    getUserMsn();
    /*投票列表*/

    $viteCont = $('.star-cont');

    function getViteMsn(){
        $.ajax({
            url: 'http://haiwai.b5m.com/event/luckdraw?op=starlist',
            type: 'post',
            dataType:"jsonp",
            jsonp: 'jsonpCallback',
            success:function(data){
                if(data['code'] == 200){
                    getViteList(data['data']);
                };
            }
        });
    };
    function getViteList(data){
        var Datas = data,
            htmls ='',
            $viteUl = $viteCont.find('.items');
        for(var i = 0; i<Datas.length; i++){
            htmls +='<li><img src="'+ Datas[i]["image"] +'" /><p class="sele"><em class="numb">'+ Datas[i]["precent"] +'%</em><span class="vato"><b style="width: '+ Datas[i]["precent"] +'%;"></b></span><input type="radio" name="a" userId ="'+ Datas[i]["id"] +'" /><span class="name">'+ Datas[i]["uname"] +'</span></p></li>'
        }
        $viteUl.html(htmls);
    };
    $viteCont.find('.sub-btn').on('click','a',function(){
       var userId = $viteCont.find('input[name="a"]:checked').attr('userId');
        if(userId){
            $.ajax({
                url: 'http://haiwai.b5m.com/event/luckdraw?op=votestar&id='+userId,
                type: 'get',
                dataType:"jsonp",
                jsonp: 'jsonpCallback',
                success:function(data){
                    alert(data['message']);
                }
            });
        }else{
            alert('请选择你心里最具魅力的韩星！');
        };
    });
    getViteMsn();





})(jQuery);