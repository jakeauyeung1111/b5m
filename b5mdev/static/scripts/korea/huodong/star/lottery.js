(function($){
	/**
	 * 抽奖入口
	 */
	var flag = true;
	var lotteryFunc = function(){
		//取掉点击旋转事件
		// $('#lotteryBtn').off('click',lotteryFunc);
		if(!flag) return false;
		flag = false;

		/*var data = [1,2,3,4,5,6,7,8],
			data = data[Math.floor(Math.random()*data.length)],
			data = 7,
			txt = '100帮豆',
			type = 'dou';*/

        /*var type = 'dou';*/

		$.ajax({
			url: 'http://haiwai.b5m.com/event/luckdraw?op=join',
			type: 'POST',
            dataType:"jsonp",
            jsonp: 'jsonpCallback',
            success:function(data){
                /**
                 * data:奖品编号
                 * txt:为奖品名称
                 * type:中奖种类（phone为手机，qq为QQ币，dou为帮豆）
                 */
                if(data['code'] == 300){
                    noChance(1);
                }else if(data['code'] == 314){
                    noChance(2);
                }else if(data['code'] == 316){
                    noChance(3);
                }else if(data['code'] == 301){
                    noChance(4);
                }else if(data['code'] == 302){
                    noChance(5);
                }else if(data['code'] == 303){
                    noChance(6);
                }else if(data['code'] == 304){
                    noChance(7);
                }else if(data['code'] == 305){
                    noChance(8);
                }else if(data['code'] == 306){
                    noChance(9);
                }else if(data['code'] == 317){
                    noChance(10);
                }else if(data['code'] == 200){
                    switch(data['data']['type']){
                        case 1:
                            rotateFunc(1,data['data']['txt'],data['type']);
                            break;
                        case 2:
                            rotateFunc(2,data['data']['txt'],data['type']);
                            break;
                        case 3:
                            rotateFunc(3,data['data']['txt'],data['type']);
                            break;
                        case 4:
                            rotateFunc(4,data['data']['txt'],data['type']);
                            break;
                        case 5:
                            rotateFunc(5,data['data']['txt'],data['type']);
                            break;
                        case 6:
                            rotateFunc(6,data['data']['txt'],data['type']);
                            break;
                        case 7:
                            rotateFunc(7,data['data']['txt'],data['type']);
                            break;
                        case 8:
                            rotateFunc(8,data['data']['txt'],data['type']);
                            break;
                    }
                }

            }
		})

	}
	/**
	 * awards:奖项编号
	 */
	var rotateFunc = function(awards,txt,type){
		var angle = (awards-1)*45 + 22;
		$('#lotteryBtn').addClass('active');
		$('#lotteryBtn').stopRotate();
		//转
		$("#lotteryBtn").rotate({
			angle:angle,
			duration: 5000, 
			animateTo: angle+1440, //angle是图片上各奖项对应的角度，1440是我要让指针旋转4圈。所以最后的结束的角度就是这样子^^
			callback:function(){
				// 获得奖品显示
				lotteryTips(awards,txt,type);
				flag = true;
				// $('#lotteryBtn').on('click',lotteryFunc);
			}
		});
	};
	/**
	 * 绑定旋转事件
	 */
	$("#lotteryBtn").rotate({ 
	   bind: 
		{ 
			click: lotteryFunc
		} 
	});

	/**
	 * 插入弹出框
	 */
	var Popup = {
		$popup:null,
		create:function(){
			$popup = $('<div class="popup" id="J_popup"><div class="popup-content"></div><span class="close vh">关闭</span></div>').appendTo($('body'));
			$popup.find('.close').on('click',function(){
				$popup.hide();
                updatePage();
			})
		},
		show:function(){
			$popup.show();
		},
		close:function(){
			$popup.hide();
		}
	}
	/**
	 * 检查弹出框是否存在
	 */
	function checkPopup(){
		if(!$('#J_popup').length){
			Popup.create();
		}else{
			Popup.show();
		}
	}
	/**
	 * 奖品判断
	 */
	 function lotteryTips(awards,txt,type){
	 	var _i = awards,
	 		_txt = txt;
	 		_type = type;
		checkPopup();
		var $content = $('#J_popup').find('.popup-content');
        if(_i == 2){
            otherGife($content,_txt);
        }else if(_i == 5){
            otherGife($content,_txt);
        }else if(_i == 6){
            getUseInfo($content,_type);
        }else if(_i == 8){
            otherGife($content,_txt);
        }else{
			getAward($content,_txt,_type);
		}
	 }

    /**
     * 其他选项
     */
    function otherGife($content,txt){
        $content.html('<p class="title">'+ txt +'</p>');
    };
	/**
	 * 不能抽奖
	 * 1.未登录
	 * 2.帮豆不足
	 */
	function noChance(type){
		checkPopup();
		var $content = $('#J_popup').find('.popup-content'),
			_type = type;
		switch(_type){
			case 1:
				$content.html('<p class="title">不好意思，您当前尚未登录!</p><div class="btn-box mt50"><a href="http://ucenter.b5m.com/forward.htm?method=/user/info/register&userType=18&url=http%3A%2F%2Fkorea.b5m.com%2Fhuodong.html" class="btn btn-2">请点击登录/注册</a></div>');
				break;
			case 2:
				$content.html('<p class="title">不存在的Api</p>');
				break;
            case 3:
                $content.html('<p class="title">已经参加过活动！</p>');
                break;
            case 4:
                $content.html('<p class="title">获取用户信息失败！</p>');
                break;
            case 5:
                $content.html('<p class="title">该活动仅限新注册的用户！</p>');
                break;
            case 6:
                $content.html('<p class="title">该活动仅限韩国馆注册用户！</p>');
                break;
            case 7:
                $content.html('<p class="title">此活动还未开始！</p>');
                break;
            case 8:
                $content.html('<p class="title">您已经提交过信息！</p>');
                break;
            case 9:
                $content.html('<p class="title">信息提交失败！</p>');
                break;
            case 10:
                $content.html('<p class="title">抽奖失败！</p>');
                break;
		}
	}

	/**
	 * 再来一次
	 */
	function tryAgain($content,txt){
		$content.html('<p class="title">恭喜您！获得了再来一次的机会。</p><div class="btn-box mt50"><a href="#" class="btn btn-1">点击再次抽奖</a></div>');
		// 关闭弹窗
		$('#J_popup').find('.btn').on('click',function(e){
			e.preventDefault();
			Popup.close();
		});	
	}
	/**
	 * 手机、QQ币、帮豆礼品等
	 */
	function getAward($content,txt,type){
		$content.html('<p class="title">恭喜您！获得了'+ txt +'</p>');
	}
	/**
	 *填写表单
	 */
	function getUseInfo($content,type){
		var _type = type;
        $content.html('<h2 class="form-title">请填写您详细的联系方式</h2> <div class="popup-in form"> <div class="item"><label for="" class="label">姓名：</label><input class="input-txt" type="text" id="u-name"></div> <div class="item"><label for="" class="label">地址：</label><input class="input-txt" id="u-address" type="text"></div> <div class="item"><label for="" class="label">联系电话：</label><input class="input-txt" id="u-phone" type="text"></div> </div> <div class="btn-box mt15"> <a href="javascript:void(0);" class="btn btn-2">提交</a> </div>');

		$('#J_popup').find('.btn').on('click',function(){
            var userNameVal = $content.find('#u-name').val(),
                addressVal = $content.find('#u-address').val(),
                phoneVal = $content.find('#u-phone').val();
            $.ajax({
                url: 'http://haiwai.b5m.com/event/luckdraw?op=saveInfo',
                type: 'post',
                data:{username:userNameVal,address:addressVal,phone:phoneVal},
                dataType:"jsonp",
                jsonp: 'jsonpCallback',
                success:function(data){
                    if(data['code'] == 200){
                        awardsTips($content,_type);
                    };
                    awardsTips($content,_type);
                }
            });
		});
	}
	/**
	 * 友情提示
	 */
	function awardsTips($content,type){
		var _type = type;
		switch(_type){
			case 'cd':
				$content.html('<p class="title">感谢您的配合，我们会在x个工作日内寄出奖品，请您注意查收。</p> ');
				break;
		}
		// 关闭弹窗
		$('#J_popup').find('.btn').on('click',function(){
			Popup.close();
		});			
	}

    if (Cookies.get('login') !== 'true') {
        noChance(1);
        return;
    }

    function fillZero(num, digit)
    {
        var str = '' + num;

        while (str.length < digit)
        {
            str = '0' + str;
        }

        return parseInt(str, 10);
    }

    function countDown()
    {
        var oNowTime = new Date().getTime();
        var oStartTime = new Date().setHours(9, 59, 59);
        var oEndTime = new Date().setHours(23, 59, 59);
        var iRemain = 0;
        var oStopTime;

        iRemain = (oNowTime - oStartTime) / 1000;

        if (iRemain > 0)
        {
            $('.down-now').addClass('down-end');
            oStopTime = (oEndTime - oNowTime) / 1000;
            formatTime(oStopTime);
        }
        else
        {
            $('.down-now').removeClass('down-end');
            formatTime(-iRemain);
        }
    }

    function formatTime(iRemain)
    {
        var iHour = 0;
        var iMin = 0;
        var iSec = 0;

        iHour = parseInt((iRemain / 3600), 10);
        iRemain %= 3600;

        iMin = parseInt((iRemain / 60), 10);
        iRemain %= 60;

        iSec = iRemain;
        // $('#J_dealCountDown').html('<span>' + fillZero(iHour, 2) + '<i></i></span><span>' + fillZero(iMin, 2) + '<i></i></span><span>' + fillZero(iSec, 2) + '<i></i></span>')
        $('.time-countdown').html(fillZero(iHour, 2) + '时' + fillZero(iMin, 2) + '分' + fillZero(iSec, 2) + '秒');
    }
    countDown();
    // setInterval(countDown, 1000);


	var timeElem = $('.time-countdown')[0];
	/*countdownTime(Date.parse(new Date('2013/11/15 14:08:20')) - Date.parse(new Date()),timeElem,function() {
	    timeElem.innerHTML = '活动进行中...';
	});*/
/*
var nowData = new Date();
countdownTime(Date.parse(new Date(nowData.getFullYear()+'/'+(nowData.getMonth()+1)+'/'+nowData.getDate()))+(3600000*24)-Date.parse(nowData),timeElem,function() {
	    timeElem.innerHTML = '活动进行中...'
	})*/

    function updatePage() {
        flag = false;
        setTimeout(function() {
            location.href = location.href;
        },1000);
    }
	
})(jQuery);
