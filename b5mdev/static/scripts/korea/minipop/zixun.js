var zixunFed={
    tabFun:function(index,cIndex){
        $('.tab-tit').find('li').eq(index).addClass('cur').siblings('li').removeClass('cur');
        $('.tab-mian').eq(index).show().siblings('.tab-mian').hide();
        $('.tab-mian').eq(index).find('.tab-ul').find('dt').show();
        $('.tab-mian').eq(index).find('.tab-ul').find('dd').hide();
        $('.tab-mian').eq(index).find('.tab-ul').eq(cIndex).find('dt').hide();
        $('.tab-mian').eq(index).find('.tab-ul').eq(cIndex).find('dd').show();
    },
    tabHover:function(){
        var $index = 0,
            $cIndex = 0,
            _this = this;
        /**
         * tab标签切换
         */
        $('.tab-tit').find('li').hover(function(){
            $index = $(this).index();

            _this.tabFun($index,$cIndex);
        });

        $('.tab-mian').find('.tab-ul').hover(function(){
            var $cIndex = $(this).index();

            _this.tabFun($index,$cIndex);
        });
        _this.tabFun($index,$cIndex);
    },
    picSlide:function(){
        var liW = 196,
            iNow = 0;
        $('.right-btn').click(function(){
            var ulItems = $(this).siblings('.pic-items'),
                liL = ulItems.find('li').length;

            ulItems.width(liW*liL+'px');
            iNow++;
            if(iNow > liL -1){
                iNow = 0;
            }
            ulItems.stop(true,true).animate({'left':-iNow*liW+'px'},400)
        });
        $('.left-btn').click(function(){
            var ulItems = $(this).siblings('.pic-items'),
                liL = ulItems.find('li').length;
            iNow--;

            ulItems.width(liW*liL+'px');
            if(iNow < 0){
                iNow = liL -1;
            }
            ulItems.stop(true,true).animate({'left':-iNow*liW+'px'},400)
        });
    },
    searchFun:function(){
        $('.search').on('click','.btn',function(){
            var words = $(this).siblings('.text').val();
            if(words){
                window.open('http://www.baidu.com/s?wd='+words);
                return false;
            }else{
                return false;
            };
        });
    },
    loadFun:function(){
        var _this = this;

        _this.tabHover();
        _this.searchFun();
    }
};