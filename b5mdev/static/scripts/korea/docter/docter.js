(function(window, $){

    var koreaDocterFed = window.koreaDocterFed = {};

    koreaDocterFed.docterInit = function(){
        var _tab = $('.busan-tab');
        _tab.each(function(){
            var _this = $(this),
                _list = _this.find('li'),
                _cont = _this.siblings('.busan-tab_cont').find('.busan-tab-item');
            _list.each(function(){
                var _self = $(this),
                    _index = _self.index();
                _self.mouseenter(function(){
                    _list.removeClass('current');
                    _self.addClass('current');
                    _cont.hide();
                    _cont.eq(_index).show();
                });
            });
        });

        $('.jcarousel').jcarousel({wrap: 'circular'}).jcarouselAutoscroll({
            interval: 3000,
            autostart: true
        });
        $('.jcarousel1').jcarousel({wrap: 'circular'}).jcarouselAutoscroll({
            interval: 3000,
            autostart: true
        });
        $('.jcarousel-pagination')
            .on('jcarouselpagination:active', 'a', function() {
                $(this).addClass('active');
            })
            .on('jcarouselpagination:inactive', 'a', function() {
                $(this).removeClass('active');
            })
            .jcarouselPagination();
    };

    koreaDocterFed.picTitHoverFun = function(){
        $('.busan-rcont').find('li').hover(function(){
            $(this).find('.pic span').stop(true,true).fadeIn();
        },function(){
            $(this).find('.pic span').stop(true,true).fadeOut();
        });
    };
    koreaDocterFed.docterFun = function(){
        var _this = this;

        _this.docterInit();

        $('.J_tab').tab({
            event: 'mouseenter',
            auto: true,
            speeds: 5000
        });

        //轮播图
        $('.gallerypic').b5mGalleryPic({
            maction: 'mouseover'
        });
    };
})(window, jQuery);

