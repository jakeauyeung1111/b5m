(function(window, $){

    var koreaTripFed = window.koreaTripFed = {};

    koreaTripFed.tripInit = function(){
        //享受生活图片切换
        var J_trip_food = $('.J_trip_food');
        J_trip_food.each(function(){
            var $this = $(this),
                J_food_img = $this.find('.J_food_img'),
                J_food_list = $this.find('.J_food_list'),
                _item = J_food_list.find('dl'),
                J_food_tit = $this.find('.J_food_tit');
            J_food_img.attr({
                src: _item.eq(0).find('a').attr('data-pic'),
                alt: _item.eq(0).find('a').text(),
                title: _item.eq(0).find('a').text()
            });
            J_food_tit.html(_item.eq(0).find('a').text());
            _item.mouseenter(function(){
                var $self = $(this),
                    _data_pic = $self.find('a').attr('data-pic'),
                    _title = $self.find('a').text();
                J_food_img.attr({
                    src: _data_pic,
                    alt: _title,
                    title: _title
                });
                J_food_tit.html(_title);
            });
        });
        //汇率换算
        var J_rate = parseFloat($('#J_rate').val()),
        	J_own = $('#J_own'),
        	J_exchange = $('#J_exchange'),
            J_money = $('#J_money'),
            J_exchange_result = $('#J_exchange_result'),
            _html;

        $('#J_convert_fun').click(function(e){
        	e.preventDefault();
        	var J_own_val = J_own.val(),
        		J_own_data = J_own.attr('data-num'),
        		J_exchange_val = J_exchange.val(),
        		J_exchange_data = J_exchange.attr('data-num');
        	
        	J_own.val(J_exchange_val).attr('data-num', J_exchange_data);
            J_exchange.val(J_own_val).attr('data-num', J_own_data);
            if (J_money.val() !== 0) {
                var _val = J_money.val();
                if ((/^\d{1,10}(\.\d{1,2})?$/g).test(_val)) {
                    if (parseInt(J_own.attr('data-num'))) {
                        //韩元兑换成人民币
                        _html = (_val * J_rate).toFixed(2).toString();
                    } else {
                        //人民币兑换成韩元
                        _html = (_val / J_rate).toFixed(2).toString();
                    }
                } else {
                    _html = '0.00';
                }
                if (/(-?[0-9]+)([0-9]{3})/.test(_html)) _html = _html.replace(/(-?[0-9]+)([0-9]{3})/, '$1,$2');
                J_exchange_result.html(_html);
            }
        });
        J_money.keyup(function(){
            var $this = $(this),
                _val = $this.val();
            if ((/^\d{1,10}(\.\d{1,2})?$/g).test(_val)) {
                if (parseInt(J_own.attr('data-num'))) {
                    //韩元兑换成人民币
                    _html = (_val * J_rate).toFixed(2).toString();
                } else {
                    //人民币兑换成韩元
                    _html = (_val / J_rate).toFixed(2).toString();
                }
            } else {
                _html = '0.00';
            }
            if (/(-?[0-9]+)([0-9]{3})/.test(_html)) _html = _html.replace(/(-?[0-9]+)([0-9]{3})/, '$1,$2');
            J_exchange_result.html(_html);
        });
        //地图
        var swfdata = {
            "data":[
                {"name":"首尔","x":337,"y":80,"position":"top","url":"","info": "首尔，韩国首都，韩国最大的城市，也是朝鲜半岛最大城市。世界十大国际大都市之一，前称汉城。"},
                {"name":"江原道","x":412,"y":56,"position":"bottom","url":"http://korea.b5m.com/lyxd/jyd/102-1.html","info": "韩剧里不可或缺的取景地，是个浪漫风景秀丽的地方,江原道下辖7市11郡24邑74洞。"},
                {"name":"仁川","x":306,"y":88,"position":"bottom","url":"http://korea.b5m.com/lyxd/rc/96-1.html","info": "韩国的第三大城市，作为东北亚的中心城市，是走向世界的交通要塞，拥有最先进的大型国际机场"},
                {"name":"京畿道","x":346,"y":110,"position":"bottom","url":"http://korea.b5m.com/lyxd/jjd/100-1.html","info": "京畿道是韩国的一个道级行政区域，占全国人口的20%以上，在经济、社会中具有很大的影响力。"},
                {"name":"郁陵岛","x":561,"y":86,"position":"bottom","url":"http://korea.b5m.com/lyxd/yld/99-1.html","info": "郁陵岛为一火山岛，是火山喷发后形成的钟状火山岛,日治时期原名松岛，欧洲人称其为Dagelet。"},
                {"name":"大邱","x":430,"y":176,"position":"top","url":"http://korea.b5m.com/lyxd/dq/98-1.html","info": "大邱广域市，韩国东南部庆尚北道首府。四周有群山环抱，琴湖江穿过市区，流入洛东江。"},
                {"name":"庆州","x":469,"y":215,"position":"top","url":"http://korea.b5m.com/lyxd/qz/97-1.html","info": "庆州是韩国主要观光城市，也是古代新罗王国首都金城，有“无围墙的博物馆”之称。"},
                {"name":"釜山","x":459,"y":269,"position":"top","url":"http://korea.b5m.com/fsg/89-1.html","info": "韩国的第二大城市，多种文化共存的国际性现代都市，“釜山国际电影节”闻名世界。"},
                {"name":"济州岛","x":308,"y":395,"position":"top","url":"http://korea.b5m.com/lyxd/jzd/101-1.html","info": "济州岛是韩国最大的岛屿，一座典型的火山岛，是世界新七大自然奇观之一素有“韩国夏威夷”之称。"}
            ]
        };

        swfobject.embedSWF("/static/lyxd/mainv1.swf","swftravel", "700", "430", "11.0.0", "expressInstall.swf",{'datas':encodeURIComponent(JSON.stringify(swfdata))},{'wmode':'transparent'});
    };
    koreaTripFed.detailFun = function(){
       var tripDetailHtml = $('.detail-cont').find('.cont');
        if(tripDetailHtml[0]) {
            tripDetailHtml.html(tripDetailHtml.html().replace(/#titleS#/g,'<div class="detail-add"> ').replace(/#titleE#/g,'</div>'))
        }
    };

    koreaTripFed.tripFun = function(){
        var _this = this;

        _this.tripInit();
        _this.detailFun();

        //轮播图
        $('.gallerypic').b5mGalleryPic({
            maction: 'mouseover'
        });
    };
})(window, jQuery);