(function(window, $){
    //资讯top10
    function setTopList(el){
        var $top_items = el.find('h3');
        $top_items.eq(0).parent().addClass('current');
        $top_items.mouseenter(function(){
            var $pa = $(this).parent();
            $pa.addClass('current').siblings().removeClass('current');
        });
    }

    setTopList($('#J_top'));
    setTopList($('#J_hot_0'));
    setTopList($('#J_hot_1'));
    //文章图片
    var $J_img = $('.J_img').find('img'),
        $J_img_cur = $('.J_img_cur'),
        $J_img_all = $('.J_img_all'),
        $J_img_prev = $('.J_img_prev'),
        $J_img_next = $('.J_img_next'),
        len = $J_img.length;
    $J_img.eq(0).addClass('show');
    $J_img_cur.text(1);
    $J_img_all.text(len);
    if (len) {
        var num = $J_img.filter('.show').index();
        $J_img_prev.click(function(e){
            e.preventDefault();
            num--;
            if (num < 0) {
                num = len - 1;
            }
            $J_img_cur.text(num + 1);
            $J_img.eq(num).addClass('show').siblings().removeClass('show');
        });
        $J_img_next.click(function(e){
            e.preventDefault();
            num++;
            if (num > len - 1) {
                num = 0;
            }
            $J_img_cur.text(num + 1);
            $J_img.eq(num).addClass('show').siblings().removeClass('show');
        });
    }
    //热点
    var $J_tab = $('.J_tab');
    $J_tab.each(function(){
        var $this = $(this),
            $J_tab_item = $this.find('.J_tab_item').find('li'),
            $J_cont_item = $this.find('.J_cont_item'),
            _h = $J_cont_item.height(),
            $J_tab_detail = $this.find('.J_tab_detail');
        $J_tab_item.mouseenter(function(){
            var i = $(this).index();
            $(this).addClass('current').siblings().removeClass('current');
            $J_tab_detail.animate({
                top: -(_h * i) + 'px'
            }, 500);
        });
    });
})(window, jQuery);
