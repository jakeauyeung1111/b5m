//顶部通栏生成
b5m.namespace('ui.topbar');
b5m.ui.topbar = (function() {

    'use strict';

   //函数接口
   function Topbar() {

       var _location = location.href,
           _domain = '.b5m.com',
           _this = this;

       this.topBarContent = $('.tpbar').eq(0);

       this.isLogin = Cookies.get('login') === 'true' && Cookies.get('token');  //判断登录状态
       b5m.info.isLogin = this.isLogin; //给外部调用，现也可直接调用：b5m.ui.topbar.isLogin

       this.userId = Cookies.get('token');

       //各环境url
       this.url = (function () {
           if (location.hostname.indexOf('stage.bang5mai.com') !== -1) {
               _domain = '.bang5mai.com';
               return 'ucenter.stage.bang5mai.com';
           }
           if (location.hostname.indexOf('prod.bang5mai.com') !== -1) {
               _domain = '.bang5mai.com';
               return 'ucenter.prod.bang5mai.com';
           }
           if (location.hostname.indexOf('ucenter.test.com') !== -1) {
               _domain = '.test.com';
               return 'ucenter.test.com';
           }
           return 'ucenter.b5m.com';
       })();

       //生成topbar dom
       function createDom() {

           //登录之后跳回当前页面地址
           var refererUrl = (function() {
               try {
                   if(location.search.indexOf('loginReferer') !== -1) {
                       return /loginReferer=([^&]*)/.exec(location.search)[1];
                   }
               } catch (e) {}
               return encodeURIComponent(_location);
           })();

           //登录标识
           var userType = (function() {
               if(_location.indexOf('www.b5m.com/forum.php') !== -1) {
                   return 17;
               }
               switch(location.host) {
                   case 'www.b5m.com':
                       return 8;
                   case 's.b5m.com':
                       return 22;
                   case 'haiwai.b5m.com':
                       return 23;
                   case 'zdm.b5m.com':
                       return 20;
                   case 'tejia.b5m.com':
                       return 6;
                   case 'tuan.b5m.com':
                       return 16;
                   case 'you.b5m.com':
                       return 13;
                   case 'piao.b5m.com':
                       return 19;
                   case 'she.b5m.com':
                       return 17;
                   case 't.b5m.com':
                       return 5;
                   case 'korea.b5m.com':
                       return 18;
                   case 'plus.gwmei.com':
                       return 15;
                   case 'www.gwmei.com':
                       return 4;
                   case 'hao.b5m.com':
                       return 7;
                   case 'tiao.b5m.com':
                       return 9;
                   case 'guang.b5m.com':
                       return 25;
                   case 'daikuan.b5m.com':
                       return 24;
                   case 'gzx.b5m.com':
                       return 26;
                   case 'yisheng.b5m.com':
                       return 21;
                   case 'usa.b5m.com':
                       return 27;
                   case 'tao.b5m.com':
                       return 29;
                   default:
                       return 0;
               }
           })();

           var content = '';

           content += '<div class="tpbar-cont">';

           content += '<div class="l-nav"><i class="iconfont">&#xB001;</i> <a href="#">帮5买</a> 服从购物渴望</div>';

           content +=  '<ul class="login-bar cf">';

               //未登录显示登录注册
               if(!_this.isLogin) {

                   content += '<li><i class="iconfont ico-qq"><a title="qq登录" href="http://'+ _this.url +'/user/third/login/auth.htm?type=1&amp;refererUrl=' + refererUrl +'&amp;userType='+ userType +'">&#xB003;</a></i></li>';

                   content += '<li><i class="iconfont ico-sina"><a title="微博登录" href="http://'+ _this.url +'/user/third/login/auth.htm?type=2&amp;refererUrl=' + refererUrl + '&amp;userType='+ userType +'">&#xB002;</a></i></li>';

                   content += ' <li><a href="http://'+ _this.url +'?loginReferer=' + refererUrl +'" >登录</a></li><li class="sp">|</li>';

                   content += '<li><a href="http://'+ _this.url +'/forward.htm?method=/user/info/register&amp;userType='+ userType +'&amp;url=' + refererUrl +'">注册</a></li>';

               }

               //已登录显示个人信息、消息、收藏
               else {

                   //个人信息
                   content += '  <li><div class="menu-hover"><a id="b5muser" href="http://ucenter.b5m.com/forward.htm?method=/user/user/index" class="menu-hd noico"><span></span><b></b></a><div class="menu-bd bd-user"><ul class="menu-bd-list"><ol><a href="http://ucenter.b5m.com/forward.htm?method=/user/account/info/index">账户信息</a></ol><ol><a href="http://ucenter.b5m.com/forward.htm?method=/user/msg/system/index">消息应用</a></ol><ol><a href="http://ucenter.b5m.com/forward.htm?method=/user/account/favorites/index">我的收藏</a></ol><ol><a href="http://ucenter.b5m.com/forward.htm?method=/user/account/invitation/index">我的邀请</a></ol><ol><a href="http://ucenter.b5m.com/forward.htm?method=/user/trade/common/record/index">我的帮豆</a></ol><ol><a href="http://ucenter.b5m.com/user/user/logout.htm">退出登录</a></ol></ul></div></div></li>';


                   content += '<li class="sp">|</li>';

                   //我的消息
                   content += '<li><div class="menu-hover"><a  href="javascript:void(0)" class="menu-hd noico hd-msg">我的消息<b></b></a><div class="menu-bd bd-msg"><div class="bd-panel"><h3>我的消息</h3><div class="loading"><span>加载中...</span></div><ul class="menu-msg-info"></ul><p><a href="http://ucenter.b5m.com/forward.htm?method=/user/msg/system/index" class="btn-more">显示更多</a></p></div></div></div></li>';

                   content += '<li class="sp">|</li>';

                   //我的收藏
                   content += ' <li><div class="menu-hover"><a href="javascript:void(0)" class="menu-hd hd-add"><i class="iconfont">&#xB006;</i> 我的收藏 <b></b></a><div class="menu-bd bd-add"><div class="bd-panel"><h3>我的收藏</h3><div class="loading"><span>加载中...</span></div><ul class="menu-add-info"></ul></div></div></div></li>';

               }

           content += '<li class="sp">|</li>';

             //关注我们
           content += '<li><div class="menu-hover"><a href="javascript:void(0)" class="menu-hd noico hd-follow">关注我们 <b></b></a><div class="menu-bd bd-follow"><div class="bd-panel"><h3><i class="iconfont ico-wx">&#xB007;</i> 微信</h3><p><span class="qrcode-wx"></span></p><div class="cf"><a href="http://e.t.qq.com/bang5mai" title="腾讯微博" target="_blank" class="follow-qq"></a><a title="新浪微博" target="_blank" href="http://weibo.com/bang5mai" class="follow-weibo"></a><span class="r">微博：</span></div></div></div></div></li>';

           content += '<li class="sp">|</li>';

           //购物助手
           content += '<li><div class="menu-hover"><a href="javascript:void(0)" class="menu-hd noico">购物助手 <b></b></a><div class="menu-bd bd-helper"><div class="bd-panel"><h3>购物助手</h3><p>安装帮5淘购物助手，实时展示商品历史价格，帮你淘到商品最低价</p><div class="cf"><a href="http://cdn.b5m.cn/upload/plugin/clients/marketing/b5t_latest.exe" class="bd-helper-download"> </a> <a href="http://t.b5m.com" class="bd-helper-detail">详情<span>></span></a></div></div></div></div></li>';

           content += '<li class="sp">|</li>';

           //手机帮5买
           content += '<li><div class="menu-hover"><a href="javascript:void(0)" class="menu-hd hd-phone"><i class="iconfont">&#xB004;</i> 手机帮5买 <b></b></a><div class="menu-bd bd-phone"><div class="bd-panel"><div class="cf"><div class="l"><span class="qrcode-iphone">app store</span></div><div class="r"><span class="qrcode-android">android</span></div><div class="l"><a href="#" class="btn-iphone">app store</a></div><div class="r"><a href="#" class="btn-android">android</a></div></div><p class="t-r"><a href="http://m.b5m.com" class="bd-phone-detail">详情 <span>></span></a></p></div></div></div></li>';

           content += '<li class="sp">|</li>';

           //收藏本站
           content += '<li><a href="javascript:void(0)" class="add-fav">收藏本站</a></li>';

           content += '<li class="sp">|</li>';

           //网站导航
           content += ' <li><div class="menu-hover"><a href="javascript:void(0)" class="menu-hd"><i class="iconfont">&#xB005;</i> 导航网站 <b></b></a><div class="menu-bd bd-dh"><ul class="menu-bd-list">' + _this.linkNav() + '</ul></div></div></li>';

           content += '</ul>';

           content += '</div>';

           _this.topBarContent.html(content);

       };


       //获取个人信息
       function getUser() {
           $.ajax({
               url:'http://'+ _this.url +'/user/user/data/info.htm?isSimple=1',
               dataType:'jsonp',
               jsonp:'jsonpCallback'
           }).success(function(data) {
               if(!data.ok) {
                 Cookies.set('login','false',new Date(),'',_domain);
                 location.href = 'http://'+ _this.url;
                 return;
                }
                $('#b5muser span').text(decodeURIComponent(data.data.showName));
           });
       };


       //获取我的消息
       function getMsg() {

           var elem = _this.topBarContent.find('.hd-msg'),
               cont = _this.topBarContent.find('.bd-msg'),
               list = cont.find('ul');

           elem.on({
               mouseenter:function() {

                   if($(this).data('loaded')) {
                       return false;
                   }
                   $(this).data('loaded',true);

                   $.ajax({
                       url:'http://'+ _this.url +'/user/message/data/list.htm',
                       dataType:'jsonp',
                       jsonp:'jsonpCallback'
                   }).success(function(data) {

                      if(!data.data.records) {
                          cont.find('.loading').text('暂无消息');
                      }else {
                          cont.find('.loading').detach();

                          list.show().on('click','a',function() {
                              $(this).hide();
                              $(this).parents('ol').find('i').hide();
                              $(this).parents('ol').find('strong').show();
                          });

                          var text ='';
                          for(var i = 0;i < data.data.records.length;i++) {
                              if(i === 5) {
                                  cont.find('.btn-more').css('display','block');
                                  break;
                              }
                              text += '<ol><p><b>'+ data.data.records[i].fromStr +'</b> <i>'+ data.data.records[i].title +'</i><strong>'+ data.data.records[i].content +'</strong></p><div> <a href="javascript:void(0)">点击查看详情 <em>></em></a><span>'+ _this.setDateFormat.call(new Date(data.data.records[i].time),'yyyy-MM-dd hh:mm:ss') +' 来自'+ data.data.records[i].fromStr +'</span></div></ol>';
                          }
                          list.append(text);
                      }

                   });

                   return false;
              }
           });

       };


       //获取我的收藏
       function getAdd() {

           var elem = _this.topBarContent.find('.hd-add'),
               cont = _this.topBarContent.find('.bd-add'),
               list = cont.find('ul'),
               jscroll;

           elem.on({
               mouseenter:function() {

                   if($(this).data('loaded')) {
                       return false;
                   }
                   $(this).data('loaded',true);

                   $.ajax({
                       url:'http://'+ _this.url +'/gc/user/favorites/data/query.htm',
                       dataType:'jsonp',
                       type:'POST',
                       data:{userId:_this.userId,pageNum:1,pageSize:8,priceType:0},
                       jsonp:'jsonpCallback'
                   }).success(function(data) {

                           if(!data.data.list.length) {
                               cont.find('.loading').text('暂无收藏');
                           }else {

                               cont.find('.loading').detach();

                               var texts = '';
                               for(var i=0;i<data.data.list.length;i++) {
                                   var text = '';
                                   text+=  '<ol class="cf">';
                                   text+=  '<div class="add-img"><a href="'+ data.data.list[i].goodsUrl +'"><img src="'+ data.data.list[i].picUrl +'" width="50" height="50" alt="'+ data.data.list[i].title +'"/></a></div>';
                                   text+=  '<div class="add-text"><a href="'+ data.data.list[i].goodsUrl +'">'+ data.data.list[i].title +'</a></div>';
                                   text+=  '<div class="add-price">';
                                   text+=  '<strong>现价： <em>￥'+ data.data.list[i].price +'</em></strong>';
                                   text+=  '<span>原价： <em>￥'+ data.data.list[i].collectionPrice +'</em></span>';
                                   text+=  '</div>';
                                   text+=  '<div class="add-opt"><a data-ugcid="'+ data.data.list[i].ugcId +'" href="javascript:void(0)">删除</a></div>';
                                   text+=  '</ol>';
                                   texts += text;
                               }

                               list.append(texts).on('click','.add-opt a',function(e) {
                                   $.ajax({
                                       url:'http://'+ _this.url +'/gc/user/favorites/data/delete.htm',
                                       dataType:'jsonp',
                                       type:'POST',
                                       data:{userId:_this.userId,ugcId:$(e.target).attr('data-ugcid')},
                                       jsonp:'jsonpCallback'}).success(function() {
                                          $(e.target).parents('ol').detach();
                                           if(jscroll) {
                                               jscroll.update();
                                               list.css('height','auto');
                                           }
                                       });
                               });

                               //如果收藏数>5 ，seajs 加载jscrollpane
                               if(data.data.list.length > 5) {
                                   seajs.use(['ui/jscrollpane/2.0.19/jscrollpane'],function(Jscroll) {
                                       list.show();
                                       jscroll = new Jscroll({
                                           container:list
                                       });
                                       elem.mouseenter(function() {
                                           jscroll.update();
                                       });
                                   });
                               }else {
                                    list.css('height','auto').show();
                               }
                           }
                       });

               }
           });
       };


       //设置我的消息日期格式
       this.setDateFormat = function(format) {
           var o ={
               "M+" : this.getMonth()+1, //month
               "d+" : this.getDate(),    //day
               "h+" : this.getHours(),   //hour
               "m+" : this.getMinutes(), //minute
               "s+" : this.getSeconds(), //second
               "q+" : Math.floor((this.getMonth()+3)/3),  //quarter
               "S" : this.getMilliseconds() //millisecond
           };
           if(/(y+)/.test(format)){
               format=format.replace(RegExp.$1,(this.getFullYear()+"").substr(4 - RegExp.$1.length));
           }
           for(var k in o) {
               if(new RegExp("("+ k +")").test(format)) {
                   format = format.replace(RegExp.$1,RegExp.$1.length==1 ? o[k] : ("00"+ o[k]).substr((""+ o[k]).length));
               }
           }
           return format;
       };

       //网站导航
       this.linkNav = function() {
           var linkArr = [
               '<a href="http://tuan.b5m.com">帮团购</a>',
               '<a href="http://tejia.b5m.com">淘特价</a>',
               '<a href="http://haiwai.b5m.com">海外馆</a>',
               '<a href="http://zdm.b5m.com">值得买</a>',
               '<a href="http://you.b5m.com">帮5游</a>',
               '<a href="http://piao.b5m.com">帮票务</a>',
               '<a href="http://dai.b5m.com">帮贷款</a>',
               '<a href="http://yisheng.b5m.com/">找医生</a>',
               '<a href="http://hao.b5m.com">网址导航</a>',
               '<a href="http://gzx.b5m.com/">购真相</a>',
               '<a href="http://t.b5m.com/">帮5淘</a>',
               '<a href="http://m.b5m.com/">手机帮5买</a>'
            ];
           return '<ol>' + linkArr.join().replace(/,/g,'</ol><ol>') + '</ol>';
       };


       //收藏功能
        function addFav() {
           var addFav = _this.topBarContent.find('.add-fav');
           addFav.on('click', function(e) {
               var url = encodeURI(location.host);
               var title = $('title').html();
               if (window.external) {
                   try {
                       window.external.addFavorite(location.href, document.title);
                   } catch (e) {
                       showConfirm('加入收藏失败，请使用Ctrl+D进行添加');
                   }
               } else if (window.sidebar) {
                   window.sidebar.addPanel(document.title, location.href, '');
               } else {
                   showConfirm('加入收藏失败，请使用Ctrl+D进行添加');
               }
               return false;
           });
       };


       function showConfirm(text) {
           seajs.use(['arale/dialog/1.3.0/confirmbox','seajs/seajs-style/1.0.2/seajs-style'],function(ConfirmBox) {
               ConfirmBox.show(text,function() {},{width:300});
           });
       }

       //初始化
       this.init = (function() {
           createDom(); //生成tpbar dom
           if(_this.isLogin) {
               getUser();  //获取个人信息
               getMsg();  //获取我的消息
               getAdd();  //获取我的收藏
           }
           addFav(); //收藏
       })();

   }



    //IE6 tips
    if(typeof document.body.style.maxHeight ==='undefined') {
        //顶部提示下载
        $('body').prepend('<div class="topbar-ie6">您的浏览器版本过旧，推荐您使用更快更安全的360浏览器 <a href="http://se.360.cn/" target="_blank">立即下载</a></div>');

        $('.tpbar').on('mouseenter','.menu-hover',function() {
            $(this).addClass('hover');
        });

        $('.tpbar').on('mouseleave','.menu-hover',function() {
            $(this).removeClass('hover');
        });
    }

   return new Topbar();

})();