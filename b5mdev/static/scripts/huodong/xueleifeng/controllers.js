packetApp.controller('prizeCtrl', ['$scope','prizeSer',function ($scope,prizeSer) {
		prizeSer.getPize().success(function(data){
			if(!data){
				return false;
			}

			$scope.prizes = data;
		})
}]);

function getById(elem){
    return elem ? document.getElementById(elem):'';
}

var speed = 40,
    origin = getById('J_container_origin'),
    copy = getById('J_container_copy'),
    container = getById('J_container');

copy.innerHTML = origin.innerHTML + origin.innerHTML;

function Marquee(){
    if(copy.offsetLeft - container.scrollLeft <= 0){
        container.scrollLeft -= origin.offsetWidth;
    }else{
        container.scrollLeft++;
    }
}

var sliderFlag = setInterval(Marquee,speed);

container.onmouseover = function(){
    clearInterval(sliderFlag);
}

container.onmouseout = function(){
    sliderFlag = setInterval(Marquee,speed);
}
    