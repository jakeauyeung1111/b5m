/**
 * 奖品信息
 * @return jsonp对象
 */
packetApp.provider('packetSer', [function () {
	this.$get = ['$http',function($http) {
		return {
			// baseUrl:'http://172.16.2.96/qianghongbao.htm?type=',
			baseUrl:'http://www.b5m.com/qianghongbao.htm?type=',
			jsonpCallback:'&jsonpCallback=JSON_CALLBACK',
			getStatus:function(type){
				if(!type){
					return false;
				}

				var url = this.baseUrl + type + this.jsonpCallback;

				return $http.jsonp(url);
			}
		}
	}];
}]);
/**
 * 获得中奖名单
 * @return jsonp 对象
 */
packetApp.provider('prizeSer', [function () {
	this.$get = ['$http',function($http) {
		return {
			baseUrl:'http://www.b5m.com/qianghongbao.htm?info=info',
			jsonpCallback:'&jsonpCallback=JSON_CALLBACK',
			getPize:function(){
				var url = this.baseUrl + this.jsonpCallback;	
				return $http.jsonp(url);
			}
		};
	}];
}]);
