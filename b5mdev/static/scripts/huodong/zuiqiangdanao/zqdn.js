
var questionApp = angular.module('questionApp',[]);

questionApp.controller('questionListCtrl',function($scope){
	$scope.questions = {
		title:{
			num:1,
			name:'最好吃的水果'
		},
		options:[
			{
				'content':'苹果'
			},
			{
				'content':'梨'
			},
			{
				'content':'香蕉'
			},
			{
				'content':'猕猴桃'
			}
		]
	}

	$scope.questionClick = function(txt){
		console.log(txt.content);
	}
	
	$scope.closeDialog = function(){
		$scope.hideDialog = false;
	}

});