questionApp.controller('answerController',['$scope','$location','$rootScope',function($scope,$location,$rootScope){

    var btnClicked = false;
    $scope.answerQuestion = function(e){
        if(btnClicked) {
            if(typeof $rootScope.$$childTail.dialogShowResult === 'boolean') {
                 $rootScope.$$childTail.dialogShowResult = true;
            }else {
                 $rootScope.$$childTail.dialogShowQuestion = true;
            }
        }else {
            btnClicked = true;
            $location.path('/question');
        }
        angular.element('.dialog-mask').show().css('height',angular.element(document).height());
        e.preventDefault();
    };

}]);

//请求问题
questionApp.controller('questionController',['$scope','Question','$location','$route','$rootScope','$http',function($scope,Question,$location,$route,$rootScope,$http) {

    $scope.dialogShowQuestion = true;

    //关闭弹窗
    $scope.closeDialog = function(e) {
        $scope.dialogShowQuestion = false;
        angular.element('.dialog-mask').hide();
        e.preventDefault();
    };

    //调用服务里面的get方法
   Question.get(function(data) {

       if(data.code==400) {
           //未登录

           angular.element('.panel-question').hide();
           angular.element('.panel-login').show();
           angular.element('.dialog i').text('提示');

           $scope.loginClick = function(e) {
               var url = angular.element('.topbar-user a').eq(2).attr('href').replace('%23%2Fquestion','');
                location.href = url;
               e.preventDefault();
           };

       }else if(data.code === 200) {

           $scope.questions = data.data[0];

           $scope.questionClick = function(event,item) {
               clearInterval(timer);
               $(event.target).addClass('radio-on').removeClass('radio-off');
               $location.path('/result/'+ $scope.questions.questionId + '/' + item);
               $scope.dialogShowQuestion = false;
           };

           //倒计时
           var timeNum = angular.element('#J_count-down');
           var timer = setInterval(function() {
               var text = +timeNum.text();
               if(text === 0) {
                   $http.get('zuiqiangdanao.htm?operation=reduceBangdou&question_id='+ $scope.questions.questionId);
                   clearInterval(timer);
                   return false;
               }
               text--;
               timeNum.text(text);
           },1000);

       }
   });

    angular.element('.dialog-mask').show().css('height',angular.element(document).height());

}]);

//请求答案
questionApp.controller('resultController',['$scope','Result','$location','$route','$http',function($scope,Result,$location,$route,$http) {

    //关闭弹窗
    $scope.closeDialog = function(e) {
        $scope.dialogShowResult = false;
        angular.element('.dialog-mask').hide();
        e.preventDefault();
    };

    //登录
   // if($route.current.params.questionId === -1 && $route.current.params.item === -1) {
    //    $('.result-panel.show').removeClass('show');
      //  $('.result-panel').eq(7).addClass('show');
   // }else {


  //  $http.get('/zuiqiangdanao.htm?operation=getQuestionInfoById&option_id=3&question_id=1').success(function(data) {
     //   console.info(data);
   // });

       Result.get({question_id:$route.current.params.question_id,option_id:$route.current.params.option_id},function(data) {

            $scope.questions = data.data[0];
            $scope.dialogShowResult = true;

           $('.result-panel.show').removeClass('show');

           if(data.code === 400) {

               $scope.errMesssage = data.message;
               $('.result-panel').eq(6).addClass('show');
               angular.element('.dialog i').text('提示');

           }else {
               $('.result-panel').eq($scope.questions.resultType).addClass('show');
               //实时更新帮豆数
               angular.element('#J_bangdou_nums').html($scope.questions.bangdouNums.getBangdouNums);
               angular.element('#J_bangdou_nums_all').html($scope.questions.bangdouNums.allBangdouNums);
               angular.element('#J_lottery_num').html($scope.questions.bangdouNums.lotteryChance);
           }

           $scope.resultClick = function(e) {
               $location.path('/question/');
               $scope.dialogShowResult = false;
               e.preventDefault();
           };

        });

    angular.element('.dialog-mask').show().css('height',angular.element(document).height());

}]);

//console.info(111aaabb);