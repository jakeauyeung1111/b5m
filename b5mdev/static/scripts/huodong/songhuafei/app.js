var phoneChargeApp = angular.module('phoneChargeApp',[
		'ngRoute'
	]);

phoneChargeApp.config(['$routeProvider',function ($routeProvider) {
		$routeProvider
		.when('/', {
			templateUrl: 'partials/loginTpl.html',
			controller: 'loginCtrl'
		})
		.when('/mobile',{
			templateUrl:'partials/bindMobileTpl.html',
			controller:'bindMobileCtrl'
		})
		.when('/email',{
			templateUrl:'partials/bindEmailTpl.html',
			controller:'bindMailCtrl'
		})
		.when('/bill',{
			templateUrl:'partials/billTpl.html',
			controller:'billCtrl'
		})
		.otherwise({ 
			redirectTo: '/'
		})
	}
]);

// phoneChargeApp.config(['$routeProvider',function ($routeProvider) {
// 		$routeProvider
// 		.when('/', {
// 			templateUrl: '/huodong/songhuafei/partials/loginTpl.html',
// 			controller: 'loginCtrl'
// 		})
// 		.when('/mobile',{
// 			templateUrl:'/huodong/songhuafei/partials/bindMobileTpl.html',
// 			controller:'bindMobileCtrl'
// 		})
// 		.when('/email',{
// 			templateUrl:'/huodong/songhuafei/partials/bindEmailTpl.html',
// 			controller:'bindMailCtrl'
// 		})
// 		.when('/bill',{
// 			templateUrl:'/huodong/songhuafei/partials/billTpl.html',
// 			controller:'billCtrl'
// 		})
// 		.otherwise({ 
// 			redirectTo: '/'
// 		})
// 	}
// ]);
