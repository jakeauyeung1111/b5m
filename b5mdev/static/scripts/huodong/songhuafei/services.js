phoneChargeApp.factory('authCode',['$http',function ($http) {
	return {
        url:'http://ucenter.b5m.com/user/info/mobile/getCode.htm?mobile=',
        url2:'http://ucenter.b5m.com/user/info/mobile/validateCode.htm?code=',
        sendCode:function(num){
			if(!num){
				return false;
			}

			var url = this.url + num + '&jsonpCallback=JSON_CALLBACK';
			//发送验证码
			return $http.jsonp(url);
		},
		checkCode:function(code){
			if(!code){
				return false;
			}

			var url = this.url2 + code + '&jsonpCallback=JSON_CALLBACK';
			return $http.jsonp(url);
		}
	}
}]);

phoneChargeApp.factory('authEmail',['$http',function($http) {
	return {
        url:'http://ucenter.b5m.com/user/info/data/verifyEmail.htm?email=',
		sendEmail:function(email,referer){
			if(!email){
				return false;
			}

			var url = this.url + email + '&jsonpCallback=JSON_CALLBACK' + '&referer=' + referer;
			return $http.jsonp(url);
		}
	};
}]);


phoneChargeApp.factory('billCharge', ['$http',function($http) {
	return {
		url:'http://t.b5m.com/event/huafei?op=join&t=',
		getStatus:function(type){

			var url = this.url + type + '&jsonpCallback=JSON_CALLBACK';
			return $http.jsonp(url);
		},
		//获取获奖名单
		getUserData:function(num){
			var url = 'http://t.b5m.com/event/huafei?op=winuserlist&num=' + num + '&jsonpCallback=JSON_CALLBACK';

			return $http.jsonp(url);
		},
		getEventStatus:function(){
			var url = 'http://t.b5m.com/event/huafei?op=config&jsonpCallback=JSON_CALLBACK';

			return $http.jsonp(url);
		}
	};
}]);

//查看用户登录、手机、邮箱绑定信息
phoneChargeApp.factory('userInfo', ['$http',function ($http) {

	return {
		tokenId:Cookies.get('token'),
		login:Cookies.get('login'),
		url:'http://ucenter.b5m.com/user/user/data/info/htm?identifier=',
		isLogin:function(){
			return this.tokenId && (this.login == 'true');
		},
		checkBindInfo:function(){
			return $http.jsonp(this.url + this.tokenId + '&params=isMobileBind,activation,createTime,showName,email,mobile,avatar&jsonpCallback=JSON_CALLBACK');
		}
	};

}])
