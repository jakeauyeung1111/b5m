//登录
phoneChargeApp.controller('loginCtrl', ['$scope', '$location', 'userInfo',
	function($scope, $location, userInfo) {

		if (userInfo.isLogin()) {
			userInfo.checkBindInfo().success(function(data) {
				var data = data.data;

				if (data.isMobileBind != 1) {
					$location.path('/mobile');
				} else if ((data.isMobileBind == 1) && (data.activation != 'REG')) {
					$location.path('/email');
				} else if ((data.isMobileBind == 1) && (data.activation == 'REG')) {
					$location.path('/bill');
				}
			})
		}

		$scope.Login = {
			qq: $('.topbar-user-unlogin').find('.qq').attr('href'),
			weibo: $('.topbar-user-unlogin').find('.weibo').attr('href'),
			b5m: $('.topbar-user-unlogin').find('.high').attr('href'),
			register: $('.topbar-user-unlogin').find('a').eq(3).attr('href')
		}
	}
]);

//绑定手机
phoneChargeApp.controller('bindMobileCtrl', ['$scope', '$location', '$timeout', 'authCode', 'userInfo',
	function($scope, $location, $timeout, authCode, userInfo) {

		if (!userInfo.isLogin()) {
			$location.path('/');
		} else {
			userInfo.checkBindInfo().success(function(data) {
				var data = data.data;
				if ((data.isMobileBind == 1) && (data.activation != 'REG')) {
					$location.path('/email');
				} else if ((data.isMobileBind == 1) && 　(data.activation == 'REG')) {
					$location.path('/bill');
				}
			});
		}

		$scope.codeNum = '';

		$scope.Mobile = {
			codeShow: true,
			NextFlag: false,
			codeError: false,
			sendCode: function() {
				var phoneNum = $scope.phoneNum,
					base = this;

				if (this.isPhone(phoneNum)) {
					//发送验证码
					authCode.sendCode(phoneNum).success(function(data) {
						if (data.ok) {
							$scope.mobileTips.numTip = '验证码发送成功，请注意查收！';
						} else {
							$scope.mobileTips.numTip = '验证码发送失败，请重新发送！';
						}
					});

					this.codeShow = false;
					var flag = $timeout(function() {
						base.codeShow = true;
					}, 60000);

					$scope.mobileTips.numTip = '请填写本人手机号码 话费将充值到本号码';
				} else {
					$scope.mobileTips.numTip = '您输入的手机号错误，请确认后重新输入';
				}
			},
			checkCode: function() {

				var inputCode = $scope.codeNum,
					base = this;
				//校验验证码
				authCode.checkCode(inputCode).success(function(data) {
					if (data.ok) {
						base.NextFlag = true;
						base.codeError = false;
					} else {
						base.NextFlag = false;
						base.codeError = true;
					}
					$scope.mobileTips.codeTip = data.data;
				})
			},
			goToBindEmail: function() {
				$location.path('/email')
			},
			isPhone: function(num) {
				// var regPhone = /^0?(13[0-9]|15[012356789]|18[02356789]|14[57])[0-9]{8}$/;
				var regPhone = /^1\d{10}$/;
				return regPhone.test(num);
			}
		}

		//验证信息
		$scope.mobileTips = {
			numTip: '请填写本人手机号码 话费将充值到本号码',
			codeTip: '您输入的验证码错误，请确认后重新输入'
		}
	}
]);

//绑定邮箱
phoneChargeApp.controller('bindMailCtrl', ['$scope', '$location', '$timeout', 'authEmail', 'userInfo',
	function($scope, $location, $timeout, authEmail, userInfo) {

		if (!userInfo.isLogin()) {
			$location.path('/');
		} else {
			userInfo.checkBindInfo().success(function(data) {
				var data = data.data;

				if (data.isMobileBind != 1) {
					$location.path('/mobile');
				} else if ((data.isMobileBind == 1) && (data.activation == 'REG')) {
					$location.path('/bill');
				} else if ((data.activation != 'REG') && data.email) {
					$scope.email = data.email;
					$scope.Mail.isReadOnly = true;
				}

			})
		}

		$scope.Mail = {
			NextFlag: false,
			errorFlag: false,
			isReadOnly: false,
			sendEmailShow: true,
			emailAddress: '',
			sendMail: function() {
				var inputEmail = $scope.email
				base = this,
					tokenID = Cookies.get('token');

				if (base.isEmail(inputEmail)) {
					var referer = location.href;
					
					authEmail.sendEmail(inputEmail, referer).success(function(data) {
						if (data.ok) {
							base.NextFlag = true;
							base.errorFlag = false;
							base.emailAddress = data.data;
						} else {
							base.NextFlag = false;
							base.errorFlag = true;
							$scope.mailTips.mailError = data.data;
						}
					});
					this.sendEmailShow = false;
					var flag = $timeout(function() {
						base.sendEmailShow = true;
					}, 60000);

				} else {
					base.errorFlag = true;
					$scope.mailTips.mailError = '您输入的邮箱错误，请确认后重新输入';
				}
			},
			goToMailBox: function() {
				console.log("message");
			},
			isEmail: function(email) {
				var regMail = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
				return regMail.test(email);
			}
		}

		$scope.mailTips = {
			mailError: '您输入的邮箱错误，请确认后重新输入'
		}
	}
]);

//领取话费
phoneChargeApp.controller('billCtrl', ['$scope', '$location', '$timeout', 'billCharge', 'userInfo',
	function($scope, $location, $timeout, billCharge, userInfo) {

		if (!userInfo.isLogin()) {
			$location.path('/');
		} else {
			userInfo.checkBindInfo().success(function(data) {
				var data = data.data;

				if (data.isMobileBind != 1) {
					$location.path('/mobile');
				} else if ((data.isMobileBind == 1) && (data.activation != 'REG')) {
					$location.path('/email');
				}

			})
		}

		//初始化数据
		$scope.Bill = {
			level2: {
				num: 0,
				got: 0,
				start: 0,
				end: 0,
				istart: 0,
				txt: '立即开抢'
			},
			level1: {
				num: 0,
				got: 0,
				start: 0,
				end: 0,
				istart: 0,
				txt: '立即开抢'
			},
			level0: {
				num: 0,
				got: 0,
				start: 0,
				end: 0,
				istart: 0,
				txt: '立即开抢'
			}
		}

		$scope.Bill.modalShown = false;
		$scope.Bill.DialogType = 'popTpl';
		$scope.Bill.showtype = 'success';

		phoneNum = '';
		userInfo.checkBindInfo().success(function(data) {
			phoneNum = data.data.mobile || '';
		});
		//获得用户信息条数
		num = 6;

		$scope.toggleModal = function(type, charge, event) {

			billCharge.getStatus(type).success(function(data) {
				var codeType = data.code,
					//是否演示遮罩层
					isMask = false;
				// var codeType = 201;
				// '200'=>'成功',
				// '201'=>'抢到',
				// '202'=>'已被抢光',
				// '311'=>'配置错误！',
				// '314'=>'不存在的Api',
				// '315'=>'错误的活动类型',
				// '316'=>'已经参加过活动',
				// '317'=>'抢购失败！',
				// '300'=>'没有登录！',
				// '301'=>'不正确的用户！',
				// '302'=>'手机未绑定！',
				// '303'=>'邮箱未绑定！',
				// '304'=>'此活动还未开始！',
				// 注：311,314,315,317没有弹出，用户自己刷新
				switch (codeType) {
					case 200:
					case 201:
						$('#J_share').show();
						$('.charge').html(charge);
						$('.phoneNum').text(phoneNum);
						isMask = true;
						break;
					case 316:
						$('#J_charge').show();
						isMask = true;
						break;
					case 300:
						$('#J_login').show();
						isMask = true;
						break;
					case 302:
						$('#J_mobile').show();
						isMask = true;
						break;
					case 303:
						$('#J_email').show();
						isMask = true;
						break;
				}

				if (isMask) {
					$scope.Bill.modalShown = !$scope.Bill.modalShown;
				}
			})
			event.preventDefault();
		};

		function getActStatus() {
			billCharge.getEventStatus().success(function(data) {
				$scope.Bill = {
					level2: {
						num: data.data['2'].num,
						got: data.data['2'].got,
						start: data.data['2'].start,
						end: data.data['2'].end,
						istart: data.data['2'].istart,
						txt: ((data.data['2'].got == 0) ? '抢光了' : '立即开抢')
					},
					level1: {
						num: data.data['1'].num,
						got: data.data['1'].got,
						start: data.data['1'].start,
						end: data.data['1'].end,
						istart: data.data['1'].istart,
						txt: ((data.data['1'].got == 0) ? '抢光了' : '立即开抢')
					},
					level0: {
						num: data.data['0'].num,
						got: data.data['0'].got,
						start: data.data['0'].start,
						end: data.data['0'].end,
						istart: data.data['0'].istart,
						txt: ((data.data['0'].got == 0) ? '抢光了' : '立即开抢')
					},
					time:data.data.ntime
				}

				var time = $scope.Bill.time;

				function countDown() {

					time = time - 1;

					if(time <= 0){
						location.reload();
					}

					var h = Math.floor(time/(60*60)) % 24,
					    m = Math.floor(time/(60)) % 60,
					    s = Math.floor(time) % 60;

					$('#J_countDown').html('<b>' + h + '</b>小时<b>' + m + '</b>分<b>' + s + '</b>秒');
				}

				setInterval(function(){
					countDown();	
				},1000);

			});
		}

		getActStatus();

		billCharge.getUserData(num).success(function(data) {
			$timeout(function() {
				$scope.Bill.userDatas = data.data;
			}, 600)
		});


	}
]);