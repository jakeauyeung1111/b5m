fis.config.merge({
    modules : {
        parser : {less : ['less']}
    },

    roadmap : {
        ext: {
            less : 'css'
        },

        path : [

            //发布map.json -> map-common.json
            {
                reg : 'map.json',
                release : '/static/maps/map-minihome.json'
            },

            //排除所有conf.js项目配置文件
            {
                reg:/\/conf-[^\.]*\.js/i,
                release:false
            },

            

            //只命中/static/html
            {
                reg:/\/static\/html\/(?!minihome\/)/i,
                release:false
            },

            //只命中/static/css
            {
                reg:/\/static\/css\/(?!minihome\/)/i,
                release:false
            },

            //只命中/static/scripts
            {
                reg:/\/static\/scripts\/(?!minihome\/)/i,
                release:false
            },

            //只命中/static/images
            {
                reg:/\/static\/images\/(?!minihome\/)/i,
                release:false
            },

            //只命中/static/public/sea-modules
            {
                reg:/\/static\/public\/(?!sea-modules\/)/i,
                release:false
            },


            //将自动合并图片的文件release到images/相应目录下
            {
                reg:/\/static\/css\/center\/([^\/]+\.png)/i,
                release:'/static/images/minihome/$1'
            }
        ]
    },
    settings : {},
    pack : {
        '/static/css/minihome/mini.css' : [
            '/static/css/minihome/mini-base.less',
            '/static/css/minihome/minihome.less',
            '/static/css/minihome/mini-article.less',
            '/static/css/minihome/mini-classify.less'
        ],
        '/static/scripts/minihome/miniFed.js' : [
            '/static/scripts/minihome/miniFed.js'
        ]
    },
    deploy : {
        local : {
            to : '../b5m'
        }
    }

});