fis.config.merge({
    modules : {
        parser : {less : ['less']}
    },

        roadmap : {
        ext: {
            less : 'css'
        },

        path : [

            //发布map.json -> map-common.json
            {
                reg : 'map.json',
                release : '/static/maps/map-korea-culture.json'
            },

            //排除所有conf.js项目配置文件
            {
                reg:/\/conf-[^\.]*\.js/i,
                release:false
            },

            //排除public目录
            {
                reg:/\/public\//i,
                release:false
            },

            //只命中/static/html/korea/korea-culture
            {
                reg:/\/static\/html\/(?!korea\/)/i,
                release:false
            },
            {
                reg:/\/static\/html\/korea\/(?!culture\/)/i,
                release:false
            },

            //只命中/static/css/korea/korea-culture
            {
                reg:/\/static\/css\/(?!korea\/)/i,
                release:false
            },
            {
                reg:/\/static\/css\/korea\/(?!culture\/)/i,
                release:false
            },


            //只命中/static/scripts/korea/korea-culture
            {
                reg:/\/static\/scripts\/(?!korea\/)/i,
                release:false
            },
            {
                reg:/\/static\/scripts\/korea\/(?!culture\/)/i,
                release:false
            },

            //只命中/static/images/korea/korea-culture
            {
                reg:/\/static\/images\/(?!korea\/)/i,
                release:false
            },
            {
                reg:/\/static\/images\/korea\/(?!culture\/)/i,
                release:false
            },

            //将自动合并图片的文件release到images/相应目录下
            {
                reg:/\/static\/css\/korea\/culture\/([^\/]+\.png)/i,
                release:'/static/images/korea/culture/$1'
            }
        ]
    },
    settings : {},
    pack : {
        '/static/css/korea/culture/culture.css':[
            '/static/css/korea/culture/*.less'
        ],
        '/statics/scripts/korea/culture/korea-culture.js':[
            '/static/scripts/korea/culture/jquery.jqote2.js',
            '/static/scripts/korea/culture/korea.js'
        ]
    },
    deploy : {
        local : {
            to : '../b5m'
        },
        zibu : {
            to : '../b5mtest'
        }
    }

});