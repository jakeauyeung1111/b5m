fis.config.merge({
    modules : {
        parser : {less : ['less']},
        postpackager : 'seajs'
    },

    //只包含 b5m-的开发目录
    project : { include : /\/static\/public\/sea-modules\/m\//i },
    // project : { exclude : /\/static\/public\/sea-modules\/(?!b5m-[^\/]*\/)/i },

    roadmap : {

        ext: {
            less : 'css'
        },

        path : [


            //seajs 配置
    /*       {
                reg : /^\/static\/public\/sea-modules\/b5m-([^\/]+)\/\1\.(js|coffee|less|css)$/i,
                isMod : true,
                useSprite : true,
                id : '$1'
            },*/

            {
                //sea-modules根目录配置
                reg : /^\/static\/public\/sea-modules\/(.*)\.(js|coffee|less|css)$/i,
                isMod : true,
                useSprite : true,
                id : '$1'
            },

            //不发布map.json
       /*     {
                reg : 'map.json',
                release : false
            },

            //排除所有conf.js项目配置文件
           {
                reg:/\/conf-[^\.]*\.js/i,
                release:false
            },

            //命中/static/public和html
            {
                reg:/\/static\/(?!public|html\/)/i,
                release:false
            },

            //排除public/!sea-modules
            {
                reg:/\/static\/public\/(?!sea-modules\/)/i,
                release:false
            },

            //排除html/!public
         {
                reg:/\/static\/html\/(?!public\/)/i,
                release:false
            },*/


            //将sea-module.js放到static/public/sea-modules下
            {
                reg:/\/(sea-config\.js)/i,
                useHash:false,
                release:'/static/public/sea-modules/$1'
            },

            //无需生成map.json
            {
                reg : 'map.json',
                release :false
            }

        ]
    },
    settings : {

        postprocessor : {
            jswrapper : {
                type : 'amd'
            }
        },
        optimizer : {
            'uglify-js' : {
                mangle : {
                    except : [ 'require' ]
                }
            }
        }

    },

    deploy : {
        local : {
            to : '../b5m'
        }
    }

});


fis.config.set('settings.spriter.csssprites', {
    margin: 50
});