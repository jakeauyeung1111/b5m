fis.config.merge({
    modules : {
        parser : {less : ['less']}
    },

    roadmap : {
        ext: {
            less : 'css'
        },

        path : [

            //发布map.json -> map-common.json
            {
                reg : 'map.json',
                release : '/static/maps/map-common.json'
            },

            //排除所有conf.js项目配置文件
            {
                reg:/\/conf-[^\.]*\.js/i,
                release:false
            },

            //排除public目录
            {
                reg:/\/public\//i,
                release:false
            },

            {
                reg:/\/static\/html\/(?!common\/)/i,
                release:false
            },

           {
                reg:/\/static\/css\/(?!common.*\/)/i,
                release:false
            },

            {
                reg:/\/static\/css\/common\/(?!common-src.*)/i,
                release:false
            },
            {
                reg:/\/static\/images\/(?!common\/)/i,
                release:false
            },
            {
                reg:/\/static\/scripts\/(?!common\/)/i,
                release:false
            },
            {
                reg:/\/static\/scripts\/common\/(?!common-src.*)/i,
                release:false
            },
           {
                reg:/\/static\/css\/common\/([^\/]+\.png)/i,
                release:'/static/images/common/$1'
            }
        ]
    },
    settings : {},
    pack : {
        '/static/css/common/common-src.css' : ['/static/css/common/common-src/common.less'],
        '/static/scripts/common/common-src.js' : [
            '/static/scripts/common/common-src/common.js',
            '/static/scripts/common/common-src/common-topbar.js'
        ]
    },
    deploy : {
        local : {
            to : '../b5m'
        }
    }

});