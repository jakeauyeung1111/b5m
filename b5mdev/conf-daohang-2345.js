fis.config.merge({
    modules : {
        parser : {less : ['less']}
    },

        roadmap : {
        ext: {
            less : 'css'
        },

        path : [

            //发布map.json -> map-common.json
            {
                reg : 'map.json',
                release : '/static/maps/map-daohang-2345.json'
            },

            //排除所有conf.js项目配置文件
            {
                reg:/\/conf-[^\.]*\.js/i,
                release:false
            },

            //排除public目录
            {
                reg:/\/public\//i,
                release:false
            },

            //只命中/static/html/korea/korea-culture
            {
                reg:/\/static\/html\/(?!daohang\/)/i,
                release:false
            },
            {
                reg:/\/static\/html\/daohang\/(?!2345\/)/i,
                release:false
            },

            //只命中/static/css/korea/korea-culture
            {
                reg:/\/static\/css\/(?!daohang\/)/i,
                release:false
            },
            {
                reg:/\/static\/css\/daohang\/(?!2345\/)/i,
                release:false
            },




            //只命中/static/scripts/korea/korea-culture
            {
                reg:/\/static\/scripts\/(?!daohang\/)/i,
                release:false
            },
            {
                reg:/\/static\/scripts\/daohang\/(?!2345\/)/i,
                release:false
            },

            //只命中/static/images/korea/korea-culture
            {
                reg:/\/static\/images\/(?!daohang\/)/i,
                release:false
            },
            {
                reg:/\/static\/images\/daohang\/(?!2345\/)/i,
                release:false
            },

            //将自动合并图片的文件release到images/相应目录下
            {
                reg:/\/static\/css\/daohang\/2345\/([^\/]+\.png)/i,
                release:'/static/images/daohang/2345/$1'
            }
        ]
    },
    settings : {},
    pack : {
        '/static/css/daohang/2345/index.css':[
            '/static/css/daohang/2345/*.less'
        ]
    },
    deploy : {
        local : {
            to : '../b5m'
        },
		wuchen:{
		    to:'C:/xampp/htdocs/b5m-web-frontpage/b5m'
		}
    }

});