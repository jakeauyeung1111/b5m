fis.config.merge({
    modules : {
        parser : {less : ['less']}
    },

    roadmap : {
        ext: {
            less : 'css'
        },

        path : [

            //发布map.json -> map-common.json
            {
                reg : 'map.json',
                release : '/static/maps/map-tao.json'
            },

            //排除所有conf.js项目配置文件
            {
                reg:/\/conf-[^\.]*\.js/i,
                release:false
            },

            

            //只命中/static/html
            {
                reg:/\/static\/html\/(?!tao\/)/i,
                release:false
            },

            //只命中/static/css
            {
                reg:/\/static\/css\/(?!tao\/)/i,
                release:false
            },

            //只命中/static/scripts
            {
                reg:/\/static\/scripts\/(?!tao\/)/i,
                release:false
            },

            //只命中/static/images
            {
                reg:/\/static\/images\/(?!tao\/)/i,
                release:false
            },

            //排除public目录
            {
                reg:/\/public\//i,
                release:false
            },

            //将自动合并图片的文件release到images/相应目录下
            {
                reg:/\/static\/css\/tao\/([^\/]+\.png)/i,
                release:'/static/images/tao/$1'
            }
        ]
    },
    settings : {},
    pack : {
        '/static/css/tao/ttj.css' : [
            '/static/css/tao/brand/brand.less'
        ]
    },
    deploy : {
        local : {
            to : '../b5m'
        },
		wuchen:{
		    to:'C:/xampp/htdocs/b5m-web-frontpage/b5m'
		}
    }

});