fis.config.set('modules.postpackager', function(ret){
    console.log(ret.map.pkg.length);
});

fis.config.merge({
    modules : {
        parser : {less : ['less']}
    },

        roadmap : {
        ext: {
            less : 'css'
        },

        path : [

            //发布map.json -> map-common.json
            {
                reg : 'map.json',
                release : '/static/maps/map-haiwai-america.json'
            },

            //排除所有conf.js项目配置文件
            {
                reg:/\/conf-[^\.]*\.js/i,
                release:false
            },

            //排除public目录
            {
                reg:/\/public\//i,
                release:false
            },

            //只命中/static/html/haiwai/america
            {
                reg:/\/static\/html\/(?!haiwai\/)/i,
                release:false
            },
            {
                reg:/\/static\/html\/haiwai\/(?!america\/)/i,
                release:false
            },

            //只命中/static/css/korea/
            {
                reg:/\/static\/css\/(?!haiwai\/)/i,
                release:false
            },
            {
                reg:/\/static\/css\/haiwai\/(?!america\/)/i,
                release:false
            },

            //只命中/static/scripts/korea/
            {
                reg:/\/static\/scripts\/(?!haiwai\/)/i,
                release:false
            },
            {
                reg:/\/static\/scripts\/haiwai\/(?!america\/)/i,
                release:false
            },

            //只命中/static/images/korea/
            {
                reg:/\/static\/images\/(?!haiwai\/)/i,
                release:false
            },
            {
                reg:/\/static\/images\/haiwai\/(?!america\/)/i,
                release:false
            },

            //将自动合并图片的文件release到images/相应目录下
            {
                reg:/\/static\/css\/haiwai\/america\/([^\/]*)\/([^\/]+\.png)/i,
                release:'/static/images/haiwai/america/$1/$2'
            }
        ]
    },
    settings : {},
    pack : {
         '/static/css/haiwai/america/pinpai/pinpai.css' : ['/static/css/haiwai/america/pinpai/*.less']   //明星集合页
    },
    deploy : {
        local : {
            to : '../b5m'
        }
    }

});