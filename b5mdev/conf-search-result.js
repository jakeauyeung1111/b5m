fis.config.merge({
    modules : {
        parser : {less : ['less']}
    },

    roadmap : {
        ext: {
            less : 'css'
        },

        path : [

            //发布map.json -> map-common.json
            {
                reg : 'map.json',
                release : '/static/maps/map-search-result.json'
            },

            //排除所有conf.js项目配置文件
            {
                reg:/\/conf-[^\.]*\.js/i,
                release:false
            },

            //排除public目录
            {
                reg:/\/public\//i,
                release:false
            },

            //只命中/static/html/korea/
            {
                reg:/\/static\/html\/(?!search\/)/i,
                release:false
            },

            //只命中/static/css/korea/
            {
                reg:/\/static\/css\/(?!search\/)/i,
                release:false
            },

            //只命中/static/scripts/korea/
            {
                reg:/\/static\/scripts\/(?!search\/)/i,
                release:false
            },

            //只命中/static/images/korea/
            {
                reg:/\/static\/images\/(?!search\/)/i,
                release:false
            },

            //将自动合并图片的文件release到images/相应目录下
            {
                reg:/\/static\/css\/search\/([^\/]+)\/([^\/]+\.png)/i,
                release:'/static/images/search/$1/$2'
            }
        ]
    },
    settings : {},
    pack : {
        '/static/css/search/search_index.css' : [
            '/static/css/search/search_head_v3.css',
            '/static/css/search/search_result/search_result_v3.css',
        ]//合并所有的less到star.css
    },
    deploy : {
        local : {
            to : '../b5m'
        }
    }

});