fis.config.merge({
    modules : {
        parser : {less : ['less']}
    },

        roadmap : {
        ext: {
            less : 'css'
        },

        path : [

            //发布map.json -> map-common.json
            {
                reg : 'map.json',
                release : '/static/maps/map-korea.json'
            },

            //排除所有conf.js项目配置文件
            {
                reg:/\/conf-[^\.]*\.js/i,
                release:false
            },

            //排除public目录
            {
                reg:/\/public\//i,
                release:false
            },

            //只命中/static/html/korea/
            {
                reg:/\/static\/html\/(?!korea\/)/i,
                release:false
            },

            //只命中/static/css/korea/
            {
                reg:/\/static\/css\/(?!korea\/)/i,
                release:false
            },

            //只命中/static/scripts/korea/
            {
                reg:/\/static\/scripts\/(?!korea\/)/i,
                release:false
            },

            //只命中/static/images/korea/
            {
                reg:/\/static\/images\/(?!korea\/)/i,
                release:false
            },

            //将自动合并图片的文件release到images/相应目录下
            {
                reg:/\/static\/css\/korea\/([^\/]*)\/([^\/]+\.png)/i,
                release:'/static/images/korea/$1/$2'
            }
        ]
    },
    settings : {},
    pack : {
        '/static/css/korea/korea.css':[
            '/static/css/korea/star/star.less',//明星集合页
            '/static/css/korea/minipop/zixun.less',//资讯mini弹出窗
            '/static/css/korea/doctor/doctor.less',//医疗美容
            '/static/css/korea/video/video.less',//商品，
            '/static/css/korea/trip/trip.less' //旅游
        ],
        '/statics/scripts/korea/korea.js':[
            '/static/scripts/korea/doctor/doctor.js',//医疗美容
            '/statics/scripts/korea/trip/trip.js',//旅游攻略
            '/statics/scripts/korea/video/video.js'//视频频道
        ]
    },
    deploy : {
        local : {
            to : '../b5m'
        },
		zibu:{
		    to:'../b5mtest'
		}
    }

});